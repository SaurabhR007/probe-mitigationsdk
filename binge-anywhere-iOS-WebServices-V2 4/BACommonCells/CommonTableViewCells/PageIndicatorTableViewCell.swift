//
//  PageIndicatorTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class PageIndicatorTableViewCell: UITableViewCell {

    @IBOutlet weak var firstIndicator: UILabel!
    @IBOutlet weak var secondIndicator: UILabel!
    @IBOutlet weak var firstIndicatorWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        configUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    private func configUI() {
        self.backgroundColor = .BAdarkBlueBackground
        firstIndicator.backgroundColor = .BABlueColor
        secondIndicator.backgroundColor = .BAmidBlue
        firstIndicator.makeCircular(roundBy: .height)
        secondIndicator.makeCircular(roundBy: .height)
    }

    func configureCell(with type: PageIndicator) {
        switch type {
        case .first:
            firstIndicator.backgroundColor = .BABlueColor
            secondIndicator.backgroundColor = .BAmidBlue
            firstIndicatorWidthConstraint.constant = 32
        case .second:
            firstIndicator.backgroundColor = .BAmidBlue
            secondIndicator.backgroundColor = .BABlueColor
            firstIndicatorWidthConstraint.constant = 8
        }
    }

}

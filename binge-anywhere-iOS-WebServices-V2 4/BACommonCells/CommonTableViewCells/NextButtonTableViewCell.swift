//
//  NextButtonTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 17/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol NextButtonActionDelegate: AnyObject {
    func nextButtonTapped()
}

class NextButtonTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var nextButton: CustomButton!
    @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    weak var delegate: NextButtonActionDelegate?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
		configureUI()
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
		self.contentView.backgroundColor = .BAdarkBlueBackground
		nextButton.text("Next")
		nextButton.style(.primary)
        nextButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
	}

    func configureCell(source: TableViewCellSourceModel) {
        nextButton.isHidden = false
        switch source {
        case .verification: nextButton.text("SignUp")
        case .otp:
			nextButton.text("Login")
			nextButton.style(.tertiary, isActive: false)
		case .login, .signup, .sidSelection, .baIdSelection:
			nextButton.text("Proceed")
			nextButton.style(.tertiary, isActive: false)
        case .linkAccount:
			nextButton.text("Enter OTP")
        case .linkAccountVerification:
			nextButton.text("Link Accounts")
		case .subscription:
            //nextButton.text("Proceed")
			nextButton.text("Start Watching")
			nextButton.style(.primary)
        case .updateDetails:
            nextButton.text("Update Profile")
            nextButton.style(.primary)
        default:
            break
        }
    }
    
    func doNotConfigureButton() {
        nextButton.isHidden = true
    }
   
    // Despite of removing this method just updated the buttonTopConstraint value as cant update whole method written by other dev as it can affect release and testing
    func configureCellForAccountSection() {
        buttonCenterYConstraint.isActive = false
        buttonBottomConstraint.isActive = true
        buttonTopConstraint.constant = 10
    }
    
    // MARK: - Actions
    @IBAction func nextButtonTapped() {
        if let delegate = delegate {
            delegate.nextButtonTapped()
        }
    }
}

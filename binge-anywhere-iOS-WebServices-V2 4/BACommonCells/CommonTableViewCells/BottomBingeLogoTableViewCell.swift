//
//  BottomBingeLogoTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 17/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

class BottomBingeLogoTableViewCell: UITableViewCell {
	@IBOutlet weak var indicatorStack: UIStackView!
    @IBOutlet weak var stackWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerLogoImageView: UIImageView!    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
	}
	
	override func prepareForReuse() {
		for item in indicatorStack.arrangedSubviews {
			item.removeFromSuperview()
		}
	}
    
    // MARK: - Functions
    func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
    }
	
    func configureIndicator(number: Int, selectedIndex: Int) {
        if number <= 2{
            stackWidthConstraint.constant = 45.0
        }
        for index in 0..<number {
            let indicatorLabel = UIView(frame: CGRect(0, 0, 8, 8))
            if index == selectedIndex {
                indicatorLabel.backgroundColor = .BABlueColor
                indicatorLabel.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
            } else {
                indicatorLabel.backgroundColor = .BAmidBlue
                indicatorLabel.widthAnchor.constraint(equalToConstant: 8.0).isActive = true
            }
            indicatorLabel.heightAnchor.constraint(equalToConstant: 8.0).isActive = true
            indicatorLabel.makeCircular(roundBy: .height)
            indicatorStack.addArrangedSubview(indicatorLabel)
        }
    }
}

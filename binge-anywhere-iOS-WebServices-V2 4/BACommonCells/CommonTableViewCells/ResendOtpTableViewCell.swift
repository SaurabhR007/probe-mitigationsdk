//
//  ResendOtpTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 18/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol ResendOtpButtonActionDelegate: AnyObject {
    func resendButtonTapped()
    func showResendCountAlert()
}

class ResendOtpTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var resendOtpButton: CustomButton!
	@IBOutlet weak var codeExpiryLabel: CustomLabel!

    // MARK: - Variables
    weak var delegate: ResendOtpButtonActionDelegate?
    var timer = Timer()
    private var resendCount = 0

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
		configureUI()
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Functions
    func configureUI() {
       // NotificationCenter.default.addObserver(self, selector: #selector(resendOtpTimer), name: NSNotification.Name("resendOTPTimerValue"), object: nil)
        resendOtpButton.isUserInteractionEnabled = false
		resendOtpButton.tintColor = .BAmidBlue
		self.contentView.backgroundColor = .BAdarkBlueBackground
        codeExpiryLabel.text = "Resend OTP in \(BAConfigManager.shared.configModel?.data?.config?.otpDuration ?? 30) seconds"
		resendOtpButton.text(kResendOTP)
        resendOtpButton.titleLabel?.font = .skyTextFont(.medium, size: 16)
        resendOtpButton.underline()
        codeExpiryLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
		runCodeExpiryTimer()
	}

	func runCodeExpiryTimer() {
        _ = BAConfigManager.shared.configModel?.data?.config?.otpResentCount ?? 0
            var resetCount = BAConfigManager.shared.configModel?.data?.config?.otpDuration ?? 0
            timer.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                if resetCount > 0 {
                    resetCount -= 1
                    self.resendOtpButton.tintColor = .BAmidBlue
                    if self.resendOtpButton.isHidden == true {
                      self.codeExpiryLabel.isHidden = true
                    } else {
                        self.codeExpiryLabel.isHidden = false
                    }
                    self.codeExpiryLabel.text = "Resend OTP in \(resetCount) seconds"
                    self.resendOtpButton.isUserInteractionEnabled = false
                } else {
                    timer.invalidate()
                    self.resendOtpButton.tintColor = .BABlueColor
                    self.resendOtpButton.isUserInteractionEnabled = true
                    self.codeExpiryLabel.isHidden = true
                }
            }
            RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
	}

    // MARK: - Actions
    @IBAction func resendButtonTapped() {
        let otpCount = BAConfigManager.shared.configModel?.data?.config?.otpResentCount ?? 0
        if otpCount > resendCount {
            resendOtpButton.isHidden = false
            resendCount += 1
            resendOtpButton.isUserInteractionEnabled = false
            self.resendOtpButton.tintColor = .BABlueColor
            if let delegate = delegate {
                if otpCount == resendCount {
                    resendOtpButton.isHidden = true
                    self.codeExpiryLabel.isHidden = true
                } else {
                    resendOtpTimer()
                }
                delegate.resendButtonTapped()
            }
        } else {
            resendOtpButton.isHidden = true
            self.codeExpiryLabel.isHidden = true
            delegate?.showResendCountAlert()
            resendOtpButton.isUserInteractionEnabled = false
            self.resendOtpButton.tintColor = .BAlightBlueGrey
        }
    }
    
    func resendOtpTimer() {
        runCodeExpiryTimer()
    }
}

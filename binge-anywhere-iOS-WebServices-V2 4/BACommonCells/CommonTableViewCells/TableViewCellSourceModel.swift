//
//  TableViewCellSourceModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 19/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

enum PageIndicator {
    case first
    case second
}

enum TableViewHeaderSource {
    case withTitle(String)
    case appSubscribed
    case appNotSubscribed
    case browseByLanguage
    case browseByGenre
    case trending

    var headerTitle: String {
        switch self {
        case .appSubscribed:
            return "Subscribed"
        case .appNotSubscribed:
            return "Not Subscribed"
        case .browseByLanguage:
            return "Browse By Language"
        case .browseByGenre:
            return "Browse By Genre"
        case .trending:
            return "Trending"
        case .withTitle(let title):
            return title
        }
    }
}

enum TableViewCellSourceModel {
    case login
    case otp
    case signup
    case verification
    case linkAccount
    case linkAccountVerification
	case sidSelection
	case baIdSelection
	case updateDetails
	case subscription
    case addressConfirm
    case installSchedule

    var headerTitle: String {
        switch self {
        case .login, .otp:
            return "Login"
        case .signup:
            return "Sign Up"
        case .verification:
            return "Verification Code"
        case .linkAccount:
            return "Enter Mobile Number \nor Subscriber ID"
        case .linkAccountVerification:
            return "Enter OTP sent \nto "
		case .sidSelection:
			return ksubscriberListHeader//"Select Subscriber ID"
		case .baIdSelection:
			return kBingeSubscriberListHeader//"You have multiple binge subscription"
		case .updateDetails:
			return "Update Your Details"
		case .subscription:
			return kSubscriptionTitle//"Current Subscription"
        case .addressConfirm:
            return "Confirm your Address"
        case .installSchedule:
            return "Installation Schedule"
		}
    }

    var headerInfoMessage: String {
        switch self {
        case .login, .signup, .linkAccountVerification, .updateDetails, .subscription:
            return ""
        case .otp, .verification:
            return "Enter the OTP sent to"
        case .linkAccount:
            return "Enter details of Tata Sky DTH account to which you want to link to your Tata Sky Binge Subscription"
		case .sidSelection:
			return "You have multiple Subscriber IDs linked to your Registered Mobile Number. Please select one and proceed."
		case .baIdSelection:
			return kBingeSubsciberSubHeader//"Select one you want to proceed with"
        case .addressConfirm:
            return "Please check and confirm your address below to arrange for the installation of the Amazon Fire TV Stick - Tata Sky Edition"
        case .installSchedule:
            return "Select your preferred date and time for installation"
		}
    }
}

//
//  ForgetPasswordTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 27/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol ForgetButtonActionDelegate: AnyObject {
    func forgetButtonTapped()
}

class ForgetPasswordTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var forgotButton: CustomButton!
	@IBOutlet weak var mobileLabel: CustomLabel!

    // MARK: - Variables
    weak var delegate: ForgetButtonActionDelegate?
	var mobileInfotext = ""
	
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        forgotButton.fontAndColor(font: .skyTextFont(.medium, size: 14), color: .BABlueColor)
		mobileLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size:  14), color: .BAwhite)
		mobileLabel.isHidden = true
    }

    func configureCell(source: TableViewCellSourceModel) {
        switch source {
        case .linkAccountVerification:
            mobileLabel.isHidden = false
            forgotButton.isHidden = false
			forgotButton.text("Resent OTP Code")
        case .login:
            mobileLabel.isHidden = true
            forgotButton.isHidden = false
			forgotButton.text("Forgot Password?")
            forgotButton.underline()
		case .otp:
			forgotButton.isHidden = true
			mobileLabel.isHidden = false
			mobileLabel.text = mobileInfotext
        default:
            break
        }
    }

    // MARK: - Actions
    @IBAction func forgetButtonTapped() {
        if let delegate = delegate {
            delegate.forgetButtonTapped()
        }
    }

}

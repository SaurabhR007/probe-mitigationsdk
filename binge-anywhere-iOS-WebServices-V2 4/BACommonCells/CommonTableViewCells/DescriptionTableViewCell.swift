//
//  DescriptionTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: CustomLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    private func configureUI() {
        self.backgroundColor = .BAdarkBlueBackground
        descriptionLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 14), color: .BAlightBlueGrey)
    }

    func configureCell(_ description: String) {
        descriptionLabel.text = description
    }
}

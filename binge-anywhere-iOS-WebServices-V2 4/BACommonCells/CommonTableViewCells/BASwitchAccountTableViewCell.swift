//
//  BASwitchAccountTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 16/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

protocol BASwitchAccountTableViewCellProtocol: class {
    func dismissController(index: Int?)
}

class BASwitchAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var gradientBackImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userIdLabel: CustomLabel!
    @IBOutlet weak var userNameLabel: CustomLabel!
    @IBOutlet weak var separatorLabel: CustomLabel!
    @IBOutlet weak var nameInitials: CustomLabel!
    //@IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    weak var delegate: BASwitchAccountTableViewCellProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        //self.activityIndicator.isHidden = true
        configUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configUI() {
        let randomColor: [UIColor] = [.BApinkColor, .BAmidBlue, .BATrendingPurple, .BAerrorRed, .BAExperingRed, .BAExclusiveOrange, .BANewBlue]
        selectionStyle = .none
		nameInitials.backgroundColor = .clear
        //UtilityFunction.shared.addCircularGradient(imageView: gradientBackImageView)
        profileImageView.makeCircular(roundBy: .height)
        separatorLabel.backgroundColor = .BAmidBlue
        nameInitials.makeCircular(roundBy: .height)
        nameInitials.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30), color: .BAwhite)
		UIView.animate(withDuration: 1, animations: {
			self.nameInitials.layer.backgroundColor = (randomColor.randomElement() ?? .BANewBlue).cgColor
		})
    }

    func configureCell(data: AccountDetailListResponseData, firstIndex: Int) {
        selectionStyle = .none
        profileImageView.makeCircular(roundBy: .height)
        userIdLabel.text = "ID:" + " " + String(data.baId ?? 0)
        userNameLabel.text = data.aliasName
        let initials = (data.aliasName ?? " ")
        let index = initials.index(initials.startIndex, offsetBy: 0)
		if index <= initials.endIndex {
			nameInitials.text = String(initials[index]).capitalizingFirstLetter()
		}
        if firstIndex == 0 {
            UtilityFunction.shared.addCircularGradient(imageView: gradientBackImageView)
            // Do Nothing
        } else {
            UIView.animate(withDuration: 1, animations: {
                self.nameInitials.layer.backgroundColor = UIColor.BAdarkBlueBackground.cgColor
                self.gradientBackImageView.makeCircular(roundBy: .height)
                self.gradientBackImageView.backgroundColor = .clear
                UtilityFunction.shared.addCircularGradientWithoutColor(imageView: self.gradientBackImageView)
                self.profileImageView.makeCircular(roundBy: .height)
            })
        }
    }
}

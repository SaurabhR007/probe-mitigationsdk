//
//  TitleHeaderTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 18/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

class TitleHeaderTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: CustomLabel!
	@IBOutlet weak var infoLabel: CustomLabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

		configureUI()
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
		self.contentView.backgroundColor = .BAdarkBlueBackground
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30 * screenScaleFactorForWidth), color: .BAwhite)
        infoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAlightBlueGrey)
	}

    func configureCell(source: TableViewCellSourceModel) {
        titleLabel.text = source.headerTitle
        infoLabel.text = source.headerInfoMessage
        switch source {
		case .login, .signup, .updateDetails, .otp:
            hideInfoLabel()
        case .subscription:
            titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 28 * screenScaleFactorForWidth), color: .BAwhite)
            hideInfoLabel()
        case .linkAccount, .linkAccountVerification:
            titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 24), color: .BAwhite)
            infoLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 15), color: .BAwhite)
		case .sidSelection, .baIdSelection:
			titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 28 * screenScaleFactorForWidth), color: .BAwhite)
			infoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAlightBlueGrey)
        case .addressConfirm,.installSchedule:
            titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 28 * screenScaleFactorForWidth), color: .BAwhite)
            infoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 17 * screenScaleFactorForWidth), color: UIColor(red: 182.0 / 255.0, green: 185.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0))
        case .verification:
            break
        }
    }
    
    func configureConstraint(paddingleft:CGFloat, width:CGFloat){
        titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: paddingleft).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: width).isActive = true

    }

	private func hideInfoLabel() {
		infoLabel.text = ""
		infoLabel.isHidden(true)
	}
}

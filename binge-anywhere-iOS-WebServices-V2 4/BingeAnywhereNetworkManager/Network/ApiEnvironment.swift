//
//  ApiEnvironment.swift
//  BingeAnywhere
//
//  Created by Shivam on 16/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

enum APIEnvironment: String {
    case qa = "https://tatasky-qa-tsmore-kong.videoready.tv/"
    //case ta = "https://tatasky-uat-tsmore-kong.videoready.tv/"
    case uat = "https://tatasky-uat-tsmore-kong.videoready.tv/"  // Change this
   // case uat = "https://tatasky-staging-tsmore-kong.videoready.tv/" // Change For Stagging
    //case uat = "https://tatasky-staging-dr-tsmore-kong.videoready.tv/" // Change For Stagging DR
    case prod = "https://tatasky-tsmore-kong.videoready.tv/"
    case aiml = "http://ec2-54-159-184-204.compute-1.amazonaws.com:8080"
}

enum APIPath: String {
    
    case configData = "binge-mobile-config/pub/v1/api/config/binge/mobile"
    case authenticate = "binge-mobile-services/pub/api/v3/auth/"
    case authenticateVerification = "binge-mobile-services/pub/api/v2/auth/"
    //case idLookup = "binge-mobile-services/api/v2/subscriber/account/details/"
    case tvodExpiry = "content-detail/api/v1/tvod/digital/playback/expiry/"
    case idLookup = "binge-mobile-services/api/v2/migration/account/details/"
    case createBingeAccount = "binge-mobile-services/api/v2/create/user"
    case changeUserPassword = "binge-mobile-services/api/v2/subscribers/password/"
    case login = "binge-mobile-services/api/v2/login/user"
    case validateRmn = "user-service/pub/api/v1/rmn/"
    case validateLoginOtp = "binge-mobile-services/pub/api/v2/login/validate/otp"
    case validateLoginPassword = "binge-mobile-services/pub/api/v2/login/password"
    case validateRegistrationOtp = "user-service/pub/api/v2/registration/validate/otp"
    case generateLoginOtp = "binge-mobile-services/pub/api/v2/user/rmn/"
    case registrationOtp = "user-service/pub/api/v1/user/registration"
    case fetchSubscriberList = "user-service/pub/api/v1/subscribers/list"
    case homeScreenData = "homescreen-client/pub/api/v1/"
    case primeResumeService = "binge-service/api/s/prime/resendSMS"
    case contentDetail = "content-subscriber-detail/api/content/info/"
    case recommendedContent = "recommendation-api/api/recommend/content/"
    case seeAllScreenData = "homescreen-client/pub/api/v2/"
    case seriesDetailData = "content-subscriber-detail/api/series"
    case changePassword = "binge-mobile-services/pub/api/v2/subscribers/"
    case forgetPassword = "binge-mobile-services/pub/api/v2/subscribers/forgot/"
    case deRegisterDevice = "binge-mobile-services/api/v2/subscriber/devices"
    case signOut = "binge-mobile-services/api/v2/logout/"
    case addAlias = "binge-mobile-services/api/v2/subscriber/"
    case editProfile = "binge-mobile-services/api/v2/subscriber/update/email"
    case atvUpgrade = "binge-mobile-services/api/v2/switch/account/atv/"
    case watchlist = "action-data-provider/recently/"
    case watching = "action-listener/api/watching"
    case addRemoveWatchlist = "action-data-provider/subscriber/"
    //case watchlistLookup = "action-data-provider/content/"
    case watchlistLookup = "event-processor/api/v1/"
    case watchListData = "action-data-provider/subscriber/favourite/"
    case switchAccount = "binge-mobile-services/api/v2/switch/account/"
    case fetchUserDetail = "binge-mobile-services/api/v2/subscriber/fetch/profile/"
    case nextPreviousDetail = "content-subscriber-detail/api/content/episode/"
    case tvodListing = "content-detail/api/v1/monetization/tvod/subscriber/list/"
    case taRecommendationData = "ta-recommendation/api/v1/binge/recommend/"
    case laFavouriteAction = "ta-recommendation/api/v1/binge/learn/FAVOURITE/"
    case laWatchAction = "ta-recommendation/api/v1/binge/learn/CLICK/"
    case preSearch = "search-connector/bingeMobile/"
    //case userList = "binge-mobile-services/api/v2/subscriber/account/details/sid/"
    case userList = "binge-mobile-services/api/v2/migration/account/details/sid/"
    case deleteUserImage = "binge-mobile-services/api/v2/subscribers/"
    case browseBy = "homescreen-client/pub/api/v2/search/"
    case deleteDevice = "binge-mobile-services/api/v2/remove/devices/"
    case browseByDetail = "search-connector/bingeMobile/search/"
    case currentPackSubscriptionDetail = "binge-mobile-services/api/v3/binge/mobile/current/subscription"
    case fetchUserBalance = "binge-mobile-services/api/v2/accountInfo/getBalance"
    case refreshRRM = "auth-service/v1/oauth/token-service/token"
    case notificationSettings = "binge-mobile-services/api/v2/change/notificationTrailer/"
    case vootPlaybackForKids = "voot-kids-playback-api/s/partner/playback"
    case vootPlaybackForSelect = "voot-select-playback-api/voot/v1/playback"
    case faq = "binge-mobile-services/pub/api/v2/help/"//"rest-api/pub/api/v1/help/"
    case zeeTag = "zee5-playback-api/tag/fetch"
    case workOrderJourney = "binge-mobile-services/api/v2/"
    case transactionHistory = "binge-mobile-services/api/v2/subscribers"
    case fetchPlayUrl = "content-detail/pub/api/v1/box-subset/"
    case viewCount = "composer/learnAction"
    case episodeDurations = "action-data-provider/episode/listing/histroy"
    case onBoardingScreens = "binge-mobile-config/pub/v1/api/screen"
    case aimlCapture = "/capture"
    // /history?subscriberId=1136080874&profileId=584c81e8-3326-4f6b-a08a-9ba6db8b4e6c&max=10&cw=false&offSet=1
    //case seasonDetailData = "content-detail/pub/api/v1/series"
}

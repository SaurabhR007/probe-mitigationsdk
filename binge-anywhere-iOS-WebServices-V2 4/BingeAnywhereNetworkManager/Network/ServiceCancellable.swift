//  Copyright © 2019 Harish Yadav. All rights reserved.
//

protocol ServiceCancellable {
    func cancel()
}

struct DummyCancellable: ServiceCancellable {
    func cancel() {}
}

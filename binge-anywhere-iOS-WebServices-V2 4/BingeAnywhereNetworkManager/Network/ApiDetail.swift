//
//  ApiDetail.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import Alamofire

typealias APIParams = [AnyHashable: Any]
typealias APIHeaders = [AnyHashable: Any]

struct APIDetail {
    var path = ""
    var parameter: APIParams = APIParams()
    var method: HTTPMethod = .GET
    var baseUrl = UtilityFunction.shared.isUatBuild() ? APIEnvironment.uat.rawValue : APIEnvironment.prod.rawValue
    var encoding: ParameterEncoding = URLEncoding.default
    var headers: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId, "deviceName": kDeviceModal]
    var isBaseUrlNeedToAppend: Bool = true
    var showLoader: Bool = true
    var showAlert: Bool = true
    var showMessageOnSuccess = false
    var isHeaderTokenRequired: Bool = true
    var supportOffline = false
    var postQueryParam: APIParams?
    
    
    init(endpoint: ServiceRequestEndPoint) {
        switch endpoint {
        case .configData(let param, _):
            path = APIPath.configData.rawValue
            parameter = param
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .faqCallback( _, _, let endUrlString):
            baseUrl = UtilityFunction.shared.isUatBuild() ?  "https://tatasky-uat-tsmore-kong.videoready.tv/binge-mobile-services/pub/api/v1/help/" : "https://tatasky-tsmore-kong.videoready.tv/binge-mobile-services/pub/api/v1/help/"
            //baseUrl = "https://tatasky-staging-dr-tsmore-kong.videoready.tv/binge-mobile-services/pub/api/v1/help/" // Uncomment for StaggingDR
            //baseUrl = "https://tatasky-qa-tsmore-kong.videoready.tv/binge-mobile-services/pub/api/v1/help/" // Uncoment for QA
            
            path = endUrlString
            parameter = [:]
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .generateOtp(let param, let header, let endUrlString):
            if endUrlString.contains("rmn") {
                path = APIPath.authenticateVerification.rawValue + endUrlString
            } else {
                path = APIPath.authenticate.rawValue + endUrlString
            }
            parameter = param
            //parameter = [:]
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .loginValidation(let param, let header, let endUrlString):
            path = APIPath.authenticateVerification.rawValue + endUrlString
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .tvodExpiry(let param, let header, let endUrlString):
            path = APIPath.tvodExpiry.rawValue + endUrlString
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .idLookUp( _, let header, let endUrlString):
            path = APIPath.idLookup.rawValue + endUrlString
            parameter = [:]
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .createBingeUser( let param, let header):
            path = APIPath.createBingeAccount.rawValue
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .login( let param, let header):
            path = APIPath.login.rawValue
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .fetchPlayData(_ , let header, let endUrlString):
            path = APIPath.fetchPlayUrl.rawValue + endUrlString
            parameter = [ : ]
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .makeViewCount(let param , let header, _):
            baseUrl = UtilityFunction.shared.isUatBuild() ? "https://tatasky-uat-kong.videoready.tv/la-proxy-app/ts/" : "https://tatasky-tsmore-kong.videoready.tv/la-proxy-app/ts/"
            //baseUrl = "https://tatasky-staging-dr-tsmore-kong.videoready.tv/la-proxy-app/ts/" // Uncomment for StaggingDR
            //baseUrl = "https://tatasky-qa-tsmore-kong.videoready.tv/la-proxy-app/ts/" // Uncoment for QA
            path = APIPath.viewCount.rawValue
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .validateRmn(let param, _):
            path = APIPath.validateRmn.rawValue + (param["rmn"] as? String ?? "")
            parameter = [:]
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .generateLoginOtp(let param, _):
            path = APIPath.generateLoginOtp.rawValue + (param["rmn"] as? String ?? "") + "/login"
            parameter = [:]
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case.deleteProfileImage(_, let header, let postParam):
            path = APIPath.deleteUserImage.rawValue + BAKeychainManager().sId + "/delete/image/"
            parameter = [:]
            postQueryParam = postParam
            headers = header
            method = .DELETE
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .generateRegistrationOtp(let param, _):
            path = APIPath.registrationOtp.rawValue
            parameter = param
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .atvUpgrade(let param, let header, let endUrlString):
            path = APIPath.atvUpgrade.rawValue + endUrlString
            parameter = param
            method = .POST
            if header != nil {
                headers = header!
            }
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .fetchSubscriberList(let param, _):
            path = APIPath.fetchSubscriberList.rawValue
            parameter = param
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .validateLoginOtp(let param, _):
            path = APIPath.validateLoginOtp.rawValue
            parameter = param
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .validateLoginPassword(let param, _):
            path = APIPath.validateLoginPassword.rawValue
            parameter = param
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .pubnubHistory(let param, let header, _):
            baseUrl = "https://ps.pndsn.com/v2/history/sub-key/sub-c-99af3d1a-707e-11e7-8293-02ee2ddab7fe/channel/sub_1413058239?count=1"
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .episodeDurations(let param, let header, _):
            path = APIPath.episodeDurations.rawValue
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .validateRegistrationOtp(let param, _):
            path = APIPath.validateRegistrationOtp.rawValue
            parameter = param
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showMessageOnSuccess = true
            showAlert = false
        case .homeScreenData(let param, let header, let endUrlString):
            path = APIPath.homeScreenData.rawValue + endUrlString
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .primeResumeService(let header):
            path = APIPath.primeResumeService.rawValue
            parameter = [:]
            headers = header
            method = .GET
            encoding = JSONEncoding.default
        case .taRecommendationData(let useCase, let queryParams, let header):
            postQueryParam = queryParams
            parameter = [ : ]
            headers = header
            path = APIPath.taRecommendationData.rawValue + useCase
            method = .POST
        case .transactionHistory(let param, let header, let endUrlString):
            path = APIPath.transactionHistory.rawValue + endUrlString
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .laFavouriteAction(_, let header, let endUrlString):
            parameter = [ : ]
            headers = header
            path = APIPath.laFavouriteAction.rawValue + endUrlString
            method = .POST
        case .laWatchAction(_, let header, let endUrlString):
            parameter = [ : ]
            headers = header
            path = APIPath.laWatchAction.rawValue + endUrlString
            method = .POST
        case .contentDetailData(let param, let header, let endUrlString):
            path = APIPath.contentDetail.rawValue + endUrlString
            method = .GET
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .recommendedContentData(let param, let header, let endUrlString):
            path = APIPath.recommendedContent.rawValue + endUrlString
            method = .GET
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .refreshRRM(let param, let header, _):
            //baseUrl = "https://tatasky-uat-kong.videoready.tv/"
            path = APIPath.refreshRRM.rawValue
            method = .POST
            parameter = param
            if header != nil {
                headers = header!
            }
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .seeAllScreenData(let param, _, let endUrlString):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            path = APIPath.seeAllScreenData.rawValue + endUrlString
            method = .GET
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .seriesDetail(let param, let header, let endUrlString):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            path = APIPath.seriesDetailData.rawValue + endUrlString
            method = .GET
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .forgetPassword(let param, _, let endUrlString):
            path = APIPath.forgetPassword.rawValue + endUrlString
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .changePassword(let param, _, let endUrlString):
            path = APIPath.changePassword.rawValue + endUrlString + "/change-password"
            method = .PUT
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .updatePassword(let param, let header, let endUrlString):
            path = APIPath.changeUserPassword.rawValue + endUrlString + "/change"
            if header != nil {
                headers = header!
            }
            method = .PUT
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .preSearchContentData(let param, _, let endUrlString):
            //baseUrl = "https://tatasky-uat-kong.videoready.tv/"
            // baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            path = APIPath.preSearch.rawValue + endUrlString
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .searchScreenData(let param, _, _):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/search-connector/bingeMobile/"
            //baseUrl = "https://tatasky-staging-dr-tsmore-kong-proxy.videoready.tv/"
            //baseUrl = "https://tatasky-uat-tsmore-kong-proxy.videoready.tv/"//"https://tatasky-uat-tsmore-kong.videoready.tv/search-connector/binge/anywhere/"
            path = "search-connector/bingeMobile/search/results"
            //path = "search/results"
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .searchFilterData(let param, _):
            //baseUrl = "https://tatasky-staging-dr-tsmore-kong-proxy.videoready.tv/portal-search/pub/api/v1/channels" // Uncommnet for Stagging
            baseUrl = UtilityFunction.shared.isUatBuild() ? "https://tatasky-uat-tsmore-kong-proxy.videoready.tv/portal-search/pub/api/v1/channels" : "https://tatasky-tsmore-kong.videoready.tv/portal-search/pub/api/v1/channels"
            //baseUrl = "https://tatasky-qa-tsmore-kong.videoready.tv/portal-search/pub/api/v1/channels" // Uncoment for QA
            path = "filter"
            method = .GET
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .notificationSettingsData(let param, let header, let endUrlString):
            path = APIPath.notificationSettings.rawValue + endUrlString
            method = .GET
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .deRegisterDevice(_, _, let endUrlString):
            path =  APIPath.deRegisterDevice.rawValue + "/" + endUrlString
            method = .DELETE
            parameter = [:]
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .getRegisteredDevices(_, _, let endUrlString):
            path =  APIPath.deRegisterDevice.rawValue + "/" + endUrlString
            method = .GET
            parameter = [:]
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .deleteDevices(_, let header, let endUrlString):
            path =  APIPath.deleteDevice.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .DELETE
            parameter = [:]
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .addAlias(_, _, let endUrlString, let postQueryParameters):
            path = APIPath.addAlias.rawValue + endUrlString
            postQueryParam = postQueryParameters
            method = .POST
            parameter = [:]
            encoding = JSONEncoding.default
            showAlert = false
        case .currentPackSubscriptionDetail(let params):
            path = APIPath.currentPackSubscriptionDetail.rawValue
            parameter = params
            method = .POST
            encoding = JSONEncoding.default
        case .continueWatchData(let param, let header, let endUrlString):
            path = APIPath.watchlist.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .GET
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .nextPreviousEpisodeData(let param, let header, let endUrlString):
            path = APIPath.nextPreviousDetail.rawValue + endUrlString
            method = .GET
            parameter = param
            if header != nil {
                headers = header!
            }
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .userListData(let param, _, _):
            baseUrl = "https://api.myjson.com/bins/qk7yw"
            method = .GET
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .signOut(_, _, let endUrlString):
            path =  APIPath.signOut.rawValue + endUrlString
            method = .POST
            parameter = [:]
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .watchingData(let param, let header, _):
            path = APIPath.watching.rawValue
            if header != nil {
                headers = header!
            }
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .addRemoveWatchlist(let param, let header, let endUrlString):
            path = APIPath.addRemoveWatchlist.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .GET
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .watchlistLookup(let param, let header, let endUrlString):
            path = APIPath.watchlistLookup.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .watchListData(let param, let header, let endUrlString):
            path = APIPath.watchListData.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .GET
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .editProfile(let param, let header):
            path = APIPath.editProfile.rawValue
            if header != nil {
                headers = header!
            }
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .zeeTag(_, let header, _):
            path = APIPath.zeeTag.rawValue
            parameter = [:]
            //headers = apiHeader
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .switchAccount(let param, let header, let endUrlString):
            path = APIPath.switchAccount.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .getPlayableContent(_, _, let endUrlString):
            baseUrl = endUrlString
            method = .GET
            parameter = [:]
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .fetchUserDetail(let header, let endUrlString):
            path = APIPath.fetchUserDetail.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .fetchUserBalance(let param, let header, _):
            path = APIPath.fetchUserBalance.rawValue //+ endUrlString
            if header != nil {
                headers = header!
            }
            method = .POST
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .tvodListing(let param, let header, let endUrlString):
            //baseUrl = "https://tatasky-uat-tsmore-kong.videoready.tv/"
            path = APIPath.tvodListing.rawValue + endUrlString//"/adeabce"
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .userListing(let header, let endUrlString):
            path = APIPath.userList.rawValue + endUrlString
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .browseBy(let param, let header, let endUrlString):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            //baseUrl = "https://tatasky-uat-kong.videoready.tv/" // Uncomment for uat
            path = APIPath.browseBy.rawValue + endUrlString
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .getBrowseByDetail(let param, let header, let endUrlString):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            //baseUrl = "https://tatasky-uat-tsmore-kong.videoready.tv/" // Uncomment for uat
            path = APIPath.browseByDetail.rawValue + endUrlString
            parameter = param
            if header != nil {
                headers = header!
            }
            method = .POST
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .vootPlabackCallForKids(let param, let header, let endUrlString):
            path = APIPath.vootPlaybackForKids.rawValue + endUrlString
            method = .POST
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .vootPlabackCallForSelect(let param, let header, let endUrlString):
            path = APIPath.vootPlaybackForSelect.rawValue + endUrlString
            method = .POST
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .preferredLanguage(let param, let header, let endUrlString):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            //baseUrl = "https://tatasky-uat-tsmore-kong-proxy.videoready.tv/" // Uncomment for uat
            path = APIPath.fetchUserDetail.rawValue + endUrlString
            method = .GET
            if header != nil {
                headers = header!
            }
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .preferredGenre(let param, let header, let endUrlString):
            //baseUrl = "https://tatasky-tsmore-kong.videoready.tv/"
            //baseUrl = "https://tatasky-uat-tsmore-kong.videoready.tv/" // Uncomment for uat
            path = APIPath.taRecommendationData.rawValue + endUrlString
            method = .POST
            if header != nil {
                headers = header!
            }
            postQueryParam = param
            parameter = [ : ]
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        case .addressDetail(let header, let endUrlString):
            path = APIPath.workOrderJourney.rawValue + endUrlString
            method = .GET
            if header != nil {
                headers = header!
            }
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
        case .fireTvCompletion(let param, let header, let postQueryParameters, let endUrlString):
            path = APIPath.workOrderJourney.rawValue + endUrlString
            method = .POST
            if header != nil {
                headers = header!
            }
            parameter = param ?? [:]
            postQueryParam = postQueryParameters
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
        case .getSlot(let param, let header, let endUrlString):
            path = APIPath.workOrderJourney.rawValue + endUrlString
            method = .POST
            if header != nil {
                headers = header!
            }
            parameter = param ?? [:]
            isHeaderTokenRequired = true
            showAlert = true
        case .confirmSlot(let param, let header, let endUrlString):
            path = APIPath.workOrderJourney.rawValue + endUrlString
            method = .POST
            if header != nil {
                headers = header!
            }
            parameter = param ?? [:]
            isHeaderTokenRequired = true
            showAlert = true
        case .marketingDetails( _, let header,_):
            path = APIPath.onBoardingScreens.rawValue
            if header != nil{
                headers = header!
            }
            method = .GET
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
        case .aimlData(let param, let header, _):
            baseUrl =  APIEnvironment.aiml.rawValue
            path = APIPath.aimlCapture.rawValue
            if header != nil {
                headers = header!
            }
            method = .POST
            
            parameter = param
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = false
        default:
            break
        }
    }
}

//  Copyright © 2019 Harish Yadav. All rights reserved.

protocol AccessAuthorizable {
    var authType: TypeOfAuthorization {get}
}

//
//  AppMessagesConstant.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 10/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

enum AppStringConstant: String {
    case otpResendLimit = "You have exceeded the number of attempts for generating OTP"
    case acaCheckFailed = "We have encountered some technical issues.Please contact customer care"
}

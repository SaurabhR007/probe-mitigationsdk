//
//  AppEnums.swift
//  BingeAnywhere
//
//  Created by Shivam on 22/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit
import ErosNow_iOS_SDK
import SonyLIVSDK

enum StoryboardType: String {
    case main
    case home
    case homeTab
    case leftSideMenu
    case BAVODDetailStoryBoard
    case seeAllStoryBoard
    case more
    case search
    case Account
    case commonAlert
    case Maketing
    var fileName: String {
        return rawValue.capitalizingFirstLetter()
    }
}

enum ViewControllers: String {
    case homeVc = "HomeVC"
    case accountVC = "AccountVC"
    case kidsVc = "KidsVC"
    case tvShowsVc = "TVShowsVC"
    case moviesVc = "MoviesVC"
    case seeAllVC = "SeeAllVC"
    case moreVC = "MoreVC"
    case recommendedSeeAllVC = "BARecommendedSeeAllVC"
    case contentDetailVC = "BAVODDetailViewController"
    case fullScreenVC = "BAFullScreenPlayerViewController"
    case webViewVC = "BAWebViewViewController"
    case zeeVC = "BAZeePlayerViewController"
    case zeeSafari = "BAZeeSafariViewController"
    case contactUsVC = "BAContactUsViewController"
    case faqViewController = "BAFAQViewController"
    case searchVC = "SearchPageVC"
    case transactionHistory = "BATransactionHistoryViewController"
    case notifications = "NotificationLandingVC"
    case subscription = "MySubscriptionVC"
    case notificationVC = "NotificationVC"
    case appDetail = "BAAppsDetailViewController"
    case notificationSettings = "NotificationSettingsVC"
    case linkAccount = "LinkAccountsVC"
    case deviceManagementVC = "BADeviceManagementVC"
    case editProfileVC = "EditProfileVC"
    case linkAccountVerificationVC = "LinkAccountVerificationVC"
    case successVC = "ApiSuccessViewController"
    case seeAllForApps = "BAAppsSeeAllViewController"
    case switchAccount = "BASwitchAccountViewController"
    case movieFullScreen = "BAFullScreenMoviePlayerViewController"
    case watchListVC = "WatchlistVC"
    case continueWatchVC = "BAContinueWatchingSeeAllViewController"
    case searchListingVC = "SearchListingVC"
    case marketingVC = "MarketingViewController"
    case partnerAlertViewController = "PartnerAlertViewController"
}

enum PageType: String {
    case kids = "BINGE_ANYWHERE_KIDS"
    case tvShows = "BINGE_ANYWHERE_TVSHOWS"
    case movies = "BINGE_ANYWHERE_MOVIES"
    case home = "BINGE_ANYWHERE_HOME"
}

enum PushScreenName: String {
    case myAccount_Screen = "MY_ACCOUNT"
    case managePack_Screen = "MANAGE_PACK"
    case selfCareRecharge_Screen = "SELFCARE_RECHARGE"
    case helpFaq_Screen = "HELP_FAQ"
    case watchList_Screen = "WATCHLIST"
    case login_Screen = "LOGIN"
    case home_Screen = "HOME"
    case detail_Screen = "DETAIL_SCREEN"
    case seeAll_Screen = "SEE_ALL"
    case appSeeAll_Screen = "appSeeAll_Screen"
    case taSeeAll_Screen = "taSeeAll_Screen"
    case continueWatch_Screen = "CONTINUE_WATCHING"
    case recommendation_Screen = "RECOMMENDATION"
}

enum BASubscriptionsConstants: String {
    case atv = "atv"
    case binge = "ANYWHERE"
    case stick = "ANDROID_STICK"
}

enum TAConstant: String {
    case showType = "showType"
    case contentType = "contentType"
    case continueWatching = "CONTINUE_WATCHING"
    case recommendationType = "RECOMMENDATION"
    case editorial = "EDITORIAL"
    case append = "APPEND"
    case prepend = "PREPEND"
    
}

enum SectionSourceType: String {
    case tvod = "TVOD"
    case continueWatching = "CONTINUE_WATCHING"
    case recommendationType = "RECOMMENDATION"
    case editorial = "EDITORIAL"
    case prime = "PRIME"
    case providerSpecificRail = "PROVIDER_SPECIFIC_RAIL"
    case heroBanner = "HERO_BANNER"
}

enum PrimeAppConstant: String {
    case activated = "ACTIVATED"
    case suspended = "SUSPENDED"
    case active = "ACTIVE"
    case inActive = "INACTIVE"
    case trivial = "TRIVIAL"

}

enum PrimeAppPopUpButtonText: String {
    case Ok
    case Proceed
    case Cancel
    case Skip
    case resumePrime = "Resume Prime"
    case existingUser = "I’m an existing Prime member"
}

enum ProviderType: String {
    case prime = "PRIME"
    case sunnext = "SUNNXT"//"SunNXT"
    case netflix = "NETFLIX"
    case eros = "EROSNOW"//"Eros"
    case shemaro = "SHEMAROOME"
    case hungama = "HUNGAMA"
    case zee = "ZEE5"//"zee5"
    case hopster = "HOPSTER"
    case vootSelect = "VOOTSELECT"
    case vootKids = "VOOTKIDS"
    case hotstar = "HOTSTAR"
    case sonyLiv = "SONYLIV"
    case tataSky = "TATASKY"
    case rental = "RENTAL"
    case curosityStream = "CURIOSITYSTREAM"
}

enum NavigationType: String {
    case backButton
    case centreImage
    case backButtonAndTitle
    case backButtonAndCentreImage
    case clearBackground
    case centerTitle
    case searchBar
    case backAndSearchBar
    case leftAndRightBar
    case backWithText
    case updateRightBarButton
}

enum UserDefault {
    enum UserInfo: String {
        case hasRunBefore, customerName, autoPlay, baId, targetBaId, subscriberId, baIdCount, userAccessToken, deviceAccessToken, selectedIndex, selectedID, videoQuality, isVolume, profileId, aliasName, fullname, mobileNumber, accountDetailServerResponse, profileImageRelativePath, apnsDeviceToken, pushScreenOpened, deviceUDID, isMicEnabled, isFSPopUpShown, deviceId, configTAAccessToken, loginType, vendorIdentifier, dsn, pageNameOnHome, contentPlayBack, deviceLoggedOut, isHungamaIdSet, hotStarAlertCount, deepLinkScreenOpened, email, isErosNowLoggedIn, isSonyLivLoggedIn, mobileNumberForSuperProperty, sonyLivShortToken, atvCancelled,  isOnEditProfileScreen, primeAppAlertCount
    }
    
    enum SearchArrayResult: String {
        case searchResultArray
    }
}

enum AlertType {
    case forceUpgrade, recommended
}

enum CardContentType {
    case series, brand, vod
}

enum SIDStatusType: String {
    case inActive = "Inactive"
    case active = "Active"
    case none = "Trivial"
}

enum AlertConstant: String {
    case alertTitle = "Tata Sky Binge"
    case alertMessage = "Allow Access From Settings"
    case changesLostError = "Changes will be lost if you exit without saving"
    case settings = "Settings"
    case cancel = "Cancel"
    case done = "Done"
    case ok = "OK"
    case yes = "Yes"
    case no = "No"
    case notNow = "Not Now"
    case logoutSuccessful = "Logout Successful"
    case logout = "Logout"
    case remove = "Remove"
    case alert = "Alert"
    case downloadAgain = "Download Again"
    case deactivatedAccount = "Your account is currently deactivated"
}


enum ErosNowConstants {
    static var partnerCode = "TSKY"
    static var uatApiClientCode = "QxrFiKSkc7D1Yq38trNbBp9YfdZk0xHHsM3RKP53"
    static var prodApiClientCode = "1MZeCKJYqvwEeaWsCd1FA19KZxHubbz41TCIqdbF"
    static var country = "IN"
    static var environment = UtilityFunction.shared.isUatBuild() ? EnvironmentType.staging : EnvironmentType.production
    static var email = (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId ?? "VKK0L83493183042") + "@tatskaydummy-125.com"
    static var partnerId = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId ?? ""
    static var userToken = BAKeychainManager().acccountDetail?.deviceAuthenticateToken ?? "" // "JvvQ3l6z08svA0v0d3yw1RlApKn4v5OA"
    static var testContentID = "7004461"
}

enum SonyLivConstants {
    static var partnerName = "TATASky"
    static var partnerDSN =  BAKeychainManager().acccountDetail?.deviceSerialNumber ?? ""
    static var partnerLoginToken = BAKeychainManager().acccountDetail?.deviceAuthenticateToken ?? ""
    static var shortValidityToken = BAKeychainManager().sonyLivShortToken
    static var partnerSource = "TSMOBILE"
}

extension SdkInitilizerModel {
    
    static func getSonyLivInitializers() -> SdkInitilizerModel {
        let sdkInitializerValue = SdkInitilizerModel.init(partnerName: SonyLivConstants.partnerName, partnerDSN: BAKeychainManager().acccountDetail?.deviceSerialNumber ?? "", partnerLoginToken: BAKeychainManager().acccountDetail?.deviceAuthenticateToken ?? "", partnerSource: SonyLivConstants.partnerSource, shortValidityToken: BAKeychainManager().sonyLivShortToken)
        return sdkInitializerValue
    }
}

enum ErosNowLoginKeys {
    static let isErosNowLoggedIn = "isErosNowLoggedIn"
}

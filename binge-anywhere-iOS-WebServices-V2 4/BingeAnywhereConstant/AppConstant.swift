//
//  AppConstant.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

// MARK: App Constants
let kDeviceWidth                            =   UIScreen.main.bounds.size.width
let kDeviceHeight                           =   UIScreen.main.bounds.size.height
let kDeviceSize                             =   UIScreen.main.bounds.size
let screenScaleFactorForWidth: CGFloat      =   UIScreen.main.bounds.size.width / 375.0
let screenScaleFactorForHeight: CGFloat     =   UIScreen.main.bounds.size.height / 812.0
let appWindow                               =   UIApplication.shared.keyWindow!
let kAppVersion                             =   "2.0.2"

let kAppDelegate                            =   UIApplication.shared.delegate as! AppDelegate
var kDeviceId                               =   BAKeychainManager().deviceId//UIDevice.current.identifierForVendor?.uuidString
var kDeviceModal                            =   UIDevice.current.modelName
let kUserDefaults                           =   UserDefaults.standard
let kKeychainStandard                       =   KeychainWrapper.standard
let isPhone                                 =   (UIDevice.current.userInterfaceIdiom == .phone)

let kSafeAreaHeight: CGFloat                =   (CGFloat((kAppDelegate.window?.rootViewController?.bottomLayoutGuide.length)!) + CGFloat((kAppDelegate.window?.rootViewController?.bottomLayoutGuide.length)!))

let kAppName                                = "Binge Anywhere"
let kDeviceTypeValue                        = "iOS"
let kTokenSimulator                         = "76243862846284873246893289743t5"
let smarturl_accesskey                      = "ywVXaTzycwZ8agEs3ujx"
let smarturl_parameters                     = "service_id=10&play_url=yes&protocol=hls&us="
let CONTROLS_FADE_ANIMATION_DURATION        = 0.20
let kPubNubProdPublicKey                    = "pub-c-d2e51136-74e8-4c47-8418-0c66767d4b60"
let kPubNubProdSubscriberKey                = "sub-c-99af3d1a-707e-11e7-8293-02ee2ddab7fe"
let kPubNubUatPublicKey                     = "pub-c-985cc8ef-ec98-4ddf-805b-adb2128f1222"
let kPubNubUatSubscriberKey                 = "sub-c-365e945c-64c6-11e9-8880-56db33cee5b7"
let kMixpanelProdKey                        = ""//"a3017925f92ffb3b9f09aee7cfb484c0"
let kMixpanelUATKey                         = ""//"0b3a850cd5db836fe8430a754dff4f93"

//UAT Environment
//let FacebookAppID                          = "1403948053320616"
//let FacebookDisplayName                    = "Binge UAT"
//let URLtypes                               = "fb1403948053320616"

//PROD Environment
//let FacebookAppID                          = "325375989196489"
//let FacebookDisplayName                    = "Tata Sky Binge"
//let URLtypes                               = "fb325375989196489"



// MARK: TableViewCell Identifiers
let kContentDetailCell = "BAContentDetailTableViewCell"
let kHomeContentlandScapeCell = ""
let kHomeContentPortraitCell = ""
let kHomeCircularCell = ""
let kBAHomeRailTableViewCell = "BAHomeRailTableViewCell"
let kBAHeroBannerTableViewCell = "BAHeroBannerTableViewCell"
let kBASeriesDetailTableViewCell = "BASeriesDetailTableViewCell"
let kTitleHeaderTableViewCell = "TitleHeaderTableViewCell"
let kRegistrationVerificationTextFieldTableViewCell = "RegistrationVerificationTextFieldTableViewCell"
let kNextButtonTableViewCell = "NextButtonTableViewCell"
let kResendOtpTableViewCell = "ResendOtpTableViewCell"
let kBottomBingeLogoTableViewCell = "BottomBingeLogoTableViewCell"
let kPrimSubscriptionTableViewCell = "PrimSubscriptionTableViewCell"
let kSignUpAlreadyUserTableViewCell = "SignUpAlreadyUserTableViewCell"
let kSignUpTextFieldTableViewCell = "SignUpTextFieldTableViewCell"
let kSignUpLoginTableViewCell = "SignUpLoginTableViewCell"
let kLoginSegmentTableViewCell = "LoginSegmentTableViewCell"
let kLoginTextFieldTableViewCell = "LoginTextFieldTableViewCell"
let kNewUserTableViewCell = "NewUserTableViewCell"
let kBAEpisodeTableViewCell = "BAEpisodeTableViewCell"
let kBASeriesFooterViewTableViewCell = "BASeriesFooterViewTableViewCell"
let kBATransactionsTableViewCell = "BATransactionsTableViewCell"
let kBATransactionHistoryTableViewCell = "BATransactionHistoryTableViewCell"
let kPageIndicatorTableViewCell = "PageIndicatorTableViewCell"
let kBAAppPosterTableViewCell = "BAAppPosterTableViewCell"
let kBAAppDetailTableViewCell = "BAAppDetailTableViewCell"
let kRMNTableViewCell = "RMNTableViewCell"
let kDescriptionTableViewCell = "DescriptionTableViewCell"
let kForgetPasswordTableViewCell = "ForgetPasswordTableViewCell"
let kBAPrimaryDeviceTableViewCell = "BAPrimaryDeviceTableViewCell"
let kBASecondryDeviceTableViewCell = "BASecondryDeviceTableViewCell"
let kEditProfileImageTableViewCell = "EditProfileImageTableViewCell"
let kEditProfileInputTableViewCell = "EditProfileInputTableViewCell"
let kEditProfileNumberTableViewCell = "EditProfileNumberTableViewCell"
let kEditProfilePasswordTableViewCell = "EditProfilePasswordTableViewCell"
let kAccountsTableViewCell = "AccountsTableViewCell"
let kAccountsHeaderTableViewCell = "AccountsHeaderTableViewCell"
let kCancelTableViewCell = "CancelTableViewCell"
let kBASwitchAccountTableViewCell = "BASwitchAccountTableViewCell"
let kBAPlayerSettingsTableViewCell = "BAPlayerSettingsTableViewCell"
let kBAOrSeparatorTableViewCell = "BAOrSeparatorTableViewCell"
let kIdSelectionTableViewCell = "IdSelectionTableViewCell"
let kShowDataTableViewCell = "ShowDataTableViewCell"
let kMySubcriptionTableViewCell = "MySubcriptionTableViewCell"
let kInstallOptionsTableViewCell = "InstallOptionsTableViewCell"
let kHeadingTableViewCell = "HeadingTableViewCell"
let kInfoTableViewCell = "InfoTableViewCell"
let kMoveToNextTableViewCell = "MoveToNextTableViewCell"
let kPersonalInfoTableViewCell = "PersonalInfoTableViewCell"
let kOptionalInfoTableViewCell = "OptionalInfoTableViewCell"
let kTimeIntervalTableViewCell = "TimeIntervalTableViewCell"
let kDateFieldTableViewCell = "DateFieldTableViewCell"
let kTimeScheduleTableViewCell = "TimeScheduleTableViewCell"
let kAdvertisementTableViewCell = "AdvertisementTableViewCell"

// MARK: Screen Recording Notification
let NOTIF_ScreenRecordingEnabled = "ScreenRecordingEnabled"
let NOTIF_PlaybackRestricted = "ScreenMirroringEnabled"

// MARK: CollectionViewCell Identifiers
let kBAHomeRailCollectionViewCell = "BAHomeRailCollectionViewCell"
let kLoadMoreCollectionViewCell  = "LoadMoreCollectionViewCell"
let kBASeasonCollectionViewCell = "BASeasonCollectionViewCell"
let kBACircularCollectionViewCell = "BACircularCollectionViewCell"
let kBAAppsHeaderCollectionViewCell = "BAAppsHeaderCollectionViewCell"
let kBAAppsHeaderReusableView = "BAAppsHeaderReusableView"
let kBAAppsCollectionViewCell = "BAAppsCollectionViewCell"
let kBAHomeTabMenuHeaderCollectionViewCell = "HomeTabMenuCollectionViewCell"
let kAdvertisementCollectionViewCell = "AdvertisementCollectionViewCell"


// MARK: Cell Size Constants
let railSize: CGFloat = 234
let heroSize: CGFloat = 213
let potraitSize: CGFloat = 330
let landscapeSize: CGFloat = 250
let circularSize: CGFloat = 125
let categorySize: CGFloat = 120

let marketingHeightMultiplier:CGFloat = 1.075

// MARK: Application Alert Text
let klogoutConfirmation = "Are you sure that you want to sign out?"
let kDeviceRemoval = "Are you sure you want to logout of \(BAKeychainManager().aliasName)?"
let kGuestUser = "Guest User"
let kAlert = "Alert!"
let kSomethingWentWrong = "Something Went Wrong"
let kErrorHeader = "Error Message"
let kDeviceRemovedHeader = "Device Removed"
let kApiFailure = "The data couldn't be read because it isn't in the correct format"
let kSessionExpire = "You have been logged out of this device.Please log in again."//"Your device has been removed. You will be logged out"
let kInactiveDTH = "No active account found for this Subscriber ID."
let kIncorrectOTP = "The OTP entered is incorrect. Please try again."
let kIncorrectPassword = "Please visit mytatasky.com to create a new password."
//let kIncorrectPassword = "The Password entered is incorrect. Please try again."

// MARK: Application DTH/Binge Inactive Messages
let kATVBingeInactive = "Please activate your Tata Sky Binge subscription to continue"//"Your Tata Sky Binge+ is not active anymore. Please visit www.watch.tatasky.com to reactivate your Tata Sky Binge+."
let kFSInactive = "Your Tata Sky Binge is not active anymore. Please visit www.tatasky.com to reactivate your Tata Sky Binge."
let kDTHInactive = "Your Tata Sky account is Inactive, Please recharge to continue."//"To enjoy your premium apps, you need to recharge to activate your DTH"
let kDTHInactiveLowBalanceHeader = "Subscription Inactive"
let kDTHInactiveLowBalance = "Your Tata Sky Binge Subscription is inactive due to insufficient balance. Please log in after activation."
let kDTHPackDropped = "Your Tata Sky Binge subscription on Binge+ box is inactive. To reactivate &amp; get access on mobile, call Customer Care on 1800 208 6633."
let kDTHPackDroppedHeader = "Binge Dropped"
let kDTHInactiveHeader = "DTH Inactive"
let kFSInactiveHeader = "Binge Inactive"
let kFSPackDroppedMessage = "This app is currently available only for Tata Sky Binge subscribers. Please log in after you have subscribed."
let kATVInactiveHeader = "Binge+ Inactive"

// MARK: Account Status
let kActive = "ACTIVE"
let kDeActive = "DEACTIVATED"
let kTempSuspended = "TEMP_SUSPENSION"
let kTempSuspension = "TEMP SUSPENSION"
let kPending = "PENDING"
let kInactiveState = "DEACTIVE"
let kWrittenOff = "WRITTEN_OFF"
let kCancelled = "CANCELLED"
let kBlackListed = "BLACKLISTED"
let kPartialDunned = "PARTIAL_DUNNED"
let kFreeSubscription = "FREE"
let kPaidSubscription = "PAID"
// MARK: More Section URL
//let kTermsAndPolicy =

// MARK: Login UI Constants
let kRegisteredRMNHeader = "Enter Your Registered Mobile Number"
let kSubscriberIdHeader = "Enter Your Tata Sky Subscriber ID"
let kEnterLogin6DigitOTP = "Enter the 6-digit OTP"
let kResendOTP = "Resend OTP"
let kPasswordHeader = "Enter your Tata Sky Password"

// MARK: Forgot Passowrd UI Screen
let kForgotPasswordHeader = "Create New Password"
let kFprgotPasswordOTP = "Enter the 4-digit OTP"

// MARK: Login Error Constants
let kValidPhoneNumber = "Please enter a valid mobile number."
let kValidSID = "Please enter a valid Subscriber ID."
let kOtpDigit = "Please enter a 4-digit OTP."
let kLoginOTPDigit = "Please enter a 6-digit OTP."
let kVerifyPasswordMismatch = "The retyped password does not match."
let kRetypedPassword = "The retyped password does not match. "
 
// MARK: Subscription Message Constants
let kPlanLinkedFTV = "This plan is linked to your Amazon Fire TV Stick - Tata Sky Edition."
let kPlanLinkedATV = "This plan is linked to your Tata Sky Binge+."
let kSubscriptionTitle = "My Subscription"

// MARK: SID/BAID Screen Constant
let kBingeActiveSID = "Tata Sky Binge Active "
let kBingeInactiveSID = "Tata Sky Binge Inactive "
let ksubscriberListHeader = "Your Tata Sky Subscriptions"
let kBingeSubscriberListHeader = "Your Tata Sky Binge subscriptions"
let kBingeSubsciberSubHeader = "Please select one and proceed."

// MARK: Device Management Constants
let kLargeDeviceHeader = "TV device"
let kSmallDeviceHeader = "Mobile devices"
let kLargeDeviceFooter = "You cannot log out of the TV device."
let kDeviceBody = "You can log into only 1 TV device and a maximum of 3 mobile devices at a time."

// MARK: No Internet Constants
let singleQuaote = #"'"#
let doubleQuoteNumber = "\"\(BAKeychainManager().mobileNumber.replaceFrom(startIndex: 0, endIndex: 4, With: "XXXXX"))\""
let kNoInternetBody = "Make sure that your Wifi or mobile data is turned on and try again."

let kHotStarUnInstalledHeader = "Install Disney+ Hotstar"
let kHotStarInstalledHeader = "You will be redirected to Disney+ Hotstar"
let kNewHotStarInstalledMessageString = "Please ensure you are \nlogged in with your Tata Sky \nRegistered Mobile Number \n\(doubleQuoteNumber)"
let kNewHotStarUnInstalledMessageString = "Please ensure that \nyou log in with your Tata Sky \nRegistered Mobile Number \(doubleQuoteNumber)"
let kThirdPartySDKLoginFailed = "Error. Content not found!" //Veribage provided by Abinav on hangout chat with Deepika, Sudhanshu and Harsh on 17April

// MARK: User Account/DTH Status Inactive
let kTempSuspendedMessage = "Your Tata Sky account is temporarily suspended. Please resume services from Tata Sky Selfcare."
let kTempSuspendedHeader = "Account Status"
let kPartiallyDunnedMessage = "Your Tata Sky account is partially dunned. Please recharge to continue."

// MARK: Account Status
let kDTHInactiveWithDBRAndMBRInactiveMessage = "Please recharge to access Tata Sky Binge."
let kDTHInactiveWithDBRAndMBRInactiveHeader = "Account Inactive"
let kBingeSubscriptionMBRDeactiveMessage = "Please ensure you have sufficient balance in your account to reactivate Tata Sky Binge."//"Your Tata Sky Binge Subscription is inactive due to insufficient balance"
let kDTHInactiveWithDBRAndMBRTempSuspendedHeader = "Account Suspended"
let kDTHInactiveWithDBRAndMBRTempSuspendedMessage = "Please call Customer Care on 1800 208 6633 and resume services to access Tata Sky Binge."
let kDTHInactiveMBRPaidActiveMessage = "Your Tata Sky account is inactive. Please recharge to ensure you can access Tata Sky Binge after \(BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.expirationDate ?? "")"
let kDTHDunnedMBRPaidActiveMessage = "Please recharge to ensure you can access Tata Sky Binge after \(BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.expirationDate ?? "")"
let kDTHTempSuspendedMBRPaidActiveMessage = "Your Tata Sky account is temporarily suspended. Please call Customer Care on 1800 208 6633 and resume services to access Tata Sky Binge after \(BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.expirationDate ?? "")"

let kBingePlus149Pack = "You have been upgraded to Tata Binge Premium with 1 month free trial linked to your Tata Sky Binge+."
let kBingePlus299Pack = "Your Tata Binge Premium subscription is now linked to your Tata Sky Binge+."


let kPartialDunnedState = "Partially Dunned"

let kScreenRecordingDetected = "Please switch off casting to watch content in the app"
let kScreenshotDetected = "Taking screenshot isn't allowed by the app or your organisation"
let kSubscriptionCancelled = "Subscription Cancelled"
let kSubscriptionDropped = "You are not subscribed to Tata Sky Binge anymore."

let kRentalExpired = "This content has expired, and is no longer available on this device."

// MARK: FireTV Stick
let kFSPopupHeader = "Get Amazon Fire TV Stick – Tata Sky Edition"
let kFSPopupBody = "You are eligible for a Amazon Fire TV Stick at no extra cost"

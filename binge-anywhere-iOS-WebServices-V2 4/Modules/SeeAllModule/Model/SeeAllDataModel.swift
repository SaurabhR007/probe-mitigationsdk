//
//  SeeAllDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

struct SeeAllDataModel: Codable {
    let code: Int?
    let message: String?
    var data: SeeAllData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SeeAllData.self, forKey: .data)
    }

    init() {
        code = 0
        message = ""
        data = nil
    }
}

struct SeeAllData: Codable {
    let id: Int?
    let title: String?
    let sectionType: String?
    let layoutType: String?
    var contentList: [BAContentListModel]?
    let totalCount: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
    }

    init() {
        id = 0
        title = ""
        sectionType = ""
        layoutType = ""
        contentList = nil
        totalCount = 0
    }
}

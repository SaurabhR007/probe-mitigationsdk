//
//  BAAppsHeaderReusableView.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAAppsHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var sectionHeaderTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureHeader(_ source: TableViewHeaderSource) {
        headerContentView.backgroundColor = .BAdarkBlueBackground
        sectionHeaderTitle.text = source.headerTitle
    }
}

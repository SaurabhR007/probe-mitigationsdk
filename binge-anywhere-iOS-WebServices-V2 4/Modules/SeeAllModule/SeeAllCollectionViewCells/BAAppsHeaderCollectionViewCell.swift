//
//  BAAppsHeaderCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAAppsHeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var sectionHeaderTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configureHeader(_ source: TableViewHeaderSource) {
        headerContentView.backgroundColor = .BAdarkBlueBackground
        sectionHeaderTitle.text = source.headerTitle
    }
}

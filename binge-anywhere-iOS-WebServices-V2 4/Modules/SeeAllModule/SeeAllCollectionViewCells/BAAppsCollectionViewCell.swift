//
//  BAAppsCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class BAAppsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var isSubscibedImageView: UIImageView!
    @IBOutlet weak var appIconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func configureApps(_ item: BAContentListModel?, isSubscribed: Bool?) {
        var url = URL.init(string: item?.image ?? "")
        switch item?.provider?.uppercased() {
        case ProviderType.prime.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoCircular ?? "")
        case ProviderType.sunnext.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoCircular ?? "")
        case ProviderType.shemaro.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoCircular ?? "")
        case ProviderType.vootSelect.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoCircular ?? "")
        case ProviderType.vootKids.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoCircular ?? "")
        case ProviderType.hungama.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoCircular ?? "")
        case ProviderType.zee.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoCircular ?? "")
        case ProviderType.sonyLiv.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoCircular ?? "")
        case ProviderType.hotstar.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoCircular ?? "")
        case ProviderType.eros.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoCircular ?? "")
        case ProviderType.curosityStream.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoCircular ?? "")
		case ProviderType.netflix.rawValue: appIconImageView.image = UIImage(named: "netflix")
		case ProviderType.hopster.rawValue: appIconImageView.image = UIImage(named: "hopster")
        default: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoCircular ?? "")
        }
		if let _url = url {
			appIconImageView.alpha = 0
			appIconImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
				UIView.animate(withDuration: 1, animations: {
					self.appIconImageView.alpha = 1
					if isSubscribed ?? false {
						self.isSubscibedImageView.isHidden = false
					} else {
						self.isSubscibedImageView.isHidden = true
					}
				})
			})
		}
    }
}

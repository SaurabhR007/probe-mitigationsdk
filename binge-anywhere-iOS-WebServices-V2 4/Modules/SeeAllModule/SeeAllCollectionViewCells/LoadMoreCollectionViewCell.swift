//
//  LoadMoreCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 30/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
protocol LoadMoreDelegate: class {
    func loadMoreTapped()
}

class LoadMoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var loadMoreImage: UIImageView!
    @IBOutlet weak var loadMoreLabel: CustomLabel!
    
    weak var delegate: LoadMoreDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    private func configureUI() {
        loadMoreLabel.textColor = .BABlueColor
        loadMoreLabel.font = .skyTextFont(.medium, size: 14)
        setTempelateImage()
    }
    
    private func setTempelateImage() {
        if let myImage = UIImage(named: "LoadMore") {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            loadMoreImage.image = tintableImage
            loadMoreImage.tintColor = .BABlueColor
        }
    }

    @IBAction func loadMoreTapped() {
        delegate?.loadMoreTapped()
    }

}

//
//  BASeeAllRailCollectionCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class BASeeAllRailCollectionCell: UICollectionViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: CustomLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUI()
    }

    // MARK: - Functions

    func configureUI() {
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BARailTitleColor)
        self.contentView.backgroundColor = .BAdarkBlue2Color
        self.cornerRadius = 3
    }

    func configureCell(_ item: BAContentListModel?) {
        if let item = item {
            titleLabel.text = item.title
            let url = URL.init(string: item.image ?? "")
			titleImageView.alpha = 0
			titleImageView.kf.setImage(with: ImageResource(downloadURL: url!), completionHandler: { _ in
				UIView.animate(withDuration: 1, animations: {
					self.titleImageView.alpha = 1
				})
			})
        }
    }

}

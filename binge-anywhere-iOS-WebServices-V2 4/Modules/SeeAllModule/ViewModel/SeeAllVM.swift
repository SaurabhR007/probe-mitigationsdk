//
//  SeeAllVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol SeeAllDataProtocol {
    var dataModel: SeeAllDataModel? {get set}
    var contentId: Int {get set}
    var nextPageAvailable: Bool {get set}
    func getContentId(_ id: Int)
    func getSeeAllData(completion: @escaping(Bool, String?, Bool) -> Void)
}

class SeeAllVM: SeeAllDataProtocol {
    var contentId: Int = 0
    var dataModel: SeeAllDataModel?
    var repo: SeeAllDataRepo
    var nextPageAvailable = false
    var pageLimit = 10
    var pageLimitFirstTime = 20
    var totalDataCount = 0
    var pageOffset = 0
    var requestToken: ServiceCancellable?
    var endPoint: String = ""
    var apiParams = APIParams()

    init(repo: SeeAllDataRepo, endPoint: String, contentId: Int) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.contentId = contentId
    }

    func getSeeAllData(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["id"] = String(contentId)
        apiParams["offset"] = String(pageOffset)
        if pageOffset == 0 {
            apiParams["limit"] = String(pageLimitFirstTime)
        } else {
            apiParams["limit"] = String(pageLimit)
        }
        
        if requestToken != nil {
            return
        }
        requestToken = repo.getSeeAllScreenData(apiParams: apiParams, completion: { (configData, error) in
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if self.pageOffset == 0 {
                            self.dataModel = _configData.parsed
                            self.totalDataCount = _configData.parsed.data?.contentList?.count ?? 0
                        } else {
                            self.dataModel?.data?.contentList?.append(contentsOf: (_configData.parsed.data?.contentList)!)
                            self.totalDataCount += _configData.parsed.data?.contentList?.count ?? 0
                        }
                        if let screenData = self.dataModel?.data {
//                            if let searchData = screenData.contentList {
                            if let _ = screenData.contentList {
//                                self.pageOffset = searchData.count
                                self.pageOffset = self.totalDataCount
                                self.nextPageAvailable = self.totalDataCount < (screenData.totalCount ?? 0)
                            }
                        }
                        if let data = self.dataModel?.data?.contentList {
                            self.dataModel?.data?.contentList = UtilityFunction.shared.filterTAData(data)
                        }
                        completion(true, nil, false)
                        self.requestToken = nil
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                                self.requestToken = nil
                            } else {
                                completion(false, "Some Error Occurred", true)
                                self.requestToken = nil
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                                self.requestToken = nil
                            } else {
                                completion(false, "Some Error Occurred", false)
                                self.requestToken = nil
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                    self.requestToken = nil
                } else {
                    completion(false, "Some Error Occurred", true)
                    self.requestToken = nil
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
                self.requestToken = nil
            } else {
                completion(false, "Some Error Occurred", true)
                self.requestToken = nil
            }
        })
    }

    func getContentId(_ id: Int) {
        self.contentId = id
    }
}

//
//  SeeAllDataRepo.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol SeeAllDataRepo {
    var apiEndPoint: String {get set}
    func getSeeAllScreenData(apiParams: APIParams, completion: @escaping(APIServicResult<SeeAllDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct SeeAllRepo: SeeAllDataRepo {
    var apiEndPoint: String = ""

    func getSeeAllScreenData(apiParams: APIParams, completion: @escaping(APIServicResult<SeeAllDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .seeAllScreenData(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<SeeAllDataModel>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

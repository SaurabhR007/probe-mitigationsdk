//
//  SeeAllVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

//protocol SeeAllVCDelegate: class {
//    func moveToDetailScreenFromSeeAll(vodId: Int?, contentType: String?)
//}

class SeeAllVC: BaseViewController {

    @IBOutlet weak var seeAllCollectionView: UICollectionView!
    var seeAllViewModel: SeeAllDataProtocol?

    // CascadableScroll - Protocol AssociatedType explicite value
    //    typealias GridViewType = UICollectionView
    //    typealias ViewCellType = UICollectionViewCell
    //    var cascadeQueue: OperationQueue = OperationQueue()
    var stopCollectionViewAnimationOnScroll: Bool = true
    var needAnimationFlag: Bool = true
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var taContentlListData: HomeScreenDataItems?
    var isSelectionMade = false
    var contentId: Int = 0
    var isTAData = false
    var isPrime = false
    var isCallAlreadyMade = false
    var layoutType: String?
    var isTVODData = false
    var pageSource = ""
    var configSource = ""
    var railName = ""
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var isAvailabilityChecked:Bool = false
    
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "See-All")
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = ""
        seeAllCollectionView.backgroundColor = .BAdarkBlueBackground
        seeAllCollectionView.delegate = self
        seeAllCollectionView.dataSource = self
        setNavigationBar()
        recheckVM()
        registerCollectionCells()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        if (isPrime || isTAData || isTVODData) {
            return
        }
        forcePaginate()
    }

    private func recheckVM() {
        if seeAllViewModel == nil {
            seeAllViewModel = SeeAllVM(repo: SeeAllRepo(), endPoint: "rail", contentId: contentId)
            if isTAData {
                seeAllViewModel?.dataModel = SeeAllDataModel()
                seeAllViewModel?.dataModel?.data = SeeAllData()
                seeAllViewModel?.dataModel?.data?.contentList =  taContentlListData?.contentList
                self.title = taContentlListData?.title ?? ""
                self.seeAllCollectionView.reloadData()
            } else if isTVODData || isPrime {
                seeAllViewModel?.dataModel = SeeAllDataModel()
                seeAllViewModel?.dataModel?.data = SeeAllData()
                seeAllViewModel?.dataModel?.data?.contentList =  taContentlListData?.contentList
                seeAllViewModel?.nextPageAvailable = false
                self.title = taContentlListData?.title ?? ""
                self.seeAllCollectionView.reloadData()
            } else {
                getSeeAllData()
            }
        }
    }

    private func registerCollectionCells() {
        seeAllCollectionView.register(UINib(nibName: kBAHomeRailCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeRailCollectionViewCell)
        seeAllCollectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
    }

    // MARK: - Functions
    private func setNavigationBar() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

    func getSeeAllData() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            showActivityIndicator(isUserInteractionEnabled: true)
            if seeAllViewModel != nil {
                seeAllViewModel?.getSeeAllData(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.title = self.seeAllViewModel?.dataModel?.data?.title ?? ""
                        self.seeAllCollectionView.reloadData {
                            self.forcePaginate()
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
			noInternet()
//                {
//				self.getSeeAllData()
//			}
        }
    }
    
    func forcePaginate() {
        if UtilityFunction.shared.forcePaginate(self.seeAllCollectionView, seeAllViewModel?.nextPageAvailable) {
            getSeeAllData()
        }
    }
    override func retryButtonAction() {
        getSeeAllData()
    }

    func moveToDetailScreenFromSeeAll(vodId: Int?, contentType: String?, timeStamp: Int?) {
        fetchContentDetail(vodId: vodId, contentType: contentType, timeStamp: timeStamp)
    }

    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?, timeStamp: Int?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId, timeStamp: timeStamp)
    }

    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?, timeStamp: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    //self.hideActivityIndicator()
                    if flagValue {
                        self.viewContentDetailEvent(contentData: dataModel.contentDetail, pageSource: "See-All", configSource: "Editorial", railName: self.railName)
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId, timeStamp: timeStamp)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?, timeStamp: Int?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId, timeStamp: timeStamp ?? 0)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int, timeStamp: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail, expiry: timeStamp)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?, expiry: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
           // showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
                //hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.layoutType = self.layoutType
                        contentDetailVC.railType = self.isTAData ? TAConstant.recommendationType.rawValue : TAConstant.editorial.rawValue
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "See-All"
                        contentDetailVC.configSource = "Editorial"
                        contentDetailVC.railName = self.railName
                        contentDetailVC.contentDetail = contentDetail
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.layoutType = self.layoutType
                        contentDetailVC.railType = self.isTAData ? TAConstant.recommendationType.rawValue : TAConstant.editorial.rawValue
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "See-All"
                        contentDetailVC.configSource = "Editorial"
                        contentDetailVC.railName = self.railName
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.layoutType = self.layoutType
                        contentDetailVC.railType = self.isTAData ? TAConstant.recommendationType.rawValue : TAConstant.editorial.rawValue
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "See-All"
                        contentDetailVC.configSource = "Editorial"
                        contentDetailVC.railName = self.railName
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func viewContentDetailEvent(contentData: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        var title = ""
        if contentData?.meta?.contentType == ContentType.brand.rawValue {
            title = contentData?.meta?.brandTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.series.rawValue {
            title = contentData?.meta?.seriesTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.tvShows.rawValue {
            title = contentData?.meta?.vodTitle ?? ""
        } else {
            title = contentData?.meta?.vodTitle ?? ""
        }
        let genreResult = contentData?.meta?.genre?.joined(separator: ",")
        let languageResult = contentData?.meta?.audio?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
}

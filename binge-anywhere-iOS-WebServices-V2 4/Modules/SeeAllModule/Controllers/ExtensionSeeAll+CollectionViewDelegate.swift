//
//  ExtensionSeeAll+CollectionViewDelegate.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension SeeAllVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout/*, CascadableScroll*/, LoadMoreDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //collectionView.collectionViewLayout.invalidateLayout()
        guard let data = seeAllViewModel?.dataModel else {
            return 0
        }
        guard let count = data.data?.contentList?.count else {
            return 0
        }
        return count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let data = seeAllViewModel?.dataModel else {
            return CGSize.zero
        }
        if seeAllViewModel?.nextPageAvailable ?? false && indexPath.row == data.data?.contentList?.count {
            return CGSize(width: collectionView.frame.size.width, height: 40)
        } else {
            let requiredNumOfColumns: CGFloat = isPhone ? 2:(data.data?.layoutType == LayoutType.potrait.rawValue) ? 5 : 4
            let deviceWidth: CGFloat = UIScreen.main.bounds.width
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

            let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
            let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
            let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing

            let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns

            let padding: CGFloat =  42
            let collectionViewSize = collectionView.frame.size.width - padding
            switch data.data?.layoutType {
            case LayoutType.potrait.rawValue:
                return CGSize(width: cellWidth, height: cellWidth*(16/9))
            case LayoutType.landscape.rawValue:
                return CGSize(width: cellWidth, height: cellWidth)
            default:
                return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
            }
        }
        // let dimension = self.seeAllCollectionView.bounds.width / 2
        //return CGSize(width: dimension, height: dimension)
        //        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        guard let data = seeAllViewModel?.dataModel else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
            return cell
        }

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
            cell.configureCell(data.data?.contentList?[indexPath.row] ?? nil, data.data?.layoutType ?? "")
            return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = seeAllViewModel?.dataModel  else {
            return
        }
        seeAllCollectionView.isUserInteractionEnabled = false
        if isPrime || data.data?.contentList?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
            startPrimeNavigationJourney(data.data?.contentList?[indexPath.row] ?? BAContentListModel())
        } else {
            moveToDetailScreenFromSeeAll(vodId: data.data?.contentList?[indexPath.row].id, contentType: data.data?.contentList?[indexPath.row].contentType, timeStamp: data.data?.contentList?[indexPath.row].rentalExpiry)
        }
        //CustomLoader.shared.showLoader(false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.seeAllCollectionView.isUserInteractionEnabled = true
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    

    func loadMoreTapped() {
        if self.seeAllViewModel?.nextPageAvailable ?? false {
           // if isCallAlreadyMade == false {
               // isCallAlreadyMade = true
                self.getSeeAllData()
           // }
        }
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sixtyP = CGFloat(80) * scrollView.contentSize.height / CGFloat(100)
        if scrollView.bounds.maxY > sixtyP {
            if seeAllViewModel?.nextPageAvailable ?? false {
                getSeeAllData()
                seeAllViewModel?.nextPageAvailable = false
            }
        }
    }
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }

}

//
//  BAAppsSeeAllViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Foundation
import ARSLineProgress

class BAAppsSeeAllViewController: BaseViewController {

    var seeAllViewModel: SeeAllDataProtocol?
    /// CascadableScroll - Protocol AssociatedType explicite value
//    typealias GridViewType = UICollectionView
//    typealias ViewCellType = UICollectionViewCell
//    var cascadeQueue: OperationQueue = OperationQueue()
    var stopCollectionViewAnimationOnScroll: Bool = true
    var needAnimationFlag: Bool = true

    @IBOutlet weak var appsSeeAllCollectionView: UICollectionView!

    var contentId: Int = 0
    var pageTypeVm: PageTypeVM?
    var partnerSubscription: PartnerSubscriptionDetails?
    var subscribedPartners = [String]()
    var availableProviderNames = [String]()
    var availableProvider = [BAContentListModel]()
    var unSubscribedProviderNames = [String]()
    var unSubscribedProvider = [BAContentListModel]()
    var isSelectionMade = false
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver()
        appsSeeAllCollectionView.backgroundColor = .BAdarkBlueBackground
        configureNavigation()
        conformDelegates()
        registerCells()
        getSubscribedProviders()
        recheckVM()
        appsSeeAllCollectionView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .notificationPackSubscription, object: nil)
    }
    
    func configureNavigation() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Apps")
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
//        self.navigationController?.popToRootViewController(animated: true)
    }

    private func recheckVM() {
        if seeAllViewModel == nil {
            seeAllViewModel = SeeAllVM(repo: SeeAllRepo(), endPoint: "rail", contentId: contentId)
            getSeeAllData()
        }
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateSubscribedApps), name: .notificationPackSubscription, object: nil)
    }
    
    
    func getSubscribedProviders() {
		partnerSubscription = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
        if partnerSubscription?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
            self.subscribedPartners = []
        } else if self.partnerSubscription?.providers?.count ?? 0 > 0 {
			if let provider = partnerSubscription?.providers {
				for partner in provider {
					let partnerName = (partner.name ?? "").lowercased()
					self.subscribedPartners.append(partnerName)
				}
			}
        } else {
            self.subscribedPartners = []
        }
    }

    func getSeeAllData() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            showActivityIndicator(isUserInteractionEnabled: true)
            if seeAllViewModel != nil {
                seeAllViewModel?.getSeeAllData(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
//                        self.availableProvider?.removeAll()
//                        self.unSubscribedProvider?.removeAll()
                        for partner in self.seeAllViewModel?.dataModel?.data?.contentList ?? [] {
                            let str = (partner.title ?? "").lowercased()
                            var subscribedName = ""
                            if self.subscribedPartners.contains("sun_nxt") {
                                subscribedName = "sunnxt"
                            }
                            if self.subscribedPartners.contains(str) || subscribedName == str {
                                self.availableProviderNames.append(partner.title ?? "")
                                self.availableProvider.append(partner)
                            } else {
                                self.unSubscribedProviderNames.append(partner.title ?? "")
                                self.unSubscribedProvider.append(partner)
                            }
                        }
                        self.appsSeeAllCollectionView.isHidden = false
                        self.appsSeeAllCollectionView.reloadData()
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
			noInternet()
//                {
//				self.getSeeAllData()
//			}
        }
    }

    override func retryButtonAction() {
        getSeeAllData()
    }

    func conformDelegates() {
        appsSeeAllCollectionView.delegate = self
        appsSeeAllCollectionView.dataSource = self
    }

    func registerCells() {
        appsSeeAllCollectionView.register(UINib(nibName: kBAAppsCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAAppsCollectionViewCell)
        appsSeeAllCollectionView.register(UINib(nibName: kBAAppsHeaderReusableView, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kBAAppsHeaderReusableView)

    }

    // MARK: - Page Type Navigation
    func fetchPageType(pageType: String?, appName: String?, isSubscribed: Bool?) {
        let endpoint = "page/" + (pageType ?? "")
        pageTypeVm = PageTypeVM(repo: HomeScreenRepo(), endPoint: endpoint, pageType: (pageType ?? ""), isSubscribed: isSubscribed ?? false, appPageName: appName ?? "")
        getDetailScreenData(pageTypeForApp: pageType, appName: appName, isSubscribed: isSubscribed)
    }

    func getDetailScreenData(pageTypeForApp: String?, appName: String?, isSubscribed: Bool?) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = pageTypeVm {
                dataModel.getHomeScreenData {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        let appDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.appDetail.rawValue, type: BAAppsDetailViewController.self)
                        appDetailVC.appContentData = self.pageTypeVm?.homeScreenData
                        appDetailVC.appName = appName
                        appDetailVC.isSubscribed = isSubscribed
                        appDetailVC.parentNavigation = self.navigationController
                        self.navigationController?.pushViewController(appDetailVC, animated: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
			noInternet()
//                {
//				self.getDetailScreenData(pageTypeForApp: pageTypeForApp, appName: appName, isSubscribed: isSubscribed)
//			}
        }
    }

    func moveToDetailForPageType(pageType: String?, appname: String?, isSubscribed: Bool?) {
        fetchPageType(pageType: pageType, appName: appname, isSubscribed: isSubscribed)
    }

    @objc func updateSubscribedApps(_ notification: NSNotification) {
        UtilityFunction.shared.performTaskInMainQueue {
            self.getSubscribedProviders()
            self.appsSeeAllCollectionView.reloadData()
        }
    }

}

extension BAAppsSeeAllViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout/*, CascadableScroll*/ {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if unSubscribedProvider.count > 0 {
            return 2
        } else {
            return 1
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if availableProvider.count > 0 && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus != "DEACTIVE" && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus != kWrittenOff {
            if section == 0 {
				return availableProvider.count
            } else {
                return unSubscribedProvider.count
            }
        } else {
            if section == 0 && (partnerSubscription?.providers?.count ?? 0 < 1 || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == "DEACTIVE" || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff) {
				return 1
			} else {
				return seeAllViewModel?.dataModel?.data?.contentList?.count ?? 0
			}
        }
    }

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        if availableProvider.count > 0 && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus != "DEACTIVE" && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus != kWrittenOff {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAAppsCollectionViewCell, for: indexPath) as! BAAppsCollectionViewCell
            if indexPath.section == 0 {
				cell.configureApps(availableProvider[indexPath.row], isSubscribed: true)
			} else {
				cell.configureApps(unSubscribedProvider[indexPath.row], isSubscribed: false)
			}
			return cell
		} else {
            if indexPath.section == 0 && (partnerSubscription?.providers?.count ?? 0 < 1 || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff) {
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAAppsCollectionViewCell, for: indexPath) as! BAAppsCollectionViewCell
				cell.appIconImageView.isHidden = true
				cell.isSubscibedImageView.isHidden = true
				let messageLabel = UILabel(frame: cell.bounds)
				messageLabel.text = "You are not subscribed to Tata Sky Binge."//"You have no subscription, currently select one to enjoy your favorite Movies and TV Shows"
				messageLabel.textColor = .BAwhite
				messageLabel.numberOfLines = 0
				messageLabel.textAlignment = .center
				messageLabel.font = .skyTextFont(.medium, size: 16)
				messageLabel.sizeToFit()
				messageLabel.center = CGPoint(x: appsSeeAllCollectionView.bounds.maxX/2, y: 20)
				cell.contentView.addSubview(messageLabel)
				return cell
			} else {
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAAppsCollectionViewCell, for: indexPath) as! BAAppsCollectionViewCell
				cell.configureApps(seeAllViewModel?.dataModel?.data?.contentList?[indexPath.row], isSubscribed: false)
				return cell
			}
		}
	}

//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        cascadeCollectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
//    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = seeAllViewModel?.dataModel  else {
            return
        }
        self.appsSeeAllCollectionView.isUserInteractionEnabled = false
        showActivityIndicator(isUserInteractionEnabled: false)
//        if isSelectionMade {
//            return
//        } else {
//            isSelectionMade = true
            if availableProvider.count > 0 && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status != "DEACTIVE" {
                if indexPath.section == 0 {
                    moveToDetailForPageType(pageType: availableProvider[indexPath.row].pageType, appname: availableProvider[indexPath.row].title, isSubscribed: true)
                } else {
                    moveToDetailForPageType(pageType: unSubscribedProvider[indexPath.row].pageType, appname: unSubscribedProvider[indexPath.row].title, isSubscribed: false)
                }
            } else {
                if indexPath.section == 1 {
                    moveToDetailForPageType(pageType: data.data?.contentList?[indexPath.row].pageType, appname: data.data?.contentList?[indexPath.row].title, isSubscribed: false)
                }
            }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
         self.appsSeeAllCollectionView.isUserInteractionEnabled = true
        }
       // }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

		if (availableProvider.count == 0 || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE") && indexPath.section == 0 {
			return CGSize(width: collectionView.frame.size.width, height: 40)
		}
		
        guard let data = seeAllViewModel?.dataModel else {
            return CGSize.zero
        }
        if seeAllViewModel?.nextPageAvailable ?? false && indexPath.row == data.data?.contentList?.count {
            return CGSize(width: collectionView.frame.size.width, height: 40)
        } else {
            let requiredNumOfColumns: CGFloat = isPhone ? 2:(data.data?.layoutType == LayoutType.potrait.rawValue) ? 5 : 4
            let deviceWidth: CGFloat = UIScreen.main.bounds.width
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

            let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
            let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
            let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing

            let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns

            let padding: CGFloat =  42
            let collectionViewSize = collectionView.frame.size.width - padding
            switch data.data?.layoutType {
            case LayoutType.potrait.rawValue:
                return CGSize(width: cellWidth, height: cellWidth*(16/9))
            case LayoutType.landscape.rawValue:
                return CGSize(width: cellWidth, height: cellWidth/1.7)
            default:
                return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kBAAppsHeaderReusableView, for: indexPath) as! BAAppsHeaderReusableView
                if indexPath.section == 0 {
                    reusableview.configureHeader(.appSubscribed)
                } else {
                    if unSubscribedProvider.count > 0 {
                        reusableview.configureHeader(.appNotSubscribed)
                    } else {
                        // Do Nothing
                    }
                }
            return reusableview
        default:  fatalError("Unexpected element kind")
        }
    }
}

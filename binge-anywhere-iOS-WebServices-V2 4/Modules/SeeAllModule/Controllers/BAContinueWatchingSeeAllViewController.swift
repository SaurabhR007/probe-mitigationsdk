//
//  BAContinueWatchingSeeAllViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

class BAContinueWatchingSeeAllViewController: BaseViewController {

    @IBOutlet weak var continueWatchingCollectionView: UICollectionView!

    var continueWatchViewModal: BAContinueWatchViewModal?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var isCW = true
    var isLoadMoreData = false
    var isSelectionMade = false
    /// CascadableScroll - Protocol AssociatedType explicite value
//    typealias GridViewType = UICollectionView
//    typealias ViewCellType = UICollectionViewCell
//    var cascadeQueue: OperationQueue = OperationQueue()
    var stopCollectionViewAnimationOnScroll: Bool = true
    var needAnimationFlag: Bool = true
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "See-All")
    }()

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        conformDelegated()
        registerCells()
        isLoadMoreData = false
        getContinueWatchData(/*isLoadMore: false*/)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    // MARK: - Function
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    func configureUI() {
        super.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "Recently Watched")
        self.continueWatchingCollectionView.backgroundColor = .BAdarkBlueBackground
    }

    func conformDelegated() {
        continueWatchingCollectionView.delegate = self
        continueWatchingCollectionView.dataSource = self
    }

    func registerCells() {
        continueWatchingCollectionView.register(UINib(nibName: kBAHomeRailCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeRailCollectionViewCell)
        continueWatchingCollectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
    }

    func getContinueWatchData(/*isLoadMore: Bool?*/) {
        continueWatchViewModal = BAContinueWatchViewModal(repo: BAContinueWatchRepo(), endPoint: "watched", baId: BAKeychainManager().baId)
        getContinueWatchingContent()
    }

    func getContinueWatchingContent() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: true)
            if continueWatchViewModal != nil {
                continueWatchViewModal?.loadMore = isLoadMoreData
                if isLoadMoreData {
                    continueWatchViewModal?.pagingState = continueWatchViewModal?.continueWatchData?.data?.pagingState ?? ""
                } else {
                    continueWatchViewModal?.pagingState = ""
                }
                continueWatchViewModal?.getContinueWatchData(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if self.continueWatchViewModal?.continueWatchData?.data?.contentList?.isEmpty ?? true {
                            self.continueWatchingCollectionView.isHidden = true
                        } else {
                            self.continueWatchingCollectionView.isHidden = false
                            self.continueWatchingCollectionView.reloadData()
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        if self.isLoadMoreData {
                            print("Load More Value is----------->", self.isLoadMoreData)
                            if message == "No data found for view history." {
                                // Do Nothing
                            } else {
                               self.apiError(message, title: "")
                            }
                        } else {
                            self.apiError(message, title: "")
                        }
                    }
                })
            }
        } else {
			noInternet()
//                {
//				self.getContinueWatchingContent()
//			}
        }
    }

    override func retryButtonAction() {
        getContinueWatchingContent()
    }

    func moveToDetailScreenFromContinueWatch(vodId: Int?, contentType: String?, timeStamp: Int?) {
        fetchContentDetail(vodId: vodId, contentType: contentType, timeStamp: timeStamp)
    }

    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?, timeStamp: Int?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId, timeStamp: timeStamp)
    }

    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?, timeStamp: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                   // self.hideActivityIndicator()
                    if flagValue {
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId, timeStamp: timeStamp)
//                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
//                        contentDetailVC.parentNavigation = self.navigationController
//                        contentDetailVC.contentDetail = dataModel.contentDetail
//                        self.navigationController?.pushViewController(contentDetailVC, animated: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?, timeStamp: Int?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId, timeStamp: timeStamp ?? 0)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int, timeStamp: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail, expiry: timeStamp)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?, expiry: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
                //hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                       // }
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                         contentDetailVC.parentNavigation = self.navigationController
                         contentDetailVC.contentDetail = contentDetail
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                         contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                         //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                             self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        // }
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                         contentDetailVC.parentNavigation = self.navigationController
                         contentDetailVC.contentDetail = contentDetail
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                         contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                         //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                             self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        // }
                       // self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

extension BAContinueWatchingSeeAllViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout/*, CascadableScroll*/, LoadMoreDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = continueWatchViewModal?.continueWatchData else {
            return 0
        }
        guard let count = data.data?.contentList?.count else {
            return 0
        }
//        if continueWatchViewModal?.continueWatchData?.data?.continuePagination ?? false {
//            return count + 1
//        } else {
            return count
//        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let data = continueWatchViewModal?.continueWatchData else {
            return CGSize.zero
        }

        if continueWatchViewModal?.continueWatchData?.data?.continuePagination ?? false && indexPath.row == data.data?.contentList?.count {
            return CGSize(width: collectionView.frame.size.width, height: 40)
        } else {
            let requiredNumOfColumns: CGFloat = isPhone ? 2 : (data.data?.layoutType == LayoutType.potrait.rawValue) ? 5 : 4
            let deviceWidth: CGFloat = UIScreen.main.bounds.width
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

            let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
            let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
            let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing

            let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns

            let padding: CGFloat =  42
            let collectionViewSize = collectionView.frame.size.width - padding
            switch data.data?.layoutType {
            case LayoutType.potrait.rawValue:
                return CGSize(width: cellWidth, height: cellWidth*(16/9))
            case LayoutType.landscape.rawValue:
                return CGSize(width: cellWidth, height: cellWidth)
            default:
                return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = true
        guard let data = continueWatchViewModal?.continueWatchData else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
            return cell
        }

//        if continueWatchViewModal?.continueWatchData?.data?.continuePagination ?? false && indexPath.row == data.data?.contentList?.count {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kLoadMoreCollectionViewCell, for: indexPath) as! LoadMoreCollectionViewCell
//            cell.delegate = self
//            return cell
//        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
            cell.configureCell(data.data?.contentList?[indexPath.row] ?? nil, data.data?.layoutType ?? "")
            return cell
//        }
    }

//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        cascadeCollectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
//    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = continueWatchViewModal?.continueWatchData  else {
            return
        }
        continueWatchingCollectionView.isUserInteractionEnabled = false
        if data.data?.contentList?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
            startPrimeNavigationJourney(data.data?.contentList?[indexPath.row] ?? BAContentListModel())
        } else {
            showActivityIndicator(isUserInteractionEnabled: false)
            moveToDetailScreenFromContinueWatch(vodId: data.data?.contentList?[indexPath.row].id, contentType: data.data?.contentList?[indexPath.row].contentType, timeStamp: data.data?.contentList?[indexPath.row].rentalExpiry)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
         self.continueWatchingCollectionView.isUserInteractionEnabled = true
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func loadMoreTapped() {
//        if continueWatchViewModal?.continueWatchData?.data?.continuePagination ?? false {
//            isLoadMoreData = true
//            getContinueWatchingContent()
//        }
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sixtyP = CGFloat(80) * scrollView.contentSize.height / CGFloat(100)
        if scrollView.bounds.maxY > sixtyP {
            if continueWatchViewModal?.nextPageAvailable ?? false {
                isLoadMoreData = true
                getContinueWatchingContent()
                continueWatchViewModal?.nextPageAvailable = false
            }
        }
    }
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }
}

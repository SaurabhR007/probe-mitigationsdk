//
//  BARecommendedVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 11/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

class BARecommendedSeeAllVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var seeAllCollectionView: UICollectionView!

    /// CascadableScroll - Protocol AssociatedType explicite value
    typealias GridViewType = UICollectionView
    typealias ViewCellType = UICollectionViewCell
    var cascadeQueue: OperationQueue = OperationQueue()
    var stopCollectionViewAnimationOnScroll: Bool = true
    var needAnimationFlag: Bool = true
    var contentDetail: ContentDetailData?
    var recommendationData: RecommendedContentData?
    var recommendedContentVM: BARecommendedContentViewModal?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var isSelectionMade = false
    var contentId: Int = 0
    var isTAData = false
    var pageSource = ""
    var configSource = ""
    var railName = ""
    var layoutType: String?
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "See-All")
    }()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        registerCollectionCells()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isTAData {
            fetchAllRecommendedContent(contentType: contentDetail?.meta?.contentType)
        }
    }
    
    override func viewDidLayoutSubviews() {
        if !isTAData {
            forcePaginate()
        }
    }

    func configureUI() {
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, recommendationData?.title ?? "")
        self.view.backgroundColor = .BAdarkBlueBackground
        self.seeAllCollectionView.backgroundColor = .BAdarkBlueBackground
        seeAllCollectionView.delegate = self
        seeAllCollectionView.dataSource = self
    }

    private func registerCollectionCells() {
        seeAllCollectionView.register(UINib(nibName: kBAHomeRailCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeRailCollectionViewCell)
        seeAllCollectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
//    func moveToDetailScreenFromSeeAll(vodId: Int?, contentType: String?) {
//        fetchContentDetail(vodId: vodId, contentType: contentType)
//    }
    
    func fetchAllRecommendedContent(contentType: String?) {
        var vodId = 0
        switch contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        
        recommendedContentVM = BARecommendedContentViewModal(repo: RecommendedContentRepo(), endPoint: "\(vodId)/" + (contentDetail?.meta?.contentType ?? ""), isFromSeeAllScreen: true)
        fetchAllEditorialRecommendataionData()
    }
    
    func fetchAllEditorialRecommendataionData() {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = recommendedContentVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getRecommendedContentData {(flagValue, _, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.seeAllCollectionView.reloadData {
                            self.forcePaginate()
                        }
                    }
                }
            }
        } else {
        }
    }


    func moveToDetailScreenFromSeeAll(vodId: Int?, contentType: String?) {
        fetchContentDetail(vodId: vodId, contentType: contentType)
    }

    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId)
    }

    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    //self.hideActivityIndicator()
                    if flagValue {
                        self.viewContentDetailEvent(contentData: dataModel.contentDetail, pageSource: "See-All", configSource: "Recommended", railName: self.railName)
                        self.fetchLastWatch(vodId: vodId, contentDetail: dataModel.contentDetail)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(vodId: Int?, contentDetail: ContentDetailData?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
                //hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.layoutType = self.layoutType
                        contentDetailVC.railType = self.isTAData ? TAConstant.recommendationType.rawValue : TAConstant.editorial.rawValue
                        contentDetailVC.id = self.contentId
                        contentDetailVC.pageSource = "See-All"
                        contentDetailVC.configSource = "Recommended"
                        contentDetailVC.railName = self.railName
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.layoutType = self.layoutType
                        contentDetailVC.railType = self.isTAData ? TAConstant.recommendationType.rawValue : TAConstant.editorial.rawValue
                        contentDetailVC.pageSource = "See-All"
                        contentDetailVC.configSource = "Recommended"
                        contentDetailVC.railName = self.railName
                        contentDetailVC.id = self.contentId
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.layoutType = self.layoutType
                        contentDetailVC.railType = self.isTAData ? TAConstant.recommendationType.rawValue : TAConstant.editorial.rawValue
                        contentDetailVC.pageSource = "See-All"
                        contentDetailVC.configSource = "Recommended"
                        contentDetailVC.railName = self.railName
                        contentDetailVC.id = self.contentId
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func viewContentDetailEvent(contentData: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        var title = ""
        if contentData?.meta?.contentType == ContentType.brand.rawValue {
            title = contentData?.meta?.brandTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.series.rawValue {
            title = contentData?.meta?.seriesTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.tvShows.rawValue {
            title = contentData?.meta?.vodTitle ?? ""
        } else {
            title = contentData?.meta?.vodTitle ?? ""
        }
        let genreResult = contentData?.meta?.genre?.joined(separator: ",")
        let languageResult = contentData?.meta?.audio?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
}

extension BARecommendedSeeAllVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CascadableScroll, LoadMoreDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isTAData {
            return recommendationData?.contentList?.count ?? 0
        } else {
            return recommendedContentVM?.recommendedData?.data?.contentList?.count ?? 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let requiredNumOfColumns: CGFloat = isPhone ? 2:(recommendationData?.layoutType == LayoutType.potrait.rawValue) ? 5 : 4
        let deviceWidth: CGFloat = UIScreen.main.bounds.width
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
        let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing
        
        let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns
        
        let padding: CGFloat =  42
        let collectionViewSize = collectionView.frame.size.width - padding
        switch recommendationData?.layoutType {
        case LayoutType.potrait.rawValue:
            return CGSize(width: cellWidth, height: cellWidth*(16/9))
        case LayoutType.landscape.rawValue:
            return CGSize(width: cellWidth, height: cellWidth)
        default:
            return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
        }
        //        let requiredNumOfColumns: CGFloat = isPhone ? 2:(recommendationData?.layoutType == LayoutType.potrait.rawValue) ? 5 : 4
        //        let deviceWidth: CGFloat = UIScreen.main.bounds.width
        //        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        //        let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
        //        let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
        //        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing
        //        let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns
        //        return CGSize(width: cellWidth, height: cellWidth)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
        if !isTAData {
            cell.configureCell(recommendedContentVM?.recommendedData?.data?.contentList?[indexPath.row], recommendationData?.layoutType ?? "")
        } else {
            cell.configureCell(recommendationData?.contentList?[indexPath.row], recommendationData?.layoutType ?? "")
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cascadeCollectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        seeAllCollectionView.isUserInteractionEnabled = false
        
        if isTAData {
            if recommendationData?.contentList?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                startPrimeNavigationJourney(recommendationData?.contentList?[indexPath.row] ?? BAContentListModel())
            } else {
                showActivityIndicator(isUserInteractionEnabled: false)
                moveToDetailScreenFromSeeAll(vodId: recommendationData?.contentList?[indexPath.row].id, contentType: recommendationData?.contentList?[indexPath.row].contentType)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                self.seeAllCollectionView.isUserInteractionEnabled = true
            }
        } else {
            if recommendedContentVM?.recommendedData?.data?.contentList?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                startPrimeNavigationJourney(recommendedContentVM?.recommendedData?.data?.contentList?[indexPath.row] ?? BAContentListModel())
            } else {
                showActivityIndicator(isUserInteractionEnabled: false)
                moveToDetailScreenFromSeeAll(vodId: recommendedContentVM?.recommendedData?.data?.contentList?[indexPath.row].id, contentType: recommendedContentVM?.recommendedData?.data?.contentList?[indexPath.row].contentType)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                self.seeAllCollectionView.isUserInteractionEnabled = true
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func loadMoreTapped() {
        //do nothing
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sixtyP = CGFloat(80) * scrollView.contentSize.height / CGFloat(100)
        if scrollView.bounds.maxY > sixtyP {
            if !isTAData {
                if recommendedContentVM?.nextPageAvailable ?? false {
                    fetchAllEditorialRecommendataionData()
                    recommendedContentVM?.nextPageAvailable = false
                }
            }
        }
    }
    
    func forcePaginate() {
        if UtilityFunction.shared.forcePaginate(self.seeAllCollectionView, recommendedContentVM?.nextPageAvailable) {
            fetchAllEditorialRecommendataionData()
        }
    }
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }
}

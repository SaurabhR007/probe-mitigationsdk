//
//  ExtensionPasswordVC+TextFieldDelegate.swift
//  BingeAnywhere
//
//  Created by Shivam on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension PasswordVC {
    // MARK: - TextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        return validateUserInput(textField.tag, txtAfterUpdate)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.view.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.textFieldHasError(false, with: "", errorMessage: "")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataModel(textField.tag, textField.text ?? "")
    }
    
    func validateUserInput(_ idx: Int, _ text: String) -> Bool {
        switch  idx {
        case 0:
//            Ask abhishek why implemented this logic
//            if text.length < 1 {
//                return false
//            }
            if text.length < 0 {
                return false
            }
            break
        default:
            if text.length > 256 {
                return false
            }
        }
        return true
    }
    
    func updateDataModel(_ idx: Int, _ text: String) {
        switch idx {
        case 0:
            passwordViewModel?.dataModel.tempPassword = text
        case 1:
            passwordViewModel?.dataModel.createPassword = text
        case 2:
            passwordViewModel?.dataModel.verifyPassword = text
        default:
            break
        }
    }
    
}

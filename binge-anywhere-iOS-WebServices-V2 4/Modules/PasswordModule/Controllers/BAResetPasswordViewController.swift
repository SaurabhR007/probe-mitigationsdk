//
//  BAResetPasswordViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/11/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAResetPasswordViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var resendOTPButtonEditProfile: CustomButton!
    @IBOutlet weak var headerTitleLabel: CustomLabel!
    @IBOutlet weak var sendOTPCodeButton: CustomButton!
    @IBOutlet weak var verifyPasswordPeekButton: UIButton!
    @IBOutlet weak var newPasswordPeekButton: UIButton!
    @IBOutlet weak var verifyNewPasswordTextField: CustomtextField!
    @IBOutlet weak var verifyPasswordLabel: CustomLabel!
    @IBOutlet weak var newpasswordTextField: CustomtextField!
    @IBOutlet weak var newPasswordLabel: CustomLabel!
    @IBOutlet weak var verifyPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    var verificationDataModel: LoginViewModel?
    var subscriberId: String?
    var rmn: String?
    var isFromEditProfile = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorLabel.isHidden = true
        self.title = ""
        if isFromEditProfile {
            super.configureNavigationBar(with: .backButton, #selector(backAction), nil, self, "Update Password")
        } else {
            self.configureNavigationBar(with: .backButton, #selector(backAction), nil, self)
        }
        configureLabelUI()
        configureTextFieldUI()
        reCheckVM()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFromEditProfile {
            resendOTPButtonEditProfile.isHidden = false
            sendOTPCodeButton.isHidden = true
        } else {
            resendOTPButtonEditProfile.isHidden = true
            sendOTPCodeButton.isHidden = false
        }
    }
    
    func configureTextFieldUI() {
        newpasswordTextField.delegate = self
        verifyNewPasswordTextField.delegate = self
        newpasswordTextField.placeholder = "New Password ⃰"
        verifyNewPasswordTextField.placeholder = "Confirm Password ⃰"
        sendOTPCodeButton.style(.tertiary, isActive: false)
        resendOTPButtonEditProfile.style(.tertiary, isActive: false)
        [newpasswordTextField,verifyNewPasswordTextField].forEach({ tf in
            tf?.text = ""
            tf?.backgroundColor = .BAdarkBlue1Color
            tf?.textColor = .BAwhite
            tf?.tintColor = .BAwhite
            tf?.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
            tf?.cornerRadius = 2
            tf?.setViewShadow(opacity: 0.12, blur: 2)
            tf?.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
            tf?.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16))
        })
    }
    
    func configureLabelUI() {
        headerTitleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30 * screenScaleFactorForWidth), color: .BAwhite)
        newPasswordLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        verifyPasswordLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        sendOTPCodeButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        resendOTPButtonEditProfile.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
    }
    
    @objc func backAction() {
        if isFromEditProfile {
            self.navigationController?.popViewController(animated: false)
        } else {
            kAppDelegate.createLoginRootView(isUserLoggedOut: true)
        }
        //kAppDelegate.createLoginRootView(isUserLoggedOut: true)
    }
    
    
    @IBAction func sendOTPCodeEditProfileAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let viewModel = verificationDataModel {
                viewModel.forgetPassword(isFromLogin: false, completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BAVerifyOTPForgotPasswordViewController", type: BAVerifyOTPForgotPasswordViewController.self)
                        pushView.verificationDataModel = self.verificationDataModel
                        pushView.isFromProfile = self.isFromEditProfile
                        self.navigationController?.pushViewController(pushView, animated: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    @IBAction func newPasswordPeekButtonAction(_ sender: Any) {
        if newpasswordTextField.isSecureTextEntry {
            newpasswordTextField.isSecureTextEntry = false
            newPasswordPeekButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            newpasswordTextField.isSecureTextEntry = true
            newPasswordPeekButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }
    
    @IBAction func verifyPeekButtonAction(_ sender: Any) {
        if verifyNewPasswordTextField.isSecureTextEntry {
            verifyNewPasswordTextField.isSecureTextEntry = false
            verifyPasswordPeekButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            verifyNewPasswordTextField.isSecureTextEntry = true
            verifyPasswordPeekButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }
    
    @IBAction func sendOTPCodeButtonAction(_ sender: Any) {
//        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BAVerifyOTPForgotPasswordViewController", type: BAVerifyOTPForgotPasswordViewController.self)
//        pushView.verificationDataModel = self.verificationDataModel
//        pushView.isFromProfile = self.isFromEditProfile
//        self.navigationController?.pushViewController(pushView, animated: true)
        
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let viewModel = verificationDataModel {
                viewModel.forgetPassword(isFromLogin: false, completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                    if viewModel.errorCode == 0 {
                            self.errorLabel.isHidden = true
                            self.verifyNewPasswordTextField.borderWidth = 0.0
                            let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BAVerifyOTPForgotPasswordViewController", type: BAVerifyOTPForgotPasswordViewController.self)
                            pushView.verificationDataModel = self.verificationDataModel
                            pushView.isFromProfile = self.isFromEditProfile
                            self.navigationController?.pushViewController(pushView, animated: true)
                        } else {
                           self.errorLabel.isHidden = false
                           self.errorLabel.text = message
                           self.verifyNewPasswordTextField.borderWidth = 1.0
                           self.verifyNewPasswordTextField.borderColor = .BAerrorRed
                        }
                    } else if isApiError {
                        self.verifyNewPasswordTextField.borderWidth = 0.0
                        self.errorLabel.isHidden = true
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func reCheckVM() {
        if self.verificationDataModel == nil {
            self.verificationDataModel = LoginViewModel()
        }
    }
}

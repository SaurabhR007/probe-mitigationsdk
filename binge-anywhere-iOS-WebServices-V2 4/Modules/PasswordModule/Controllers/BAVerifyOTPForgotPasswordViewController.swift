//
//  BAVerifyOTPForgotPasswordViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/11/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAVerifyOTPForgotPasswordViewController: BaseViewController, UITextFieldDelegate {
    @IBOutlet weak var resendCodeButton: CustomButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var backToAccountsButton: CustomButton!
    @IBOutlet weak var autenticatOTPEditProfileButton: CustomButton!
    @IBOutlet weak var expiryCounterLabel: CustomLabel!
    @IBOutlet weak var validateOTPButton: CustomButton!
    @IBOutlet weak var enterOTPLabel: CustomLabel!
    @IBOutlet weak var enterOTPtextField: CustomtextField!
    @IBOutlet weak var enterOtpView: UIView!
    @IBOutlet weak var titleHeaderLabel: CustomLabel!
    @IBOutlet weak var subtitleHeaderLabel: CustomLabel!
    var verificationDataModel: LoginViewModel?
    var passwordViewModel: PasswordVM?
    var newPassword: String?
    var verifyPassword: String?
    var rmn: String = "0000000000"
    var timer = Timer()
    var isFromProfile = false
    private var resendCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.isHidden = true
        self.title = ""
        if isFromProfile {
            super.configureNavigationBar(with: .backButton, #selector(backAction), nil, self, "Update Password")
        } else {
            self.configureNavigationBar(with: .backButton, #selector(backAction), nil, self)
        }
        configureLabelUI()
        configureUI()
        configureTextFieldUI()
        reCheckVM()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFromProfile {
            autenticatOTPEditProfileButton.isHidden = false
            backToAccountsButton.isHidden = false
            validateOTPButton.isHidden = true
        } else {
            autenticatOTPEditProfileButton.isHidden = true
            backToAccountsButton.isHidden = true
            validateOTPButton.isHidden = false
        }
    }
    
    func configureTextFieldUI() {
        enterOTPtextField.delegate = self
        enterOTPtextField.placeholder = "4 Digit"
        validateOTPButton.style(.tertiary, isActive: false)
        autenticatOTPEditProfileButton.style(.tertiary, isActive: false)
        [enterOTPtextField].forEach({ tf in
            tf?.text = ""
            tf?.backgroundColor = .BAdarkBlue1Color
            tf?.textColor = .BAwhite
            tf?.tintColor = .BAwhite
            tf?.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
            tf?.cornerRadius = 2
            tf?.setViewShadow(opacity: 0.12, blur: 2)
            tf?.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
            tf?.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16))
        })
    }
    
    func configureUI() {
        resendCodeButton.isUserInteractionEnabled = false
        rmn = verificationDataModel?.dataModel.rmn ?? "0000000000"
        print("OTP Sent to the RMN ----------->", rmn)
        subtitleHeaderLabel.text = "Please enter 4-digit OTP sent to your Registered Mobile Number and create a new password."//"The OTP is sent to +XX " + rmn.replaceFrom(startIndex: 0, endIndex: 4, With: "XXXXX")
        resendCodeButton.tintColor = .BAmidBlue
        expiryCounterLabel.text = "Resend OTP in \(BAConfigManager.shared.configModel?.data?.config?.otpDuration ?? 30) Secs"
        resendCodeButton.text(kResendOTP)
        resendCodeButton.titleLabel?.font = .skyTextFont(.medium, size: 16)
        resendCodeButton.underline()
        backToAccountsButton.text("Back To Accounts")
        backToAccountsButton.titleLabel?.font = .skyTextFont(.medium, size: 16)
        backToAccountsButton.underline()
        expiryCounterLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        runCodeExpiryTimer()
    }
    
    private func reCheckVM() {
        if self.verificationDataModel == nil {
            self.verificationDataModel = LoginViewModel()
        }
    }
    
    func intializePasswordVM() {
        if passwordViewModel == nil {
            passwordViewModel = PasswordVM(repo: PasswordDataCall(), endPoint: verificationDataModel?.dataModel.sid ?? "")
            passwordViewModel?.dataModel.createPassword = verificationDataModel?.dataModel.password ?? ""
            passwordViewModel?.dataModel.verifyPassword = verificationDataModel?.dataModel.verifyPassword ?? ""
            passwordViewModel?.dataModel.tempPassword = verificationDataModel?.dataModel.otp ?? ""
        }
    }

    func runCodeExpiryTimer() {
            var resetCount = BAConfigManager.shared.configModel?.data?.config?.otpDuration ?? 0
            timer.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                if resetCount ?? 0 > 0 {
                    resetCount -= 1
                    self.resendCodeButton.tintColor = .BAmidBlue
                    if self.resendCodeButton.isHidden == true {
                      self.expiryCounterLabel.isHidden = true
                    } else {
                        self.expiryCounterLabel.isHidden = false
                    }
                    self.expiryCounterLabel.text = "Resend OTP in \(resetCount ?? 30) secs"
                    self.resendCodeButton.isUserInteractionEnabled = false
                } else {
                    timer.invalidate()
                    self.resendCodeButton.tintColor = .BABlueColor
                    self.resendCodeButton.isUserInteractionEnabled = true
                    self.expiryCounterLabel.isHidden = true
                }
            }
            RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
    }
    
    func configureLabelUI() {
        titleHeaderLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30 * screenScaleFactorForWidth), color: .BAwhite)
        subtitleHeaderLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        enterOTPLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
    }
    
    func showResendCountAlert() {
        apiError(AppStringConstant.otpResendLimit.rawValue, title: "")
    }
    
    
    @IBAction func autenticateOTPEditProfileButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = passwordViewModel {
                viewModel.changePassword { (flagVal, message, isApiError) in
                    kAppDelegate.window?.isUserInteractionEnabled = true
                    if flagVal {
                        self.errorLabel.isHidden = true
                        self.enterOTPtextField.borderWidth = 0.0
                        if self.isFromProfile {
                            self.showSuccessAlert()
                        } else {
                           self.popupNotification(message ?? "Password has been updated") {
                               kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                           }
                        }
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeSuccess.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue, MixpanelConstants.ParamName.sid.rawValue: self.verificationDataModel?.dataModel.sid ?? ""])
                    } else if isApiError {
                        self.errorLabel.isHidden = true
                        self.enterOTPtextField.borderWidth = 0.0
                        self.apiError(message, title: kSomethingWentWrong)
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? "", MixpanelConstants.ParamName.sid.rawValue: self.verificationDataModel?.dataModel.sid ?? ""])
                    } else {
                        self.errorLabel.isHidden = false
                        self.enterOTPtextField.borderWidth = 1.0
                        self.enterOTPtextField.borderColor = .BAerrorRed
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? "", MixpanelConstants.ParamName.sid.rawValue: self.verificationDataModel?.dataModel.sid ?? ""])
                       // self.apiError(message, title: kSomethingWentWrong)
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    
    @IBAction func backToAccountsButtonAction(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AccountVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func validateOTPButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = passwordViewModel {
                viewModel.changePassword { (flagVal, message, isApiError) in
                    kAppDelegate.window?.isUserInteractionEnabled = true
                    if flagVal {
                        self.errorLabel.isHidden = true
                        self.enterOTPtextField.borderWidth = 0.0
                        if self.isFromProfile {
                            self.showSuccessAlert()
                        } else {
                           self.popupNotification(message ?? "Password has been updated") {
                               kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                           }
                        }
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeSuccess.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue, MixpanelConstants.ParamName.sid.rawValue: BAKeychainManager().sId])
                    } else if isApiError {
                        self.errorLabel.isHidden = true
                        self.enterOTPtextField.borderWidth = 0.0
                        self.apiError(message, title: kSomethingWentWrong)
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                    } else {
                        self.errorLabel.isHidden = false
                        self.errorLabel.text = message
                        self.enterOTPtextField.borderWidth = 1.0
                        self.enterOTPtextField.borderColor = .BAerrorRed
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                        //self.apiError(message, title: kSomethingWentWrong)
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func showSuccessAlert() {
        showBackAccountAlert("Password has been updated") {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AccountVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func resendCodeButtonAction(_ sender: Any) {
        let otpCount = BAConfigManager.shared.configModel?.data?.config?.otpResentCount ?? 0
        if otpCount ?? 3 > resendCount {
            resendCodeButton.isHidden = false
            resendCount += 1
            resendCodeButton.isUserInteractionEnabled = true
            self.resendCodeButton.tintColor = .BABlueColor
            if otpCount == resendCount {
                resendCodeButton.isHidden = true
                self.expiryCounterLabel.isHidden = true
            } else {
                runCodeExpiryTimer()
            }
            resendButtonTapped()
        } else {
            resendCodeButton.isHidden = true
            self.expiryCounterLabel.isHidden = true
            showResendCountAlert()
            resendCodeButton.isUserInteractionEnabled = false
            self.resendCodeButton.tintColor = .BAlightBlueGrey
        }
    }
    
    func resendButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let viewModel = verificationDataModel {
                viewModel.forgetPassword(isFromLogin: true, completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if viewModel.errorCode == 0 {
                           self.otpResent("OTP generated successfully", completion: nil)
                        } else {
                            self.otpResent(message ?? "", completion: nil)
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func moveToSuccessScreen() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: "CommonAlert", identifierVC: "ApiSuccessViewController", type: ApiSuccessViewController.self)
        pushView.successTitleText = "Your Password Has Been Updated"
        pushView.hideNextButton = true
        pushView.addTimer = true
        pushView.secondaryTitleText = "You’ll be Redirected back to the Login Page in \(BAConfigManager.shared.configModel?.data?.config?.passwordRedirectionTimeInSecs ?? 5) secs"
        self.navigationController?.pushViewController(pushView, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  PasswordVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
//TODO: all the button methods and placeholder value to be optimised as sudden changes lead to many raw code and methods.

class PasswordVC: BaseViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var dataModel = UpdatePasswordDataModel()
    var passwordViewModel: PasswordVM?
    var loginViewModel: LoginViewModel?
    var showFirstField = true
    var isFromEditProfile = false
    
    // MARK: - Outlets
    @IBOutlet weak var headerLogo: CustomLabel!
    @IBOutlet weak var errorInfoLabel: CustomLabel!
    @IBOutlet weak var subHeaderLogo: CustomLabel!
    @IBOutlet weak var frstDescLabel: CustomLabel!
    @IBOutlet weak var scndDescLabel: CustomLabel!
    @IBOutlet weak var thrdDescLabel: CustomLabel!
    @IBOutlet weak var tempPassTextField: CustomtextField!
    @IBOutlet weak var createPassTextField: CustomtextField!
    @IBOutlet weak var verifyPassTextField: CustomtextField!
    @IBOutlet weak var updateButton: CustomButton!
    @IBOutlet weak var peekCurrentPasswordButton: UIButton!
    @IBOutlet weak var peekCreatePasswordButton: UIButton!
    @IBOutlet weak var firstViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var peekverifyPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = ""
        //self.configureNavigationBar(with: .backButton, #selector(backAction), nil, self)
        super.configureNavigationBar(with: .backButton, #selector(backAction), nil, self, "Update Password")
        reCheckVM()
        configureLabelUI()
        configureTextFieldUI()
        setViewPlaceholderText()
        setViewPlaceholderText()
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePassword.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !showFirstField {
            firstViewConstraint.constant = 0
            udpateLabelHeaders()
        }
        peekCurrentPasswordButton.isHidden = dataModel.hideCurrentPasswordPeekButton
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func reCheckVM() {
        if passwordViewModel == nil {
            passwordViewModel = PasswordVM(repo: PasswordDataCall(), endPoint: BAKeychainManager().sId)
            passwordViewModel?.dataModel = dataModel
        }
    }
    
    @objc func backAction() {
        if isFromEditProfile {
            self.navigationController?.popViewController(animated: false)
        } else {
            kAppDelegate.createLoginRootView(isUserLoggedOut: true)
        }
        //self.navigationController?.popToViewController(, animated: <#T##Bool#>)
    }
    
    //TODO:- Hotfix for create password screen
    func udpateLabelHeaders() {
        headerLogo.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 24 * screenScaleFactorForWidth), color: .BAwhite)
        scndDescLabel.text = "New Password"
        thrdDescLabel.text = "Verify New Password"
        self.title = "Update Password"
    }
    
    func configureLabelUI() {
        headerLogo.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30 * screenScaleFactorForWidth), color: .BAwhite)
        subHeaderLogo.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAlightBlueGrey)
        frstDescLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        scndDescLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        thrdDescLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        updateButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
    }
    
    func configureTextFieldUI() {
        tempPassTextField.placeholder = dataModel.frstLabelPlaceHolderText
        createPassTextField.placeholder = dataModel.hideCurrentPasswordPeekButton ? "Password ⃰" : "New Password ⃰"
        errorInfoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 10 * screenScaleFactorForWidth), color: .BAerrorRed)
        verifyPassTextField.placeholder = "Verify Password ⃰"
        updateButton.style(.tertiary, isActive: false)
        [tempPassTextField,createPassTextField,verifyPassTextField].forEach({ tf in
            tf?.text = ""
            tf?.backgroundColor = .BAdarkBlue1Color
            tf?.textColor = .BAwhite
            tf?.tintColor = .BAwhite
            tf?.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
            tf?.cornerRadius = 2
            tf?.setViewShadow(opacity: 0.12, blur: 2)
//            tf?.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
            tf?.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 50 * screenScaleFactorForWidth, isRightValueInUse: true)
            tf?.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16))
        })
        tempPassTextField.keyboardType = dataModel.hideCurrentPasswordPeekButton ? .asciiCapableNumberPad : .asciiCapable
    }
    
    func setViewPlaceholderText() {
        headerLogo.text = dataModel.headerLogoText
        subHeaderLogo.text = dataModel.subheaderLogoText
        updateButton.setTitle(dataModel.buttonText, for: .normal)
        frstDescLabel.text = dataModel.frstLabelDescText
    }
    
    @IBAction func updateButtonTapped(_ sender: CustomButton) {
        self.view.endEditing(true)
        if !showFirstField {
            view.makeToast("Under Development", duration: 3.0, position: .bottom)
            return
        }
        self.showActivityIndicator(isUserInteractionEnabled: false)
        if let viewModel = passwordViewModel {
            viewModel.validatePassword { (flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    self.updatePasswordEdirProfileApiCall()
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        }
    }
    
    @IBAction func peekVerifyPasswordAction(_ sender: Any) {
        if verifyPassTextField.isSecureTextEntry {
            verifyPassTextField.isSecureTextEntry = false
            peekverifyPasswordButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            verifyPassTextField.isSecureTextEntry = true
            peekverifyPasswordButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }
    
    @IBAction func peekCreatePasswordAction(_ sender: Any) {
        if createPassTextField.isSecureTextEntry {
            createPassTextField.isSecureTextEntry = false
            peekCreatePasswordButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            createPassTextField.isSecureTextEntry = true
            peekCreatePasswordButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }
    
    
    @IBAction func peekConfirmPasswordAction(_ sender: Any) {
        if tempPassTextField.isSecureTextEntry {
            tempPassTextField.isSecureTextEntry = false
            peekCurrentPasswordButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            tempPassTextField.isSecureTextEntry = true
            peekCurrentPasswordButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }
    
    
    @IBAction func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == tempPassTextField {
            updateButton.style((textField.text?.count ?? 0) > 0 ? .primary : .tertiary, isActive: (textField.text?.count ?? 0) > 0)
        }
    }
    
    func updatePasswordApiCall() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = passwordViewModel {
                viewModel.changePassword { (flagVal, message, isApiError) in
                    kAppDelegate.window?.isUserInteractionEnabled = true
                    if flagVal {
                        if self.isFromEditProfile {
                            self.showSuccessAlert()
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordSucess.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
                        } else {
                           self.moveToSuccessScreen()
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.passwordChangeSuccess.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
                        }
                    } else if isApiError {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                        self.apiError(message, title: kSomethingWentWrong)
                    } else if viewModel.errorCode == 6034 || viewModel.errorCode == 6031 {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                        self.textFieldHasError(true, with: "", errorMessage: message ?? "")
                    } else {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                }
            }
        } else {
			noInternet()
        }
    }
    
    func updatePasswordEdirProfileApiCall() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = passwordViewModel {
                viewModel.updatePassword { (flagVal, message, isApiError) in
                    kAppDelegate.window?.isUserInteractionEnabled = true
                    if flagVal {
                        if self.isFromEditProfile {
                            self.showSuccessAlert()
                        } else {
                           self.moveToSuccessScreen()
                        }
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordSucess.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else if viewModel.errorCode == 6034 || viewModel.errorCode == 6031 {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                        self.textFieldHasError(true, with: "", errorMessage: message ?? "")
                    } else {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updatePasswordFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: message ?? ""])
                        self.apiError(message, title: ""/*kSomethingWentWrong*/)
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
        if hasError {
            tempPassTextField.borderWidth = 1.0
            tempPassTextField.borderColor = .BAerrorRed
            errorInfoLabel.text = errorMessage
        } else {
            tempPassTextField.borderWidth = 0.0
            errorInfoLabel.text = ""
        }
    }
    
    func showSuccessAlert() {
        showBackAccountAlert("Password has been updated") {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AccountVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }

    
    func moveToSuccessScreen() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: "CommonAlert", identifierVC: "ApiSuccessViewController", type: ApiSuccessViewController.self)
        pushView.successTitleText = "Your Password Has Been Updated"
        pushView.hideNextButton = true
        pushView.addTimer = true
        pushView.secondaryTitleText = "You’ll be Redirected back to the Login Page in \(BAConfigManager.shared.configModel?.data?.config?.passwordRedirectionTimeInSecs ?? 5) secs"
        self.navigationController?.pushViewController(pushView, animated: true)
    }
    
    func hideFirstField() {
        tempPassTextField.isHidden = true
        frstDescLabel.isHidden = true
    }
}

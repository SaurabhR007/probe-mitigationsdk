//
//  BAVerifyOTPForgotPasswordViewController + TextFieldDelegate.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/11/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension BAVerifyOTPForgotPasswordViewController {
    // MARK: - TextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        self.errorLabel.isHidden = true
        self.enterOTPtextField.borderWidth = 0.0
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        if txtAfterUpdate.count > 4 {
            return false
        } else {
            return true
        }
        //return validateUserInput(textField.tag, txtAfterUpdate)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.view.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if enterOTPtextField.text?.length == 4 {
            validateOTPButton.style(.primary, isActive: true)
            autenticatOTPEditProfileButton.style(.primary, isActive: true)
            verificationDataModel?.dataModel.otp = enterOTPtextField.text
            passwordViewModel?.dataModel.tempPassword = verificationDataModel?.dataModel.otp ?? ""
            BAKeychainManager().sId = verificationDataModel?.dataModel.sid ?? ""
            intializePasswordVM()
        } else {
            validateOTPButton.style(.tertiary, isActive: false)
            autenticatOTPEditProfileButton.style(.tertiary, isActive: false)
        }
    }
    
    func validateUserInput(_ idx: Int, _ text: String) -> Bool {
        switch  idx {
        case 0:
//            Ask abhishek why implemented this logic
//            if text.length < 1 {
//                return false
//            }
            if text.length < 0 {
                return false
            }
            break
        default:
            if text.length > 16 {
                return false
            }
        }
        return true
    }
}


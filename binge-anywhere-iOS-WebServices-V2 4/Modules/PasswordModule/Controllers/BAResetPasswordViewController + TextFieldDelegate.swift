//
//  BAResetPasswordViewController + TextFieldDelegate.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/11/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension BAResetPasswordViewController {
    // MARK: - TextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        errorLabel.isHidden = true
        self.verifyNewPasswordTextField.borderWidth = 0.0
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        if textField.tag == 1 {
            if txtAfterUpdate == newpasswordTextField.text {
                sendOTPCodeButton.style(.primary, isActive: true)
                verificationDataModel?.dataModel.password = newpasswordTextField.text ?? ""
                verificationDataModel?.dataModel.verifyPassword = txtAfterUpdate
            } else {
                sendOTPCodeButton.style(.tertiary, isActive: false)
            }
        } else {
            if txtAfterUpdate == verifyNewPasswordTextField.text {
                sendOTPCodeButton.style(.primary, isActive: true)
                verificationDataModel?.dataModel.password = txtAfterUpdate
                verificationDataModel?.dataModel.verifyPassword = verifyNewPasswordTextField.text ?? ""
            } else {
                sendOTPCodeButton.style(.tertiary, isActive: false)
            }
        }
        return validateUserInput(textField.tag, txtAfterUpdate)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.view.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !(verifyNewPasswordTextField.text?.isEmpty ?? true) && !(newpasswordTextField.text?.isEmpty ?? true) {
            sendOTPCodeButton.style(.primary, isActive: true)
            resendOTPButtonEditProfile.style(.primary, isActive: true)
            verificationDataModel?.dataModel.password = newpasswordTextField.text ?? ""
            verificationDataModel?.dataModel.verifyPassword = verifyNewPasswordTextField.text ?? ""
           // verificationDataModel?.dataModel.sid = verificationDataModel?.dataModel.sid
        } else {
            sendOTPCodeButton.style(.tertiary, isActive: false)
            //sendOTPCodeButton.style(.tertiary, isActive: false)
        }
    }
    
//    func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
//        if hasError {
//            inputTextField.borderWidth = 1.0
//            inputTextField.borderColor = .BAerrorRed
//            inputTextField.placeholder = Placeholder
//            errorInfoLabel.text = errorMessage
//            errorInfoLabel.isHidden = false
//        } else {
//            inputTextField.borderWidth = 0.0
//            errorInfoLabel.text = ""
//            errorInfoLabel.isHidden = true
//        }
//    }
    
    func validateUserInput(_ idx: Int, _ text: String) -> Bool {
        switch  idx {
        case 0:
//            Ask abhishek why implemented this logic
//            if text.length < 1 {
//                return false
//            }
            if text.length < 0 {
                return false
            }
            break
        default:
            if text.length > 16 {
                return false
            }
        }
        return true
    }
    
//    func updateDataModel(_ idx: Int, _ text: String) {
//        switch idx {
//        case 0:
//            passwordViewModel?.dataModel.tempPassword = text
//        case 1:
//            passwordViewModel?.dataModel.createPassword = text
//        case 2:
//            passwordViewModel?.dataModel.verifyPassword = text
//        default:
//            break
//        }
//    }
    
}


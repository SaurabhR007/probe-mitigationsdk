//
//  PasswordModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct UpdatePasswordDataModel {
    var headerLogoText = ""
    var subheaderLogoText = ""
    var buttonText = ""
    var frstLabelPlaceHolderText = ""
    var frstLabelDescText = ""
    var rmn = ""
	var sid = ""
    var tempPassword = ""
    var createPassword = ""
    var verifyPassword = ""
    var hideCurrentPasswordPeekButton = true
}

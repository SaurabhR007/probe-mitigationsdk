//
//  PasswordVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol PasswordScreenDelegate: class {
    var dataModel: UpdatePasswordDataModel {get set}
    func changePassword(completion: @escaping (Bool, String?, Bool) -> Void)
    func createPassword(completion: @escaping (Bool, String?, Bool) -> Void)
    func validatePassword(completion: @escaping (Bool, String?, Bool) -> Void)
}

class PasswordVM: PasswordScreenDelegate {
    var dataModel = UpdatePasswordDataModel()
    var repo: UpdatePasswordDataCall
    var endPoint: String = ""
    var errorCode: Int?
    var requestToken: ServiceCancellable?

    init(repo: UpdatePasswordDataCall, endPoint: String ) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }

    func changePassword(completion: @escaping (Bool, String?, Bool) -> Void) {
        createApiParams()
        requestToken = repo.changePasswordApiCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    self.errorCode = statusCode
                    if statusCode == 0 {
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.allowWatchNotification.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: MixpanelConstants.ParamValue.falseValue.rawValue])
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func updatePassword(completion: @escaping (Bool, String?, Bool) -> Void) {
        createApiParams()
        requestToken = repo.changePasswordEditProfileApiCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    self.errorCode = statusCode
                    if statusCode == 0 {
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }

    func createPassword(completion: @escaping (Bool, String?, Bool) -> Void) {
        createApiParams()
        requestToken = repo.createPasswordApiCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }

    func validatePassword(completion: @escaping (Bool, String?, Bool) -> Void) {
        if dataModel.createPassword.isEmpty || dataModel.tempPassword.isEmpty || dataModel.verifyPassword.isEmpty {
            completion(false, "All fields are mandatory.", false)
            return
        } else if !dataModel.createPassword.isValidPasswordRegex() {
            completion(false, "Password must contain One Special Character, One Number, and of length greater than 8.", false)
            return
        } else if dataModel.createPassword != dataModel.verifyPassword {
            completion(false, "New Password and Verify New Password is not same.", false)
            return
        } else {
           completion(true, nil, true)
        }
    }

    func createApiParams() {
        repo.apiParams = [ : ]
        repo.apiEndPoint = BAKeychainManager().sId
        repo.apiParams["confirmPwd"] = dataModel.verifyPassword
        repo.apiParams["newPwd"] = dataModel.createPassword
        repo.apiParams["oldPwd"] = dataModel.tempPassword
    }
}

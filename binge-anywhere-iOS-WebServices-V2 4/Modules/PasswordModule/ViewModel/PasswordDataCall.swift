//
//  PasswordDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol UpdatePasswordDataCall {
    var apiEndPoint: String {get set}
    var apiParams: [AnyHashable: Any] {get set}
    func changePasswordApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func changePasswordEditProfileApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func createPasswordApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct PasswordDataCall: UpdatePasswordDataCall {
    var apiParams: [AnyHashable: Any] = [:]
    var apiEndPoint: String = ""

    func changePasswordApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .changePassword(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<UpdatePasswordResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
    
    func changePasswordEditProfileApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
        let SID = BAKeychainManager().sId
        
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "\(accessToken)", "profileId": "\(BAKeychainManager().profileId)", "platform": "BINGE_ANYWHERE", "subscriberid": "\(SID)", "deviceType": "IOS", "deviceId": kDeviceId, "deviceName": kDeviceModal]
        let target = ServiceRequestDetail.init(endpoint: .updatePassword(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<UpdatePasswordResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }

    // TODO: - call when create password integrated
    func createPasswordApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .forgetPassword(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<UpdatePasswordResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

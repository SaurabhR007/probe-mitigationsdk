//
//  ExtensionSwitchAccount.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

extension SidSelectionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let count = verificationDataModel?.sidLookUpModel?.count ?? 0
		return count + 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
			cell?.configureIndicator(number: 4, selectedIndex: 1)
			return cell!
		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
			cell?.configureCell(source: .sidSelection)
			return cell!
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: kIdSelectionTableViewCell) as? IdSelectionTableViewCell
			cell?.delegate = self
            cell?.selectedIndex = selectedIndex
            cell?.configureCellForSid((verificationDataModel?.sidLookUpModel?[indexPath.row - 2])!, index: indexPath.row - 2)
			return cell!
		}
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
    }
}

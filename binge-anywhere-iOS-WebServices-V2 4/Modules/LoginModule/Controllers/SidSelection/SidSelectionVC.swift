//
//  SwitchAccountVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class SidSelectionVC: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var proceedButton: CustomButton!
    
    // MARK: - Variables
    var verificationDataModel: LoginViewModel?
    var subscriberDetailModal: SubscriberDetailResponseData?
    var subscriptionVM: PackSubscriptionVM?
    var signOutVM: BASignOutViewModal?
    var selectedIndex = -1
    var isErrorOccured = false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupTableView()
    }
    
    // MARK: - Functions
    func configureUI() {
        super.configureNavigationBar(with: .backButton, #selector(backAction), nil, self)
        self.view.backgroundColor = .BAdarkBlueBackground
        tableView.backgroundColor = .BAdarkBlueBackground
        proceedButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        proceedButton.style(.tertiary, isActive: false)
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: kIdSelectionTableViewCell, bundle: nil), forCellReuseIdentifier: kIdSelectionTableViewCell)
        tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
        tableView.register(UINib(nibName: kTitleHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: kTitleHeaderTableViewCell)
    }
    
    // MARK: - Actions
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
        UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.subscriberId.rawValue)
    }
    
    @IBAction func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = verificationDataModel {
                if let sidData = viewModel.sidLookUpModel {
                    for sid in sidData where String(sid.subscriberId ?? 0) == BAKeychainManager().sId {
                        BAKeychainManager().fullName = (sid.subscriberName ?? "")
                        BAKeychainManager().baIdCount = (sid.accountDetailList?.count ?? 0) > 0 ? (sid.accountDetailList?.count ?? 0) : 1
                        if sid.accountDetailList?.count == 1 {
                            //only one baId found for current SID open home
                            if sid.accountDetailList?[0].baId == nil {
                                viewModel.dataModel.sid = BAKeychainManager().sId
                                BAKeychainManager().dsn = sid.accountDetailList?[0].deviceSerialNumber ?? ""
                                viewModel.dataModel.dsn = sid.accountDetailList?[0].deviceSerialNumber
                                if let subscriberModal = viewModel.sidLookUpModel {
                                    for subscriber in subscriberModal  where String(subscriber.subscriberId ?? 0) == BAKeychainManager().sId {
                                        subscriberDetailModal = subscriber
                                    }
                                }
                                let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "CreateBingeAccountVC", type: CreateBingeAccountVC.self)
                                pushView.createAccountDataModel = viewModel
                                pushView.createDataModel = subscriberDetailModal
                                self.navigationController?.pushViewController(pushView, animated: true)
                            } else {
                                viewModel.dataModel.sid = BAKeychainManager().sId
                                viewModel.dataModel.baid = String(sid.accountDetailList?.first?.baId ?? 0)
                                self.showActivityIndicator(isUserInteractionEnabled: false)
                                viewModel.initiateLogin(completion: { (flagValue, message, isApiError) in
                                    self.hideActivityIndicator()
                                    if flagValue {
                                        self.showInactiveAlerts()
                                        /*
                                        if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" && BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                                            self.inactiveDTHAlert("You need to Activate DTH and Binge to continue watching.", title: kTempSuspendedHeader) {
                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                    self.fetchSubscriptionDetails()
                                                } else {
                                                    kAppDelegate.createHomeTabRootView()
                                                }
                                            }
                                        } else if (BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunned || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunnedState) {
                                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                                        DispatchQueue.main.async { [weak self] in
                                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                                if isOkay {
                                                                    self?.signOut()
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        DispatchQueue.main.async { [weak self] in
                                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                                if isOkay {
                                                                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                                        self?.fetchSubscriptionDetails()
                                                                    } else {
                                                                        kAppDelegate.createHomeTabRootView()
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                DispatchQueue.main.async { [weak self] in
                                                    AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                        if isOkay {
                                                            self?.signOut()
                                                        }
                                                    }
                                                }
                                            }
                                        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" {
                                            self.inactiveDTHAlert(kDTHInactive, title: kTempSuspendedHeader) {
                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                    self.fetchSubscriptionDetails()
                                                } else {
                                                    kAppDelegate.createHomeTabRootView()
                                                }
                                            }
                                        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "TEMP_SUSPENSION" || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
                                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                                        DispatchQueue.main.async { [weak self] in
                                                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                                if isOkay {
                                                                    self?.signOut()
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        DispatchQueue.main.async { [weak self] in
                                                            AlertController.initialization().showAlert(.doubleButton(.ok, .skip, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                                if isOkay {
                                                                    self?.signOut()
                                                                } else {
                                                                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                                        self?.fetchSubscriptionDetails()
                                                                    } else {
                                                                        kAppDelegate.createHomeTabRootView()
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                DispatchQueue.main.async { [weak self] in
                                                    AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                        if isOkay {
                                                            self?.signOut()
                                                        }
                                                    }
                                                }
                                            }
                                        } else if BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                                            self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                    self.fetchSubscriptionDetails()
                                                } else {
                                                    kAppDelegate.createHomeTabRootView()
                                                }
                                            }
                                        } else if (BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState) {
                                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                                        DispatchQueue.main.async { [weak self] in
                                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                                if isOkay {
                                                                    self?.signOut()
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        DispatchQueue.main.async { [weak self] in
                                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                                if isOkay {
                                                                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                                        self?.fetchSubscriptionDetails()
                                                                    } else {
                                                                        kAppDelegate.createHomeTabRootView()
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                DispatchQueue.main.async { [weak self] in
                                                    AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                        if isOkay {
                                                            self?.signOut()
                                                        }
                                                    }
                                                }
                                            }
                                        }  else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                               self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                    self.fetchSubscriptionDetails()
                                                } else {
                                                    kAppDelegate.createHomeTabRootView()
                                                }
                                            }
                                        } else {
                                            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                self.fetchSubscriptionDetails()
                                            } else {
                                                kAppDelegate.createHomeTabRootView()
                                            }
                                        }
                                       */
                                        //Only one Sid and One BaId found
                                    } else if isApiError {
                                        self.apiError(message ?? "Error logging in.", title: kSomethingWentWrong)
                                    } else {
                                        self.apiError(message ?? "Error logging in.", title: "")
                                    }
                                })
                            }
                        } else if sid.accountDetailList?.count ?? 0 > 1 {
                            //Multiple BaId found, open BaId selection screen
                            let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BaIdSelectionVC", type: BaIdSelectionVC.self)
                            pushView.baidDataModel = viewModel
                            pushView.bingeAccountList = viewModel.sidLookUpModel?[selectedIndex].accountDetailList
                            pushView.isFromAccount = false
                            pushView.verificationDataModel = self.verificationDataModel
                            pushView.isFromRMN = true
                            self.navigationController?.pushViewController(pushView, animated: true)
                        } else {
                            //no baid found, new user, CREATE
                            viewModel.dataModel.sid = BAKeychainManager().sId
                            viewModel.dataModel.dsn = BAKeychainManager().dsn
                            if let subscriberModal = viewModel.sidLookUpModel {
                                for subscriber in subscriberModal  where String(subscriber.subscriberId ?? 0) == BAKeychainManager().sId {
                                    subscriberDetailModal = subscriber
                                }
                            }
                            let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "CreateBingeAccountVC", type: CreateBingeAccountVC.self)
                            pushView.createAccountDataModel = viewModel
                            pushView.createDataModel = subscriberDetailModal
                            self.navigationController?.pushViewController(pushView, animated: true)
                        }
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func fetchSubscriptionDetails() {
        subscriptionVM = PackSubscriptionVM()
        getSubscription()
    }
    
    func showInactiveAlerts() {
        if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kDeActive || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kInactiveState {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
                self.alertWithInactiveTitleAndMessage(kBingeSubscriptionMBRDeactiveMessage, title: kDTHInactiveLowBalanceHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHDunnedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspended || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHTempSuspendedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else {
            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                self.fetchSubscriptionDetails()
            } else {
                kAppDelegate.createHomeTabRootView()
            }
        }
    }
    
    func getSubscription() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if subscriptionVM != nil {
                subscriptionVM?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                            kAppDelegate.createHomeTabRootView()
                        } else {
                            let subscriptionVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.subscription.rawValue, type: MySubscriptionVC.self)
                            subscriptionVC.subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                            subscriptionVC.isFromAccountScreen = false
                            self.navigationController?.pushViewController(subscriptionVC, animated: true)
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet() {
                self.getSubscription()
            }
        }
    }
    
    func signOut() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            signOutVM = BASignOutViewModal(repo: BASignOutRepo())
            if let signOutVM = signOutVM {
                signOutVM.signOut {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        BAKeychainManager().isFSPopUpShown = false
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

extension SidSelectionVC: IdSelectionTableViewCellProtocol {
    // HOTFIX: to remove multiple selections and to disable selection of disabled id
    func cellSelected(_ text: String, _ id: String,_ index: Int?) {
        if (verificationDataModel?.sidLookUpModel?[index ?? 0].loginErrorMessage?.isEmpty ?? false) || (verificationDataModel?.sidLookUpModel?[index ?? 0].loginErrorMessage == nil) {
            proceedButton.style(text.count > 0 ? .primary : .tertiary, isActive: text.count > 0)
            BAKeychainManager().sId = text
            selectedIndex = index ?? 0
            tableView.reloadData()
        } else {
            if verificationDataModel?.sidLookUpModel?[index ?? 0].accountStatus == kPending {
                apiError(verificationDataModel?.sidLookUpModel?[index ?? 0].loginErrorMessage ?? "", title: kErrorHeader)
            } else {
                showOkAlert(verificationDataModel?.sidLookUpModel?[index ?? 0].loginErrorMessage ?? "")
            }
            tableView.reloadData()
        }
    }
}


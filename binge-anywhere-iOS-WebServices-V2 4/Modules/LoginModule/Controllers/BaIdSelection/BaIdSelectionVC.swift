//
//  BaIdSelectionVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 29/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BaIdSelectionVC: BaseViewController {
	
	// MARK: - Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var proceedButton: CustomButton!
	
	// MARK: - Variables
	var baidDataModel: LoginViewModel?
	var bingeAccountList: [AccountDetailListResponseData]?
    var switchAccountModel: BASwitchAccountViewModal?
    var verificationDataModel: LoginViewModel?
    var signOutVM: BASignOutViewModal?
    var userProfileVM: BAUserProfileViewModal?
    var subscriptionVM: PackSubscriptionVM?
    var subscriberDetailModal: SubscriberDetailResponseData?
    var loginVerificationCell: LoginVerificationInputTableViewCell?
    var selectedIndex: Int?
    var isFromAccount = false
    var isFromRMN = false
    var aliasName = ""
    var currentBaiD = ""
    var preSelectedIndex = -1
	
	// MARK: - Life Cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		configureUI()
		setupTableView()
        if isFromRMN {
            loadData()
        } else {
            loadSIDSelectionData()
        }
		
	}
	
	// MARK: - Functions
	func configureUI() {
		super.configureNavigationBar(with: .backButton, #selector(backAction), nil, self)
		self.view.backgroundColor = .BAdarkBlueBackground
		tableView.backgroundColor = .BAdarkBlueBackground
		proceedButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
		proceedButton.style(.tertiary, isActive: false)
	}
	
	func setupTableView() {
		tableView.dataSource = self
		tableView.delegate = self
		tableView.register(UINib(nibName: kIdSelectionTableViewCell, bundle: nil), forCellReuseIdentifier: kIdSelectionTableViewCell)
		tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
		tableView.register(UINib(nibName: kTitleHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: kTitleHeaderTableViewCell)
	}
	
	func loadData() {
        if isFromAccount {
            if self.switchAccountModel == nil {
                setSelectedRadioButton()
                self.switchAccountModel = BASwitchAccountViewModal(repo: BASwitchAccountRepo())
            }
        } else {
            
        }
	}
    
    
    func setSelectedRadioButton() {
        let index = bingeAccountList?.firstIndex(where: { (data) -> Bool in
            return data.baId == Int(BAKeychainManager().baId)
        })
        if let idx = index {
            selectedIndex = idx
            preSelectedIndex = idx
        }
    }
    
    func loadSIDSelectionData() {
        if let viewModel = baidDataModel {
            if let sidList = viewModel.sidLookUpModel {
                // Check to add selected SID is now removed in migration task and now whole list will be added.
                //                for sid in sidList where String(sid.subscriberId ?? 0) == BAUserDefaultManager().sId {
                //                    if let baidList = sid.accountDetailList {
                //                        bingeAccountList = baidList
                //                    }
                //                }
                for itemList in sidList {
                    if let item = itemList.accountDetailList {
                        bingeAccountList = item
                    }
                }
            }
        }
    }
	
	// MARK: - Actions
	@objc func backAction() {
		self.navigationController?.popViewController(animated: true)
        if isFromAccount {
            // Do Nothing
        } else {
            UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.aliasName.rawValue)
            UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.baId.rawValue)
        }
		
	}
	
	@IBAction func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if isFromAccount {
                switchAccount()
            } else {
                if let viewModel = baidDataModel, let baidlist = bingeAccountList {
                    if let idx = selectedIndex {
                        let item = baidlist[idx]
                        let baId = String(item.baId ?? 0)
                        if baId == "0" {
                            self.createBingeUser()
                        } else {
                            viewModel.dataModel.sid = BAKeychainManager().sId
                            viewModel.dataModel.baid = baId
                            self.showActivityIndicator(isUserInteractionEnabled: false)
                            viewModel.initiateLogin(completion: { (flagValue, message, isApiError) in
                                self.hideActivityIndicator()
                                if flagValue {
                                    /*
                                    if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" && BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                                        self.inactiveDTHAlert("You need to Activate DTH and Binge to continue watching.", title: kTempSuspendedHeader) {
                                            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                self.fetchSubscriptionDetails()
                                            } else {
                                                kAppDelegate.createHomeTabRootView()
                                            }
                                        }
                                    } else if (BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunned || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunnedState) {
                                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                                DispatchQueue.main.async { [weak self] in
                                                    AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                        if isOkay {
                                                            self?.signOut()
                                                        }
                                                    }
                                                }
                                            } else {
                                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                                    self?.fetchSubscriptionDetails()
                                                                } else {
                                                                    kAppDelegate.createHomeTabRootView()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            DispatchQueue.main.async { [weak self] in
                                                AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                    if isOkay {
                                                        self?.signOut()
                                                    }
                                                }
                                            }
                                        }
                                    } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" {
                                        self.inactiveDTHAlert(kDTHInactive, title: kTempSuspendedHeader) {
                                            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                self.fetchSubscriptionDetails()
                                            } else {
                                                kAppDelegate.createHomeTabRootView()
                                            }
                                        }
                                    } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "TEMP_SUSPENSION" || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
                                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                                DispatchQueue.main.async { [weak self] in
                                                    AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                        if isOkay {
                                                            self?.signOut()
                                                        }
                                                    }
                                                }
                                            } else {
                                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.doubleButton(.ok, .skip, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            } else {
                                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                                    self?.fetchSubscriptionDetails()
                                                                } else {
                                                                    kAppDelegate.createHomeTabRootView()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            DispatchQueue.main.async { [weak self] in
                                                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                    if isOkay {
                                                        self?.signOut()
                                                    }
                                                }
                                            }
                                        }
                                    } else if BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                                        self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                self.fetchSubscriptionDetails()
                                            } else {
                                                kAppDelegate.createHomeTabRootView()
                                            }
                                        }
                                    } else if (BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState) {
                                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                                DispatchQueue.main.async { [weak self] in
                                                    AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                        if isOkay {
                                                            self?.signOut()
                                                        }
                                                    }
                                                }
                                            } else {
                                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                self?.signOut()
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    DispatchQueue.main.async { [weak self] in
                                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                            if isOkay {
                                                                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                                    self?.fetchSubscriptionDetails()
                                                                } else {
                                                                    kAppDelegate.createHomeTabRootView()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            DispatchQueue.main.async { [weak self] in
                                                AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                    if isOkay {
                                                        self?.signOut()
                                                    }
                                                }
                                            }
                                        }
                                    }  else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                           self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                self.fetchSubscriptionDetails()
                                            } else {
                                                kAppDelegate.createHomeTabRootView()
                                            }
                                        }
                                    } else {
                                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                            self.fetchSubscriptionDetails()
                                        } else {
                                            kAppDelegate.createHomeTabRootView()
                                        }
                                    }*/
                                    self.showInactiveAlerts()
                                    //Only one Sid and One BaId found
                                } else if isApiError {
                                    self.apiError(message ?? "Error logging in.", title: kSomethingWentWrong)
                                } else {
                                    self.apiError(message ?? "Error logging in.", title: "")
                                }
                            })
                        }
                    }
                }
            }
        } else {
            noInternet()
        }
	}
    
    func showInactiveAlerts() {
        if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kDeActive || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kInactiveState {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
                self.alertWithInactiveTitleAndMessage(kBingeSubscriptionMBRDeactiveMessage, title: kDTHInactiveLowBalanceHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHDunnedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspended || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHTempSuspendedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else {
            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                self.fetchSubscriptionDetails()
            } else {
                kAppDelegate.createHomeTabRootView()
            }
        }
    }
    
    func switchAccount() {
        if let switchAccountVM = switchAccountModel {
            showActivityIndicator(isUserInteractionEnabled: false)
            if self.bingeAccountList?[selectedIndex ?? 0].baId == nil {
                BAKeychainManager().targetBaId = "0"
                BAKeychainManager().dsn = self.bingeAccountList?[selectedIndex ?? 0].deviceSerialNumber ?? "0"
            } else {
                BAKeychainManager().targetBaId = String(self.bingeAccountList?[selectedIndex ?? 0].baId ?? 0)
                BAKeychainManager().dsn = "0"
            }
            switchAccountVM.switchAccount(completion: { (flag, message, isApiError) in
                self.hideActivityIndicator()
                if flag {
                    BAKeychainManager().baId = BAKeychainManager().targetBaId
                    BAKeychainManager().aliasName = self.aliasName
                    BAKeychainManager().customerIndex = "\(self.selectedIndex ?? 0)"
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.switchProfile.rawValue, properties: [MixpanelConstants.ParamName.toBaid.rawValue: BAKeychainManager().baId])
                    kAppDelegate.createHomeTabRootView()
//                    self.dismissController(index: self.selectedIndex)
                } else if isApiError {
                    BAKeychainManager().baId = self.currentBaiD
                    //BAUserDefaultManager().aliasName = self.aliasName
                    self.apiError(message ?? "Failed to select profile", title: kSomethingWentWrong)
                } else {
                    BAKeychainManager().baId = self.currentBaiD
                    //BAUserDefaultManager().aliasName = self.aliasName
                    self.apiError(message ?? "Failed to select profile", title: "")
                }
            })
        }
    }
    
    func dismissController(index: Int?) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AccountVC.self) {
                var userProfile = [String: String]()
                let _index = index ?? 0
                userProfile["name"] = bingeAccountList?[_index].aliasName ?? ""
                userProfile["aliasName"] = bingeAccountList?[_index].aliasName ?? ""
                userProfile["baId"] = String(bingeAccountList?[_index].baId ?? 0)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfile"), object: nil, userInfo: userProfile)
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func createBingeUser() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = verificationDataModel {
                showActivityIndicator(isUserInteractionEnabled: false)
                viewModel.createBingeUser(completion: { (flagValue, message, statusCode, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if (BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunned || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunnedState) {
                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                    DispatchQueue.main.async { [weak self] in
                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                            if isOkay {
                                                self?.signOut()
                                            }
                                        }
                                    }
                                } else {
                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                        DispatchQueue.main.async { [weak self] in
                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                if isOkay {
                                                    self?.signOut()
                                                }
                                            }
                                        }
                                    } else {
                                        DispatchQueue.main.async { [weak self] in
                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                if isOkay {
                                                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                        self?.fetchSubscriptionDetails()
                                                    } else {
                                                        kAppDelegate.createHomeTabRootView()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                DispatchQueue.main.async { [weak self] in
                                    AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                        if isOkay {
                                            self?.signOut()
                                        }
                                    }
                                }
                            }
                        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" {
                            self.inactiveDTHAlert("You need to Activate DTH", title: "Activate DTH") {
                                self.fetchSubscriptionDetails()
                            }
                        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "TEMP_SUSPENSION" || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                    DispatchQueue.main.async { [weak self] in
                                        AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                            if isOkay {
                                                self?.signOut()
                                            }
                                        }
                                    }
                                } else {
                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                        DispatchQueue.main.async { [weak self] in
                                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                if isOkay {
                                                    self?.signOut()
                                                }
                                            }
                                        }
                                    } else {
                                        DispatchQueue.main.async { [weak self] in
                                            AlertController.initialization().showAlert(.doubleButton(.ok, .skip, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                if isOkay {
                                                    self?.signOut()
                                                } else {
                                                    self?.fetchSubscriptionDetails()
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                DispatchQueue.main.async { [weak self] in
                                    AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                        if isOkay {
                                            self?.signOut()
                                        }
                                    }
                                }
                            }
                        } else if BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                            self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                self.fetchSubscriptionDetails()
                            }
                        } else if (BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState) {
                         if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                             if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                 DispatchQueue.main.async { [weak self] in
                                     AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                         if isOkay {
                                             self?.signOut()
                                         }
                                     }
                                 }
                             } else {
                                 if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                     DispatchQueue.main.async { [weak self] in
                                         AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                             if isOkay {
                                                 self?.signOut()
                                             }
                                         }
                                     }
                                 } else {
                                     DispatchQueue.main.async { [weak self] in
                                         AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                             if isOkay {
                                                self?.fetchSubscriptionDetails()
                                             }
                                         }
                                     }
                                 }
                             }
                         } else {
                             DispatchQueue.main.async { [weak self] in
                                 AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                     if isOkay {
                                      self?.signOut()
                                     }
                                 }
                             }
                           }
                        }  else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                self.fetchSubscriptionDetails()
                            }
                        } else {
                            self.fetchSubscriptionDetails()
                        }
                        kAppDelegate.createHomeTabRootView()
                    } else if isApiError {
                        self.apiError(message ?? "Failed to update email id", title: "")
                    } else {
                        if let status = statusCode {
                            if status == 40016 || status == 80001 || status == 20003 || status == 80002 || status == 3006 {
                                self.apiError(message ?? "Failed to update email id", title: "") {
                                    if let status_Code = statusCode {
                                        if status_Code == 40016 || status_Code == 80001 {
                                            UtilityFunction.shared.logoutUser()
                                            kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                                            return
                                        }
                                    }
                                }
                            } else {
                                print("In 80002 block")
                            }
                        } else {
                            
                        }
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    func signOut() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            signOutVM = BASignOutViewModal(repo: BASignOutRepo())
            if let signOutVM = signOutVM {
                signOutVM.signOut {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        BAKeychainManager().isFSPopUpShown = false
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func fetchSubscriptionDetails() {
        subscriptionVM = PackSubscriptionVM()
        getSubscription()
    }
    
    func getSubscription() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if subscriptionVM != nil {
                subscriptionVM?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                            kAppDelegate.createHomeTabRootView()
                        } else {
                            let subscriptionVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.subscription.rawValue, type: MySubscriptionVC.self)
                            subscriptionVC.subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                            subscriptionVC.isFromAccountScreen = false
                            self.navigationController?.pushViewController(subscriptionVC, animated: true)
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet() {
                self.getSubscription()
            }
        }
    }
}

extension BaIdSelectionVC: IdSelectionTableViewCellProtocol {
    func cellSelected(_ text: String, _ id: String, _ index: Int?) {
        if preSelectedIndex == index {
            proceedButton.style(.tertiary, isActive: false)
        } else {
            proceedButton.style(text.count > 0 ? .primary : .tertiary, isActive: text.count > 0)
        }
        
        if isFromAccount {
            aliasName = text
            currentBaiD = BAKeychainManager().baId
            BAKeychainManager().targetBaId = id
        } else {
            BAKeychainManager().aliasName = text
            BAKeychainManager().baId = id
        }
        selectedIndex =  index
        if let item = bingeAccountList, let idx = index {
            BAKeychainManager().dsn = item[idx].deviceSerialNumber ?? ""
        }
        tableView.reloadData()
    }
}


//
//  ExtensionBaIdSelectionVC+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 29/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension BaIdSelectionVC: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let count = bingeAccountList?.count ?? 0
		return count + 2
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
			cell?.configureIndicator(number: 4, selectedIndex: 1)
			return cell!
		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
			cell?.configureCell(source: .baIdSelection)
			return cell!
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: kIdSelectionTableViewCell) as? IdSelectionTableViewCell
//            cell?.selectionFlag = "\(bingeAccountList?[indexPath.row - 2].baId ?? 0)" == BAUserDefaultManager().baId
            cell?.selectionButton.tag = indexPath.row - 2
            cell?.selectedIndex = selectedIndex
            cell?.configureCellForBaId((bingeAccountList?[indexPath.row - 2])!, index: indexPath.row - 2)
			cell?.delegate = self
			return cell!

		}

	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

//
//  VerificationVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

//protocol LoginVerificationVCDelegate: class {
//    func checkForOTP(otpString: String?)
//}

class LoginVerificationVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var proceedButton: CustomButton!

    // MARK: - Variables
    var verificationDataModel: LoginViewModel?
    var userProfileVM: BAUserProfileViewModal?
    var signOutVM: BASignOutViewModal?
    var subscriptionVM: PackSubscriptionVM?
    var isSegmentSwitched = false
    var previousIndex = 0
    var subscriberDetailModal: SubscriberDetailResponseData?
    var loginVerificationCell: LoginVerificationInputTableViewCell?
    //weak var delegate: LoginVerificationVCDelegate?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setNavigationBar()
        reCheckVM()
        setUpTableView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: - Functions
    private func setNavigationBar() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
		switch verificationDataModel?.dataModel.loginType {
		case .sidWithOtp, .sidWithPwd:
			verificationDataModel?.dataModel.rmn = ""
		case .rmnWithOtp:
			verificationDataModel?.dataModel.sid = ""
		default: break
		}
    }

    private func configureUI() {
        self.view.backgroundColor = .BAdarkBlueBackground
        tableView.allowsSelection = false
		proceedButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
		proceedButton.style(.tertiary, isActive: false)
//        verificationDataModel?.dataModel.segmentIndex = 0
//        verificationDataModel?.dataModel.tfTag = 0
    }

    private func reCheckVM() {
        if self.verificationDataModel == nil {
            self.verificationDataModel = LoginViewModel()
        }
    }

    private func setUpTableView() {
        let cells = VerificationVcDataSource.CellType.typeOfCells
		tableView.delegate = self
		tableView.dataSource = self
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension

        for item in cells {
            UtilityFunction.shared.registerCell(tableView, item.cellIndentifier)
        }
    }

    func showResendCountAlert() {
        apiError(AppStringConstant.otpResendLimit.rawValue, title: "")
		//showOkAlert(AppStringConstant.otpResendLimit.rawValue)
    }
    
    func showInactiveAlerts() {
        if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kDeActive || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kInactiveState {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
                self.alertWithInactiveTitleAndMessage(kBingeSubscriptionMBRDeactiveMessage, title: kDTHInactiveLowBalanceHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHDunnedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspended || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHTempSuspendedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else {
            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                self.fetchSubscriptionDetails()
            } else {
                kAppDelegate.createHomeTabRootView()
            }
        }
    }

    func verifyLogin(cell: LoginVerificationInputTableViewCell) {
        if let viewModel = verificationDataModel {
            showActivityIndicator(isUserInteractionEnabled: false)
			viewModel.loginValidationCall { (flagValue, message, isApiError) in
                self.hideActivityIndicator()
                cell.errorInfoLabel.isHidden = true
                if flagValue {
                    //BAUserDefaultManager().isAutoPlayOn = true
                    //MixpanelManager.shared.saveMixPanelDistinctID()
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginSuccess.rawValue, properties: [MixpanelConstants.ParamName.type.rawValue: viewModel.loginType, MixpanelConstants.ParamName.auth.rawValue: viewModel.authType, MixpanelConstants.ParamName.value.rawValue: viewModel.valueType])
                    if viewModel.loginType == "RMN" {
                        BAKeychainManager().mobileNumberForSuperProperty = viewModel.valueType
                    }
                    if message == "HOME" {
                        self.showInactiveAlerts()
                        //Only one Sid and One BaId found
                    } else if message == "SID" {
						let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "SidSelectionVC", type: SidSelectionVC.self)
						pushView.verificationDataModel = viewModel
						self.navigationController?.pushViewController(pushView, animated: true)
					} else if message == "BAID" {
						let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BaIdSelectionVC", type: BaIdSelectionVC.self)
						pushView.baidDataModel = viewModel
                        pushView.isFromAccount = false
                        pushView.isFromRMN = false
                        pushView.verificationDataModel = self.verificationDataModel
                        BAKeychainManager().sId = String(viewModel.sidLookUpModel?.first?.subscriberId ?? 0)
						self.navigationController?.pushViewController(pushView, animated: true)
					} else if message == "CREATE" {
                        if let subscriberModal = viewModel.sidLookUpModel {
                            for subscriber in subscriberModal {
                                self.subscriberDetailModal = subscriber
                            }
                        }
                        BAKeychainManager().sId = "\(self.subscriberDetailModal?.subscriberId ?? 0)"
						let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "CreateBingeAccountVC", type: CreateBingeAccountVC.self)
						pushView.createAccountDataModel = viewModel
                        pushView.createDataModel = self.subscriberDetailModal
						self.navigationController?.pushViewController(pushView, animated: true)
                    } else if message == "CREATEFIRESTICK" {
                        self.createBingeUser()
                    } else {
                        if viewModel.errorMessage.isEmpty {
                            self.showOkAlert(message ?? "")
                        } else {
                            self.apiError(message ?? "", title: kErrorHeader)
                        }
                    }
                } else if isApiError {
                    let mixPanelData = self.updateMixpanelEvent()
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginFailed.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.homeMain.rawValue, MixpanelConstants.ParamName.type.rawValue: mixPanelData.0, MixpanelConstants.ParamName.auth.rawValue: mixPanelData.1, MixpanelConstants.ParamName.reason.rawValue: message ?? "", MixpanelConstants.ParamName.value.rawValue: viewModel.valueType])
                        if message == "cancelled" {
                            // Do nothing
                        } else if viewModel.errorCode == 429 {
                            self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                        } else if viewModel.errorCode == 40041 {
                            self.apiError(message, title: kErrorHeader)
                        } else {
                            self.apiError(message, title: kSomethingWentWrong)
                        }
                } else {
                    let mixPanelData = self.updateMixpanelEvent()
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginFailed.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.homeMain.rawValue, MixpanelConstants.ParamName.type.rawValue: mixPanelData.0, MixpanelConstants.ParamName.auth.rawValue: mixPanelData.1, MixpanelConstants.ParamName.reason.rawValue: message ?? "", MixpanelConstants.ParamName.value.rawValue: viewModel.valueType])
                    if viewModel.errorCode == 60001 || viewModel.errorCode == 6021 || viewModel.errorCode == 6035 {
                        cell.textFieldHasError(true)
                        cell.errorInfoLabel.isHidden = false
                        cell.errorInfoLabel.text = message
                        return
                    } else if viewModel.errorCode == 80002 { // DTH code was confirmed from Srikant
                        self.apiError(message, title: kTempSuspendedHeader)
                        return
                    } else if viewModel.errorCode == 429 {
                        self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                    } else if viewModel.errorCode == 40041 {
                        self.apiError(message, title: kErrorHeader)
                    } else if message == "cancelled" {
                        // Do nothing
                    } else {
                        if viewModel.errorCode == 1 {
                            cell.textFieldHasError(true)
                            cell.errorInfoLabel.isHidden = false
                            cell.errorInfoLabel.text = message
                        } else {
                            self.apiError(message, title: "")
                        }
                    }
//                    else { //TODO: worst case scenario handeled this as doing the above handling on the last day of build to client
//                        if message == kInactiveDTH {
//                            self.apiError(message, title: "DTH Inactive")
//                        } else {
//                            if message == kIncorrectOTP || message == kIncorrectPassword {
//                                cell.textFieldHasError(true)
//                                cell.errorInfoLabel.isHidden = false
//                                cell.errorInfoLabel.text = message
//                            }
//                        }
//                    }
                }
            }
        }
    }
    
    func updateMixpanelEvent() -> (String, String) {
        var type = ""
        var auth = ""
        switch verificationDataModel?.dataModel.loginType {
        case .sidWithOtp:
            type = "SID"
            auth = "OTP"
        case .sidWithPwd:
            type = "SID"
            auth = "Password"
        case .rmnWithOtp:
            type = "RMN"
            auth = "OTP"
        default: break
        }
        return (type, auth)
    }
    
    func fetchSubscriptionDetails() {
        subscriptionVM = PackSubscriptionVM()
        getSubscription()
    }
    
    func getSubscription() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if subscriptionVM != nil {
                subscriptionVM?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                            kAppDelegate.createHomeTabRootView()
                        } else {
                            let subscriptionVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.subscription.rawValue, type: MySubscriptionVC.self)
                            subscriptionVC.subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                            subscriptionVC.isFromAccountScreen = false
                            self.navigationController?.pushViewController(subscriptionVC, animated: true)
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet() {
                self.getSubscription()
            }
        }
    }
    
    func updateForgotPasswordModel() -> UpdatePasswordDataModel {
        var dataModel = UpdatePasswordDataModel()
        dataModel.headerLogoText = """
        Enter
        Verification Code
        """
        dataModel.subheaderLogoText = "& Create a New Tata Sky Binge Password"
        dataModel.frstLabelDescText = "Enter OTP"
		dataModel.createPassword = "New Password"
        dataModel.rmn = (self.verificationDataModel?.dataModel.rmn ?? "")
		dataModel.sid = (self.verificationDataModel?.dataModel.sid ?? "")
        dataModel.frstLabelPlaceHolderText = "Enter OTP"
        dataModel.buttonText = "Update"
        return dataModel
    }
	
	@IBAction func nextButtonTapped() {
		if BAReachAbility.isConnectedToNetwork() {
			if let viewModel = verificationDataModel {
				viewModel.clientEndValidation(verificationDataModel?.dataModel.loginType ?? .rmnWithOtp, completion: { (flagValue) in
					if flagValue {
						MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginOtpEnter.rawValue)
                        self.verifyLogin(cell: self.loginVerificationCell!)
					} else {
						NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowOtpAlertForLogin"), object: nil)
						self.view.endEditing(true)
					}
				})
			}
		} else {
			noInternet()
		}
	}
    
    func signOut() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            signOutVM = BASignOutViewModal(repo: BASignOutRepo())
            if let signOutVM = signOutVM {
                signOutVM.signOut {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        BAKeychainManager().isFSPopUpShown = false
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }

}

// MARK: - Extention
extension LoginVerificationVC: SegmentSelectedProtocol, VerificationTextFieldTableViewCellProtocol, SignUpButtonActionDelegate, ForgetButtonActionDelegate, ResendOtpButtonActionDelegate {

    func valueUpdated(_ idx: Int, _ text: String) {
        switch idx {
        case 0:
            verificationDataModel?.dataModel.otp = text
			proceedButton.style(text.count == 6 ? .primary : .tertiary, isActive: text.count == 6)
        default:
            verificationDataModel?.dataModel.password = text
			proceedButton.style(text.count >= 8 ? .primary : .tertiary, isActive: text.count >= 8)
        }
    }
    
    func segmentSwitched(_ index: Int) {
        self.view.endEditing(true)
        verificationDataModel?.dataModel.segmentIndex = index
        if verificationDataModel?.dataModel.tfTag == 0 {
            verificationDataModel?.dataModel.loginType = .rmnWithOtp
        } else if verificationDataModel?.dataModel.tfTag == 1 {
            if index == 0 {
                if index == previousIndex {
                    // Do Nothing
                } else {
                    verificationDataModel?.dataModel.loginType = .sidWithOtp
                    let text = verificationDataModel?.dataModel.otp ?? ""
                    if isSegmentSwitched == false {
                        isSegmentSwitched = true
                        generateOtp()
                    }
                    proceedButton.style(text.count >= 6 ? .primary : .tertiary, isActive: text.count >= 6)
                }
                
            } else {
                if index == previousIndex {
                    
                } else {
                    isSegmentSwitched = true
                    verificationDataModel?.dataModel.loginType = .sidWithPwd
                    let text = verificationDataModel?.dataModel.password ?? ""
                    proceedButton.style(text.count >= 8 ? .primary : .tertiary, isActive: text.count >= 8)
                }
            }
        }
        print("Selected segment is ", index)
        //loginVerificationCell?.updateCellForSegment()
        if index == previousIndex {
            // Do not reload table
        } else {
            tableView.reloadRows(at: [IndexPath(item: 3, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 4, section: 0)], with: .none)
        }
        previousIndex = index
        //tableView.isUserInteractionEnabled = true
    }
    
    func generateOtp() {
        if let vmModel = verificationDataModel {
            vmModel.generateOtp { (flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    //self.moveToVerificationScreen()
                } else if isApiError {
                    if message == "cancelled" {
                        // Do nothing
                    } else if vmModel.errorCode == 429 {
                        self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                    } else {
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                } else {
                    let cell = self.tableView.cellForRow(at: IndexPath(row: vmModel.dataModel.loginType == .rmnWithOtp ? 3 : 5, section: 0)) as? LoginTextFieldTableViewCell
                    cell?.textFieldHasError(true, with: vmModel.dataModel.loginType == .rmnWithOtp ? "" : "", errorMessage: message ?? "Error")

                }
            }
        }
    }

    func forgetButtonTapped() {
        sendOTPCode()
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.forgotPassword.rawValue, properties: [MixpanelConstants.ParamName.sid.rawValue: self.verificationDataModel?.dataModel.sid ?? ""])
        /*
        if BAReachAbility.isConnectedToNetwork() {
            let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BAResetPasswordViewController", type: BAResetPasswordViewController.self)
//            pushView.subscriberId = verificationDataModel?.dataModel.sid
//            pushView.rmn = verificationDataModel?.dataModel.rmn
            pushView.verificationDataModel = verificationDataModel
            //pushView.verificationDataModel?.dataModel.loginType = verificationDataModel?.dataModel.loginType
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.forgotPassword.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
            self.navigationController?.pushViewController(pushView, animated: true)
        } else {
            noInternet()
        }
        */
    }
    
    func sendOTPCode() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let viewModel = verificationDataModel {
                viewModel.forgetPassword(isFromLogin: true, completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BAResetPasswordViewController", type: BAResetPasswordViewController.self)
                        //            pushView.subscriberId = verificationDataModel?.dataModel.sid
                        //            pushView.rmn = verificationDataModel?.dataModel.rmn
                        pushView.verificationDataModel = self.verificationDataModel
                        //pushView.verificationDataModel?.dataModel.loginType = verificationDataModel?.dataModel.loginType
                        //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.forgotPassword.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
                        self.navigationController?.pushViewController(pushView, animated: true)
                    } else if isApiError {
                        if viewModel.errorCode == 429 {
                            self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                        } else {
                           self.apiError(message, title: kSomethingWentWrong)
                        }
                    } else {
                        if viewModel.errorCode == 429 {
                            self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                        } else {
                           self.apiError(message, title: "")
                        }
                    }
                })
            }
        } else {
            noInternet()
        }
    }

    func resendButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginOtpResend.rawValue)
            if let viewModel = verificationDataModel {
                showActivityIndicator(isUserInteractionEnabled: false)
                viewModel.reSendOtp { (flagValue, message, isAppiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.otpResent(message ?? "", completion: nil)
					} else if isAppiError {
                        if viewModel.errorCode == 429 {
                            self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                        } else {
                           self.apiError(message ?? "Failed to resend OTP", title: kSomethingWentWrong)
                        }
                    } else {
                        if viewModel.errorCode == 429 {
                            self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                        } else {
                           self.apiError(message ?? "Failed to resend OTP", title: "")
                        }
                    }
                }
            }
        } else {
			noInternet()
        }
    }
    
    func createBingeUser() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = verificationDataModel {
                showActivityIndicator(isUserInteractionEnabled: false)
                viewModel.createBingeUser(completion: { (flagValue, message, statusCode, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.showInactiveAlerts()
                    } else if isApiError {
                        if viewModel.errorCode == 429 {
                            self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                        } else {
                           self.apiError(message ?? "Failed to update email id", title: "")
                        }
                    } else {
                        if let status = statusCode {
                            if status == 40016 || status == 80001 || status == 20003 || status == 80002 {
                                self.apiError(message ?? "Failed to update email id", title: "") {
                                    if let status_Code = statusCode {
                                        if status_Code == 40016 || status_Code == 80001 {
                                            UtilityFunction.shared.logoutUser()
                                            kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                                            return
                                        }
                                    }
                                }
                            } else {
                                if status == 429 {
                                    self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                                } else {
                                   self.apiError(message, title: kSomethingWentWrong)
                                   print("in 80002 block")
                                }
                            }
                        } else {
                            
                        }
                    }
                })
            }
        } else {
            noInternet()
        }
    }

    func signUpButtonTapped() {
        super.moveToSignUpScreen()
    }
}

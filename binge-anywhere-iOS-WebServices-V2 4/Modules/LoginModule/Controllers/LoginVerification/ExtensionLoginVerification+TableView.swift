//
//  ExtensionVerification+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 19/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

// MARK: - Extention
extension LoginVerificationVC: UITableViewDataSource, UITableViewDelegate {

    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VerificationVcDataSource.CellType.typeOfCells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
			cell?.configureIndicator(number: 4, selectedIndex: 0)
			return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
            cell?.configureCell(source: .otp)
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoginSegmentTableViewCell") as? LoginSegmentTableViewCell
            cell?.delegate = self
            cell?.updateSegment(index: verificationDataModel?.dataModel.segmentIndex ?? 0)
			cell?.isEnabled(verificationDataModel?.dataModel.loginType != .rmnWithOtp)
            cell?.segmentView.selectorColor = .BABlueColor
            return cell!
        case 3:
            tableView.isUserInteractionEnabled = false
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoginVerificationInputTableViewCell") as? LoginVerificationInputTableViewCell
            loginVerificationCell = cell! 
            cell?.delegate = self
            cell?.inputTextField.tag = verificationDataModel?.dataModel.loginType == .sidWithPwd ? 1 : 0
            cell?.updateCellForSegment()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                self.tableView.isUserInteractionEnabled = true
            }
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ForgetPasswordTableViewCell") as? ForgetPasswordTableViewCell
			switch verificationDataModel?.dataModel.loginType {
			case .rmnWithOtp, .sidWithOtp:
				let rmn = verificationDataModel?.dataModel.maskedNumberToShow ?? ""
                cell?.mobileInfotext = "The OTP is sent to +91 " + rmn
//                 commented this line as now masked number is coming from server
//                let rmn = verificationDataModel?.dataModel.rmn ?? ""
//				cell?.mobileInfotext = "Enter the code sent to +91 " + rmn.replaceFrom(startIndex: 0, endIndex: 4, With: "XXXXX")
				cell?.configureCell(source: .otp)
                cell?.mobileLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size:  14), color: .BAwhite)
            case .sidWithPwd:
                cell?.configureCell(source: .login)
			default:
				cell?.configureCell(source: .login)
			}
            cell?.delegate = self
            return cell!
		case 5:
			let cell = tableView.dequeueReusableCell(withIdentifier: "ResendOtpTableViewCell") as? ResendOtpTableViewCell
			cell?.delegate = self
			cell?.clipsToBounds = true
			return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 {
			let cell = tableView.cellForRow(at: indexPath)
			cell?.isHiddenAnimated(value: verificationDataModel?.dataModel.segmentIndex == 1)
			return verificationDataModel?.dataModel.segmentIndex == 1 ? 0.01 : UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

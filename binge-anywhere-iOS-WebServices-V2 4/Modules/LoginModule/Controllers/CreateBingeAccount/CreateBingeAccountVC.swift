//
//  CreateBingeAccountVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 30/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class CreateBingeAccountVC: BaseViewController {

	// MARK: - Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var proceedButton: CustomButton!
	
	// MARK: - Variables
	var createAccountDataModel: LoginViewModel?
    var createDataModel: SubscriberDetailResponseData?
    var signOutVM: BASignOutViewModal?
    var subscriptionVM: PackSubscriptionVM?
    var loginTextFieldCell: LoginTextFieldTableViewCell?
	// MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
		setupTableView()
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUI()
    }
	
	func configureUI() {
		super.configureNavigationBar(with: .backButton, #selector(backAction), nil, self)
		self.view.backgroundColor = .BAdarkBlueBackground
		tableView.backgroundColor = .BAdarkBlueBackground
		proceedButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        if createDataModel?.emailId == nil || createDataModel?.emailId?.isEmpty ?? false {
            proceedButton.style(.tertiary, isActive: false)
        } else {
            proceedButton.style(.primary, isActive: true)
        }
	}
	
	func setupTableView() {
		tableView.dataSource = self
		tableView.delegate = self
		tableView.register(UINib(nibName: kShowDataTableViewCell, bundle: nil), forCellReuseIdentifier: kShowDataTableViewCell)
		tableView.register(UINib(nibName: kLoginTextFieldTableViewCell, bundle: nil), forCellReuseIdentifier: kLoginTextFieldTableViewCell)
		tableView.register(UINib(nibName: kSignUpAlreadyUserTableViewCell, bundle: nil), forCellReuseIdentifier: kSignUpAlreadyUserTableViewCell)
		tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
		tableView.register(UINib(nibName: kTitleHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: kTitleHeaderTableViewCell)
	}
    
    // MARK: - IBActions
	@objc func backAction() {
		self.navigationController?.popViewController(animated: true)
	}
	
    @IBAction func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = createAccountDataModel {
                showActivityIndicator(isUserInteractionEnabled: false)
                       viewModel.createBingeUser(completion: { (flagValue, message, statusCode, isApiError) in
                           self.loginTextFieldCell?.errorInfoLabel.isHidden = true
                           self.hideActivityIndicator()
                           if flagValue {
                            self.showInactiveAlerts()
                               /*
                               if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" && BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                                   self.inactiveDTHAlert("You need to Activate DTH and Binge to continue watching.", title: kTempSuspendedHeader) {
                                       if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                           self.fetchSubscriptionDetails()
                                       } else {
                                           kAppDelegate.createHomeTabRootView()
                                       }
                                   }
                               } else if (BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunned || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kPartialDunnedState) {
                                   if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                       if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                           DispatchQueue.main.async { [weak self] in
                                               AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                   if isOkay {
                                                       self?.signOut()
                                                   }
                                               }
                                           }
                                       } else {
                                           if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                               DispatchQueue.main.async { [weak self] in
                                                   AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                       if isOkay {
                                                           self?.signOut()
                                                       }
                                                   }
                                               }
                                           } else {
                                               DispatchQueue.main.async { [weak self] in
                                                   AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                       if isOkay {
                                                           if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                               self?.fetchSubscriptionDetails()
                                                           } else {
                                                               kAppDelegate.createHomeTabRootView()
                                                           }
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   } else {
                                       DispatchQueue.main.async { [weak self] in
                                           AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kDTHInactiveHeader), .withMessage(kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                               if isOkay {
                                                self?.signOut()
                                               }
                                           }
                                       }
                                   }
                               } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "DEACTIVATED" {
                                   self.inactiveDTHAlert("You need to Activate DTH", title: kTempSuspendedHeader) {
                                       if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                           self.fetchSubscriptionDetails()
                                       } else {
                                           kAppDelegate.createHomeTabRootView()
                                       }
                                   }
                               } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == "TEMP_SUSPENSION" || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
                                   if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                       if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                           DispatchQueue.main.async { [weak self] in
                                               AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                   if isOkay {
                                                       self?.signOut()
                                                   }
                                               }
                                           }
                                       } else {
                                           if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                               DispatchQueue.main.async { [weak self] in
                                                   AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                       if isOkay {
                                                           self?.signOut()
                                                       }
                                                   }
                                               }
                                           } else {
                                               DispatchQueue.main.async { [weak self] in
                                                   AlertController.initialization().showAlert(.doubleButton(.ok, .skip, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                                       if isOkay {
                                                           self?.signOut()
                                                       } else {
                                                           self?.fetchSubscriptionDetails()
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   } else {
                                    DispatchQueue.main.async { [weak self] in
                                        AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kTempSuspendedMessage), .hidden, .hidden)) { (isOkay) in
                                            if isOkay {
                                                self?.signOut()
                                            }
                                        }
                                    }
                                }
                               } else if BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
                                   self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                       if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                           self.fetchSubscriptionDetails()
                                       } else {
                                           kAppDelegate.createHomeTabRootView()
                                       }
                                   }
                               } else if (BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState) {
                                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated ?? false {
                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packType == "FREE" {
                                        DispatchQueue.main.async { [weak self] in
                                            AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                if isOkay {
                                                    self?.signOut()
                                                }
                                            }
                                        }
                                    } else {
                                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                            DispatchQueue.main.async { [weak self] in
                                                AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                    if isOkay {
                                                        self?.signOut()
                                                    }
                                                }
                                            }
                                        } else {
                                            DispatchQueue.main.async { [weak self] in
                                                AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                                    if isOkay {
                                                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                                            self?.fetchSubscriptionDetails()
                                                        } else {
                                                            kAppDelegate.createHomeTabRootView()
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async { [weak self] in
                                        AlertController.initialization().showAlert(.singleButton(.cancel, .withTopImage(.alert), .withTitle(kTempSuspendedHeader), .withMessage(BAKeychainManager().acccountDetail?.message ?? kPartiallyDunnedMessage), .hidden, .hidden)) { (isOkay) in
                                            if isOkay {
                                             self?.signOut()
                                            }
                                        }
                                    }
                                }
                               } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                                    self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) {
                                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                        self.fetchSubscriptionDetails()
                                    } else {
                                        kAppDelegate.createHomeTabRootView()
                                    }
                                }
                               } else {
                                   if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                                       self.fetchSubscriptionDetails()
                                   } else {
                                       kAppDelegate.createHomeTabRootView()
                                   }
                               }*/
                               //kAppDelegate.createHomeTabRootView()
                           } else if isApiError {
                               self.apiError(message ?? "Failed to update email id", title: "")
                           } else {
                               if let status = statusCode {
                                   if status == 40016 || status == 80001 || status == 20003 || status == 80002 || status == 40013 {
                                       self.apiError(message ?? "Failed to update email id", title: "") {
                                           if let status_Code = statusCode {
                                            if status_Code == 40016 || status_Code == 80001 {
                                                UtilityFunction.shared.logoutUser()
                                                kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                                                return
                                            }
                                           }
                                       }
                                   } else {
                                       self.loginTextFieldCell?.errorInfoLabel.isHidden = false
                                       self.loginTextFieldCell?.errorInfoLabel.text = "Failed to update email id"
                                   }
                               } else {
                                   self.loginTextFieldCell?.errorInfoLabel.isHidden = false
                                   self.loginTextFieldCell?.errorInfoLabel.text = "Failed to update email id"
                               }
                           }
                       })
                   }
        } else {
            noInternet()
        }
    }
    
    func showInactiveAlerts() {
        if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kDeActive || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kInactiveState {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
                self.alertWithInactiveTitleAndMessage(kBingeSubscriptionMBRDeactiveMessage, title: kDTHInactiveLowBalanceHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunnedState || BAKeychainManager().acccountDetail?.accountSubStatus == kPartialDunned {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHDunnedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspended || BAKeychainManager().acccountDetail?.subscriberAccountStatus == kTempSuspension {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
                    self.alertWithInactiveTitleAndMessage(kDTHTempSuspendedMBRPaidActiveMessage, title: kAlert) {
                        if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                            self.fetchSubscriptionDetails()
                        } else {
                            kAppDelegate.createHomeTabRootView()
                        }
                    }
                } else {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                        self.fetchSubscriptionDetails()
                    } else {
                        kAppDelegate.createHomeTabRootView()
                    }
                }
            } else {
                if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                    self.fetchSubscriptionDetails()
                } else {
                    kAppDelegate.createHomeTabRootView()
                }
            }
        } else {
            if  BAKeychainManager().acccountDetail?.firstTimeLogin ?? false {
                self.fetchSubscriptionDetails()
            } else {
                kAppDelegate.createHomeTabRootView()
            }
        }
    }
    
    func signOut() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            signOutVM = BASignOutViewModal(repo: BASignOutRepo())
            if let signOutVM = signOutVM {
                signOutVM.signOut {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        BAKeychainManager().isFSPopUpShown = false
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func fetchSubscriptionDetails() {
        subscriptionVM = PackSubscriptionVM()
        getSubscription()
    }
    
    func getSubscription() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: true)
            if subscriptionVM != nil {
                subscriptionVM?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        let subscriptionVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.subscription.rawValue, type: MySubscriptionVC.self)
                        subscriptionVC.subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                        subscriptionVC.isFromAccountScreen = false
                        self.navigationController?.pushViewController(subscriptionVC, animated: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet() {
                self.getSubscription()
            }
        }
    }
}

extension CreateBingeAccountVC: LoginTextFieldTableViewCellProtocol {
    func notifyValidEmail() {
        proceedButton.style(.tertiary, isActive: false)
    }
    
    func valueUpdated(_ idx: Int, _ text: String) {
        if let viewModel = createAccountDataModel {
            viewModel.dataModel.email = text
        }
        proceedButton.style(text.count > 0 ? .primary : .tertiary, isActive: text.count > 0)
    }
    
    
}

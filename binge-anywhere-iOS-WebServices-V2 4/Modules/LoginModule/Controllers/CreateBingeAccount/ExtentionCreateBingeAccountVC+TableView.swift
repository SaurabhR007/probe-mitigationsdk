//
//  ExtentionCreateBingeAccountVC+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 30/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension CreateBingeAccountVC: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 5
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
			cell?.configureIndicator(number: 4, selectedIndex: 2)
			return cell!
		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
			cell?.configureCell(source: .updateDetails)
			return cell!
		case 2, 3:
			let cell = tableView.dequeueReusableCell(withIdentifier: kShowDataTableViewCell) as? ShowDataTableViewCell
			if let sidModel = createAccountDataModel?.sidLookUpModel {
				for sidList in sidModel /*where String(sidList.subscriberId ?? 0) == BAUserDefaultManager().sId*/ {
					cell?.titleText = indexPath.row == 2 ? "Name" : "Registered Mobile Number"
                    cell?.nameText = (indexPath.row == 2 ? sidList.subscriberName : "+91 \(sidList.rmn ?? "")") ?? ""
				}
			}
			cell?.configUI()
			return cell!
		case 4:
			let cell = tableView.dequeueReusableCell(withIdentifier: kLoginTextFieldTableViewCell) as? LoginTextFieldTableViewCell
			if let sidModel = createAccountDataModel?.sidLookUpModel {
                loginTextFieldCell = cell
                cell?.configureUIForEmail(email: "")
				//for sidList in sidModel/* where String(sidList.subscriberId ?? 0) == BAUserDefaultManager().sId*/ {
					cell?.configureUIForEmail(email: createDataModel?.emailId ?? "")
                createAccountDataModel?.dataModel.email = createDataModel?.emailId ?? ""
				//}
			}
			cell?.inputTextField.tag = 3
			cell?.delegate = self
			return cell!
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as? BottomBingeLogoTableViewCell
			return cell!
		}
	}
	
}

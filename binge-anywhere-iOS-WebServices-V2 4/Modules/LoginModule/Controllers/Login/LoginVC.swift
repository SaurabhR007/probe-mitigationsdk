//
//  LoginVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class LoginVC: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var proceedButton: CustomButton!
    @IBOutlet weak var notOnTataButton: CustomButton!
    var termsSelected = false
    // MARK: - Variables
    var loginViewModel: LoginViewModel?
    // HOTFIX variable taken to solve state issue due to class variable
    var selectedSegmentIndex = 0
    var loginType: LoginType?
    var previouslySelectedLoginType: LoginType?
    var selectedTermsForRmnWithOtp = false
    //------------------------//
    
    var loginTextFieldCell: LoginTextFieldTableViewCell?
    var textFieldTag = 0
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        configureUI()
        setUpTableView()
        reCheckVM()
        print("----------- view Didi Load ----------")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBar()
        print("------------ view Will Appear ----------")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("------------ view Will DisAppear ----------")
    }
    
    // MARK: - Functions
    private func setNavigationBar() {
        super.configureNavigationBar(with: .clearBackground, #selector(backButtonAction), nil, self)
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func configureUI() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.view.backgroundColor = UIColor.BAdarkBlueBackground
        self.tableView.backgroundColor = UIColor.BAdarkBlueBackground
        tableView.allowsSelection = false
        proceedButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        proceedButton.style(.tertiary, isActive: false)
        notOnTataButton.titleLabel?.font = .skyTextFont(.medium, size: 16)
        notOnTataButton.underline()
    }
    
    private func reCheckVM() {
        if self.loginViewModel == nil {
            self.loginViewModel = LoginViewModel()
            self.loginType = .rmnWithOtp
            self.loginViewModel?.dataModel.loginType = .rmnWithOtp
        }
    }
    
    private func setUpTableView() {
        let cells = LoginVcDataSource.CellType.typeOfCells
		tableView.delegate = self
		tableView.dataSource = self
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension
        
        for item in cells {
            UtilityFunction.shared.registerCell(tableView, item.cellIndentifier)
        }
    }
    
    func generateOtp() {
        if let vmModel = loginViewModel {
            vmModel.generateOtp { (flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    self.moveToVerificationScreen()
                } else if isApiError {
                    if message == "cancelled" {
                        // Do nothing
                    } else if vmModel.errorCode == 429 {
                        self.otpResent(BAConfigManager.shared.configModel?.data?.config?.rateLimit ?? "", completion: nil)
                    } else {
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                } else {
                    let cell = self.tableView.cellForRow(at: IndexPath(row: vmModel.dataModel.loginType == .rmnWithOtp ? 3 : 5, section: 0)) as? LoginTextFieldTableViewCell
                    cell?.textFieldHasError(true, with: vmModel.dataModel.loginType == .rmnWithOtp ? "" : "", errorMessage: message ?? "Error")

                }
            }
        }
    }
    
    func moveToVerificationScreen() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "VerificationVC", type: LoginVerificationVC.self)
        self.hideActivityIndicator()
        self.view.endEditing(true)
        loginViewModel?.dataModel.segmentIndex = selectedSegmentIndex
        loginViewModel?.dataModel.loginType = loginType
        pushView.verificationDataModel = loginViewModel
        self.navigationController?.pushViewController(pushView, animated: true)
    }
    
    @IBAction func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if loginViewModel != nil {
                showActivityIndicator(isUserInteractionEnabled: false)
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginOtpInvoke.rawValue)
                self.generateOtp()
            }
        } else {
			noInternet()
        }
    }
    
    @IBAction func openTataPage() {
        if let url = URL(string: "https://www.tatasky.com"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
}

// MARK: - Extensions
extension LoginVC: SegmentSelectedProtocol, LoginTextFieldTableViewCellProtocol, SignUpButtonActionDelegate, SignUpAlreadyUserTableViewCellProtocol {
    func notifyValidEmail() {
        // Do Nothing
    }
    
    func isAccepted(_ value: Bool) {
        if selectedSegmentIndex == 0 && loginType == LoginType.rmnWithOtp {
            selectedTermsForRmnWithOtp = value
        }
        
        termsSelected = value
        enableProceedButton(termsSelected)
    }
    
    
    func openTermsAndCondition() {
        if BAReachAbility.isConnectedToNetwork() {
            let webViewVC: BAWebViewViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.webViewVC.rawValue, type: BAWebViewViewController.self)
            webViewVC.isPrivacyPolicy = false
            webViewVC.isFAQ = false
            self.navigationController?.pushViewController(webViewVC, animated: true)
        } else {
            noInternet()
        }
    }
    
    
    func segmentSwitched(_ index: Int) {
        self.view.endEditing(true)
		loginViewModel?.dataModel.segmentIndex = index
        selectedSegmentIndex = index
        if loginViewModel?.dataModel.tfTag == 0 {
            loginViewModel?.dataModel.loginType = .rmnWithOtp
            termsSelected = selectedTermsForRmnWithOtp
            loginType = .rmnWithOtp
        } else if loginViewModel?.dataModel.tfTag == 1 {
            if index == 0 {
                loginViewModel?.dataModel.loginType = .sidWithOtp
                loginType = .sidWithOtp
            } else {
                loginViewModel?.dataModel.loginType = .sidWithPwd
                loginType = .sidWithPwd
            }
        }
        checkForLoginTypeToEnableProceedButton(index)
        if selectedSegmentIndex == 0{
            tableView.reloadRows(at: [IndexPath(item: 3, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 4, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 6, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 5, section: 0)], with: .none)
        }else{
            tableView.reloadRows(at: [IndexPath(item: 3, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 4, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 6, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(item: 5, section: 0)], with: .none)
        }
        print("Selected segment is ", index)
    }
    
    func valueUpdated(_ idx: Int, _ text: String) {
        let segmentCell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? LoginSegmentTableViewCell
        if termsSelected {
            proceedButton.style(text.count >= 10 ? .primary : .tertiary, isActive: text.count >= 10)
        } else {
            proceedButton.style(.tertiary, isActive: false)
        }
        loginViewModel?.dataModel.tfTag = idx
        if idx == 0 {
            if text.length > 0 && loginViewModel?.dataModel.sid != nil {
                loginViewModel?.dataModel.sid = nil
                updateTextfieldInput(5)
            }
            loginViewModel?.dataModel.rmn = text
            loginViewModel?.dataModel.loginType = .rmnWithOtp
            loginType = .rmnWithOtp
        } else if idx == 1 {
            if text.length > 0 && loginViewModel?.dataModel.rmn != nil  {
                loginViewModel?.dataModel.rmn = nil
                updateTextfieldInput(3)
            }
            loginViewModel?.dataModel.sid = text
            if segmentCell?.segmentView.selectedSegmentIndex == 0 && text.count > 0 {
                loginViewModel?.dataModel.loginType = .sidWithOtp
                loginType = .sidWithOtp
            } else {
                loginViewModel?.dataModel.loginType = .sidWithPwd
                loginType = .sidWithPwd
                textFieldTag = 1
            }
        }
    }
    
    func signUpButtonTapped() {
        super.moveToSignUpScreen()
    }
    
    func updateTextfieldInput(_ idx: Int) {
        let rmnCell = tableView.cellForRow(at: IndexPath.init(row: idx, section: 0)) as! LoginTextFieldTableViewCell
        rmnCell.sendertagValue = textFieldTag
        rmnCell.inputTextField.text = ""
        rmnCell.textFieldHasError(false)
    }
    
    // Hot fix method added to support data persistence while login and switching segment
    func checkForLoginTypeToEnableProceedButton(_ index: Int) {
        switch loginType {
        case .rmnWithOtp:
            if loginViewModel?.dataModel.rmn?.count ?? 0 >= 10 && termsSelected && index == 0 {
                proceedButton.style(.primary, isActive: true)
            } else {
                termsSelected = false
                proceedButton.style(.tertiary, isActive: false)
            }
        case .sidWithOtp, .sidWithPwd:
            if loginViewModel?.dataModel.sid?.count ?? 0 >= 10 && termsSelected {
                proceedButton.style(.primary, isActive: true)
            } else {
                proceedButton.style(.tertiary, isActive: false)
            }
        case .none:
            break
        }
    }
    
    func enableProceedButton(_ termsSelected: Bool) {
        if termsSelected {
            if loginViewModel?.dataModel.sid?.count ?? 0 >= 10 || loginViewModel?.dataModel.rmn?.count ?? 0 >= 10 {
                proceedButton.style(.primary, isActive: true)
            } else {
                proceedButton.style(.tertiary, isActive: false)
            }
        } else {
            proceedButton.style(.tertiary, isActive: false)
        }
    }
}

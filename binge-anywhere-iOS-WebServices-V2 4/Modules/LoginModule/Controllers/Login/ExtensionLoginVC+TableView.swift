//
//  ExtensionLoginVC+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 18/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Extention
extension LoginVC: UITableViewDelegate, UITableViewDataSource {

    // MARK: - TableView Delegate Methods
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LoginVcDataSource.CellType.typeOfCells.count
	}

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.isUserInteractionEnabled = true
        switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as? BottomBingeLogoTableViewCell
			cell?.configureIndicator(number: 4, selectedIndex: 0)
			return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: kTitleHeaderTableViewCell) as? TitleHeaderTableViewCell
            cell?.configureCell(source: .login)
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: kLoginSegmentTableViewCell) as? LoginSegmentTableViewCell
            cell?.delegate = self
            switch loginViewModel?.dataModel.loginType {
                case .sidWithPwd:
                    cell?.updateSegment(index: 1)
                default:
                    cell?.updateSegment(index: 0)
            }
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: kLoginTextFieldTableViewCell) as? LoginTextFieldTableViewCell
            cell?.configureUIForMobileNumer()
            loginTextFieldCell = cell
//            loginViewModel?.dataModel.rmn = ""
            cell?.delegate = self
			cell?.clipsToBounds = true
            cell?.inputTextField.tag = 0
            cell?.configureTextFieldWithValue(loginViewModel?.dataModel.rmn ?? "")
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: kBAOrSeparatorTableViewCell) as? BAOrSeparatorTableViewCell
            return cell!
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: kLoginTextFieldTableViewCell) as? LoginTextFieldTableViewCell
            cell?.configureUIForSubscriberId()
            loginTextFieldCell = cell
            cell?.delegate = self
			cell?.clipsToBounds = true
            cell?.inputTextField.tag = 1
            cell?.configureTextFieldWithValue(loginViewModel?.dataModel.sid ?? "")
            return cell!
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: kSignUpAlreadyUserTableViewCell) as? SignUpAlreadyUserTableViewCell
            cell?.checkButton.isSelected = termsSelected
            cell?.configureCell(source: .verification)
            cell?.delegate = self
            return cell!
		default:
            let cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as? BottomBingeLogoTableViewCell
            return cell!
        }
    }

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.row == 3 || indexPath.row == 4 {
			let cell = tableView.cellForRow(at: indexPath)
			cell?.isHiddenAnimated(value: loginViewModel?.dataModel.segmentIndex == 1)
			return loginViewModel?.dataModel.segmentIndex == 1 ? 0.01 : UITableView.automaticDimension
		} else {
			return UITableView.automaticDimension
		}
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

//
//  NewUserTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 17/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol SignUpButtonActionDelegate: AnyObject {
    func signUpButtonTapped()
}

class NewUserTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var signUpInfoLabel: CustomLabel!
	@IBOutlet weak var signUpButton: CustomButton!

    // MARK: - Variables
    weak var delegate: SignUpButtonActionDelegate?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
		configureUI()
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Functions
    func configureUI() {
		self.contentView.backgroundColor = .BAdarkBlueBackground
		signUpInfoLabel.text = "Not on Tata Sky Binge?"
		signUpButton.text("Sign Up")
		signUpButton.tintColor = .BABlueColor
        signUpButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BABlueColor)
        signUpButton.underline()
        signUpInfoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
	}

    // MARK: - Actions
    @IBAction func SignUpButtonTapped() {
        if let delegate = delegate {
            delegate.signUpButtonTapped()
        }
    }
}

//
//  BAOrSeparatorTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAOrSeparatorTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: CustomLabel?
	@IBOutlet var titleLabelHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
	
    func configureCell(_ text: String, labelIsHidden: Bool) {
        titleLabel?.isHidden = labelIsHidden
		titleLabel?.text = text
        titleLabelTopConstraint.constant = 20
		titleLabel?.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 14), color: .BAwhite)
	}
}

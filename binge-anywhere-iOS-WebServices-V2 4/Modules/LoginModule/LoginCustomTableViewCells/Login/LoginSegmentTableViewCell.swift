//
//  LoginHeaderCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 17/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol SegmentSelectedProtocol: AnyObject {
    func segmentSwitched(_ index: Int)
}

class LoginSegmentTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var segmentView: WMSegment!

    // MARK: - Variables
    weak var delegate: SegmentSelectedProtocol?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

		configureUI()
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

	}

    // MARK: - Functions
    private func configureUI() {
		self.contentView.backgroundColor = .BAdarkBlueBackground
		segmentView.normalFont = .skyTextFont(.medium, size: 18)
		segmentView.selectedFont = .skyTextFont(.medium, size: 18)
		segmentView.selectorColor = .BABlueColor
		segmentView.textColor = .BAlightBlueGrey
		segmentView.selectorType = .bottomBar
		segmentView.buttonTitles = "OTP,Password"

        if let del = delegate {
            del.segmentSwitched(segmentView.selectedSegmentIndex)
        }
	}

    func updateSegment(index: Int) {
        segmentView.selectedSegmentIndex = index
    }
	
	func isEnabled(_ value: Bool) {
		self.isUserInteractionEnabled = value
		segmentView.selectorColor = value ? .BABlueColor : .BAdarkBlue1Color
	}

    // MARK: - Actions
    @IBAction func segmentControlChange(_ sender: WMSegment) {
        if let del = delegate {
            del.segmentSwitched(sender.selectedSegmentIndex)
        }
    }

}

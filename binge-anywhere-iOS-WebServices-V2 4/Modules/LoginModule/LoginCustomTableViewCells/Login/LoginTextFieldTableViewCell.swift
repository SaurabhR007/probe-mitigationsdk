//
//  SignUpTextFieldTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 24/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol LoginTextFieldTableViewCellProtocol: AnyObject {
    func valueUpdated(_ idx: Int, _ text: String)
    func notifyValidEmail()
    //func updateTextfieldInput(_ idx: Int, _ isEnabled: Bool)
}


class LoginTextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var inputTextField: CustomtextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var prefixLabel: CustomLabel!
    @IBOutlet weak var errorInfoLabel: CustomLabel!
    var myMutableString = NSMutableAttributedString()
    var sendertagValue = 0
    // MARK: - Variables
    weak var delegate: LoginTextFieldTableViewCellProtocol?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        prefixLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        //titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        errorInfoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 10 * screenScaleFactorForWidth), color: .BAerrorRed)
        
    }

    // MARK: - Functions
	func configureUI() {
		self.contentView.backgroundColor = .BAdarkBlueBackground
		inputTextField.delegate = self
		inputTextField.text = ""
		inputTextField.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth))
		inputTextField.backgroundColor = .BAdarkBlue1Color
		inputTextField.textColor = .BAwhite
		inputTextField.tintColor = .BAwhite
		inputTextField.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
		inputTextField.addDoneButtonOnKeyBoardWithControl()
		inputTextField.cornerRadius = 2
		inputTextField.setViewShadow(opacity: 0.12, blur: 2)
	}
	
    func configureUIForMobileNumer() {
        textFieldHasError(false)
        isTextFieldHighlighted(false)
        inputTextField.text = "3001471899"
        inputTextField.placeholder = ""//"1234 56789"
		inputTextField.keyboardType = .asciiCapableNumberPad
        inputTextField.setHorizontalPadding(left: 45 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
        titleLabel.text = kRegisteredRMNHeader//"Enter Your Registered Mobile Number"
        prefixLabel.text = "+91"
        errorInfoLabel.text = ""
    }

    func configureUIForSubscriberId() {
        textFieldHasError(false)
        isTextFieldHighlighted(false)
        inputTextField.text = "3001471899"
        inputTextField.placeholder = ""
		inputTextField.keyboardType = .asciiCapableNumberPad
        inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
        titleLabel.text = kSubscriberIdHeader//"Enter Your Tata Sky Sub ID"
        prefixLabel.text = ""
        errorInfoLabel.text = ""
    }
	
    
    func configureTextFieldWithValue(_ textValue: String) {
        inputTextField.text = textValue
    }
    
	func configureUIForEmail(email: String) {
		inputTextField.text = email
		inputTextField.placeholder = ""
		inputTextField.keyboardType = .emailAddress
		inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
        // set label Attribute
        //labName.attributedText = myMutableString
        let attrs1 = [NSAttributedString.Key.font : UIFont.skyTextFont(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.BAwhite]

        let attrs2 = [NSAttributedString.Key.font : UIFont.skyTextFont(.medium, size: 16), NSAttributedString.Key.foregroundColor : UIColor.BAerrorRed]

        let attributedString1 = NSMutableAttributedString(string:"Email", attributes:attrs1)

        let attributedString2 = NSMutableAttributedString(string:"*", attributes:attrs2)

        attributedString1.append(attributedString2)
        self.titleLabel.attributedText = attributedString1
		//titleLabel.text = "Email*"
		prefixLabel.text = ""
		errorInfoLabel.text = ""
	}

    private func isTextFieldHighlighted(_ highlighted: Bool) {
        inputTextField.borderWidth = 0.0
        if highlighted {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAlightBlueGrey
        }
    }

     func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
        if hasError {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAerrorRed
            inputTextField.placeholder = Placeholder
            errorInfoLabel.text = errorMessage
        } else {
            inputTextField.borderWidth = 0.0
            errorInfoLabel.text = ""
        }
    }

    private func validateData(_ tag: Int, _ text: String) -> Bool {
		if tag == 3 {
			return true
		} else {
			return text.length <= 10
		}
    }

    // MARK: - Actions
    @IBAction private func txtFieldBeginEditing(_ sender: CustomtextField) {
        textFieldHasError(false)
        isTextFieldHighlighted(true)
    }

    @IBAction private func txtFieldEndEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(false)
        if let textString = sender.text {
            sendertagValue = sender.tag
            switch sender.tag {
            case 0:
                if !textString.isValidContact && (textString.length > 0) {
                    textFieldHasError(true, with: "", errorMessage: kValidPhoneNumber)
                }
            case 1:
                if !textString.isValidContact && (textString.length > 0) {
                    textFieldHasError(true, with: "", errorMessage: kValidSID)
            }
			case 3:
				if !textString.isValidEmail() {
					textFieldHasError(true, with: "", errorMessage: "Please enter a valid Email")
                    delegate?.notifyValidEmail()
				}
            default:
                if textString.isEmpty {
                    textFieldHasError(false)
                }
            }
        }
    }
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let del = delegate {
            del.valueUpdated(sender.tag, sender.text ?? "")
        }
    }
}

// MARK: - Extension
extension LoginTextFieldTableViewCell {

    // MARK: - TableView Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print(textField.tag == 0 ? "Login Number " : "SID ", txtAfterUpdate)
//        if let del = delegate {
//            del.updateTextfieldInput(textField.tag, txtAfterUpdate.count == 0)
//        }
        return validateData(textField.tag, txtAfterUpdate)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.superview?.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

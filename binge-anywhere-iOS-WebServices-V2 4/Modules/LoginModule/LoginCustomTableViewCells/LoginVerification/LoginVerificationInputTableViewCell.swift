//
//  SignUpTextFieldTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 24/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol VerificationTextFieldTableViewCellProtocol: AnyObject {
    func valueUpdated(_ idx: Int, _ text: String)
}

class LoginVerificationInputTableViewCell: UITableViewCell, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var inputTextField: CustomtextField!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var prefixLabel: CustomLabel!
    @IBOutlet weak var errorInfoLabel: CustomLabel!
    @IBOutlet weak var passwordPeekButton: CustomButton!

    // MARK: - Variables
    weak var delegate: VerificationTextFieldTableViewCellProtocol?
    //var loginVCVerificaton: LoginVerificationVC?
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    override func prepareForReuse() {
        updateCellForSegment()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        NotificationCenter.default.addObserver(self, selector: #selector(showOTPError), name: NSNotification.Name(rawValue: "ShowOtpAlertForLogin"), object: nil)
        inputTextField.delegate = self
        inputTextField.text = ""
        inputTextField.placeholder = "Enter OTP"
        inputTextField.backgroundColor = .BAdarkBlue1Color
        inputTextField.textColor = .BAwhite
        inputTextField.tintColor = .BAwhite
        inputTextField.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
        inputTextField.keyboardType = .asciiCapableNumberPad
        inputTextField.addDoneButtonOnKeyBoardWithControl()
        inputTextField.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth))
        inputTextField.cornerRadius = 2
        inputTextField.setViewShadow(opacity: 0.12, blur: 2)
        inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 50 * screenScaleFactorForWidth, isRightValueInUse: true)

        titleLabel.text = kEnterLogin6DigitOTP//"Enter Your Code"
        titleLabel.textColor = .BAwhite
        titleLabel.font = .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth)

        prefixLabel.isHidden = true
        prefixLabel.text = "+91"
        prefixLabel.textColor = .BAwhite
        prefixLabel.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)

        errorInfoLabel.text = ""
        errorInfoLabel.textColor = .BAerrorRed
        errorInfoLabel.font = .skyTextFont(.medium, size: 10 * screenScaleFactorForWidth)
    }

    @objc private func showOTPError(notification: NSNotification) {
        textFieldHasError(true, with: "Enter OTP", errorMessage: "OTP cannot be blank")
    }

    func updateCellForSegment() {
        switch inputTextField.tag {
        case 0:
            titleLabel.text = kEnterLogin6DigitOTP//"Enter Your Code"
            inputTextField.placeholder = "Enter OTP"
            inputTextField.isSecureTextEntry = false
            passwordPeekButton.isHidden = true
        case 1:
            titleLabel.text = kPasswordHeader//"Enter Password"
            inputTextField.placeholder = "Password"
            inputTextField.keyboardType = .asciiCapable
            inputTextField.isSecureTextEntry = true
            passwordPeekButton.isHidden = true
        default: break
        }

        if inputTextField.text!.isEmpty {
            textFieldHasError(false)
        }
    }

    private func isTextFieldHighlighted(_ highlighted: Bool) {
        inputTextField.borderWidth = 0.0
        if highlighted {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAlightBlueGrey
        }
    }

    func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
        if hasError {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAerrorRed
            inputTextField.placeholder = Placeholder
            errorInfoLabel.text = errorMessage
            errorInfoLabel.isHidden = false
        } else {
            inputTextField.borderWidth = 0.0
            errorInfoLabel.text = ""
            errorInfoLabel.isHidden = true
        }
    }

    private func validateData(_ tag: Int, _ text: String) -> Bool {
        return tag == 0 ? text.length <= 6 : text.length <= 256
    }

    // MARK: - Actions
    @IBAction func peekPassword() {
        if inputTextField.isSecureTextEntry {
            inputTextField.isSecureTextEntry = false
            passwordPeekButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            inputTextField.isSecureTextEntry = true
            passwordPeekButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }

    @IBAction private func txtFieldBeginEditing(_ sender: CustomtextField) {
        textFieldHasError(false)
        isTextFieldHighlighted(true)
    }

    @IBAction private func txtFieldEndEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(false)
        if let textValue = sender.text {
            if textValue.isEmpty {
                switch sender.tag {
                case 0: textFieldHasError(false, with: "6 Digits", errorMessage: "OTP cannot be blank")
                case 1: textFieldHasError(false, with: "Password", errorMessage: "Password cannot be blank")
                default: textFieldHasError(false)
                }
            } else {
                switch sender.tag {
                case 0: textValue.count < 6 ? textFieldHasError(true, with: "6 Digits", errorMessage: kLoginOTPDigit) : textFieldHasError(false)
                case 1: textValue.isValidPasswordRegex() ? textFieldHasError(false) : textFieldHasError(true, with: "Password", errorMessage: "The Password entered is incorrect. Please try again.")
                default: textFieldHasError(false)
                }
            }
        }
    }

    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        switch inputTextField.tag {
        case 0:
            passwordPeekButton.isHidden = true
        case 1:
            if sender.text?.isEmpty ?? false {
                passwordPeekButton.isHidden = true
            } else {
                passwordPeekButton.isHidden = false
            }
        default: break
            
        }
        
        if let del = delegate {
            del.valueUpdated(sender.tag, sender.text ?? "")
        }
    }

}

// MARK: - Extension
extension LoginVerificationInputTableViewCell {

    // MARK: - TableView Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        return validateData(textField.tag, txtAfterUpdate)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.superview?.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

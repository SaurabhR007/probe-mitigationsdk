//
//  IdSelectionTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 28/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol IdSelectionTableViewCellProtocol: AnyObject {
    func cellSelected(_ text: String, _ id: String, _ index: Int?)
}

class IdSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deviceIconImageView: UIImageView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var statusLabel: CustomLabel!
    @IBOutlet weak var selectionButton: CustomButton!
    @IBOutlet weak var deviceImageView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    var delegate: IdSelectionTableViewCellProtocol?
    var activeBingeStatus = [String]()
    var selectedIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
        statusLabel.text = ""
        selectionButton.isSelected = false
    }
    
    private func configUI() {
        self.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle = .none
        containerView.backgroundColor = .BAdarkBlue2Color
        containerView.cornerRadius = 4
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 18), color: .BAwhite)
        statusLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 15), color: .BAlightBlueGrey)
    }
    
    func configureCellForSid(_ data: SubscriberDetailResponseData, index: Int?) {
        titleLabel.text = String(data.subscriberId ?? 0)
        activeBingeStatus = []
        tag = index ?? 0
        if data.accountDetailList?.count ?? 0 > 0 {
            for index in 0..<(data.accountDetailList?.count ?? 0) {
                activeBingeStatus.append(data.accountDetailList?[index].accountStatus ?? "")
            }
        } else {
            statusLabel.text = ""
        }
        let statusToShow = data.statusType//checkForStatus(array: data.accountDetailList ?? [])
        if statusToShow?.uppercased() == SIDStatusType.active.rawValue.uppercased() {
            statusLabel.text = kBingeActiveSID
            statusLabel.textColor = .BAlightBlueGrey
        } else if statusToShow?.uppercased() == SIDStatusType.inActive.rawValue.uppercased() {
            statusLabel.text = kBingeInactiveSID
            statusLabel.textColor = .BAerrorRed
        } else {
           statusLabel.text = ""
        }

        selectionButton.isSelected = (index == selectedIndex)
        deviceImageView.isHidden = true
    }
    
    func configureCellForBaId(_ id: AccountDetailListResponseData, index: Int?) {
        titleLabel.text = id.aliasName ?? ""
        statusLabel.text = String(id.baId ?? 0)
        tag = index ?? 0
        selectionButton.isSelected = (index == selectedIndex)
        if id.subscriptionType == BASubscriptionsConstants.binge.rawValue {
            deviceIconImageView.image = UIImage(named: "device")
        } else {
            deviceIconImageView.image = UIImage(named: "tvicon")
        }
        statusLabel.isHidden(true)
    }
    
    @IBAction func selectionAction(_ sender: UIButton) {
        print((titleLabel.text ?? "") + " | " + (statusLabel.text ?? ""))
        selectionButton.isSelected = true //!selectionButton.isSelected
        guard let del = delegate else { return }
        if selectionButton.isSelected {
            selectedIndex = tag
            del.cellSelected(titleLabel.text ?? "", statusLabel.text ?? "", selectedIndex)
        } else {
            del.cellSelected("", "", nil)
        }
    }
    
    func checkForStatus(array : [AccountDetailListResponseData]) -> String {
        if let firstStatus = array.first {
            for status in array {
                //activeBingeStatus.append(status.accountStatus ?? "")
                if status.accountStatus == kActive {
                    return kActive
                } else if status.accountStatus != firstStatus.accountStatus {
                    return kWrittenOff
                }
            }
        }
        let inActiveBingeStatus = activeBingeStatus.dropFirst().allSatisfy({ $0 == activeBingeStatus.first })
        if inActiveBingeStatus && activeBingeStatus[0] == kDeActive {
            return kDeActive
        } else {
            return kWrittenOff
        }
        
    }
}

//
//  ShowDataTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 30/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class ShowDataTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: CustomLabel!
	@IBOutlet weak var nameLabel: CustomLabel!
		
	var titleText = ""
	var nameText = ""
	
	override func awakeFromNib() {
        super.awakeFromNib()
		
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
	
	func configUI() {
		self.backgroundColor = .BAdarkBlueBackground
		titleLabel.text = titleText
		nameLabel.text = nameText
	}
    
}

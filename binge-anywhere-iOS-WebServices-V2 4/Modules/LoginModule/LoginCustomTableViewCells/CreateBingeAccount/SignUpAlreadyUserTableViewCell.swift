//
//  SignUpAlreadyUserTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 17/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol SignUpAlreadyUserTableViewCellProtocol: AnyObject {
    func isAccepted(_ value: Bool)
    func openTermsAndCondition()
}

class SignUpAlreadyUserTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var termLabel: CustomLabel!
    @IBOutlet weak var checkButton: CustomButton!

    // MARK: - Variables
    weak var delegate: SignUpAlreadyUserTableViewCellProtocol?
    var termsTextRange = NSMakeRange(0, 0)

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - Functions
    func configureCell(source: TableViewCellSourceModel) {
        switch source {
        case .signup:
            let labelString = "I am already a Tata Sky subscriber, and I want to link my accounts"
            let attributedText = NSMutableAttributedString(string: labelString)
            let substr = "Tata Sky subscriber"
            let textRange = attributedText.mutableString.range(of: substr, options: NSString.CompareOptions.caseInsensitive)
            let wholerange = attributedText.mutableString.range(of: labelString)
            let textColor: UIColor = .BABlueColor
            let underLineStyle = NSUnderlineStyle.single.rawValue
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            attributedText.addAttribute(.foregroundColor, value: textColor, range: textRange)
            attributedText.addAttribute(.underlineColor, value: textColor, range: textRange)
            attributedText.addAttribute(.underlineStyle, value: underLineStyle, range: textRange)
            attributedText.addAttribute(.font, value: UIFont.skyTextFont(.regular, size: 14 * screenScaleFactorForWidth), range: wholerange)
//            attributedText.addAttribute(.link, value: substr, range: textRange)
            termLabel.attributedText = attributedText
            termLabel.isUserInteractionEnabled = true
            termLabel.addGestureRecognizer(tap)
        case .verification, .otp:
            let labelString = "I accept the License Agreement"//"I accept the Terms and Conditions"
            let attributedText = NSMutableAttributedString(string: labelString)
            let substr = "License Agreement"//"Terms and Conditions"
            let textRange = attributedText.mutableString.range(of: substr, options: NSString.CompareOptions.caseInsensitive)
            termsTextRange = textRange
            let wholerange = attributedText.mutableString.range(of: labelString)
            let textColor: UIColor = .BABlueColor
            let underLineStyle = NSUnderlineStyle.single.rawValue
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            attributedText.addAttribute(.foregroundColor, value: textColor, range: textRange)
            attributedText.addAttribute(.underlineColor, value: textColor, range: textRange)
            attributedText.addAttribute(.underlineStyle, value: underLineStyle, range: textRange)
            attributedText.addAttribute(.font, value: UIFont.skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), range: wholerange)
            //attributedText.addAttribute(.link, value: substr, range: textRange)
            termLabel.attributedText = attributedText
            termLabel.isUserInteractionEnabled = true
            termLabel.addGestureRecognizer(tap)
        default:
            break
        }
    }

    @objc func tapFunction(sender: UITapGestureRecognizer) {
        if sender.didTapAttributedTextInLabel(label: termLabel, inRange: termsTextRange) {
            delegate?.openTermsAndCondition()
            print("Tapped targetRange1")
        } else {
            print("Tapped none")
        }
        
        print("tap working")
    }

    func updateCheckButtonState(_ flagValue: Bool) {
        checkButton.isSelected = flagValue
        checkButton.isEnabled = flagValue
    }

    // MARK: - Actions
    @IBAction func selectionToggle() {
        checkButton.isSelected = !checkButton.isSelected
        if let del = delegate {
            del.isAccepted(checkButton.isSelected)
        }
    }
}

//
//  VerificationDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

enum LoginType {
	case rmnWithOtp
	case sidWithOtp
	case sidWithPwd
}

struct LoginDataModel {
	var rmn: String?
	var sid: String?
	var baid: String?
	var otp: String?
	var segmentIndex: Int = 0
	var loginType: LoginType?
	var tfTag: Int?
	var formType: String?
	var source: String?
	var name: String?
	var email: String?
	var eulaChecked: Bool?
    var dsn: String?
	var password: String = ""
	var verifyPassword: String = ""
    var maskedNumber: String?
    var maskedNumberToShow: String? // hotfix added to show masked number coming from server now
}

//Forget Password Response
struct UpdatePasswordResponse: Codable {
	let code: Int?
	let message: String?
	let data: [String: String]?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent([String: String].self, forKey: .data)
	}
}

//Generate OTP Response
struct InitialLoginServerResponse: Codable {
	let code: Int?
	let message: String?
	let data: InitialLoginResponseData?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(InitialLoginResponseData.self, forKey: .data)
	}
}

struct InitialLoginResponseData: Codable {
	let rmn: String?
    let maskedNumber: String?
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
        maskedNumber = try values.decodeIfPresent(String.self, forKey: .maskedNumber)
	}
}

//Validate OTP & Password Response
struct ValidationServerResponse: Codable {
	let code: Int?
	let message: String?
	let data: ValidationResponseData?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(ValidationResponseData.self, forKey: .data)
	}
}

struct ValidationResponseData: Codable {
	let userAuthenticateToken: String?
	let deviceAuthenticateToken: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userAuthenticateToken = try values.decodeIfPresent(String.self, forKey: .userAuthenticateToken)
		deviceAuthenticateToken = try values.decodeIfPresent(String.self, forKey: .deviceAuthenticateToken)
	}
}

//Baid Lookup Call
struct BaidLookUpServerResponse: Codable {
	let code: Int?
	let message: String?
	let data: SubscriberDetailResponseData?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(SubscriberDetailResponseData.self, forKey: .data)
	}
}

//Sid Lookup Call
struct SidLookUpServerResponse: Codable {
	let code: Int?
	let message: String?
	let data: [SubscriberDetailResponseData]?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent([SubscriberDetailResponseData].self, forKey: .data)
	}
}

struct SubscriberDetailResponseData: Codable {
	let subscriberId: Int?
	let accountStatus: String?
    let emailId: String?
    let rmn: String?
    let subscriberName: String?
    let loginErrorMessage: String?
	let accountDetailList: [AccountDetailListResponseData]?
    let statusType: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		subscriberId = try values.decodeIfPresent(Int.self, forKey: .subscriberId)
		accountStatus = try values.decodeIfPresent(String.self, forKey: .accountStatus)
        emailId = try values.decodeIfPresent(String.self, forKey: .emailId)
        rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
        subscriberName = try values.decodeIfPresent(String.self, forKey: .subscriberName)
        loginErrorMessage = try values.decodeIfPresent(String.self, forKey: .loginErrorMessage)
		accountDetailList = try values.decodeIfPresent([AccountDetailListResponseData].self, forKey: .accountDetailList)
        statusType = try values.decodeIfPresent(String.self, forKey: .statusType)
	}
}

struct AccountDetailListResponseData: Codable {
	let baId: Int?
	let subscriberId: Int?
    let firstTimeLogin: Bool?
    let firstTimeLoginDate: String?
	let defaultProfile: String?
	let userAuthenticateToken: String?
	let deviceAuthenticateToken: String?
	let aliasName: String?
	var emailId: String?
	let rrmSessionInfoDTO: RrmSessionInfoDTOResponseData?
	let tvodEntitlements: [TvodEntitlementsResponseData]?
	let deviceDetails: DeviceDetailsResponseData?
	let freeTrialAvailed: Bool?
    let deviceLoginCount: String?
    let bingeAccountCount: String?
    let accountSubStatus: String?
    let message: String?
    let fsTaken: Bool?
//    var partnerSubscriptions: PartnerSubscription?
    var partnerSubscriptionsDetails: PartnerSubscriptionDetails?
    var partnerSubscriptions: [PartnerSubscription]?
	let imageUrl: String?
	let accountStatus: String?
    let subscriptionType: String?
    let deviceSerialNumber: String?
    let rmn: String?
    let firstName: String?
    let lastName: String?
	let subscriberAccountStatus: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		baId = try values.decodeIfPresent(Int.self, forKey: .baId)
        rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
		subscriberId = try values.decodeIfPresent(Int.self, forKey: .subscriberId)
		defaultProfile = try values.decodeIfPresent(String.self, forKey: .defaultProfile)
		userAuthenticateToken = try values.decodeIfPresent(String.self, forKey: .userAuthenticateToken)
		deviceAuthenticateToken = try values.decodeIfPresent(String.self, forKey: .deviceAuthenticateToken)
		aliasName = try values.decodeIfPresent(String.self, forKey: .aliasName)
		emailId = try values.decodeIfPresent(String.self, forKey: .emailId)
		rrmSessionInfoDTO = try values.decodeIfPresent(RrmSessionInfoDTOResponseData.self, forKey: .rrmSessionInfoDTO)
		tvodEntitlements = try values.decodeIfPresent([TvodEntitlementsResponseData].self, forKey: .tvodEntitlements)
		deviceDetails = try values.decodeIfPresent(DeviceDetailsResponseData.self, forKey: .deviceDetails)
		freeTrialAvailed = try values.decodeIfPresent(Bool.self, forKey: .freeTrialAvailed)
		partnerSubscriptionsDetails = try values.decodeIfPresent(PartnerSubscriptionDetails.self, forKey: .partnerSubscriptionsDetails)
		partnerSubscriptions = try values.decodeIfPresent([PartnerSubscription].self, forKey: .partnerSubscriptions)
		imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
		accountStatus = try values.decodeIfPresent(String.self, forKey: .accountStatus)
		subscriberAccountStatus = try values.decodeIfPresent(String.self, forKey: .subscriberAccountStatus)
        deviceLoginCount = try values.decodeIfPresent(String.self, forKey: .deviceLoginCount)
        bingeAccountCount = try values.decodeIfPresent(String.self, forKey: .bingeAccountCount)
        firstTimeLogin = try values.decodeIfPresent(Bool.self, forKey: .firstTimeLogin)
        accountSubStatus = try values.decodeIfPresent(String.self, forKey: .accountSubStatus)
        subscriptionType = try values.decodeIfPresent(String.self, forKey: .subscriptionType)
        deviceSerialNumber = try values.decodeIfPresent(String.self, forKey: .deviceSerialNumber)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        fsTaken = try values.decodeIfPresent(Bool.self, forKey: .fsTaken)
        firstTimeLoginDate = try values.decodeIfPresent(String.self, forKey: .firstTimeLoginDate)
	}
}

struct RrmSessionInfoDTOResponseData: Codable {
	let sessionId: String?
	let  sessionTicket: String?
	let ticket: String?
	let nonce: String?
	let expiryTime: Int?
	let heartbeatInterval: Int?
	let maxMissedHeartbeats: Int?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		sessionId = try values.decodeIfPresent(String.self, forKey: .sessionId)
		sessionTicket = try values.decodeIfPresent(String.self, forKey: .sessionTicket)
		ticket = try values.decodeIfPresent(String.self, forKey: .ticket)
		nonce = try values.decodeIfPresent(String.self, forKey: .nonce)
		expiryTime = try values.decodeIfPresent(Int.self, forKey: .expiryTime)
		heartbeatInterval = try values.decodeIfPresent(Int.self, forKey: .heartbeatInterval)
		maxMissedHeartbeats = try values.decodeIfPresent(Int.self, forKey: .maxMissedHeartbeats)
	}
}

struct TvodEntitlementsResponseData: Codable {
	let type: String?
	let pkgId: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		pkgId = try values.decodeIfPresent(String.self, forKey: .pkgId)
	}
}

struct DeviceDetailsResponseData: Codable {
	let baId: String?
	let deviceId: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		baId = try values.decodeIfPresent(String.self, forKey: .baId)
		deviceId = try values.decodeIfPresent(String.self, forKey: .deviceId)
	}
}

struct PartnerSubscriptionDetails: Codable {
	let packName: String?
	let packId: String?
	let packCreatedDate: String?
    let packCreationDate: String?
	let packPrice: String?
	let expirationDate: String?
	let rechargeDue: String?
	let amountDue: String?
	let message: String?
	let packType: String?
	let status: String?
	let alternatePackId: String?
    let partnerUniqueId: String?
	let fsEligibility: Bool?
    let dthStatus: String?
    let accountSubStatus: String?
    let fsTaken: Bool?
    let subscriptionType: String?
    let migrated: Bool?
    let packSubscriptionStatus: String?
    let subscriptionExpiryMessage: String?
    let accountScreenExpiryMessage: String?
    let expiredWithinSixtyDays: Bool?
    let largeScreenEligibility: Bool?
    let hungamaDSN: String?
	let providers: [PartnerProvider]?
	let cancelled: Bool?
	let downgrade: Bool?
    let partnerDesc: String?
    let primePackDetails: PrimePackDetails?
    let subscriptionInformationDTO: SubscriptionInformationDTO?
    let ocsFlag: String?
    let contentPlayBack: Bool?
    let contentPlayBackHybrid: Bool?
    let logoutHybrid: Bool?
    let logout: Bool?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		packName = try values.decodeIfPresent(String.self, forKey: .packName)
		packId = try values.decodeIfPresent(String.self, forKey: .packId)
		packCreatedDate = try values.decodeIfPresent(String.self, forKey: .packCreatedDate)
		packPrice = try values.decodeIfPresent(String.self, forKey: .packPrice)
		expirationDate = try values.decodeIfPresent(String.self, forKey: .expirationDate)
		rechargeDue = try values.decodeIfPresent(String.self, forKey: .rechargeDue)
		amountDue = try values.decodeIfPresent(String.self, forKey: .amountDue)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		packType = try values.decodeIfPresent(String.self, forKey: .packType)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		alternatePackId = try values.decodeIfPresent(String.self, forKey: .alternatePackId)
		fsEligibility = try values.decodeIfPresent(Bool.self, forKey: .fsEligibility)
        fsTaken = try values.decodeIfPresent(Bool.self, forKey: .fsTaken)
        packSubscriptionStatus = try values.decodeIfPresent(String.self, forKey: .packSubscriptionStatus)
		providers = try values.decodeIfPresent([PartnerProvider].self, forKey: .providers)
		cancelled = try values.decodeIfPresent(Bool.self, forKey: .cancelled)
		downgrade = try values.decodeIfPresent(Bool.self, forKey: .downgrade)
        primePackDetails = try values.decodeIfPresent(PrimePackDetails.self, forKey: .primePackDetails)
        ocsFlag = try values.decodeIfPresent(String.self, forKey: .ocsFlag)
        expiredWithinSixtyDays = try values.decodeIfPresent(Bool.self, forKey: .expiredWithinSixtyDays)
        migrated = try values.decodeIfPresent(Bool.self, forKey: .migrated)
        subscriptionType = try values.decodeIfPresent(String.self, forKey: .subscriptionType)
        contentPlayBack = try values.decodeIfPresent(Bool.self, forKey: .contentPlayBack)
        logout = try values.decodeIfPresent(Bool.self, forKey: .logout)
        contentPlayBackHybrid = try values.decodeIfPresent(Bool.self, forKey: .contentPlayBackHybrid)
        largeScreenEligibility = try values.decodeIfPresent(Bool.self, forKey: .largeScreenEligibility)
        logoutHybrid = try values.decodeIfPresent(Bool.self, forKey: .logoutHybrid)
        partnerDesc = try values.decodeIfPresent(String.self, forKey: .partnerDesc)
        partnerUniqueId = try values.decodeIfPresent(String.self, forKey: .partnerUniqueId)
        dthStatus = try values.decodeIfPresent(String.self, forKey: .dthStatus)
        packCreationDate = try values.decodeIfPresent(String.self, forKey: .packCreationDate)
        subscriptionExpiryMessage = try values.decodeIfPresent(String.self, forKey: .subscriptionExpiryMessage)
        accountScreenExpiryMessage = try values.decodeIfPresent(String.self, forKey: .accountScreenExpiryMessage)
        accountSubStatus = try values.decodeIfPresent(String.self, forKey: .accountSubStatus)
        hungamaDSN = try values.decodeIfPresent(String.self, forKey: .hungamaDSN)
        subscriptionInformationDTO = try values.decodeIfPresent(SubscriptionInformationDTO.self, forKey: .subscriptionInformationDTO)
	}
}

struct SubscriptionInformationDTO: Codable {
    let subscriptionType: String?
    let migrated: Bool?
    let bingeAccountStatus: String?
    let planType: String?
    let complementaryPlan: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subscriptionType = try values.decodeIfPresent(String.self, forKey: .subscriptionType)
        bingeAccountStatus = try values.decodeIfPresent(String.self, forKey: .bingeAccountStatus)
        migrated = try values.decodeIfPresent(Bool.self, forKey: .migrated)
        planType = try values.decodeIfPresent(String.self, forKey: .planType)
        complementaryPlan = try values.decodeIfPresent(String.self, forKey: .complementaryPlan)
    }
}

struct PartnerSubscription: Codable {
	let partnerId: String?
	let partnerName: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		partnerId = try values.decodeIfPresent(String.self, forKey: .partnerId)
		partnerName = try values.decodeIfPresent(String.self, forKey: .partnerName)
	}
}

struct PartnerProvider: Codable {
	var name: String?
	let providerId: String?
	let iconUrl: String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		providerId = try values.decodeIfPresent(String.self, forKey: .providerId)
		iconUrl = try values.decodeIfPresent(String.self, forKey: .iconUrl)
	}
}

struct PrimePackDetails: Codable {
    let packStatus: Bool?
    let bundleState: String?
    let packAmount: Double?
    let title: String?
    let imageUrl: String?
    let expiryDate: String?
    let packType: String?
    let subscriptionExpiryMessage: String?
    let accountScreenExpiryMessage: String?
    let primeMessageDetails: PrimePopUpData?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        packStatus = try values.decodeIfPresent(Bool.self, forKey: .packStatus)
        bundleState = try values.decodeIfPresent(String.self, forKey: .bundleState)
        packAmount = try values.decodeIfPresent(Double.self, forKey: .packAmount)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        expiryDate = try values.decodeIfPresent(String.self, forKey: .expiryDate)
        packType = try values.decodeIfPresent(String.self, forKey: .packType)
        subscriptionExpiryMessage = try values.decodeIfPresent(String.self, forKey: .subscriptionExpiryMessage)
        accountScreenExpiryMessage = try values.decodeIfPresent(String.self, forKey: .accountScreenExpiryMessage)
        primeMessageDetails = try values.decodeIfPresent(PrimePopUpData.self, forKey: .primeMessageDetails)
    }
}


struct PrimePopUpData: Codable {
    let primeTitle1: String?
    let primeTitle2: String?
    let primeMessage1: String?
    let primeMessage2: String?
    let statusType: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        primeTitle1 = try values.decodeIfPresent(String.self, forKey: .primeTitle1)
        primeTitle2 = try values.decodeIfPresent(String.self, forKey: .primeTitle2)
        primeMessage1 = try values.decodeIfPresent(String.self, forKey: .primeMessage1)
        primeMessage2 = try values.decodeIfPresent(String.self, forKey: .primeMessage2)
        statusType = try values.decodeIfPresent(String.self, forKey: .statusType)
    }
}

//Create Account Call
struct CreateBingeAccountServerResponse: Codable {
	let code: Int?
	let message: String?
	let data: AccountDetailListResponseData?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(AccountDetailListResponseData.self, forKey: .data)
	}
}

struct LoginExistingServerResponse: Codable {
	let code: Int?
	let message: String?
	let data: AccountDetailListResponseData?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(AccountDetailListResponseData.self, forKey: .data)
	}
}

enum LoginVcDataSource {
	case title
	case segment
	case textfield
	case next
	case signUp
	case footer
	
	static let numberOfCells = [title, segment, textfield, next, signUp, footer]
	
	enum CellType {
		case title
		case segment
		case textfield
		case next
		case subscriberTextField
		case footer
		case orTextfield
        case terms
		
		static let typeOfCells = [title, segment, textfield, subscriberTextField, footer, orTextfield, terms]
		
		var cellIndentifier: String {
			switch self {
			case .title:
				return "TitleHeaderTableViewCell"
			case .segment:
				return "LoginSegmentTableViewCell"
			case .textfield:
				return "LoginTextFieldTableViewCell"
			case .next:
				return "NextButtonTableViewCell"
			case .subscriberTextField:
				return "LoginTextFieldTableViewCell"
			case .orTextfield:
				return "BAOrSeparatorTableViewCell"
			case .footer:
				return "BottomBingeLogoTableViewCell"
            case .terms:
                return "SignUpAlreadyUserTableViewCell"
			}
		}
	}
}

enum VerificationVcDataSource {
    case title
    case segment
    case textfield
//    case next
    case resendOtp
    case signUp
    case footer

    static let numberOfCells = [title, segment, textfield, resendOtp, signUp, footer]

    enum CellType {
        case title
        case segment
        case textfield
        case resetPassword
//        case next
        case resendOtp
        //case signUp
        case footer

        static let typeOfCells = [title, segment, textfield, resetPassword, resendOtp, /*signUp,*/footer]

        var cellIndentifier: String {
            switch self {
            case .title:
                return "TitleHeaderTableViewCell"
            case .segment:
                return "LoginSegmentTableViewCell"
            case .textfield:
                return "LoginVerificationInputTableViewCell"
            case .resetPassword:
                return "ForgetPasswordTableViewCell"
//            case .next:
//                return "NextButtonTableViewCell"
            case .resendOtp:
                return "ResendOtpTableViewCell"
//            case .signUp:
//                return "NewUserTableViewCell"
            case .footer:
                return "BottomBingeLogoTableViewCell"

            }
        }
    }
}

//
//  VerificationDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol LoginDataCallProtocol {
    var apiParams: [AnyHashable: Any] {get set}
    var apiEndPoint: String {get set}
	
	func generateOtpApiCall(completion: @escaping(APIServicResult<InitialLoginServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func loginValidationApiCall(completion: @escaping (APIServicResult<ValidationServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func reSendLoginOtpApiCall(completion: @escaping(APIServicResult<InitialLoginServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func forgetPasswordOtpApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
	func sidLookUpApiCall(completion: @escaping(APIServicResult<SidLookUpServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
	func baidLookUpApiCall(completion: @escaping(APIServicResult<BaidLookUpServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
	func createBingeAccountApiCall(completion: @escaping(APIServicResult<CreateBingeAccountServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

// MARK: - Extention
extension LoginDataCallProtocol {
    func loginValidationApiCall(completion: @escaping (APIServicResult<ValidationServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .loginValidation(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<ValidationServerResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
	
	func generateOtpApiCall(completion: @escaping(APIServicResult<InitialLoginServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		let target = ServiceRequestDetail.init(endpoint: .generateOtp(param: apiParams, headers: nil, endUrlString: apiEndPoint))
		return APIService().request(target) { (response: APIResult<APIServicResult<InitialLoginServerResponse>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}

    func reSendLoginOtpApiCall(completion: @escaping(APIServicResult<InitialLoginServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .generateOtp(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<InitialLoginServerResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }

    func forgetPasswordOtpApiCall(completion: @escaping(APIServicResult<UpdatePasswordResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .forgetPassword(param: apiParams, headers: nil, endUrlString: apiEndPoint))
		target.detail.headers["locale"] = "en"
		return APIService().request(target) { (response: APIResult<APIServicResult<UpdatePasswordResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
	
	func sidLookUpApiCall(completion: @escaping(APIServicResult<SidLookUpServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		var target = ServiceRequestDetail.init(endpoint: .idLookUp(param: apiParams, headers: nil, endUrlString: apiEndPoint))
		target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["platform"] = "BINGE"
		return APIService().request(target) { (response: APIResult<APIServicResult<SidLookUpServerResponse>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}
	
	func baidLookUpApiCall(completion: @escaping(APIServicResult<BaidLookUpServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		var target = ServiceRequestDetail.init(endpoint: .idLookUp(param: apiParams, headers: nil, endUrlString: apiEndPoint))
		target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["platform"] = "BINGE"
		return APIService().request(target) { (response: APIResult<APIServicResult<BaidLookUpServerResponse>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}
	
	func createBingeAccountApiCall(completion: @escaping(APIServicResult<CreateBingeAccountServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		var target = ServiceRequestDetail.init(endpoint: .createBingeUser(param: apiParams, headers: nil))
		target.detail.headers["platform"] = "ios"
		target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
		target.detail.headers["deviceToken"] = BAKeychainManager().deviceAccessToken
        target.detail.headers["deviceName"] = kDeviceModal
        target.detail.headers["deviceId"] = kDeviceId
		target.detail.headers["locale"] = "en"
		return APIService().request(target) { (response: APIResult<APIServicResult<CreateBingeAccountServerResponse>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}
		
	func loginExistingApiCall(completion: @escaping(APIServicResult<LoginExistingServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		var target = ServiceRequestDetail.init(endpoint: .login(param: apiParams, headers: nil))
		target.detail.headers["platform"] = "ios"
		target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
		target.detail.headers["deviceToken"] = BAKeychainManager().deviceAccessToken
		return APIService().request(target) { (response: APIResult<APIServicResult<LoginExistingServerResponse>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}

}

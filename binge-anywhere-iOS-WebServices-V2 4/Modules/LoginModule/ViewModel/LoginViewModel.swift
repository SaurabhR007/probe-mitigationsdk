//
//  VerificationVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol LoginViewModelProtocol {
    var dataModel: LoginDataModel {get set}
    func clientEndValidation(_ loginType: LoginType, completion: @escaping (Bool) -> Void)
    func reSendOtp(completion: @escaping(Bool, String?, Bool) -> Void)
    func generateOtp(completion: @escaping(Bool, String?, Bool) -> Void)
    func forgetPassword(isFromLogin: Bool, completion: @escaping (Bool, String?, Bool) -> Void)
    func loginValidationCall(completion: @escaping (Bool, String?, Bool) -> Void)
    func sidLookUp(completion: @escaping (Bool, String?, Bool) -> Void)
    func baidLookUp(completion: @escaping (Bool, String?, Bool) -> Void)
    func createBingeUser(completion: @escaping (Bool, String?, Int?, Bool) -> Void)
}

class LoginViewModel: LoginViewModelProtocol, LoginDataCallProtocol, ErrorHandler {
    
    var apiEndPoint: String = ""
    var newPassword: String = ""
    var verifyPassword: String = ""
    var apiParams = [AnyHashable: Any]()
    var dataModel = LoginDataModel()
    var requestToken: ServiceCancellable?
    var verificationModel: ValidationResponseData?
    var sidLookUpModel: [SubscriberDetailResponseData]?
    var createUserModel: AccountDetailListResponseData?
    var errorCode: Int?
    var errorMessage = ""
    var loginType = ""
    var authType = ""
    var valueType = ""
    
    func generateOtp(completion: @escaping(Bool, String?, Bool) -> Void) {
        switch dataModel.loginType {
        case .rmnWithOtp:
            if !isValidContact() {
                completion(false, kValidPhoneNumber, false)
                return
            }
            apiEndPoint = "rmn/" + dataModel.rmn! + "/otp"
            dataModel.loginType = .rmnWithOtp
        case .sidWithOtp:
            if !isValidSid() {
                completion(false, kValidSID, false)
                return
            }
            apiParams["isPassword"] = "false"
            apiEndPoint = "subscriber/" + dataModel.sid! + "/otp"
        case .sidWithPwd:
            if !isValidSid() {
                completion(false, kValidSID, false)
                return
            }
            apiParams["isPassword"] = "true"
            apiEndPoint = "subscriber/" + dataModel.sid! + "/otp"
        case .none:
            completion (false, "Some error occured", true)
            return
        }
        requestToken = generateOtpApiCall(completion: { (configData, error)  in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if let rmn = _configData.parsed.data?.rmn {
                            self.dataModel.maskedNumberToShow = rmn
                            self.dataModel.maskedNumber = _configData.parsed.data?.maskedNumber
                        }
                        completion(true, nil, false)
                    } else {
                        print("+++++++++++ 1 +++++++++++")
                        if _configData.parsed.code == 3006 {
                            completion(false, _configData.parsed.message, true)
                        } else {
                            if _configData.serviceResponse.statusCode != 200 {
                                self.errorCode = _configData.serviceResponse.statusCode
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, true)
                                } else {
                                    completion(false, "Some Error Occurred", true)
                                }
                            } else {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, false)
                                } else {
                                    completion(false, "Some Error Occurred", false)
                                }
                            }
                        }
                    }
                } else {
                    self.errorCode = _configData.serviceResponse.statusCode
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    
    func loginValidationCall(completion: @escaping (Bool, String?, Bool) -> Void) {
        createValidationParams()
        switch dataModel.loginType {
        case .sidWithOtp:
            apiEndPoint = "subscriber/validate/otp"
            loginType = "SID"
            authType = "OTP"
            valueType = dataModel.sid ?? ""
            if !isValidOtp() {
                completion(false, "Invalid OTP", false)
                return
            }
        case .rmnWithOtp:
            apiEndPoint = "validate/otp"
            loginType = "RMN"
            authType = "OTP"
            valueType = dataModel.rmn ?? ""
            if !isValidOtp() {
                completion(false, "Invalid OTP", false)
                return
            }
        case .sidWithPwd:
            apiEndPoint = "validate/pwd"
            loginType = "SID"
            authType = "Password"
            valueType = dataModel.sid ?? ""
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginPassword.rawValue)
            if !isValidPwd() {
                completion(false, "Invalid Password", false)
                return
            }
        default: return
        }
        requestToken = loginValidationApiCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    print(statusCode)
                    self.errorCode = statusCode
                    if statusCode == 0 {
                        if let validationData = _configData.parsed.data {
                            self.verificationModel = validationData
                            BAKeychainManager().userAccessToken = validationData.userAuthenticateToken ?? ""
                            BAKeychainManager().deviceAccessToken = validationData.deviceAuthenticateToken ?? ""
                        }
                        switch self.dataModel.loginType {
                        case .rmnWithOtp:
                            self.sidLookUp(completion: { (flagValue, message, isApiError) in
                                completion(flagValue, message, false)
                            })
                            
                        case .sidWithOtp:
                            self.baidLookUp(completion: { (flagValue, message, isApiError) in
                                completion(flagValue, message, false)
                            })
                        case .sidWithPwd:
                            self.baidLookUp(completion: { (flagValue, message, isApiError) in
                                completion(flagValue, message, false)
                            })
                        default: break
                        }
                    } else {
                        print("________1_____________")
                        if _configData.serviceResponse.statusCode != 200 {
                            self.errorCode = _configData.serviceResponse.statusCode
                            print("________2_____________")
                            if let errorMessage = _configData.parsed.message {
                                print("________3_____________")
                                completion(false, errorMessage, true)
                            } else {
                                print("________4_____________")
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            print("________5_____________")
                            if let errorMessage = _configData.parsed.message {
                                print("________6_____________")
                                completion(false, errorMessage, false)
                            } else {
                                print("________7_____________")
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else if _configData.serviceResponse.statusCode == 429 {
                    self.errorCode = _configData.serviceResponse.statusCode
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func sidLookUp (completion: @escaping (Bool, String?, Bool) -> Void) {
        apiEndPoint = "rmn/" + dataModel.rmn!
        if !isValidContact() {
            completion(false, kValidPhoneNumber, false)
            return
        }
        requestToken = sidLookUpApiCall(completion: { (configData, error)  in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if let sidList = _configData.parsed.data {
                            self.saveLookupResponse(data: sidList)
                            if sidList.count == 1 {
                                //Only one SID found with only one BaID so we will open home now
                                if let accountList = sidList.first?.accountDetailList {
                                    if accountList.count == 1 {
                                        if sidList.first?.accountDetailList?.first?.baId == nil {
                                            BAKeychainManager().sId = String(accountList.first?.subscriberId ?? 0)
                                            BAKeychainManager().dsn = accountList.first?.deviceSerialNumber ?? ""
                                            completion(true, "CREATEFIRESTICK", false)
                                        } else {
                                            self.dataModel.sid = String(accountList.first?.subscriberId ?? 0)
                                            self.dataModel.baid = String(accountList.first?.baId ?? 0)
                                            self.initiateLogin(completion: { (flagValue, message, isApiError) in
                                                completion(flagValue, message, false)
                                            })
                                        }
                                    } else if accountList.count > 1 {
                                        //Only one SID found with multiple BAId so we will show BAId selection screen
                                        BAKeychainManager().sId = String(accountList.first?.subscriberId ?? 0)
                                        completion(true, "BAID", false)
                                    } else {
                                        //Only one SID found with no BAId
                                        //Create BaId API will be called
                                       // BAKeychainManager().sId = String(accountList.first?.subscriberId ?? 0)
                                        if sidList.first?.loginErrorMessage != nil || !(sidList.first?.loginErrorMessage?.isEmpty ?? true) {
                                            if sidList.first?.accountStatus == kPending {
                                                self.errorMessage = sidList.first?.loginErrorMessage ?? ""
                                                completion(true, sidList.first?.loginErrorMessage, false)
                                            } else {
                                                completion(true, sidList.first?.loginErrorMessage, false)
                                            }
                                        } else {
                                            BAKeychainManager().sId = String(accountList.first?.subscriberId ?? 0)
                                            completion(true, "CREATE", false)
                                        }
                                        /*
                                        BAKeychainManager().sId = String(accountList.first?.subscriberId ?? 0)
                                        completion(true, "CREATE", false)
                                        */
                                    }
                                } else {
                                    BAKeychainManager().sId = String(sidList.first?.subscriberId ?? 0)
                                    completion(true, "CREATE", false)
                                }
                            } else if sidList.count > 1 {
                                //Multiple SID Found now we will open SID selection screen
                                completion(true, "SID", false)
                            } else {
                                //No SID found
                                completion(false, "No SID found linked to the RMN", false)
                            }
                        } else {
                            completion(false, "Invalid data", false)
                        }
                    } else {
                        print("===========1=============")
                        if _configData.serviceResponse.statusCode != 200 {
                            self.errorCode = _configData.serviceResponse.statusCode
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else {
                    self.errorCode = _configData.serviceResponse.statusCode
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func baidLookUp (completion: @escaping (Bool, String?, Bool) -> Void) {
        apiEndPoint = "sid/" + dataModel.sid!
        requestToken = baidLookUpApiCall(completion: { (configData, error)  in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    self.errorCode = statusCode
                    if statusCode == 0 {
                        if let baidData = _configData.parsed.data {
                            BAKeychainManager().fullName = baidData.subscriberName ?? ""
                            BAKeychainManager().baIdCount = (baidData.accountDetailList?.count ?? 0) > 0 ? (baidData.accountDetailList?.count ?? 0) : 1
                            self.saveLookupResponse(data: [baidData])
                            if baidData.accountDetailList?.count == 1 {
                                //Only one baid found open home
                                if baidData.accountDetailList?.first?.baId == nil {
                                    BAKeychainManager().sId = String(self.sidLookUpModel?.first?.subscriberId ?? 0)
                                    BAKeychainManager().dsn = baidData.accountDetailList?.first?.deviceSerialNumber ?? ""
                                    completion(true, "CREATEFIRESTICK", false)
                                } else {
                                    self.dataModel.baid = String(baidData.accountDetailList?.first?.baId ?? 0)
                                    self.initiateLogin(completion: { (flagValue, message, isApiError) in
                                        completion(flagValue, message, false)
                                    })
                                }
                            } else if baidData.accountDetailList?.count ?? 0 > 1 {
                                //Multiple baid found open Baid selection screen
                                completion(true, "BAID", false)
                            } else {
                                if self.sidLookUpModel?.first?.loginErrorMessage != nil || !(self.sidLookUpModel?.first?.loginErrorMessage?.isEmpty ?? true) {
                                    completion(true, self.sidLookUpModel?.first?.loginErrorMessage, false)
                                } else {
                                    BAKeychainManager().sId = String(self.sidLookUpModel?.first?.subscriberId ?? 0)
                                    completion(true, "CREATE", false)
                                }
                                //No baId found, create API and screen will be opened
                            }
                        } else {
                            completion(false, "Invalid data", false)
                        }
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            self.errorCode = _configData.serviceResponse.statusCode
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else if _configData.serviceResponse.statusCode == 429 {
                    self.errorCode = _configData.serviceResponse.statusCode
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func clientEndValidation(_ loginType: LoginType, completion: @escaping (Bool) -> Void) {
        switch loginType {
        case .rmnWithOtp, .sidWithOtp:
            guard let otpString = dataModel.otp else {
                completion(false)
                return
            }
            if otpString.length < 6 {
                completion(false)
            }
            completion(true)
        default: completion(true)
        }
    }
    
    func reSendOtp(completion: @escaping(Bool, String?, Bool) -> Void) {
        switch dataModel.loginType {
        case .rmnWithOtp:
            apiEndPoint = "rmn/" + dataModel.rmn! + "/otp"
            if !isValidContact() {
                completion(false, kValidPhoneNumber, false)
                return
            }
        case .sidWithOtp:
            apiParams = [ : ]
            if !isValidSid() {
                completion(false, kValidSID, false)
                return
            }
            apiParams["isPassword"] = "false"
            apiEndPoint = "subscriber/" + dataModel.sid! + "/otp"
        case .sidWithPwd:
            apiParams = [ : ]
            if !isValidSid() {
                completion(false, kValidSID, false)
                return
            }
            apiParams["isPassword"] = "true"
            apiEndPoint = "subscriber/" + dataModel.sid! + "/otp"
        default:
            completion (false, "Empty value", false)
            return
        }
        requestToken = reSendLoginOtpApiCall(completion: { (configData, error)  in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if let rmn = _configData.parsed.data?.rmn {
                            self.dataModel.maskedNumberToShow = rmn
                        }
                        completion(true, _configData.parsed.message, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            self.errorCode = _configData.serviceResponse.statusCode
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else {
                    self.errorCode = _configData.serviceResponse.statusCode
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func forgetPassword(isFromLogin: Bool, completion: @escaping (Bool, String?, Bool) -> Void) {
        apiEndPoint = (dataModel.sid ?? "") + "/password"
        if isFromLogin {
            apiParams = APIParams()
            // Send API params as nil
        } else {
            createForgotAPIParams()
        }
        requestToken = forgetPasswordOtpApiCall { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 || statusCode == 6015 || statusCode == 6035 || statusCode == 6027 {
                        self.errorCode = statusCode
                        completion(true, _configData.parsed.message, false)
                    } else if statusCode == 6013 {
                        self.errorCode = statusCode
                        completion(true, _configData.parsed.message, false)
                    } else if statusCode == 429 {
                        self.errorCode = statusCode
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else {
                    self.errorCode = _configData.serviceResponse.statusCode
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        }
    }
    
    func initiateLogin(completion: @escaping (Bool, String?, Bool) -> Void) {
        createLoginParams()
        requestToken = loginExistingApiCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if let loginData = _configData.parsed.data {
                            self.saveLoginResponse(data: loginData)
                            completion(true, "HOME", false)
                        } else {
                            completion(false, "Invalid data", false)
                        }
                    } else {
                        if statusCode == 200007 {
                            self.errorCode = statusCode
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.maxDeviceLimit.rawValue, properties: [MixpanelConstants.ParamName.value.rawValue: self.dataModel.sid ?? ""])
                        }
                        if let errorMessage = _configData.parsed.message {
                            completion(false, errorMessage, false)
                        } else {
                            completion(false, "Some Error Occurred", false)
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else if _configData.serviceResponse.statusCode == 429 {
                    self.errorCode = _configData.serviceResponse.statusCode
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func createBingeUser(completion: @escaping (Bool, String?, Int?, Bool) -> Void) {
        createNewAccountParams()
        requestToken = createBingeAccountApiCall(completion: { (configData, error)  in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if let createdAccountData = _configData.parsed.data {
                            self.saveLoginResponse(data: createdAccountData)
                            completion(true, nil, statusCode, false)
                        } else {
                            completion(false, "Invalid data", statusCode, false)
                        }
                    } else {
                        if let errorMessage = _configData.parsed.message {
                            completion(false, errorMessage, statusCode, false)
                        } else {
                            completion(false, "Some Error Occurred", statusCode, false)
                        }
                    }
                } else {
                    self.errorCode = _configData.serviceResponse.statusCode
                    completion(false, "Some Error Occurred", nil, true)
                }
            } else if configData?.serviceResponse.statusCode == 429 {
                self.errorCode = configData?.serviceResponse.statusCode
            } else if let error = error {
                completion(false, error.error.localizedDescription, nil, true)
            } else {
                completion(false, "Some Error Occurred", nil, true)
            }
        })
    }
    
    func createValidationParams() {
        apiParams = [ : ]
        switch dataModel.loginType {
        case .rmnWithOtp:
            apiParams["rmn"] = dataModel.rmn ?? ""
            apiParams["otp"] = dataModel.otp ?? "000000"
        case .sidWithOtp:
            apiParams["sid"] = dataModel.sid ?? ""
            apiParams["maskedNumber"] = dataModel.maskedNumber ?? ""
            apiParams["otp"] = dataModel.otp ?? "000000"
        case .sidWithPwd:
            apiParams["sid"] = dataModel.sid ?? ""
            apiParams["pwd"] = dataModel.password
        default: break
        }
    }
    
    func createNewAccountParams() {
        apiParams = [ : ]
        apiParams["email"] = dataModel.email
        apiParams["eulaChecked"] = true//dataModel.eulaChecked
        apiParams["login"] = dataModel.otp ?? "000000"
        apiParams["dsn"] = BAKeychainManager().dsn
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["isHybrid"] = true
        switch self.dataModel.loginType {
        case .rmnWithOtp, .sidWithOtp:
            apiParams["login"] = "OTP"//dataModel.otp ?? "000000"
            BAKeychainManager().loginType = "OTP"
        case .sidWithPwd:
            apiParams["login"] = "PASSWORD"//dataModel.password
            BAKeychainManager().loginType = "PASSWORD"
        default: break
        }
    }
    
    func createForgotAPIParams() {
        apiParams = [ : ]
        apiParams["confirmPwd"] = dataModel.password
        apiParams["newPwd"] = dataModel.verifyPassword//dataModel.eulaChecked
        
    }
    
    func createLoginParams() {
        apiParams = [ : ]
        apiParams["baId"] = dataModel.baid ?? ""
        apiParams["sid"] = dataModel.sid ?? ""
        apiParams["isHybrid"] = true
        switch dataModel.loginType {
        case .rmnWithOtp, .sidWithOtp:
            apiParams["loginType"] = "OTP"
            BAKeychainManager().loginType = "OTP"
        case .sidWithPwd:
            apiParams["loginType"] = "PASSWORD"
            BAKeychainManager().loginType = "PASSWORD"
        default: break
        }
    }
    
    func saveLookupResponse(data: [SubscriberDetailResponseData]?) {
        self.sidLookUpModel = data
    }
    
    func saveLoginResponse(data: AccountDetailListResponseData) {
        self.createUserModel = data
        BAKeychainManager().acccountDetail = data
        BAKeychainManager().baId = String(data.baId ?? 0)
        BAKeychainManager().sId = String(data.subscriberId ?? 0)
        BAKeychainManager().profileId = data.defaultProfile ?? ""
        BAKeychainManager().userAccessToken = data.userAuthenticateToken ?? ""
        BAKeychainManager().deviceAccessToken = data.deviceAuthenticateToken ?? ""
        BAKeychainManager().mobileNumber = self.dataModel.rmn ?? ""
        BAKeychainManager().aliasName = data.aliasName ?? ""
        BAKeychainManager().email = data.emailId ?? ""
        //callAnalyticsDataAfterLogin()
        callAnalyticsData()
    }
    
    func callAnalyticsData() {
        MoengageManager.shared.userUniqueId()
        MixpanelManager.shared.saveMixPanelDistinctID()
        
        MixpanelManager.shared.registerPeopleProperties()
        //MixpanelManager.shared.registerSuperProperties()
        MoengageManager.shared.setUserAttributes()
    }
    
//    func callAnalyticsDataAfterLogin() {
//        MoengageManager.shared.userUniqueId()
//        MixpanelManager.shared.createAliasDistinctID()
//
//        MixpanelManager.shared.registerPeopleProperties()
//        MoengageManager.shared.setUserAttributes()
//    }
    
    func isValidContact() -> Bool {
        guard dataModel.rmn?.isValidContact ?? false else {
            return false
        }
        return true
    }
    
    func isValidSid() -> Bool {
        guard dataModel.sid?.count == 10 else {
            return false
        }
        return true
    }
    
    func isValidOtp() -> Bool {
        guard dataModel.otp?.count == 6 else {
            return false
        }
        return true
    }
    
    func isValidPwd() -> Bool {
        guard dataModel.password.count >= 8 else {
            return false
        }
        return true
    }
}

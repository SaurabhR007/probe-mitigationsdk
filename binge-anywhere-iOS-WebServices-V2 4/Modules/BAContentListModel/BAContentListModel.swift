//
//  BAContentListModel.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 16/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

class BAContentListModel: Codable {
    
    var id: Int?
    var title: String?
    var imageUrlWeb: String?
    var imageUrlApp: String?
    var image: String?
    var imageUrl: String?
    var platform: String?
    var imageUrlDongle: String?
    var contentType: String?
    var contentShowType: String?
    var subTitles: [String]?
    var position: Int?
    var parentContentType: String?
    var contractName: String?
    var entitlements: [String]?
    var linearBroadcastStartDate: String?
    var duration: Int?
    var seriesParentId: String?
    var seriescontentType: String?
    var seriesTitle: String?
    var seriesimage: String?
    var seriesshowType: String?
    var genre: [String]?
    var language: [String]?
    var allowedOnBrowsers: Bool?
    var provider: String?
    var providerContentId: String?
    var blackOut: Bool?
    var pageType: String?
    var categoryType: String?
    var rentalPrice: String?
    var rentalExpiry: Int?
    var rentalStatus: String?
    var summary: String?
    var secondsWatched: Int?
    var durationInSeconds: Int?
    var continueWatching = false
    var linkUrl: String?
    var description: String?
    var contentId: Int?
    var airedDate: Int?
    var suggestorType: String?
    var posterImage: String?
    var boxCoverImage: String?

    init() {
    }

    
    required   init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        contentId = try values.decodeIfPresent(Int.self, forKey: .contentId)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        imageUrlWeb = try values.decodeIfPresent(String.self, forKey: .imageUrlWeb)
        imageUrlApp = try values.decodeIfPresent(String.self, forKey: .imageUrlApp)
        imageUrlDongle = try values.decodeIfPresent(String.self, forKey: .imageUrlDongle)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        contentShowType = try values.decodeIfPresent(String.self, forKey: .contentShowType)
        posterImage = try values.decodeIfPresent(String.self, forKey: .posterImage)
        boxCoverImage = try values.decodeIfPresent(String.self, forKey: .boxCoverImage)
        position = try values.decodeIfPresent(Int.self, forKey: .position)
        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
        genre = try values.decodeIfPresent([String].self, forKey: .genre)
        language = try values.decodeIfPresent([String].self, forKey: .language)
        allowedOnBrowsers = try values.decodeIfPresent(Bool.self, forKey: .allowedOnBrowsers)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
        blackOut = try values.decodeIfPresent(Bool.self, forKey: .blackOut)
        categoryType = try values.decodeIfPresent(String.self, forKey: .categoryType)
        rentalPrice = try values.decodeIfPresent(String.self, forKey: .rentalPrice)
        rentalExpiry = try values.decodeIfPresent(Int.self, forKey: .rentalExpiry)
        rentalStatus = try values.decodeIfPresent(String.self, forKey: .rentalStatus)
        summary = try values.decodeIfPresent(String.self, forKey: .summary)
        pageType = try values.decodeIfPresent(String.self, forKey: .pageType)
        secondsWatched = try values.decodeIfPresent(Int.self, forKey: .secondsWatched)
        durationInSeconds = try values.decodeIfPresent(Int.self, forKey: .durationInSeconds)
        linkUrl = try values.decodeIfPresent(String.self, forKey: .linkUrl)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        linearBroadcastStartDate = try values.decodeIfPresent(String.self, forKey: .linearBroadcastStartDate)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        platform = try values.decodeIfPresent(String.self, forKey: .platform)
        suggestorType = try values.decodeIfPresent(String.self, forKey: .suggestorType)
        parentContentType = try values.decodeIfPresent(String.self, forKey: .parentContentType)
        seriesParentId = try values.decodeIfPresent(String.self, forKey: .seriesParentId)
        seriescontentType = try values.decodeIfPresent(String.self, forKey: .seriescontentType)
        seriesTitle = try values.decodeIfPresent(String.self, forKey: .seriesTitle)
        seriesimage = try values.decodeIfPresent(String.self, forKey: .seriesimage)
        seriesshowType = try values.decodeIfPresent(String.self, forKey: .seriesshowType)
        
        
        // these are keys whose value are of two different data types so type checking is performed here
        if let _airedDate = try? values.decode(String.self, forKey: .airedDate) {
            airedDate = Int(_airedDate)
        } else if let _airedDate = try? values.decode(Int.self, forKey: .airedDate) {
            airedDate = _airedDate
        } else {
            airedDate = nil
        }
        
        if let _subTitles = try? values.decodeIfPresent([String].self, forKey: .subTitles) {
            subTitles = _subTitles
        } else if let _subTitles = try? values.decodeIfPresent(String.self, forKey: .subTitles) {
            subTitles = []
            subTitles?.append(_subTitles)
        } else {
            subTitles = nil
        }
        
        // As per discussion with Ajay duration is converted to int and formating Int value in millisecond is done here to make the consistency of duration value for all model
        if let _duration = try? values.decodeIfPresent(String.self, forKey: .duration) {
            duration = Int(_duration.secondFromString*1000)
        } else if let _duration = try? values.decodeIfPresent(Int.self, forKey: .duration) {
            duration = _duration
        } else {
            duration = nil
        }
        
        if let _id = try? values.decodeIfPresent(Int.self, forKey: .id) {
            id = _id
        } else if let _id = try? values.decodeIfPresent(String.self, forKey: .id) {
            id = Int(_id)
        } else {
            id = nil
        }
    }
}

//
//  InstallationScheduleViewController.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 31/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol cellTappedDelagate {
    func expandTapped()
}

class InstallationScheduleViewController: BaseViewController {
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var proceedButton: CustomButton!
    @IBOutlet weak var notNowButton: CustomButton!
    
    var testCalendar = Calendar(identifier: Calendar.Identifier.iso8601)
    var currentDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var placeHolderDate: String = "DD / MM / YYYY"
    var curDate: String = ""
    var selectedTimeSlot: Slots?
    var installOptionIndex: Int = -1
    var slotdelegate: cellTappedDelagate?
    var userDetailData: AddressDataModel?
    var getSlotVM: FireStickViewModel?
    var confirmSlotAndWorkOrderVM: FireStickViewModel?
    var slotDataSource: [Slots] = []
    var isDoItYourself : Bool = false
    var dateMsgForPopUp : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentDate = Date()
        if userDetailData?.ocsFlag?.compare("N") == .orderedSame {
            slotDataSource = userDetailData?.slotSuggestions ?? []
        }
        configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
        configureUI()
    }
    
    func configureUI(){
        placeHolderDate = "DD / MM / YYYY"
        proceedButton.style(.tertiary, isActive: false)
        proceedButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth), color: .white)
        //notNowButton.style(.other)
        notNowButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth), color: .BABlueColor)
        notNowButton.backgroundColor = .clear
        notNowButton.tintColor = .BABlueColor
        notNowButton.underline()
        configureTabelView()
    }
    
    func configureTabelView(){
        
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.delegate = self 
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
        tableView.register(UINib(nibName: kTitleHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: kTitleHeaderTableViewCell)
        tableView.register(UINib(nibName: kDateFieldTableViewCell, bundle: nil), forCellReuseIdentifier: kDateFieldTableViewCell)
        tableView.register(UINib(nibName: kTimeScheduleTableViewCell, bundle: nil), forCellReuseIdentifier: kTimeScheduleTableViewCell)
    }
    
    @objc func backButtonAction() {
        print("back tapped")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func proceedTapped(_ sender: Any) {
        if userDetailData?.ocsFlag?.compare("Y") == .orderedSame {
            confirmSlotViewModelSetup()
        } else {
            getWorkOrderResponse()
        }
    }
    
    @IBAction func notNowTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setDate() {
        curDate = currentDate.asString(dateFormat: "dd / MM / YYYY")
        let indexPath = IndexPath(row: 2, section: 0)
        let cell =  self.tableView.cellForRow(at: indexPath) as? DateFieldTableViewCell
        cell?.dateLabel.text = curDate
        placeHolderDate = curDate
    }
    
    private func shouldEnableProceedButton() {
        if !curDate.isEmpty && selectedTimeSlot != nil {
            proceedButton.style(.primary, isActive: true)
        } else {
            proceedButton.style(.tertiary, isActive: false)
        }
    }
    
    private func resetTimeSlotWithData(_ data: [Slots]?) {
        self.slotDataSource = data ?? []
        self.selectedTimeSlot = nil
        shouldEnableProceedButton()
        self.tableView.reloadData()
    }
    
    func getSlotViewModelSetup() {
        self.getSlotVM = FireStickViewModel(repo: FireStickTvRepo(), apiEndPoint: "get/slot")
        getSlotResponse()
    }
    
    func getSlotResponse() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if getSlotVM != nil {
                getSlotVM?.getSlot(param: createSlotParams(isConfirmSlot: false), completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.resetTimeSlotWithData(self.getSlotVM?.getSlotData?.slotSuggestions)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                        self.resetTimeSlotWithData(nil)
                    } else {
                        self.apiError(message, title: "")
                        self.resetTimeSlotWithData(nil)
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    func confirmSlotViewModelSetup() {
        self.confirmSlotAndWorkOrderVM = FireStickViewModel(repo: FireStickTvRepo(), apiEndPoint: "confirm/slot")
        confirmSlotResponse()
    }
    
    func confirmSlotResponse() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if confirmSlotAndWorkOrderVM != nil {
                confirmSlotAndWorkOrderVM?.confirmSlot(param: createSlotParams(isConfirmSlot: true), completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.getWorkOrderResponse()
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    private func createSlotParams(isConfirmSlot: Bool) -> APIParams {
        var params = APIParams()
        params["contactPoint"] = ["name": BAKeychainManager().sId, "number": BAKeychainManager().sId]
        let city: String = userDetailData?.city ?? ""
        let pinCode: String = userDetailData?.pincode ?? ""
        let state: String = userDetailData?.state ?? ""
        params["locationAddress"] = ["city": city, "formattedAddress": pinCode, "id": "", "pincode": pinCode, "state": state]
        let date = currentDate.asString(dateFormat: "dd-MM-YYYY")
        let startSlot: String = timeConversion24(time12: selectedTimeSlot?.start)
        let endSlot: String = timeConversion24(time12: selectedTimeSlot?.end)
        params["slot"] = ["end": "\(date) \(endSlot)", "start": "\(date) \(startSlot)"]
        params["teamId"] = userDetailData?.serviceRegionId
        if isConfirmSlot {
            params["taskId"] = getSlotVM?.getSlotData?.taskId?.taskId
        }
        return params
    }
    
    private func timeConversion24(time12: String?) -> String {
        let dateAsString = time12
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US_POSIX")
        df.dateFormat = "h:mm a"
        let date = df.date(from: dateAsString ?? "12:00 AM")
        
        df.dateFormat = "HH:mm:ss"
        guard let _date = date else { return "00:00:00" }
        let time24 = df.string(from: _date)
        return time24
    }
    
    func getWorkOrderResponse() {
        self.confirmSlotAndWorkOrderVM = FireStickViewModel(repo: FireStickTvRepo(), apiEndPoint: "create/wo")
        fetchWorkOrderResponse()
    }
    
    func fetchWorkOrderResponse(){
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if confirmSlotAndWorkOrderVM != nil {
                
                confirmSlotAndWorkOrderVM?.fireStickWOCompletion(param: createWorkOrderParams(), completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        print("Successfully Called Api")
                        let message  = self.confirmSlotAndWorkOrderVM?.workOrderData
                        self.openConfirmationPopup(message: message)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    private func createWorkOrderParams() -> APIParams {
        var params = APIParams()
        params["dyi"] = isDoItYourself //false
        params["subscriberId"] = BAKeychainManager().sId
        let date = currentDate.asString(dateFormat: "dd-MM-YYYY")
        let startSlot: String = timeConversion24(time12: selectedTimeSlot?.start)
        let endSlot: String = timeConversion24(time12: selectedTimeSlot?.end)
        params["slotEndTime"] = "\(date) \(endSlot)"
        params["slotStartTime"] = "\(date) \(startSlot)"
        return params
    }
    
    private func openConfirmationPopup(message: String?) {
        deliveryPopupWithReschedule(configurePopupMsg(), isDoItYourself) { _ in
                self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    // fixed code for removing optional string which got appended with start and end date
    func configurePopupMsg() -> String {
        if let selectedTimeSlotEnd = selectedTimeSlot?.end, let seletedTimeSlotStart = selectedTimeSlot?.start {
            let popupMsg = " \(dateMsgForPopUp) \n \(seletedTimeSlotStart) - \(selectedTimeSlotEnd) \n" + "Details will be sent via SMS to your Registered Mobile Number."
            return popupMsg
        }
        let popupMsg = " \(dateMsgForPopUp) \n \(String(describing: selectedTimeSlot?.start)) - \(String(describing: selectedTimeSlot?.end)) \n" + "Details will be sent via SMS to your Registered Mobile Number."
        return popupMsg
    }
}

extension InstallationScheduleViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
            cell?.configureIndicator(number: 2, selectedIndex: 1)
            cell?.selectionStyle = .none
            return cell!
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
            cell?.configureCell(source: .installSchedule)
            cell?.selectionStyle = .none
            return cell!
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DateFieldTableViewCell") as? DateFieldTableViewCell
            cell?.dateLabel.text = placeHolderDate
            return cell!
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeScheduleTableViewCell") as? TimeScheduleTableViewCell
            cell?.delegate = self
            self.slotdelegate = cell
            cell?.setupSlotsDataSource(slotDataSource)
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2{
            let xibView = Bundle.main.loadNibNamed("CalendarPopUp", owner: nil, options: nil)?[0] as! CalendarPopUp
            xibView.calendarDelegate = self
            xibView.selected = currentDate
            xibView.startDate = Calendar.current.date(byAdding: .month, value: -12, to: currentDate)!
            xibView.endDate = Calendar.current.date(byAdding: .year, value: 2, to: currentDate)!
            PopupContainer.generatePopupWithView(xibView).show()
            self.tableView.reloadData()
        }
        
        if indexPath.row == 3{
            slotdelegate?.expandTapped()
        }
    }
    
}

extension InstallationScheduleViewController: CalendarPopUpDelegate {
    func dateChaged(date: Date, dateMsg: String) {
        currentDate = date
        dateMsgForPopUp = dateMsg
        if userDetailData?.ocsFlag == "Y" {
            let cell: TimeScheduleTableViewCell? = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? TimeScheduleTableViewCell
            cell?.timeLabel.text = "Please select a time slot"
            resetTimeSlotWithData(nil)
            getSlotViewModelSetup()
        } else {
            shouldEnableProceedButton()
        }
    }
}

extension InstallationScheduleViewController: TimeScheduleTableViewCellDelegate {
    func reloadRowData(indexPath: Int) {
        tableView.reloadData()
    }
    
    func timeSlotDidSelect(_ slot: Slots) {
        selectedTimeSlot = slot
        shouldEnableProceedButton()
    }
}

//
//  FireTvViewController.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

struct DataModel{
    var selectedIndex: Int = -1
}

class FireTvViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var proceedButton: CustomButton!
    
    @IBOutlet weak var notNowButton: CustomButton!
    
    var optionSelected: Bool = false
    var indx: DataModel = DataModel()
    
    var fireStickVM: FireStickViewModel?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
        configureUI()
    }
    
    func configureUI(){
        proceedButton.style(.tertiary, isActive: false)
        //proceedButton.isEnabled = false
        proceedButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .white)
        //notNowButton.style(.other)
        notNowButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BABlueColor)
        notNowButton.backgroundColor = .clear
        notNowButton.tintColor = .BABlueColor
        notNowButton.underline()
        configureTabelView()
    }
    
    
    func configureTabelView(){
        
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        //tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
        tableView.register(UINib(nibName: kHeadingTableViewCell, bundle: nil), forCellReuseIdentifier: kHeadingTableViewCell)
        tableView.register(UINib(nibName: kInfoTableViewCell, bundle: nil), forCellReuseIdentifier: kInfoTableViewCell)
        tableView.register(UINib(nibName: kInstallOptionsTableViewCell, bundle: nil), forCellReuseIdentifier: kInstallOptionsTableViewCell)
        tableView.register(UINib(nibName: kMoveToNextTableViewCell, bundle: nil), forCellReuseIdentifier: kMoveToNextTableViewCell)
    }
    
    func getAddressDetail(){
        self.fireStickVM = FireStickViewModel(repo: FireStickTvRepo(), apiEndPoint: "subscriber/profile/address")
        fetchAddressDetail()
    }
    
    func fetchAddressDetail(){
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if fireStickVM != nil {
                fireStickVM?.getUserAddress(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.pushToAddressScreen()
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    private func pushToAddressScreen() {
        let ConfirmDetailVC: ConfirmDetailViewController  = UIStoryboard.loadViewController(storyBoardName: "FireTV", identifierVC: "ConfirmDetailViewController", type: ConfirmDetailViewController.self)
        ConfirmDetailVC.installOptionIndex = indx.selectedIndex
        ConfirmDetailVC.passAddressModel(modal: fireStickVM?.userAddressDetail)
        self.navigationController?.pushViewController(ConfirmDetailVC, animated: true)
    }
    
    @objc func backButtonAction(){
        print("back tapped")
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    @IBAction func proceedTapped(_ sender: Any) {
        getAddressDetail()
    }
    

    @IBAction func notNowTapped(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    

}

extension FireTvViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
            cell?.selectionStyle = .none
            return cell!
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingTableViewCell") as? HeadingTableViewCell
            cell?.selectionStyle = .none
            return cell!
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell") as? InfoTableViewCell
            cell?.selectionStyle = .none
            return cell!
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstallOptionsTableViewCell") as? InstallOptionsTableViewCell
            cell?.configureCell(index: 3, selected: (indx.selectedIndex == indexPath.row))
            cell?.selectionStyle = .none
            //cell?.optionSelectionButton.addTarget(self, action: #selector(buttonChecked), for: .touchUpInside)
            return cell!
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstallOptionsTableViewCell") as? InstallOptionsTableViewCell
            cell?.configureCell(index: 4,selected:(indx.selectedIndex == indexPath.row))
            cell?.selectionStyle = .none
            //cell?.optionSelectionButton.addTarget(self, action: #selector(buttonChecked), for: .touchUpInside)
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //indx.selectedIndex = (indx.selectedIndex==indexPath.row ? -1 : indexPath.row)
        indx.selectedIndex = indexPath.row
        if indx.selectedIndex == 3 || indx.selectedIndex == 4 {
            proceedButton.style(.primary, isActive: true)
            //self.proceedButton.isEnabled = true
            
        }
        tableView.reloadData()
    }
}

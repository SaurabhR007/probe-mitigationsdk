//
//  ConfirmDeatailViewController.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 22/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class ConfirmDetailViewController: BaseViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var proceedButton: CustomButton!
    @IBOutlet weak var notNowButton: CustomButton!
    
    var installOptionIndex: Int = -1
    var userDetailData: AddressDataModel?
    var fireStickVM: FireStickViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
        configureUI()
    }
    
    func configureUI(){
        proceedButton.style(.primary)
        proceedButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .white)
        //notNowButton.style(.other)
        notNowButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BABlueColor)
        notNowButton.backgroundColor = .clear
        notNowButton.tintColor = .BABlueColor
        notNowButton.underline()
        configureTabelView()
    }
    
    func configureTabelView(){
           
           tableView.backgroundColor = .BAdarkBlueBackground
           tableView.delegate = self
           tableView.dataSource = self
           tableView.rowHeight = UITableView.automaticDimension
           tableView.allowsSelection = false
           tableView.separatorStyle = .none
           tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
           tableView.register(UINib(nibName: kTitleHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: kTitleHeaderTableViewCell)
           tableView.register(UINib(nibName: kPersonalInfoTableViewCell, bundle: nil), forCellReuseIdentifier: kPersonalInfoTableViewCell)
           tableView.register(UINib(nibName: kInfoTableViewCell, bundle: nil), forCellReuseIdentifier: kInfoTableViewCell)
           tableView.register(UINib(nibName: kOptionalInfoTableViewCell, bundle: nil), forCellReuseIdentifier: kOptionalInfoTableViewCell)
           tableView.register(UINib(nibName: kMoveToNextTableViewCell, bundle: nil), forCellReuseIdentifier: kMoveToNextTableViewCell)
       }
    
    func getWorkOrderResponse(){
        self.fireStickVM = FireStickViewModel(repo: FireStickTvRepo(), apiEndPoint: "create/wo")
        fetchWorkOrderResponse()
    }
    
    func fetchWorkOrderResponse() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if fireStickVM != nil {
                let rawData : APIParams = ["dyi" : true, "subscriberId" : BAKeychainManager().sId ]
                fireStickVM?.fireStickWOCompletion(param: rawData, completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        print("Successfully Called Api")
                        let message  = self.fireStickVM?.workOrderData
                        self.deliveryFireStickPopup(message)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    
    func passAddressModel(modal: AddressDataModel?){
        self.userDetailData = modal
    }
    
    @objc func backButtonAction(){
        print("back tapped")
        self.navigationController?.popViewController(animated: true)

        
    }
    
    @IBAction func proceedTapped(_ sender: Any) {
        openAppropiateScreen()
    }
    
    @IBAction func notNowTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)

    }
    
    private func openAppropiateScreen() {
//        if installOptionIndex == 3 {
//            print("Successfully availed")
//            getWorkOrderResponse()
//        } else {
            let installationScheduleVC: InstallationScheduleViewController  = UIStoryboard.loadViewController(storyBoardName: "FireTV", identifierVC: "InstallationScheduleViewController", type: InstallationScheduleViewController.self)
            installationScheduleVC.userDetailData = self.userDetailData
            installationScheduleVC.isDoItYourself = installOptionIndex == 3 
            self.navigationController?.pushViewController(installationScheduleVC, animated: true)
        //}
    }
}

extension ConfirmDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
            if self.installOptionIndex == 4{
              cell?.configureIndicator(number: 2, selectedIndex: 0)
            }
            return cell!
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
            cell?.configureCell(source: .addressConfirm)
            cell?.configureConstraint(paddingleft: 30, width: 261)
            return cell!
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalInfoTableViewCell") as? PersonalInfoTableViewCell
            cell?.infoNameLabel.text = "Name"
            cell?.infoDetailLabel.text = userDetailData?.name ?? ""
            return cell!
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalInfoTableViewCell") as? PersonalInfoTableViewCell
            cell?.infoNameLabel.text = "Address"
            cell?.infoDetailLabel.text = userDetailData?.address ?? ""
            return cell!
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell") as? InfoTableViewCell
            cell?.configureCell(fontSize: 12, padding: 12, align:.left, str:"*If the address is incorrect, please reach out to us on 1800 208 6633.")
            return cell!
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionalInfoTableViewCell") as? OptionalInfoTableViewCell
            //cell?.configureCell(index: 3)
            return cell!
        
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}


//
//  FireStickTvRepo.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol FireStickRepo {
    var apiEndPoint: String {get set}
    var apiParams: [AnyHashable: Any] {get set}
    
    func getUserAddressDetail(completion: @escaping(APIServicResult<GetAddressResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func fireStickWOResponse(apiParam : APIParams? ,completion: @escaping(APIServicResult<WorkOrderResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func getSlot(apiParam : APIParams?, completion: @escaping (APIServicResult<GetSlotResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func confirmSlot(apiParam : APIParams?, completion: @escaping (APIServicResult<ConfirmSlotResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct FireStickTvRepo: FireStickRepo {
    var apiEndPoint: String = ""
    var apiParams: [AnyHashable : Any] = APIParams()
    
    func fireStickWOResponse(apiParam : APIParams?, completion: @escaping (APIServicResult<WorkOrderResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let postQuery: APIParams = ["baId" : BAKeychainManager().baId]
        var target = ServiceRequestDetail.init(endpoint: .fireTvCompletion(param: apiParam, header: nil, postQueryParameters: postQuery, endUrlString: apiEndPoint))
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["rule"] = "BA"
        target.detail.headers["locale"] = "USA"
        target.detail.headers["deviceId"] = kDeviceId
        target.detail.headers["deviceType"] = "IOS"
        target.detail.headers["deviceName"] = kDeviceModal
        target.detail.headers["profileId"] = BAKeychainManager().profileId
        target.detail.headers["platform"] = "BINGE_ANYWHERE"
        target.detail.headers["deviceToken"] = BAKeychainManager().deviceAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["Cache-Control"] = "no-cache"
        target.detail.headers["Content-Type"] = "application/json"
        return APIService().request(target) { (response: APIResult<APIServicResult<WorkOrderResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
    
    func getUserAddressDetail(completion: @escaping (APIServicResult<GetAddressResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .addressDetail(header: nil, endUrlString: apiEndPoint))
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["accept"] = "*/*"
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["locale"] = "en"
        return APIService().request(target) { (response: APIResult<APIServicResult<GetAddressResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
    
    func getSlot(apiParam : APIParams?, completion: @escaping (APIServicResult<GetSlotResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .getSlot(param: apiParam, header: nil, endUrlString: apiEndPoint))
        target.detail.headers["accept"] = "*/*"
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["locale"] = "en"
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        return APIService().request(target) { (response: APIResult<APIServicResult<GetSlotResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
    
    func confirmSlot(apiParam: APIParams?, completion: @escaping (APIServicResult<ConfirmSlotResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .confirmSlot(param: apiParam, header: nil, endUrlString: apiEndPoint))
        target.detail.headers["accept"] = "*/*"
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["locale"] = "en"
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        return APIService().request(target) { (response: APIResult<APIServicResult<ConfirmSlotResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

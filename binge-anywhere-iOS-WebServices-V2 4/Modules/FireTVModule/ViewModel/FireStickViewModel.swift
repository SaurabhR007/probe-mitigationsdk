//
//  FireStickViewModel.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol FireStickViewModelProtocol {
    func getUserAddress(completion: @escaping(Bool, String, Bool) -> Void)
    func fireStickWOCompletion(param : APIParams? ,completion: @escaping(Bool, String, Bool) -> Void)
    func getSlot(param : APIParams? ,completion: @escaping(Bool, String, Bool) -> Void)
    func confirmSlot(param : APIParams? ,completion: @escaping(Bool, String, Bool) -> Void)
}

class FireStickViewModel: FireStickViewModelProtocol{
    
    var repo: FireStickRepo
    var requestToken: ServiceCancellable?
    var userAddressDetail: AddressDataModel?
    var workOrderData: String?
    var getSlotData: GetSlotModel?
    var confirmSlotData: ConfirmSlotModel?
    var apiParams = APIParams()
    
    init(repo: FireStickRepo, apiEndPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = apiEndPoint
    }
    
    func getUserAddress(completion: @escaping (Bool, String, Bool) -> Void) {
        requestToken = repo.getUserAddressDetail(completion: {(configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.userAddressDetail = _configData.parsed.data
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func fireStickWOCompletion(param: APIParams?, completion: @escaping (Bool, String, Bool) -> Void) {
        requestToken = repo.fireStickWOResponse(apiParam: param, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.workOrderData = _configData.parsed.data
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func getSlot(param : APIParams? ,completion: @escaping(Bool, String, Bool) -> Void) {
        requestToken = repo.getSlot(apiParam: param, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.getSlotData = _configData.parsed.data
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func confirmSlot(param: APIParams?, completion: @escaping (Bool, String, Bool) -> Void) {
        requestToken = repo.confirmSlot(apiParam: param, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.confirmSlotData = _configData.parsed.data
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}


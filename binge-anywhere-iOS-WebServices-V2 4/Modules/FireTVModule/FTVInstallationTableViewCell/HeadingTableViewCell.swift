//
//  HeadingTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class HeadingTableViewCell: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureUI(){
        self.contentView.backgroundColor = .BAdarkBlueBackground
        headingLabel.font = UIFont.skyTextFont(.medium, size: 28 * screenScaleFactorForWidth)
    }
    
}

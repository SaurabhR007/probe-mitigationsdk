//
//  InfoTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    

    @IBOutlet weak var infoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureUI(){
        self.contentView.backgroundColor = .BAdarkBlueBackground
        infoLabel.font = UIFont.skyTextFont(.medium, size: 17)
    }
    
    func configureCell(fontSize:CGFloat,padding:CGFloat,align:NSTextAlignment, str:String){
        infoLabel.font = UIFont.skyTextFont(.medium, size: fontSize)
        infoLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: padding).isActive = true
        infoLabel.setNeedsUpdateConstraints()
        infoLabel.text = str
        infoLabel.textAlignment = align
        infoLabel.setLblLineHeight(lineHeight: 2)
    }
    
}

//
//  PersonalInfoTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 22/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class PersonalInfoTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var infoNameLabel: UILabel!
    @IBOutlet weak var infoDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func configureUI(){
        infoNameLabel.font = UIFont.skyTextFont(.medium, size: 16)
        infoDetailLabel.font = UIFont.skyTextFont(.regular, size: 15)
    }
    
}

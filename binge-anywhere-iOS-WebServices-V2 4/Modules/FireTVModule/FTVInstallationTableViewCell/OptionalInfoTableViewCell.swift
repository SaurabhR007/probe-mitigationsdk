//
//  OptionalInfoTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 22/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class OptionalInfoTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var landMarkLabel: CustomLabel!
    @IBOutlet weak var addressTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ConfigureUI()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func ConfigureUI(){
        landMarkLabel.isHidden = true
        addressTextView.isHidden = true
        landMarkLabel.setupCustomFontAndColor(font: UIFont.skyTextFont(.medium, size: 16), color: .BAwhite)
        addressTextView.font = UIFont.skyTextFont(.medium, size: 16)
        addressTextView.textColor = .BAwhite
    }
    
}

//
//  MoveToNextTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class MoveToNextTableViewCell: UITableViewCell {
    

    @IBOutlet weak var proceedButton: CustomButton!
    
    @IBOutlet weak var cancelButton: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureUI(){
        self.contentView.backgroundColor = .BAdarkBlueBackground
        proceedButton.backgroundColor = .BABlueColor
        proceedButton.setTitle("Proceed", for: .normal)
        proceedButton.setTitleColor(.white, for: .normal)
        proceedButton.titleLabel?.textAlignment = .center
        proceedButton.cornerRadius = 20
        
        cancelButton.backgroundColor = .clear
        cancelButton.setTitle("Not now", for: .normal)
        cancelButton.setTitleColor(.BABlueColor, for: .normal)
        cancelButton.underline()
        cancelButton.titleLabel?.font = UIFont.skyTextFont(.medium, size: 16)
    }
    
}

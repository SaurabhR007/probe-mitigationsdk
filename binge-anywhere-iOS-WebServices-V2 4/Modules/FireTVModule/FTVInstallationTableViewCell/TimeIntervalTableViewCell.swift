//
//  TimeIntervalTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 02/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit

class TimeIntervalTableViewCell: UITableViewCell {

    @IBOutlet weak var intervalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.backgroundColor = .BAdarkBlue2Color
        configureUI()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
    func configureUI(){
        intervalLabel.font = UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth)
    }
    
}

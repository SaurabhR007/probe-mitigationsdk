//
//  TimeScheduleTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 29/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol TimeScheduleTableViewCellDelegate {
    func reloadRowData(indexPath: Int)
    func timeSlotDidSelect(_ slot: Slots)
}

class TimeScheduleTableViewCell: UITableViewCell, cellTappedDelagate {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
        
    @IBOutlet weak var intervalTableView: UITableView!
    
    @IBOutlet weak var timeView: UIView!
    
    @IBOutlet weak var expandIconView: UIImageView!
    @IBOutlet weak var scheduleStackView: UIStackView!
     
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var delegate:TimeScheduleTableViewCellDelegate?
    
    var dataSource: [Slots] = [] //["10am - 12pm","12pm - 2pm","2pm - 4pm","4pm - 6pm"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        configureUI()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureUI(){
        self.contentView.backgroundColor = .BAdarkBlueBackground
        titleLabel.font = UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth)
        timeLabel.font = UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth)
        titleLabel.backgroundColor = .clear
        timeLabel.backgroundColor = .clear
        timeView.backgroundColor = .BAdarkBlue2Color
        scheduleStackView.backgroundColor = .BAdarkBlue2Color
        configureTableView()
    }
    
    
    func configureTableView(){
        intervalTableView.backgroundColor = .BAdarkBlue2Color
        intervalTableView.delegate = self
        intervalTableView.dataSource = self
        intervalTableView.separatorStyle = .none
        intervalTableView.showsVerticalScrollIndicator = false
        intervalTableView.isHidden = true
        intervalTableView.rowHeight = UITableView.automaticDimension
        intervalTableView.register(UINib(nibName: kTimeIntervalTableViewCell, bundle: nil), forCellReuseIdentifier: kTimeIntervalTableViewCell)
    }
    
    func setupSlotsDataSource(_ slots: [Slots]) {
        dataSource = []
        dataSource = slots
        intervalTableView.reloadData()
        tableViewHeightConstraint.constant = CGFloat(44 * dataSource.count)
    }
    
     func expandTapped() {
        if intervalTableView.isHidden{
            animate(toggle: true)
        }else{
            animate(toggle: false)
        }
    }
    
    func animate(toggle:Bool){
        if toggle {
            UIView.animate(withDuration: 0.3) {
                self.intervalTableView.isHidden = false
                self.expandIconView.image = UIImage(named: "closeArrow")
                self.delegate?.reloadRowData(indexPath: 3)
            }
        }else {
            UIView.animate(withDuration: 0.3) {
                self.intervalTableView.isHidden = true
                self.expandIconView.image = UIImage(named: "expandArrow")
                self.intervalTableView.scrollToTop(animated: true)
                self.delegate?.reloadRowData(indexPath: 3)
            }
        }
        layoutIfNeeded()
    }
    
}

extension TimeScheduleTableViewCell : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeIntervalTableViewCell") as? TimeIntervalTableViewCell
        let time = dataSource[indexPath.row]
        cell?.intervalLabel.text = "\(time.start ?? "") - \(time.end ?? "")"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTime = dataSource[indexPath.row]
        self.timeLabel.text = "\(selectedTime.start ?? "") - \(selectedTime.end ?? "")"
        animate(toggle: false)
        delegate?.timeSlotDidSelect(selectedTime)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
    }
}

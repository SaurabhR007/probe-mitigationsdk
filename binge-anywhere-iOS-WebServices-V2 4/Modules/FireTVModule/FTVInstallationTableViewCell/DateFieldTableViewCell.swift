//
//  DateFieldTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 28/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class DateFieldTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var calenderIconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        configureUI()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureUI(){
        self.contentView.backgroundColor = .BAdarkBlueBackground
        titleLable.font = UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth)
        dateLabel.font = UIFont.skyTextFont(.medium, size: 16*screenScaleFactorForWidth)



    }
    
}

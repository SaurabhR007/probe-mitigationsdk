//
//  installOptionsTableViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class InstallOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var optionSelectionView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configureUI(){
        self.contentView.backgroundColor = .BAdarkBlueBackground
        optionLabel.font = UIFont.skyTextFont(.medium, size: 18*screenScaleFactorForWidth)
    }
    
    
    
    func configureCell(index : Int, selected : Bool){
        
        switch index {
        case 3 :
            optionLabel.text = "Do It Yourself"
            let str = "The device will be shipped to your address with an installation guide for self-installation.\n\n"
            let descriptionAttributedTxt = NSMutableAttributedString(string:str, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 14*screenScaleFactorForWidth)])
            descriptionAttributedTxt.append(NSAttributedString(string:"Recommended for contactless installation.", attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 14*screenScaleFactorForWidth)]))
            descriptionLabel.attributedText = descriptionAttributedTxt
            break
        case 4 :
            optionLabel.text  = "Installer Required"
            let str = "Our technician will visit your address and do the installation for you.\n\n"
            let descriptionAttributedTxt = NSMutableAttributedString(string:str, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 14*screenScaleFactorForWidth)])
            descriptionAttributedTxt.append(NSAttributedString(string:"Recommended for someone who is not comfortable with self-installation.", attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 14*screenScaleFactorForWidth)]))
            descriptionLabel.attributedText = descriptionAttributedTxt
            break
        default:
            break
        }
        
        if selected{
            optionSelectionView.image = UIImage(named: "selected")
        }else{
            optionSelectionView.image = UIImage(named: "nonSelected")
        }
        
    }
    
}

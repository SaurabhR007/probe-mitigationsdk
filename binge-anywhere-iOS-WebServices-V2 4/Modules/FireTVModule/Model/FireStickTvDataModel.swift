//
//  FireStickTvDataModel.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct AddressDataModel : Codable {
    let address : String?
    let serviceRegionId : String?
    let pincode : String?
    let city : String?
    let state : String?
    let name : String?
    let email : String?
    let ocsFlag : String?
    let slotSuggestions : [Slots]?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        serviceRegionId = try values.decodeIfPresent(String.self, forKey: .serviceRegionId)
        pincode = try values.decodeIfPresent(String.self, forKey: .pincode)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        ocsFlag = try values.decodeIfPresent(String.self, forKey: .ocsFlag)
        slotSuggestions = try values.decodeIfPresent([Slots].self, forKey: .slotSuggestions)
    }
}

struct Slots: Codable {
    let start: String?
    let end: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        start = try values.decodeIfPresent(String.self, forKey: .start)
        end = try values.decodeIfPresent(String.self, forKey: .end)
    }
}

struct GetAddressResponse : Codable {
    let code : Int?
    let message : String?
    let data : AddressDataModel?


    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AddressDataModel.self, forKey: .data)
    }
}

struct WorkOrderResponse : Codable {
    let code : Int?
    let message : String?
    let data : String?


    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(String.self, forKey: .data)
    }
}

struct GetSlotResponse: Codable {
    let code: Int?
    let message: String?
    let data: GetSlotModel?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(GetSlotModel.self, forKey: .data)
    }
}

struct GetSlotModel: Codable {
    let transMessage: String?
    let transStatus: String?
    let taskId: TaskModel?
    let slotSuggestions: [Slots]?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        transMessage = try values.decodeIfPresent(String.self, forKey: .transMessage)
        transStatus = try values.decodeIfPresent(String.self, forKey: .transStatus)
        taskId = try values.decodeIfPresent(TaskModel.self, forKey: .taskId)
        slotSuggestions = try values.decodeIfPresent([Slots].self, forKey: .slotSuggestions)
    }
}

struct TaskModel: Codable {
    let clientId: String?
    let taskId: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        clientId = try values.decodeIfPresent(String.self, forKey: .clientId)
        taskId = try values.decodeIfPresent(String.self, forKey: .taskId)
    }
}

struct ConfirmSlotResponse: Codable {
    let code: Int?
    let message: String?
    let data: ConfirmSlotModel?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ConfirmSlotModel.self, forKey: .data)
    }
}

struct ConfirmSlotModel: Codable {
    let transMessage: String?
    let transStatus: String?
    let taskId: String?
    let travelTime: Int?
    let transactionDuration: Int?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        transMessage = try values.decodeIfPresent(String.self, forKey: .transMessage)
        transStatus = try values.decodeIfPresent(String.self, forKey: .transStatus)
        taskId = try values.decodeIfPresent(String.self, forKey: .taskId)
        travelTime = try values.decodeIfPresent(Int.self, forKey: .travelTime)
        transactionDuration = try values.decodeIfPresent(Int.self, forKey: .transactionDuration)
    }
}

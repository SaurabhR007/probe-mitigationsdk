//
//  BAVootRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol BAVootScreenRepo {
    var apiEndPoint: String {get set}
    var contentId: String {get set}
    var contentType: String {get set}
    var partner: String {get set}
    func makePlayabaleContentCall(completion: @escaping(APIServicResult<BAVootModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAVootRepo: BAVootScreenRepo {
    var apiEndPoint: String = ""
    var contentId: String = ""
    var contentType: String = ""
    var partner: String = ""
    //var apiHeader: APIHeaders
    func makePlayabaleContentCall(completion: @escaping (APIServicResult<BAVootModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var apiParams = APIParams()
        apiParams["contentId"] = contentId
        apiParams["contentType"] = contentType
        apiParams["partner"] = partner
        apiParams["baId"] = BAKeychainManager().baId
        apiParams["type"] = "HLS"
        let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
        let SID = BAKeychainManager().sId
        let PID = BAKeychainManager().profileId
        let dsn = BAKeychainManager().acccountDetail?.deviceSerialNumber ?? ""
        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId != nil/*BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.binge.rawValue && dsn.isEmpty*/ {
            let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": accessToken, "profileId": "\(PID)", "subscriberId": "\(SID)", "deviceId": kDeviceId, "deviceToken": BAKeychainManager().acccountDetail?.deviceAuthenticateToken ?? "", "rule": "BA", "platform": "BINGE_ANYWHERE", "partnerUniqueId": BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId]
            if partner.uppercased() == ProviderType.vootKids.rawValue {
                let target = ServiceRequestDetail.init(endpoint: .vootPlabackCallForKids(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
                return APIService().request(target) { (response: APIResult<APIServicResult<BAVootModal>, ServiceProviderError>) in
                    completion(response.value, response.error)
                }
            } else {
                let target = ServiceRequestDetail.init(endpoint: .vootPlabackCallForSelect(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
                return APIService().request(target) { (response: APIResult<APIServicResult<BAVootModal>, ServiceProviderError>) in
                    completion(response.value, response.error)
                }
            }
        } else {
            let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": accessToken, "profileId": "\(PID)", "subscriberId": "\(SID)", "deviceId": kDeviceId, "deviceToken": BAKeychainManager().acccountDetail?.deviceAuthenticateToken ?? "", "rule": "BA", "platform": "BINGE_ANYWHERE", "partnerUniqueId": ""]
            if partner.uppercased() == ProviderType.vootKids.rawValue {
                let target = ServiceRequestDetail.init(endpoint: .vootPlabackCallForKids(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
                return APIService().request(target) { (response: APIResult<APIServicResult<BAVootModal>, ServiceProviderError>) in
                    completion(response.value, response.error)
                }
            } else {
                let target = ServiceRequestDetail.init(endpoint: .vootPlabackCallForSelect(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
                return APIService().request(target) { (response: APIResult<APIServicResult<BAVootModal>, ServiceProviderError>) in
                    completion(response.value, response.error)
                }
            }
        }
    }
}

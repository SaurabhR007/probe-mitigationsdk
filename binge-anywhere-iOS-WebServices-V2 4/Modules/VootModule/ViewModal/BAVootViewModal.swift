//
//  BAVootViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol BAVootViewModalProtocol {
    //var playbackModel: ContentDetailData {get set}
    func makePlayabaleContentCall(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAVootViewModal: BAVootViewModalProtocol {

    var requestToken: ServiceCancellable?
    var repo: BAVootScreenRepo
    var endPoint: String = ""
    var apiParams = [AnyHashable: Any]()
    var playbackId: String?
    var playbackContentType: String?
    var playbackpartner: String?
    var playbackData: VootPlaybackData?

    init(repo: BAVootScreenRepo, endPoint: String, contentId: String?, contentType: String?, partner: String?) {
        self.repo = repo
        self.playbackId = contentId ?? ""
        self.playbackpartner = partner ?? ""
        self.playbackContentType = contentType ?? ""
        self.repo.apiEndPoint = endPoint
    }

    func makePlayabaleContentCall(completion: @escaping(Bool, String?, Bool) -> Void) {
        //createVootPlaybackParams()
        self.repo.contentId = playbackId ?? ""
        self.repo.partner = playbackpartner ?? ""
        self.repo.contentType = playbackContentType ?? ""
        self.repo.apiEndPoint = endPoint
        self.requestToken = self.repo.makePlayabaleContentCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        if _configData.parsed.data != nil {
                            self.playbackData = _configData.parsed.data
                            completion(true, message, false)
                        } else {
                            self.playbackData = _configData.parsed.data
                            completion(true, message, true)
                        }
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func createVootPlaybackParams() {
        apiParams = [ : ]
        apiParams["contentId"] = playbackId
        apiParams["contentType"] = playbackContentType
        apiParams["partner"] = playbackpartner
        apiParams["baId"] = BAKeychainManager().baId
        apiParams["type"] = "HLS"
    }
}

//
//  BAVootModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAVootModal: Codable {
    let code: Int?
    let message: String?
    let data: VootPlaybackData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(VootPlaybackData.self, forKey: .data)
    }
}

struct VootPlaybackData : Codable {
    let objectType: String?
    let assetId: Int?
    let duration: Int?
    let externalId: String?
    let fileSize: Int?
    let id: Int?
    let opl: String?
    let outputProtecationLevel: String?
    let type: String?
    let url: String?
    let format: String?
    let protocols: String?
    let drm: [Drm]?
    let tokenized: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        objectType = try values.decodeIfPresent(String.self, forKey: .objectType)
        assetId = try values.decodeIfPresent(Int.self, forKey: .assetId)
        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
        externalId = try values.decodeIfPresent(String.self, forKey: .externalId)
        fileSize = try values.decodeIfPresent(Int.self, forKey: .fileSize)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        opl = try values.decodeIfPresent(String.self, forKey: .opl)
        outputProtecationLevel = try values.decodeIfPresent(String.self, forKey: .outputProtecationLevel)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        format = try values.decodeIfPresent(String.self, forKey: .format)
        protocols = try values.decodeIfPresent(String.self, forKey: .protocols)
        drm = try values.decodeIfPresent([Drm].self, forKey: .drm)
        tokenized = try values.decodeIfPresent(String.self, forKey: .tokenized)
    }
}

struct Drm: Codable {
    let objectType: String?
    let licenseURL: String?
    let scheme: String?
    let certificate: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        objectType = try values.decodeIfPresent(String.self, forKey: .objectType)
        licenseURL = try values.decodeIfPresent(String.self, forKey: .licenseURL)
        scheme = try values.decodeIfPresent(String.self, forKey: .scheme)
        certificate = try values.decodeIfPresent(String.self, forKey: .certificate)
    }
}


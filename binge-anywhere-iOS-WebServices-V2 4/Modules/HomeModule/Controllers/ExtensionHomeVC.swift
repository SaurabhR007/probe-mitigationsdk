//
//  ExtensionHomeVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 06/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import CommonCrypto
import ARSLineProgress

extension HomeVC: TableViewCellDelegate {
    
    func moveToBrowseByDetail(intent: String?, genre: String?, language: String?, pageSource: String?, configSource: String?, railName: String?) {
        fetchBrowseByData(intent: intent, genre: genre, language: language, pagename: BAKeychainManager().pageNameOnHome, pageSource: pageSource, configSource: configSource, railName: railName)//, pageType:)
    }
    
    
    func moveToDetailScreen(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, timestamp: Int?, pageSource: String?, configSource: String?, railName: String?) {
        fetchContentDetail(railType: railType, layoutType: layoutType, vodId: vodId, contentType: contentType, expiryTime: timestamp, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    /**
     ********************************************
     Content Detail Cell Delegate Methods
     ********************************************
     **/
    
    func moveToDetailForPageType(pageType: String?, appname: String?, isSubscribed: Bool?) {
        fetchPageType(pageType: pageType, appName: appname, isSubscribed: isSubscribed)
    }
    
    func moveToSeeAllScreenFromVOD(_ data: RecommendedContentData?) {
        // Do Nothing
    }
    
    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, expiryTime: Int?, pageSource: String?, configSource: String?, railName: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiryTime, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    // MARK: Content Detail API Calling
    func getContentDetailData(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, pageSource: String?, configSource: String?, railName: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    //self.hideActivityIndicator()
                    if flagValue {
                        self.viewContentDetailEvent(contentData: dataModel.contentDetail, pageSource: pageSource ?? "", configSource: configSource ?? "", railName: railName ?? "")
                        //                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        //                        contentDetailVC.parentNavigation = self.parentNavigationController
                        //                        contentDetailVC.railType = railType
                        //                        contentDetailVC.layoutType = layoutType
                        //                        contentDetailVC.id = vodId
                        //                        contentDetailVC.pageSource = pageSource ?? ""
                        //                        contentDetailVC.configSource = configSource ?? ""
                        //                        contentDetailVC.railName = railName ?? ""
                        //                        contentDetailVC.contentDetail = self.contentDetail
                        //contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        //                        if self.contentDetail?.detail?.contractName == "RENTAL" {
                        //                            contentDetailVC.timestamp = expiry ?? 0
                        //                        }
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        //self.parentNavigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        self.fetchLastWatch(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiry, contentDetail: dataModel.contentDetail, pageSource: pageSource ?? "", configSource: configSource ?? "", railName: railName ?? "")
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func viewContentDetailEvent(contentData: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        var title = ""
        if contentData?.meta?.contentType == ContentType.brand.rawValue {
            title = contentData?.meta?.brandTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.series.rawValue {
            title = contentData?.meta?.seriesTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.tvShows.rawValue {
            title = contentData?.meta?.vodTitle ?? ""
        } else {
            title = contentData?.meta?.vodTitle ?? ""
        }
        let genreResult = contentData?.meta?.genre?.joined(separator: ",")
        let languageResult = contentData?.meta?.audio?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
    
    func fetchLastWatch(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, contentDetail: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiry, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    func fetchWatchlistLookUp(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int, pageSource: String, configSource: String, railName: String) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiry, contentDetail: contentDetail, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    func confgureWatchlistIcon(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, contentDetail: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
                
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigationController
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = pageSource
                        contentDetailVC.configSource = configSource
                        contentDetailVC.railName = railName
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.hideActivityIndicator()
                        self.parentNavigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigationController
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = pageSource
                        contentDetailVC.configSource = configSource
                        contentDetailVC.railName = railName
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.hideActivityIndicator()
                        self.parentNavigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigationController
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = pageSource
                        contentDetailVC.configSource = configSource
                        contentDetailVC.railName = railName
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.parentNavigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    // MARK: Get Browse By Detail
    func fetchBrowseByData(intent: String?, genre: String?, language: String?, pagename: String?, pageSource: String?, configSource: String?, railName: String?) {
        browseByDetailVM = BABrowseByModuleDetailVM(repo: BABrowseByModuleDetailRepo(), endPoint: "results", intent: intent ?? "", genre: genre ?? "", language: language ?? "", pageName: BAKeychainManager().pageNameOnHome)
        getBrowseByDetails(intent: intent, genre: genre, language: language, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    // MARK: Browse By Detail API Calling
    func getBrowseByDetails(intent: String?, genre: String?, language: String?, pageSource: String?, configSource: String?, railName: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if browseByDetailVM != nil {
                browseByDetailVM?.getBrowseByDetail(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.navigateToBrowseByDetail(content: self.browseByDetailVM?.browseByData?.data, intent: intent, genre: genre, language: language, pageSource: pageSource, configSource: configSource, railName: railName)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
            //                {
            //				self.getBrowseByDetails(intent: intent, genre: genre, language: language)
            //			}
        }
    }
    
    func navigateToBrowseByDetail(content: BrowseByDetailData?, intent: String?, genre: String?, language: String?, pageSource: String?, configSource: String?, railName: String?) {
        let searchListingVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.search.fileName, identifierVC: ViewControllers.searchListingVC.rawValue, type: SearchListingVC.self)
        searchListingVC.parentNavigation = self.parentNavigationController
        searchListingVC.pageSource = pageSource ?? ""
        searchListingVC.configSource = configSource ?? ""
        searchListingVC.railTitle = railName ?? ""
        if intent == "GENRE" {
            searchListingVC.selectedFilter = genre
        } else {
            searchListingVC.selectedFilter = language
        }
        searchListingVC.intent = intent
        searchListingVC.browseByDetailVM = self.browseByDetailVM
        searchListingVC.contentResults = content?.contentList
        self.parentNavigationController?.pushViewController(searchListingVC, animated: true)
    }
    
    // MARK: Move to See All Screen Method
    func moveToSeeAllScreen(_ data: HomeScreenDataItems?, _ index: Int) {
        let configType = (data?.sectionSource == TAConstant.recommendationType.rawValue) ? MixpanelConstants.ParamValue.recomended.rawValue : MixpanelConstants.ParamValue.editorial.rawValue
        if data?.sectionSource == "PROVIDER" {
            let appsSeeAllVC: BAAppsSeeAllViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllForApps.rawValue, type: BAAppsSeeAllViewController.self)
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: configType, MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "NO",  MixpanelConstants.ParamName.partner.rawValue: "MIX", MixpanelConstants.ParamName.railPosition.rawValue: index, MixpanelConstants.ParamName.pageName.rawValue: self.title ?? ""])
            appsSeeAllVC.contentId = data?.id ?? 0
            self.parentNavigationController?.pushViewController(appsSeeAllVC, animated: true)
        } else if data?.sectionSource == "CONTINUE_WATCHING" {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: "CONTINUE-WATCHING", MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "NO", MixpanelConstants.ParamName.partner.rawValue: "MIX", MixpanelConstants.ParamName.railPosition.rawValue: index, MixpanelConstants.ParamName.pageName.rawValue: self.title ?? ""])
            let cwSeeAllVC: BAContinueWatchingSeeAllViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.continueWatchVC.rawValue, type: BAContinueWatchingSeeAllViewController.self)
            cwSeeAllVC.isCW = true
            self.parentNavigationController?.pushViewController(cwSeeAllVC, animated: true)
        } else {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: configType, MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "NO",  MixpanelConstants.ParamName.partner.rawValue: "MIX", MixpanelConstants.ParamName.railPosition.rawValue: index, MixpanelConstants.ParamName.pageName.rawValue: self.title ?? ""])
            let seeAllVC: SeeAllVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllVC.rawValue, type: SeeAllVC.self)
            seeAllVC.contentId = data?.id ?? 0
            seeAllVC.pageSource = self.title ?? ""
            seeAllVC.configSource = "Editorial"
            seeAllVC.railName = data?.title ?? ""
            seeAllVC.isTAData = (data?.sectionSource == TAConstant.recommendationType.rawValue)
            seeAllVC.isTVODData = (data?.sectionSource == "TVOD")
            seeAllVC.isPrime = (data?.sectionSource?.lowercased() == SectionSourceType.prime.rawValue.lowercased())
            seeAllVC.layoutType = data?.layoutType
            seeAllVC.taContentlListData = data
            self.parentNavigationController?.pushViewController(seeAllVC, animated: true)
        }
    }
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }
    
}
/**
 ********************************************
 See All Delegate Methods
 ********************************************
 **/
//extension HomeVC: SeeAllVCDelegate {
//    func moveToDetailScreenFromSeeAll(vodId: Int?, contentType: String?) {
//        fetchContentDetail(vodId: vodId, contentType: contentType)
//    }
//}

/**
 ********************************************
 Hero Banner Delegate Methods
 ********************************************
 **/

extension HomeVC: BAHeroBannerTableViewCellDelegate {
    func moveToDetailScreenFromHeroBanner(vodId: Int?, contentType: String?, pageSource: String?, configSource: String?, railName: String?) {
        //showActivityIndicator(isUserInteractionEnabled: false)
        fetchContentDetail(railType: "", layoutType: "LANDSCAPE", vodId: vodId, contentType: contentType, expiryTime: 0, pageSource: pageSource, configSource: configSource, railName: railName)
    }
}

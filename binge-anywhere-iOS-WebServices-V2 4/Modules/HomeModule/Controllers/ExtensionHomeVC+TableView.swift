//
//  ExtensionHomeVC+TableView.swift
//  BingeAnywhere
//
//  Created by Shivam on 06/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = homeViewModel?.homeScreenData?.data?.items?.count else {
            return 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if homeViewModel?.homeScreenData?.data?.items?.count ?? 0 > indexPath.row {
            let item = homeViewModel?.homeScreenData?.data?.items?[indexPath.row]
            if item?.sectionType == RailType.hero.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: kBAHeroBannerTableViewCell) as! BAHeroBannerTableViewCell
                cell.isPartnerHomePage = false
                cell.bannerItem = homeViewModel?.homeScreenData?.data?.items?[indexPath.row]
                cell.pageType = self.pageType ?? ""
                cell.pageName = self.pageName ?? ""
                cell.delegate = self
                cell.setup()
                return cell
            } else if item?.sectionType == RailType.rail.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell) as? BAHomeRailTableViewCell
                cell?.isPartnerHomePage = false
                cell?.pageType = self.pageType ?? ""
                cell?.railPosition = indexPath.row
                cell?.pageName = self.pageName ?? ""
                cell?.delegate = self
                cell?.seeAllButton.tag = indexPath.row
                if homeViewModel?.homeScreenData?.data != nil {
                    print(" ===========> 7 \(continueWatchingChanged)")
                    cell?.configureCell((homeViewModel?.homeScreenData?.data?.items?[indexPath.row])!, scrollToFirst: continueWatchingChanged)
                }
                return cell ?? UITableViewCell()
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell) as! BAHomeRailTableViewCell
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell) as! BAHomeRailTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? BAHomeRailTableViewCell else {
            return
        }
        let offset = cell.collectionView.contentOffset
        cell.collectionView.setContentOffset(offset, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if homeViewModel?.homeScreenData?.data?.items?.count ?? 0 > indexPath.row {
            let item = homeViewModel?.homeScreenData?.data?.items?[indexPath.row]
            if item?.sectionType == RailType.hero.rawValue {
                return ScreenSize.screenWidth * (9/16)
            } else if item?.sectionSource == "LANGUAGE" || item?.sectionSource == "GENRE" {
                return 179
            } else if item?.sectionSource == "PROVIDER" {
                return 156
            } else if item?.sectionType == RailType.rail.rawValue {
                if item?.layoutType == LayoutType.landscape.rawValue {
                    return 251
                } else if item?.layoutType == LayoutType.potrait.rawValue {
                    return 330
                } else {
                    return 180
                }
            } else {
                return 180
            }
        } else {
            return 0.01
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        loadMore(indexPath)
    }
    
    private func loadMore(_ index: IndexPath) {
        let count = homeViewModel?.homeScreenData?.data?.items?.count ?? 0
        let totalCount = (homeViewModel?.totalDataCount ?? 0 )
        let totalApiCount = (homeViewModel?.homeScreenData?.data?.total ?? 0)
        
        let rows = count - 1
        let isLastCellVisible = tableView.indexPathsForVisibleRows?.filter({$0.row == (rows - 1)}).first != nil
        
        guard isLastCellVisible else {
            return
        }
        
        if index.row >= (count - 1) && totalCount < totalApiCount {
            if self.homeViewModel?.requestToken == nil && (self.homeViewModel?.nextPageAvailable ?? false){
                self.getHomeData()
            }
        }
    }
}

//
//  HomeVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 20/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import FirebaseCrashlytics
//import ARSLineProgress

class HomeVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var pageType: String?
    var pageName: String?
    var homeViewModel: HomeVM?
    var pageTypeVm: PageTypeVM?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var browseByDetailVM: BABrowseByModuleDetailVM?
    var contentDetail: ContentDetailData?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var oldData = [BAContentListModel]()
    var isHomeDataCalled = false
    var parentNavigationController: UINavigationController?
    private let refreshControl = UIRefreshControl()
    var partnerSubscription: PartnerSubscriptionDetails?
    var isPullToRefresh = false
    var homeArray = [UITableViewCell] ()
    var disPatchGroup = DispatchGroup()
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "Home")
    }()
    
    private(set) var continueWatchingChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        //oldData = cell.railCollectionCellsData?.contentList
        refreshControl.addTarget(self, action: #selector(refreshHomeContent(_:)), for: .valueChanged)
        setUpTableView()
       // checkForRecharge()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isHomeDataCalled {
            self.mixPanelEvent()
            self.updateContinueWatchData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //fatalError()
//        UtilityFunction.shared.resetAppbadgeCount()
        if BAKeychainManager().deepLinkScreenOpened {
            checkForDeeplinkData()
        } else {
            checkForPushData()
        }
    }
    
     func reCheckVM() {
        if homeViewModel == nil {
            let endpoint = "page/" + (pageType ?? "")
            homeViewModel = HomeVM(repo: HomeScreenRepo(), endPoint: endpoint, pageType: (pageType ?? ""), isFromHome: true)
            mixPanelEvent()
            if self.title?.uppercased() == "HOME" {
                self.addObserver()
            }
            self.getHomeData()
        }
    }
    
    private func checkForRecharge() {
        if BAKeychainManager().acccountDetail?.accountStatus == "DEACTIVATED" {
            self.apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader) 
        } 
    }
    
    @objc func makeContinueWatchCall() {
        self.updateContinueWatchData()
    }
    
    
    @objc private func refreshHomeContent(_ sender: Any) {
        refreshHomeData()
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateSubscribedApps), name: .notificationPackSubscription, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeContinueWatchCall), name: NSNotification.Name(rawValue: "makeContinueWatchCall"), object: nil)
    }
    
    @objc func updateSubscribedApps(_ notification: NSNotification) {
        UtilityFunction.shared.performTaskInMainQueue {
            self.refreshHomeData()
            //self.tableView.reloadData()
        }
    }
    
    func setUpTableView() {
        UtilityFunction.shared.registerCell(tableView, kBAHomeRailTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kBAHeroBannerTableViewCell)
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }
    
    func checkForDeeplinkData() {
        if BAKeychainManager().deepLinkScreenOpened {
            BAKeychainManager().deepLinkScreenOpened = !BAKeychainManager().deepLinkScreenOpened
            UtilityFunction.shared.performTaskInMainQueue {
                let contentType = kUserDefaults.object(forKey: "contentType") as? String
                let contentId = kUserDefaults.object(forKey: "contentId") as? String
                kAppDelegate.parseDeepLinkData(contentType: contentType ?? "", contentId: contentId ?? "")
                kUserDefaults.removeObject(forKey: "contentType")
                kUserDefaults.removeObject(forKey: "contentId")
            }
        }
    }
    
    func checkForPushData() {
        if BAKeychainManager().isScreenPushCodeCalled {
            BAKeychainManager().isScreenPushCodeCalled = !BAKeychainManager().isScreenPushCodeCalled
            UtilityFunction.shared.performTaskInMainQueue {
                if let data2 = kUserDefaults.object(forKey: "dict1") as? NSData {
                    kUserDefaults.removeObject(forKey: "dict1")
                    let dict = NSKeyedUnarchiver.unarchiveObject(with: data2 as Data)
                    let pushDic = (dict as? NSDictionary) as? [AnyHashable: Any]
                    if let pushDict = pushDic {
                        kAppDelegate.parsePushData(pushDict)
                    }
                }
            }
        }
    }
    
    func mixPanelEvent() {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homePageView.rawValue, properties: [MixpanelConstants.ParamName.name.rawValue: self.title ?? ""])
    }
    
    // MARK: Home Data Response handling
    func getHomeData() {
        if BAReachAbility.isConnectedToNetwork() {
            isPullToRefresh = false
            self.disPatchGroup.enter()
            removePlaceholderView()
            showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = homeViewModel {
                dataModel.getHomeScreenData {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    self.checkForLogOutState()
                    self.isHomeDataCalled = true
                    if flagValue {
                        //self.configureCell()
                        self.tableView.reloadData {
                            self.showHideWaterMark()
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                    self.disPatchGroup.leave()
                    self.disPatchGroup.notify(queue: .main) {
                        self.hideActivityIndicator()
                    }
                    self.hideActivityIndicator()
                }
            } else {
                checkForLogOutState()
                self.hideActivityIndicator()
            }
        } else {
            checkForLogOutState()
            hideActivityIndicator()
            noInternet()
        }
    }
    
    
    func checkForLogOutState() {
        if BAKeychainManager().deviceLoggedOut {
            UtilityFunction.shared.alertPopUpBox(true)
        }
    }
    
    func refreshHomeData() {
        if BAReachAbility.isConnectedToNetwork() {
            isPullToRefresh = true
            removePlaceholderView()
            showActivityIndicator(isUserInteractionEnabled: false)
            self.homeViewModel?.nextPageAvailable = false
            self.homeViewModel?.pageOffset = 0
            self.refreshControl.endRefreshing()
            if let dataModel = homeViewModel {
                dataModel.getHomeScreenData {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        //self.configureCell()
                        UtilityFunction.shared.performTaskInMainQueue {
                            self.tableView.reloadData{
                                self.showHideWaterMark()
                            }
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.showHideWaterMark()
                self.hideActivityIndicator()
            }
        } else {
            self.refreshControl.endRefreshing()
            noInternet()
        }
    }
    
    func updateContinueWatchData() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            if let dataModel = homeViewModel {
                let group = DispatchGroup()
                group.enter()
                dataModel.checkForContinueWatchData(group: group, {
                    group.leave()
                })
                group.notify(queue: .main) {
                    dataModel.omitBlankContentSectionRail()
                    let index = dataModel.homeScreenData?.data?.items?.firstIndex(where: { (data) -> Bool in
                        return data.sectionSource == "CONTINUE_WATCHING"
                    })
                    UtilityFunction.shared.performTaskInMainQueue {
                        if let index = index {
                            let indexPath = IndexPath.init(row: index, section: 0)
                            if let cell = self.tableView.cellForRow(at: indexPath) as? BAHomeRailTableViewCell {
                                var shouldScrollToFirst = false
                                if let items = dataModel.homeScreenData?.data?.items?[index] {
                                    if self.oldData.isEmpty {
                                        self.oldData = dataModel.homeScreenData?.data?.items?[index].contentList ?? []
                                    } else {
                                        // Do Not do anything
                                    }
                                    //let oldData = cell.railCollectionCellsData?.contentList
                                    if self.oldData.count == items.contentList?.count {
                                        if let newItems = items.contentList {
                                            for (index, _) in newItems.enumerated() {
                                                if self.oldData[index].id != newItems[index].id {
                                                    self.oldData = newItems
                                                    shouldScrollToFirst = true
                                                    break
                                                }
                                            }
                                        }
                                    }
                                    if shouldScrollToFirst {
                                        self.continueWatchingChanged = true
                                        cell.collectionView.setContentOffset(.zero, animated: false)
                                        //cell.collectionView.reloadData()
                                        cell.configureCell(items, scrollToFirst: shouldScrollToFirst)
                                    } else {
                                        self.oldData = dataModel.homeScreenData?.data?.items?[index].contentList ?? []
                                        cell.collectionView.reloadData()
                                    }
                                }
                            }
                            //self.tableView.endUpdates()
                        } else {
                            //self.configureCell()
                            self.tableView.reloadData {
                                self.showHideWaterMark()
                            }
                        }
                    }
                }
            } else {
                // Do Nothing
                self.showHideWaterMark()
            }
        } else {
            noInternet()
        }
    }
    
    // MARK: - Page Type Navigation
    func fetchPageType(pageType: String?, appName: String?, isSubscribed: Bool?) {
        let endpoint = "page/" + (pageType ?? "")
        pageTypeVm = PageTypeVM(repo: HomeScreenRepo(), endPoint: endpoint, pageType: (pageType ?? ""), isSubscribed: isSubscribed ?? false, appPageName: appName ?? "")
        getDetailScreenData(pageTypeForApp: pageType, appname: appName, isSubscribed: isSubscribed)
    }
    
    func getDetailScreenData(pageTypeForApp: String?, appname: String?, isSubscribed: Bool?) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = pageTypeVm {
                dataModel.getHomeScreenData {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.pushToAppDetailScreen(appname, isSubscribed: isSubscribed, pageTypeApp: pageTypeForApp)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func pushToAppDetailScreen(_ appname: String?, isSubscribed: Bool?, pageTypeApp: String?) {
        let appDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.appDetail.rawValue, type: BAAppsDetailViewController.self)
        appDetailVC.parentNavigation = self.parentNavigationController
        appDetailVC.appContentData = self.pageTypeVm?.homeScreenData
        appDetailVC.appName = appname
        appDetailVC.pageType = pageTypeApp
        appDetailVC.isSubscribed = isSubscribed
        self.parentNavigationController?.pushViewController(appDetailVC, animated: true)
    }
    
    @objc func moveToDetailScreenFromPush() {
        fetchContentDetail(railType: "", layoutType: "", vodId: 6734, contentType: "MOVIES", expiryTime: 0, pageSource: "", configSource: "", railName: "")
    }
    
    override func retryButtonAction() {
        self.getHomeData()
    }
    
    func showHideWaterMark() {
        guard let _ = homeViewModel?.homeScreenData?.data?.items?.count else {
            tableView.addWatermark(with: "No Content available to watch")
            return
        }
        tableView.addWatermark(with: "")
    }
}

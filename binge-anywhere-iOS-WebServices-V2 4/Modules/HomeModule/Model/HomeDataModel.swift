//
//  HomeDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 20/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

class HomeScreenDataModel: Codable {
    let code: Int?
    let message: String?
    var data: HomeScreenData?

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(HomeScreenData.self, forKey: .data)
    }
}

class HomeScreenData: Codable {
    var items: [HomeScreenDataItems]?
    let total: Int?
    let offset: Int?
    let limit: Int?

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([HomeScreenDataItems].self, forKey: .items)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        offset = try values.decodeIfPresent(Int.self, forKey: .offset)
        limit = try values.decodeIfPresent(Int.self, forKey: .limit)
    }

}

class HomeScreenDataItems: Codable {
    var id: Int?
    var title: String?
    var placeHolder: String?
    var sectionType: String?
    var sectionSource: String?
    var layoutType: String?
    var imageMetadata: ImageMetadata?
    var contentList: [BAContentListModel]?
    var autoScroll: Bool?
    var recommendationPosition: String?
    var totalCount: Int?
    var position: Int?
    var configType: String?
    var specialRail: Bool?
    var provider: String?
    var lastScrolledOffset: CGFloat = 0.0 // used to remember the last scoll offset for collection view inside table view cell on Home Page
    init() {
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        sectionSource = try values.decodeIfPresent(String.self, forKey: .sectionSource)
        placeHolder = try values.decodeIfPresent(String.self, forKey: .placeHolder)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        imageMetadata = try values.decodeIfPresent(ImageMetadata.self, forKey: .imageMetadata)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        autoScroll = try values.decodeIfPresent(Bool.self, forKey: .autoScroll)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        position = try values.decodeIfPresent(Int.self, forKey: .position)
        configType = try values.decodeIfPresent(String.self, forKey: .configType)
        specialRail = try values.decodeIfPresent(Bool.self, forKey: .specialRail)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        recommendationPosition = try values.decodeIfPresent(String.self, forKey: .recommendationPosition)
    }

}

struct ImageMetadata: Codable {
    let cardSize: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cardSize = try values.decodeIfPresent(String.self, forKey: .cardSize)
    }
}

struct BAContinueWatchingModal: Codable {
    let code: Int?
    let message: String?
    var data: ContinueWatchingData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ContinueWatchingData.self, forKey: .data)
    }
}

struct ContinueWatchingData: Codable {
    let id: Int?
    let title: String?
    let sectionType: String?
    let layoutType: String?
    var contentList: [BAContentListModel]?
    let totalCount: Int?
    var continuePagination: Bool?
    let continueWatching: Bool?
    var pagingState: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        continuePagination = try values.decodeIfPresent(Bool.self, forKey: .continuePagination)
        continueWatching = try values.decodeIfPresent(Bool.self, forKey: .continueWatching)
        pagingState = try values.decodeIfPresent(String.self, forKey: .pagingState)
    }
}

enum CardSize: String, Codable {
    case cardSizeDEFAULT = "DEFAULT"
    case cardSizeSMALL = "SMALL"
}

enum ContentShowType: String, Codable {
    case vodMovies = "VOD_MOVIES"
    case vodTvShows = "VOD_TV_SHOWS"
    case vodOnly  = "VOD_"
}

enum ContentType: String {
    case brand = "BRAND"
    case vod = "VOD"
    case tavodMovie = "VOD-MOVIES"
    case movies = "MOVIES"
    case series = "SERIES"
    case customSeries = "CUSTOM_SERIES_DETAIL"
    case customBrand = "CUSTOM_BRAND_DETAIL"
    case customMovies = "CUSTOM_MOVIES_DETAIL"
    case customTVShows = "CUSTOM_TV_SHOWS_DETAIL"
    case customWebShorts = "CUSTOM_WEB_SHORTS_DETAIL"
    case tvShows = "TV_SHOWS"
    case webShorts = "WEB_SHORTS"
    case rental = "RENTAL"
    case genre1 = "GENRE1"
    case genre2 = "GENRE2"
    case language1 = "LANGUAGE1"
    case language2 = "LANGUAGE2"
}

enum PartnerSubscriptionType: String {
   case premium = "PREMIUM"
    case free = "FREE"
}

enum ContractName: String {
    case free = "FREE"
    case clear = "CLEAR"
}

enum Language: String, Codable {
    case english = "English"
}

enum Provider: String, Codable {
    case tatasky = "Tatasky"
}

// MARK: - Enum for types of rail on Home screen
enum RailType: String {
    case rail = "RAIL"
    case hero = "HERO_BANNER"

    var itemHeight: CGFloat {
        switch self {
        case .rail:
            return railSize
        case .hero:
            return heroSize
        }
    }
}

// MARK: - Enum for sizes of cell in each type of rail on Home Screen.
enum LayoutType: String {
    case landscape = "LANDSCAPE"
    case potrait = "PORTRAIT"
    case circular = "CIRCULAR"
    case category = "CATEGORY"

    var itemHeight: CGFloat {
        switch self {
        case .landscape:
            return landscapeSize
        case .potrait:
            return potraitSize
        case .circular:
            return circularSize
        case .category:
            return categorySize
        }
    }
}

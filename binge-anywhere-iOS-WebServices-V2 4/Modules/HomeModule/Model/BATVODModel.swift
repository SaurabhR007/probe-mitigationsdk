//
//  BATVODModel.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

struct BATVODModel: Codable {
    let code: Int?
    let message: String?
    var data: TVODData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(TVODData.self, forKey: .data)
    }

}

struct TVODData: Codable {
    var items: [BAContentListModel]?
    let offset: Int?
    let limit: Int?
    let total: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([BAContentListModel].self, forKey: .items)
        offset = try values.decodeIfPresent(Int.self, forKey: .offset)
        limit = try values.decodeIfPresent(Int.self, forKey: .limit)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }

}

//struct TVODItems: Codable {
//    let id: Int?
//    let title: String?
//    let image: String?
//    let subTitles: [String]?
//    let contentType: String?
//    let contractName: String?
//    let entitlements: [String]?
//    let rentalPrice: String?
//    let rentalExpiry: Int?
//    let rentalStatus: String?
//    let genre: [String]?
//    let duration: Int?
//    let description: String?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        image = try values.decodeIfPresent(String.self, forKey: .image)
//        subTitles = try values.decodeIfPresent([String].self, forKey: .subTitles)
//        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
//        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
//        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
//        rentalPrice = try values.decodeIfPresent(String.self, forKey: .rentalPrice)
//        rentalExpiry = try values.decodeIfPresent(Int.self, forKey: .rentalExpiry)
//        rentalStatus = try values.decodeIfPresent(String.self, forKey: .rentalStatus)
//        genre = try values.decodeIfPresent([String].self, forKey: .genre)
//        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//    }
//
//}

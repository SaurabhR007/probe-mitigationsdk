//
//  BABrowseByModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BABrowseByModal: Codable {
    let message: String?
    let code: Int?
    let status: Int?
    let data: BrowseByData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        data = try values.decodeIfPresent(BrowseByData.self, forKey: .data)
    }
}

struct BrowseByData: Codable {
    let layoutType: String?
    let totalContent: Int?
    let id: Int?
    let title: String?
    let sectionType: String?
    let type: String?
    let contentList: [BAContentListModel]?
    var continuePaging: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        totalContent = try values.decodeIfPresent(Int.self, forKey: .totalContent)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        continuePaging = try values.decodeIfPresent(Bool.self, forKey: .continuePaging)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
    }
}



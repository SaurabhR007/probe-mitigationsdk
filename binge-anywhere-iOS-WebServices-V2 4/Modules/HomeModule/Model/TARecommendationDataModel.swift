//
//  TARecommendationDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 07/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

class TARecommendationDataModel: Codable {
    let code: Int?
    let message: String?
    var data: ContentList?

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ContentList.self, forKey: .data)
    }
}

// MARK: - DataClass
class ContentList: Codable {
    let contentList: [BAContentListModel]?
    let genreType: String?
    let languageType: String?

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        genreType = try values.decodeIfPresent(String.self, forKey: .genreType)
        languageType = try values.decodeIfPresent(String.self, forKey: .languageType)
    }
}

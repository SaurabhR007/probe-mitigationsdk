//
//  PrimeDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation


class PrimeResumeAPIResponse: Codable {
    var code: Int?
    var title: String?
    var message: String?
    var amount: String?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        amount = try values.decodeIfPresent(String.self, forKey: .amount)
    }
}

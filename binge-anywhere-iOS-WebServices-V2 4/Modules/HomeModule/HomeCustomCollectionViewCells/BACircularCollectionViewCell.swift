//
//  BACircularCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class BACircularCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subscribedImageView: UIImageView!
    @IBOutlet weak var appIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCircularCell(_ item: BAContentListModel?, subscribedPartners: PartnerSubscriptionDetails?) {
        appIcon.layer.borderWidth = 1.0
        appIcon.layer.masksToBounds = false
        appIcon.layer.borderColor = UIColor.clear.cgColor
        appIcon.layer.cornerRadius = appIcon.frame.size.width / 2
        appIcon.clipsToBounds = true
        appIcon.backgroundColor = .white
        //        let url = URL.init(string: item?.image ?? "")
        //        appIcon.kf.setImage(with: ImageResource(downloadURL: url!))
        var url = URL.init(string: "")
        switch item?.provider?.uppercased() {
        case ProviderType.prime.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoCircular ?? "")
        case ProviderType.sunnext.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoCircular ?? "")
        case ProviderType.netflix.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.NETFLIX?.logoCircular ?? "")
        case ProviderType.shemaro.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoCircular ?? "")
        case ProviderType.vootSelect.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoCircular ?? "")
        case ProviderType.vootKids.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoCircular ?? "")
        case ProviderType.hungama.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoCircular ?? "")
        case ProviderType.zee.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoCircular ?? "")
        case ProviderType.sonyLiv.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoCircular ?? "")
        case ProviderType.hotstar.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoCircular ?? "")
        case ProviderType.eros.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoCircular ?? "")
        case ProviderType.curosityStream.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoCircular ?? "")
        case ProviderType.hopster.rawValue: appIcon.image = UIImage(named: "hopster")
        default: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoCircular ?? "")
        }
        if let _url = url {
            appIcon.alpha = appIcon.image == nil ? 0 : 1
            appIcon.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
                UIView.animate(withDuration: 1, animations: { self.appIcon.alpha = 1 })
            })
        }
        checkForSubcribed(item, subscribedPartners: subscribedPartners)
    }
    
    func configureCircularCellForContent(_ item: BAContentListModel?) {
        appIcon.layer.borderWidth = 1.0
        appIcon.layer.masksToBounds = false
        appIcon.layer.borderColor = UIColor.clear.cgColor
        appIcon.layer.cornerRadius = appIcon.frame.size.width / 2
        appIcon.clipsToBounds = true
        appIcon.contentMode = .scaleAspectFill
        appIcon.backgroundColor = .clear
        
        let cellWidth = Int(appIcon.frame.width)
        let cellHeight = Int(appIcon.frame.height)
        let imageUrl = (item?.image ?? "")
        let imageURL = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
        let url = URL.init(string: imageURL ?? "")
        
        UtilityFunction.shared.performTaskInMainQueue {
            if let _url = url {
                self.appIcon.alpha = self.appIcon.image == nil ? 0 : 1
                self.appIcon.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler:  { _ in
                    UIView.animate(withDuration: 1, animations: { self.appIcon.alpha = 1 })
                })
            }
        }
    }
    
    // MARK: Check for subscribed view
    func checkForSubcribed(_ item: BAContentListModel?, subscribedPartners: PartnerSubscriptionDetails?) {
        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
            subscribedImageView.isHidden = true
        } else if (subscribedPartners?.providers?.count ?? 0 > 0 && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status != "DEACTIVE") {
            var providerName = ""
            if item?.title == "Sun_Nxt" || item?.title == "Sun_nxt" || item?.title == "sun_Nxt" || item?.title == "sun_nxt" {
                item?.title = "SunNxt"
            }
            if let providers = subscribedPartners?.providers {
                for partner in providers {
                    if partner.name == "Sun_Nxt" || partner.name == "Sun_nxt" || partner.name == "sun_Nxt" || partner.name == "sun_nxt" {
                        providerName = "SunNxt"
                    }
                    if partner.name?.lowercased() == item?.title?.lowercased() || providerName.lowercased() == item?.title?.lowercased() {
                        subscribedImageView.isHidden = false
                        return
                    } else {
                        subscribedImageView.isHidden = true
                    }
                }
            }
        } else {
            subscribedImageView.isHidden = true
        }
    }
    
    // MARK: For Testing Static Provider Logo
    func configureCircularCellTest(_ index: Int?, isSubscribed: Bool) {
        subscribedImageView.isHidden = !isSubscribed
        appIcon.layer.borderWidth = 1.0
        appIcon.layer.masksToBounds = false
        appIcon.layer.borderColor = UIColor.white.cgColor
        appIcon.layer.cornerRadius = appIcon.frame.size.width / 2
        appIcon.clipsToBounds = true
        appIcon.backgroundColor = .white
        switch index {
        case 0:
            appIcon.image = UIImage(named: "sun")
        case 1:
            appIcon.image = UIImage(named: "ama")
        case 2:
            appIcon.image = UIImage(named: "zeeee")
        case 3:
            appIcon.image = UIImage(named: "ero")
        case 4:
            appIcon.image = UIImage(named: "hot")
        default:
            appIcon.image = UIImage(named: "sun")
        }
    }
}

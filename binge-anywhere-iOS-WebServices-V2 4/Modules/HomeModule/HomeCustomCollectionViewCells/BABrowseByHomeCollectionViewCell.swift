//
//  BABrowseByHomeCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/10/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class BABrowseByHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var browseByLabel: UILabel!
    @IBOutlet weak var browseByImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
    }
    
    func addGradient() {
        gradientView.isHidden = false
        if let gradientLayer = gradientView.layer.sublayers?.first as? CAGradientLayer {
            gradientLayer.removeFromSuperlayer()
        }
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.orange.withAlphaComponent(0.1).cgColor,
                           UIColor.red.withAlphaComponent(0.1).cgColor,
                           UIColor.purple.withAlphaComponent(0.1).cgColor,
                           UIColor.blue.withAlphaComponent(0.1).cgColor]
        gradient.locations = [0.0, 0.06, 0.21, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: gradientView.frame.size.width, height: gradientView.frame.size.height)
        gradientView.layer.insertSublayer(gradient, at: 0)
    }
    
    func configureCell(_ item: BAContentListModel?, intent: String?) {
        self.contentView.layer.cornerRadius = 5
        browseByImage.layer.cornerRadius = 5
        browseByLabel.text = item?.title
        
        let cellWidth = Int(browseByImage.frame.width)
        let cellHeight = Int(browseByImage.frame.height)
        let imageUrl = (item?.image ?? "")
        let imageURL = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
        let url = URL.init(string: imageURL ?? "")
        if let url = url {
            browseByImage.alpha = (browseByImage.image == nil) ? 0 : 1
            debugPrint("this is browse url \(url)")
            UtilityFunction.shared.performTaskInMainQueue {
                self.browseByImage.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { result in
                    switch result {
                    case .success(let value):
                        break
//                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("BABrowseByHomeCollectionViewCell Error: \(error)")
                    }
                    UIView.animate(withDuration: 0.3, animations: { self.browseByImage.alpha = 1 })
                })
            }
        } else {
            browseByImage.image = nil
        }
    }
}

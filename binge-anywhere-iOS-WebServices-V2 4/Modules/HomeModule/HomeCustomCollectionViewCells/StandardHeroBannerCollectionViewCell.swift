//
//  StandardHeroBannerCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 19/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Kingfisher
import FSPagerView
class StandardHeroBannerCollectionViewCell: FSPagerViewCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        layoutIfNeeded()
        configUI()
    }
    private func configUI() {
        self.backgroundColor = .BAdarkBlueBackground
        if let gradientLayer = gradientView.layer.sublayers?.first as? CAGradientLayer {
            gradientLayer.removeFromSuperlayer()
        }
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.black.withAlphaComponent(0).cgColor, UIColor.black.withAlphaComponent(0.45).cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: gradientView.frame.size.width, height: gradientView.frame.size.height)
        gradientView.layer.insertSublayer(gradient, at: 0)
    }
    
    func configureCell(_ item: BAContentListModel) {
        var url = URL.init(string: "")
        if let image = item.imageUrlApp, image.length > 0 {
            let cellWidth = Int(bannerImageView.frame.width)
            let cellHeight = Int(bannerImageView.frame.height)
            let imageUrl = (item.imageUrlApp ?? "")
            let imageURL = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
            url = URL.init(string: imageURL ?? "")
        } else {
            url = URL.init(string: item.image ?? "")
        }
        setImage(with: bannerImageView, url) { flag in
            if !flag {
                self.bannerImageView.image = nil
            }
        }
        url = nil
        switch item.provider?.uppercased() {
        case ProviderType.prime.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoRectangular ?? "")
        case ProviderType.sunnext.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoRectangular ?? "")
        case ProviderType.netflix.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.NETFLIX?.logoRectangular ?? "")
        case ProviderType.shemaro.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoRectangular ?? "")
        case ProviderType.vootSelect.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoRectangular ?? "")
        case ProviderType.vootKids.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoRectangular ?? "")
        case ProviderType.hungama.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoRectangular ?? "")
        case ProviderType.zee.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoRectangular ?? "")
        case ProviderType.sonyLiv.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoRectangular ?? "")
        case ProviderType.hotstar.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoRectangular ?? "")
        case ProviderType.eros.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoRectangular ?? "")
        case ProviderType.curosityStream.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoRectangular ?? "")
        case ProviderType.hopster.rawValue: logoImageView.image = UIImage(named: "hopster")
        default: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoRectangular ?? "")
        }
        setImage(with: logoImageView, url) { flag in
            if !flag {
                self.logoImageView.image = nil
            }
        }
    }
    
    private func setImage(with imageView: UIImageView, _ url: URL?, completion: ((Bool) -> Void)? = nil) {
        UtilityFunction.shared.performTaskInMainQueue {
            if let _url = url {
                imageView.alpha = (imageView.image == nil) ? 0 : 1
                imageView.kf.setImage(with: _url, completionHandler:  { result in
                    switch result {
                    case .success(let value):
                        break
//                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("StandardHeroBannerCollectionViewCell Error: \(error)")
                    }
                    UIView.animate(withDuration: 1, animations: { imageView.alpha = 1 })
                })
                completion?(true)
            } else {
                completion?(false)
            }
        }
    }
}

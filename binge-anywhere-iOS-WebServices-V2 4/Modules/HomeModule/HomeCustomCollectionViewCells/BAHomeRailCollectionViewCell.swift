//
//  CollectionViewCell.swift
//  HomeVIewWithBanner
//
//  Created by Shivam on 05/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class BAHomeRailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var validationLeftLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var continueWatchingView: UIView!
    @IBOutlet weak var progressBarContraint: NSLayoutConstraint!
    @IBOutlet weak var logoImageWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()
    }
    
    // MARK: - Functions
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
        titleImageView.image = nil
        //        logoImageView.image = nil
        logoImageWidthConstraint.constant = 0
        continueWatchingView.isHidden = true
        progressBarContraint.constant = 0
        if let gradient = continueWatchingView.layer.sublayers {
            for obj in gradient where obj is CAGradientLayer {
                obj.removeFromSuperlayer()
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
    }
    
    func configureUI() {
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BARailTitleColor)
        self.contentView.backgroundColor = .BAdarkBlue2Color
        self.cornerRadius = 3
        continueWatchingView.isHidden = true
    }
    
    func addGradient() {
        continueWatchingView.isHidden = false
        if let gradientLayer = continueWatchingView.layer.sublayers?.first as? CAGradientLayer {
            gradientLayer.removeFromSuperlayer()
        }
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.orange.withAlphaComponent(0.1).cgColor,
                           UIColor.red.withAlphaComponent(0.1).cgColor,
                           UIColor.purple.withAlphaComponent(0.1).cgColor,
                           UIColor.blue.withAlphaComponent(0.1).cgColor]
        gradient.locations = [0.0, 0.06, 0.21, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: continueWatchingView.frame.size.width, height: continueWatchingView.frame.size.height)
        continueWatchingView.layer.insertSublayer(gradient, at: 0)
    }
    
    func showProgress(from: Int, to: Int) {
        let progress: Double = Double(from) / Double(to)
        if !progress.isNaN {
            progressBarContraint.constant = CGFloat(progress * Double(self.continueWatchingView.frame.width))
        }
    }
    
    func configureCell(_ item: BAContentListModel?, _ layOutType: String) {
        if let item = item {
            if item.seriesTitle?.isEmpty ?? true {
                titleLabel.text = item.title
            } else {
                titleLabel.text = item.seriesTitle
            }
            titleImageView.contentMode = .scaleToFill
            if item.contractName == "RENTAL" {
                logoImageView.isHidden = true
                validationLeftLabel.isHidden = false
                let timeLeft: Double = Double(convertTimestampDifference(timeStamp: item.rentalExpiry))
                if timeLeft < 0 {
                    validationLeftLabel.text = ""
                } else {
                    if timeLeft < 60 {
                        validationLeftLabel.text = "Expires in " + String(timeLeft) + " min"
                    } else {
                        let timeLeftInHours = timeLeft / 60
                        if timeLeftInHours > 48 {
                            let timeLeftDoubleValue: Double = timeLeft / 1440
                            let timeLeftInDays = Int(timeLeftDoubleValue.rounded(.awayFromZero))
                            validationLeftLabel.text = String(timeLeftInDays) + " Days" + " Left"
                        } else {
                            let timeleftinHours = Double(timeLeftInHours)
                            let timeleftInHoursInt = Int(timeleftinHours.rounded(.awayFromZero))
                            validationLeftLabel.text = "Expires in " + String(timeleftInHoursInt) + " hours"
                        }
                    }
                }
            } else {
                logoImageView.isHidden = false
                validationLeftLabel.isHidden = true
            }
            
            var cellWidth = Int(logoImageView.frame.width)
            var cellHeight = Int(logoImageView.frame.height)
            
            // In the last added web p support so hardcoded the value to get proper image downloded.
            if layOutType == "LANDSCAPE" {
                cellWidth = 137
                cellHeight = 78
            } else if layOutType == "PORTRAIT" {
                cellWidth = 125
                cellHeight = 246
            } else if layOutType == "" {
                cellWidth = 137
                cellHeight = 78
            }
            
            var imageUrl = ""//(item.image ?? "")
            if item.contentType == ContentType.tvShows.rawValue {
                imageUrl = item.seriesimage ?? ""
                if imageUrl.isEmpty {
                    imageUrl = item.image ?? ""
                }
            } else {
                imageUrl = item.image ?? ""
            }
            let imageURL = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
            
            
            var url = URL.init(string: imageURL ?? "")
            setImage(with: titleImageView, url)
            url = nil
            switch item.provider?.uppercased() {
            case ProviderType.prime.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoRectangular ?? "")
            case ProviderType.sunnext.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoRectangular ?? "")
            case ProviderType.netflix.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.NETFLIX?.logoRectangular ?? "")
            case ProviderType.shemaro.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoRectangular ?? "")
            case ProviderType.vootSelect.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoRectangular ?? "")
            case ProviderType.vootKids.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoRectangular ?? "")
            case ProviderType.hungama.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoRectangular ?? "")
            case ProviderType.zee.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoRectangular ?? "")
            case ProviderType.hotstar.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoRectangular ?? "")
            case ProviderType.eros.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoRectangular ?? "")
            case ProviderType.sonyLiv.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoRectangular ?? "")
            case ProviderType.curosityStream.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoRectangular ?? "")
            case ProviderType.hopster.rawValue:
                logoImageView.image = UIImage(named: "hopster")
                self.updateLogoWidth(image: logoImageView.image)
            default:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoRectangular ?? "")
            }
            setImage(with: logoImageView, url) { (success, image) in
                if success {
                    self.updateLogoWidth(image: image)
                }
            }
        }
    }
    
    private func setImage(with imageView: UIImageView, _ url: URL?, completion: ((Bool, UIImage) -> Void)? = nil) {
        UtilityFunction.shared.performTaskInMainQueue {
            if let _url = url {
                imageView.alpha = (imageView.image == nil) ? 0 : 1
                imageView.kf.setImage(with: _url, completionHandler:  { result in
                    UIView.animate(withDuration: 1, animations: { imageView.alpha = 1 })
                    switch result {
                    case .success(let data):
                        completion?(true, data.image)
                    case .failure(let error):
                        print("BAHomeRailCollectionViewCell Error: \(error)")
                        break
                    }
                })
            }
        }
    }

    
    // MARK: To set the constraint of logoImageView in such a way that image get aligned on left
    func updateLogoWidth(image: UIImage?) {
        let imageWidth = ((image?.size.width ?? 1) / (image?.size.height ?? 1)) * logoImageView.frame.size.height
        logoImageWidthConstraint.constant = imageWidth
    }
    
    // MARK: Calculate Difference between current and API timestamp
    func convertTimestampDifference(timeStamp: Int?)  -> Int{
        let currentTime = Date().currentTimeMillis()
        let timeDifference = (Int64(timeStamp ?? 0) - currentTime) / 60000
        return Int(timeDifference)
    }
}

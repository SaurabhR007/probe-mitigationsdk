//
//  HomeVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 20/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol HomeVMProtocol {
    func getHomeScreenData(completion: @escaping(Bool, String?, Bool) -> Void)
    func checkForContinueWatchData(group: DispatchGroup, _ completion: @escaping () -> Void)
    func checkForTVODData( _ completion: @escaping () -> Void)
    func checkForBrowseByLanguage(_ completion: @escaping () -> Void)
    func checkForBrowseByGenre(_ completion: @escaping () -> Void)
}

extension HomeVMProtocol {
    func checkForContinueWatchData(group: DispatchGroup, _ completion: @escaping () -> Void) {}
    func checkForTVODData( _ completion: @escaping () -> Void) {}
    func checkForBrowseByLanguage(_ completion: @escaping () -> Void) {}
    func checkForBrowseByGenre(_ completion: @escaping () -> Void) {}
}

class HomeVM: HomeVMProtocol {
    var repo: HomeScreenDetailRepo
    var pageType: String
    var nextPageAvailable = true
    var pageLimit = 10
    var homeScreenData: HomeScreenDataModel?
    var pageOffset = 0
    var requestToken: ServiceCancellable?
    var endPoint: String = ""
    var apiParams = APIParams()
    var continueWatchingVM: BAContinueWatchingViewModal?
    var browseByViewModal: BABrowseByViewModal?
    var tvodVM: BATVODViewModal?
    var tvodData: TVODData?
    var isFromSubscribedHome = false
    var taRecommendationVM = TARecommendationVM()
    var partnerSubscription: PartnerSubscriptionDetails?
    var totalDataCount = 0
    
    init(repo: HomeScreenDetailRepo, endPoint: String, pageType: String, isFromHome: Bool = false) {
        self.repo = repo
        self.pageType = pageType
        self.isFromSubscribedHome = isFromHome
        self.repo.apiEndPoint = endPoint
    }
    
    func getHomeScreenData(completion: @escaping(Bool, String?, Bool) -> Void) {
        if isFromSubscribedHome {
            apiParams["pageLimit"] = String(pageLimit)
            apiParams["pageOffset"] = String(pageOffset)
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.count ?? 0 > 0 && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status != "DEACTIVE" {
                apiParams["Subscribed"] = "true"
                apiParams["UnSubscribed"] = "false"
            } else {
                apiParams["Subscribed"] = "false"
                apiParams["UnSubscribed"] = "true"
            }
        } else {
            apiParams["pageLimit"] = String(pageLimit)
            apiParams["pageOffset"] = String(pageOffset)
        }
        
        if requestToken != nil {
            return
        }
        
        requestToken = repo.getHomeTypeScreenData(apiParams: apiParams, completion: { (configData, error) in
            if let _configData = configData {
                self.requestToken = nil
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        let count = _configData.parsed.data?.items?.count ?? 0
                        if self.pageOffset == 0 {
                            self.homeScreenData = _configData.parsed
                            self.totalDataCount = _configData.parsed.data?.items?.count ?? 0
                        } else {
                            self.homeScreenData?.data?.items?.append(contentsOf: (_configData.parsed.data?.items)!)
                            self.totalDataCount  += _configData.parsed.data?.items?.count ?? 0
                        }
                        if let homeData = self.homeScreenData?.data {
                            if let _ = homeData.items {
                                self.nextPageAvailable = (self.pageOffset) < (homeData.total ?? 0)
                                let group = DispatchGroup()
                                group.enter()
//                                self.checkForContinueWatchData(group: group) {
//                                    group.leave()
//
//                                    ///////////////
//                                    group.enter()
//                                    self.makeTARecommendationCalls(group: group, {
//                                        group.leave()
//                                    })
//
//                                    //////////////
//                                    group.notify(queue: .main) {
//                                        self.pageOffset += count
//                                        self.omitBlankContentSectionRail()
//                                        completion(true, nil, false)
//                                    }
//                                }
                                self.checkForTVODData {
                                    let group = DispatchGroup()
                                    group.enter()
                                    self.checkForContinueWatchData(group: group, {
                                        group.leave()
                                    })
                                    group.enter()
                                    self.makeTARecommendationCalls(group: group, {
                                        group.leave()
                                    })
                                    group.notify(queue: .main) {
                                        self.pageOffset += count
                                        self.omitBlankContentSectionRail()
                                        completion(true, nil, false)
                                    }
                                }
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    } else {
                        self.requestToken = nil
                        completion(false, "Some Error Occurred", false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else if let error = error {
                    self.requestToken = nil
                    completion(false, error.error.localizedDescription, true)
                } else {
                    self.requestToken = nil
                    completion(false, "Some Error Occurred", true)
                }
            } else {
                // if any how parsing failed
                self.requestToken = nil
                completion(false, "Some Error Occurred", false)
            }
        })
    }
    
    func checkForContinueWatchData(group: DispatchGroup, _ completion: @escaping () -> Void) {
        self.checkForTVODDataInCW {
            if let data = self.homeScreenData?.data?.items {
                for (index, item) in data.enumerated() where item.sectionSource == "CONTINUE_WATCHING" {
                    self.continueWatchingVM = BAContinueWatchingViewModal(repo: BAContinueWatchingRepo(), endPoint: "watched", baId: BAKeychainManager().baId)
                    if let cwVM = self.continueWatchingVM {
                        cwVM.getContinueWatchingtData { (flagValue, _, isApiError) in
                            if flagValue {
                                let index = (item.position ?? index) <= index ? (item.position ?? index) : index
                                let cwList = cwVM.watchListData?.data?.contentList
                                cwVM.watchListData?.data?.contentList?.removeAll()
                                for cwData in cwList ?? [] {
                                    if cwData.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == cwData.title}) ?? true {
                                                for item in self.tvodData?.items ?? [] {
                                                    if item.title == cwData.title {
                                                        cwData.rentalExpiry = item.rentalExpiry
                                                        cwVM.watchListData?.data?.contentList?.append(cwData)
                                                    }
                                                }
                                                //cwData.rentalExpiry = self.tvodData?.items?[0].rentalExpiry
                                                //cwVM.watchListData?.data?.contentList?.append(cwData)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        cwVM.watchListData?.data?.contentList?.append(cwData)
                                    }
                                }
                                let arrayIndex = self.homeScreenData?.data?.items?.indices.contains(index) ?? false
                                if let data = cwVM.watchListData?.data?.contentList {
                                    if arrayIndex {
                                        self.homeScreenData?.data?.items?[index].contentList = data
                                    }
                                } else {
                                    if arrayIndex {
                                        self.homeScreenData?.data?.items?.remove(at: index)
                                    }
                                }
                                completion()
                            } else {
                                let array = self.homeScreenData?.data?.items?.indices.contains(index) ?? false
                                if array {
                                    self.homeScreenData?.data?.items?.remove(at: index)
                                }
                                completion()
                            }
                        }
                        return
                    }
                    break
                }
                completion()
            } else {
                completion()
            }
        }
    }
    
    
    func filterContentList() {
        if let item = self.homeScreenData?.data?.items {
            for i in 0..<item.count {
                if let item = self.homeScreenData?.data?.items?[i].contentList {
                    if self.homeScreenData?.data?.items?[i].sectionSource?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item, false)
                    } else if self.homeScreenData?.data?.items?[i].sectionSource?.lowercased() == SectionSourceType.continueWatching.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item, false)
                    } else if self.homeScreenData?.data?.items?[i].sectionType?.lowercased() == SectionSourceType.heroBanner.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item, false)
                    } else if self.homeScreenData?.data?.items?[i].sectionSource?.lowercased() != SectionSourceType.tvod.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.removeRentalData(item)
                    } else {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item)
                    }
                }
            }
        }
    }
    
    // MARK: Get TVOD Content List Of Active Ones
    func checkForTVODDataInCW( _ completion: @escaping () -> Void) {
        tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: 0)
        if let tVODVM = tvodVM {
            tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                if flagValue {
                    self.tvodData = tVODVM.tvodModal?.data
                    completion()
                } else {
                    completion()
                }
            }
            return
        }
        completion()
    }
    
    func checkForTVODData( _ completion: @escaping () -> Void) {
        if let data = homeScreenData?.data?.items {
            for (index, item) in data.enumerated() where item.sectionSource == "TVOD" {
                tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: item.id ?? 0)
                if let tVODVM = tvodVM {
                    tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                        if flagValue {
                            let index = (item.position ?? index) <= index ? (item.position ?? index) : index
                            let array = self.homeScreenData?.data?.items?.indices.contains(index) ?? false
                            if array {
                                if let data = tVODVM.tvodModal?.data?.items {
                                    self.homeScreenData?.data?.items?[index].contentList = data
                                } else {
                                    self.homeScreenData?.data?.items?.remove(at: index)
                                }
                            }
                            completion()
                        } else {
                            self.homeScreenData?.data?.items?.remove(at: index)
                            completion()
                        }
                    }
                    return
                }
                break
            }
            completion()
        } else {
            completion()
        }
    }
    
    func omitBlankContentSectionRail() {
        self.filterContentList()
        self.homeScreenData?.data?.items = self.homeScreenData?.data?.items?.filter({ (data) -> Bool in
            return !(data.contentList?.isEmpty ?? true)
        })
        updatePrimeRailSectionName()
    }
    
    func updatePrimeRailSectionName() {
        if let data = homeScreenData?.data?.items {
            let primeSectionRail = data.filter { (homeScreenData) -> Bool in
                return homeScreenData.sectionSource?.lowercased() == SectionSourceType.prime.rawValue.lowercased()
            }
            if primeSectionRail.count > 0 {
                let subscribedPrimeHeader = BAConfigManager.shared.configModel?.data?.config?.subscribedTitle ?? "Prime Rail"
                primeSectionRail[0].title = subscribedPrimeHeader
                /** As per demo on 9th june the title is updated to be subscribed for every time
                if let _ = UtilityFunction.shared.checkForPrimeSubscription() {
                    let subscribedPrimeHeader = BAConfigManager.shared.configModel?.data?.config?.subscribedTitle ?? "Prime Rail"
                    primeSectionRail[0].title = subscribedPrimeHeader
                } else {
                    let unSubscribedPrimeHeader = BAConfigManager.shared.configModel?.data?.config?.unSubscribedTitle ?? "Prime Rail"
                    primeSectionRail[0].title = unSubscribedPrimeHeader
                }*/
            }
        }
    }
    
    func makeTARecommendationCalls(group: DispatchGroup,_ completion: @escaping () -> Void) {
        if let homeScreenData = homeScreenData?.data?.items {
            taRecommendationVM.organiseTAApiRequestHit(homeScreenData, pageOffset, self.pageType, completion: { (data) in
                completion()
            })
        } else {
            completion()
        }
    }
    
    // MARK: Get TVOD Content List Of Active Ones
    func checkForTVOD( _ completion: @escaping () -> Void) {
        tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: 0)
        if let tVODVM = tvodVM {
            tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                if flagValue {
                    self.tvodData = tVODVM.tvodModal?.data
                    completion()
                } else {
                    completion()
                }
            }
            return
        }
        completion()
    }
}

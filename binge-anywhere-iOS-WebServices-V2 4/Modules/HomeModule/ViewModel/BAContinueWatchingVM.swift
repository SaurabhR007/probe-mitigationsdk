//
//  BAContinueWtchingVM.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 28/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAContinueWatchingViewModalProtocol {
    var watchListData: BAContinueWatchingModal? {get set}
    var nextPageAvailable: Bool {get set}
    func getContinueWatchingtData(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAContinueWatchingViewModal: BAContinueWatchingViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAContinueWatchingRepo
    var tvodVM: BATVODViewModal?
    var tvodData: TVODData?
    var baId: String = "0"
    var endPoint: String = ""
    var nextPageAvailable = false
    var pageLimit = 20
    var pageOffset = 0
    var watchListData: BAContinueWatchingModal?
    var apiParams = APIParams()

    init(repo: BAContinueWatchingRepo, endPoint: String, baId: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.baId = baId
    }

    func getContinueWatchingtData(completion: @escaping(Bool, String?, Bool) -> Void) {
        self.checkForTVODData {
            self.apiParams["subscriberId"] = BAKeychainManager().sId
            self.apiParams["profileId"] = BAKeychainManager().profileId
            self.apiParams["max"] = String(self.pageLimit)
            self.apiParams["offSet"] = String(self.pageOffset)

            self.requestToken = self.repo.getWatchListData(apiParams: self.apiParams, completion: { configData, error in
                self.requestToken = nil
                if let _configData = configData {
                    if let statusCode = _configData.parsed.code {
                        if statusCode == 0 {
                            if self.pageOffset == 0 {
                                self.watchListData = _configData.parsed
                                self.watchListData?.data?.contentList?.removeAll()
                                for cwData in _configData.parsed.data?.contentList ?? [] {
                                    if cwData.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == cwData.title}) ?? true {
                                                self.watchListData?.data?.contentList?.append(cwData)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        self.watchListData?.data?.contentList?.append(cwData)
                                    }
                                }
                            } else {
                                for content in (_configData.parsed.data?.contentList)! {
                                    if content.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == content.title}) ?? true {
                                                self.watchListData?.data?.contentList?.append(content)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        self.watchListData?.data?.contentList?.append(content)
                                    }
                                }
                            }
                            if let screenData = self.watchListData?.data {
                                if let watchListData = screenData.contentList {
                                    self.pageOffset = watchListData.count
                                    self.nextPageAvailable = watchListData.count < (screenData.totalCount ?? 0)
                                }
                            }
                            completion(true, nil, false)
                        } else {
                            if _configData.serviceResponse.statusCode != 200 {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, true)
                                } else {
                                    completion(false, "Some Error Occurred", true)
                                }
                            } else {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, false)
                                } else {
                                    completion(false, "Some Error Occurred", false)
                                }
                            }
                        }
                    } else if _configData.serviceResponse.statusCode == 401 {
                        completion(false, kSessionExpire, false)
                    } else {
                        completion(false, "Some Error Occurred", true)
                    }
                } else if let error = error {
                    completion(false, error.error.localizedDescription, true)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            })
        }
    }
    
    // MARK: Get TVOD Content List Of Active Ones
    func checkForTVODData( _ completion: @escaping () -> Void) {
        tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: 0)
        if let tVODVM = tvodVM {
            tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                if flagValue {
                    self.tvodData = tVODVM.tvodModal?.data
                    completion()
                } else {
                    completion()
                }
            }
            return
        }
        completion()
    }
}

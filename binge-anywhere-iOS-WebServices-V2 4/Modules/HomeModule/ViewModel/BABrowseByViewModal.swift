//
//  BABrowseByViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BABrowseByViewModalProtocol {
    var browseByModal: BABrowseByModal? {get set}
    func getBrowseByData(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BABrowseByViewModal: BABrowseByViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BABrowseByScreenRepo
    var browseByModal: BABrowseByModal?
    var apiParams = APIParams()
    var intent: String?

    init(repo: BABrowseByScreenRepo, endPoint: String, intent: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.intent = intent
    }

    func getBrowseByData(completion: @escaping(Bool, String?, Bool) -> Void) {
        
        apiParams["platform"] = "BINGE"//self.intent
        print("Browse by ----??", apiParams)
        requestToken = repo.getBrowseByData(apiParams: apiParams, completion: { configData, error in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.browseByModal = _configData.parsed
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", false)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

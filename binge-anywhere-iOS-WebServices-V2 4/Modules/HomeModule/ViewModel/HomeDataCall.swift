//
//  HomeDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam on 20/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol HomeScreenDetailRepo {
    var apiEndPoint: String {get set}
    func getHomeTypeScreenData(apiParams: APIParams, completion: @escaping (APIServicResult<HomeScreenDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct HomeScreenRepo: HomeScreenDetailRepo {
    var apiEndPoint: String = ""

    func getHomeTypeScreenData(apiParams: APIParams, completion: @escaping (APIServicResult<HomeScreenDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId, "platform": "BINGE_ANYWHERE", "rule": "BA"]
//        let apiHeader: APIHeaders = ["Content-Type": "application/json", "x-authenticated-userid": BAUserDefaultManager().sId, "authorization": BAUserDefaultManager().userAccessToken, "subscriberid": BAUserDefaultManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAUserDefaultManager().profileId, "rule": "BA"]
        let target = ServiceRequestDetail.init(endpoint: .homeScreenData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<HomeScreenDataModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BATVODViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BATVODViewModalProtocol {
    var tvodModal: BATVODModel? {get set}
    var nextPageAvailable: Bool {get set}
    func getTVODListingData(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BATVODViewModal: BATVODViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BATVODScreenRepo
//    var baId: String = "0"
//    var endPoint: String = ""
    var nextPageAvailable = false
    var layoutTypes = ""
    var pageLimit = 10
    var pageOffset = 0
    var tvodModal: BATVODModel?
    var apiParams = APIParams()

    init(repo: BATVODScreenRepo, endPoint: Int/*, layoutType: String*/) {
        self.repo = repo
        //self.layoutTypes = LayoutType
        self.repo.apiEndPoint = BAKeychainManager().sId//BAConfigManager.shared.configModel?.data?.config?.tvodDetails?.subsciberId ?? "3001180136"//"3001180136"//String(endPoint)
    }

    func getTVODListingData(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["max"] = String(pageLimit)
        apiParams["offset"] = String(pageOffset)
//        apiParams["layoutType"] = layoutTypes
        requestToken = repo.getTVODListingData(apiParams: apiParams, completion: { configData, error in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if self.pageOffset == 0 {
                            self.tvodModal = _configData.parsed
                            self.tvodModal?.data?.items?.removeAll()
                            for tvodData in _configData.parsed.data?.items ?? [] {
                                if tvodData.rentalStatus == "ACTIVE" {
                                    self.tvodModal?.data?.items?.append(tvodData)
                                }
                            }
                        } else {
                            for content in (_configData.parsed.data?.items)! {
                                self.tvodModal?.data?.items?.append(content)
                            }
                        }
                        if let screenData = self.tvodModal?.data {
                            if let watchListData = screenData.items {
                                self.pageOffset = watchListData.count
                                self.nextPageAvailable = watchListData.count < (screenData.total ?? 0)
                            }
                        }
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

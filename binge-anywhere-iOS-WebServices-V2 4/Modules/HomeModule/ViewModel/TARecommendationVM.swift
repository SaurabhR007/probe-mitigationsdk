//
//  TARecommendationVM.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class TARecommendationVM {
    var repo = TARecommendationData()
    var appPageName = ""
    var isAppPageTaCall = false
    var apiParams = APIParams()
    var requestToken = [ServiceCancellable?]()
    private var cachedTaIDs = [Int]()
    var errorCode: Int?
    
    func organiseTAApiRequestHit(_ homeScreenSectionData: [HomeScreenDataItems], _ offset: Int, _ pageType: String, completion: @escaping([HomeScreenDataItems]?) -> Void) {
        if offset == 0 {
            cachedTaIDs = []
            // fetch herobanner first then recommendation data
            fetchHeroBannerRecommendationData(homeScreenSectionData, pageType) {
                self.fetchTARecommendationRailData(homeScreenSectionData) { (data) in
                    completion(data)
                }
            }
        } else {
            // fetch recommendation data
            fetchTARecommendationRailData(homeScreenSectionData) { (data) in
                completion(data)
            }
        }
    }
    
    func fetchTARecommendationRailData( _ data: [HomeScreenDataItems], completion: @escaping([HomeScreenDataItems]) -> Void) {
        let filteredData = data.filter { (homeScreenData) -> Bool in
            return homeScreenData.sectionSource == TAConstant.recommendationType.rawValue && !cachedTaIDs.contains(homeScreenData.id ?? 0)
            // Line commented to support mixed ta rail logic
//            return homeScreenData.contentList?.isEmpty ?? false && homeScreenData.sectionSource == TAConstant.recommendationType.rawValue && !cachedTaIDs.contains(homeScreenData.id ?? 0)
        }
        
        guard !filteredData.isEmpty else {
            completion(data)
            return
        }
        
        apiParams.removeAll()
        let dispatchGroup = DispatchGroup()
        for i in 0..<filteredData.count {
            dispatchGroup.enter()
            let useCaseID = filteredData[i].placeHolder ?? ""
            let cachedID = filteredData[i].id ?? -1
            cachedTaIDs.append(cachedID)
            apiParams["max"] = "20" // As per subhro message on binge hangout on 13 March adding this
            apiParams["layout"] = filteredData[i].layoutType
            var reqToken: ServiceCancellable?
            repo.appPageName = appPageName
            repo.isAppPageTaCall = isAppPageTaCall
            reqToken = repo.getTARecommendationData(apiParams, useCaseID) {[weak self] (apiData, _) in
                if let weakSelf = self {
                    if let _configData = apiData {
                        if let statusCode = _configData.parsed.code {
                            if statusCode == 0 {
                                let matchedData = data.filter({ (data) -> Bool in
                                    return data.title == filteredData[i].title
                                })
                                if !matchedData.isEmpty {
                                    if let genre = _configData.parsed.data?.genreType, var title = matchedData[0].title {
                                        if genre.count > 0 {
                                            matchedData[0].title = weakSelf.updateGenreTypeSectionTitle(genre, &title)
                                        }
                                    }
                                    if let genre = _configData.parsed.data?.languageType, var title = matchedData[0].title {
                                        if genre.count > 0 {
                                            matchedData[0].title = weakSelf.updateLanguageSectionTitle(genre, &title)
                                        }
                                    }
                                    
                                    if let taData = _configData.parsed.data?.contentList {
                                        print("this is tadata count \(taData.count)")
                                        if weakSelf.checkToUpdateMixedRailOrNot(taData) {
                                            let taRailsData = matchedData[0].contentList
                                            print("this is ta rail data count \(taRailsData?.count ?? -1)")
                                            if matchedData[0].recommendationPosition == TAConstant.prepend.rawValue {
                                                matchedData[0].contentList = taRailsData! + taData
                                            } else if matchedData[0].recommendationPosition == TAConstant.append.rawValue {
                                                matchedData[0].contentList = taData + taRailsData!
                                            } else {
                                                matchedData[0].contentList = taData
                                            }
                                            if var data = matchedData[0].contentList {
                                                print("this is data count \(data.count)")
                                                matchedData[0].contentList = UtilityFunction.shared.filterTAData(data)
                                                matchedData[0].contentList = UtilityFunction.shared.removeDuplicatesArrayObject(&data)
                                                print("this is data count \(matchedData[0].contentList?.count ?? -2)")
                                            }
                                        } else {
                                            if !(matchedData[0].contentList?.isEmpty ?? false) {
                                                matchedData[0].contentList?.removeAll()
                                            }
                                        }
                                    } else {
                                        if !(matchedData[0].contentList?.isEmpty ?? false) {
                                            matchedData[0].contentList?.removeAll()
                                        }
                                    }
                                }
                            }
                        } 
                        dispatchGroup.leave()
                    } else {
                        dispatchGroup.leave()
                    }
                } else {
                    dispatchGroup.leave()
                }
            }
            requestToken.append(reqToken)
        }
        dispatchGroup.notify(queue: .main) {
            completion(data)
        }
    }
    
    
    func checkToUpdateMixedRailOrNot(_ arr: [BAContentListModel]) -> Bool {
        return !(arr.count == 0)
    }
    
    func fetchTARecommendationSeeAllRailData( _ placeHolder: String, _ railTitle: String, id: String, completion: @escaping([BAContentListModel]) -> Void) {
        apiParams.removeAll()
        let useCaseID = placeHolder
        apiParams["layout"] = "LANDSCAPE"
        var reqToken: ServiceCancellable?
        reqToken = repo.getTARecommendationData(apiParams, useCaseID, completion: {(apiData, _) in
                if let _configData = apiData {
                    if let statusCode = _configData.parsed.code {
                        if statusCode == 0 {
                            if let data = apiData?.parsed.data?.contentList {
                                let filteredData = UtilityFunction.shared.filterTAData(data)
                                completion(filteredData)
                            }
                        }
                    }
                }
        })
        requestToken.append(reqToken)
    }
    
    
    
    func fetchHeroBannerRecommendationData( _ data: [HomeScreenDataItems], _ homeType: String, completion:@escaping () -> Void) {
        let taHeroBannerArray = BAConfigManager.shared.configModel?.data?.config?.taHeroBanner
        if let taHeroBannerArray = taHeroBannerArray {
            
            let index = taHeroBannerArray.firstIndex { (data) -> Bool in
                return (data.count ?? 0) > 0 && data.pageType == homeType
            }
            
            guard let taArrayindex = index else {
                completion()
                return
            }
            apiParams.removeAll()
            
            var reqToken: ServiceCancellable?
            let selectedArr = taHeroBannerArray[taArrayindex]
            let useCaseId = selectedArr.placeHolder ?? ""
            apiParams["max"] = String(selectedArr.count ?? 0)
            apiParams["layout"] = "LANDSCAPE"
            repo.appPageName = appPageName
            repo.isAppPageTaCall = isAppPageTaCall
            
            reqToken = repo.getTAHeroBannerRecommendationData(apiParams, useCaseId, homeType, completion: {[weak self] (apiData, _) in
                if  self != nil {
                    if let _configData = apiData {
                        if let statusCode = _configData.parsed.code {
                            if statusCode == 0 {
                                let matchedData = data.filter({ (data) -> Bool in
                                    return data.sectionType == RailType.hero.rawValue
                                })
                                if !matchedData.isEmpty {
                                    if selectedArr.position == TAConstant.append.rawValue {
                                        matchedData[0].contentList?.append(contentsOf: (_configData.parsed.data?.contentList)!)
                                    } else {
                                        let heroBannerArray = matchedData[0].contentList
                                        matchedData[0].contentList = (_configData.parsed.data?.contentList)! + heroBannerArray!
                                    }
                                    if let data = matchedData[0].contentList {
                                        matchedData[0].contentList = UtilityFunction.shared.filterTAData(data)
                                    }
                                }
                            }
                        }
                    }
                }
                completion()
            })
            requestToken.append(reqToken)
        } else {
            completion()
        }
    }
    
    func fetchDetailScreenRecommendedData(postQueryParams: APIParams, completion: @escaping(TARecommendationDataModel?, Bool) -> Void) {
        var reqToken: ServiceCancellable?
        let useCaseId = appendRelatedRailUseCases(postQueryParams["contentType"] as! String)
        reqToken = repo.getTARecommendationData(postQueryParams, useCaseId, completion: { (apiData, _) in
            if let _configData = apiData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        completion(_configData.parsed, false)
                    } else {
                        completion(nil, true)
                    }
                } else {
                    self.errorCode = _configData.serviceResponse.statusCode
                    completion(nil, true)
                }
            }
        })
        requestToken.append(reqToken)
    }
    
    func appendRelatedRailUseCases(_ contentType: String) -> String {
        let taRelatedRailArray = BAConfigManager.shared.configModel?.data?.config?.taRelatedRail
        var useCaseId = ""
        if let array = taRelatedRailArray {
            if !array.isEmpty {
                let filteredValue = array.filter { (data) -> Bool in
                    return data.contentType == contentType
                }
                if !filteredValue.isEmpty {
                    useCaseId = filteredValue[0].useCase ?? "COMMON_RELATED_UC_MORE_LIKE_MOVIE"
                }
            }
        }
        return useCaseId
    }
    
    
    func updateGenreTypeSectionTitle(_ genreStr: String,_ titleStr: inout String) -> String {
        if let genreRange = titleStr.range(of: ContentType.genre1.rawValue, options: .caseInsensitive) {
            titleStr = titleStr.replacingCharacters(in: genreRange, with: genreStr)
        }
        if let genreRange = titleStr.range(of: ContentType.genre2.rawValue, options: .caseInsensitive) {
            titleStr = titleStr.replacingCharacters(in: genreRange, with: genreStr)
        }
        return titleStr
    }
    
    func updateLanguageSectionTitle(_ languageStr: String,_ titleStr: inout String) -> String {
        if let languageRange = titleStr.range(of: ContentType.language1.rawValue, options: .caseInsensitive) {
            titleStr = titleStr.replacingCharacters(in: languageRange, with: languageStr)
        }
        if let languageRange = titleStr.range(of: ContentType.language2.rawValue, options: .caseInsensitive) {
            titleStr = titleStr.replacingCharacters(in: languageRange, with: languageStr)
        }
        return titleStr
    }
}

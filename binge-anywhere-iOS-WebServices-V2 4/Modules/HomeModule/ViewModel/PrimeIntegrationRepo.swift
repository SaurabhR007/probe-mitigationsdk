//
//  PrimeIntegrationRepo.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol PrimeIntegrationApiRepo {
    var apiEndPoint: String {get set}
    func requestToResumePrimeSubscription(completion: @escaping (APIServicResult<PrimeResumeAPIResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}


struct PrimeResponseRepo: PrimeIntegrationApiRepo {
    var apiEndPoint: String = ""
    func requestToResumePrimeSubscription(completion: @escaping (APIServicResult<PrimeResumeAPIResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "subscriberid": BAKeychainManager().sId, "authorization": BAKeychainManager().userAccessToken]
        let target = ServiceRequestDetail.init(endpoint: .primeResumeService(headers: apiHeader))
        return APIService().request(target) { (response: APIResult<APIServicResult<PrimeResumeAPIResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

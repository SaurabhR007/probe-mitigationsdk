//
//  PrimeIntegrationVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol PrimeIntegrationVM {
    func primeResumeService(_ completion: @escaping (_ success: Bool) -> Void)
}

class PrimeIntegrationViewModal: PrimeIntegrationVM {
    var requestToken: ServiceCancellable?
    var repo: PrimeResponseRepo
    
    init(repo: PrimeResponseRepo) {
        self.repo = repo
    }
    
    func primeResumeService(_ completion: @escaping (_ success: Bool) -> Void) {
        if requestToken != nil {return}
        requestToken = repo.requestToResumePrimeSubscription(completion: { (data, _) in
            if let _data = data {
                if let statusCode = _data.parsed.code {
                    if statusCode == 0 {
                        completion(true)
                        self.requestToken = nil
                    } else {
                        completion(false)
                        self.requestToken = nil
                    }
                }
            } else {
                completion(false)
                self.requestToken = nil
            }
        })
    }
}

//
//  PageTypeVM.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

class PageTypeVM: HomeVMProtocol {    
    var repo: HomeScreenDetailRepo
    var nextPageAvailable = true
    var appPageName: String = ""
    var pageLimit = 10
    var pageType: String
    var homeScreenData: HomeScreenDataModel?
    var continueWatchingVM: BAContinueWatchingViewModal?
    var tvodVM: BATVODViewModal?
    var tvodData: TVODData?
    var taRecommendationVM = TARecommendationVM()
    var pageOffset = 0
    var requestToken: ServiceCancellable?
    var endPoint: String = ""
    var subscribed = false
    var apiParams = APIParams()
    init(repo: HomeScreenDetailRepo, endPoint: String, pageType: String, isSubscribed: Bool, appPageName: String) {
        self.repo = repo
        self.pageType = pageType
        self.appPageName = appPageName
        self.taRecommendationVM.appPageName = appPageName
        self.taRecommendationVM.isAppPageTaCall = true
        self.repo.apiEndPoint = endPoint
        self.subscribed = isSubscribed
    }

    func getHomeScreenData(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["pageLimit"] = String(pageLimit)
        apiParams["pageOffset"] = String(pageOffset)
        if subscribed {
            apiParams["Subscribed"] = "true"
            apiParams["UnSubscribed"] = "false"
        } else {
            apiParams["Subscribed"] = "false"
            apiParams["UnSubscribed"] = "true"
        }
        requestToken = repo.getHomeTypeScreenData(apiParams: apiParams, completion: { (configData, error) in
            if let _configData = configData {
                self.requestToken = nil
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        let count = _configData.parsed.data?.items?.count ?? 0
                        if self.pageOffset == 0 {
                            self.homeScreenData = _configData.parsed
                        } else {
                            self.homeScreenData?.data?.items?.append(contentsOf: (_configData.parsed.data?.items)!)
                        }
                        if let homeData = self.homeScreenData?.data {
                            if let _ = homeData.items {
                                
                                self.nextPageAvailable = (self.pageOffset) < (homeData.total ?? 0)
                                self.checkForTVODData {
                                    let group = DispatchGroup()
                                    group.enter()
                                    self.checkForContinueWatchData(group: group, {
                                        group.leave()
                                    })
                                    group.enter()
                                    self.makeTARecommendationCalls(group: group, {
                                        group.leave()
                                    })
                                    group.notify(queue: .main) {
                                        self.pageOffset += count
                                        self.omitBlankContentSectionRail()
                                        completion(true, nil, false)
                                    }
                                }
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    } else {
                        self.requestToken = nil
                        completion(false, "Some Error Occurred", false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else if let error = error {
                    self.requestToken = nil
                    completion(false, error.error.localizedDescription, true)
                } else {
                    self.requestToken = nil
                    completion(false, "Some Error Occurred", true)
                }
            }
        })
    }
    
    func checkForContinueWatchData(group: DispatchGroup, _ completion: @escaping () -> Void) {
        self.checkForTVODData {
            if let data = self.homeScreenData?.data?.items {
                for (index, item) in data.enumerated() where item.sectionSource == "CONTINUE_WATCHING" {
                    self.continueWatchingVM = BAContinueWatchingViewModal(repo: BAContinueWatchingRepo(), endPoint: "watched", baId: BAKeychainManager().baId)
                    if let cwVM = self.continueWatchingVM {
                        cwVM.getContinueWatchingtData { (flagValue, _, isApiError) in
                            if flagValue {
                                let index = (item.position ?? index) <= index ? (item.position ?? index) : index
                                let cwList = cwVM.watchListData?.data?.contentList
                                cwVM.watchListData?.data?.contentList?.removeAll()
                                for cwData in cwList ?? [] {
                                    if cwData.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == cwData.title}) ?? true {
                                                cwVM.watchListData?.data?.contentList?.append(cwData)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        cwVM.watchListData?.data?.contentList?.append(cwData)
                                    }
                                }
                                if let data = cwVM.watchListData?.data?.contentList {
                                    self.homeScreenData?.data?.items?[index].contentList = data
                                } else {
                                    self.homeScreenData?.data?.items?.remove(at: index)
                                }
                                completion()
                            } else {
                                let array = self.homeScreenData?.data?.items?.indices.contains(index) ?? false
                                if array {
                                    self.homeScreenData?.data?.items?.remove(at: index)
                                }
                                completion()
                            }
                        }
                        return
                    }
                    break
                }
                completion()
            } else {
                completion()
            }
        }
    }
    
    
    func filterContentList() {
        if let item = self.homeScreenData?.data?.items {
            for i in 0..<item.count {
                if let item = self.homeScreenData?.data?.items?[i].contentList {
                    if self.homeScreenData?.data?.items?[i].sectionSource?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item, false)
                    } else if self.homeScreenData?.data?.items?[i].sectionSource?.lowercased() == SectionSourceType.continueWatching.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item, false)
                    } else if self.homeScreenData?.data?.items?[i].sectionType?.lowercased() == SectionSourceType.heroBanner.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item, false)
                    } else if self.homeScreenData?.data?.items?[i].sectionSource?.lowercased() != SectionSourceType.tvod.rawValue.lowercased() {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.removeRentalData(item)
                    } else {
                        self.homeScreenData?.data?.items?[i].contentList =  UtilityFunction.shared.filterTAData(item)
                    }
                }
            }
        }
    }
    
    func checkForTVODData( _ completion: @escaping () -> Void) {
        if let data = homeScreenData?.data?.items {
            for (index, item) in data.enumerated() where item.sectionSource == "TVOD" {
                tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: item.id ?? 0)
                if let tVODVM = tvodVM {
                    tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                        if flagValue {
                            let index = (item.position ?? index) <= index ? (item.position ?? index) : index
                            let array = self.homeScreenData?.data?.items?.indices.contains(index) ?? false
                            if array {
                                if let data = tVODVM.tvodModal?.data?.items {
                                    self.homeScreenData?.data?.items?[index].contentList = data
                                } else {
                                    self.homeScreenData?.data?.items?.remove(at: index)
                                }
                            }
                            completion()
                        } else {
                            self.homeScreenData?.data?.items?.remove(at: index)
                            completion()
                        }
                    }
                    return
                }
                break
            }
            completion()
        } else {
            completion()
        }
    }
    
    func omitBlankContentSectionRail() {
        self.filterContentList()
        self.homeScreenData?.data?.items = self.homeScreenData?.data?.items?.filter({ (data) -> Bool in
            return !(data.contentList?.isEmpty ?? true)
        })
    }
    
    func makeTARecommendationCalls(group: DispatchGroup,_ completion: @escaping () -> Void) {
        if let homeScreenData = homeScreenData?.data?.items {
            taRecommendationVM.organiseTAApiRequestHit(homeScreenData, pageOffset, self.pageType, completion: { (_) in
                completion()
            })
        } else {
            completion()
        }
    }
}




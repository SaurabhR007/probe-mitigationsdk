//
//  BAContinueWatchingRepo.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 28/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAContinueWatchingScreenRepo {
    var apiEndPoint: String {get set}
    func getWatchListData(apiParams: APIParams, completion: @escaping(APIServicResult<BAContinueWatchingModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAContinueWatchingRepo: BAContinueWatchingScreenRepo {
    var apiEndPoint: String = ""
    fileprivate func extractedFunc() -> APIHeaders {
        return ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
    }
    
    func getWatchListData(apiParams: APIParams, completion: @escaping(APIServicResult<BAContinueWatchingModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        //let apiParams = APIParams()
        // TODO: Remove once kong is setup
        let apiHeader: APIHeaders = extractedFunc()
        let target = ServiceRequestDetail.init(endpoint: .continueWatchData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAContinueWatchingModal>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

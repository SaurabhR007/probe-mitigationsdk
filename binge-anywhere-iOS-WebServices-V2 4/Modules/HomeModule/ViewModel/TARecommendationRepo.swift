//
//  TARecommendationRepo.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol TARecommendationRepo {
    func getTARecommendationData(_ postQueryParams: APIParams, _ useCaseId: String, completion: @escaping(APIServicResult<TARecommendationDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    
    func getTAHeroBannerRecommendationData(_ postQueryParams: APIParams, _ useCaseId: String, _ pageType: String, completion: @escaping(APIServicResult<TARecommendationDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    var appPageName: String {get set}
    var isAppPageTaCall: Bool {get set}
}

struct TARecommendationData: TARecommendationRepo {
    var appPageName: String = ""
    var isAppPageTaCall: Bool = false
    
    func getTARecommendationData(_ postQueryParams: APIParams, _ useCaseId: String, completion: @escaping(APIServicResult<TARecommendationDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
        let SID = BAKeychainManager().sId
        let PID = BAKeychainManager().profileId
        
        var apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "\(accessToken)", "profileId": "\(PID)", "platform": "binge_anywhere_ios", "subscriberid": "\(SID)"]
        print("TA call for app pages is \(isAppPageTaCall)")
        print("TA call for app pages for app name is \(appPageName)")
        if isAppPageTaCall {
            apiHeader["subPage"] = appPageName
        }
        
        let target = ServiceRequestDetail.init(endpoint: .taRecommendationData(usecaseid: useCaseId, postQueryParams: postQueryParams, header: apiHeader ))
        return APIService().request(target) { (response: APIResult<APIServicResult<TARecommendationDataModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
    
    
    func getTAHeroBannerRecommendationData(_ postQueryParams: APIParams, _ useCaseId: String,  _ pageType: String, completion: @escaping(APIServicResult<TARecommendationDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
        let SID = BAKeychainManager().sId
        let PID = BAKeychainManager().profileId
        
        var apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "\(accessToken)", "profileId": "\(PID)", "platform": "binge_anywhere_ios", "subscriberid": "\(SID)", "pageType": "\(pageType)"]
        print("TA call for app hero banner is \(isAppPageTaCall)")
        print("TA call for app hero banner app name is \(appPageName)")
        if isAppPageTaCall {
            apiHeader["subPage"] = appPageName
        }
        
        let target = ServiceRequestDetail.init(endpoint: .taRecommendationData(usecaseid: useCaseId, postQueryParams: postQueryParams, header: apiHeader ))
        return APIService().request(target) { (response: APIResult<APIServicResult<TARecommendationDataModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

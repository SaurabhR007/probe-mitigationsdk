//
//  BATVODRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BATVODScreenRepo {
    var apiEndPoint: String {get set}
    func getTVODListingData(apiParams: APIParams, completion: @escaping(APIServicResult<BATVODModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BATVODRepo: BATVODScreenRepo {
    var apiEndPoint: String = ""
    func getTVODListingData(apiParams: APIParams, completion: @escaping(APIServicResult<BATVODModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        //let apiParams = APIParams()
        // TODO: Remove once kong is setup
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "bearer \(BAKeychainManager().userAccessToken)"/*BAConfigManager.shared.configModel?.data?.config?.tvodDetails?.token ?? "bearer 0d9dfb23a2ae4888af75fd37724240e0"*/]
        //let apiHeader: APIHeaders = ["authorization": "bearer 0d9dfb23a2ae4888af75fd37724240e0"]
        let target = ServiceRequestDetail.init(endpoint: .tvodListing(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BATVODModel>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

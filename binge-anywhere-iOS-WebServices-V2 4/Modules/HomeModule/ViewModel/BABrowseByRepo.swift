//
//  BABrowseByRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BABrowseByScreenRepo {
    var apiEndPoint: String {get set}
    func getBrowseByData(apiParams: APIParams, completion: @escaping(APIServicResult<BABrowseByModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BABrowseByRepo: BABrowseByScreenRepo {
    var apiEndPoint: String = ""
    func getBrowseByData(apiParams: APIParams, completion: @escaping(APIServicResult<BABrowseByModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        // TODO: Remove once kong is setup
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId , "platform": "BINGE_ANYWHERE"]
        //let apiHeader: APIHeaders = ["authorization": "bearer 0d9dfb23a2ae4888af75fd37724240e0"]
        let target = ServiceRequestDetail.init(endpoint: .browseBy(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BABrowseByModal>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

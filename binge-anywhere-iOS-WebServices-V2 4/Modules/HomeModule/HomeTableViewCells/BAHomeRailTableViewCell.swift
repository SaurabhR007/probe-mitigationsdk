//
//  TableViewCell.swift
//  HomeVIewWithBanner
//
//  Created by Shivam on 05/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import Kingfisher


protocol TableViewCellDelegate: class, PrimeAppDelegateMethod {
    func moveToDetailScreen(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, timestamp: Int?, pageSource: String?, configSource: String?, railName: String?)
    func moveToSeeAllScreen(_ data: HomeScreenDataItems?, _ index: Int)
    func moveToDetailForPageType(pageType: String?, appname: String?, isSubscribed: Bool?)
    func moveToSeeAllScreenFromVOD(_ data: RecommendedContentData?)
    func moveToBrowseByDetail(intent: String?, genre: String?, language: String?, pageSource: String?, configSource: String?, railName: String?)
}


class BAHomeRailTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var seeAllButton: CustomButton!
    @IBOutlet weak var relatedRailImageView: UIImageView!
    @IBOutlet weak var railHeaderDividerForBrowseBy: UIImageView!
    @IBOutlet weak var providerLogo: UIImageView!
    @IBOutlet weak var providerLogoWidth: NSLayoutConstraint!
    @IBOutlet weak var providerLogoLeading: NSLayoutConstraint!
    @IBOutlet weak var railtTitletrailingConstraint: NSLayoutConstraint!
    weak var delegate: TableViewCellDelegate?
    var railCollectionCellsData: HomeScreenDataItems?
    var recommendedRailCellData: RecommendedContentData?
    var partnerSubscription: PartnerSubscriptionDetails?
    var subscriberPartners = [String]()
    var isPartnerHomePage: Bool?
    var isViewReloaded: Bool?
    var isSelectionMade = false
    var pageType = ""
    var pageName = ""
    var pageSource = ""
    var configSource = ""
    var railName = ""
    var railPosition = 0
    //var didSelectTime = Date()
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.isUserInteractionEnabled = true
        configureUI()
        //handlePageType()
        registerCells()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        titleLabel.isHidden = true
        seeAllButton.isHidden = true
        relatedRailImageView.isHidden = true
    }
    
    private func registerCells() {
        collectionView.register(UINib(nibName: kBAHomeRailCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeRailCollectionViewCell)
        collectionView.register(UINib(nibName: kBACircularCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBACircularCollectionViewCell)
         collectionView.register(UINib(nibName: "FilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")
        collectionView.register(UINib(nibName: "BABrowseByHomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BABrowseByHomeCollectionViewCell")
    }
    
    class func createCell() -> BAHomeRailTableViewCell {
        let nib = UINib(nibName: kBAHomeRailTableViewCell, bundle: nil)
        let cell = (nib.instantiate(withOwner: self, options: nil).last as? BAHomeRailTableViewCell)!
        return cell
    }
    
    func handlePageType() {
        switch pageType {
        case PageType.home.rawValue:
            pageType = "Home"
        case PageType.kids.rawValue:
            pageType = "Kids"
        case PageType.tvShows.rawValue:
            pageType = "TV Shows"
        case PageType.movies.rawValue:
            pageType = "Movies"
        default:
            pageType = "Home"
        }
    }
    
    func configureCell(_ item: HomeScreenDataItems, scrollToFirst: Bool = false) {
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 18 * screenScaleFactorForWidth), color: .BARailTitleColor)
        providerLogoWidth.constant = 0
        providerLogoLeading.constant = 0
        if item.sectionSource == "LANGUAGE" || item.sectionSource == "GENRE" {
            seeAllButton.isHidden = true
            railHeaderDividerForBrowseBy.isHidden = false
            relatedRailImageView.isHidden = true
        } else if (item.sectionSource == SectionSourceType.providerSpecificRail.rawValue) {
            seeAllButton.isHidden = true
            providerLogoWidth.constant = 30
            railtTitletrailingConstraint.constant = 30
            providerLogoLeading.constant = 20
            configureProviderLogo(provider: item.provider ?? "")
            railHeaderDividerForBrowseBy.isHidden = false
            relatedRailImageView.isHidden = true
        }
        /*else if ( item.sectionSource?.lowercased() == SectionSourceType.prime.rawValue.lowercased()) {
            seeAllButton.isHidden = true
            railtTitletrailingConstraint.constant = 30
            railHeaderDividerForBrowseBy.isHidden = false
            relatedRailImageView.isHidden = true
        } line commented as now we are showing the see all button for prime rail also*/
        else {
            seeAllButton.isHidden = false
            railtTitletrailingConstraint.constant = 104*screenScaleFactorForWidth
            railHeaderDividerForBrowseBy.isHidden = true
            relatedRailImageView.isHidden = false
        }
        partnerSubscriptionArray()
        seeAllButton.setTitle("See all", for: [])
        seeAllButton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BABlueColor)
        titleLabel.text = item.title
        railCollectionCellsData = item
        titleLabel.text = item.title
        titleLabel.isHidden = false
        relatedRailImageView.isHidden = false
        //seeAllButton.isHidden = false
        
        if item.sectionSource == "CONTINUE_WATCHING" && scrollToFirst {
            collectionView.setContentOffset(.zero, animated: false)
        }
       // if item.sectionSource == "CONTINUE_WATCHING" && item.contentList
        collectionView.reloadData()
        collectionView.contentOffset = CGPoint(x: item.lastScrolledOffset, y: 0.0)
    }
    
    
    func configureProviderLogo(provider: String) {
        providerLogo.layer.borderWidth = 1.0
        providerLogo.layer.masksToBounds = false
        providerLogo.layer.borderColor = UIColor.clear.cgColor
        providerLogo.layer.cornerRadius = providerLogoWidth.constant / 2
        providerLogo.clipsToBounds = true
        providerLogo.backgroundColor = .white
        var url = URL.init(string: "")
        switch provider.uppercased() {
            case ProviderType.prime.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoCircular ?? "")
            case ProviderType.sunnext.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoCircular ?? "")
            case ProviderType.shemaro.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoCircular ?? "")
            case ProviderType.vootKids.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoCircular ?? "")
            case ProviderType.vootSelect.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoCircular ?? "")
            case ProviderType.hungama.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoCircular ?? "")
            case ProviderType.zee.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoCircular ?? "")
            case ProviderType.hotstar.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoCircular ?? "")
            case ProviderType.eros.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoCircular ?? "")
            case ProviderType.sonyLiv.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoCircular ?? "")
            case ProviderType.curosityStream.rawValue:
                url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoCircular ?? "")
            case ProviderType.hopster.rawValue: providerLogo.image = UIImage(named: "hopster")
            case ProviderType.netflix.rawValue: providerLogo.image = UIImage(named: "netflix")
            default: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoCircular ?? "")
        }
        setImage(with: providerLogo, url)
    }
    
    private func setImage(with imageView: UIImageView, _ url: URL?) {
        if let _url = url {
            imageView.alpha = (imageView.image == nil) ? 0 : 1
            imageView.kf.setImage(with: _url, completionHandler:  { result in
                UIView.animate(withDuration: 1, animations: { imageView.alpha = 1 })
            })
        } else {
            imageView.image = nil
        }
    }
    
    func partnerSubscriptionArray() {
        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails != nil {
           partnerSubscription = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
        }
    }
    
    func configureCellWithRecommendedData(recommendedContent: RecommendedContentData) {
        if isViewReloaded ?? false {
            // Do Nothing
        } else {
            titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 18 * screenScaleFactorForWidth), color: .BARailTitleColor)
            seeAllButton.setTitle("See all", for: [])
            seeAllButton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BABlueColor)
            titleLabel.text = recommendedContent.title
            recommendedRailCellData = recommendedContent
            collectionView.reloadData {
                self.relatedRailImageView.isHidden = false
                self.seeAllButton.isHidden = false
                self.titleLabel.isHidden = false
            }
        }
    }
    
    @IBAction func seeAllBtnTapped(_ sender: CustomButton) {
        if let del = delegate {
            if railCollectionCellsData != nil {
                del.moveToSeeAllScreen(railCollectionCellsData, sender.tag)
            } else if recommendedRailCellData != nil {
                del.moveToSeeAllScreenFromVOD(recommendedRailCellData)
            }
        }
    }
}

// MARK: - Extention
extension BAHomeRailTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if railCollectionCellsData != nil {
            count = railCollectionCellsData?.contentList?.count ?? 0
        } else if recommendedRailCellData != nil {
            count = recommendedRailCellData?.contentList?.count ?? 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if railCollectionCellsData?.layoutType == LayoutType.landscape.rawValue || recommendedRailCellData?.layoutType == LayoutType.landscape.rawValue {
            if railCollectionCellsData?.sectionSource == "PROVIDER" {
                return CGSize(width: 72, height: 72)
            } else if railCollectionCellsData?.sectionSource == "LANGUAGE" || railCollectionCellsData?.sectionSource == "GENRE" {
                return CGSize(width: 137, height: 78)
            } else {
                return CGSize(width: 184, height: 167)
            }
        } else if railCollectionCellsData?.layoutType == LayoutType.potrait.rawValue || recommendedRailCellData?.layoutType == LayoutType.potrait.rawValue {
            return CGSize(width: 125, height: 246)
        } else {
            if railCollectionCellsData?.sectionSource == "LANGUAGE" || railCollectionCellsData?.sectionSource == "GENRE" {
                return CGSize(width: 75, height: 95)
            } else {
                return CGSize(width: 72, height: 72)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        if railCollectionCellsData?.layoutType == LayoutType.landscape.rawValue || recommendedRailCellData?.layoutType == LayoutType.landscape.rawValue {
            if recommendedRailCellData != nil {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
                cell.configureCell(recommendedRailCellData?.contentList?[indexPath.row] ?? nil, recommendedRailCellData?.layoutType ?? "")
                return cell
            } else {
                if railCollectionCellsData?.sectionSource == "LANGUAGE" {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BABrowseByHomeCollectionViewCell", for: indexPath) as! BABrowseByHomeCollectionViewCell
                    cell.addGradient()
                    cell.configureCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, intent: "LANGUAGE")
                    return cell
                } else if railCollectionCellsData?.sectionSource == "GENRE" {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BABrowseByHomeCollectionViewCell", for: indexPath) as! BABrowseByHomeCollectionViewCell
                    cell.addGradient()
                    cell.configureCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, intent: "GENRE")
                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
                    cell.configureCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, railCollectionCellsData?.layoutType ?? "")
                    return cell
                }
            }
        } else if railCollectionCellsData?.layoutType == LayoutType.potrait.rawValue || recommendedRailCellData?.layoutType == LayoutType.potrait.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
            if recommendedRailCellData != nil {
                cell.configureCell(recommendedRailCellData?.contentList?[indexPath.row] ?? nil, recommendedRailCellData?.layoutType ?? "")
            } else {
                cell.configureCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, railCollectionCellsData?.layoutType ?? "")
            }
            return cell
        } else  if railCollectionCellsData?.layoutType == LayoutType.circular.rawValue || recommendedRailCellData?.layoutType == LayoutType.circular.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBACircularCollectionViewCell, for: indexPath) as! BACircularCollectionViewCell
            if railCollectionCellsData?.sectionSource == "PROVIDER" {
				partnerSubscriptionArray()
				if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.count ?? 0 > 0 {
                    cell.configureCircularCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, subscribedPartners: partnerSubscription)
                } else {
                    cell.configureCircularCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, subscribedPartners: partnerSubscription)
                }
            } else if railCollectionCellsData?.sectionSource == "LANGUAGE" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
                cell.isExclusiveTouch = true
                 cell.configurCell(content: railCollectionCellsData?.contentList?[indexPath.row] ?? nil, intent: "LANGUAGE")
                 return cell
            } else if railCollectionCellsData?.sectionSource == "GENRE" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
                 cell.configurCell(content: railCollectionCellsData?.contentList?[indexPath.row] ?? nil, intent: "GENRE")
                 return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBACircularCollectionViewCell, for: indexPath) as! BACircularCollectionViewCell
                cell.configureCircularCellForContent(railCollectionCellsData?.contentList?[indexPath.row] ?? nil)
                cell.subscribedImageView.isHidden = true
                return cell
            }
           return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBACircularCollectionViewCell, for: indexPath) as! BACircularCollectionViewCell
			cell.configureCircularCell(railCollectionCellsData?.contentList?[indexPath.row] ?? nil, subscribedPartners: nil)
            cell.subscribedImageView.isHidden = true
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.isUserInteractionEnabled = false
        self.isSelectionMade = true
        if self.recommendedRailCellData != nil {
            self.recommendedMixPanelEvents(indexPath)
            self.delegate?.moveToDetailScreen(railType: "", layoutType: "", vodId: self.recommendedRailCellData?.contentList?[indexPath.row].id, contentType: self.recommendedRailCellData?.contentList?[indexPath.row].contentType, timestamp: self.railCollectionCellsData?.contentList?[indexPath.row].rentalExpiry, pageSource: pageSource, configSource: configSource, railName: railName)
        } else {
            if self.railCollectionCellsData?.contentList?[indexPath.row].pageType?.isEmpty == false {
                editorialMixpanelViewAndHomeClickEvent(indexPath)
               // isPartnerHomePage = true
                if self.partnerSubscription?.providers?.count ?? 0 > 0 {
                    if let providers = self.partnerSubscription?.providers {
                        for partner in providers {
                            let partnerName = (partner.name ?? "").lowercased()
                            self.subscriberPartners.append(partnerName)
                        }
                    }
                } else {
                    self.subscriberPartners = []
                }
                let partnerName = (self.railCollectionCellsData?.contentList?[indexPath.row].title ?? "").lowercased()
                var providerAppName = ""
                if self.subscriberPartners.contains("sun_nxt") {
                    providerAppName = "sunnxt"
                }
                if (self.subscriberPartners.contains(partnerName) || providerAppName == partnerName) && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status != "DEACTIVE" {
                    self.delegate?.moveToDetailForPageType(pageType: self.railCollectionCellsData?.contentList?[indexPath.row].pageType, appname: self.railCollectionCellsData?.contentList?[indexPath.row].title, isSubscribed: true)
                } else {
                    self.delegate?.moveToDetailForPageType(pageType: self.railCollectionCellsData?.contentList?[indexPath.row].pageType, appname: self.railCollectionCellsData?.contentList?[indexPath.row].title, isSubscribed: false)
                }
            } else if self.railCollectionCellsData?.sectionSource == "GENRE" {
                editorialMixpanelViewAndHomeClickEvent(indexPath)
                self.delegate?.moveToBrowseByDetail(intent: "GENRE", genre: self.railCollectionCellsData?.contentList?[indexPath.row].title, language: "", pageSource: pageSource, configSource: configSource, railName: railName)
            } else if self.railCollectionCellsData?.sectionSource == "LANGUAGE" {
                editorialMixpanelViewAndHomeClickEvent(indexPath)
                self.delegate?.moveToBrowseByDetail(intent: "LANGUAGE", genre: "", language: self.railCollectionCellsData?.contentList?[indexPath.row].title, pageSource: pageSource, configSource: configSource, railName: railName)
            } else if self.railCollectionCellsData?.sectionSource?.lowercased() == SectionSourceType.prime.rawValue.lowercased() || self.railCollectionCellsData?.contentList?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                self.delegate?.startPrimeNavigationJourney(self.railCollectionCellsData?.contentList?[indexPath.row] ?? BAContentListModel())
            } else {
                //isPartnerHomePage = false
                editorialMixPanelEvents(indexPath)
                self.delegate?.moveToDetailScreen(railType: self.railCollectionCellsData?.sectionSource, layoutType: self.railCollectionCellsData?.layoutType, vodId: self.railCollectionCellsData?.contentList?[indexPath.row].id, contentType: self.railCollectionCellsData?.contentList?[indexPath.row].contentType, timestamp: self.railCollectionCellsData?.contentList?[indexPath.row].rentalExpiry, pageSource: pageSource, configSource: configSource, railName: railName)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.collectionView.isUserInteractionEnabled = true
        }
    }
    
    func editorialMixpanelViewAndHomeClickEvent(_ indexPath: IndexPath) {
        var properties = [String: Any]()
        var configType = ""
        var pageTypes = ""
        if pageType == "BINGE_ANYWHERE_HOME" {
            pageTypes = "Home"
        } else if pageType == "BINGE_ANYWHERE_MOVIES" {
            pageTypes = "Movies"
        } else if pageType == "BINGE_ANYWHERE_TVSHOWS" {
            pageTypes = "TV Shows"
        } else if pageType == "BINGE_ANYWHERE_KIDS" {
            pageTypes = "Kids"
        } else {
            pageTypes = "Home"
        }
        pageSource = pageName
        let genreResult = railCollectionCellsData?.contentList?[indexPath.row].genre?.joined(separator: ",")
        let languageResult = railCollectionCellsData?.contentList?[indexPath.row].language?.joined(separator: ",")
        if self.seeAllButton.isHidden {
            properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(railPosition)"
        } else {
            properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(self.seeAllButton.tag)"
        }
        
        properties[MixpanelConstants.ParamName.contentPosition.rawValue] = indexPath.row + 1
        properties[MixpanelConstants.ParamName.contentTitle.rawValue] = railCollectionCellsData?.contentList?[indexPath.row].title ?? ""
        properties[MixpanelConstants.ParamName.contentType.rawValue] = railCollectionCellsData?.contentList?[indexPath.row].contentType ?? MixpanelConstants.ParamValue.vod.rawValue
        properties[MixpanelConstants.ParamName.section.rawValue] = MixpanelConstants.ParamValue.rail.rawValue
        properties[MixpanelConstants.ParamName.genre.rawValue] = genreResult //railCollectionCellsData?.contentList?[indexPath.row].genre ?? []
        properties[MixpanelConstants.ParamName.railTitle.rawValue] = railCollectionCellsData?.title ?? ""
        properties[MixpanelConstants.ParamName.partner.rawValue] = railCollectionCellsData?.contentList?[indexPath.row].provider
        properties[MixpanelConstants.ParamName.partnerHome.rawValue] = (isPartnerHomePage ?? false) ? "YES" : "NO"
        properties[MixpanelConstants.ParamName.pageName.rawValue] = pageSource
        properties[MixpanelConstants.ParamName.contentLanguage.rawValue] = languageResult
        railName = railCollectionCellsData?.title ?? ""
        if railCollectionCellsData?.title == "Recently Watched" {
            configSource = "CONTINUE-WATCHING"
            properties[MixpanelConstants.ParamName.configType.rawValue] = "CONTINUE-WATCHING"
        } else {
            configSource = MixpanelConstants.ParamValue.editorial.rawValue
            properties[MixpanelConstants.ParamName.configType.rawValue] = MixpanelConstants.ParamValue.editorial.rawValue
        }
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeClick.rawValue, properties: properties)
        
        if railCollectionCellsData?.title == "Recently Watched" {
            configType = "CONTINUE-WATCHING"
            
        } else {
            configType = MixpanelConstants.ParamValue.editorial.rawValue
        }
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: railCollectionCellsData?.contentList?[indexPath.row].title ?? "", MixpanelConstants.ParamName.contentType.rawValue: railCollectionCellsData?.contentList?[indexPath.row].contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: railCollectionCellsData?.contentList?[indexPath.row].provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railCollectionCellsData?.title ?? "", MixpanelConstants.ParamName.origin.rawValue: MixpanelConstants.ParamValue.editorial.rawValue, MixpanelConstants.ParamName.source.rawValue: pageSource, MixpanelConstants.ParamName.configType.rawValue: configType, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
    
    func editorialMixPanelEvents(_ indexPath: IndexPath) {
        var properties = [String: Any]()
        _ = ""
        var pageTypes = ""
        if pageType == "BINGE_ANYWHERE_HOME" {
            pageTypes = "Home"
        } else if pageType == "BINGE_ANYWHERE_MOVIES" {
            pageTypes = "Movies"
        } else if pageType == "BINGE_ANYWHERE_TVSHOWS" {
            pageTypes = "TV Shows"
        } else if pageType == "BINGE_ANYWHERE_KIDS" {
            pageTypes = "Kids"
        } else {
            pageTypes = "Home"
        }
        pageSource = pageName
        let genreResult = railCollectionCellsData?.contentList?[indexPath.row].genre?.joined(separator: ",")
        let languageResult = railCollectionCellsData?.contentList?[indexPath.row].language?.joined(separator: ",")
        if self.seeAllButton.isHidden {
            properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(railPosition)"
        } else {
            properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(self.seeAllButton.tag)"
        }
        properties[MixpanelConstants.ParamName.contentPosition.rawValue] = indexPath.row + 1
        properties[MixpanelConstants.ParamName.contentTitle.rawValue] = railCollectionCellsData?.contentList?[indexPath.row].title ?? ""
        properties[MixpanelConstants.ParamName.contentType.rawValue] = railCollectionCellsData?.contentList?[indexPath.row].contentType ?? MixpanelConstants.ParamValue.vod.rawValue
        properties[MixpanelConstants.ParamName.section.rawValue] = MixpanelConstants.ParamValue.rail.rawValue
        properties[MixpanelConstants.ParamName.genre.rawValue] = genreResult //railCollectionCellsData?.contentList?[indexPath.row].genre ?? []
        properties[MixpanelConstants.ParamName.railTitle.rawValue] = railCollectionCellsData?.title ?? ""
        properties[MixpanelConstants.ParamName.partner.rawValue] = railCollectionCellsData?.contentList?[indexPath.row].provider
        properties[MixpanelConstants.ParamName.partnerHome.rawValue] = (isPartnerHomePage ?? false) ? "YES" : "NO"
        properties[MixpanelConstants.ParamName.pageName.rawValue] = pageTypes
        properties[MixpanelConstants.ParamName.contentLanguage.rawValue] = languageResult
        railName = railCollectionCellsData?.title ?? ""
        if railCollectionCellsData?.title == "Recently Watched" {
            configSource = "CONTINUE-WATCHING"
           properties[MixpanelConstants.ParamName.configType.rawValue] = "CONTINUE-WATCHING"
        } else {
            configSource = MixpanelConstants.ParamValue.editorial.rawValue
            properties[MixpanelConstants.ParamName.configType.rawValue] = MixpanelConstants.ParamValue.editorial.rawValue
        }
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeClick.rawValue, properties: properties)
    }
    
    func recommendedMixPanelEvents(_ indexPath: IndexPath) {
        var properties = [String: Any]()
        var pageTypes = ""
        _ = ""
        if pageType == "BINGE_ANYWHERE_HOME" {
            pageTypes = "Home"
        } else if pageType == "BINGE_ANYWHERE_MOVIES" {
            pageTypes = "Movies"
        } else if pageType == "BINGE_ANYWHERE_TVSHOWS" {
            pageTypes = "TV Shows"
        } else if pageType == "BINGE_ANYWHERE_KIDS" {
            pageTypes = "Kids"
        } else {
            pageTypes = "Home"
        }
        pageSource = pageName
        if self.seeAllButton.isHidden {
            properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(railPosition)"
        } else {
            properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(self.seeAllButton.tag)"
        }
        let genreResult = recommendedRailCellData?.contentList?[indexPath.row].genre?.joined(separator: ",")
        let languageResult = recommendedRailCellData?.contentList?[indexPath.row].language?.joined(separator: ",")
        properties[MixpanelConstants.ParamName.section.rawValue] = MixpanelConstants.ParamValue.rail.rawValue
        properties[MixpanelConstants.ParamName.railPosition.rawValue] = "\(railPosition + 1)"
        //properties[MixpanelConstants.ParamName.configType.rawValue] = MixpanelConstants.ParamValue.recomendation.rawValue
        properties[MixpanelConstants.ParamName.contentTitle.rawValue] = recommendedRailCellData?.contentList?[indexPath.row].title ?? ""
        properties[MixpanelConstants.ParamName.contentType.rawValue] = recommendedRailCellData?.contentList?[indexPath.row].contentType ?? MixpanelConstants.ParamValue.vod.rawValue
        properties[MixpanelConstants.ParamName.genre.rawValue] = genreResult ?? ""
        properties[MixpanelConstants.ParamName.railTitle.rawValue] = recommendedRailCellData?.title ?? ""
        properties[MixpanelConstants.ParamName.partner.rawValue] = recommendedRailCellData?.contentList?[indexPath.row].provider
        properties[MixpanelConstants.ParamName.partnerHome.rawValue] = (isPartnerHomePage ?? false) ? "YES" : "NO"
        properties[MixpanelConstants.ParamName.pageName.rawValue] = pageTypes
        properties[MixpanelConstants.ParamName.contentLanguage.rawValue] = languageResult
        railName = railCollectionCellsData?.title ?? ""
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeClick.rawValue, properties: properties)
        if recommendedRailCellData?.title == "Recently Watched" {
            configSource = "CONTINUE-WATCHING"
           properties[MixpanelConstants.ParamName.configType.rawValue] = "CONTINUE-WATCHING"
        } else {
            configSource = MixpanelConstants.ParamValue.recomendation.rawValue
            properties[MixpanelConstants.ParamName.configType.rawValue] = MixpanelConstants.ParamValue.recomendation.rawValue
        }
        
//        if recommendedRailCellData?.title == "Recently Watched" {
//            configType = "CONTINUE-WATCHING"
//        } else {
//            configType = MixpanelConstants.ParamValue.recomendation.rawValue
//
//        }
//        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: recommendedRailCellData?.contentList?[indexPath.row].title ?? "", MixpanelConstants.ParamName.contentType.rawValue: recommendedRailCellData?.contentList?[indexPath.row].contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: recommendedRailCellData?.contentList?[indexPath.row].provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: recommendedRailCellData?.title ?? "", MixpanelConstants.ParamName.origin.rawValue: MixpanelConstants.ParamValue.recomendation.rawValue, MixpanelConstants.ParamName.source.rawValue: pageTypes, MixpanelConstants.ParamName.configType.rawValue: configType])
    }
}


extension BAHomeRailTableViewCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == collectionView else {
            return
        }
        self.railCollectionCellsData?.lastScrolledOffset = scrollView.contentOffset.x
    }
    
}

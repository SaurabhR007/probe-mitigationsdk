//
//  BAHeroBannerTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import FSPagerView
import Mixpanel

protocol PrimeAppDelegateMethod {
    func startPrimeNavigationJourney(_ content: BAContentListModel)
}

extension PrimeAppDelegateMethod {
    func startPrimeNavigationJourney(_ content: BAContentListModel) {}
}

protocol BAHeroBannerTableViewCellDelegate: class, PrimeAppDelegateMethod {
    func moveToDetailScreenFromHeroBanner(vodId: Int?, contentType: String?, pageSource: String?, configSource: String?, railName: String?)
}


class BAHeroBannerTableViewCell: UITableViewCell, FSPagerViewDelegate, FSPagerViewDataSource {

    fileprivate let imageNames = ["1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "1.png"]
    fileprivate var numberOfItems = 7
    var bannerItem: HomeScreenDataItems?
    var isPartnerHomePage: Bool?
    var pageType = ""
    var pageSource = ""
    var configSource = ""
    var railName = ""
    var pageName = ""
    weak var delegate: BAHeroBannerTableViewCellDelegate?
    //var didSelectTime = Date()

    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: "StandardHeroBannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StandardHeroBannerCollectionViewCell")
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.isInfinite = true
            self.pagerView.automaticSlidingInterval = 3.0
        }
    }

    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = 0
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.backgroundColor = .clear
            self.pageControl.itemSpacing = 10.0
            self.pageControl.setFillColor(UIColor.BAwhite.withAlphaComponent(0.8), for: .selected)
            self.pageControl.setFillColor(UIColor.BAwhite.withAlphaComponent(0.2), for: .normal)
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup() {
        self.pagerView.delegate = self
        self.pagerView.dataSource = self
        if let autoScroll = bannerItem?.autoScroll {
            self.pagerView.automaticSlidingInterval = autoScroll ? 3.0 : 0.0
        }
        self.pagerView.reloadData()
        self.pageControl.numberOfPages = bannerItem?.contentList?.count ?? 0
        self.pagerView.backgroundColor = .BAdarkBlueBackground
        self.pagerView.itemSize = CGSize.init(width: ScreenSize.screenWidth, height: ScreenSize.screenWidth * (9/16))
    }

    // MARK: - FSPagerView DataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        guard  let count = bannerItem?.contentList?.count else {
            return 0
        }
        return count
    }

    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> UICollectionViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "StandardHeroBannerCollectionViewCell", at: index) as! StandardHeroBannerCollectionViewCell
        cell.configureCell((bannerItem?.contentList?[index])!)
        return cell
    }

    // MARK: - FSPagerView Delegate

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) { //shivam
        self.pagerView.isUserInteractionEnabled = false
        //CustomLoader.shared.showLoader(false)
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        mixPanelEvents(index)
        if bannerItem?.contentList?[index].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
            self.delegate?.startPrimeNavigationJourney(bannerItem?.contentList?[index] ?? BAContentListModel())
        } else {
            delegate?.moveToDetailScreenFromHeroBanner(vodId: bannerItem?.contentList?[index].id, contentType: bannerItem?.contentList?[index].contentType, pageSource: pageSource, configSource: configSource, railName: railName)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.pagerView.isUserInteractionEnabled = true
        }
    }

    func mixPanelEvents(_ index: Int) {
        var properties = [String: Any]()
        var pageTypes = ""
        if pageType == "BINGE_ANYWHERE_HOME" {
            pageTypes = "Home"
        } else if pageType == "BINGE_ANYWHERE_MOVIES" {
            pageTypes = "Movies"
        } else if pageType == "BINGE_ANYWHERE_TVSHOWS" {
            pageTypes = "TV Shows"
        } else if pageType == "BINGE_ANYWHERE_KIDS" {
            pageTypes = "Kids"
        } else {
            pageTypes = "Home"
        }
        pageSource = pageName
        let genreResult = bannerItem?.contentList?[index].genre?.joined(separator: ",")
        let langugageResult = bannerItem?.contentList?[index].language?.joined(separator: ",")
        properties[MixpanelConstants.ParamName.section.rawValue] = MixpanelConstants.ParamValue.hero.rawValue
        properties[MixpanelConstants.ParamName.contentType.rawValue] = bannerItem?.contentList?[index].contentType ?? MixpanelConstants.ParamValue.vod.rawValue
        properties[MixpanelConstants.ParamName.configType.rawValue] = MixpanelConstants.ParamValue.editorial.rawValue
        properties[MixpanelConstants.ParamName.contentTitle.rawValue] = bannerItem?.contentList?[index].title ?? ""
        properties[MixpanelConstants.ParamName.partnerName.rawValue] = bannerItem?.contentList?[index].provider ?? ""
        properties[MixpanelConstants.ParamName.genre.rawValue] = genreResult ?? ""
        properties[MixpanelConstants.ParamName.railPosition.rawValue] = "0"
        properties[MixpanelConstants.ParamName.railTitle.rawValue] = MixpanelConstants.ParamValue.hero.rawValue
        properties[MixpanelConstants.ParamName.heroBannerNumber.rawValue] = index + 1
        properties[MixpanelConstants.ParamName.contentPosition.rawValue] = index + 1
        properties[MixpanelConstants.ParamName.partnerHome.rawValue] = (isPartnerHomePage ?? false) ? "YES" : "NO"
        properties[MixpanelConstants.ParamName.pageName.rawValue] = pageSource
        properties[MixpanelConstants.ParamName.contentLanguage.rawValue] = langugageResult
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeClick.rawValue, properties: properties)
        railName = MixpanelConstants.ParamValue.hero.rawValue
        configSource = MixpanelConstants.ParamValue.editorial.rawValue
        
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: bannerItem?.contentList?[index].title ?? "", MixpanelConstants.ParamName.contentType.rawValue: bannerItem?.contentList?[index].contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: bannerItem?.contentList?[index].provider ?? "", MixpanelConstants.ParamName.source.rawValue: pageSource, MixpanelConstants.ParamName.contentLanguage.rawValue: langugageResult ?? ""])
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }

    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }

    @IBAction func sliderValueChanged(_ sender: UISlider) {
        switch sender.tag {
        case 1:
            _ = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
//            self.pagerView.itemSize = CGSize.init(width: 375, height: 200)
//            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
        case 2:
            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
        case 3:
            self.numberOfItems = Int(roundf(sender.value*7.0))
            self.pageControl.numberOfPages = self.numberOfItems
            self.pagerView.reloadData()
        default:
            break
        }
    }
}

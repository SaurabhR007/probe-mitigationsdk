//
//  ApiSuccessViewController.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 23/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

class ApiSuccessViewController: BaseViewController {

    @IBOutlet weak var successHitImage: UIImageView!
    @IBOutlet weak var successTitleLabel: CustomLabel!
    @IBOutlet weak var secondaryTitleLabel: CustomLabel!
    @IBOutlet weak var messageLabel: CustomLabel!
    @IBOutlet weak var nextButton: CustomButton!
    var hideNextButton = false
    var successTitleText = ""
    var secondarySuccessTitleText = ""
    var secondaryTitleText: String = ""
    var messageText = ""
    var nextButtonTitle = ""
    var addTimer = false
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    private func configureUI() {
        view.backgroundColor = .BAdarkBlueBackground
        successTitleLabel.textColor = .BAwhite
        successTitleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30), color: .BAwhite)
        secondaryTitleLabel.textColor = .BAwhite
        secondaryTitleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        messageLabel.textColor = .BAwhite
        messageLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BASecondaryGray)
        nextButton.backgroundColor = .BABlueColor
        nextButton.makeCircular(roundBy: .height)
        nextButton.isHidden = hideNextButton
        setText()
        if addTimer {
            runCodeExpiryTimer()
        }
    }

    func setText() {
        successTitleLabel.text = successTitleText
        secondaryTitleLabel.text = secondaryTitleText
        messageLabel.text = messageText
		nextButton.text(nextButtonTitle)
    }

    @IBAction func nextAction(_ sender: Any) {
        UtilityFunction.shared.performTaskInMainQueue {
            if self.nextButton.currentTitle == "Back to Accounts" {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: AccountVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            } else {
                ///Discussion: To be discussed what is happening.
				UtilityFunction.shared.clearLocalData()
                kAppDelegate.createHomeTabRootView()
            }
        }
    }

    func moveToHomeScreen() {
        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
    }

    func runCodeExpiryTimer() {
        var resetCount = 5
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            if resetCount > 0 {
                resetCount -= 1
                self.secondaryTitleLabel.text = "You’ll be Redirected back to the Login Page in \(resetCount) secs"
            } else {
                timer.invalidate()
                self.moveToHomeScreen()
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }

}

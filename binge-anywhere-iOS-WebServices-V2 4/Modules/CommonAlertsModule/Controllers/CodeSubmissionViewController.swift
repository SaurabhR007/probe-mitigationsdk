//
//  CodeSubmissionViewController.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 23/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

class CodeSubmissionViewController: UIViewController {

    @IBOutlet weak var successMessageTitleLabel: CustomLabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }

    private func configureUI() {
        view.backgroundColor = .BAdarkBlueBackground
        successMessageTitleLabel.text = "Your Code is Successful"
        successMessageTitleLabel.textColor = .BAwhite
        successMessageTitleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 30), color: .BAwhite)
        activityIndicator.startAnimating()
    }

    func configureMessage(with message: String) {
        successMessageTitleLabel.text = message
    }
}

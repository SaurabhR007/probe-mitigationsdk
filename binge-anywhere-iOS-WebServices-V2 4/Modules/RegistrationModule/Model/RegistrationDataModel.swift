//
//  RegistrationDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

enum RegistrationVcDataSource {
    case pageIndicator
    case header
    case name
    case mobile
    case email
    case createPassword
    case verifyPassword
    case tataSkySubscriber
    case next
    case alreadyCustomer
    case footer

    static let numberOfCells = [pageIndicator, header, name, mobile, email, createPassword, verifyPassword, tataSkySubscriber, next, alreadyCustomer, footer]

    static var user: LoginDataModel?
    var user: LoginDataModel? {
        get {return RegistrationVcDataSource.user}
        set {RegistrationVcDataSource.user = newValue}
    }

    var title: String {
        switch self {
        case .header, .pageIndicator: return ""
        case .name: return "Name"
        case .mobile: return "Mobile"
        case .email: return "Email"
        case .createPassword: return "Create Password (Optional)"
        case .verifyPassword: return "Verify Password (Optional)"
        default: return ""
        }
    }

    var placeHolder: String {
        switch self {
        case .header, .pageIndicator: return ""
        case .name: return "Name"
        case .mobile: return ""//"1234 56789"
        case .email: return "name@email.com"
        case .createPassword: return "Password ⃰"
        case .verifyPassword: return "Verify Password ⃰"
        default: return ""
        }
    }

    var value: String {
        switch self {
        case .header, .pageIndicator: return ""
        case .name: return user?.name ?? ""
        case .mobile: return user?.rmn ?? ""
        case .email: return user?.email ?? ""
        case .createPassword: return user?.password ?? ""
        case .verifyPassword: return user?.verifyPassword ?? ""
        default: return ""
        }
    }

    enum CellType {
        case pageIndicator
        case header
        case textField
        case linkMyAccount
        case nextButton
        case alreadyCustomer
        case footer

        static let typeOfCells = [pageIndicator, header, textField, linkMyAccount, nextButton, alreadyCustomer, footer]

        var cellIdentifier: String {
            switch self {
            case .pageIndicator:
                return "PageIndicatorTableViewCell"
            case .header:
                return "TitleHeaderTableViewCell"
            case .textField:
                return "SignUpTextFieldTableViewCell"
            case .linkMyAccount:
                return "SignUpAlreadyUserTableViewCell"
            case .nextButton:
                return "NextButtonTableViewCell"
            case .alreadyCustomer:
                return "SignUpLoginTableViewCell"
            case .footer:
                return "BottomBingeLogoTableViewCell"
            }
        }
    }

    enum Keyboard {
        case emailAddess
        case defaultKeyboard
        case number
    }

    enum SecureTextField {
        case password
        case defaultField
    }

    enum ReturnType {
        case next
        case done
    }

    var cellType: CellType {
        switch self {
        case .pageIndicator:
            return .pageIndicator
        case .header:
            return .header
        case .name, .mobile, .email, .verifyPassword, .createPassword:
            return .textField
        case .tataSkySubscriber:
            return .linkMyAccount
        case .alreadyCustomer:
            return .alreadyCustomer
        case .footer:
            return .footer
        case .next:
            return .nextButton
        }
    }

    var keyBoardType: Keyboard {
        switch self {
        case .email: return .emailAddess
        case .mobile: return .number
        default:
            return .defaultKeyboard
        }
    }

    var secureTextField: SecureTextField {
        switch self {
        case .createPassword, .verifyPassword:
            return .password
        default:
            return .defaultField
        }
    }

    var keyBoardReturnType: ReturnType {
        switch self {
        case .verifyPassword:
            return .done
        default:
            return .next
        }
    }

}

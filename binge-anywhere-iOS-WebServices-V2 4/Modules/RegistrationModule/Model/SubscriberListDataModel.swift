//
//  SubscriberListDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 22/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct SubscriberListModel: Codable {
    let code: Int?
    let message: String?
    let data: [String]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([String].self, forKey: .data)
    }
}

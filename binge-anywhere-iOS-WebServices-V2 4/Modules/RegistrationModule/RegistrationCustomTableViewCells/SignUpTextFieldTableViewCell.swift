//
//  SignUpTextFieldTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 24/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol SignUpTextFieldTableViewCellProtocol: AnyObject {
    func valueUpdated(_ idx: Int, _ text: String)
    func fetchSubscriberList()
}

class SignUpTextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var inputTextField: CustomtextField!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var prefixLabel: CustomLabel!
    @IBOutlet weak var errorInfoLabel: CustomLabel!
    @IBOutlet weak var passwordPeekButton: CustomButton!

    // MARK: - Variables
    weak var delegate: SignUpTextFieldTableViewCellProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()

        configureUI()
    }

    // MARK: - Lifecycle
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground

        inputTextField.delegate = self
        inputTextField.text = ""
        inputTextField.backgroundColor = .BAdarkBlue1Color
        inputTextField.textColor = .BAwhite
        inputTextField.tintColor = .BAwhite
        inputTextField.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
        inputTextField.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth))
        inputTextField.cornerRadius = 2
        inputTextField.setViewShadow(opacity: 0.12, blur: 2)
        inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)

        titleLabel.text = ""
        titleLabel.textColor = .BAwhite
        titleLabel.font = .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth)

        prefixLabel.text = ""
        prefixLabel.textColor = .BAwhite
        prefixLabel.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)

        errorInfoLabel.text = ""
        errorInfoLabel.textColor = .BAerrorRed
        errorInfoLabel.font = .skyTextFont(.medium, size: 10 * screenScaleFactorForWidth)

        passwordPeekButton.isHidden = !inputTextField.isSecureTextEntry
    }

    private func isTextFieldHighlighted(_ highlighted: Bool) {
        inputTextField.borderWidth = 0.0
        if highlighted {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAlightBlueGrey
        }
    }

    private func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
        if hasError {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAerrorRed
            inputTextField.placeholder = Placeholder
            errorInfoLabel.text = errorMessage
        } else {
            inputTextField.borderWidth = 0.0
            errorInfoLabel.text = ""
        }
    }

    private func validateData(_ tag: Int, _ text: String) -> Bool {
        switch tag {
        case 3:
            return text.length <= 10
        default:
            return true
        }
    }

    func configureCell(_ data: RegistrationVcDataSource) {
        titleLabel.text = data.title
        inputTextField.placeholder = data.placeHolder

        switch data.keyBoardType {
        case .number:
            prefixLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
            prefixLabel.text = "+91"
            inputTextField.setHorizontalPadding(left: 45 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
            inputTextField.addDoneButtonOnKeyBoardWithControl()
            inputTextField.keyboardType = .numberPad
        case .emailAddess:
            inputTextField.keyboardType = .asciiCapable
        default:
            inputTextField.keyboardType = .asciiCapable
        }

        switch data.secureTextField {
        case .password:
            inputTextField.isSecureTextEntry = true
            passwordPeekButton.isHidden = false
            inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 40 * screenScaleFactorForWidth)
        default:
            inputTextField.isSecureTextEntry = false
        }

        switch data.keyBoardReturnType {
        case .done:
            inputTextField.returnKeyType = .done
        default:
            inputTextField.returnKeyType = .next
        }
    }

    // MARK: - Actions
    @IBAction func peekPassword() {
        if inputTextField.isSecureTextEntry {
            inputTextField.isSecureTextEntry = false
            passwordPeekButton.setImage(UIImage(named: "PasswordVisibleEyeopen"), for: .normal)
        } else {
            inputTextField.isSecureTextEntry = true
            passwordPeekButton.setImage(UIImage(named: "PasswordHiddenEyeclosed"), for: .normal)
        }
    }

    @IBAction private func txtFieldBeginEditing(_ sender: CustomtextField) {
        textFieldHasError(false)
        isTextFieldHighlighted(true)
    }

    @IBAction private func txtFieldEndEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(false)
        if let textString = sender.text {
            switch sender.tag {
            case 3:
                if !textString.isValidContact {
                    textFieldHasError(true, with: "", errorMessage: "Please enter a valid number")
                    return
                }
                delegate?.fetchSubscriberList()
            case 4:
                if !textString.isValidEmail() {
                    textFieldHasError(true, with: "name@email.com", errorMessage: "Please enter a valid email")
                }
            default:
                if textString.isEmpty {
                    textFieldHasError(false)
                }
            }
        }
    }
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let del = delegate {
            del.valueUpdated(sender.tag, sender.text ?? "")
        }
    }

}

// MARK: - Extension
extension SignUpTextFieldTableViewCell {

    // MARK: - TableView Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        return validateData(textField.tag, txtAfterUpdate)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.superview?.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

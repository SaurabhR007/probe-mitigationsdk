//
//  SignUpLoginTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 17/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol SignUpToLoginProtocol: AnyObject {
    func moveToSignInView()
}

class SignUpLoginTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var firstLabel: CustomLabel!
    @IBOutlet weak var attributedLabel: CustomLabel!

    // MARK: - Variables
    weak var delegate: SignUpToLoginProtocol?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        addAttributedString()
        configureUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - Functions
    func configureUI() {
        firstLabel.textColor = .BAwhite
        firstLabel.font = UIFont.skyTextFont(.regular, size: 16*screenScaleFactorForWidth)
        attributedLabel.textColor = .BABlueColor
        firstLabel.font = UIFont.skyTextFont(.regular, size: 16*screenScaleFactorForWidth)
    }

    func addAttributedString() {
        let labelString = "Login"
        let attributedText = NSMutableAttributedString(string: labelString)
        let substr = "Login"
        let textRange = attributedText.mutableString.range(of: substr, options: NSString.CompareOptions.caseInsensitive)
        let underLineColor: UIColor = .BABlueColor
        let underLineStyle = NSUnderlineStyle.single.rawValue
        attributedText.addAttribute(.foregroundColor, value: underLineColor, range: textRange)
        attributedText.addAttribute(.underlineColor, value: underLineColor, range: textRange)
        attributedText.addAttribute(.underlineStyle, value: underLineStyle, range: textRange)
        attributedText.addAttribute(.font, value: UIFont.skyTextFont(.regular, size: 16 * screenScaleFactorForWidth), range: textRange)
        self.attributedLabel.attributedText = attributedText
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        attributedLabel.isUserInteractionEnabled = true
        attributedLabel.addGestureRecognizer(tap)
    }

    @objc func tapFunction(sender: UITapGestureRecognizer) {
        if let delegate = delegate {
            delegate.moveToSignInView()
        }
    }
}

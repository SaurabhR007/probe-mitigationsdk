//
//  RegistrationVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol RegistrationVMProtocol {
    var dataModel: [RegistrationVcDataSource] {get set}
    var subscriberIdList: [String] {get set}
    func createApiParams(completion: @escaping(Bool, String?) -> Void)
//    func hitApiForRegistration(completion: @escaping (Bool, String?) -> Void)
//    func fetchSubscriberList(completion: @escaping (Bool, String?) -> Void)
}

class RegistrationVM: RegistrationVMProtocol, RegistrationCall {

    var dataModel: [RegistrationVcDataSource] = []
    var subscriberIdList: [String] = []
    var apiParams: [AnyHashable: Any] = [ : ]
    var requestToken: ServiceCancellable?
//    var fullname: String?
//    var mobileNumber: String?

    func createApiParams(completion: @escaping(Bool, String?) -> Void) {
        guard let email = dataModel[0].user?.email, let name = dataModel[0].user?.name, let rmn = dataModel[0].user?.rmn else {
            completion(false, "Please fill all mandatory fields")
            return
        }

        if !rmn.isValidContact {
            completion(false, "Please Enter Valid Number")
            return
        } else if !email.isValidEmail() {
            completion(false, "Please Enter Valid Email")
            return
        }

        apiParams["rmn"] = rmn
        var firstName: String = ""
        var lastName: String = ""
        let fullName = UtilityFunction.shared.trimWhiteSpaces(value: name)
        let fullNameArr: [String] = fullName.components(separatedBy: " ")
        if fullNameArr.count > 1 {
            firstName = fullNameArr[0]
            lastName = fullNameArr[1]
        } else {
            firstName = fullNameArr[0]
            lastName = firstName
        }

        apiParams["firstName"] = firstName
        apiParams["lastName"] = lastName
        apiParams["email"] = email

        let message = checkForPassword().0
        let boolValue = checkForPassword().1
        if !boolValue {
            completion(false, message)
            return
        }
        completion(true, nil)
    }

    func checkForPassword() -> (String, Bool) {
        let password = dataModel[0].user?.password ?? ""
        let createPassword = dataModel[0].user?.verifyPassword ?? ""

        if password == createPassword && !password.isEmpty {
            if !password.isValidPasswordRegex() {
                return ("Password must contain One Special Character, One Number, and of length greater than 8.", false)
            }
            apiParams["confirmPwd"] = createPassword
            apiParams["password"] = password
            return ("", true)
        } else if password.isEmpty && createPassword.isEmpty {
            return ("", true)
        }
        return ("Create Password and verify password is not same", false)
    }

//    func hitApiForRegistration(completion: @escaping (Bool, String?) -> Void) {
//        requestToken = generateRegistrationOtpApiCall(completion: { (configData, error) in
//            self.requestToken = nil
//            if let _configData = configData {
//                if let statusCode = _configData.parsed.code {
//                    if statusCode == 0 {
//                       // BAUserDefaultManager().fullName = _configData.parsed.data.
//                        completion(true, nil)
//                    } else {
//                        if let errorMessage = _configData.parsed.message {
//                            completion(false, errorMessage)
//                        } else {
//                            completion(false, "Some Error Occurred")
//                        }
//                    }
//                } else {
//                    completion(false, "Some Error Occurred")
//                }
//            } else if let error = error {
//                completion(false, error.error.localizedDescription)
//            } else {
//                completion(false, "Some Error Occurred")
//            }
//        })
//    }

//    func fetchSubscriberList(completion: @escaping (Bool, String?) -> Void) {
//        var params = APIParams()
//        params["rmn"] = dataModel[0].user?.rmn ?? ""
//        params["userDetail"] = "SHORT_DESCRIPTION"
//        requestToken = fetchSubscriberListApiCall(params: params, completion: { (configData, error) in
//            self.requestToken = nil
//            if let _configData = configData {
//                if let statusCode = _configData.parsed.code {
//                    if statusCode == 0 {
//                        if let subscriberList = _configData.parsed.data {
//                            self.subscriberIdList = subscriberList
//                            if subscriberList.isEmpty {
//                                completion(false, nil)
//                                return
//                            }
//                            completion(true, nil)
//                        }
//                        completion(true, nil)
//                    } else {
//                        if let errorMessage = _configData.parsed.message {
//                            completion(false, errorMessage)
//                        } else {
//                            completion(false, "Some Error Occurred")
//                        }
//                    }
//                } else {
//                    completion(false, "Some Error Occurred")
//                }
//            } else if let error = error {
//                completion(false, error.error.localizedDescription)
//            } else {
//                completion(false, "Some Error Occurred")
//            }
//        })
//    }
}

//
//  RegistrationDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol RegistrationCall {
    var apiParams: [AnyHashable: Any] {get set}
//    func generateRegistrationOtpApiCall(completion: @escaping (APIServicResult<LoginDataServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
//    func fetchSubscriberListApiCall(params: APIParams, completion: @escaping (APIServicResult<SubscriberListModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

// MARK: - Extention
extension RegistrationCall {

//    func generateRegistrationOtpApiCall(completion: @escaping (APIServicResult<LoginDataServerResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
//        let target = ServiceRequestDetail.init(endpoint: .generateRegistrationOtp(param: apiParams, headers: nil))
//        return APIService().request(target) { (response: APIResult<APIServicResult<LoginDataServerResponse>, ServiceProviderError>) in
//            completion(response.value, response.error)
//        }
//    }
//
//    func fetchSubscriberListApiCall(params: APIParams, completion: @escaping (APIServicResult<SubscriberListModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
//        let target = ServiceRequestDetail.init(endpoint: .fetchSubscriberList(param: params, headers: nil))
//        return APIService().request(target) { (response: APIResult<APIServicResult<SubscriberListModel>, ServiceProviderError>) in
//            completion(response.value, response.error)
//        }
//    }

}

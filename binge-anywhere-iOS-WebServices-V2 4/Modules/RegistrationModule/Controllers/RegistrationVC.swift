//
//  RegistrationVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class RegistrationVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Variables
    var registrationViewModel: RegistrationVM?
    var apiParams: [AnyHashable: Any] = [ : ]
    var linkAccount = false

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.registerOnTataSky.rawValue, properties: [MixpanelConstants.ParamName.type.rawValue: MixpanelConstants.ParamValue.bingeMobile.rawValue])
        setNavigationBar()
        setUpTableView()
        reCheckVM()
        self.view.backgroundColor = .BAdarkBlueBackground
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: - Functions
    private func reCheckVM() {
        if registrationViewModel == nil {
            registrationViewModel = RegistrationVM()
            registrationViewModel?.dataModel = RegistrationVcDataSource.numberOfCells
            registrationViewModel?.dataModel[0].user = LoginDataModel()
        }
    }

    func setUpTableView() {
        let count = RegistrationVcDataSource.CellType.typeOfCells
        tableView.rowHeight = UITableView.automaticDimension
        for item in count {
            UtilityFunction.shared.registerCell(tableView, item.cellIdentifier)
        }

        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }

    private func setNavigationBar() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
    }

    @objc func backButtonAction() {
        self.navigationController?.popToRootViewController(animated: true)
    }

    func fetchSubscriberList() {
//        if let viewModel = registrationViewModel {
//            viewModel.fetchSubscriberList { (flagValue, message) in
//                self.linkAccount = flagValue
//                self.tableView.reloadRows(at: [IndexPath(item: 7, section: 0)], with: .none)
//                print(message ?? "")
//            }
//        }
    }

    func hitApiForRegistration() {
        self.showActivityIndicator(isUserInteractionEnabled: false)
//        self.registrationViewModel?.hitApiForRegistration(completion: { (flagValue, message) in
//            self.hideActivityIndicator()
//            if flagValue {
//                self.moveToRegistrationVerificationView()
//            } else {
//				self.apiError(message)
//            }
//        })
    }

//    func showOkAlert(_ message: String) {
//        AlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, value) in
//            self.hideActivityIndicator()
//            self.view.endEditing(true)
//            print(index, value)
//        }
//    }

    func moveToRegistrationVerificationView() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "SignUpVerificationVC", type: RegistrationVerificationVC.self)
        pushView.registrationModel = registrationViewModel?.apiParams ?? [ : ]
        pushView.subscriberIdList = registrationViewModel?.subscriberIdList ?? []
        pushView.linkMyAccountFlag = linkAccount
        self.navigationController?.pushViewController(pushView, animated: true)
    }
}

// MARK: - Extention
extension RegistrationVC: SignUpTextFieldTableViewCellProtocol, NextButtonActionDelegate, SignUpToLoginProtocol {

    func valueUpdated(_ idx: Int, _ text: String) {
        switch idx {
        case 2:
            registrationViewModel?.dataModel[0].user?.name = text
        case 3:
            registrationViewModel?.dataModel[0].user?.rmn = text
        case 4:
            registrationViewModel?.dataModel[0].user?.email = text
        case 5:
            registrationViewModel?.dataModel[0].user?.password = text
        case 6:
            registrationViewModel?.dataModel[0].user?.verifyPassword = text
        default:
            break
        }
    }

    func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            registrationViewModel?.createApiParams(completion: { (flagValue, message) in
                if flagValue {
                    self.hitApiForRegistration()
                } else {
                    self.apiError(message, title: kSomethingWentWrong)
                }
            })
        } else {
			noInternet() {
				self.nextButtonTapped()
			}
        }
    }

    func moveToSignInView() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "LoginVC", type: LoginVC.self)
        navigationController?.pushViewController(pushView, animated: true)
    }
}

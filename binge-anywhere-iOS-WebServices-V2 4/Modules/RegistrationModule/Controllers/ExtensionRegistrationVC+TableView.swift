//
//  ExtensionRegistrationVC+TableView.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

// MARK: - Extention
extension RegistrationVC: UITableViewDataSource, UITableViewDelegate {

    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataSource = registrationViewModel?.dataModel else { return 0 }
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kPageIndicatorTableViewCell) as! PageIndicatorTableViewCell
            cell.configureCell(with: .first)
            return cell
        case 1:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kTitleHeaderTableViewCell) as! TitleHeaderTableViewCell
            cell.configureCell(source: .signup)
            return cell
        case 2, 3, 4, 5, 6:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kSignUpTextFieldTableViewCell) as! SignUpTextFieldTableViewCell
            cell.delegate = self
            cell.inputTextField.tag = indexPath.row
            cell.configureCell((registrationViewModel?.dataModel[indexPath.row])!)
            return cell
        case 7:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kSignUpAlreadyUserTableViewCell) as! SignUpAlreadyUserTableViewCell
            cell.updateCheckButtonState(linkAccount)
            cell.configureCell(source: .signup)
            return cell
        case 8:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kNextButtonTableViewCell) as! NextButtonTableViewCell
            cell.delegate = self
            return cell
        case 9:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kSignUpLoginTableViewCell) as! SignUpLoginTableViewCell
            cell.delegate = self
            return cell
        case 10:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as! BottomBingeLogoTableViewCell
            return cell
        default:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as! BottomBingeLogoTableViewCell
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 60
        default:
            return UITableView.automaticDimension
        }
    }
}

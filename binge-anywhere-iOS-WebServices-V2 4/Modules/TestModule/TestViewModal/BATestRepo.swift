//
//  BATestRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 09/11/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BATestScreenRepo {
    var apiEndPoint: String {get set}
    func testApiCall(completion: @escaping(APIServicResult<BAFAQModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BATestRepo: BATestScreenRepo {
    var apiEndPoint: String = ""
    let apiParams = APIParams()
    func testApiCall(completion: @escaping (APIServicResult<BAFAQModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .faqCallback(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAFAQModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

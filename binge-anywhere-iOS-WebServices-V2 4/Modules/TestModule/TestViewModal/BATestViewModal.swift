//
//  BATestViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 09/11/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BATestViewModalProtocol {
    func faqApiCall(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BATestViewModal: BAFAQViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAFAQScreenRepo
    var faqModal: [Faq]?

    init(repo: BAFAQScreenRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }
    
    func faqApiCall(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo.faqApiCall(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.faqModal = _configData.parsed.data
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

//
//  BAAppDetailTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BAAppDetailTableViewCellDelegate: class {
    func moveToSubscriptionPage()
}

class BAAppDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var addSubscriptionButton: CustomButton!
    @IBOutlet weak var appDetailLabel: UILabel!
    @IBOutlet weak var notSubscribedLabel: UILabel!
    weak var delegate: BAAppDetailTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureAppInfo(appName: String?) {
        let appConcatinatedName = (appName ?? "") + " " + "|"
        notSubscribedLabel.text = appConcatinatedName + " " + "Not Subscribed"
        appDetailLabel.text = "\(appName ?? "") is a streaming service that offers a wide variety of award-winning TV shows, movies, anime, documentaries and more."
        addSubscriptionButton.makeCircular(roundBy: .height)
        addSubscriptionButton.setTitle("Add \(appName ?? "") to My Subscription", for: .normal)
        self.contentView.backgroundColor = .BAdarkBlueBackground
    }

    @IBAction func addSubscriptionButtonAction(_ sender: Any) {
        delegate?.moveToSubscriptionPage()
        // TODO: Flow yet to be decided
    }
}

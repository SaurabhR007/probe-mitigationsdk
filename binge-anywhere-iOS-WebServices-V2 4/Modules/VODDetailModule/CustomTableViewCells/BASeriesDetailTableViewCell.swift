//
//  BASeriesDetailTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BASeriesDetailTableViewCellDelegate: class {
    func switchToSeason(index: Int?)
}

class BASeriesDetailTableViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var seasonCollectionView: UICollectionView!
    var seasonDetail: [SeriesYearsList]?
    var episodesData: BASeriesDetailModal?
    weak var delegate: BASeriesDetailTableViewCellDelegate?
    var contentType = ""
    var contentDetails: ContentDetailData?
    var selectedIndex: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        registerCells()
        conformDelegates()
        // Initialization code
    }

    private func registerCells() {
        seasonCollectionView.register(UINib(nibName: kBASeasonCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBASeasonCollectionViewCell)
    }

    func conformDelegates() {
        seasonCollectionView.delegate = self
        seasonCollectionView.dataSource = self
    }

    func configureCollectionView(seriesList: [SeriesYearsList]?, selectedSeason: Int?, contentType: String?, contentDetail: ContentDetailData?) {
        selectedIndex = selectedSeason ?? 0
        seasonDetail = seriesList
        self.contentType = contentType ?? ""
        self.contentDetails = contentDetail
        seasonCollectionView.backgroundColor = .BAdarkBlueBackground
        seasonCollectionView.reloadData{
            if self.selectedIndex >= 0 {
                self.scrollToSelectedSeason(self.selectedIndex)
            }
        }
        self.seasonCollectionView.layoutIfNeeded()
    }

    @IBAction func loadMoreButtonAction(_ sender: Any) {
    }
}

extension BASeriesDetailTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if contentType == ContentType.brand.rawValue {
            return self.seasonDetail?.count ?? 0
        } else {
           return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var seasonTitle = ""
        if contentType == ContentType.brand.rawValue {
            seasonTitle = seasonDetail?[indexPath.row].seriesName ?? ""
        } else if contentType == ContentType.tvShows.rawValue && contentDetails?.meta?.parentContentType == ContentType.brand.rawValue {
            seasonTitle = seasonDetail?[indexPath.row].seriesName ?? ""
        } else {
            seasonTitle = " Other Episodes"
        }
        let labelWidth = stringWidth(string: seasonTitle, font: .skyTextFont(.regular, size: 16))
        return CGSize(width: labelWidth /*(collectionView.frame.width / 4)*/, height: 50.0)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBASeasonCollectionViewCell, for: indexPath) as! BASeasonCollectionViewCell
        cell.backgroundColor = .BAdarkBlue1Color
        if indexPath.row == selectedIndex {
            cell.selectedView.isHidden = false
            cell.seasonlabel.textColor = .white
        } else {
            cell.selectedView.isHidden = true
            cell.seasonlabel.textColor = .lightGray
        }
        if contentType == ContentType.brand.rawValue {
           cell.configureSeasonsList(seriesList: seasonDetail?[indexPath.row])
        } else if contentType == ContentType.tvShows.rawValue && contentDetails?.meta?.parentContentType == ContentType.brand.rawValue {
            cell.configureSeasonsList(seriesList: seasonDetail?[indexPath.row])
        } else {
          cell.configureSeasonsListForSeries(title: " Other Episodes")
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.switchToSeason(index: indexPath.row)
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? BASeasonCollectionViewCell {
            UtilityFunction.shared.performTaskInMainQueue {
                cell.selectedView.isHidden = true
                cell.seasonlabel.textColor = .lightGray
            }
        }
    }
    
    func scrollToSelectedSeason(_ idx: Int) {
        let index = IndexPath.init(row: idx, section: 0)
        UtilityFunction.shared.performTaskInMainQueue {
            guard let _ = self.seasonCollectionView.cellForItem(at: index) as? BASeasonCollectionViewCell else {
                return
            }
            self.seasonCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            self.seasonCollectionView.selectItem(at: index, animated: false, scrollPosition: .centeredHorizontally)
        }
    }
    
    func stringWidth(string: String, font: UIFont) -> CGFloat {
        let attributes = NSDictionary(object: font, forKey: NSAttributedString.Key.font as NSCopying)
        let sizeOfText = string.size(withAttributes: (attributes as! [NSAttributedString.Key: AnyObject]))
        return sizeOfText.width
    }
}

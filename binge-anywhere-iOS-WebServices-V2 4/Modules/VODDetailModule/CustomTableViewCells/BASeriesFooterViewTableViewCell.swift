//
//  BASeriesFooterViewTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 21/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BASeriesFooterViewTableViewCellDelegate: class {
    func seriesFooterViewTableViewCellDelegateDidTapLoadMore(_ cell: BASeriesFooterViewTableViewCell)
}

class BASeriesFooterViewTableViewCell: UITableViewCell {

    @IBOutlet weak var loadMoreButton: UIButton!
    @IBOutlet weak var loadMoreLabel: UILabel!
    weak var delegate: BASeriesFooterViewTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.BAdarkBlueBackground
        loadMoreLabel.font = .skyTextFont(.medium, size: 14)
        loadMoreLabel.textColor = .BABlueColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func loadMoreButtonAction(_ sender: Any) {
        delegate?.seriesFooterViewTableViewCellDelegateDidTapLoadMore(self)
    }
}

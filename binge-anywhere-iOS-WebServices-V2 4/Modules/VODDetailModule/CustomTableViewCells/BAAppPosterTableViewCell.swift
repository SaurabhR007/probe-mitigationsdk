//
//  BAAppPosterTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class BAAppPosterTableViewCell: UITableViewCell {

    @IBOutlet weak var appLogoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	func configureAppPoster(appName: String?) {
		var url: URL?
        switch appName?.uppercased() {
        case ProviderType.prime.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.unsubscribedMob ?? "")
        case ProviderType.sunnext.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.unsubscribedMob ?? "")
        case ProviderType.netflix.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.NETFLIX?.unsubscribedMob ?? "")
        case ProviderType.shemaro.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.unsubscribedMob ?? "")
        case ProviderType.vootSelect.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.unsubscribedMob ?? "")
        case ProviderType.vootKids.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.unsubscribedMob ?? "")
        case ProviderType.hungama.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.unsubscribedMob ?? "")
        case ProviderType.zee.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.unsubscribedMob ?? "")
        case ProviderType.sonyLiv.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.unsubscribedMob ?? "")
        case ProviderType.hotstar.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.unsubscribedMob ?? "")
        case ProviderType.eros.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.unsubscribedMob ?? "")
        case ProviderType.curosityStream.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.unsubscribedMob ?? "")
        case ProviderType.hopster.rawValue:
            appLogoImageView.image = UIImage(named: "heroPalceholder")
        default:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.unsubscribedMob ?? "")
        }
		if let _url = url {
			appLogoImageView.alpha = 0
			appLogoImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
				UIView.animate(withDuration: 1, animations: {
					self.appLogoImageView.alpha = 1
				})
			})
		}
	}

}

//
//  BAEpisodeTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

protocol BAEpisodeTableViewCellDelegate: class {
    func baEpisodeTableViewCell(_ cell: BAEpisodeTableViewCell, didUpdateEpisode episode: Episodes?, for indexPath: IndexPath?)
    func moveToPlayEpisode(episode: Episodes?, isResume: Bool?, isPlayerInPausedState: Bool?)
}

class BAEpisodeTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var continueWatchImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var episodeDetail: UILabel!
    @IBOutlet weak var moreLessButton: UIButton!
    @IBOutlet weak var episodeDuration: UILabel!
    @IBOutlet weak var episodeTitle: UILabel!
    @IBOutlet weak var episodeImageView: UIImageView!
    @IBOutlet weak var watchedProgressView: UIProgressView!

    // MARK: - Variables
    weak var delegate: BAEpisodeTableViewCellDelegate?
    var selectedIndex: IndexPath?
    var episode: Episodes?
    private var isConfigured = false

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.separatorView.backgroundColor = .BAdarkBlueBackground
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - Functions
    func configureEpisodes(episodeData: Episodes?, index: IndexPath) {
        episode = episodeData
        selectedIndex = index
        episodeTitle.text = "\(index.row + 1). \(episodeData?.title ?? "")"
        moreLessButton.setTitleColor(.BABlueColor, for: .normal)
        let totalDuration = Int(episodeData?.totalDuration ?? "0")
        let duration = (totalDuration ?? 0) / 60
        if duration < 1 {
            episodeDuration.text = String(totalDuration ?? 0) + " " + "s"
        } else {
            episodeDuration.text = String(((totalDuration ?? 0) / 60)) + " " + "m"
        }
        
        if episodeData != nil {
            let url = URL(string: episodeData?.boxCoverImage ?? "")
            if let _url = url {
                episodeImageView.alpha = (episodeImageView.image == nil) ? 0 : 1
                episodeImageView.kf.setImage(with: _url, completionHandler:  { result in
                    UIView.animate(withDuration: 1, animations: { self.episodeImageView.alpha = 1 })
                })
            } else {
                episodeImageView.image = nil
            }
        }
        
        if episodeData?.watchedDuration != nil && episodeData?.watchedDuration != episodeData?.totalDuration {
            watchedProgressView.isHidden = false
            let totalTime = Float(episodeData?.totalDuration ?? "0.0") ?? 0.0
            let watchedTime = Float(episodeData?.watchedDuration ?? "0.0") ?? 0.0
            let filledPercentage = watchedTime / totalTime
            print(filledPercentage)
            watchedProgressView.progress = filledPercentage
           // watchedProgressView.transform = watchedProgressView.transform.scaledBy(x: 1, y: 1.5)
            if filledPercentage > 0.99 {
                continueWatchImageView.image = UIImage(named: "replay")
            } else {
                continueWatchImageView.image = UIImage(named: "continueWatching")
            }
        } else if episodeData?.watchedDuration == episodeData?.totalDuration {
            let totalTime = Float(episodeData?.totalDuration ?? "0.0") ?? 0.0
            let watchedTime = Float(episodeData?.watchedDuration ?? "0.0") ?? 0.0
            let filledPercentage = watchedTime / totalTime
            print(filledPercentage)
            watchedProgressView.progress = filledPercentage
            continueWatchImageView.image = UIImage(named: "replay")
            watchedProgressView.isHidden = false
        } else {
            continueWatchImageView.image = UIImage(named: "continueWatching")
            watchedProgressView.isHidden = true
        }
        configureDetail()
        isConfigured = true

    }

    func height() -> CGFloat {
        if isConfigured == false {
            return UITableView.automaticDimension
        }
        layoutIfNeeded()
        return bounds.height
    }

    private func configureDetail() {
        episodeDetail.font = UIFont.skyTextFont(.regular, size: 15)
        let isExpanded = episode?.isDetailExpanded ?? false
        if !isExpanded {
            moreLessButton.setTitle("+More", for: .normal)
            episodeDetail.text = ""
        } else {
            moreLessButton.setTitle("-Less", for: .normal)
            episodeDetail.text = episode?.description
        }
        episodeDetail.setLblLineHeight(lineHeight: 4.04)
    }

    // MARK: - Actions
    @IBAction func moreLessButtonAction(_ sender: Any) {
        isConfigured = false
        episode?.isDetailExpanded = !(episode?.isDetailExpanded ?? false)
        delegate?.baEpisodeTableViewCell(self, didUpdateEpisode: episode, for: selectedIndex)
    }
}

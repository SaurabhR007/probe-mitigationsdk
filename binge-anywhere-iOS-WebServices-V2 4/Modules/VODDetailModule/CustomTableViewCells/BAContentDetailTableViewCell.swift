//
//  BAContentDetailTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Kingfisher
import TTTAttributedLabel
import AVKit
import AVFoundation
import Mixpanel

protocol BAContentDetailTableViewCellDelegate: class {
    func contentDetailTableViewCellDidExpand(_ cell: BAContentDetailTableViewCell, expanded: Bool)
    func scrollToSeasons()
    func noInternetConnection()
    func showAlert()
    func playTrailer(_ cell: BAContentDetailTableViewCell)
    func playFullScreenPlayer(contentType: String, isReplay: Bool?, contractName: String?, playerStateToPause: Bool?)
    func shareButtonTapped(on cell: BAContentDetailTableViewCell, with data: [Any])
    func watchlistButtonTapped(isAdded: Bool)
    //func configurePlayerView(containerView: UIView)
}

class BAContentDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var watchlistLabel: UILabel!
    @IBOutlet weak var favImageView: UIImageView!
    @IBOutlet weak var whatsappButton: UIButton!
    @IBOutlet weak var watchlistButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var watsappView: UIView!
    @IBOutlet weak var watchlistView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var shareButtonStackView: UIStackView!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var validityLeftLabel: UILabel!
    @IBOutlet weak var contentPosterTrailerView: UIView!
    @IBOutlet weak var contentPosterImage: UIImageView!
    @IBOutlet weak var contentTitleLabel: UILabel!
    @IBOutlet weak var contentPartnerLogo: UIImageView!
    @IBOutlet weak var contentGenreLabel: UILabel!
    @IBOutlet weak var contentSubtitleLabels: UILabel!
    @IBOutlet weak var contentAudioTypesLabel: UILabel!
    @IBOutlet weak var contentProducerLabel: UILabel!
    @IBOutlet weak var contentDirectorLabel: UILabel!
    @IBOutlet weak var contentStartCastLabel: UILabel!
    @IBOutlet weak var contentDetaillabel: ExpandableLabel!
    @IBOutlet weak var contentButtonStackView: UIStackView!
    @IBOutlet weak var contentGenreDetail: UILabel!
    weak var delegate: BAContentDetailTableViewCellDelegate?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var playButton: CustomButton = CustomButton(type: .system)
    var trailerButton: CustomButton = CustomButton(type: .system)
    var addRemoveWatchlistVM: BAAddRemoveWatchlistViewModal?
    private var player: BAPlayerViewController?
    var contentData: ContentDetailData?
    var buttonActionTitle = ""
    var isForReplay = false
    var isViewReloaded: Bool?
    var shouldReloadButtons = true
    var isAddedToFav: Bool = false
    var watchlistVodId: Int = 0
    let kCharacterBeforReadMore =  140
    let kReadMoreText           =  " +More"
    let kReadLessText           =  "-Less"
    var contentDescription = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        NotificationCenter.default.addObserver(self, selector: #selector(updateButtonStateToPlay), name: NSNotification.Name(rawValue: "PlayPausePlayerNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateButtonStateToPause), name: NSNotification.Name(rawValue: "PlayPlayerNotification"), object: nil)
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
      //  contentButtonStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        //shareButtonStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // *************************************************************
    //         Configure Content Detail Data Onto Cell
    // *************************************************************
    func configureContentData(contentDetail: ContentDetailData?, isExpanded: Bool = false, expiryTime: Int?, buttonTitle: String = "", shouldButtonsReload: Bool = true) {
        shouldReloadButtons = shouldButtonsReload
        buttonActionTitle = buttonTitle
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(contentType: contentDetail?.meta?.contentType, vodId: watchlistVodId)
        contentPartnerLogo.layer.borderWidth = 1.0
        contentPartnerLogo.layer.masksToBounds = false
        contentPartnerLogo.layer.borderColor = UIColor.clear.cgColor
        contentPartnerLogo.layer.cornerRadius = contentPartnerLogo.frame.size.width / 2
        contentPartnerLogo.clipsToBounds = true
        contentPartnerLogo.backgroundColor = .clear
        if contentDetail?.detail?.contractName == "RENTAL" {
            validityLeftLabel.isHidden = false
            expiryLabel.isHidden = false
            var timeLeft = 0
            if contentDetail?.meta?.purchaseExpiry != nil && contentDetail?.meta?.purchaseExpiry != 0 {
                timeLeft = convertTimestampDifference(timeStamp: "\(contentDetail?.meta?.purchaseExpiry ?? 0)")
                
            } else {
                timeLeft = convertTimestampDifference(timeStamp: "\(expiryTime ?? 0)")
            }
            if timeLeft < 60 && timeLeft > 0 {
                validityLeftLabel.text = String(timeLeft) + "min"
            } else if timeLeft > 60 {
                let timeLeftInHours = timeLeft / 60
                if timeLeftInHours > 48 {
                    validityLeftLabel.text = String(timeLeftInHours/24) + "days"
                } else {
                   validityLeftLabel.text = String(timeLeftInHours) + "hrs"
                }
                
            } else {
                validityLeftLabel.text = "EXPIRED"
            }
        } else {
            validityLeftLabel.isHidden = true
            expiryLabel.isHidden = true
        }
        var imageUrlString = ""
        var url = URL.init(string: "")
        switch contentDetail?.meta?.provider?.uppercased() {
        case ProviderType.prime.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoCircular ?? ""
        case ProviderType.sunnext.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoCircular ?? ""
        case ProviderType.shemaro.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoCircular ?? ""
        case ProviderType.vootKids.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoCircular ?? ""
        case ProviderType.vootSelect.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoCircular ?? ""
        case ProviderType.hungama.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoCircular ?? ""
        case ProviderType.zee.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoCircular ?? ""
        case ProviderType.hotstar.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoCircular ?? ""
        case ProviderType.eros.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoCircular ?? ""
        case ProviderType.sonyLiv.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoCircular ?? ""
        case ProviderType.curosityStream.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoCircular ?? ""
        case ProviderType.hopster.rawValue: contentPartnerLogo.image = UIImage(named: "hopster")
        case ProviderType.netflix.rawValue: contentPartnerLogo.image = UIImage(named: "netflix")
        default:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoCircular ?? "")
            imageUrlString = BAConfigManager.shared.configModel?.data?.config?.providerLogo?.TATASKY?.logoCircular ?? ""
        }
        
        if isViewReloaded ?? false {
        } else {
            setImage(with: contentPartnerLogo, url)
        }
        
        contentData = contentDetail
        if contentDetail?.meta?.contentType == ContentType.brand.rawValue {
            contentTitleLabel.text = contentDetail?.meta?.brandTitle
        } else if contentDetail?.meta?.contentType == ContentType.series.rawValue {
            contentTitleLabel.text = contentDetail?.meta?.seriesTitle
        } else if contentDetail?.meta?.contentType == ContentType.tvShows.rawValue {
            if contentDetail?.meta?.parentContentType == ContentType.brand.rawValue {
                contentTitleLabel.text = contentDetail?.meta?.brandTitle
            } else if contentDetail?.meta?.parentContentType == ContentType.series.rawValue {
                contentTitleLabel.text = contentDetail?.meta?.seriesTitle
            } else {
                contentTitleLabel.text = contentDetail?.meta?.vodTitle
            }
        } else {
            contentTitleLabel.text = contentDetail?.meta?.vodTitle
        }
        contentTitleLabel.numberOfLines = 2
        contentTitleLabel.lineBreakMode = .byWordWrapping
        contentSubtitleLabels.isHidden = true
        contentSubtitleLabels.text = "Subtitles: " + " - - "
        let contentGenre = " |" + " " + (contentDetail?.meta?.genre?[0] ?? "")
        let contentReleaseYear = contentDetail?.meta?.releaseYear ?? ""
        var contentRating = ""
        let contentSeconds = contentDetail?.meta?.duration ?? 0
        let hour = contentSeconds / 3600
        let min = (contentSeconds % 3600)/60
        var contentDuration = ""
        if hour > 0 && min > 0{
            contentDuration = " |" + " " + "\(hour)h \(min)m"
        }else if hour==0 && min > 0{
            contentDuration = " |" + " " + "\(min)m"
        }else{
            contentDuration = " |" + " " + "\(hour)h"
        }
        
        if contentDetail?.seriesList?.count ?? 0 > 0 {
            let seasonCount = contentDetail?.meta?.seasonCount ?? 1
            var selectedSeason = ""
            if seasonCount > 1 {
                selectedSeason = " |" + "  \(seasonCount)" + " Seasons"
            } else if seasonCount == 0 {
                selectedSeason = ""
            } else {
                selectedSeason = " |" + "  \(seasonCount)" + " Season"
            }
            if contentDetail?.meta?.rating?.count ?? 0 > 0 {
                contentRating  = " |" + " \(contentDetail?.meta?.rating ?? "")"
            } else {
                contentRating = ""
            }
            contentGenreDetail.text = contentReleaseYear + contentGenre + selectedSeason + contentRating
        } else {
            if contentDetail?.meta?.rating?.count ?? 0 > 0 {
                contentRating  = " |" + " \(contentDetail?.meta?.rating ?? "")"
            } else {
                contentRating = ""
            }
            if contentSeconds != 0 && contentSeconds != nil {
                contentGenreDetail.text = (contentDetail?.meta?.releaseYear ?? "") + contentGenre + contentDuration + contentRating
            } else {
                contentGenreDetail.text = (contentDetail?.meta?.releaseYear ?? "") + contentGenre + contentRating
            }
        }
        
        if contentDetail?.meta?.contentType == ContentType.series.rawValue {
            contentDescription = contentDetail?.meta?.seriesDescription ?? ""
        } else if contentDetail?.meta?.contentType == ContentType.brand.rawValue{
            contentDescription = contentDetail?.meta?.brandDescription ?? ""
        } else if contentDetail?.meta?.contentType == ContentType.tvShows.rawValue {
            if contentDetail?.meta?.parentContentType == ContentType.brand.rawValue {
                contentDescription = contentDetail?.meta?.description ?? ""
            } else if contentDetail?.meta?.parentContentType == ContentType.series.rawValue {
                contentDescription = contentDetail?.meta?.seriesDescription ?? ""
            } else {
                contentDescription = contentDetail?.meta?.description ?? ""
            }
        } else {
            contentDescription = contentDetail?.meta?.description ?? ""
        }
        
        contentDetaillabel.font = UIFont.skyTextFont(.regular, size: 15)
        contentDetaillabel.textColor = UIColor.white
        
        contentDetaillabel.collapsedAttributedLink =  NSAttributedString(string: kReadMoreText, attributes: [NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15), NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor])
        contentDetaillabel.expandedAttributedLink = NSAttributedString(string: kReadLessText ,attributes: [NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15), NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor])
        contentDetaillabel.ellipsis = nil
        self.layoutIfNeeded()
        
        contentDetaillabel.shouldCollapse = true
        contentDetaillabel.textReplacementType = .character
        contentDetaillabel.numberOfLines = 3
        contentDetaillabel.collapsed = !isExpanded
        print(contentDescription.length)
        
        contentDetaillabel.text = contentDescription
        
        var producerStr:String = ""
        var producerDetail:String=""
        
        if contentDetail?.meta?.producer?.count ?? 0 > 0 {
            if contentDetail?.meta?.producer?.count ?? 1 > 1 {
                producerStr = "Producers: "
            } else {
                producerStr = "Producer: "
            }
            if contentDetail?.meta?.producer?.first?.isEmpty ?? false {
                contentProducerLabel.isHidden = true
                contentProducerLabel.text! += contentDetail?.meta?.producer?.joined(separator: " | ") ?? ""
                
            } else {
                contentProducerLabel.isHidden = false
                producerDetail = contentDetail?.meta?.producer?.joined(separator: " | ") ?? ""
            }
        } else {
            contentProducerLabel.isHidden = true
            contentProducerLabel.text = "Producer: " + " - - "
        }
        
        let prodAttributedTxt = NSMutableAttributedString(string: producerStr, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 15)])
        prodAttributedTxt.append(NSAttributedString(string: producerDetail, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 15)]))
        contentProducerLabel.attributedText = prodAttributedTxt
        
        
        var directorStr:String = ""
        var directorDetail:String=""
        if contentDetail?.meta?.director?.count ?? 0 > 0 {
            if contentDetail?.meta?.director?.count ?? 1 > 1 {
                directorStr = "Directors: "
            } else {
                directorStr = "Director: "
            }
            if contentDetail?.meta?.director?.first?.isEmpty ?? false {
                contentDirectorLabel.isHidden = true
                contentDirectorLabel.text! += contentDetail?.meta?.director?.joined(separator: " | ") ?? ""
            } else {
                contentDirectorLabel.isHidden = false
                directorDetail = contentDetail?.meta?.director?.joined(separator: " | ") ?? ""
            }
        } else {
            contentDirectorLabel.isHidden = true
            contentDirectorLabel.text = "Director: " + " - - "
        }
        let directorAttributedTxt = NSMutableAttributedString(string: directorStr, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 15)])
        directorAttributedTxt.append(NSAttributedString(string: directorDetail, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 15)]))
        contentDirectorLabel.attributedText = directorAttributedTxt
        
        
        var castStr:String=""
        var castDetail:String=""
        if contentDetail?.meta?.actor?.count ?? 0 > 0 {
            castStr = "Starring: "
            if contentDetail?.meta?.actor?.first?.isEmpty ?? false {
                contentStartCastLabel.isHidden = true
                contentStartCastLabel.text! += contentDetail?.meta?.actor?.joined(separator: " | ") ?? ""
            } else {
                contentStartCastLabel.isHidden = false
                castDetail = contentDetail?.meta?.actor?.joined(separator: " | ") ?? ""
            }
        } else {
            contentStartCastLabel.isHidden = true
            contentStartCastLabel.text = "Starring: " + " - - "
        }
        let castAttributedTxt = NSMutableAttributedString(string: castStr, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 15)])
        castAttributedTxt.append(NSAttributedString(string: castDetail, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 15)]))
        contentStartCastLabel.attributedText = castAttributedTxt
        
        
        var audioStr:String = ""
        var audioDetail:String = ""
        if contentDetail?.meta?.audio?.count ?? 0 > 0 {
            audioStr = "Audio: "
            if contentDetail?.meta?.audio?.first?.isEmpty ?? false {
                contentAudioTypesLabel.isHidden = true
                contentAudioTypesLabel.text! += contentDetail?.meta?.audio?.joined(separator: " | ") ?? ""
            } else {
                contentAudioTypesLabel.isHidden = false
                audioDetail = contentDetail?.meta?.audio?.joined(separator: " | ") ?? ""
            }
        } else {
            contentAudioTypesLabel.isHidden = true
            contentAudioTypesLabel.text = "Audio: " + " - - "
        }
        let audioAttributedTxt = NSMutableAttributedString(string: audioStr, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 15)])
        audioAttributedTxt.append(NSAttributedString(string: audioDetail, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 15)]))
        contentAudioTypesLabel.attributedText = audioAttributedTxt
        
        
        var genreStr:String = ""
        var genreDetail:String = ""
        if contentDetail?.meta?.genre?.count ?? 0 > 0 {
            if contentDetail?.meta?.audio?.count ?? 1 > 1 {
                genreStr = "Genres: "
            } else {
                genreStr = "Genre: "
            }
            if contentDetail?.meta?.genre?.first?.isEmpty ?? false {
                contentGenreLabel.isHidden = true
                contentGenreLabel.text! += contentDetail?.meta?.genre?.joined(separator: " | ") ?? ""
            } else {
                contentGenreLabel.isHidden = false
                genreDetail = contentDetail?.meta?.genre?.joined(separator: " | ") ?? ""
            }
        } else {
            contentGenreLabel.isHidden = true
            contentGenreLabel.text = "Genres: " + " - - "
        }
        let genreAttributedTxt = NSMutableAttributedString(string: genreStr, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.medium, size: 15)])
        genreAttributedTxt.append(NSAttributedString(string: genreDetail, attributes: [NSAttributedString.Key.font:UIFont.skyTextFont(.regular, size: 15)]))
        contentGenreLabel.attributedText = genreAttributedTxt
        
        if contentDetail != nil {
            if isViewReloaded == nil {
                configureButtons(contentDetails: contentDetail, buttonTitle: buttonTitle)
                var url = URL.init(string: "")
                let cellWidth = Int(contentPosterImage.frame.width)
                let cellHeight = Int(contentPosterImage.frame.height)
                
                var imageString = ""
                if let image = contentDetail?.meta?.imageUrlApp {
                    imageString = image
                    let imageUrl = imageString
                    let cloudnaryUrl = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
                    url = URL.init(string: cloudnaryUrl ?? "")
                    print("Image Url Value is1 ------------>", imageString)
                } else {
                    imageString = contentDetail?.meta?.boxCoverImage ?? ""
                    let imageUrl = imageString
                    let cloudnaryUrl = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
                    url = URL.init(string: cloudnaryUrl ?? "")
                    print("Image Url Value is2 ------------>", imageString)
                }
                print("Image Url Value is ------------>", imageString)
                setImage(with: contentPosterImage, url)
            } else if isViewReloaded ?? false {
                configureButtons(contentDetails: contentDetail, buttonTitle: buttonTitle)
            } else {
                configureButtons(contentDetails: contentDetail, buttonTitle: buttonTitle)
            }
        }
        watsappView.isHidden = !isWhatsappInstalled()
    }
    
    func fetchWatchlistLookUp(contentType: String?, vodId: Int?) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType ?? "", contentId: String(vodId ?? 0))
        confgureWatchlistIcon()
    }
    
    func confgureWatchlistIcon() {
        if let dataModel = watchlistLookupVM {
            //showActivityIndicator(isUserInteractionEnabled: false)
            dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                //hideActivityIndicator()
                if flagValue {
                    self.contentData?.lastWatch = dataModel.watchlistLookUp?.data
                    //self.configureLastWatchButtonStates(contentDetails: self.contentData!, buttonTitle: self.buttonActionTitle)
                    if dataModel.watchlistLookUp?.data?.isFavourite ?? false {
                        self.isAddedToFav = true
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addFav"), object: nil)
                        self.delegate?.watchlistButtonTapped(isAdded: self.isAddedToFav)
                        self.favImageView.image = UIImage(named: "favIconNew")
                        self.watchlistLabel.text = "Watchlist"
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeFav"), object: nil)
                        self.isAddedToFav = false
                        self.delegate?.watchlistButtonTapped(isAdded: self.isAddedToFav)
                        self.favImageView.image = UIImage(named: "fav")
                        self.watchlistLabel.text = "Watchlist"
                    }
                } else if isApiError {
                    // Do Nothing
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    func configureLastWatchButtonStates(contentDetails: ContentDetailData?, buttonTitle: String) {
        UtilityFunction.shared.performTaskInMainQueue {
            if contentDetails?.meta?.contentType == ContentType.brand.rawValue || contentDetails?.meta?.contentType == ContentType.customBrand.rawValue || contentDetails?.meta?.contentType == ContentType.tvShows.rawValue {
                if contentDetails?.meta?.contentType == ContentType.tvShows.rawValue && (contentDetails?.meta?.parentContentType == ContentType.series.rawValue || contentDetails?.meta?.parentContentType == ContentType.vod.rawValue) {
                    self.configureLatWatchStatesForVOD(contentDetails: contentDetails, buttonTitle: buttonTitle)
                } else {
                    self.configureLastWatchStatesForOthers(contentDetails: contentDetails, buttonTitle: buttonTitle)
                }
            } else if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                self.configureLastWatchStatesForOthers(contentDetails: contentDetails, buttonTitle: buttonTitle)
            } else {
                self.configureLatWatchStatesForVOD(contentDetails: contentDetails, buttonTitle: buttonTitle)
            }
        }
    }
    
    func configureLatWatchStatesForVOD(contentDetails: ContentDetailData?, buttonTitle: String) {
        if contentDetails?.lastWatch != nil {
            let totalTime = Float(contentDetails?.lastWatch?.durationInSeconds ?? 0)
            let watchedTime = Float(contentDetails?.lastWatch?.secondsWatched ?? 1)
            let getWatchedPercent = (watchedTime) / (totalTime)
            if getWatchedPercent > 0.99 {
                isForReplay = true
                if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                    if buttonTitle == "" {
                        playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                    } else {
                        playButton.selectedButton(title: buttonTitle , iconName: "")
                    }
                } else {
                    if buttonTitle == "" {
                        playButton.selectedButton(title: "Replay", iconName: "btnReplay")
                    } else {
                        playButton.selectedButton(title: buttonTitle , iconName: "")
                    }
                }
            } else {
                isForReplay = false
                if buttonTitle == "Replay" {
                    playButton.selectedButton(title: "Replay", iconName: "btnReplay")
                } else {
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue || contentDetails?.meta?.parentContentType == ContentType.series.rawValue {
                        if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                            playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                        } else {
                            playButton.selectedButton(title: "Play" , iconName: "play")
                        }
                    } else {
                        if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                            playButton.selectedButton(title: "Resume", iconName: "play")
                        } else {
                            playButton.selectedButton(title: "Play" , iconName: "play")
                        }
                    }
                }
            }
        } else {
            isForReplay = false
            playButton.selectedButton(title: "Play", iconName: "play")
        }
    }
    
    func configureLastWatchStatesForOthers(contentDetails: ContentDetailData?, buttonTitle: String) {
        if contentDetails?.lastWatch != nil {
            let watchedTime = Float(contentDetails?.lastWatch?.secondsWatched ?? 0)
            let totalTime = Float(contentDetails?.lastWatch?.durationInSeconds ?? 1)
            let getWatchedPercent = (watchedTime) / (totalTime)
            if getWatchedPercent > 0.99 {
                isForReplay = true
                if contentDetails?.meta?.contentType == ContentType.brand.rawValue {
                    if buttonTitle == "" {
                        playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                    } else {
                        playButton.selectedButton(title: buttonTitle , iconName: "")
                    }
                } else {
                    if buttonTitle == "" {
                        playButton.selectedButton(title: "Replay", iconName: "btnReplay")
                    } else {
                        playButton.selectedButton(title: buttonTitle , iconName: "")
                    }
                }
            } else {
                isForReplay = false
                if contentDetails?.lastWatch?.season != nil {
                    if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                        if contentDetails?.lastWatch?.season == 0 {
                            playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                        } else {
                            playButton.selectedButton(title: "Resume S\(contentDetails?.lastWatch?.season ?? 0) E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                        }
                    } else {
                        playButton.selectedButton(title: "Play" , iconName: "play")
                    }
                } else {
                    if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                        playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                    } else {
                        playButton.selectedButton(title: "Play" , iconName: "play")
                    }
                }
            }
        } else {
            if buttonTitle == "" {
                isForReplay = false
                playButton.selectedButton(title: "Play", iconName: "play")
            } else {
                playButton.selectedButton(title: buttonTitle , iconName: "")
            }
        }
    }
    
    private func setImage(with imageView: UIImageView, _ url: URL?) {
        UtilityFunction.shared.performTaskInMainQueue {
            if let _url = url {
                imageView.alpha = (imageView.image == nil) ? 0 : 1
                imageView.kf.setImage(with: _url, completionHandler:  { result in
                    switch result {
                    case .success(let value):
                        break
//                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("BAContentDetailTableViewCell Error: \(error)")
                    }
                    UIView.animate(withDuration: 1, animations: { imageView.alpha = 1 })
                })
            } else {
                imageView.image = nil
            }
        }
    }
    
    // MARK: Configure Player For VOD Content
    func configurePlayer(_ player: BAPlayerViewController) {
        let addedPlayers = contentPosterTrailerView.subviews.filter { $0 is BAPlayerView }
        if addedPlayers.isEmpty { //Player is not already added
            contentPosterTrailerView.addSubview(player.view)
            player.view.fillParentView()
            //contentPosterImage.image = nil
        }
        if contentData?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                CustomLoader.shared.hideLoader(on: self.contentPosterTrailerView)
            }
        } else {
            CustomLoader.shared.hideLoader(on: self.contentPosterTrailerView)
        }
        
    }
    
    // *********************************************
    //     Configure Buttons in Stack View
    // *********************************************
    func configureButtons(contentDetails: ContentDetailData?, buttonTitle: String?) {
        if shouldReloadButtons {
            UtilityFunction.shared.performTaskInMainQueue {
                if contentDetails?.meta?.contentType == ContentType.brand.rawValue || contentDetails?.meta?.contentType == ContentType.customBrand.rawValue || contentDetails?.meta?.contentType == ContentType.tvShows.rawValue {
                    if contentDetails?.meta?.contentType == ContentType.tvShows.rawValue && (contentDetails?.meta?.parentContentType == ContentType.series.rawValue || contentDetails?.meta?.parentContentType == ContentType.vod.rawValue) {
                        self.configureButtonForVOD(contentDetails: contentDetails, buttonTitle: buttonTitle)
                    } else {
                        self.configureButtonsForOthers(contentDetails: contentDetails, buttonTitle: buttonTitle)
                    }
                } else if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                    self.configureButtonsForOthers(contentDetails: contentDetails, buttonTitle: buttonTitle)
                } else {
                    self.configureButtonForVOD(contentDetails: contentDetails, buttonTitle: buttonTitle)
                }
            }
        } else {
            
        }
    }
    
    // MARK: Configure Buttons for Brands and Series
    func configureButtonsForOthers(contentDetails: ContentDetailData?, buttonTitle: String?) {

        playButton = CustomButton(type: .system)
        playButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        playButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        playButton.backgroundColor = UIColor.BABlueColor
        if contentDetails?.lastWatch != nil {
            //            let watchedTime = Float(contentDetails?.lastWatch?.watchedDuration ?? 0)
            //            let totalTime = Float(contentDetails?.lastWatch?.totalDuration ?? 1)
            let watchedTime = Float(contentDetails?.lastWatch?.secondsWatched ?? 0)
            let totalTime = Float(contentDetails?.lastWatch?.durationInSeconds ?? 1)
            let getWatchedPercent = (watchedTime) / (totalTime)
            if getWatchedPercent > 0.99 {
                isForReplay = true
                if contentDetails?.meta?.contentType == ContentType.brand.rawValue {
                    if buttonTitle == "" {
                        playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                    } else {
                        playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                    }
                } else {
                    if buttonTitle == "" {
                        playButton.selectedButton(title: "Replay", iconName: "btnReplay")
                    } else {
                        playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                    }
                }
            } else {
                isForReplay = false
                if contentDetails?.lastWatch?.season != nil {
                    if buttonTitle == "" {
                        if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                            if contentDetails?.lastWatch?.season == 0 {
                                playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            } else {
                                playButton.selectedButton(title: "Resume S\(contentDetails?.lastWatch?.season ?? 0) E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            }
                        } else {
                            playButton.selectedButton(title: "Play", iconName: "play")
                        }
                    } else {
                        playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                    }
                } else {
                    if buttonTitle == "" {
                        if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                            playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                        } else {
                            playButton.selectedButton(title: "Play", iconName: "play")
                        }
                    } else {
                        playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                    }
                }
            }
        } else {
            if buttonTitle == "" {
                isForReplay = false
                playButton.selectedButton(title: "Play", iconName: "play")
            } else {
                playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
            }
        }        
        print("View Loaded Button Count")
        if contentData?.meta?.parentContentType == ContentType.brand.rawValue {
            playButton.setTitleColor(.white, for: .normal)
            playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
            playButton.layer.cornerRadius = 22
            
            trailerButton = CustomButton(type: .system)
            trailerButton.backgroundColor = UIColor.BAGreyButtonColor
            trailerButton.setTitle("View Seasons", for: .normal)
            trailerButton.setTitleColor(.white, for: .normal)
            trailerButton.addTarget(self, action: #selector(watchTrailer), for: .touchUpInside)
            trailerButton.layer.cornerRadius = 22
            contentButtonStackView.addArrangedSubview(playButton)
            contentButtonStackView.addArrangedSubview(trailerButton)
        } else {
            playButton.setTitleColor(.white, for: .normal)
            playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
            playButton.layer.cornerRadius = 22
            contentButtonStackView.addArrangedSubview(playButton)
        }
    }
    
    // MARK: Calculate Difference between current and API timestamp
    func convertTimestampDifference(timeStamp: String?)  -> Int{
        let currentTime = Date().currentTimeMillis()
        let timeDifference = ((Int64(timeStamp ?? "0") ?? 0) - currentTime) / 60000
        return Int(timeDifference)
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            let text = "Watch \(contentData?.meta?.vodTitle ?? "") on Tata Sky Binge!. \n \(UtilityFunction.shared.isUatBuild() ? "https://Binge-uat.tatasky.com/detail/\(contentData?.meta?.contentType ?? "")/\(watchlistVodId)" : "https://tataskybinge.com/detail/\(contentData?.meta?.contentType ?? "")/\(watchlistVodId)")"
            let textToShare: [Any] = [ text ]
            delegate?.shareButtonTapped(on: self, with: textToShare)
        } else {
            delegate?.noInternetConnection()
        }
    }
    
    @IBAction func watchlistButtonAction(_ sender: Any) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        if BAReachAbility.isConnectedToNetwork() {
            var vodId = 0
            let contentType = contentData?.meta?.contentType
            switch contentType {
            case ContentType.series.rawValue:
                vodId = contentData?.meta?.seriesId ?? 0
            case ContentType.brand.rawValue:
                vodId = contentData?.meta?.brandId ?? 0
            default:
                vodId = contentData?.meta?.vodId ?? 0
            }
            //configureLearnAction(vodId: vodId)
            addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentType ?? "", contentId: vodId)
            addRemoveFromWatchlist()
        } else {
            delegate?.noInternetConnection()
        }
    }
    
    // MARK: AddRemoveToWatchlist
    func addRemoveFromWatchlist() {
        if let dataModel = addRemoveWatchlistVM {
            dataModel.addRemoveWatchlist {(flagValue, message, isApiError) in
                if flagValue {
                    var title = ""
                    if self.contentData?.meta?.contentType == ContentType.brand.rawValue {
                        title = self.contentData?.meta?.brandTitle ?? ""
                    } else if self.contentData?.meta?.contentType == ContentType.series.rawValue {
                        title = self.contentData?.meta?.seriesTitle ?? ""
                    } else if self.contentData?.meta?.contentType == ContentType.tvShows.rawValue {
                        title = self.contentData?.meta?.vodTitle ?? ""
                    } else {
                        title = self.contentData?.meta?.vodTitle ?? ""
                    }
                    if self.isAddedToFav {
                        self.isAddedToFav = false
                        self.delegate?.watchlistButtonTapped(isAdded: self.isAddedToFav)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeFav"), object: nil)
                        let genreResult = self.contentData?.meta?.genre?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentData?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentData?.meta?.provider ?? ""])
                        self.favImageView.image = UIImage(named: "fav")
                        self.watchlistLabel.text = "Watchlist"
                        self.makeToast(message: "Removed from Watchlist", imageName: "WatchlistActive")
                        let delay = 1.0
                        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    } else {
                        self.isAddedToFav = true
                        let genreResult = self.contentData?.meta?.genre?.joined(separator: ",")
                        let languageResult = self.contentData?.meta?.audio?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentData?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        self.delegate?.watchlistButtonTapped(isAdded: self.isAddedToFav)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addFav"), object: nil)
                        self.favImageView.image = UIImage(named: "favIconNew")
                        self.watchlistLabel.text = "Watchlist"
                        self.makeToast(message: "Added to Watchlist", imageName: "WatchlistActive")
                        let delay = 1.0
                        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                        //self.mixPanelEvent()
                    }
                } else if isApiError {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                }
            }
        }
    }
    
    func makeToast(message: String, imageName: String) {
        //UIApplication.shared.beginIgnoringInteractionEvents()
        kAppDelegate.window?.makeToast(message, duration: 1.0, point: .bottom, title: "", image: UIImage.init(named: imageName), toastYPosition: tabBarHeight) { _ in
        }
    }
    
    func configurePlayPauseButton(buttonTitle: String) {
        playButton.selectedButton(title: buttonTitle, iconName: "")
    }
    
    func isWhatsappInstalled() -> Bool {
        if let whatsappLink = URL(string: "whatsapp://") {
            if UIApplication.shared.canOpenURL(whatsappLink) {
                return true
            }
            return false
        }
        return false
    }
    
    @objc func updateButtonStateToPause(gestureRecognizer: UIGestureRecognizer) {
        self.playButton.selectedButton(title: "Pause", iconName: "")
    }
    
    @objc func updateButtonStateToPlay(gestureRecognizer: UIGestureRecognizer) {
        self.playButton.selectedButton(title: "Play", iconName: "")
    }
    
    @IBAction func whatsAppButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            let genreResult = contentData?.meta?.genre?.joined(separator: ",")
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.shareWatsapp.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: contentData?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? ""])
            let text = "Watch \(contentData?.meta?.vodTitle ?? "") on Tata Sky Binge!. \n \(UtilityFunction.shared.isUatBuild() ? "https://Binge-uat.tatasky.com/detail/" : "https://tataskybinge.com/detail/")\(contentData?.meta?.contentType ?? "")/\(watchlistVodId)"//"This is some text that I want to share, find it on "
            let urlConvertedText: String = text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            //let urlString = "https://www.apple.com"
            if isWhatsappInstalled() {
                if let whatsappShareLink = URL(string: "whatsapp://send?text=\(urlConvertedText)"), UIApplication.shared.canOpenURL(whatsappShareLink) {
                    UIApplication.shared.open(whatsappShareLink, options: [:], completionHandler: nil)
                }
            }
        } else {
            delegate?.noInternetConnection()
        }
    }
    
    // MARK: Configure Buttons For VOD
    func configureButtonForVOD(contentDetails: ContentDetailData?, buttonTitle: String?) {
        //if playButton.
//        if shouldReloadButtons {
//            contentButtonStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
//        }
        //contentButtonStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        playButton = CustomButton(type: .custom)
        let spacing: CGFloat = 10
        playButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing)
        playButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0)
        
        if BAKeychainManager().isAutoPlayOn == false {
            if contentDetails?.lastWatch != nil {
                let watchedTime = Float(contentDetails?.lastWatch?.watchedDuration ?? 0)
                let totalTime = Float(contentDetails?.lastWatch?.totalDuration ?? 1)
                let getWatchedPercent = (watchedTime) / (totalTime)
                if getWatchedPercent > 0.99 {
                    isForReplay = true
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                        if buttonTitle == "" {
                            playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    } else {
                        if buttonTitle == "" {
                            playButton.selectedButton(title: "Replay", iconName: "btnReplay")
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    }
                } else {
                    isForReplay = false
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue || contentDetails?.meta?.parentContentType == ContentType.series.rawValue {
                        if buttonTitle == "" {
                            if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                                playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            } else {
                                playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            }
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    } else {
                        if buttonTitle == "" {
                            if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                                playButton.selectedButton(title: "Resume", iconName: "play")
                            } else {
                                playButton.selectedButton(title: "Play", iconName: "play")
                            }
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    }
                }
                playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
                playButton.layer.cornerRadius = 22
                if contentDetails?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil){
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue || contentDetails?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.eros.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else {
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                        contentButtonStackView.addArrangedSubview(playButton)
                    } else {
                        contentButtonStackView.addArrangedSubview(playButton)
                        if contentData?.meta?.provider?.uppercased() == ProviderType.zee.rawValue || contentData?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue || contentData?.detail?.contractName == "RENTAL" {
                            // Do not add trailer button
                        } else {
                            trailerButton = CustomButton(type: .system)
                            trailerButton.backgroundColor = UIColor.BAGreyButtonColor
                            trailerButton.setTitle("Watch Trailer", for: .normal)
                            trailerButton.setTitleColor(.white, for: .normal)
                            trailerButton.addTarget(self, action: #selector(watchTrailer), for: .touchUpInside)
                            trailerButton.layer.cornerRadius = 22
                            contentButtonStackView.addArrangedSubview(trailerButton)
                        }
                    }
                }
            } else {
                isForReplay = false
                playButton.selectedButton(title: "Play", iconName: "play")
                playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
                playButton.layer.cornerRadius = 22
                if contentDetails?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue || contentDetails?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else if contentDetails?.meta?.provider?.uppercased() == ProviderType.eros.rawValue && (contentDetails?.meta?.partnerTrailerInfo?.isEmpty ?? false || contentDetails?.meta?.partnerTrailerInfo == nil) {
                    contentButtonStackView.addArrangedSubview(playButton)
                } else {
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                        contentButtonStackView.addArrangedSubview(playButton)
                    } else {
                        contentButtonStackView.addArrangedSubview(playButton)
                        if contentData?.meta?.provider?.uppercased() == ProviderType.zee.rawValue || contentData?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue || contentData?.detail?.contractName == "RENTAL" {
                            // Do not add trailer button
                        } else {
                            trailerButton = CustomButton(type: .system)
                            trailerButton.backgroundColor = UIColor.BAGreyButtonColor
                            trailerButton.setTitle("Watch Trailer", for: .normal)
                            trailerButton.setTitleColor(.white, for: .normal)
                            trailerButton.addTarget(self, action: #selector(watchTrailer), for: .touchUpInside)
                            trailerButton.layer.cornerRadius = 22
                            contentButtonStackView.addArrangedSubview(trailerButton)
                        }
                    }
                }
            }
        } else {
            if contentDetails?.lastWatch != nil {
                let watchedTime = Float(contentDetails?.lastWatch?.watchedDuration ?? 0)
                let totalTime = Float(contentDetails?.lastWatch?.totalDuration ?? 1)
                let getWatchedPercent = (watchedTime) / (totalTime)
                if getWatchedPercent > 0.99 {
                    isForReplay = true
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue {
                        if buttonTitle == "" {
                            if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                                playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            } else {
                                playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            }
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    } else {
                        if buttonTitle == "" {
                            playButton.selectedButton(title: "Replay", iconName: "btnReplay")
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    }
                } else {
                    isForReplay = false
                    if contentDetails?.meta?.contentType == ContentType.series.rawValue || contentDetails?.meta?.parentContentType == ContentType.series.rawValue  {
                        if buttonTitle == "" {
                            if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                                playButton.selectedButton(title: "Resume E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            } else {
                                playButton.selectedButton(title: "Play E\(contentDetails?.lastWatch?.episodeId ?? 1)", iconName: "play")
                            }
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    } else {
                        if buttonTitle == "" {
                            if contentDetails?.lastWatch?.secondsWatched ?? 0 > 0 {
                                playButton.selectedButton(title: "Resume", iconName: "play")
                            } else {
                                playButton.selectedButton(title: "Play", iconName: "play")
                            }
                        } else {
                            playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                        }
                    }
                }
            } else {
                if buttonTitle == "" {
                    isForReplay = false
                    playButton.selectedButton(title: "Play", iconName: "play")
                } else {
                    playButton.selectedButton(title: buttonTitle ?? "", iconName: "")
                }
                
            }
            playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
            playButton.layer.cornerRadius = 22
            contentButtonStackView.addArrangedSubview(playButton)
        }
    }
    
    @objc func playButtonAction() {
        let playButtonTitle = playButton.currentTitle ?? ""
        if playButtonTitle == "Play Now" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PlayNowState"), object: nil)
        } else {
            if playButtonTitle == "Play" || playButtonTitle == "Replay" || playButtonTitle.contains("Resume") {
                if playButtonTitle == "Replay" {
                    delegate?.playFullScreenPlayer(contentType: contentData?.meta?.contentType ?? "", isReplay: true, contractName: contentData?.detail?.contractName, playerStateToPause: false)
                } else {
                    delegate?.playFullScreenPlayer(contentType: contentData?.meta?.contentType ?? "", isReplay: isForReplay, contractName: contentData?.detail?.contractName, playerStateToPause: false)
                }
            } else {
                delegate?.playFullScreenPlayer(contentType: contentData?.meta?.contentType ?? "", isReplay: isForReplay, contractName: contentData?.detail?.contractName, playerStateToPause: true)
            }
        }
    }
    
    @objc func watchTrailer() {
        if contentData?.meta?.contentType == ContentType.brand.rawValue || contentData?.meta?.contentType == ContentType.series.rawValue || contentData?.meta?.contentType == ContentType.tvShows.rawValue {
            delegate?.scrollToSeasons()
        } else {
            if BAReachAbility.isConnectedToNetwork() {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name?.uppercased() == contentData?.meta?.provider?.uppercased()}) ?? false {
                    let genreResult = self.contentData?.meta?.genre?.joined(separator: ",")
                    let langugaeResult = self.contentData?.meta?.audio?.joined(separator: ",")
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playTrailer.rawValue, properties: [MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? "", MixpanelConstants.ParamName.contentTitle.rawValue: contentData?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.subscribed.rawValue: "Yes", MixpanelConstants.ParamName.contentLanguage.rawValue: langugaeResult ?? ""])
                } else {
                    let genreResult = self.contentData?.meta?.genre?.joined(separator: ",")
                    let langugaeResult = self.contentData?.meta?.audio?.joined(separator: ",")
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playTrailer.rawValue, properties: [MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? "", MixpanelConstants.ParamName.contentTitle.rawValue: contentData?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.subscribed.rawValue: "No", MixpanelConstants.ParamName.contentLanguage.rawValue: langugaeResult ?? ""])
                }
                delegate?.playTrailer(self)
            } else {
                delegate?.showAlert()
            }
        }
        
    }
}

extension BAContentDetailTableViewCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWithTransitInformation components: [AnyHashable: Any]!) {
        if let _ = components as? [String: String] {
            if let value = components["ReadMore"] as? String, value == "1" {
                delegate?.contentDetailTableViewCellDidExpand(self, expanded: true)
            }
            if let value = components["ReadLess"] as? String, value == "1" {
                delegate?.contentDetailTableViewCellDidExpand(self, expanded: false)
            }
        }
    }
}

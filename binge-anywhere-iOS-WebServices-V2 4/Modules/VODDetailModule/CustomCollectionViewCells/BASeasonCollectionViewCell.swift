//
//  BASeasonCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BASeasonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var seasonlabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        // Initialization code
    }

    override var isSelected: Bool {
        didSet {
            self.seasonlabel.textColor = .white
            self.selectedView.isHidden = false
        }
    }

    func configureSeasonsList(seriesList: SeriesYearsList?) {
        seasonlabel.text = seriesList?.seriesName
    }
    
    func configureSeasonsListForSeries(title: String?) {
        seasonlabel.text = title
    }
}

//
//  BAZeePlayerViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit
import WebKit

protocol BAZeePlayerViewControllerDelegate: class {
    func zeePlayerDismissed()
}

class BAZeePlayerViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var zeeWebView: UIWebView!
    var playableUrl: String?
    weak var delegate: BAZeePlayerViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .BAdarkBlueBackground
        zeeWebView.isOpaque = false
        zeeWebView.delegate = self
        configureWebView()
        configureNavigation()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let cookies = HTTPCookieStorage.shared.cookies else {
            return
        }
        print(cookies)
    }
    
    func configureNavigation() {
        
        super.configureNavigationBar(with: .backWithText, #selector(backButtonAction), nil, self, "Zee5")
        self.navigationController?.navigationBar.barTintColor = .clear
    }

    func configureWebView() {
        zeeWebView.backgroundColor = .BAdarkBlueBackground
        let url = URL (string: playableUrl ?? "")
        let requestObj = URLRequest(url: url!)
        zeeWebView.loadRequest(requestObj)
        //zeeWebView.stringByEvaluatingJavaScript(from: playableUrl ?? "")
        //print("Javascript--->", zeeWebView.stringByEvaluatingJavaScript(from: playableUrl ?? ""))
    }


    func webViewDidStartLoad(_ webView: UIWebView) {
        //showActivityIndicator(isUserInteractionEnabled: false)
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        hideActivityIndicator()
        let scrollableSize = CGSize(width: view.frame.size.width, height: webView.scrollView.contentSize.height)
        self.zeeWebView?.scrollView.contentSize = scrollableSize
    }

    @objc func backButtonAction() {
        delegate?.zeePlayerDismissed()
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

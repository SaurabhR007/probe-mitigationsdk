//
//  BAAppsDetailViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

class BAAppsDetailViewController: BaseViewController {
    
    @IBOutlet weak var appDetailtableView: UITableView!
    var appContentData: HomeScreenDataModel?
    var homeViewModel: HomeVM?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var contentDetail: ContentDetailData?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var parentNavigation: UINavigationController?
    var continueWatchingVM: BAContinueWatchingViewModal?
    var appName: String?
    var pageType: String?
    var isSubscribed: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        registerCells()
        conformDelegates()
        appDetailtableView.backgroundColor = .BAdarkBlueBackground
        self.title = appName
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mixPanelEvent()
        if appContentData?.data?.items?.isEmpty ?? false {
           showHideWaterMark()
        } else {
            self.updateContinueWatchData()
        }
        
    }
    
    func registerCells() {
        UtilityFunction.shared.registerCell(appDetailtableView, kBAAppDetailTableViewCell)
        UtilityFunction.shared.registerCell(appDetailtableView, kBAHeroBannerTableViewCell)
        UtilityFunction.shared.registerCell(appDetailtableView, kBAAppPosterTableViewCell)
        UtilityFunction.shared.registerCell(appDetailtableView, kBAHomeRailTableViewCell)
    }
    
    func mixPanelEvent() {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homePageView.rawValue, properties: [MixpanelConstants.ParamName.name.rawValue: "\(self.title ?? "") - Home"])
    }
    
    func conformDelegates() {
        appDetailtableView.delegate = self
        appDetailtableView.dataSource = self
    }
    
    // MARK: - Functions
    private func setNavigationBar() {
        super.configureNavigationBar(with: .backButtonAndCentreImage, #selector(backButtonAction), nil, self, "", appName ?? "")
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func updateContinueWatchData() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            if let dataModel = homeViewModel {
                let group = DispatchGroup()
                group.enter()
                dataModel.checkForContinueWatchData(group: group, {
                    group.leave()
                })
                group.notify(queue: .main) {
                    dataModel.omitBlankContentSectionRail()
                    let index = dataModel.homeScreenData?.data?.items?.firstIndex(where: { (data) -> Bool in
                        return data.sectionSource == "CONTINUE_WATCHING"
                    })
                    UtilityFunction.shared.performTaskInMainQueue {
                        if let index = index {
                            let indexPath = IndexPath.init(row: index, section: 0)
                            if (self.appDetailtableView.cellForRow(at: indexPath) as? BAHomeRailTableViewCell) != nil {
                                self.appDetailtableView.reloadRows(at: [indexPath], with: .none)
                            } else {
                                self.showHideWaterMark()
                            }
                        }
                    }
                }
            } else {
                // Do Nothing
                showHideWaterMark()
            }
        } else {
            noInternet()
        }
    }
    
    func showHideWaterMark() {
        if appContentData?.data?.items?.isEmpty ?? true {
            appDetailtableView.addWatermark(with: "No Content available to watch")
        } else {
            appDetailtableView.addWatermark(with: "")
        }
        
        if #available(iOS 11.0, *) {
            appDetailtableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }
}

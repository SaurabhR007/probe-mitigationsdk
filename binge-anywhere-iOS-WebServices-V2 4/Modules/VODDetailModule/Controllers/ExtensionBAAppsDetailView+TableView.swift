//
//  ExtensionBAAppsDetailView+TableView.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension BAAppsDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appContentData?.data?.items?.isEmpty ?? false {
            return 0
        } else {
            return  appContentData?.data?.items?.count ?? 0
//            if isSubscribed ?? false {
//                return  appContentData?.data?.items?.count ?? 0
//            } else {
//                if section == 2 {
//                    return (appContentData?.data?.items?.count ?? 0) - 1
//                } else {
//                    return 1
//                }
//            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if appContentData?.data?.items?.isEmpty ?? false {
            return 0
        } else {
            return 1
//            if isSubscribed ?? false {
//                return 1
//            } else {
//                return 3
//            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = appContentData?.data?.items![indexPath.row]
       // if isSubscribed ?? false {
            if item?.sectionType == RailType.hero.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: kBAHeroBannerTableViewCell) as! BAHeroBannerTableViewCell
                cell.isPartnerHomePage = true
                cell.bannerItem =  appContentData?.data?.items![indexPath.row] //homeViewModel?.homeScreenData?.data?.items?[indexPath.row]
                cell.delegate = self
                cell.setup()
                return cell
            } else {
                guard let data = appContentData?.data else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell) as! BAHomeRailTableViewCell
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell) as! BAHomeRailTableViewCell
                cell.isPartnerHomePage = true
                cell.delegate = self
                cell.railPosition = indexPath.row
                cell.seeAllButton.tag = indexPath.row
                cell.configureCell((data.items?[indexPath.row])!)
                cell.backgroundColor = .BAdarkBlueBackground
                return cell
            }
//        } else {
//            switch indexPath.section {
//            case 0:
//                let count = appContentData?.data?.items?.count ?? 0
//                if count > 0 {
//                    let cell = tableView.dequeueReusableCell(withIdentifier: kBAHeroBannerTableViewCell) as! BAHeroBannerTableViewCell
//                    cell.isPartnerHomePage = true
//                    cell.bannerItem = appContentData?.data?.items![indexPath.row] //homeViewModel?.homeScreenData?.data?.items?[indexPath.row]
//                    cell.delegate = self
//                    cell.setup()
//                    return cell
//                } else {
//                    let cell = tableView.dequeueReusableCell(withIdentifier: kBAAppPosterTableViewCell) as! BAAppPosterTableViewCell
//                    cell.configureAppPoster(appName: appName)
//                    cell.backgroundColor = .BAdarkBlueBackground
//                    return cell
//                }
//            case 1:
//                let cell = tableView.dequeueReusableCell(withIdentifier: kBAAppDetailTableViewCell) as! BAAppDetailTableViewCell
//                cell.configureAppInfo(appName: appName)
//                cell.delegate = self
//                cell.backgroundColor = .BAdarkBlueBackground
//                return cell
//            case 2:
//                let cell = tableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell) as! BAHomeRailTableViewCell
//                cell.delegate = self
//                cell.railPosition = indexPath.row + 1
//                cell.seeAllButton.tag = indexPath.row + 1
//                cell.configureCell((appContentData?.data?.items?[indexPath.row+1])!)
//                cell.backgroundColor = .BAdarkBlueBackground
//                return cell
//            default:
//                return UITableViewCell()
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = appContentData?.data?.items?[indexPath.row]
        if !(isSubscribed ?? true) && indexPath.section == 0 && (appContentData?.data?.items?.count ?? 0) == 0 {
            return UITableView.automaticDimension // BAAppPosterTableViewCell
        } else if item?.sectionType == RailType.hero.rawValue && indexPath.section == 0 {
            return ScreenSize.screenWidth * (9/16) // BAHeroBannerTableViewCell
        } else if item?.layoutType == LayoutType.landscape.rawValue {
            return 251 // BAHomeRailTableViewCell
        } else if item?.layoutType == LayoutType.potrait.rawValue {
            return 330 // BAHomeRailTableViewCell
        } else {
            return UITableView.automaticDimension
        }
    }
}


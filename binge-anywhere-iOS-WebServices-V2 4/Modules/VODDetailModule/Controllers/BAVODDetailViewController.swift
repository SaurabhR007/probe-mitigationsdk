//
//  BAVODDetailViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 26/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Mixpanel
import CommonCrypto
import ARSLineProgress
import VideoPlayer
import Kingfisher
import HungamaPlayer
import SafariServices
import ErosNow_iOS_SDK
import SonyLIVSDK
import Logix_Player_IOS_SDK

class BAVODDetailViewController: BaseViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var contentPosterImage: UIImageView!
    @IBOutlet weak var playerViewArea: UIView!
    @IBOutlet weak var markFavourite: UIButton!
    @IBOutlet weak var vodDetailTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    var recommendedContentVM: BARecommendedContentViewModal?
    var signOutVM: BASignOutViewModal?
    var refreshRRMVM: BARefreshRRMViewModal?
    var seriesDetailVM: BASeriesDetailViewModal?
    var tvodExpiryVM: TVODExpiryViewModal?
    var addRemoveWatchlistVM: BAAddRemoveWatchlistViewModal?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var vootPlaybackViewModal: BAVootViewModal?
    var zeeTagViewModal: BAZeeTagViewModal?
    var continueWatchingVM: BAWatchingViewModal?
    var playbackUrlVM: BAPlayBackUrlViewModal?
    var pageType: String?
    var playButtonTitle: String = ""
    var watchlistVodId: Int = 0
    var playEpisode: Episodes?
    var timestamp: Int = 0
    var previousButtonTitle = ""
    var previousContentId = 0
    var playUrl: String = ""
    var isTableReloaded = false
    var isContentPlaybackStarted = false
    var isContentPlaying = false
    var shemarooContentVM: ShemarooContentViewModal?
    var learnWatchEventVM: BAWatchLearnActionViewModal?
    var learnFavEventVM: BAFavouriteLearnActionViewModal?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var viewCountVM: BAViewCountViewModal?
    var userDetailVM: BAUserDetailViewModal?
    var contentDetail: ContentDetailData?
    var allEpisodes: [Episodes]?
    var parentNavigation: UINavigationController?
    var player: BAPlayerViewController?
    var moviePlayer: BAPlayerFullScreenViewController?
    var shouldShowFullDesc: Bool = false
    var dataSourceArray: [Any] = []
    var selectedSeason: Int = 0
    var isRentalExpiryShown = false
    var playerExistingState = (false,false)
    var isAddedToFav = false
    var isTrailerPlayed: Bool?
    var videoTimer: Timer?
    var isVideoPlaying: Bool?
    var lastContentOffset: CGFloat = 0
    var isMoviePlaying = false
    var fullscreenVodId = "0"
    var contentDetailCell: BAContentDetailTableViewCell?
    var shouldWaitForPlaying = true
    var isMovedToPlayer = false
    var isCallAlreadyMade = false
    var railType: String?
    var previousContentType: String?
    var layoutType: String?
    var limit = 0
    var pageSource = ""
    var configSource = ""
    var railName = ""
    var offset = 1
    var shoudlButtonsReloaded = true
    var id: Int?
    var rotationButtonTapped: Bool = false
    var erosNowLoginAttempt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        registerCells()
        conformDelegates()
        configureContentPoster()
        checkForTrailerAutoPlay()
        checkToMakeViewContentCall()
        addNotificationObservers()
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(publishWatchEvent), name: NSNotification.Name(rawValue: "publishWatchEventTime"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showValidationAlert), name: NSNotification.Name(rawValue: "showValidationAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showValidationAlertOnSessionFailure), name: NSNotification.Name(rawValue: "showValidationAlertOnSessionFailure"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showsomethingWentAlert), name: NSNotification.Name(rawValue: "showSomethingwentWrong"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseButtonFunction), name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playButtonFunction), name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissPlayerOnPush), name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureReplayButton), name: NSNotification.Name(rawValue: "syncReplayPIButton"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeHungamaContentPlayer), name: NSNotification.Name(rawValue: "removeHungamaPlayer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(popToBackView), name: NSNotification.Name(rawValue: "popPIScreen"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        BAKeychainManager().isOnEditProfileScreen = false
        self.parentNavigation?.setNavigationBarHidden(true, animated: false)
        fetchContentOtherDetails()
        fetchLastWatch(isZeePlayerDismissed: false)
        fetchCSPlaybackUrl()
    }
    
    func fetchCSPlaybackUrl() {
        if contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
            var vodId = 0
            var contentType = ""
            switch contentDetail?.meta?.contentType {
            case ContentType.series.rawValue:
                vodId = contentDetail?.meta?.seriesId ?? 0
                contentType = contentDetail?.meta?.contentType ?? ""
            case ContentType.brand.rawValue:
                vodId = contentDetail?.meta?.brandId ?? 0
                contentType = contentDetail?.meta?.contentType ?? ""
            default:
                vodId = contentDetail?.meta?.vodId ?? 0
                contentType = "vod"
            }
            if contentType == ContentType.brand.rawValue {
                fetchPlayBackUrl(contentType: contentType.lowercased(), contentId: vodId)
            } else {
                fetchPlayBackUrl(contentType: contentType.lowercased(), contentId: vodId)
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if (self.traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass) || (self.traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass) {
            print("orientation change 2")
            print("rotationButtonTapped2 -------->", rotationButtonTapped)
            if !rotationButtonTapped {
                print("rotationButtonTapped -------->", rotationButtonTapped)
                screenRotated()
            }
        }
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    func checkForTrailerAutoPlay() {
        if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue {
            HungamaPlayerManager.shared.releasePlayer()
        }
        guard let trailerInfo = contentDetail?.meta?.partnerTrailerInfo else {
            shouldWaitForPlaying = false
            return
        }
        shouldWaitForPlaying = !(trailerInfo.isEmpty)
    }
    
    
    func checkToMakeViewContentCall() {
        if checkViewCallAndTALearnActionCall() {
            makeViewCountCall()
        } else {
            // TODO: Make TA Calls
        }
    }
    
    func checkViewCallAndTALearnActionCall() -> Bool {
        if contentDetail?.meta?.contentType == ContentType.webShorts.rawValue || self.contentDetail?.detail?.contractName == "RENTAL" || (self.contentDetail?.meta?.partnerSubscriptionType != nil  && self.contentDetail?.meta?.partnerSubscriptionType?.uppercased() == PartnerSubscriptionType.free.rawValue) {
            return true
            //makeViewCountCall()
        } else {
            // TODO: Make TA Calls
            return false
        }
    }
    
    func makeViewCountCall() {
        var vodId = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        viewCountVM = BAViewCountViewModal(repo: BAViewCountRepo(), endPoint: "", contentType: contentDetail?.meta?.contentType ?? "", id: "\(vodId)")
        performViewCountCall()
    }
    
    func performViewCountCall() {
        if BAReachAbility.isConnectedToNetwork() {
            if viewCountVM != nil {
                viewCountVM?.makeViewCountActionCall(completion: {(flagValue, message, isApiError) in
                    if flagValue {
                        print("******* View Count Call Success ********")
                        //BAKeychainManager().isAutoPlayOn = self.userDetailVM?.userDetail?.autoPlayTrailer ?? true
                    } else if isApiError {
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        //self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(isZeePlayerDismissed: Bool?) {
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(contentType: contentDetail?.meta?.contentType, vodId: watchlistVodId, isBackFromZeePlayer: isZeePlayerDismissed)
    }
    
    func fetchPlayBackUrl(contentType: String, contentId: Int) {
        playbackUrlVM = BAPlayBackUrlViewModal(repo: BAPlayBackUrlRepo(), endPoint: "\(contentType)/\(contentId)")
        configurePlayabaleUrl()
    }
    
    func configurePlayabaleUrl() {
        if isCallAlreadyMade == false {
            isCallAlreadyMade = true
            if let dataModel = playbackUrlVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getPlaybackUrl {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.isCallAlreadyMade = false
                        self.playUrl = self.playbackUrlVM?.playbackUrlModal?.detail?.playUrl ?? ""
                    } else if isApiError {
                        self.isCallAlreadyMade = false
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.isCallAlreadyMade = false
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                }
            }
        } else {
            // Do Nothing
        }
    }

    func updateContentType() {
        previousContentType = contentDetail?.meta?.contentType
        
        if contentDetail?.meta?.parentContentType != nil && contentDetail?.meta?.parentContentType != contentDetail?.meta?.contentType {
            let contentType = contentDetail?.meta?.parentContentType
            contentDetail?.meta?.contentType = contentType
        }
    }
    
    func recheckVM() {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.vodDetailTableView.tableFooterView?.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
        UtilityFunction.shared.performTaskInMainQueue {
            CustomLoader.shared.hideLoader()
        }
        self.parentNavigation?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc func configureReplayButton() {
        playButtonTitle = "Replay"
        contentDetailCell?.playButton.selectedButton(title: "Replay", iconName: "btnReplay")
    }
    
    @objc func screenRotated() {
        if UIDevice.current.orientation.isLandscape {
            print("=========> 1 \(player) ============ \(isMoviePlaying)")
            if player != nil && isMoviePlaying == false {
                playerViewControllerWillChangeLayout(isButtonTapped: false)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeSmallScreenIcon"), object: nil)
            } else {
                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            }
        } else if UIDevice.current.orientation.isPortrait {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeFullScreenIcon"), object: nil)
        }
    }
    
    @objc func dismissPlayerOnPush() {
        if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.atv.rawValue {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus?.uppercased() == kActive.uppercased() {
                self.apiError(kDTHPackDropped, title: kDTHPackDroppedHeader) {
                    self.signOut()
                }
            } else {
                self.apiError(kDTHPackDropped, title: kATVInactiveHeader) {
                    self.signOut()
                }
            }
        } else if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.stick.rawValue {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated == true {
                self.apiError(kDTHInactive, title: kDTHInactiveHeader)
            } else {
                self.apiError(kDTHInactive, title: kDTHInactiveHeader)
            }
        }
    }
    
    // MARK: Check for playback permission
    func checkForPlaybackAvailabilityStatus() -> Bool {
        return BAKeychainManager().contentPlayBack
        /*if BAKeychainManager().contentPlayBack == true {
            return true
        } else if BAKeychainManager().contentPlayBack == false {
            return false
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kActive && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
            return true
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kActive && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
            return false
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kInactiveState && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription {
                return false
            } else {
                return true
            }
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kInactiveState && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
            return false
        } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspended || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspension) && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription {
               return false
            } else {
                return true
            }
        } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspended || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspension) && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
            return false
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.accountSubStatus == kPartialDunnedState && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription {
               return false
            } else {
                return true
            }
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.accountSubStatus == kPartialDunnedState && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
            return false
        } else {
            return true
        }*/
    }
    
    func showDTHAlerts() {
        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kDeActive || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kInactiveState {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated == false {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
                }
            }
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kActive {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState {
                self.alertWithInactiveTitleAndMessage(kBingeSubscriptionMBRDeactiveMessage, title: kDTHInactiveLowBalanceHeader) {
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                self.noSubscription()
            }
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.accountSubStatus == kPartialDunnedState || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.accountSubStatus == kPartialDunned {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kAlert) {
                }
            }
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspended || BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspension {
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == false {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.migrated == true {
                if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    }
                } else if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kFreeSubscription || (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.planType?.uppercased() == kPaidSubscription && BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == kInactiveState)) {
                    self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                    }
                }
            } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
                self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRTempSuspendedMessage, title: kDTHInactiveWithDBRAndMBRTempSuspendedHeader) {
                }
            }
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails == nil {
            noSubscription()
            //self.alertWithInactiveTitleAndMessage(kDTHInactiveWithDBRAndMBRInactiveMessage, title: kDTHInactiveWithDBRAndMBRInactiveHeader) {
            //}
        } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO == nil {
            noSubscription()
        }
    }
    
    func signOut() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            signOutVM = BASignOutViewModal(repo: BASignOutRepo())
            if let signOutVM = signOutVM {
                signOutVM.signOut {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        BAKeychainManager().isFSPopUpShown = false
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        popToBackView()
    }
    
   @objc func popToBackView() {
        removePlayerViewContent()
        shouldWaitForPlaying = true
        isMovedToPlayer = false
        stopVideoTimer()
        UtilityFunction.shared.performTaskInMainQueue {
            HungamaPlayerManager.shared.releasePlayer()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func removePlayerViewContent() {
        if let videoPlayer = self.player {
            videoPlayer.playerView.probeInterface?.sendEvent(eventType: .STOPPED)
            if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue {
                videoPlayer.hungamaPlayer.releasePlayer()
                videoPlayer.hungamaPlayer.cancelTimer()
                videoPlayer.hungamaPlayer.playbackEventTracking(railName: railName, pageSource: pageSource, configSource: configSource)
            }
            else {
                videoPlayer.playerView.player.stop()
                videoPlayer.playerView.player.pause()
                videoPlayer.playerView.player.isMuted = true
                videoPlayer.playerView.cancelTimer()
                videoPlayer.playerView.playbackEventTracking(railName: railName, pageSource: pageSource, configSource: configSource)
                ENSDK.shared.playerClosed() // Method exposed by eros team to remove the player sessions at their end.
                videoPlayer.playerView.player.removePlayerObservers()
                //videoPlayer.playerView.player = nil
            }
        }
    }
    
    @objc func removeHungamaContentPlayer() {
        if player != nil {
            player?.view.removeFromSuperview()
            removePlayerViewContent()
            self.isContentPlaybackStarted = false
            self.isCallAlreadyMade = false
            if previousButtonTitle == "" {
                self.playButtonTitle = self.contentDetailCell?.playButton.currentTitle ?? ""
                self.contentDetailCell?.playButton.selectedButton(title: self.self.contentDetailCell?.playButton.currentTitle ?? "Play", iconName: "play")
            } else {
                self.playButtonTitle = self.previousButtonTitle
                self.contentDetailCell?.playButton.selectedButton(title: self.previousButtonTitle, iconName: "play")
            }
            // self.playButtonFunction()
            player = nil
        }
    }
    
    func configureUI() {
        headerView.backgroundColor = .clear
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(headerTapped(gestureRecognizer:)))
        self.headerView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func getUserDetail() {
        self.userDetailVM = BAUserDetailViewModal(BAUserDetailRepo())
        fetchUserDetails()
    }
    
    func configurePlayer(_ player: BAPlayerViewController) {
        UtilityFunction.shared.performTaskInMainQueue {
            let addedPlayers = self.playerViewArea.subviews.filter { $0 is BAPlayerView }
            if addedPlayers.isEmpty { //Player is not already added
                self.playerViewArea.addSubview(player.view)
                player.view.fillParentView()
            }
            if self.isContentPlaybackStarted {
                // Do Nothing
            } else {
                if self.contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        CustomLoader.shared.hideLoader(on: self.contentPosterImage)
                    }
                } else {
                    CustomLoader.shared.hideLoader(on: self.contentPosterImage)
                }
            }
        }
    }
    
    func fetchUserDetails() {
        if BAReachAbility.isConnectedToNetwork() {
            if userDetailVM != nil {
                userDetailVM?.getUserDetail(completion: {(flagValue, message, isApiError) in
                    if flagValue {
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    func fetchContentOtherDetails() {
        if BAReachAbility.isConnectedToNetwork() {
            var vodId = 0
            var contentType = contentDetail?.meta?.contentType
            if contentDetail?.meta?.parentContentType != nil && contentDetail?.meta?.parentContentType != contentDetail?.meta?.contentType {
                contentType = contentDetail?.meta?.parentContentType
            } else {
                contentType = contentDetail?.meta?.contentType
            }
            switch contentType {
            case ContentType.series.rawValue:
                vodId = contentDetail?.meta?.seriesId ?? 0
            case ContentType.brand.rawValue:
                vodId = contentDetail?.meta?.brandId ?? 0
            default:
                vodId = contentDetail?.meta?.vodId ?? 0
            }
            fullscreenVodId = String(vodId)
            getUserDetail()
            isMoviePlaying = false
            if pageType?.isEmpty == false {
                
            } else {
                updateDataSourceArray()
                if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                    let seasonId = Int(contentDetail?.lastWatch?.seriesId ?? "0") ?? 0
                    var index = 0//contentDetail?.seriesList?.firstIndex(where: {$0 === seasonId})
                    if contentDetail?.meta?.parentContentType == ContentType.series.rawValue {
                        fetchSeasonDetailForContentId(seasonId, limit: 5, offsset: 0 /*contentDetail?.lastWatch?.episodeId ?? 0*/, isAutoScroll: "true", isLastWatch: "false"/*"true"*/, isFromScroll: false)
                    } else {
                        if contentType == ContentType.series.rawValue {
                            fetchSeasonDetailForContentId(seasonId, limit: 5, offsset: 0 /*contentDetail?.lastWatch?.episodeId ?? 0*/, isAutoScroll: "true", isLastWatch: "false"/*"true"*/, isFromScroll: false)
                        } else {
                            if contentDetail?.seriesList?.isEmpty ?? true {
                                // TODO: DO NOthing, It Is an Episode
                            } else {
                                for i in 0...(contentDetail?.seriesList?.count ?? 1)-1 {
                                    if contentDetail?.seriesList?[i].id == seasonId {
                                        index = i
                                    }
                                }
                                switchToSeason(index: index)
                            }
                        }
                    }
                } else if contentType == ContentType.series.rawValue {
                    if contentDetail?.lastWatch != nil {
                        fetchSeasonDetailForContentId(contentDetail?.meta?.seriesId ?? 0, limit: 5, offsset: 0 /*contentDetail?.lastWatch?.episodeId ?? 0*/, isAutoScroll: "true", isLastWatch: "false"/*"true"*/, isFromScroll: false)
                    } else {
                        fetchSeasonDetailForContentId(contentDetail?.meta?.seriesId ?? 0, limit: 5, offsset: 0, isAutoScroll: "true", isLastWatch: "false", isFromScroll: false)
                    }
                } else if contentDetail?.meta?.parentContentType == ContentType.series.rawValue {
                    fetchSeasonDetailForContentId(contentDetail?.meta?.seriesId ?? 0, limit: 5, offsset: 0, isAutoScroll: "true", isLastWatch: "false", isFromScroll: false)
                } else {
                    if let seasonId = contentDetail?.seriesList?.first?.id {
                        if contentDetail?.lastWatch != nil {
                            fetchSeasonDetailForContentId(seasonId, limit: 5, offsset: 0/*contentDetail?.lastWatch?.episodeId ?? 0*/, isAutoScroll: "true", isLastWatch: "false"/*"true"*/, isFromScroll: false)
                        } else {
                            fetchSeasonDetailForContentId(seasonId, limit: 5, offsset: 0, isAutoScroll: "true", isLastWatch: "false", isFromScroll: false)
                        }
                        
                    }
                }
                if contentDetail?.meta?.parentContentType != nil && contentDetail?.meta?.parentContentType != contentDetail?.meta?.contentType {
                    if contentDetail?.meta?.contentType == ContentType.webShorts.rawValue || contentDetail?.meta?.contentType == ContentType.movies.rawValue {
                        fetchRecommendedContent(contentType: contentDetail?.meta?.contentType)
                    } else {
                        let contentType = contentDetail?.meta?.parentContentType
                        fetchRecommendedContent(contentType: contentType/*contentDetail?.meta?.contentType*/)
                    }
                } else {
                    fetchRecommendedContent(contentType: contentDetail?.meta?.contentType)
                }
            }
        } else {
            noInternet()
        }
        vodDetailTableView.backgroundColor = UIColor.BAdarkBlueBackground
    }
    
    func fetchWatchlistLookUp(contentType: String?, vodId: Int?, isBackFromZeePlayer: Bool?) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType ?? "", contentId: String(vodId ?? 0))
        confgureWatchlistIcon(isBackFromZeeWebPlayer: isBackFromZeePlayer)
    }
    
    func configureContentPoster() {
        var url = URL.init(string: "")
        let cellWidth = Int(contentPosterImage.frame.width)
        let cellHeight = Int(contentPosterImage.frame.height)
        
        var imageString = ""
        if let image = contentDetail?.meta?.imageUrlApp {
            imageString = image
            
            let imageUrl = imageString
            let cloudnaryUrl = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
            url = URL.init(string: cloudnaryUrl ?? "")
            print("Image Url Value is1 ------------>", imageString)
        } else {
            imageString = contentDetail?.meta?.boxCoverImage ?? ""
            let imageUrl = imageString
            let cloudnaryUrl = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
            url = URL.init(string: cloudnaryUrl ?? "")
            print("Image Url Value is2 ------------>", imageString)
        }
        print("Image Url Value is ------------>", imageString)
        setImage(with: contentPosterImage, url)
    }
    
    private func setImage(with imageView: UIImageView, _ url: URL?) {
        UtilityFunction.shared.performTaskInMainQueue {
            if let _url = url {
                imageView.alpha = (imageView.image == nil) ? 0 : 1
                imageView.kf.setImage(with: _url, completionHandler:  { result in
                    switch result {
                    case .success(let value):
                        break
//                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("BAVODDetailViewController Error: \(error)")
                    }
                    UIView.animate(withDuration: 1, animations: { imageView.alpha = 1 })
                })
            } else {
                imageView.image = nil
            }
        }
    }
    
    func confgureWatchlistIcon(isBackFromZeeWebPlayer: Bool?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = watchlistLookupVM {
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        self.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
//                        if self.contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
//                            self.contentDetailCell?.configureLastWatchButtonStates(contentDetails: self.contentDetail, buttonTitle: self.playButtonTitle ?? "")
//                        }
//                        if isBackFromZeeWebPlayer ?? false {
//                            self.shoudlButtonsReloaded = true
//                            self.vodDetailTableView.reloadData()
//                        }
                        if dataModel.watchlistLookUp?.data?.isFavourite ?? false {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addFav"), object: nil)
                            self.markFavourite.isSelected = true
                            self.isAddedToFav = true
                            //self.mixPanelEvents()
                            self.markFavourite.setImage(UIImage(named: "favSelected"), for: .normal)
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeFav"), object: nil)
                            self.markFavourite.isSelected = false
                            self.isAddedToFav = false
                            self.markFavourite.setImage(UIImage(named: "followIcon"), for: .normal)
                        }
                    } else if isApiError {
                        // self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        // self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func mixPanelEvents() {
        var tilteText = ""
        if contentDetail?.meta?.contentType == "BRAND" {
            tilteText = contentDetail?.meta?.brandTitle ?? ""
        } else {
            tilteText = contentDetail?.meta?.vodTitle ?? ""
        }
        let genreResult = contentDetail?.meta?.genre?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: tilteText, MixpanelConstants.ParamName.contentType.rawValue: contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? ""])
    }
    
    // MARK: Content Detail API Calling
    func updateContentDetail(vodId: Int?, contentType: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        updateContentDetailData(vodId: vodId)
    }
    
    // MARK: Content Detail Response Handling
    func updateContentDetailData(vodId: Int?) {
        if let dataModel = contentDetailVM {
            dataModel.getContentScreenData { (flagValue, message, isApiError) in
                if flagValue {
                    self.contentDetail = dataModel.contentDetail
                    self.hideActivityIndicator()
                    self.fetchContentOtherDetails()
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        } else {
            //self.hideActivityIndicator()
        }
    }
    
    // MARK: Header Tap Gesture
    @objc func headerTapped(gestureRecognizer: UIGestureRecognizer) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(configureControls), object: nil)
        perform(#selector(configureControls), with: nil, afterDelay: 0.5)
    }
    
    @objc func configureControls() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideControls"), object: nil)
    }
    
    @IBAction func favButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            var vodId = 0
            let contentType = contentDetail?.meta?.contentType
            switch contentType {
            case ContentType.series.rawValue:
                vodId = contentDetail?.meta?.seriesId ?? 0
            case ContentType.brand.rawValue:
                vodId = contentDetail?.meta?.brandId ?? 0
            default:
                vodId = contentDetail?.meta?.vodId ?? 0
            }
            configureLearnAction(vodId: vodId)
            addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentType ?? "", contentId: vodId)
            addRemoveFromWatchlist()
        } else {
            noInternet()
        }
    }
    
    func configureLearnAction(vodId: Int) {
        learnFavEventVM = BAFavouriteLearnActionViewModal(repo: BAFavouriteLearnActionRepo(), endPoint: "\(contentDetail?.meta?.contentType ?? "")/\(vodId)/VOD")
        favLearnAction()
    }
    
    func favLearnAction() {
        if !checkViewCallAndTALearnActionCall() {
            if BAReachAbility.isConnectedToNetwork() {
                if learnFavEventVM != nil {
                    learnFavEventVM?.makeFavouriteLearnActionCall(completion: { (flagValue, message, isApiError) in
                        if flagValue {
                            // Do Nothing
                        } else if isApiError {
                            self.apiError(message, title: kSomethingWentWrong)
                        } else {
                            self.apiError(message, title: "")
                        }
                    })
                }
            } else {
                noInternet()
            }
        } else {
            // TODO: Else call
        }
    }
    
    func watchLearnAction() {
        if !checkViewCallAndTALearnActionCall() {
            if BAReachAbility.isConnectedToNetwork() {
                if learnWatchEventVM != nil {
                    learnWatchEventVM?.makeWatchLearnActionCall(completion: { (flagValue, message, isApiError) in
                        if flagValue {
                            // Do Nothing
                        } else if isApiError {
                            self.apiError(message, title: "")
                        } else {
                            self.apiError(message, title: "")
                        }
                    })
                }
            } else {
                noInternet()
            }
        } else {
            // TODO: Else call
        }
    }
    
    func registerCells() {
        UtilityFunction.shared.registerCell(vodDetailTableView, kContentDetailCell)
        UtilityFunction.shared.registerCell(vodDetailTableView, kBAHomeRailTableViewCell)
        UtilityFunction.shared.registerCell(vodDetailTableView, kBAAppPosterTableViewCell)
        let headerNib = UINib.init(nibName: "BASeriesDetailTableViewCell", bundle: Bundle.main)
        vodDetailTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "BASeriesDetailTableViewCell")
        UtilityFunction.shared.registerCell(vodDetailTableView, kBAEpisodeTableViewCell)
        UtilityFunction.shared.registerCell(vodDetailTableView, kBASeriesFooterViewTableViewCell)
    }
    
    func conformDelegates() {
        vodDetailTableView.delegate = self
        vodDetailTableView.dataSource = self
    }
    
    func fetchRecommendedContent(contentType: String?) {
        var vodId = 0
        switch contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        recommendedContentVM = BARecommendedContentViewModal(repo: RecommendedContentRepo(), endPoint: "\(vodId)/" + (contentType ?? ""))
        getRecommendedContentData(contentType ?? "")
    }
    
    func fetchRecommendedContentOnTAFailure(contentType: String?) {
        var vodId = 0
        switch contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        
        recommendedContentVM = BARecommendedContentViewModal(repo: RecommendedContentRepo(), endPoint: "\(vodId)/" + (contentDetail?.meta?.contentType ?? ""))
        fetchEditorialRecommendataionData()
    }
    
    // MARK: Recommended Content API Calling
    func getRecommendedContentData(_ contentType: String) {
        let rentalData = contentDetail?.detail?.contractName ?? ""
        if checkViewCallAndTALearnActionCall() {
            fetchEditorialRecommendataionData()
        } else {
            fetchTARecommendationData()
        }
    }
    
    func fetchEditorialRecommendataionData() {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = recommendedContentVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getRecommendedContentData {(flagValue, _, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        let recomendedList = self.recommendedContentVM?.recommendedData?.data?.contentList
                        self.recommendedContentVM?.recommendedData?.data?.contentList?.removeAll()
                        if recomendedList?.count ?? 0 > 0 {
                            for recommendedData in recomendedList ?? [] {
                                if recommendedData.provider != ProviderType.prime.rawValue {
                                    self.recommendedContentVM?.recommendedData?.data?.contentList?.append(recommendedData)
                                } else {
                                    // Do Nothing
                                }
                            }
                        }
                        self.updateDataSourceArray()
                        self.shoudlButtonsReloaded = false
                        self.vodDetailTableView.reloadData()
                    }
                }
            }
        } else {
            //hideActivityIndicator()
        }
    }
    
    func fetchTARecommendationData() {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = recommendedContentVM {
                dataModel.taShowType = (contentDetail?.meta?.taShowType ?? "")
                switch contentDetail?.meta?.vodContentType {
                case ContentType.series.rawValue:
                    id = contentDetail?.meta?.seriesId
                case ContentType.brand.rawValue:
                    id = contentDetail?.meta?.brandId
                default:
                    id = contentDetail?.meta?.vodId
                }
                dataModel.apiParams["id"] = String(id ?? 0)
                dataModel.apiParams["layout"] = layoutType ?? "LANDSCAPE"
                dataModel.apiParams["provider"] = contentDetail?.meta?.provider
                dataModel.makeTARecommendationCalls {(flagValue, _, isApiError) in
                    if flagValue {
                        let recomendedList = self.recommendedContentVM?.recommendedData?.data?.contentList
                        if recomendedList?.count ?? 0 > 0 {
                            self.railType = TAConstant.recommendationType.rawValue
                            self.recommendedContentVM?.recommendedData?.data?.layoutType = "LANDSCAPE" //self.layoutType ?? ""
                            self.recommendedContentVM?.recommendedData?.data?.title = UtilityFunction.shared.taRecommendedRailTitle(taShowType: (self.contentDetail?.meta?.taShowType ?? ""))
                            
                            self.recommendedContentVM?.recommendedData?.data?.contentList?.removeAll()
                            if recomendedList?.count ?? 0 > 0 {
                                for recommendedData in recomendedList ?? [] {
                                    if recommendedData.provider != ProviderType.prime.rawValue {
                                        self.recommendedContentVM?.recommendedData?.data?.contentList?.append(recommendedData)
                                    } else {
                                        // Do Nothing
                                    }
                                }
                            }
                            self.updateDataSourceArray()
                            self.shoudlButtonsReloaded = false
                            self.vodDetailTableView.reloadData()
                        } else {
                            self.railType = TAConstant.editorial.rawValue
                            self.fetchRecommendedContentOnTAFailure(contentType: self.contentDetail?.meta?.contentType)
                        }
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.railType = TAConstant.editorial.rawValue
                        self.fetchRecommendedContentOnTAFailure(contentType: self.contentDetail?.meta?.contentType)
                    } else {
                        self.hideActivityIndicator()
                        self.railType = TAConstant.editorial.rawValue
                        self.fetchEditorialRecommendataionData()
                    }
                }
                
//                if self.recommendedContentVM?.recommendedData?.data?.contentList == nil {
//                    self.fetchRecommendedContentOnTAFailure(contentType: self.contentDetail?.meta?.contentType)
//                    print("no recommende content")
//                } else {
//                    print("Recommended content there")
//                }
            }
        } else {
            
        }
    }
    
    // MARK: AddRemoveToWatchlist
    func addRemoveFromWatchlist() {
        if let dataModel = addRemoveWatchlistVM {
            dataModel.addRemoveWatchlist {(flagValue, message, isApiError) in
                if flagValue {
                    if self.markFavourite.isSelected {
                        self.markFavourite.isSelected = false
                        self.isAddedToFav = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeFav"), object: nil)
                        let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                        let languageResult = self.contentDetail?.meta?.audio?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        self.markFavourite.setImage(UIImage(named: "followIcon"), for: .normal)
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addFav"), object: nil)
                        let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                        let languageResult = self.contentDetail?.meta?.audio?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        self.markFavourite.isSelected = true
                        self.isAddedToFav = true
                        self.mixPanelEvent()
                        self.markFavourite.setImage(UIImage(named: "favSelected"), for: .normal)
                    }
                } else if isApiError {
                    self.hideActivityIndicator()
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.hideActivityIndicator()
                    self.apiError(message, title: "")
                }
            }
        }
    }
    
    func mixPanelEvent() {
        let genreResult = contentDetail?.meta?.genre?.joined(separator: ",")
        let languageResult = contentDetail?.meta?.genre?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentType.rawValue: contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
    
    // MARK: Season Details API Calling
    func fetchSeasonDetailForContentId(_ contentId: Int, limit: Int, offsset: Int, isAutoScroll: String, isLastWatch: String, isFromScroll: Bool) {
        var apiParams: [String: String] = [:]
        apiParams["limit"] = "\(limit)"
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["profileId"] = BAKeychainManager().profileId
        apiParams["isAutoScroll"] = isAutoScroll
        apiParams["isLastWatch"] = isLastWatch
        apiParams["offset"] = "\(offsset)"
        seriesDetailVM = BASeriesDetailViewModal(repo: SeriesDetailContentRepo(), endPoint: "/list"+"/\(contentId)", params: apiParams, contentId: contentId)
        getSeasonData(contentId, contentType: contentDetail?.meta?.contentType, isScroll: isFromScroll, parentContentType: contentDetail?.meta?.parentContentType)
    }
    
    // MARK: Recommended Content API Calling
    func getSeasonData(_ contentId: Int, contentType: String?, isScroll: Bool, parentContentType: String?) {
        if let dataModel = seriesDetailVM {
            dataModel.getSeriesData { (flagValue, message, isApiError) in
                if flagValue {
                    self.contentDetail?.updateSeasons(self.seriesDetailVM?.seriesDetailModal?.data, seasonId: contentId, contentType: contentType ?? "", parentContentType: parentContentType ?? "")
                    var previousEpisodeLength = 0
                    var updatedEpisodeLength = 0
                    let totalEpisodeCount: Int = self.getSelectedSeasonEpisodeCount().totalEpisodeCount
                    previousEpisodeLength = self.getSelectedSeasonEpisodeCount().episodeCount
                    self.updateDataSourceArray()
                    updatedEpisodeLength = self.getSelectedSeasonEpisodeCount().episodeCount
                    var indexPaths: [IndexPath] = []
                    for i in previousEpisodeLength..<updatedEpisodeLength {
                        indexPaths.append(IndexPath(row: i, section: 1))
                    }
                    if self.previousContentId == 0 || self.previousContentId == contentId {
                        self.previousContentId = contentId
                        self.vodDetailTableView.layoutIfNeeded()
                        UIView.transition(with: self.vodDetailTableView, duration: 0.35, options: [.transitionCrossDissolve, .allowUserInteraction], animations: {
                            if previousEpisodeLength < 1 {
                                self.shoudlButtonsReloaded = false
                                self.vodDetailTableView.reloadData()
                            } else if !indexPaths.isEmpty {
                                self.vodDetailTableView.beginUpdates()
                                if updatedEpisodeLength == totalEpisodeCount {
                                    self.vodDetailTableView.deleteRows(at: [IndexPath(row: previousEpisodeLength, section: 1)], with: .automatic)
                                }
                                self.vodDetailTableView.insertRows(at: indexPaths, with: .bottom)
                                self.vodDetailTableView.endUpdates()
                            }
                        }, completion: nil)
                        if self.contentDetail?.meta?.contentType == ContentType.series.rawValue {
                            // Do Nothing
                        } else {
                            
                        }
                    } else {
                        self.previousContentId = contentId
                        self.vodDetailTableView.layoutIfNeeded()
                        UIView.transition(with: self.vodDetailTableView, duration: 0.35, options: [.transitionCrossDissolve, .allowUserInteraction], animations: {
                            if previousEpisodeLength < 1 {
                                self.shoudlButtonsReloaded = false
                                self.vodDetailTableView.reloadData()
                            } else if !indexPaths.isEmpty {
                                self.vodDetailTableView.beginUpdates()
                                if updatedEpisodeLength == totalEpisodeCount {
                                    self.vodDetailTableView.deleteRows(at: [IndexPath(row: previousEpisodeLength, section: 1)], with: .automatic)
                                }
                                self.vodDetailTableView.insertRows(at: indexPaths, with: .bottom)
                                self.vodDetailTableView.endUpdates()
                            }
                        }, completion: nil)
                        if self.contentDetail?.meta?.contentType == ContentType.series.rawValue {
                            // Do Nothing
                        } else {
                            //self.scrollToSeasons()
                        }
                    }
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }}
    }
    
    func getSelectedSeasonEpisodeCount() -> (episodeCount: Int, totalEpisodeCount: Int) {
        var episodeCount = 0
        var totalEpisodeCount = 0
        for data in dataSourceArray where data is [SeriesYearsList] {
            if let seriesYearList = data as? [SeriesYearsList] {
                episodeCount =  seriesYearList[selectedSeason].seriesDetail?.items?.count ?? 0
                totalEpisodeCount = seriesYearList[selectedSeason].seriesDetail?.total ?? 0
            }
        }
        return (episodeCount: episodeCount, totalEpisodeCount: totalEpisodeCount)
    }
    
    // MARK: Fire Continue Watching Event
    @objc func publishWatchEvent(notification: Notification) {
        guard let _ = notification.userInfo!["time"] else { return }
    }
    
    @objc func showValidationAlert() {
        if isRentalExpiryShown {
            // DO Nothing
        } else {
            isRentalExpiryShown = true
            if self.contentDetail?.lastWatch?.playerDetail?.contractName == "RENTAL" {
                if let vodID = contentDetail?.meta?.vodId {
                    let id = Int64(vodID)
                    DataBaseManager.deleteJwtToken(id)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.apiError("You have no longer access to view this content", title: kSomethingWentWrong) {
                            self.getRefreshToken(isReplay: self.playerExistingState.0, isPlayerInPausedState: self.playerExistingState.1)
                        }
                    }
                }
            }else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.apiError("You have no longer access to view this content", title: kSomethingWentWrong)
                }
            }
        }
    }
    
    @objc func showValidationAlertOnSessionFailure() {
    if isRentalExpiryShown {
        // DO Nothing
    } else {
        isRentalExpiryShown = true
            if let vodID = contentDetail?.meta?.vodId {
                let id = Int64(vodID)
                playButtonTitle = ""
                contentDetailCell?.playButton.selectedButton(title: "Resume", iconName: "play")
                DataBaseManager.deleteJwtToken(id)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.apiError("You have no longer access to view this content", title: kSomethingWentWrong) {
                        self.getRefreshToken(isReplay: self.playerExistingState.0, isPlayerInPausedState: self.playerExistingState.1)
                    }
                }
            }
        }
    }

    @objc func pauseButtonFunction() {
        playButtonTitle = "Pause"
        contentDetailCell?.playButton.selectedButton(title: "Pause", iconName: "")
    }
    
    @objc func playButtonFunction() {
        playButtonTitle = "Play"
        contentDetailCell?.playButton.selectedButton(title: "Play", iconName: "")
    }
    
    @objc func showsomethingWentAlert() {
        if isRentalExpiryShown {
            // DO Nothing
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.apiError("Something Went Wrong While Playing Content", title: kSomethingWentWrong)
            }
        }
    }
    
    func playFullScreenPlayer(isReplay: Bool?, isPlayerInPausedState: Bool?) {
        var playableUrl = ""
        if BAReachAbility.isConnectedToNetwork() {
            if playButtonTitle == "Replay" || playButtonTitle.isEmpty || playButtonTitle == previousButtonTitle {
                isContentPlaybackStarted = false
            } else {
                isContentPlaybackStarted = true
            }
            if contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue {
                isContentPlaybackStarted = false
            }
            if isContentPlaybackStarted {
                if isPlayerInPausedState ?? false {
                    if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue { player?.hungamaPlayer.togglePlayPauseHungamaPlayer(false)
                    } else {
                        player?.playerView.player.pause()
                    }
                    playButtonTitle = "Play"
                    previousButtonTitle = contentDetailCell?.playButton.currentTitle ?? ""
                    contentDetailCell?.playButton.selectedButton(title: "Play", iconName: "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePlayerIcons"), object: nil)
                } else {
                    if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue { player?.hungamaPlayer.togglePlayPauseHungamaPlayer(true) } else { player?.playerView.player.play() }
                    playButtonTitle = "Pause"
                    previousButtonTitle = contentDetailCell?.playButton.currentTitle ?? ""
                    contentDetailCell?.playButton.selectedButton(title: "Pause", iconName: "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPlayerIcons"), object: nil)
                }
            } else {
                if player != nil {
                    player?.view.removeFromSuperview()
                    removePlayerViewContent()
                    player = nil
                }
                isContentPlaybackStarted = true
                if contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue  || contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue {
                    isContentPlaybackStarted = false
                    // Do Nothing
                } else {
                    previousButtonTitle = contentDetailCell?.playButton.currentTitle ?? ""
                    playButtonTitle = "Pause"
                    contentDetailCell?.configurePlayPauseButton(buttonTitle: "Pause")
                }
                isMovedToPlayer = true
                stopVideoTimer()
                if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        let md5SignatureData = MD5(string: smarturl_accesskey + (contentDetail?.lastWatch?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
                        let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                        print("md5SignatureHex: \(md5SignatureHex)")
                        let signedUrl = (contentDetail?.lastWatch?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
                        self.showActivityIndicator(isUserInteractionEnabled: false)
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        self.getPlayableUrl(signedUrl: signedUrl, isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil)
                    } else {
                        let md5SignatureData = MD5(string: smarturl_accesskey + (contentDetail?.meta?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
                        let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                        print("md5SignatureHex: \(md5SignatureHex)")
                        let signedUrl = (contentDetail?.meta?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
                        self.showActivityIndicator(isUserInteractionEnabled: false)
                        var vodId = 0
                        switch self.contentDetail?.meta?.contentType {
                        case ContentType.series.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        case ContentType.brand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customBrand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customSeries.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        default:
                            vodId = contentDetail?.meta?.vodId ?? 0
                        }
                        makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                        self.getPlayableUrl(signedUrl: signedUrl, isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil)
                    }
                } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: contentDetail?.lastWatch?.providerContentId, contentType: contentDetail?.lastWatch?.contentType, partner: contentDetail?.meta?.provider)
                        configureVootPlayer(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil)
                    } else {
                        vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: contentDetail?.meta?.providerContentId, contentType: contentDetail?.meta?.contentType, partner: contentDetail?.meta?.provider)
                        configureVootPlayer(isReplay: false, isResume: false, contentData: contentDetail, episodeData: nil)
                    }
                } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        configureHungamaPlayer(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil, isTrailer: false)
                    } else {
                        var vodId = 0
                        switch self.contentDetail?.meta?.contentType {
                        case ContentType.series.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        case ContentType.brand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customBrand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customSeries.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        default:
                            vodId = contentDetail?.meta?.vodId ?? 0
                        }
                        makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                        configureHungamaPlayer(isReplay: isReplay, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: false)
                    }
                }
                // SonyLiv Integration
                else if contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        configureSonyLivPlayer(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil, isTrailer: false)
                    } else {
                        var vodId = 0
                        switch self.contentDetail?.meta?.contentType {
                        case ContentType.series.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        case ContentType.brand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customBrand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customSeries.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        default:
                            vodId = contentDetail?.meta?.vodId ?? 0
                        }
                        makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                        configureSonyLivPlayer(isReplay: isReplay, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: false)
                    }
                }
                else if contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        confugureCurosityStreamPlayer(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil)
                    } else {
                        var vodId = 0
                        switch self.contentDetail?.meta?.contentType {
                        case ContentType.series.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        case ContentType.brand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customBrand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customSeries.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        default:
                            vodId = contentDetail?.meta?.vodId ?? 0
                        }
                        makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                        confugureCurosityStreamPlayer(isReplay: isReplay, isResume: false, contentData: contentDetail, episodeData: nil)
                    }
                } // Eros Now Integration
                else if contentDetail?.meta?.provider?.uppercased() == ProviderType.eros.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        configureErosNowPlayer(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil, isTrailer: false)
                    } else {
                        var vodId = 0
                        switch self.contentDetail?.meta?.contentType {
                        case ContentType.series.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        case ContentType.brand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customBrand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customSeries.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        default:
                            vodId = contentDetail?.meta?.vodId ?? 0
                        }
                        makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                        configureErosNowPlayer(isReplay: isReplay, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: false)
                    }
                }
                else if contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue {
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                        zeeTagViewModal = BAZeeTagViewModal(repo: BAZeeTagRepo(), endPoint: "")
                        configureZeePlayback(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil)
                    } else {
                        var vodId = 0
                        switch self.contentDetail?.meta?.contentType {
                        case ContentType.series.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        case ContentType.brand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customBrand.rawValue:
                            vodId = contentDetail?.meta?.brandId ?? 0
                        case ContentType.customSeries.rawValue:
                            vodId = contentDetail?.meta?.seriesId ?? 0
                        default:
                            vodId = contentDetail?.meta?.vodId ?? 0
                        }
                        makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                        zeeTagViewModal = BAZeeTagViewModal(repo: BAZeeTagRepo(), endPoint: "")
                        configureZeePlayback(isReplay: isReplay, isResume: false, contentData: contentDetail, episodeData: nil)
                    }
                } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue {
                    let count = BAKeychainManager().hotStarAlertCount // HOTFIX to make 3 time hotstar alert appear
                    if isHotstarInstalled() {
                        let genreResult = contentDetail?.meta?.genre?.joined(separator: ",") ?? ""
                        let languageResult = contentDetail?.meta?.audio?.joined(separator: ",") ?? ""
                        if count < 3 {
                            showHotstarProceedAlertWithCallback(kHotStarInstalledHeader, kNewHotStarInstalledMessageString) {
                                if self.contentDetail?.lastWatch != nil && self.contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                                    self.playbackEventTracking(contentTitle: self.contentDetail?.lastWatch?.contentTitle ?? "", contentType: self.contentDetail?.lastWatch?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                    if self.contentDetail?.lastWatch?.hotstarAppDeeplink?.isEmpty ?? false || self.contentDetail?.lastWatch?.hotstarAppDeeplink == nil {
                                        self.otpResent("Error. Content not found!", completion: nil)
                                    } else {
                                        self.makeContinueWatchCall(contentId: self.contentDetail?.lastWatch?.contentId, contentType: self.contentDetail?.lastWatch?.contentType, watchedDuration: self.contentDetail?.lastWatch?.secondsWatched, totalDuration: self.contentDetail?.lastWatch?.durationInSeconds)
                                        //let hotstarLink = contentDetail?.meta?.hotstarAppDeeplink ?? ""
                                        // let trimmedHotstarLink = hotstarLink.trimmingCharacters(in: .whitespaces)
                                        if let hotstarPlayLink = URL(string: self.contentDetail?.meta?.hotstarAppDeeplink ?? ""), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                                            UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
                                        }
                                    }
                                } else {
                                    if self.contentDetail?.meta?.hotstarAppDeeplink?.isEmpty ?? false || self.contentDetail?.meta?.hotstarAppDeeplink == nil {
                                        self.otpResent("Error. Content not found!", completion: nil)
                                    } else {
                                        var vodId = 0
                                        var videoTitle = ""
                                        switch self.contentDetail?.meta?.contentType {
                                        case ContentType.series.rawValue:
                                            vodId = self.contentDetail?.meta?.seriesId ?? 0
                                            videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                        case ContentType.brand.rawValue:
                                            vodId = self.contentDetail?.meta?.brandId ?? 0
                                            videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                        case ContentType.customBrand.rawValue:
                                            vodId = self.contentDetail?.meta?.brandId ?? 0
                                            videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                        case ContentType.customSeries.rawValue:
                                            vodId = self.contentDetail?.meta?.seriesId ?? 0
                                            videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                        default:
                                            vodId = self.contentDetail?.meta?.vodId ?? 0
                                            videoTitle = self.contentDetail?.meta?.vodTitle ?? ""
                                        }
                                        self.playbackEventTracking(contentTitle: videoTitle, contentType: self.contentDetail?.meta?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                        self.makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: self.contentDetail?.meta?.duration)
                                        if let hotstarPlayLink = URL(string: self.contentDetail?.meta?.hotstarAppDeeplink ?? ""), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                                            UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
                                        }
                                    }
                                }
                            }
                        } else {
                                if self.contentDetail?.lastWatch != nil && self.contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                                    if self.contentDetail?.lastWatch?.hotstarAppDeeplink?.isEmpty ?? false || self.contentDetail?.lastWatch?.hotstarAppDeeplink == nil {
                                        self.otpResent("Error. Content not found!", completion: nil)
                                    } else {
                                        self.playbackEventTracking(contentTitle: self.contentDetail?.lastWatch?.contentTitle ?? "", contentType: self.contentDetail?.lastWatch?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: railName, pageSource: pageSource, configSource: configSource)
                                        self.makeContinueWatchCall(contentId: self.contentDetail?.lastWatch?.contentId, contentType: self.contentDetail?.lastWatch?.contentType, watchedDuration: self.contentDetail?.lastWatch?.secondsWatched, totalDuration: self.contentDetail?.lastWatch?.durationInSeconds)
                                        //let hotstarLink = contentDetail?.meta?.hotstarAppDeeplink ?? ""
                                        // let trimmedHotstarLink = hotstarLink.trimmingCharacters(in: .whitespaces)
                                        if let hotstarPlayLink = URL(string: self.contentDetail?.meta?.hotstarAppDeeplink ?? ""), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                                            UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
                                        }
                                    }
                                } else {
                                    if self.contentDetail?.meta?.hotstarAppDeeplink?.isEmpty ?? false || self.contentDetail?.meta?.hotstarAppDeeplink == nil {
                                        self.otpResent("Error. Content not found!", completion: nil)
                                    } else {
                                        var vodId = 0
                                        var videoTitle = ""
                                        switch self.contentDetail?.meta?.contentType {
                                        case ContentType.series.rawValue:
                                            vodId = self.contentDetail?.meta?.seriesId ?? 0
                                            videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                        case ContentType.brand.rawValue:
                                            vodId = self.contentDetail?.meta?.brandId ?? 0
                                            videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                        case ContentType.customBrand.rawValue:
                                            vodId = self.contentDetail?.meta?.brandId ?? 0
                                            videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                        case ContentType.customSeries.rawValue:
                                            vodId = self.contentDetail?.meta?.seriesId ?? 0
                                            videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                        default:
                                            vodId = self.contentDetail?.meta?.vodId ?? 0
                                            videoTitle = self.contentDetail?.meta?.vodTitle ?? ""
                                        }
                                        self.playbackEventTracking(contentTitle: videoTitle, contentType: self.contentDetail?.meta?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: railName, pageSource: pageSource, configSource: configSource)
                                        self.makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: self.contentDetail?.meta?.duration)
                                        if let hotstarPlayLink = URL(string: self.contentDetail?.meta?.hotstarAppDeeplink ?? ""), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                                            UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
                                        }
                                    }
                                }
                            }
                    } else {
                        let genreResult = contentDetail?.meta?.genre?.joined(separator: ",") ?? ""
                        let languageResult = contentDetail?.meta?.audio?.joined(separator: ",") ?? ""
                        if count < 3 {
                            showHotstarProceedAlertWithCallback(kHotStarUnInstalledHeader, kNewHotStarUnInstalledMessageString) {
                                if let url = URL(string: "itms-apps://apple.com/app/id934459219") {
                                    if self.contentDetail?.lastWatch != nil && self.contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                                        self.playbackEventTracking(contentTitle: self.contentDetail?.lastWatch?.contentTitle ?? "", contentType: self.contentDetail?.lastWatch?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                    } else {
                                            var videoTitle = ""
                                            switch self.contentDetail?.meta?.contentType {
                                            case ContentType.series.rawValue:
                                                videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                            case ContentType.brand.rawValue:
                                                videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                            case ContentType.customBrand.rawValue:
                                                videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                            case ContentType.customSeries.rawValue:
                                                videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                            default:
                                                videoTitle = self.contentDetail?.meta?.vodTitle ?? ""
                                            }
                                        self.playbackEventTracking(contentTitle: videoTitle, contentType: self.contentDetail?.meta?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                           
                                    }
                                    UIApplication.shared.open(url)
                                }
                            }
                        } else {
                            if let url = URL(string: "itms-apps://apple.com/app/id934459219") {
                                if self.contentDetail?.lastWatch != nil && self.contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                                    self.playbackEventTracking(contentTitle: self.contentDetail?.lastWatch?.contentTitle ?? "", contentType: self.contentDetail?.lastWatch?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: railName, pageSource: pageSource, configSource: configSource)
                                } else {
                                        var videoTitle = ""
                                        switch self.contentDetail?.meta?.contentType {
                                        case ContentType.series.rawValue:
                                            videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                        case ContentType.brand.rawValue:
                                            videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                        case ContentType.customBrand.rawValue:
                                            videoTitle = self.contentDetail?.meta?.brandTitle ?? ""
                                        case ContentType.customSeries.rawValue:
                                            videoTitle = self.contentDetail?.meta?.seriesTitle ?? ""
                                        default:
                                            videoTitle = self.contentDetail?.meta?.vodTitle ?? ""
                                        }
                                    self.playbackEventTracking(contentTitle: videoTitle, contentType: self.contentDetail?.meta?.contentType ?? "", contentGenre: genreResult, partnerName: self.contentDetail?.meta?.provider ?? "", languages: languageResult, railName: railName, pageSource: pageSource, configSource: configSource)
                                       
                                }
                                UIApplication.shared.open(url)
                            }
                        }
                        //showOkAlertWithCustomTitle(kHotstarNotInstalled, title: "")
                        //showOkAlert(kHotstarNotInstalled)
                        // TODO: Show No App Installed Alert
                    }
                } else {
                    if !(contentDetail?.meta?.showCase ?? false) && !(contentDetail?.meta?.isPlaybackStarted ?? false) {
                        tvodExpiryVM = TVODExpiryViewModal(repo: TVODExpiryRepo(), endPoint: "\(self.contentDetail?.meta?.vodId ?? 0)")
                        getTVODExpiry()
                    }
                    if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                        if self.contentDetail?.detail?.contractName == "RENTAL" {
                            var vodId = 0
                            vodId = contentDetail?.lastWatch?.contentId ?? 0
                            let id = Int64(vodId)
                            let dbValue = DataBaseManager.getJwtTokenData(id)
                            TTNPlayerConfiguration.sharedInstance.vendorInfo(data: ["ls_session": dbValue?[0].token ?? ""], vendorDataRequired: true)
                            let kTataSkyFPSCertificateURL = "https://tatasky.stage.ott.irdeto.com/Streaming/getCertificate?accountId=tatasky&applicationId=tatasky"
                            TTNPlayerConfiguration.sharedInstance.fpsCertificateUrl(kTataSkyFPSCertificateURL)
                            if self.contentDetail?.detail?.fairplayUrl?.isEmpty ?? true {
                                showOkAlertWithCustomTitle("We are unable to play your video right now. Please try again in a few minutes", title: "Video unavailable")
                                return
                            } else {
                                playableUrl = self.contentDetail?.detail?.fairplayUrl ?? "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
                            }
                            playableUrl = self.contentDetail?.detail?.fairplayUrl ?? "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
                            makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                            self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: "", content: contentDetail, episode: nil, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
                        } else {
                            partnerPending()
                        }
                    } else {
                        if self.contentDetail?.detail?.contractName == "RENTAL" {
                            var token = ""
                            var vodId = 0
                            switch self.contentDetail?.meta?.contentType {
                            case ContentType.series.rawValue:
                                vodId = contentDetail?.meta?.seriesId ?? 0
                            case ContentType.brand.rawValue:
                                vodId = contentDetail?.meta?.brandId ?? 0
                            case ContentType.customBrand.rawValue:
                                vodId = contentDetail?.meta?.brandId ?? 0
                            case ContentType.customSeries.rawValue:
                                vodId = contentDetail?.meta?.seriesId ?? 0
                            default:
                                vodId = contentDetail?.meta?.vodId ?? 0
                            }
                            let id = Int64(vodId)
                            let dbValue = DataBaseManager.getJwtTokenData(id)
                            guard let data = dbValue else {
                                return
                            }
                            token = data[0].token ?? ""
                            TTNPlayerConfiguration.sharedInstance.vendorInfo(data: ["ls_session": token], vendorDataRequired: true)
                            let kTataSkyFPSCertificateURL = "https://tatasky.stage.ott.irdeto.com/Streaming/getCertificate?accountId=tatasky&applicationId=tatasky"
                            TTNPlayerConfiguration.sharedInstance.fpsCertificateUrl(kTataSkyFPSCertificateURL)
                            if self.contentDetail?.detail?.fairplayUrl?.isEmpty ?? true {
                                showOkAlertWithCustomTitle("We are unable to play your video right now. Please try again in a few minutes", title: "Video unavailable")
                                return
                            } else {
                                playableUrl = self.contentDetail?.detail?.fairplayUrl ?? ""
                            }
                            makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                            self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: "", content: contentDetail, episode: nil, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
                        } else {
                            partnerPending()
                        }
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func getTVODExpiry() {
        if BAReachAbility.isConnectedToNetwork() {
            if tvodExpiryVM != nil {
                tvodExpiryVM?.getExpiryDate(completion: {(flagValue, message, isApiError) in
                    if flagValue {
                        if self.tvodExpiryVM?.expiryModal?.data?.purchaseExpiry ?? 0 > 0 {
                            var timeLeft = 0
                            timeLeft = self.convertTimestampDifference(timeStamp: "\(self.tvodExpiryVM?.expiryModal?.data?.purchaseExpiry ?? 0)")
                            if timeLeft < 60 && timeLeft > 0 {
                                self.contentDetailCell?.validityLeftLabel.text = String(timeLeft) + "min"
                            } else if timeLeft > 60 {
                                let timeLeftInHours = timeLeft / 60
                                if timeLeftInHours > 48 {
                                    self.contentDetailCell?.validityLeftLabel.text = String(timeLeftInHours/24) + "days"
                                } else {
                                   self.contentDetailCell?.validityLeftLabel.text = String(timeLeftInHours) + "hrs"
                                }
                                
                            } else {
                                self.contentDetailCell?.validityLeftLabel.text = "EXPIRED"
                            }
                        }
                        print("******* TVOD Expiry Call succesfully made ********")
                        //BAKeychainManager().isAutoPlayOn = self.userDetailVM?.userDetail?.autoPlayTrailer ?? true
                    } else if isApiError {
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        //self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    // MARK: Calculate Difference between current and API timestamp
    func convertTimestampDifference(timeStamp: String?)  -> Int{
        let currentTime = Date().currentTimeMillis()
        let timeDifference = ((Int64(timeStamp ?? "0") ?? 0) - currentTime) / 60000
        return Int(timeDifference)
    }
    
    func configureHungamaPlayer(isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?, isTrailer: Bool) {
        var hungamaContent: IContentVO?
        var contentTitle = ""
        var contentId = ""
        var contentPosterImage = ""
        var contentType: HungamaPlayer.ContentType?
        if episodeData != nil {
            contentTitle = episodeData?.title ?? ""
            contentId = episodeData?.providerContentId ?? ""
            contentPosterImage = episodeData?.boxCoverImage ?? ""
            contentType = HungamaPlayer.ContentType.returnStringValue(episodeData?.contentType ?? "")
        } else {
            if contentDetail?.meta?.contentType == ContentType.brand.rawValue {
                contentTitle = contentDetail?.meta?.brandTitle ?? ""
            } else if contentDetail?.meta?.contentType == ContentType.series.rawValue {
                contentTitle = contentDetail?.meta?.seriesTitle ?? ""
            } else if contentDetail?.meta?.contentType == ContentType.tvShows.rawValue {
                if contentDetail?.meta?.parentContentType == ContentType.brand.rawValue {
                    contentTitle = contentDetail?.meta?.brandTitle ?? ""
                } else if contentDetail?.meta?.parentContentType == ContentType.series.rawValue {
                    contentTitle = contentDetail?.meta?.seriesTitle ?? ""
                } else {
                    contentTitle = contentDetail?.meta?.vodTitle ?? ""
                }
            } else {
                contentTitle = contentDetail?.meta?.vodTitle ?? ""
            }
            contentId = isTrailer ? contentDetail?.meta?.partnerTrailerInfo ?? "" : contentDetail?.meta?.providerContentId ?? ""
            contentPosterImage = contentDetail?.meta?.boxCoverImage ?? ""
            contentType = HungamaPlayer.ContentType.returnStringValue(contentDetail?.meta?.contentType ?? "")
        }
        
        hungamaContent = Content(withId: contentId, title: contentTitle, posterURL: contentPosterImage , type: contentType ?? HungamaPlayer.ContentType.unknown)
        
        let url = URL(string: "https://www.apple.com")
        if isResume ?? false && contentData != nil {
            if contentData?.lastWatch?.contentType == ContentType.tvShows.rawValue || contentData?.lastWatch?.contentType == ContentType.series.rawValue {
                self.configurePlayer(makePlayer(url!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType ?? "", isTrailerPlaying: isTrailer, isEpisode: true, isHungamaContent: true, hungamaContent: hungamaContent))
            } else {
                self.configurePlayer(makePlayer(url!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType ?? "", isTrailerPlaying: isTrailer, isEpisode: false, isHungamaContent: true, hungamaContent: hungamaContent))
            }
        } else if contentData != nil {
            self.configurePlayer(makePlayer(url!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType ?? "", isTrailerPlaying: isTrailer, isEpisode: false, isHungamaContent: true, hungamaContent: hungamaContent))
        } else {
            self.configurePlayer(makePlayer(url!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType ?? "", isTrailerPlaying: isTrailer, isEpisode: true, isHungamaContent: true, hungamaContent: hungamaContent))
        }
    }
    
    func configureErosNowPlayer(isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?, isTrailer:Bool) {
        var contentID = ""
        if episodeData != nil {
            contentID = episodeData?.providerContentId ?? ""
        } else {
            contentID = contentData?.meta?.providerContentId ?? ""
        }
        showLoaderAnimation()
        if BAKeychainManager().isErosNowLoggedIn {
            if isCallAlreadyMade == false {
                isCallAlreadyMade = true
                UtilityFunction.shared.getErosNowContentProfile(for: contentID) { (flagValue, contentProfile, stringMessage) in
                    self.hideActivityIndicator()
                    if flagValue {
                        var url = URL(string: "https://www.apple.com")
                        if let streamUrl = contentProfile?.streamUrl {
                            url = streamUrl
                        } else {
                            self.isCallAlreadyMade = false
                            self.erosNowPlayError(stringMessage)
                            self.thirdPartySDKFailureAnalyticsEvents(contentData: contentData, episode: episodeData, isTrailer: isTrailer, stringMessage)
                            return
                        }
                        UtilityFunction.shared.performTaskInMainQueue {
                            self.configurePlayer(self.makePlayer(url!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: self.contentDetail?.meta?.contentType ?? "", isTrailerPlaying: false, isEpisode: true, isHungamaContent: false, erosNowContent: contentProfile))
                        }
                    } else {
                        self.isCallAlreadyMade = false
                        self.apiError(stringMessage, title: kSomethingWentWrong)
                    }
                }
            } else {
                self.hideActivityIndicator()
                // Do Nothing
            }
        } else {
            UtilityFunction.shared.checkErosNowPartnerLogin { (flagValue, message) in
                if flagValue {
                    self.hideActivityIndicator()
                    self.configureErosNowPlayer(isReplay: isReplay, isResume: isResume, contentData: contentData, episodeData: episodeData, isTrailer: isTrailer)
                } else {
                    self.erosNowPlayError(message)
                    self.thirdPartySDKFailureAnalyticsEvents(contentData: contentData, episode: episodeData, isTrailer: isTrailer, message)
                }
            }
        }
    }
    
    // When Eros Now Login Failed or stream url is empty move back to Home Screen or stat on pi screen with reset player view
    func erosNowPlayError(_ errorMessage: String) {
        UtilityFunction.shared.performTaskInMainQueue {
            self.hideActivityIndicator()
            self.view.isUserInteractionEnabled = false
//            let toastMessage = "Playback Error \(errorMessage)"
            kAppDelegate.window?.makeToastOnlyForMessage(kThirdPartySDKLoginFailed) { (boolVal) in
                self.view.isUserInteractionEnabled = true
                self.erosNowLoginAttempt = 0
                self.playButtonTitle = self.previousButtonTitle
                self.removePlayerViewContent()
                self.contentDetailCell?.configurePlayPauseButton(buttonTitle: self.previousButtonTitle)
            }
        }
    }
    
    func configureSonyLivPlayer(isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?, isTrailer: Bool) {
        var contentID = ""
        if episodeData != nil {
            contentID = episodeData?.providerContentId ?? ""
        } else {
            contentID = contentData?.meta?.providerContentId ?? ""
        }
        showLoaderAnimation()
        UtilityFunction.shared.checkForSonyLivPartnerLogin { (loginStatus, errorMessage)  in
            self.hideActivityIndicator()
            if loginStatus {
                self.externalPlayerMixPanelEvents(contentData: contentData, episodeData: episodeData, isTrailer: isTrailer)
                SonyLIVSDKManager.playContent(assetID: contentID) { (sdkStatus, error) in
                    print("this is status \(sdkStatus) and this is error \(String(describing: error))")
                    switch sdkStatus {
                    case .failure:
                        BAKeychainManager().isSonyLivLoggedIn = false
                        print("Sony Liv Player failed to play")
                        break
                    case .inProgress:
                        print("Sony Liv Player status is in progress")
                        break
                    case .success:
                        print("Sony Liv Player successfully played the content")
                        break
                    }
                }
            } else {
                self.isCallAlreadyMade = false
                var toastError = ""
                let erroCodes = UtilityFunction.shared.defractErrorCode(errorMessage)
                 if let values = erroCodes {
                     if values.count > 1 {
                        toastError = values[0]
                     }
                 }
                self.sonyLivLoginFailed(toastError)
                self.thirdPartySDKFailureAnalyticsEvents(contentData: contentData, episode: episodeData, isTrailer: isTrailer, errorMessage)
            }
        }
    }
    
    //Sony Liv and Eros MixPanel Events Added
    func externalPlayerMixPanelEvents(contentData: ContentDetailData?, episodeData: Episodes?, isTrailer: Bool) {
        var contentTitle = ""
        var contentId = ""
        var contentType = ""
        var partnerName = ""
        let genreResult = contentDetail?.meta?.genre?.joined(separator: ",") ?? ""
        let languageResult = contentDetail?.meta?.audio?.joined(separator: ",")
        if episodeData != nil {
            contentTitle = episodeData?.title ?? ""
            contentId = isTrailer ? contentDetail?.meta?.partnerTrailerInfo ?? "" : contentDetail?.meta?.providerContentId ?? ""
            contentType = episodeData?.contentType ?? ""
        } else {
            contentType = contentDetail?.meta?.contentType ?? ""
            switch contentType {
            case ContentType.series.rawValue:
                contentTitle = self.contentDetail?.meta?.seriesTitle ?? ""
            case ContentType.brand.rawValue:
                contentTitle = self.contentDetail?.meta?.brandTitle ?? ""
            case ContentType.customBrand.rawValue:
                contentTitle = self.contentDetail?.meta?.brandTitle ?? ""
            case ContentType.customSeries.rawValue:
                contentTitle = self.contentDetail?.meta?.seriesTitle ?? ""
            default:
                contentTitle = self.contentDetail?.meta?.vodTitle ?? ""
            }
            contentId = isTrailer ? contentDetail?.meta?.partnerTrailerInfo ?? "" : contentDetail?.meta?.providerContentId ?? ""
            print(contentId)
            partnerName = self.contentDetail?.meta?.provider ?? ""
            self.playbackEventTracking(contentTitle: contentTitle, contentType: contentType, contentGenre: genreResult, partnerName: partnerName, languages: languageResult ?? "", railName: railName, pageSource: pageSource, configSource: configSource)
        }
    }
    
    
    // When Sony Liv Login Failed PI screen with reset player view
    // Error message is updated after abinav and shilpi confirmation on 7th may testing call with client
    func sonyLivLoginFailed(_ errorMessage: String) {
        UtilityFunction.shared.performTaskInMainQueue {
            self.hideActivityIndicator()
            let toastMessage = "Playback Error \(errorMessage)"
            self.view.isUserInteractionEnabled = false
            kAppDelegate.window?.makeToastOnlyForMessage(toastMessage) { (boolVal) in
                self.updateSonylivPlayerState()
            }
        }
    }
    
    
    func thirdPartySDKFailureAnalyticsEvents(contentData: ContentDetailData?, episode: Episodes?, isTrailer: Bool, _ loginFailureReason: String) {
        if contentDetail != nil {
            let title = contentDetail?.meta?.vodTitle ?? ""
            let contentGenre = contentDetail?.meta?.genre ?? []
            let videoContentType = contentDetail?.meta?.contentType ?? ""
            let genreResult = contentGenre.joined(separator: ",")
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.failurePlayback.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.reason.rawValue: loginFailureReason])
        } else {
            let title = episode?.id ?? 0
            let contentGenre = episode?.genres ?? []
            let videoContentType = episode?.contentType ?? ""
            let genreResult = contentGenre.joined(separator: ",")
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.failurePlayback.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.reason.rawValue: loginFailureReason])
        }
    }
    
    
    
    func updateSonylivPlayerState() {
        self.view.isUserInteractionEnabled = true
//        self.playButtonTitle = self.previousButtonTitle
//        self.removePlayerViewContent()
//        self.contentDetailCell?.configurePlayPauseButton(buttonTitle: self.previousButtonTitle)
    }

    func configureVootPlayer(isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?) {
        if isCallAlreadyMade == false {
            isCallAlreadyMade = true
            if let dataModel = vootPlaybackViewModal {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.makePlayabaleContentCall {(flagValue, message, isApiError) in
                    if flagValue {
                        if isApiError {
                            self.showOkAlertWithCustomTitle(message ?? "", title: "Video unavailable")
                            self.contentDetailCell?.playButton.selectedButton(title: self.previousButtonTitle, iconName: "play")
                            self.playButtonTitle = self.previousButtonTitle
                            self.isCallAlreadyMade = false
                        } else {
                            self.configureVootPlayerView(isReplay: isReplay, vootPlayback: self.vootPlaybackViewModal?.playbackData, isResume: isResume, contentData: contentData, episodeData: episodeData)
                        }
                    } else if isApiError {
                        self.isCallAlreadyMade = false
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.isCallAlreadyMade = false
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                    self.hideActivityIndicator()
                }
            }
        } else {
            // Do Nothing
        }
    }
    
    func configureVootPlayerView(isReplay: Bool?, vootPlayback: VootPlaybackData?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?) {
        var playableUrl = ""
        let kTataSkyFPSCertificateURL = vootPlayback?.drm?.first?.licenseURL ?? ""
        TTNPlayerConfiguration.sharedInstance.fpsCertificateUrl(kTataSkyFPSCertificateURL)
        TTNPlayerConfiguration.sharedInstance.drmInfo(data: vootPlayback?.drm?.first?.certificate ?? "", drmType: contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue ? .kaltura : .kidskaltura)
        playableUrl = vootPlayback?.url ?? ""
        self.hideActivityIndicator()
        if playableUrl.isEmpty {
            self.isCallAlreadyMade = false
            //otpResent("Error. Content not found!", completion: nil)
            //self.showOkAlert("Unable To Fetch Playback Url")
        } else {
            isContentPlaying = true
            ///Voot Movie
            if isResume ?? false && contentData != nil {
                if contentData?.lastWatch?.contentType == ContentType.tvShows.rawValue || contentData?.lastWatch?.contentType == ContentType.series.rawValue {
                    self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true))
                } else {
                    self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
                }
            } else if contentData != nil {
                self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
            } else {
                self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true))
            }
        }
    }
    
    
    func getPlayableUrlForTrailer(signedUrl: String?, cell: BAContentDetailTableViewCell?) {
        // Create URL
        let url = URL(string: signedUrl ?? "")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
                do {
                    let shemarooContent = try JSONDecoder().decode(ShemarooContentModal.self, from: data)
                    print(shemarooContent)
                    let playbackUrl = shemarooContent.adaptive_urls?[0].playback_url ?? ""
                    DispatchQueue.main.async {
                        //cell?.configurePlayer(self.makePlayer(URL(string: playbackUrl)!))
                        ///Shemaroo trailer
                        print("********************* In Trailer **********************", self.player?.playerView.isHidden)
                        self.configurePlayer(self.makePlayer(URL(string: playbackUrl)!, content: self.contentDetail, isMarkedFav: self.isAddedToFav))
                        //cell?.contentPosterImage.image = nil
                    }
                } catch {
                    DispatchQueue.main.async {
                    }
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    // MARK: Get Playabkle Content For Shemaroo Me
    func getPlayableUrl(signedUrl: String?, isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?) {
        if isCallAlreadyMade == false {
            isCallAlreadyMade = true
            showActivityIndicator(isUserInteractionEnabled: false)
            // Create URL
            let url = URL(string: signedUrl ?? "")
            guard let requestUrl = url else { fatalError() }
            // Create URL Request
            var request = URLRequest(url: requestUrl)
            // Specify HTTP Method to use
            request.httpMethod = "GET"
            // Send HTTP Request
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                // Check if Error took place
                if let error = error {
                    self.isCallAlreadyMade = false
                    print("Error took place \(error)")
                    self.hideActivityIndicator()
                    self.apiError(error.localizedDescription, title: "")
                    return
                }
                
                // Read HTTP Response Status code
                if let response = response as? HTTPURLResponse {
                    print("Response HTTP Status code: \(response.statusCode)")
                }
                
                // Convert HTTP Response Data to a simple String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                    do {
                        let shemarooContent = try JSONDecoder().decode(ShemarooContentModal.self, from: data)
                        print(shemarooContent)
                        let playbackUrl = shemarooContent.adaptive_urls?[0].playback_url ?? ""//"http://sample.vodobox.com/planete_interdite/planete_interdite_alternate.m3u8"////
                        DispatchQueue.main.async {
                            self.hideActivityIndicator()
                            if playbackUrl == "" {
                                self.isCallAlreadyMade = false
                                self.otpResent("Error. Content not found!", completion: nil)
                                // self.showOkAlert("Unable To Fetch Playback Url")
                            } else {
                                self.isContentPlaying = true
                                self.isCallAlreadyMade = false
                                ///Shemaroo Movie
                                if isResume ?? false && contentData != nil {
                                    if contentData?.lastWatch?.contentType == ContentType.tvShows.rawValue || contentData?.lastWatch?.contentType == ContentType.series.rawValue {
                                        self.configurePlayer(self.makePlayer(URL(string: playbackUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: self.contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true))
                                    } else {
                                        self.configurePlayer(self.makePlayer(URL(string: playbackUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: self.contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
                                    }
                                } else if contentData != nil {
                                    self.configurePlayer(self.makePlayer(URL(string: playbackUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: self.contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
                                } else {
                                    self.configurePlayer(self.makePlayer(URL(string: playbackUrl)!, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: self.contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true))
                                }
                            }
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.isCallAlreadyMade = false
                            self.hideActivityIndicator()
                        }
                        print(error)
                    }
                }
            }
            task.resume()
        } else {
            
        }
    }
    
    func startVideoTimer(cell: BAContentDetailTableViewCell?) {
        if videoTimer == nil {
            videoTimer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(configurePlayerView), userInfo: nil, repeats: false)
            RunLoop.current.add(videoTimer!, forMode: RunLoop.Mode.common)
        }
    }
    
    func stopVideoTimer() {
        if videoTimer != nil {
            videoTimer?.invalidate()
            videoTimer = nil
        }
    }
    
    @objc func configurePlayerView() {
        if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue {
            let md5SignatureData = MD5(string: smarturl_accesskey + (contentDetail?.meta?.partnerTrailerInfo ?? "") + "?" + smarturl_parameters)
            let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
            print("md5SignatureHex: \(md5SignatureHex)")
            let signedUrl = (contentDetail?.meta?.partnerTrailerInfo ?? "") + "?" + smarturl_parameters + md5SignatureHex
            self.getPlayableUrlForTrailer(signedUrl: signedUrl, cell: contentDetailCell)
        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            // DO Nothing
        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            // Do nothing
        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
            ///Curosity Trailer
            configurePlayer(self.makePlayer(URL(string: contentDetail?.detail?.trailerUrl ?? "")!,content: self.contentDetail ,isMarkedFav: self.isAddedToFav))
        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue {
            configureHungamaPlayer(isReplay: false, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: true)
        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue {
            configureSonyLivPlayer(isReplay: false, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: true)
        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.eros.rawValue {
            configureErosNowPlayer(isReplay: false, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: true)
        }
    }
    
    func makeContinueWatchCall(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?) {
        continueWatchingVM = BAWatchingViewModal(repo: BAWatchingRepo())
        continueWatchingVM?.apiParams["id"] = contentId
        continueWatchingVM?.apiParams["contentType"] = contentType
        continueWatchingVM?.apiParams["watchDuration"] = (watchedDuration ?? 0)
        continueWatchingVM?.apiParams["totalDuration"] = totalDuration ?? 0
        continueWatchingVM?.getWatchingtData({ _, _, isApiError  in
        })
    }
}

/**
 ********************************************
 VOD Detail Extension Methods
 ********************************************
 **/
extension BAVODDetailViewController {
    func makePlayer(_ url: URL, cookieString: String = "", content: ContentDetailData? = nil, episode: Episodes? = nil, replay: Bool? = nil, contentType: String? = "", isTrailerPlaying: Bool = true, isEpisode: Bool = false, isMarkedFav: Bool = false, isHungamaContent: Bool = false, hungamaContent: IContentVO? = nil, erosNowContent: ENContentProfile? = nil) -> BAPlayerViewController {
        
        if let player = player {
            player.isPlayerPlaying = isVideoPlaying ?? true
            player.delegate = self
            return player
        }
        let player = BAPlayerViewController(url, cookieString: cookieString, contentData: content, episodeDetail: episode, isReplayContent: replay, videoType: contentType, contentId: watchlistVodId, isTrailerPlaying: isTrailerPlaying, hungamaContent: hungamaContent, isEpisodePlay: isEpisode, isAddedToFav: isAddedToFav, erosNowContent: erosNowContent)
        if isVideoPlaying == false && isTrailerPlayed == true {
            player.isPlayerPlaying = true
        } else {
            player.isPlayerPlaying = isVideoPlaying ?? true
        }
        // Add in Main Thread
        UtilityFunction.shared.performTaskInMainQueue {
            self.contentDetailCell?.trailerButton.setTitleColor(.BASecondaryGray, for: .normal)
        }
        player.delegate = self
        self.player = player
        return player
    }
    
    func makeFullScreenPlayer(_ url: URL, cookieString: String, content: ContentDetailData?, episode: Episodes?, replay: Bool?, contentType: String?) -> BAPlayerFullScreenViewController {
        let player = BAPlayerFullScreenViewController(url, cookieString: cookieString, contentData: content, episodeDetail: episode, isReplayContent: replay, videoType: contentType, contentId: watchlistVodId)
        return player
    }
    
    func updateDataSourceArray() {
        dataSourceArray.removeAll()
        if let contentDetail = contentDetail {
            dataSourceArray.append(contentDetail)
        }
        
        
        if let series = contentDetail?.seriesList, !series.isEmpty {
            dataSourceArray.append(series)
        }
        
        if let data = recommendedContentVM?.recommendedData?.data {
            dataSourceArray.append(data)
        }
    }
    
    func configureZeePlayback(isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?) {
        //self.configureZeePlayerView(isReplay: isReplay, zeeTagData: self.zeeTagViewModal?.zeeTag?.tag, isResume: isResume, contentData: contentData, episodeData: episodeData)
        if isCallAlreadyMade == false {
            if let dataModel = zeeTagViewModal {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getZeeTagDetail {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        print(self.zeeTagViewModal?.zeeTag)
                        self.configureZeePlayerView(isReplay: isReplay, zeeTagData: self.zeeTagViewModal?.zeeTag?.tag, isResume: isResume, contentData: contentData, episodeData: episodeData)
                    } else if isApiError {
                        //self.isCallAlreadyMade = false
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        //self.isCallAlreadyMade = false
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                }
            }
        } else {
            // Do Nothing
        }
    }
    
    func configureZeePlayerView(isReplay: Bool?, zeeTagData: String?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?) {
        var playableUrl = ""
        if contentData != nil {
            let genreResult = contentDetail?.meta?.genre?.joined(separator: ",") ?? ""
            let languageResult = contentDetail?.meta?.audio?.joined(separator: ",")
            if contentData?.lastWatch != nil {
                playbackEventTracking(contentTitle: contentData?.lastWatch?.contentTitle ?? "", contentType: contentData?.lastWatch?.contentType ?? "", contentGenre: genreResult, partnerName: contentData?.meta?.provider ?? "", languages: languageResult ?? "", railName: railName, pageSource: pageSource, configSource: configSource)
                //                if self.contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                //                    if self.contentDetail?.lastWatch?.partnerDeepLinkUrl?.isEmpty ?? true || contentDetail?.lastWatch?.partnerDeepLinkUrl == nil {
                //                        otpResent("Error. Content not found!", completion: nil)
                //                        isCallAlreadyMade = false
                //                        return
                //                    } else {
                //                        playableUrl = contentData?.lastWatch?.partnerDeepLinkUrl ?? ""
                //                    }
                //                } else {
                
                if self.contentDetail?.meta?.partnerWebUrl?.isEmpty ?? true || contentDetail?.meta?.partnerWebUrl == nil {
                    otpResent("Error. Content not found!", completion: nil)
                    isCallAlreadyMade = false
                    return
                } else {
                    playableUrl = contentData?.meta?.partnerWebUrl ?? ""
                }
                
                //}
            } else {
                var videoTitle = ""
                switch contentData?.meta?.contentType {
                case ContentType.series.rawValue:
                    videoTitle = contentDetail?.meta?.seriesTitle ?? ""
                case ContentType.brand.rawValue:
                    videoTitle = contentDetail?.meta?.brandTitle ?? ""
                case ContentType.customBrand.rawValue:
                    videoTitle = contentDetail?.meta?.brandTitle ?? ""
                case ContentType.customSeries.rawValue:
                    videoTitle = contentDetail?.meta?.seriesTitle ?? ""
                default:
                    videoTitle = contentDetail?.meta?.vodTitle ?? ""
                }
                playbackEventTracking(contentTitle: videoTitle, contentType: contentData?.meta?.contentType ?? "", contentGenre: genreResult, partnerName: contentData?.meta?.provider ?? "", languages: languageResult ?? "", railName: railName, pageSource: pageSource, configSource: configSource)
                if self.contentDetail?.meta?.partnerWebUrl?.isEmpty ?? true || contentDetail?.meta?.partnerWebUrl == nil {
                    //showOkAlertWithCustomTitle("Not able to deeplink", title: "Video unavailable")
                    otpResent("Error. Content not found!", completion: nil)
                    isCallAlreadyMade = false
                    return
                } else {
                    playableUrl = contentData?.meta?.partnerWebUrl ?? ""
                }
            }
        } else {
            let genreResult = episodeData?.genres?.joined(separator: ",") ?? ""
            let languageResult = episodeData?.languages?.joined(separator: ",")
            playbackEventTracking(contentTitle: episodeData?.title ?? "", contentType: episodeData?.contentType ?? "", contentGenre: genreResult, partnerName: episodeData?.provider ?? "", languages: languageResult ?? "", railName: railName, pageSource: pageSource, configSource: configSource)
            if playEpisode?.partnerWebUrl?.isEmpty ?? true || playEpisode?.partnerWebUrl == nil {
                otpResent("Error. Content not found!", completion: nil)
                isCallAlreadyMade = false
                return
            } else {
                playableUrl = playEpisode?.partnerWebUrl ?? ""
            }
        }
        let basePlayUrl = UtilityFunction.splitAtFirst(str: playableUrl, delimiter: "?")
        let playUrl = "\(basePlayUrl?.firstString ?? "")?utm_source=tataskybinge&utm_medium=amazonstick&utm_campaign=zee5campaign&partner=tataskybinge"
        playableUrl = playUrl + "&tag=" + (zeeTagData ?? "")
        // playableUrl = playableUrl + "&tag=" + (zeeTagData ?? "")//"1f68dae8658f2513a55f5a2fbe1b3c30"//
        if playableUrl.isEmpty {
            self.isCallAlreadyMade = false
            otpResent("Error. Content not found!", completion: nil)
            isCallAlreadyMade = false
        } else {
            isContentPlaying = true
            if isResume ?? false && contentData != nil {
                if contentData?.lastWatch?.contentType == ContentType.tvShows.rawValue || contentData?.lastWatch?.contentType == ContentType.series.rawValue {
                    self.configureZeePlayer(playableUrl, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true)
                } else {
                    self.configureZeePlayer(playableUrl, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false)
                }
            } else if contentData != nil {
                self.configureZeePlayer(playableUrl, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false)
            } else {
                self.configureZeePlayer(playableUrl, cookieString: "", content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true)
            }
        }
    }
    
    func configureZeePlayer(_ url: String, cookieString: String = "", content: ContentDetailData? = nil, episode: Episodes? = nil, replay: Bool? = nil, contentType: String? = "", isTrailerPlaying: Bool = true, isEpisode: Bool = false, isMarkedFav: Bool = false, isHungamaContent: Bool = false, hungamaContent: IContentVO? = nil) {
        if let url = URL(string: url) {
            let safariView = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            safariView.delegate = self
            safariView.modalPresentationStyle = .fullScreen
            if #available(iOS 13.0, *) {
                safariView.isModalInPresentation = true
            } else {
                // Fallback on earlier versions
            }
            present(safariView, animated: true)
        }
    }
    
    func playbackEventTracking(contentTitle: String, contentType: String, contentGenre: String, partnerName: String, languages: String, railName: String, pageSource: String, configSource: String) {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: contentTitle, MixpanelConstants.ParamName.contentType.rawValue: contentType, MixpanelConstants.ParamName.contentGenre.rawValue: contentGenre, MixpanelConstants.ParamName.startTime.rawValue: "", MixpanelConstants.ParamName.stopTime.rawValue: "", MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: "", MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: "", MixpanelConstants.ParamName.numberOfResume.rawValue: "", MixpanelConstants.ParamName.numberOfPause.rawValue: "", MixpanelConstants.ParamName.duraionMinunte.rawValue: "", MixpanelConstants.ParamName.durationSeconds.rawValue: "", MixpanelConstants.ParamName.partnerName.rawValue: partnerName, MixpanelConstants.ParamName.contentLanguage.rawValue: languages, MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource,MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        zeeSafariPlayerDismissed()
        dismiss(animated: true)
    }
    
    func zeeSafariPlayerDismissed() {
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        playButtonTitle = "Resume"
        isCallAlreadyMade = false
        isContentPlaybackStarted = false
        //fetchLastWatch(isZeePlayerDismissed: true)
        //contentDetailCell?.playButton.selectedButton(title: "Resume", iconName: "play")
    }
    
    // MARK: Configure Curosity Stream Player
    
    // Cookie string has been set to empty string as discussed
    func confugureCurosityStreamPlayer(isReplay: Bool?, isResume: Bool?, contentData: ContentDetailData?, episodeData: Episodes?) {
        
        CustomLoader.shared.showLoader(on: playerViewArea)
        var playableUrl = ""
        isCallAlreadyMade = true
        isContentPlaying = true
        if contentData != nil {
            if contentDetail?.lastWatch != nil && contentData?.lastWatch?.secondsWatched ?? 0 > 0 {
                //self.contentDetail?.lastWatch?.playerDetail?.playUrl = playUrl
                if playUrl.isEmpty {
                    showOkAlertWithCustomTitle("We are unable to play your video right now. Please try again in a few minutes", title: "Video unavailable")
                    return
                } else {
                    playableUrl = playUrl
                }
            } else {
                //self.contentDetail?.detail?.playUrl = playUrl
                if playUrl.isEmpty {
                    showOkAlertWithCustomTitle("We are unable to play your video right now. Please try again in a few minutes", title: "Video unavailable")
                    return
                } else {
                    playableUrl = playUrl
                }
                var vodId = 0
                switch self.contentDetail?.meta?.contentType {
                case ContentType.series.rawValue:
                    vodId = contentDetail?.meta?.seriesId ?? 0
                case ContentType.brand.rawValue:
                    vodId = contentDetail?.meta?.brandId ?? 0
                case ContentType.customBrand.rawValue:
                    vodId = contentDetail?.meta?.brandId ?? 0
                case ContentType.customSeries.rawValue:
                    vodId = contentDetail?.meta?.seriesId ?? 0
                default:
                    vodId = contentDetail?.meta?.vodId ?? 0
                }
                makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
            }
        } else {
            playableUrl = episodeData?.playerDetail?.playUrl ?? ""
        }

        
        if isResume ?? false && contentData != nil {
            if contentData?.lastWatch?.contentType == ContentType.tvShows.rawValue || contentData?.lastWatch?.contentType == ContentType.series.rawValue {
                self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: ""/*cookie*/, content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true))
            } else {
                self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: ""/*cookie*/, content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
            }
            
        } else if contentData != nil {
            self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: ""/*cookie*/, content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: false))
            
        } else {
            self.configurePlayer(self.makePlayer(URL(string: playableUrl)!, cookieString: ""/*cookie*/, content: contentData, episode: episodeData, replay: isReplay, contentType: contentDetail?.meta?.contentType, isTrailerPlaying: false, isEpisode: true))
        }
    }


}

extension BAVODDetailViewController: OrientationHandlable {
    var blockRotation: Bool {
        player == nil && isMoviePlaying == false
    }
}


extension HungamaPlayer.ContentType {
    static func returnStringValue(_ contentType: String) -> HungamaPlayer.ContentType?{
        if contentType == "MUSIC" {
            return .musicVideo
        } else if contentType == "MOVIE" || contentType == "MOVIES" {
            return .movie
        } else if contentType == "VOD" {
            return .shortVideo
        } else if contentType == "" {
            return .unknown
        } else if contentType == "TV_SHOWS" || contentType == "SERIES" || contentType == "BRAND" {
            return .tvShowEpisode
        }
        return .unknown
    }
}

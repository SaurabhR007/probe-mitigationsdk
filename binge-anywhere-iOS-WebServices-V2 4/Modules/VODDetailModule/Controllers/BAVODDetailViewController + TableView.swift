//
//  BAVODDetailViewController + TableView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 28/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

// MARK: TableView Delegate and Datasource Methods
extension BAVODDetailViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let data = dataSourceArray[section]
        if data is ContentDetailData {
            return 1
        }

        if let series = data as? [SeriesYearsList], !series.isEmpty, series.count >= selectedSeason {
            guard let seriesDetail = series[selectedSeason].seriesDetail, let items = seriesDetail.items, let total = seriesDetail.total else { return 0 }
            var numberOfRows = items.count
            if items.count < total {
                numberOfRows += 1
            }
            return numberOfRows
        }

        if data is RecommendedContentData {
            return 1
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var idx = indexPath.section
        if dataSourceArray.count == indexPath.section {
            idx = indexPath.section - 1
        }
        
        let data = dataSourceArray[idx]

        if let data = data as? ContentDetailData {
            /*
            if indexPath.row == 0 {
                let cell = vodDetailTableView.dequeueReusableCell(withIdentifier: kBAAppPosterTableViewCell) as! BAAppPosterTableViewCell
                //cell.configureAppPoster(appName: appName)
                cell.backgroundColor = .BAdarkBlueBackground
                return cell
            } else {
            */
                print("Is Cell reloading")
                let cell = vodDetailTableView.dequeueReusableCell(withIdentifier: kContentDetailCell, for: indexPath) as?  BAContentDetailTableViewCell
                contentDetailCell = cell
                cell?.configureContentData(contentDetail: data, isExpanded: shouldShowFullDesc, expiryTime: timestamp, buttonTitle: playButtonTitle, shouldButtonsReload: shoudlButtonsReloaded)
                cell?.isViewReloaded = isTableReloaded
                cell?.delegate = self
                cell?.contentDetaillabel.delegate = self
                if BAKeychainManager().isAutoPlayOn == true && data.meta?.contentType != "BRAND" {
                    print("Is Cell reloading1")
                    if isMovedToPlayer {
                        // Do Nothing
                    } else {
                        if shouldWaitForPlaying {
                            print("Is Cell reloading2")
                            startVideoTimer(cell: cell)
                        } else {
                            print("Is Cell reloading3")
                            if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                                let md5SignatureData = MD5(string: smarturl_accesskey + (contentDetail?.meta?.partnerTrailerInfo ?? "") + "?" + smarturl_parameters)
                                let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                                print("md5SignatureHex: \(md5SignatureHex)")
                                let signedUrl = (contentDetail?.meta?.partnerTrailerInfo ?? "") + "?" + smarturl_parameters + md5SignatureHex
                                self.getPlayableUrlForTrailer(signedUrl: signedUrl, cell: cell)
                            } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                                    configurePlayer(self.makePlayer(URL(string: contentDetail?.detail?.trailerUrl ?? "")!))
                            } else {
                                //cell?.configurePlayer(self.makePlayer(URL(string: "")!))
                            }
                        }
                    }
                    return cell!
                } else {
                    if isTrailerPlayed == true {
                        print("Is Cell reloading4")
                        if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                            let md5SignatureData = MD5(string: smarturl_accesskey + (contentDetail?.meta?.partnerTrailerInfo ?? "") + "?" + smarturl_parameters)
                            let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                            print("md5SignatureHex: \(md5SignatureHex)")
                            let signedUrl = (contentDetail?.meta?.partnerTrailerInfo ?? "") + "?" + smarturl_parameters + md5SignatureHex
                            self.getPlayableUrlForTrailer(signedUrl: signedUrl, cell: cell)
                        } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                           // contentDetailCell?.configurePlayer(self.makePlayer(URL(string: contentDetail?.detail?.trailerUrl ?? "")!))
                            configurePlayer(self.makePlayer(URL(string: contentDetail?.detail?.trailerUrl ?? "")!))
                        } else {
                            //cell?.configurePlayer(self.makePlayer(URL(string: "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8")!))
                        }
                    } else {
                        
                    }
                    // Do Nothing
               // }
                return cell!
            }
            
            // TODO: Again to be implemented once trailer are received from backend
            /*
             if /*data.detail?.trailerUrl?.count ?? 0 > 0 && */BAUserDefaultManager().isAutoPlayOn == true {
             DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
             cell?.configurePlayer(self.makePlayer(URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")!))
             }
             
             } else {
             // Do Nothing
             }*/
        }

        if let series = data as? [SeriesYearsList] {
            let episodes = series[selectedSeason].seriesDetail?.items
            allEpisodes = episodes
            if indexPath.row < (episodes?.count ?? 0) {
                let cell = vodDetailTableView.dequeueReusableCell(withIdentifier: kBAEpisodeTableViewCell, for: indexPath) as?  BAEpisodeTableViewCell
                cell?.delegate = self
                cell?.configureEpisodes(episodeData: allEpisodes?[indexPath.row], index: indexPath)
                return cell!
            } else {
                let cell = vodDetailTableView.dequeueReusableCell(withIdentifier: kBASeriesFooterViewTableViewCell, for: indexPath) as? BASeriesFooterViewTableViewCell
                cell?.delegate = self
                return cell!
            }
        }

        if let data = data as? RecommendedContentData {
            let cell = vodDetailTableView.dequeueReusableCell(withIdentifier: kBAHomeRailTableViewCell, for: indexPath) as! BAHomeRailTableViewCell
            cell.delegate = self
            //cell.isViewReloaded = isTableReloaded
            cell.configureCellWithRecommendedData(recommendedContent: data)
            return cell
        }
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if BAReachAbility.isConnectedToNetwork() {
            if dataSourceArray.count == indexPath.section {
                return
            }
            let data = dataSourceArray[indexPath.section]
            if let series = data as? [SeriesYearsList] {
                let episodes = series[selectedSeason].seriesDetail?.items
                allEpisodes = episodes
                if indexPath.row < (episodes?.count ?? 0) {
                    let cell = vodDetailTableView.cellForRow(at: indexPath) as? BAEpisodeTableViewCell
                    if episodes?[indexPath.row].provider?.uppercased() == ProviderType.zee.rawValue || episodes?[indexPath.row].provider?.uppercased() == ProviderType.hotstar.rawValue {
                        // Do Nothing
                    } else {
                        if player != nil {
                            self.removePlayerViewContent()
                            player?.view.removeFromSuperview()
                            player = nil
                        }
                        self.isCallAlreadyMade = false
                        playButtonTitle = ""
                        contentDetailCell?.playButton.selectedButton(title: "Pause", iconName: "")
                    }
                    self.showDTHAlerts()
                    
                    let playbackStatus = checkForPlaybackAvailabilityStatus()
                    if playbackStatus {
                        cell?.delegate?.moveToPlayEpisode(episode: episodes?[indexPath.row], isResume: false, isPlayerInPausedState: false)
                    } else {
//                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus == kTempSuspended {
//                            apiError(kTempSuspendedMessage, title: kTempSuspendedHeader) {
//                                //self.signOut()
//                            }
//                        } else if BAKeychainManager().acccountDetail?.accountStatus == kPartialDunnedState || BAKeychainManager().acccountDetail?.accountStatus == kPartialDunned {
//                            apiError(kPartiallyDunnedMessage, title: kTempSuspendedHeader) {
//                                //self.signOut()
//                            }
//                        }
                    }
                }
            }
        } else {
            noInternet()
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let data = dataSourceArray[section]
        if let series = data as? [SeriesYearsList], !series.isEmpty {
            return 50
        }
        return -1
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = dataSourceArray[section]
        if let series = data as? [SeriesYearsList], !series.isEmpty {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BASeriesDetailTableViewCell") as! BASeriesDetailTableViewCell
            headerView.backgroundColor = .BAdarkBlueBackground
            headerView.configureCollectionView(seriesList: series, selectedSeason: selectedSeason, contentType: contentDetail?.meta?.contentType, contentDetail: contentDetail)
            headerView.delegate = self
            return headerView
        }
        return nil
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if dataSourceArray.count == indexPath.section {
            return UITableView.automaticDimension
        }
        
        let data = dataSourceArray[indexPath.section]

        if data is ContentDetailData {
            return UITableView.automaticDimension
        }

        if data is [SeriesYearsList] {
            guard let cell = tableView.cellForRow(at: indexPath) as? BAEpisodeTableViewCell else {
                return UITableView.automaticDimension
            }
            return cell.height()
        }

        if data is RecommendedContentData {
            let layoutType = recommendedContentVM?.recommendedData?.data?.layoutType
            switch layoutType {
            case LayoutType.landscape.rawValue:
                return LayoutType.landscape.itemHeight
            case LayoutType.potrait.rawValue:
                return LayoutType.potrait.itemHeight
            case LayoutType.circular.rawValue:
                return LayoutType.circular.itemHeight
            default:
                return UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

extension BAVODDetailViewController: ExpandableLabelDelegate {
    func willExpandLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        vodDetailTableView.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        shouldShowFullDesc = true
        vodDetailTableView.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        vodDetailTableView.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        shouldShowFullDesc = false
        vodDetailTableView.endUpdates()
    }
}

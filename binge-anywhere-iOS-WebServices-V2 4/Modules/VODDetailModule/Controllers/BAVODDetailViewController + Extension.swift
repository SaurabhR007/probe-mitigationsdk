//
//  BAVODDetailViewController + Extension.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 28/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import CommonCrypto
import ARSLineProgress
import Kingfisher

/**
 ********************************************
 Content Detail Cell Delegate Methods
 ********************************************
 **/
extension BAVODDetailViewController: TableViewCellDelegate {
    
    func moveToBrowseByDetail(intent: String?, genre: String?, language: String?, pageSource: String?, configSource: String?, railName: String?) {
        // Do Nothing
    }
    
    func moveToDetailForPageType(pageType: String?, appname: String?, isSubscribed: Bool?) {
        // Do Nothing
    }
    
    func moveToDetailScreen(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, timestamp: Int?, pageSource: String?, configSource: String?, railName: String?) {
        fetchContentDetail(railType: railType, layoutType: layoutType, vodId: vodId, contentType: contentType, expiry: timestamp, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    func moveToSeeAllScreen(_ data: HomeScreenDataItems?, _ index: Int) {
        // Do Nothing
    }
    
    func moveToSeeAllScreenFromVOD(_ data: RecommendedContentData?) {
            if self.player != nil {
                self.removePlayerViewContent()
                self.player?.view.removeFromSuperview()
                self.player = nil
            }
            self.contentDetailCell?.playButton.selectedButton(title: "Resume", iconName: "play")
            self.playButtonTitle = ""
        let seeAllVC: BARecommendedSeeAllVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.recommendedSeeAllVC.rawValue, type: BARecommendedSeeAllVC.self)
        seeAllVC.recommendationData = data
        if let data = data?.contentList {
            seeAllVC.recommendationData?.contentList = UtilityFunction.shared.filterTAData(data)
        }
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: "Recommendation", MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "NO", MixpanelConstants.ParamName.railPosition.rawValue: "", MixpanelConstants.ParamName.partner.rawValue: "MIX", MixpanelConstants.ParamName.pageName.rawValue: "Content-Detail"])
        seeAllVC.contentId = self.id ?? 0
        seeAllVC.pageSource = "Content-Detail"
        seeAllVC.configSource = "Recommendation"
        seeAllVC.railName = data?.title ?? ""
//        if railType == TAConstant.recommendationType.rawValue {
//            self.layoutType = "LANDSCAPE"
//        } else {
//            self.layoutType = data?.layoutType
//        }
        seeAllVC.layoutType = data?.layoutType//self.layoutType
        seeAllVC.contentDetail = self.contentDetail
        seeAllVC.isTAData = (railType == TAConstant.recommendationType.rawValue)
        self.navigationController?.pushViewController(seeAllVC, animated: true)
    }
    
    // MARK: Content Detail API Calling
    func fetchContentDetail(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, expiry: Int?, pageSource: String?, configSource: String?, railName: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: "\(railContentType)/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId, layoutType: layoutType, railType: railType, expiryTime: expiry, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    // MARK: Content Detail Response Handling
    func getContentDetailData(vodId: Int?, layoutType: String?, railType: String?, expiryTime: Int?, pageSource: String?, configSource: String?, railName: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    //self.hideActivityIndicator()
                    if flagValue {
                       let genreResult = dataModel.contentDetail?.meta?.genre?.joined(separator: ",")
                        let languageResult = dataModel.contentDetail?.meta?.audio?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.vodRail.rawValue: "Related-Rail", MixpanelConstants.ParamName.contentTitle.rawValue: dataModel.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentType.rawValue: dataModel.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: dataModel.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.source.rawValue: "Content-Detail", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.origin.rawValue: "Editorial", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        self.fetchLastWatch(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiryTime, contentDetail: dataModel.contentDetail)
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, contentDetail: ContentDetailData?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiry, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId)
    }
    
    func fetchWatchlistLookUp(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(railType: railType, layoutType: layoutType, vodId: vodId, expiry: expiry, contentDetail: contentDetail)
    }
    
    func confgureWatchlistIcon(railType: String?, layoutType: String?, vodId: Int?, expiry: Int?, contentDetail: ContentDetailData?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = watchlistLookupVM {
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if self.player != nil {
                        self.removePlayerViewContent()
                        self.player?.view.removeFromSuperview()
                        self.player = nil
                    }
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigation
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "Content-Detail"
                        contentDetailVC.configSource = "Editorial"
                        contentDetailVC.railName = "Related-Rail"
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        self.hideActivityIndicator()
                        self.parentNavigation?.pushViewController(contentDetailVC, animated: true)
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigation
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        self.hideActivityIndicator()
                        self.parentNavigation?.pushViewController(contentDetailVC, animated: true)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigation
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        self.hideActivityIndicator()
                        self.parentNavigation?.pushViewController(contentDetailVC, animated: true)
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

// MARK: - UIScrollViewDelegate
extension BAVODDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*
         if self.lastContentOffset < scrollView.contentOffset.y {
         // did move up
         if contentDetail?.meta?.contentType == ContentType.series.rawValue || contentDetail?.meta?.contentType == ContentType.series.rawValue {
         guard let seasonDetail = contentDetail?.seriesList?[selectedSeason] else { return }
         limit = seasonDetail.seriesDetail?.limit ?? 0
         offset = (seasonDetail.seriesDetail?.offset ?? 0) - (seasonDetail.seriesDetail?.limit ?? 0)
         if offset < 0 {
         // Do Nothing
         } else {
         fetchSeasonDetailForContentId(contentDetail?.meta?.seriesId ?? 0, limit: limit , offsset: offset, isAutoScroll: "false", isLastWatch: "false", isFromScroll: true)
         }
         }
         } else if self.lastContentOffset > scrollView.contentOffset.y {
         // did move down
         } else {
         // didn't move
         }
         */
        /*
         let yPos: CGFloat = scrollView.contentOffset.y
         if yPos < 1 {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResumeContent"), object: nil)
         } else {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
         }
         headerView.backgroundColor = UIColor.BAdarkBlueBackground.withAlphaComponent(yPos/154)
         *///154 is the height of PI screen image
        
    }
}

/**
 ********************************************
 Player Controller Delegate Methods
 ********************************************
 **/
extension BAVODDetailViewController: BAPlayerViewControllerDelegate {
    func configureContentDetailButtonOnUpNext() {
        playButtonTitle = "Play Now"
        contentDetailCell?.playButton.selectedButton(title: "Play Now", iconName: "")
    }
    
    func videoDidBeginAfterReplay() {
        playButtonTitle = "Pause"
        contentDetailCell?.playButton.selectedButton(title: "Pause", iconName: "")
    }
    
    func configureFavButtonForTrailer(isAddedToFavorite: Bool?) {
        if isAddedToFavorite ?? false {
            self.markFavourite.isSelected = true
            self.isAddedToFav = true
            self.markFavourite.setImage(UIImage(named: "favSelected"), for: .normal)
        } else {
            self.markFavourite.isSelected = false
            self.isAddedToFav = false
            self.markFavourite.setImage(UIImage(named: "followIcon"), for: .normal)
        }
    }
    
    func videoEndNotification() {
        playButtonTitle = "Replay"
        contentDetailCell?.playButton.selectedButton(title: "Replay", iconName: "play")
    }
    
    func playerViewControllerWillChangeLayout(isButtonTapped: Bool) {
        print("playerViewControllerWillChangeLayout in VODDEtailViewController")
        rotationButtonTapped = isButtonTapped
        let fullscreenVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.fullScreenVC.rawValue, type: BAFullScreenPlayerViewController.self)
        player?.delegate = fullscreenVC
        player?.isPlayerPlaying = isVideoPlaying ?? true
        fullscreenVC.player = player
        fullscreenVC.delegate = self
        fullscreenVC.isAddedToWatchlist = self.isAddedToFav
        fullscreenVC.contentDetail = self.contentDetail
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeSmallScreenIcon"), object: nil)
        
        if #available(iOS 13.0, *) {
            self.navigationController?.modalPresentationStyle = .fullScreen
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.present(fullscreenVC, animated: false, completion: nil)
        player?.view.removeFromSuperview()
    }
    
    func didChangePlayerPlayingState(_ isPlayerPlaying: Bool) {
        isVideoPlaying = isPlayerPlaying
    }
}

/**
 ********************************************
 Full Screen Player Delegate Methods
 ********************************************
 **/
extension BAVODDetailViewController: BAFullScreenPlayerViewControllerDelegate {
    
    func fullScreenPlayerViewControllerDidDismiss(_ controller: BAFullScreenPlayerViewController, _ isAutoRotated: Bool) {
        if controller.player.playerView.removeDeviceView?.isHidden == false {
            rotationButtonTapped = !isAutoRotated
            player?.view.removeFromSuperview()
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        } else {
            print("Is Auto Rotated ---->", isAutoRotated)
            rotationButtonTapped = !isAutoRotated
            player?.view.removeFromSuperview()
            if let player = player {
                self.configurePlayer(player)
            }
            let player = controller.player
            self.player = player
            player?.delegate = self
            shouldWaitForPlaying = false
            isMovedToPlayer = false
            DispatchQueue.main.async {
                self.shoudlButtonsReloaded = false
                self.vodDetailTableView.reloadData()
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeFullScreenIcon"), object: nil)
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        }
    }
    
    func fullScreenPlayerDidChangePlayingState(_ isPlayerPlaying: Bool) {
        isVideoPlaying = isPlayerPlaying
    }
}

extension BAVODDetailViewController: BASeriesDetailTableViewCellDelegate {
    func switchToSeason(index: Int?) {
        guard let index = index else {
            return
        }
        guard let seasonDetail = contentDetail?.seriesList?[index] else {
            return
        }
        selectedSeason = index
        if let items = seasonDetail.seriesDetail?.items, !items.isEmpty { //if data is already available just update array and reload
            updateDataSourceArray()
            UIView.transition(with: vodDetailTableView, duration: 0.35, options: [.transitionCrossDissolve, .allowUserInteraction], animations: {
                self.vodDetailTableView.reloadData()
            }) { success in
                if success {
                    self.scrollToSeasons()
                }
            }
            return
        }
        fetchSeasonDetailForContentId(seasonDetail.id ?? 0, limit: 5, offsset: 0, isAutoScroll: "true", isLastWatch: "false", isFromScroll: false)
    }
}

extension BAVODDetailViewController: BAEpisodeTableViewCellDelegate {
    func moveToPlayEpisode(episode: Episodes?, isResume: Bool?, isPlayerInPausedState: Bool?) {
        var appName = ""
        if playButtonTitle == "Replay" || playButtonTitle.isEmpty || playButtonTitle == previousButtonTitle || contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue  {
            isContentPlaybackStarted = false
        } else {
            isContentPlaybackStarted = true//isContentPlayback ?? false
        }
        if isContentPlaybackStarted {
            if isPlayerInPausedState ?? false {
                if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue { player?.hungamaPlayer.togglePlayPauseHungamaPlayer(false)
                } else {
                    player?.playerView.player.pause()
                }
                playButtonTitle = "Play"
                contentDetailCell?.playButton.selectedButton(title: "Play", iconName: "")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePlayerIcons"), object: nil)
            } else {
                if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue { player?.hungamaPlayer.togglePlayPauseHungamaPlayer(true) } else { player?.playerView.player.play() }
                playButtonTitle = "Pause"
                contentDetailCell?.playButton.selectedButton(title: "Pause", iconName: "")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPlayerIcons"), object: nil)
            }
        } else {
            if contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue  {
                isContentPlaybackStarted = false
            } else {
                playButtonTitle = "Pause"
                contentDetailCell?.configurePlayPauseButton(buttonTitle: "Pause")
            }
            if contentDetail?.meta?.provider == "Sun_Nxt" || contentDetail?.meta?.provider == "Sun_nxt" || contentDetail?.meta?.provider == "sun_Nxt" || contentDetail?.meta?.provider == "sun_nxt"  {
                appName = "SunNxt"
            }
            var subscribedApp = "Sun"
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name == "Sun_Nxt"}) ?? false {
                subscribedApp = "SunNxt"
            }
            if BAKeychainManager().contentPlayBack {
                if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name?.uppercased() == contentDetail?.meta?.provider?.uppercased()}) ?? false || appName == subscribedApp) {
                    if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                        let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
                        let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                        print("md5SignatureHex: \(md5SignatureHex)")
                        let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
                        self.showActivityIndicator(isUserInteractionEnabled: false)
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            self.getPlayableUrl(signedUrl: signedUrl, isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        } else {
                            self.getPlayableUrl(signedUrl: signedUrl, isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        }
                        
                    } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            confugureCurosityStreamPlayer(isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        } else {
                            confugureCurosityStreamPlayer(isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        }
                    } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue {
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        zeeTagViewModal = BAZeeTagViewModal(repo: BAZeeTagRepo(), endPoint: "")
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            configureZeePlayback(isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        } else {
                            configureZeePlayback(isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        }
                    } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.hotstar.rawValue {
                        let count = BAKeychainManager().hotStarAlertCount // HOTFIX to make 3 time hotstar alert appear
                        if isHotstarInstalled() {
                            let genreResult = episode?.genres?.joined(separator: ",") ?? ""
                            let languageResult = episode?.languages?.joined(separator: ",")
                            if count < 3 {
                                showHotstarProceedAlertWithCallback(kHotStarInstalledHeader, kNewHotStarInstalledMessageString) {
                                    self.playEpisode = episode
                                    if episode?.hotstarAppDeeplink?.isEmpty ?? false {
                                        self.otpResent("Error. Content not found!", completion: nil)
                                    } else {
                                        self.playbackEventTracking(contentTitle: episode?.title ?? "", contentType: episode?.contentType ?? "", contentGenre: genreResult, partnerName: episode?.provider ?? "", languages: languageResult ?? "", railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                        self.makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                                        if let hotstarPlayLink = URL(string: episode?.hotstarAppDeeplink ?? ""), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                                            UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
                                        }
                                    }
                                }
                            } else {
                                self.playEpisode = episode
                                if episode?.hotstarAppDeeplink?.isEmpty ?? false {
                                    self.otpResent("Error. Content not found!", completion: nil)
                                } else {
                                    self.playbackEventTracking(contentTitle: episode?.title ?? "", contentType: episode?.contentType ?? "", contentGenre: genreResult, partnerName: episode?.provider ?? "", languages: languageResult ?? "", railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                    self.makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                                    if let hotstarPlayLink = URL(string: episode?.hotstarAppDeeplink ?? ""), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                                        UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
                                    }
                                }
                            }
                        } else {
                            let genreResult = episode?.genres?.joined(separator: ",") ?? ""
                            let languageResult = episode?.languages?.joined(separator: ",")
                            if count < 3 {
                                showHotstarProceedAlertWithCallback(kHotStarUnInstalledHeader, kNewHotStarUnInstalledMessageString) {
                                    if let url = URL(string: "itms-apps://apple.com/app/id934459219") {
                                        self.playbackEventTracking(contentTitle: episode?.title ?? "", contentType: episode?.contentType ?? "", contentGenre: genreResult, partnerName: episode?.provider ?? "", languages: languageResult ?? "", railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                        UIApplication.shared.open(url)
                                    }
                                }
                            } else {
                                if let url = URL(string: "itms-apps://apple.com/app/id934459219") {
                                    self.playbackEventTracking(contentTitle: episode?.title ?? "", contentType: episode?.contentType ?? "", contentGenre: genreResult, partnerName: episode?.provider ?? "", languages: languageResult ?? "", railName: self.railName, pageSource: self.pageSource, configSource: self.configSource)
                                    UIApplication.shared.open(url)
                                }
                            }
                            //showOkAlertWithCustomTitle(kHotstarNotInstalled, title: "")
                            //showOkAlert(kHotstarNotInstalled)
                            // TODO: Show No App Installed Alert
                        }
                    } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue {
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            configureHungamaPlayer(isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode, isTrailer: false)
                        } else {
                            configureHungamaPlayer(isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode, isTrailer: false)
                        }
                        
                    } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue {
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: episode?.providerContentId, contentType: episode?.contentType, partner: episode?.provider)
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            configureVootPlayer(isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        } else {
                            configureVootPlayer(isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode)
                        }
                    }  else if contentDetail?.meta?.provider?.uppercased() == ProviderType.sonyLiv.rawValue {
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            configureSonyLivPlayer(isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode, isTrailer: false)
                        } else {
                            configureSonyLivPlayer(isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode, isTrailer: false)
                        }
                    } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.eros.rawValue {
                        playEpisode = episode
                        if episode?.watchedDuration != nil {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(episode?.watchedDuration ?? "0"), totalDuration: Int(episode?.totalDuration ?? "0"))
                        } else {
                            makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: 1, totalDuration: Int(episode?.totalDuration ?? "0"))
                        }
                        let totalTime = Float(episode?.totalDuration ?? "0.0") ?? 0.0
                        let watchedTime = Float(episode?.watchedDuration ?? "0.0") ?? 0.0
                        let filledPercentage = watchedTime / totalTime
                        if filledPercentage > 0.99 {
                            configureErosNowPlayer(isReplay: true, isResume: isResume, contentData: nil, episodeData: playEpisode, isTrailer: false)
                        } else {
                            configureErosNowPlayer(isReplay: false, isResume: isResume, contentData: nil, episodeData: playEpisode, isTrailer: false)
                        }
                        /*
                        if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                            makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: contentDetail?.lastWatch?.secondsWatched, totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                            configureErosNowPlayer(isReplay: isReplay, isResume: true, contentData: contentDetail, episodeData: nil, isTrailer: false)
                        } else {
                            var vodId = 0
                            switch self.contentDetail?.meta?.contentType {
                            case ContentType.series.rawValue:
                                vodId = contentDetail?.meta?.seriesId ?? 0
                            case ContentType.brand.rawValue:
                                vodId = contentDetail?.meta?.brandId ?? 0
                            case ContentType.customBrand.rawValue:
                                vodId = contentDetail?.meta?.brandId ?? 0
                            case ContentType.customSeries.rawValue:
                                vodId = contentDetail?.meta?.seriesId ?? 0
                            default:
                                vodId = contentDetail?.meta?.vodId ?? 0
                            }
                            makeContinueWatchCall(contentId: vodId, contentType: self.contentDetail?.meta?.contentType, watchedDuration: 1, totalDuration: contentDetail?.meta?.duration)
                            configureErosNowPlayer(isReplay: isReplay, isResume: false, contentData: contentDetail, episodeData: nil, isTrailer: false)
                        }*/
                    } else {
                        partnerPending()
                    }
                } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == "DEACTIVE" {
                    apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader)
                } else {
                    upgradeSubscription()
                }
            } else {
                /*
                if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.atv.rawValue {
                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus?.uppercased() == kActive.uppercased() {
                        self.apiError(kDTHPackDropped, title: kDTHPackDroppedHeader) {
                            self.signOut()
                        }
                    } else {
                        self.apiError(kDTHPackDropped, title: kATVInactiveHeader) {
                            self.signOut()
                        }
                    }
                } else if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.stick.rawValue {
                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.dthStatus?.uppercased() == kActive.uppercased() {
                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.status == "DEACTIVE" {
                            self.apiError(kFSPackDroppedMessage, title: kDTHPackDroppedHeader) {
                                self.signOut()
                            }
                        } else {
                            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated == true {
                                self.apiError(kDTHInactive, title: kTempSuspendedHeader)
                            } else {
                                self.apiError(kDTHInactive, title: kTempSuspendedHeader) {
                                    self.signOut()
                                }
                            }
                        }
                    } else {
                        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.migrated == true {
                            self.apiError(kDTHInactive, title: kTempSuspendedHeader)
                        } else {
                            self.apiError(kDTHInactive, title: kTempSuspendedHeader) {
                                self.signOut()
                            }
                        }
                    }
                }
              */
            }
        }
    }
    
    func isHotstarInstalled() -> Bool {
        if let hotstarappLink = URL(string: "hotstar://") {
            if UIApplication.shared.canOpenURL(hotstarappLink) {
                return true
            }
            return false
        }
        return false
    }
    
    func baEpisodeTableViewCell(_ cell: BAEpisodeTableViewCell, didUpdateEpisode episode: Episodes?, for indexPath: IndexPath?) {
        guard let index = indexPath,
              let episode = episode else { return }
        if var seriesYearList = dataSourceArray[index.section] as? SeriesYearsList {
            seriesYearList.seriesDetail?.items?[index.row] = episode
            dataSourceArray[index.section] = seriesYearList
        }
        vodDetailTableView.beginUpdates()
        vodDetailTableView.reloadRows(at: [index], with: .none)
        vodDetailTableView.endUpdates()
        
    }
}

extension BAVODDetailViewController: BASeriesFooterViewTableViewCellDelegate {
    func seriesFooterViewTableViewCellDelegateDidTapLoadMore(_ cell: BASeriesFooterViewTableViewCell) {
        guard let seasonDetail = contentDetail?.seriesList?[selectedSeason] else { return }
        if BAReachAbility.isConnectedToNetwork() {
            if seasonDetail.id == nil {
                limit = seasonDetail.seriesDetail?.limit ?? 0
                offset = (seasonDetail.seriesDetail?.limit ?? 0) + (seasonDetail.seriesDetail?.offset ?? 0)
                fetchSeasonDetailForContentId(contentDetail?.meta?.seriesId ?? 0, limit: limit , offsset: offset, isAutoScroll: "false", isLastWatch: "false", isFromScroll: false)
            } else {
                limit = seasonDetail.seriesDetail?.limit ?? 0
                offset = (seasonDetail.seriesDetail?.limit ?? 0) + (seasonDetail.seriesDetail?.offset ?? 0)
                fetchSeasonDetailForContentId(seasonDetail.id ?? 0, limit: limit, offsset: offset, isAutoScroll: "false", isLastWatch: "false", isFromScroll: false)
            }
        } else {
            noInternet()
        }
    }
}

extension BAVODDetailViewController: BAFullScreenMoviePlayerViewControllerDelegate {
    func isDismissed() {
        if contentDetail?.meta?.contentType == ContentType.series.rawValue || contentDetail?.meta?.contentType == ContentType.brand.rawValue {
            showActivityIndicator(isUserInteractionEnabled: false)
        }
        if UIDevice.current.orientation.isLandscape {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            UIViewController.attemptRotationToDeviceOrientation()
        } else {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            UIViewController.attemptRotationToDeviceOrientation()
        }
        //
        isMovedToPlayer = true
        isCallAlreadyMade = false
        var vodId = 0
        //contentDetail?.meta?.contentType = previousContentType
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        updateContentDetail(vodId: vodId, contentType: contentDetail?.meta?.contentType)
    }
    
    func configureFavouriteButton(isAdddedToFav: Bool?) {
        if isAdddedToFav ?? false {
            self.markFavourite.isSelected = true
            self.isAddedToFav = true
            self.markFavourite.setImage(UIImage(named: "favSelected"), for: .normal)
        } else {
            self.markFavourite.isSelected = false
            self.isAddedToFav = false
            self.markFavourite.setImage(UIImage(named: "followIcon"), for: .normal)
        }
    }
}

extension BAVODDetailViewController: BAZeePlayerViewControllerDelegate {
    func zeePlayerDismissed() {
        //playButtonTitle = "Resume"
       // contentDetailCell?.playButton.selectedButton(title: "Resume", iconName: "play")
    }
}

extension BAVODDetailViewController: BAContentDetailTableViewCellDelegate {
    
    func playFullScreenPlayer(contentType: String, isReplay: Bool?, contractName: String?, playerStateToPause: Bool?) {
            if BAReachAbility.isConnectedToNetwork() {
                showDTHAlerts()
                let playbackStatus = checkForPlaybackAvailabilityStatus()
                if playbackStatus {
                    if BAKeychainManager().contentPlayBack == false {
                       // self.showDTHAlerts()
                    } else {
                            if contractName == "RENTAL" {
                                if contentDetailCell?.validityLeftLabel.isHidden == false && contentDetailCell?.validityLeftLabel.text == "EXPIRED" {
                                    showOkAlertWithCustomTitle(kRentalExpired, title: "Video unavailable")
                                } else {
                                    playerExistingState.0 = isReplay ?? false
                                    playerExistingState.1 = playerStateToPause ?? false
                                    refreshRRMVM = BARefreshRRMViewModal(repo: BARefreshRRMRepo(), endPoint: "/\(BAKeychainManager().acccountDetail?.subscriberId ?? 0)/session", epids: contentDetail?.detail?.offerId?.epids ?? [])
                                    getRefreshToken(isReplay: isReplay, isPlayerInPausedState: playerStateToPause)
                                }
                            } else {
                                if contentType == ContentType.brand.rawValue || contentType == ContentType.series.rawValue || contentType == ContentType.customSeries.rawValue || contentType == ContentType.customBrand.rawValue {
                                    var appName = ""
                                    if contentDetail?.meta?.provider == "Sun_Nxt" || contentDetail?.meta?.provider == "Sun_nxt" || contentDetail?.meta?.provider == "sun_Nxt" || contentDetail?.meta?.provider == "sun_nxt"  {
                                        appName = "SunNxt"
                                    }
                                    var subscribedApp = "Sun"
                                    
                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name == "Sun_Nxt"}) ?? false {
                                        subscribedApp = "SunNxt"
                                    }
                                    if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name?.uppercased() == contentDetail?.meta?.provider?.uppercased()}) ?? false || appName == subscribedApp) {
                                        if contentDetail?.lastWatch != nil && contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                                            playFullScreenPlayer(isReplay: isReplay, isPlayerInPausedState: playerStateToPause)
                                        } else {
                                            moveToPlayEpisode(episode: allEpisodes?[0], isResume: false, isPlayerInPausedState: playerStateToPause)
                                        }
                                        if contractName == "RENTAL" || contentDetail?.meta?.contentType == ContentType.webShorts.rawValue {
                                            // Do Nothing
                                        } else {
                                            var vodId = 0
                                            switch contentType {
                                            case ContentType.series.rawValue:
                                                vodId = contentDetail?.meta?.seriesId ?? 0
                                            case ContentType.brand.rawValue:
                                                vodId = contentDetail?.meta?.brandId ?? 0
                                            default:
                                                vodId = contentDetail?.meta?.vodId ?? 0
                                            }
                                            if !checkToCallLearnAction(Int64(vodId)) {
                                                learnWatchEventVM = BAWatchLearnActionViewModal(repo: BAWatchLearnActionRepo(), endPoint: "\(contentType)/\(vodId)/VOD")
                                                saveLearnActionData(contentType, Int64(vodId), Date())
                                                watchLearnAction()
                                            }
                                        }
                                    } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == "DEACTIVE" {
                                        apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader)
                                    } else {
                                        upgradeSubscription()
                                    }
                                } else {
                                    var appName = ""
                                    if contentDetail?.meta?.provider == "Sun_Nxt" || contentDetail?.meta?.provider == "Sun_nxt" || contentDetail?.meta?.provider == "sun_Nxt" || contentDetail?.meta?.provider == "sun_nxt"  {
                                        appName = "SunNxt"
                                    }
                                    var subscribedApp = "Sun"
                                    
                                    if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name == "Sun_Nxt"}) ?? false {
                                        subscribedApp = "SunNxt"
                                    }
                                    if (BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name?.uppercased() == contentDetail?.meta?.provider?.uppercased()}) ?? false || appName == subscribedApp) {
                                        playFullScreenPlayer(isReplay: isReplay, isPlayerInPausedState: playerStateToPause)
                                        if contractName == "RENTAL" || contentDetail?.meta?.contentType == ContentType.webShorts.rawValue {
                                            // Do Nothing
                                        } else {
                                            var vodId = 0
                                            switch contentType {
                                            case ContentType.series.rawValue:
                                                vodId = contentDetail?.meta?.seriesId ?? 0
                                            case ContentType.brand.rawValue:
                                                vodId = contentDetail?.meta?.brandId ?? 0
                                            case ContentType.customSeries.rawValue:
                                                vodId = contentDetail?.meta?.seriesId ?? 0
                                            case ContentType.customBrand.rawValue:
                                                vodId = contentDetail?.meta?.brandId ?? 0
                                            default:
                                                vodId = contentDetail?.meta?.vodId ?? 0
                                            }
                                            if !checkToCallLearnAction(Int64(vodId)) {
                                                learnWatchEventVM = BAWatchLearnActionViewModal(repo: BAWatchLearnActionRepo(), endPoint: "\(contentType)/\(vodId)/VOD")
                                                saveLearnActionData(contentType, Int64(vodId), Date())
                                                watchLearnAction()
                                            }
                                        }
                                    } else if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionInformationDTO?.bingeAccountStatus == "DEACTIVE" {
                                        apiError(kDTHInactiveLowBalance, title: kDTHInactiveLowBalanceHeader)
                                    } else {
                                        upgradeSubscription()
                                    }
                                }
                            }
                    }
                } else {
                       
                }
                
            } else {
            noInternet()
        }
    }
    
    func checkToCallLearnAction(_ vodId: Int64) -> Bool {
        let dbValue = DataBaseManager.getLearnActionAnalyticsData(vodId)
        guard  let data = dbValue else {
            return false
        }
        let currentDate = Date()
        let diff = Date.daysBetween(start: data[0].timeStamp ?? currentDate, end: currentDate)
        return !(diff >= 1)
    }
    
    func noInternetConnection() {
        noInternet()
    }
    
    func saveLearnActionData(_ contentType: String, _ contentId: Int64 , _ learnActionTime: Date) {
        var dataDic = [String: Any]()
        dataDic["contentType"] = contentType
        dataDic["contentId"] = contentId
        DataBaseManager.saveLearnActionAnalyticsData(dataDic, learnActionTime)
    }
    
    func getRefreshToken(isReplay: Bool?, isPlayerInPausedState: Bool?) {
        if let vodID = contentDetail?.meta?.vodId {
            let id = Int64(vodID)
            let dbValue = DataBaseManager.getJwtTokenData(id)
            guard let data = dbValue else {
                if BAReachAbility.isConnectedToNetwork() {
                    if refreshRRMVM != nil {
                        refreshRRMVM?.getRefreshRRMDetail(completion: { (flagValue, message, isApiError) in
                            if flagValue {
                                self.saveJwtToken(id)
                                self.playFullScreenPlayer(isReplay: isReplay, isPlayerInPausedState: isPlayerInPausedState)
                            } else if isApiError {
                                self.apiError(message, title: kSomethingWentWrong)
                            } else {
                                self.apiError(message, title: "")
                            }
                        })
                    }
                } else {
                    noInternet()
                }
                return
            }
            print("JWT Token Already Exist")
            self.playFullScreenPlayer(isReplay: isReplay, isPlayerInPausedState: isPlayerInPausedState)
            self.updateJwtToken(id, data[0])
        }
    }
    
    
    func saveJwtToken(_ id: Int64) {
        let dbCount = DataBaseManager.getTotalJwtTokenDataCount()
        var dic = [String: Any]()
        dic["contentId"] = id
        dic["token"] = refreshRRMVM?.rrmDetail?.token
        dic["expiresIn"] = refreshRRMVM?.rrmDetail?.expiresIn
        dic["usageCount"] = 1
        if dbCount < 20 {
            DataBaseManager.saveJwtTokenData(dic)
        } else {
            removeLeastUsedToken(dic)
        }
    }
    
    func updateJwtToken(_ id: Int64, _ data: PlayerJWTToken) {
        var dic = [String: Any]()
        dic["contentId"] = id
        dic["token"] = data.token
        dic["expiresIn"] = data.expiresIn
        dic["usageCount"] = data.usageCount + 1
        DataBaseManager.saveJwtTokenData(dic)
    }
    
    func removeLeastUsedToken(_ dic: [String: Any]) {
        let dbData = DataBaseManager.getTotalJwtData()
        guard let data = dbData else {
            return
        }
        let id = data[0].contentId
        DataBaseManager.deleteJwtToken(id)
        DataBaseManager.saveJwtTokenData(dic)
    }
    
    func playTrailer(_ cell: BAContentDetailTableViewCell) {
        if isContentPlaying {
            // Do Not Play Trailer
        } else {
            let addedPlayers = playerViewArea.subviews.filter { $0 is BAPlayerView }
            if addedPlayers.isEmpty {
                CustomLoader.shared.showLoader(on: contentPosterImage)
                if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue ||
                    contentDetail?.meta?.provider?.uppercased() == ProviderType.hungama.rawValue {
                    contentDetailCell = cell
                    configurePlayerView()
                    isTrailerPlayed = true
                } else {
                    partnerPending()
                }
            }
        }
    }
    
    func showAlert() {
        noInternet()
        // Do Nothing
    }
    
    func scrollToSeasons() {
        let sectionIndexPath = IndexPath(row: NSNotFound, section: 1)
        //self.vodDetailTableView.scrollToRow(at: sectionIndexPath, at: .middle, animated: true)
        
        self.vodDetailTableView.scrollToRow(at: sectionIndexPath, at: .top, animated: true)
        // Scroll to index should go here
    }
    
    func contentDetailTableViewCellDidExpand(_ cell: BAContentDetailTableViewCell, expanded: Bool) {
        shouldShowFullDesc = expanded
        vodDetailTableView.reloadData()
        isTableReloaded = true
        vodDetailTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .middle, animated: true)
    }
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using: .utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData
    }
    
    func shareButtonTapped(on cell: BAContentDetailTableViewCell, with data: [Any]) {
        let activityViewController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func watchlistButtonTapped(isAdded: Bool) {
        self.isAddedToFav = isAdded
        if isAddedToFav {
            var vodId = 0
            let contentType = contentDetail?.meta?.contentType
            switch contentType {
            case ContentType.series.rawValue:
                vodId = contentDetail?.meta?.seriesId ?? 0
            case ContentType.brand.rawValue:
                vodId = contentDetail?.meta?.brandId ?? 0
            default:
                vodId = contentDetail?.meta?.vodId ?? 0
            }
            configureLearnAction(vodId: vodId)
            //watchLearnAction()
        }
    }
}

//
//  BAZeeSafariViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 30/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit
import SafariServices

protocol BAZeeSafariViewControllerDelegate: class {
    func zeeSafariPlayerDismissed()
}

class BAZeeSafariViewController: BaseViewController, SFSafariViewControllerDelegate {
    var playableUrl: String?
    weak var delegate: BAZeeSafariViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .BAdarkBlueBackground
        configureNavigation()
        configureSafariView()
        // Do any additional setup after loading the view.
    }
    
    func configureNavigation() {
        
        super.configureNavigationBar(with: .backWithText, #selector(backButtonAction), nil, self, "Zee5")
        self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    func configureSafariView() {
        if let url = URL(string: playableUrl ?? "") {
            let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            vc.delegate = self
            present(vc, animated: true)
        }
    }
    
    @objc func backButtonAction() {
        delegate?.zeeSafariPlayerDismissed()
        self.navigationController?.popViewController(animated: true)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        delegate?.zeeSafariPlayerDismissed()
        dismiss(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

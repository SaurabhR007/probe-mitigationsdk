//
//  ExtensionBAAppsDetailView.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension BAAppsDetailViewController: TableViewCellDelegate, BAHeroBannerTableViewCellDelegate, BAAppDetailTableViewCellDelegate {
   
    func moveToSubscriptionPage() {
        let subscriptionVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.subscription.rawValue, type: MySubscriptionVC.self)
        subscriptionVC.isFromAccountScreen = true
        subscriptionVC.subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
        self.navigationController?.pushViewController(subscriptionVC, animated: true)
    }
    
    func moveToBrowseByDetail(intent: String?, genre: String?, language: String?, pageSource: String?, configSource: String?, railName: String?) {
        // TODO: If required, DO same as Home VC
    }
    
    func moveToDetailScreen(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, timestamp: Int?, pageSource: String?, configSource: String?, railName: String?) {
        fetchContentDetail(railType: railType, layoutType: layoutType, vodId: vodId, contentType: contentType, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    /**
     ********************************************
     Content Detail Cell Delegate Methods
     ********************************************
     **/
    func moveToDetailForPageType(pageType: String?, appname: String?, isSubscribed: Bool?) {
        // Do Nothing
    }
    
    func moveToSeeAllScreenFromVOD(_ data: RecommendedContentData?) {
        // Do Nothing
    }
    
    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(railType: String?, layoutType: String?, vodId: Int?, contentType: String?, pageSource: String?, configSource: String?, railName: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(railType: railType, layoutType: layoutType, vodId: vodId, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    // MARK: Content Detail API Calling
    func getContentDetailData(railType: String?, layoutType: String?, vodId: Int?, pageSource: String?, configSource: String?, railName: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                   // self.hideActivityIndicator()
                    if flagValue {
                        self.viewContentDetailEvent(contentData: dataModel.contentDetail, pageSource: pageSource ?? "", configSource: configSource ?? "", railName: railName ?? "")
                        self.fetchLastWatch(railType: railType, layoutType: layoutType, vodId: vodId, contentDetail: dataModel.contentDetail, pageSource: pageSource ?? "", configSource: configSource ?? "", railName: railName ?? "")
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func viewContentDetailEvent(contentData: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        var title = ""
        if contentData?.meta?.contentType == ContentType.brand.rawValue {
            title = contentData?.meta?.brandTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.series.rawValue {
            title = contentData?.meta?.seriesTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.tvShows.rawValue {
            title = contentData?.meta?.vodTitle ?? ""
        } else {
            title = contentData?.meta?.vodTitle ?? ""
        }
        let genreResult = contentData?.meta?.genre?.joined(separator: ",")
        let languageResult = contentData?.meta?.audio?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: "Partner Home", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
    
    func fetchLastWatch(railType: String?, layoutType: String?, vodId: Int?, contentDetail: ContentDetailData?, pageSource: String?, configSource: String?, railName: String?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(railType: railType, layoutType: layoutType, vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId, pageSource: pageSource ?? "", configSource: configSource ?? "", railName: railName ?? "")
    }
    
    func fetchWatchlistLookUp(railType: String?, layoutType: String?, vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int, pageSource: String?, configSource: String?, railName: String?) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(railType: railType, layoutType: layoutType, vodId: vodId, contentDetail: contentDetail, pageSource: pageSource ?? "", configSource: configSource ?? "", railName: railName ?? "")
    }
    
    func confgureWatchlistIcon(railType: String?, layoutType: String?, vodId: Int?, contentDetail: ContentDetailData?, pageSource: String?, configSource: String?, railName: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
                //hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigation
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = pageSource ?? ""
                        contentDetailVC.configSource = configSource ?? ""
                        contentDetailVC.railName = railName ?? ""
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.parentNavigation?.pushViewController(contentDetailVC, animated: true)
                        //}
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigation
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = pageSource ?? ""
                        contentDetailVC.configSource = configSource ?? ""
                        contentDetailVC.railName = railName ?? ""
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil// dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                        self.parentNavigation?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.parentNavigation
                        contentDetailVC.railType = railType
                        contentDetailVC.layoutType = layoutType
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = pageSource ?? ""
                        contentDetailVC.configSource = configSource ?? ""
                        contentDetailVC.railName = railName ?? ""
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil// dataModel.watchlistLookUp?.data
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                        self.parentNavigation?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    // MARK: Move to See All Screen Method
    func moveToSeeAllScreen(_ data: HomeScreenDataItems?, _ index: Int) {
        let configType = (data?.sectionSource == TAConstant.recommendationType.rawValue) ? MixpanelConstants.ParamValue.recomended.rawValue : MixpanelConstants.ParamValue.editorial.rawValue
        if data?.sectionSource == "PROVIDER" {
            let appsSeeAllVC: BAAppsSeeAllViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllForApps.rawValue, type: BAAppsSeeAllViewController.self)
            appsSeeAllVC.contentId = data?.id ?? 0
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: configType, MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "YES", MixpanelConstants.ParamName.railPosition.rawValue: index, MixpanelConstants.ParamName.partner.rawValue: self.title ?? "", MixpanelConstants.ParamName.pageName.rawValue: self.title ?? ""])
            self.parentNavigation?.pushViewController(appsSeeAllVC, animated: true)
        } else if data?.sectionSource == "CONTINUE_WATCHING" {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: configType, MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "YES", MixpanelConstants.ParamName.railPosition.rawValue: index, MixpanelConstants.ParamName.partner.rawValue: self.title ?? "", MixpanelConstants.ParamName.pageName.rawValue: self.title ?? ""])
            let cwSeeAllVC: BAContinueWatchingSeeAllViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.continueWatchVC.rawValue, type: BAContinueWatchingSeeAllViewController.self)
            cwSeeAllVC.isCW = true
            self.parentNavigation?.pushViewController(cwSeeAllVC, animated: true)
        } else {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.seeAllFromHome.rawValue, properties: [MixpanelConstants.ParamName.configType.rawValue: configType, MixpanelConstants.ParamName.railTitle.rawValue: data?.title ?? "", MixpanelConstants.ParamName.partnerHome.rawValue: "YES", MixpanelConstants.ParamName.railPosition.rawValue: index, MixpanelConstants.ParamName.partner.rawValue: self.title ?? "", MixpanelConstants.ParamName.pageName.rawValue: self.title ?? ""])
            let seeAllVC: SeeAllVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllVC.rawValue, type: SeeAllVC.self)
            seeAllVC.contentId = data?.id ?? 0
            seeAllVC.pageSource = self.title ?? ""
            seeAllVC.configSource = "Editorial"
            seeAllVC.railName = data?.title ?? ""
            seeAllVC.isTAData = (data?.sectionSource == TAConstant.recommendationType.rawValue)
            seeAllVC.isTVODData = (data?.sectionSource == "TVOD")
            seeAllVC.layoutType = data?.layoutType
            seeAllVC.taContentlListData = data
            // seeAllVC.delegate = self
            self.parentNavigation?.pushViewController(seeAllVC, animated: true)
        }
    }
    
    func moveToDetailScreenFromHeroBanner(vodId: Int?, contentType: String?, pageSource: String?, configSource: String?, railName: String?) {
        fetchContentDetail(railType: "", layoutType: "LANDSCAPE", vodId: vodId, contentType: contentType, pageSource: pageSource, configSource: configSource, railName: railName)
    }
}

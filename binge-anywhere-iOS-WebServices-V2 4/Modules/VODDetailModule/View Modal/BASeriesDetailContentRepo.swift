//
//  BASeriesDetailContentRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol SeriesContentRepo {
    var apiEndPoint: String {get set}
    var apiParams: [AnyHashable: Any] {get set}
    func getSeriesData(completion: @escaping(APIServicResult<BASeriesDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct SeriesDetailContentRepo: SeriesContentRepo {
    var apiParams: [AnyHashable: Any] = [:]
    var apiEndPoint: String = ""

    func getSeriesData(completion: @escaping(APIServicResult<BASeriesDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiParams = self.apiParams
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .seriesDetail(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BASeriesDetailModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BARefreshRRMViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 16/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BARefreshRRMViewModalProtocol {
    var rrmDetail: RRMData? { get set }
    func getRefreshRRMDetail(completion: @escaping(Bool, String?, Bool) -> Void)
    //func addRemoveFromWatchlist(completion: @escaping(Bool, String?) -> Void)
}

class BARefreshRRMViewModal: BARefreshRRMViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BARefreshRRMScreenRepo
    var epiDetail: [EpiDetail]
    var endPoint: String = ""
    var rrmDetail: RRMData?

    init(repo: BARefreshRRMScreenRepo, endPoint: String, epids: [EpiDetail]) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.epiDetail = epids
    }

    func getRefreshRRMDetail(completion: @escaping(Bool, String?, Bool) -> Void) {
        var apiParams = APIParams()
        apiParams["action"] = "stream"
        let arr = epiDetail.map{["epid": $0.epid, "bid": $0.bid]}
        apiParams["epids"] = arr
        print("Request Param is------>", apiParams)
        requestToken = repo.getRefreshRRMDetail(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
             if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.rrmDetail = _configData.parsed.data
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    
}

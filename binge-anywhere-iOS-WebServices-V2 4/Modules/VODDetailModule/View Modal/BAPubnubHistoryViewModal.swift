//
//  BAPubnubHistoryViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 18/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAPubnubHistoryViewModalProtocol {
    var pubnubHistory: BAPubnubHistoryModal? {get set}
    func getPubnubHistory(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAPubnubHistoryViewModal: BAPubnubHistoryViewModalProtocol {
    
    var requestToken: ServiceCancellable?
    var repo: BAPubnubHistoryScreenRepo?
    var pubnubHistory: BAPubnubHistoryModal?
    var apiParams = APIParams()

    init(repo: BAPubnubHistoryScreenRepo) {
        self.repo = repo
        self.repo?.apiEndPoint = BAKeychainManager().sId
    }

    func getPubnubHistory(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo?.getPubnubHistory(apiParams: apiParams, completion: { configData, error in
            self.requestToken = nil
            if let _configData = configData {
                if _configData.serviceResponse.statusCode == 200 {
                    //if statusCode == 0 {
                    BAKeychainManager().contentPlayBack = _configData.parsed.deviceInfo?.first?.contentPlayBackHybrid ?? false
                    BAKeychainManager().deviceLoggedOut = _configData.parsed.deviceInfo?.first?.logoutHybrid ?? false
                    completion(true, nil, false)
                   // }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

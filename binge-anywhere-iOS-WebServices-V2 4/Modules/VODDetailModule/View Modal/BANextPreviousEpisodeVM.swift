//
//  BANextPreviousEpisodeVM.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
protocol BANextPreviousEpisodeVMProtocol {
    func getNextPreviousEpisodeData(completion: @escaping(Bool, String?) -> Void)
}

class BANextPreviousEpisodeVM: BANextPreviousEpisodeVMProtocol {
    var requestToken: ServiceCancellable?
    var repo: BANextPreviousEpisodeScreenRepo
    var id: Int = 0
    var endPoint: String = ""
    var nextPreviousData: BANextPreviousEpisodeModel?
    var apiParams = APIParams()

    init(repo: BANextPreviousEpisodeScreenRepo, endPoint: String, id: Int) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.id = id
    }

    func getNextPreviousEpisodeData(completion: @escaping(Bool, String?) -> Void) {
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["profileId"] = BAKeychainManager().profileId
        apiParams["id"] = String(id)
        requestToken = repo.getNextPreviousEpisodeData(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.nextPreviousData = _configData.parsed
                        completion(true, nil)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage)
                            } else {
                                completion(false, "Some Error Occurred")
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage)
                            } else {
                                completion(false, "Some Error Occurred")
                            }
                        }
                    }
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription)
            } else {
                completion(false, "Some Error Occurred")
            }
        })

    }
}

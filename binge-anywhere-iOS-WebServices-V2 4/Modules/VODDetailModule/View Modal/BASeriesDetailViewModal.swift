//
//  BASeriesDetailViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASeriesDetailViewModalProtocol {
    func getSeriesData(completion: @escaping(Bool, String?, Bool) -> Void)
    func checkForEpisodeDuration( _ completion: @escaping () -> Void)
}

class BASeriesDetailViewModal: BASeriesDetailViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: SeriesContentRepo
    var episodeDurationVM: BAEpisodeDurationViewModal?
    var episodeDurationsDetail: BAEpisodeDurationModal?
    var seriesDetailModal: BASeriesDetailModal?
    var contentId: Int?
    var episodeDetail = [[ : ]]

    init(repo: SeriesContentRepo, endPoint: String, params: [AnyHashable: Any]?, contentId: Int) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.repo.apiParams = params ?? [:]
    }

    func getSeriesData(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo.getSeriesData(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.seriesDetailModal = _configData.parsed
                        for item in self.seriesDetailModal?.data?.items ?? [] {
                            let episodeDetailDict = ["contentId": item.id ?? 0, "contentType": item.contentType ?? ""] as [String : Any]
                            self.episodeDetail.append(episodeDetailDict)
                        }
                        self.episodeDetail.removeFirst()
                        self.checkForEpisodeDuration {
                            if let data = self.seriesDetailModal?.data?.items {
                                for (index, item) in data.enumerated() {
                                    item.watchedDuration = "\(self.episodeDurationVM?.episodeDurationsDetail?.data?[index].secondsWatched ?? 0)"
                                   // item.totalDuration = "\(self.episodeDurationsDetail?.data?[index].durationInSeconds ?? 0)"
                                }
                            }
                            completion(true, nil, false)
                        }
                        
                        //completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func checkForEpisodeDuration(_ completion: @escaping () -> Void) {
        episodeDurationVM = BAEpisodeDurationViewModal(repo: BAEpisodeDurationsRepo(), endPoint: self.contentId ?? 0, episodes: self.episodeDetail)
        if let episodeDurationsVM = episodeDurationVM {
            episodeDurationsVM.getEpisodeDurations { (flagValue, _, isApiError) in
                if flagValue {
                    if (self.episodeDurationVM?.episodeDurationsDetail) != nil {
                        self.episodeDurationsDetail?.data = self.episodeDurationVM?.episodeDurationsDetail?.data
                    } else {
                    }
                    completion()
                } else {
                    completion()
                }
            }
            return
        }
    }

}

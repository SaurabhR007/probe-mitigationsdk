//
//  BAAddRemoveWatchlistViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAAddRemoveWatchlistViewModalProtocol {
    func addRemoveWatchlist(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAAddRemoveWatchlistViewModal: BAAddRemoveWatchlistViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: AddRemoveWatchlistScreenRepo
    var endPoint: String = ""
    var baId: String = "0"
    var profileId: String = ""
    var contentType: String = ""
    var contentId: Int = 0
    var addRemoveWatchlist: BAAddRemoveWatchlist?
    var apiParams = APIParams()

    init(repo: AddRemoveWatchlistScreenRepo, endPoint: String, baId: String, contentType: String, contentId: Int) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.contentId = contentId
        self.contentType = contentType
        self.baId = baId
        self.profileId = BAKeychainManager().profileId//"584c81e8-3326-4f6b-a08a-9ba6db8b4e6c"
    }

    func addRemoveWatchlist(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["profileId"] = BAKeychainManager().profileId
        apiParams["contentId"] = String(contentId)
        apiParams["contentType"] = contentType
        requestToken = repo.addRemoveWatchlist(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

//
//  BAZeeTagRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAZeeTagScreenRepo {
    //var apiEndPoint: String {get set}
    func getZeeTagDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BAZeeTagModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAZeeTagRepo: BAZeeTagScreenRepo {
    //var apiEndPoint: String = ""
    func getZeeTagDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BAZeeTagModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId != nil {
            let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "bearer \(BAKeychainManager().userAccessToken)", "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "deviceId": kDeviceId, "baId": BAKeychainManager().baId, "partnerUniqueId": BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId]
            let target = ServiceRequestDetail.init(endpoint: .zeeTag(param: apiParams, headers: apiHeader, endUrlString: ""))
            return APIService().request(target) { (response: APIResult<APIServicResult<BAZeeTagModal>, ServiceProviderError>) in
                completion(response.value, response.error)
            }
        } else {
            let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "bearer \(BAKeychainManager().userAccessToken)", "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "deviceId": kDeviceId, "baId": BAKeychainManager().baId, "partnerUniqueId": ""]
            let target = ServiceRequestDetail.init(endpoint: .zeeTag(param: apiParams, headers: apiHeader, endUrlString: ""))
            return APIService().request(target) { (response: APIResult<APIServicResult<BAZeeTagModal>, ServiceProviderError>) in
                completion(response.value, response.error)
            }
        }
    }
}

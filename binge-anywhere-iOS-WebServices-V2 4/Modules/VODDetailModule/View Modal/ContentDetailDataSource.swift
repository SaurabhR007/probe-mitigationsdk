//
//  ContentDetailDataSource.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 20/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class ContentDetailDataSource: NSObject {

    var contentDetail: ContentDetailData?
    var recommendedDetail: Any?
    var selectedSeason = 0

    init(_ contentDetail: ContentDetailData?, recommened: Any?) {
        self.contentDetail = contentDetail
        self.recommendedDetail = recommened
    }

//    func numberOfSections() -> Int {
//        //var sections = contentDetail?.requestNumberOfSection() ?? 0
//        if recommendedDetail != nil {
//            sections += 1
//        }
//        return sections
//    }

//    func numberOfRowsForSections(_ section: Int) -> Int {
//        if contentDetail != nil {
//            if section == 0 {
//                return 1
//            }
//            if section == 1 && contentDetail?.seriesList != nil, contentDetail?.seriesList?.count ?? 0 > 0 {
//                return contentDetail?.seriesList![selectedSeason].seriesDetail?.items?.count ?? 0
//            }
//        }
//        if recommendedDetail != nil {
//            return 1
//        }
//        return 0
//    }

//    func dataForIndexpath(_ indexpath: IndexPath) -> Any? {
//        return contentDetail
//    }
}

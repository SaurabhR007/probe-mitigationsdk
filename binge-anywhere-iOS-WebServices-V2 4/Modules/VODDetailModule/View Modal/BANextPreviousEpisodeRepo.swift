//
//  BANextPreviousEpisodeRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
protocol BANextPreviousEpisodeScreenRepo {
    var apiEndPoint: String {get set}
    func getNextPreviousEpisodeData(apiParams: APIParams, completion: @escaping(APIServicResult<BANextPreviousEpisodeModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BANextPreviousEpisodeRepo: BANextPreviousEpisodeScreenRepo {
    var apiEndPoint: String = ""
    func getNextPreviousEpisodeData(apiParams: APIParams, completion: @escaping(APIServicResult<BANextPreviousEpisodeModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        //let apiParams = APIParams()
        // TODO: Remove once kong is setup
        //let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId ?? "", "platform": "BINGE_ANYWHERE"]
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .nextPreviousEpisodeData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BANextPreviousEpisodeModel>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

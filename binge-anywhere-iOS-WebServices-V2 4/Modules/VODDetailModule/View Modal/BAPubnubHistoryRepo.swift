//
//  BAPubnubHistoryRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 18/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAPubnubHistoryScreenRepo {
    var apiEndPoint: String {get set}
    func getPubnubHistory(apiParams: APIParams, completion: @escaping(APIServicResult<BAPubnubHistoryModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAPubnubHistoryRepo: BAPubnubHistoryScreenRepo {
    var apiEndPoint: String = ""
    func getPubnubHistory(apiParams: APIParams, completion: @escaping(APIServicResult<BAPubnubHistoryModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var apiParams = APIParams()
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId, "deviceName": kDeviceModal, "authorization": "bearer \(BAKeychainManager().userAccessToken)", "subscriberId": BAKeychainManager().sId, "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .pubnubHistory(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAPubnubHistoryModal>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

//
//  BARecommendedContentViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol BARecommendedContentViewModalProtocol {
    func getRecommendedContentData(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BARecommendedContentViewModal: BARecommendedContentViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: RecommendedContentRailRepo
    var tvodVM: BATVODViewModal?
    var tvodData: TVODData?
    var contentList: [BAContentListModel]?
    var apiParams = APIParams()
    var recommendedData: BARecommendedContentModal?
    var dataModel: TARecommendationDataModel?
    var taRecommendationVM = TARecommendationVM()
    var endPoint: String = ""
    var nextPageAvailable = false
    var pageLimit = 10
    var pageLimitFirstTime = 20
    var totalDataCount = 0
    var pageOffset = 0
    var taShowType = ""
    var errorCode: Int?
    var isFromSeeAll = false

    init(repo: RecommendedContentRailRepo, endPoint: String, isFromSeeAllScreen: Bool = false) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.isFromSeeAll = isFromSeeAllScreen
    }

    func getRecommendedContentData(completion: @escaping(Bool, String?, Bool) -> Void) {
        self.checkForTVODData {
            if self.isFromSeeAll {
                self.apiParams["from"] = String(self.pageOffset)
                if self.pageOffset == 0 {
                    self.apiParams["max"] = String(self.pageLimitFirstTime)
                } else {
                    self.apiParams["max"] = String(self.pageLimit)
                }
                
            } else {
                self.apiParams = APIParams()
            }
            self.requestToken = self.repo.getRecommendedContentData(apiParams: self.apiParams, completion: { (configData, error) in
                self.requestToken = nil
                if let _configData = configData {
                    if let statusCode = _configData.parsed.code {
                        if statusCode == 0 {
                            if self.isFromSeeAll {
                                if self.pageOffset == 0 {
                                    self.recommendedData = _configData.parsed
                                    self.totalDataCount = _configData.parsed.data?.contentList?.count ?? 0
                                } else {
                                    self.recommendedData?.data?.contentList?.append(contentsOf: (_configData.parsed.data?.contentList)!)
                                    self.totalDataCount += _configData.parsed.data?.contentList?.count ?? 0
                                }
                                if let screenData = self.recommendedData?.data {
                                    if let _ = screenData.contentList {
                                        self.pageOffset = self.totalDataCount
                                        self.nextPageAvailable = self.totalDataCount < (screenData.totalCount ?? 0)
                                    }
                                }
                                if let data = self.recommendedData?.data?.contentList {
                                    self.recommendedData?.data?.contentList = UtilityFunction.shared.filterTAData(data)
                                }
                                //var contentList = [BAContentListModel] ()
                                self.contentList = self.recommendedData?.data?.contentList
                                self.contentList?.removeAll()
                                for recomendedContent in self.recommendedData?.data?.contentList ?? [] {
                                    if recomendedContent.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == recomendedContent.title}) ?? true {
                                                self.contentList?.append(recomendedContent)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        self.contentList?.append(recomendedContent)
                                    }
                                }
                                self.recommendedData?.data?.contentList = self.contentList
                                completion(true, nil, false)
                            } else {
                                self.recommendedData = _configData.parsed
                                self.recommendedData?.data?.contentList?.removeAll()
                                for recomendedContent in _configData.parsed.data?.contentList ?? [] {
                                    if recomendedContent.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == recomendedContent.title}) ?? true {
                                                self.recommendedData?.data?.contentList?.append(recomendedContent)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        self.recommendedData?.data?.contentList?.append(recomendedContent)
                                    }
                                }
                                completion(true, nil, false)
                            }
                        } else {
                            if _configData.serviceResponse.statusCode != 200 {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, true)
                                } else {
                                    completion(false, "Some Error Occurred", true)
                                }
                            } else {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, false)
                                } else {
                                    completion(false, "Some Error Occurred", false)
                                }
                            }
                        }
                    } else if _configData.serviceResponse.statusCode == 401 {
                        completion(false, kSessionExpire, false)
                    } else {
                        completion(false, "Some Error Occurred", true)
                    }
                } else if let error = error {
                    completion(false, error.error.localizedDescription, true)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            })
        }
    }

    func makeTARecommendationCalls(_ completion: @escaping(Bool, String?, Bool) -> Void) {
       let taValues = UtilityFunction.shared.defractTAShowType(taShowType)
        if let values = taValues {
            if values.count >= 2 {
                apiParams["showType"] = values[0]
                apiParams["contentType"] = values[1]
            }
        }
        taRecommendationVM.fetchDetailScreenRecommendedData(postQueryParams: apiParams) { (configData, error) in
            if !error {
                if let content = configData {
                    self.recommendedData = BARecommendedContentModal()
                    self.recommendedData?.data = RecommendedContentData()
                    if let data = content.data?.contentList {
                        let taData = UtilityFunction.shared.filterTAData(data)
                        self.recommendedData?.data?.contentList = taData
                    }
//                    self.recommendedData?.data?.contentList = (content.data?.contentList)! to avoid unwrapping
                }
                completion(true, nil, false)
            } else {
                self.errorCode = self.taRecommendationVM.errorCode
                completion(false, "Some Error Occurred", true)
            }
        }
    }

    func extractShowType() {
        if taShowType.contains("-") {
            let delimiter = "-"
            let splitArray = taShowType.components(separatedBy: delimiter)
            apiParams["contentType"] = splitArray[1] //"TV_SHOWS" //
            apiParams["showType"] =  splitArray[0] //"VOD" //
        }
    }
    
    // MARK: Get TVOD Content List Of Active Ones
    func checkForTVODData( _ completion: @escaping () -> Void) {
        tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: 0)
        if let tVODVM = tvodVM {
            tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                if flagValue {
                    self.tvodData = tVODVM.tvodModal?.data
                    completion()
                } else {
                    completion()
                }
            }
            return
        }
        completion()
    }

}

//
//  BAPlayBackUrlRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAPlayBackUrlScreenRepo {
    var apiEndPoint: String {get set}
    func getPlaybackUrl(apiParams: APIParams, completion: @escaping(APIServicResult<BAPlayBackUrlModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAPlayBackUrlRepo: BAPlayBackUrlScreenRepo {
    var apiEndPoint: String = ""
    func getPlaybackUrl(apiParams: APIParams, completion: @escaping(APIServicResult<BAPlayBackUrlModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json"]
        let target = ServiceRequestDetail.init(endpoint: .fetchPlayData(param: apiParams, headers: apiHeader     , endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAPlayBackUrlModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BAWatchingRepo.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 02/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAWatchingScreenRepo {
    func getWatchingData(apiParams: APIParams, completion: @escaping(APIServicResult<BAWatchingModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAWatchingRepo: BAWatchingScreenRepo {
    func getWatchingData(apiParams: APIParams, completion: @escaping (APIServicResult<BAWatchingModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .watchingData(param: apiParams, headers: apiHeader, endUrlString: ""))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAWatchingModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BAVODDetailViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol BAVODDetailViewModalProtocol {
    var contentDetail: ContentDetailData? { get set }
    func getContentScreenData(completion: @escaping(Bool, String?, Bool) -> Void)
    //func addRemoveFromWatchlist(completion: @escaping(Bool, String?) -> Void)
}

class BAVODDetailViewModal: BAVODDetailViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: ContentScreenDetailRepo
   // var addRemoveWatchlistRepo: AddRemoveWatchlistScreenRepo
    var endPoint: String = ""
    var apiParams = APIParams()

    var contentDetail: ContentDetailData?
    var addRemoveWatchlist: BAAddRemoveWatchlist?

    init(repo: ContentScreenDetailRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }

    func getContentScreenData(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["profileId"] = BAKeychainManager().profileId
        requestToken = repo.getContentScreenData(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
             if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.contentDetail = _configData.parsed.data
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

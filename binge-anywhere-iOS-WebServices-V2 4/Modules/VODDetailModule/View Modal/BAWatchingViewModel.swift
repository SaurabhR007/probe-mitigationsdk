//
//  BAWatchingViewModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 02/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAWatchingViewModalProtocol {
    func getWatchingtData(_ completion: @escaping (Bool, String, Bool) -> Void)
}

class BAWatchingViewModal: BAWatchingViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAWatchingRepo
    var baId: String = BAKeychainManager().baId
    var watchListData: BAWatchingModal?
    var apiParams = APIParams()

    init(repo: BAWatchingRepo) {
        self.repo = repo
    }

    func getWatchingtData(_ completion: @escaping (Bool, String, Bool) -> Void) {
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["profileId"] = BAKeychainManager().profileId //"584c81e8-3326-4f6b-a08a-9ba6db8b4e6c"
        requestToken = repo.getWatchingData(apiParams: apiParams, completion: { (data, _) in
            self.requestToken = nil
            if let _data = data {
                if let statusCode = _data.parsed.code {
                    if statusCode == 0 {
                        completion(true, _data.parsed.message ?? "", false)
                    } else {
                       if _data.serviceResponse.statusCode != 200 {
                            if let errorMessage = _data.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _data.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _data.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

//
//  BAPlayBackUrlViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAPlayBackUrlViewModalProtocol {
    var playbackUrlModal: PlaybackData? { get set }
    func getPlaybackUrl(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAPlayBackUrlViewModal: BAPlayBackUrlViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAPlayBackUrlRepo
    var endPoint: String = ""
    var playbackUrlModal: PlaybackData?

    init(repo: BAPlayBackUrlRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }

    func getPlaybackUrl(completion: @escaping(Bool, String?, Bool) -> Void) {
        let apiParams = APIParams()
        requestToken = repo.getPlaybackUrl(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
             if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.playbackUrlModal = _configData.parsed.data
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

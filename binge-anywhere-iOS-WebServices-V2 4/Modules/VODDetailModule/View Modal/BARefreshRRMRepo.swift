//
//  BARefreshRRMRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 16/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BARefreshRRMScreenRepo {
    var apiEndPoint: String {get set}
    func getRefreshRRMDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BARRMSessionModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BARefreshRRMRepo: BARefreshRRMScreenRepo {
    var apiEndPoint: String = ""
    func getRefreshRRMDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BARRMSessionModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "x-device-type": "IOS", "x-app-id": "vr-dm-ios-uat", "x-device-id": kDeviceId, "authorization": "bearer \(BAKeychainManager().userAccessToken)", "x-subscriber-id" : BAKeychainManager().sId, "x-device-platform": "MOBILE", "x-app-key":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6InZyLWRtLWlvcy11YXQiLCJrZXkiOiJ2ci1kbS1pb3MtdWF0In0.Mav7FEycUhVkkWZsNzldk2NBcVji7HhDwvEPPSU7qOI", "x-subscriber-name": "Riaz"]
        print("Param in Repo ---->", apiParams)
        let target = ServiceRequestDetail.init(endpoint: .refreshRRM(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BARRMSessionModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

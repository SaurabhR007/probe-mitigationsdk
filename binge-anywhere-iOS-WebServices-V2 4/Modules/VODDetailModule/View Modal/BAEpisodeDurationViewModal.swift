//
//  BAEpisodeDurationViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 18/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAEpisodeDurationViewModalProtocol {
    var episodeDurationsDetail: BAEpisodeDurationModal? {get set}
    func getEpisodeDurations(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAEpisodeDurationViewModal: BAEpisodeDurationViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAEpisodeDurationsScreenRepo?
    var episodeDurationsDetail: BAEpisodeDurationModal?
    var episodeDetails = [[ : ]]
    var apiParams = APIParams()

    init(repo: BAEpisodeDurationsScreenRepo, endPoint: Int, episodes: [[AnyHashable: Any]]) {
        self.repo = repo
        self.episodeDetails = episodes
        self.repo?.apiEndPoint = BAKeychainManager().sId
    }

    func getEpisodeDurations(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["subscriberId"] = BAKeychainManager().sId
        apiParams["profileId"] = BAKeychainManager().profileId
        apiParams["contentIdAndType"] = self.episodeDetails
        requestToken = repo?.getEpisodeDurations(apiParams: apiParams, completion: { configData, error in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.episodeDurationsDetail = _configData.parsed
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

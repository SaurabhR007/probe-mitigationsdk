//
//  BAZeeTagViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAZeeTagViewModalProtocol {
    var zeeTag: TagData? { get set }
    func getZeeTagDetail(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAZeeTagViewModal: BAZeeTagViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAZeeTagRepo
    var endPoint: String = ""
    var zeeTag: TagData?

    init(repo: BAZeeTagRepo, endPoint: String) {
        self.repo = repo
    }

    func getZeeTagDetail(completion: @escaping(Bool, String?, Bool) -> Void) {
        let apiParams = APIParams()
        requestToken = repo.getZeeTagDetail(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
             if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.zeeTag = _configData.parsed.data
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

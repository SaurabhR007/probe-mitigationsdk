//
//  BARecommendedContentRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol RecommendedContentRailRepo {
    var apiEndPoint: String {get set}
    func getRecommendedContentData(apiParams: APIParams, completion: @escaping(APIServicResult<BARecommendedContentModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct RecommendedContentRepo: RecommendedContentRailRepo {
    var apiEndPoint: String = ""

    func getRecommendedContentData(apiParams: APIParams, completion: @escaping(APIServicResult<BARecommendedContentModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        //let apiParams = APIParams()
        let header: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId, "deviceName": kDeviceModal, "platform": "BINGE_ANYWHERE"]
        let target = ServiceRequestDetail.init(endpoint: .recommendedContentData(param: apiParams, headers: header, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BARecommendedContentModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

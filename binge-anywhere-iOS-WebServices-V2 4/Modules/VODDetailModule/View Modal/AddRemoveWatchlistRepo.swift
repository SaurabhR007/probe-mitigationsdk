//
//  AddRemoveWatchlistRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol AddRemoveWatchlistScreenRepo {
    var apiEndPoint: String {get set}
    func addRemoveWatchlist(apiParams: APIParams, completion: @escaping(APIServicResult<BAAddRemoveWatchlist>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct AddRemoveWatchlistRepo: AddRemoveWatchlistScreenRepo {
    var apiEndPoint: String = ""
    func addRemoveWatchlist(apiParams: APIParams, completion: @escaping(APIServicResult<BAAddRemoveWatchlist>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .addRemoveWatchlist(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAAddRemoveWatchlist>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

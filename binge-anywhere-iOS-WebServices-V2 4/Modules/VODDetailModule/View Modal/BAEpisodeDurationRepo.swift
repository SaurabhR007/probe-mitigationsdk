//
//  BAEpisodeDurationRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 18/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAEpisodeDurationsScreenRepo {
    var apiEndPoint: String {get set}
    func getEpisodeDurations(apiParams: APIParams, completion: @escaping(APIServicResult<BAEpisodeDurationModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAEpisodeDurationsRepo: BAEpisodeDurationsScreenRepo {
    var apiEndPoint: String = ""
    func getEpisodeDurations(apiParams: APIParams, completion: @escaping(APIServicResult<BAEpisodeDurationModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId, "deviceName": kDeviceModal, "authorization": "bearer \(BAKeychainManager().userAccessToken)"]
        let target = ServiceRequestDetail.init(endpoint: .episodeDurations(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAEpisodeDurationModal>, ServiceProviderError>) in
           completion(response.value, response.error)
        }
    }
}

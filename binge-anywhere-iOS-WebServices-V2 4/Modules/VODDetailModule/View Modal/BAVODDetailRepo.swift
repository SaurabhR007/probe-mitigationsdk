//
//  BAVODDetailBaseRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol ContentScreenDetailRepo {
    var apiEndPoint: String {get set}
    func getContentScreenData(apiParams: APIParams, completion: @escaping(APIServicResult<BAVODDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct ContentDetailRepo: ContentScreenDetailRepo {
    var apiEndPoint: String = ""
    func getContentScreenData(apiParams: APIParams, completion: @escaping(APIServicResult<BAVODDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .contentDetailData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAVODDetailModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

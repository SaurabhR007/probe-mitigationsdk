//
//  BAPlayBackUrlModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct BAPlayBackUrlModal : Codable {
    let code : Int?
    let message : String?
    let data : PlaybackData?
    let localizedMessage : String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(PlaybackData.self, forKey: .data)
        localizedMessage = try values.decodeIfPresent(String.self, forKey: .localizedMessage)
    }

}

struct PlaybackData : Codable {
    let description : String?
    let rating : String?
    let previewImage : String?
    let detail : PlaybackDetail?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        previewImage = try values.decodeIfPresent(String.self, forKey: .previewImage)
        detail = try values.decodeIfPresent(PlaybackDetail.self, forKey: .detail)
    }

}

struct PlaybackDetail : Codable {
    let contractName : String?
    let entitlements : [String]?
    let offerId : OfferId?
    let enforceL3 : String?
    let dashPlayreadyPlayUrl : String?
    let dashPlayreadyLicenseUrl : String?
    let dashWidewinePlayUrl : String?
    let dashWidewineLicenseUrl : String?
    let ssPlayreadyPlayUrl : String?
    let ssPlayreadyLicenseUrl : String?
    let dashWidewineTrailerUrl : String?
    let dashPlayreadyTrailerUrl : String?
    let fairplayUrl : String?
    let fairplayTrailerUrl : String?
    var playUrl : String?
    let trailerUrl : String?
    let authorizedCookies : String?

    enum CodingKeys: String, CodingKey {

        case contractName = "contractName"
        case entitlements = "entitlements"
        case offerId = "offerId"
        case enforceL3 = "enforceL3"
        case dashPlayreadyPlayUrl = "dashPlayreadyPlayUrl"
        case dashPlayreadyLicenseUrl = "dashPlayreadyLicenseUrl"
        case dashWidewinePlayUrl = "dashWidewinePlayUrl"
        case dashWidewineLicenseUrl = "dashWidewineLicenseUrl"
        case ssPlayreadyPlayUrl = "ssPlayreadyPlayUrl"
        case ssPlayreadyLicenseUrl = "ssPlayreadyLicenseUrl"
        case dashWidewineTrailerUrl = "dashWidewineTrailerUrl"
        case dashPlayreadyTrailerUrl = "dashPlayreadyTrailerUrl"
        case fairplayUrl = "fairplayUrl"
        case fairplayTrailerUrl = "fairplayTrailerUrl"
        case playUrl = "playUrl"
        case trailerUrl = "trailerUrl"
        case authorizedCookies = "authorizedCookies"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
        offerId = try values.decodeIfPresent(OfferId.self, forKey: .offerId)
        enforceL3 = try values.decodeIfPresent(String.self, forKey: .enforceL3)
        dashPlayreadyPlayUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyPlayUrl)
        dashPlayreadyLicenseUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyLicenseUrl)
        dashWidewinePlayUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewinePlayUrl)
        dashWidewineLicenseUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewineLicenseUrl)
        ssPlayreadyPlayUrl = try values.decodeIfPresent(String.self, forKey: .ssPlayreadyPlayUrl)
        ssPlayreadyLicenseUrl = try values.decodeIfPresent(String.self, forKey: .ssPlayreadyLicenseUrl)
        dashWidewineTrailerUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewineTrailerUrl)
        dashPlayreadyTrailerUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyTrailerUrl)
        fairplayUrl = try values.decodeIfPresent(String.self, forKey: .fairplayUrl)
        fairplayTrailerUrl = try values.decodeIfPresent(String.self, forKey: .fairplayTrailerUrl)
        playUrl = try values.decodeIfPresent(String.self, forKey: .playUrl)
        trailerUrl = try values.decodeIfPresent(String.self, forKey: .trailerUrl)
        authorizedCookies = try values.decodeIfPresent(String.self, forKey: .authorizedCookies)
    }

}

struct OfferId : Codable {
    let key : String?
    let epids : String?

    enum CodingKeys: String, CodingKey {

        case key = "key"
        case epids = "epids"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        epids = try values.decodeIfPresent(String.self, forKey: .epids)
    }

}


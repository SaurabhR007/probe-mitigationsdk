//
//  BAZeeTagModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct BAZeeTagModal: Codable {
    let code: Int?
    let title: String?
    let message: String?
    let data: TagData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        data = try values.decodeIfPresent(TagData.self, forKey: .data)
    }
}

struct TagData: Codable {
    let tag: String?
    init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    tag = try values.decodeIfPresent(String.self, forKey: .tag)
    }
}

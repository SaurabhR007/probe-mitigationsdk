//
//  BAWatchingModal.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 02/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAWatchingModal: Codable {
    let code: Int?
    let message: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}

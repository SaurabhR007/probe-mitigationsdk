//
//  BAEpisodeDurationModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 18/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct BAEpisodeDurationModal: Codable {
    let code: Int?
    let message: String?
    var data: [EpisodeDurationDetail]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([EpisodeDurationDetail].self, forKey: .data)
    }
}

struct EpisodeDurationDetail: Codable {
    let subscriberId: String?
    let profileId: String?
    let contentId: Int?
    let secondsWatched: Int?
    let durationInSeconds: Int?
    let favourite: Bool?
    init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
        subscriberId = try values.decodeIfPresent(String.self, forKey: .subscriberId)
        profileId = try values.decodeIfPresent(String.self, forKey: .profileId)
        contentId = try values.decodeIfPresent(Int.self, forKey: .contentId)
        secondsWatched = try values.decodeIfPresent(Int.self, forKey: .secondsWatched)
        durationInSeconds = try values.decodeIfPresent(Int.self, forKey: .durationInSeconds)
        favourite = try values.decodeIfPresent(Bool.self, forKey: .favourite)
    }
}

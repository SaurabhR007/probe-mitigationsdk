//
//  BARRMSessionModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 16/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BARRMSessionModal: Codable {
    let code: Int?
    let message: String?
    let data: RRMData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(RRMData.self, forKey: .data)
    }
}

struct RRMData: Codable {
    let token: String?
    let expiresIn: Int64?
    init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    token = try values.decodeIfPresent(String.self, forKey: .token)
    expiresIn = try values.decodeIfPresent(Int64.self, forKey: .expiresIn)
    }
}


//
//  BAAddRemoveWatchlist.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 03/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAAddRemoveWatchlist: Codable {
    let code: Int?
    let message: String?
    let data: AddRemoveData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AddRemoveData.self, forKey: .data)
    }
}

struct AddRemoveData: Codable {
    let status: Bool?
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }
}

//
//  BARecommendedContentModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

struct BARecommendedContentModal: Codable {
    let code: Int?
    let message: String?
    var data: RecommendedContentData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(RecommendedContentData.self, forKey: .data)
    }

    init() {
        code = 0
        message = ""
        data = nil
    }
}

// MARK: Recommended Content Modal
struct RecommendedContentData: Codable {
    let id: Int?
    var title: String?
    let sectionType: String?
    var layoutType: String?
    var contentList: [BAContentListModel]?
    let totalCount: Int?
    let autoScroll: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        autoScroll = try values.decodeIfPresent(Bool.self, forKey: .autoScroll)
    }

    init() {
        id = 0
        title = ""
        sectionType = ""
        layoutType = ""
        contentList = nil
        totalCount = 0
        autoScroll = false
    }

}

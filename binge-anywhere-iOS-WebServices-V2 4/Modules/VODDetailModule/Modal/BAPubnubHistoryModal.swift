//
//  BAPubnubHistoryModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 18/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct BAPubnubHistoryModal: Codable {
    let isDeviceManagement: Bool?
    let isPremium: Bool?
    let sName: String?
    let devices: [String]?
    let timestamp: String?
    let deviceInfo: [PubnubDeviceInfo]?
    let acStatus: String?
    let profiles: [PubnubProfiles]?
    let sid: String?
    let entitlements: [Entitlements]?
    let rmn: String?
    let isPVR: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isDeviceManagement = try values.decodeIfPresent(Bool.self, forKey: .isDeviceManagement)
        isPremium = try values.decodeIfPresent(Bool.self, forKey: .isPremium)
        sName = try values.decodeIfPresent(String.self, forKey: .sName)
        devices = try values.decodeIfPresent([String].self, forKey: .devices)
        timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
        deviceInfo = try values.decodeIfPresent([PubnubDeviceInfo].self, forKey: .deviceInfo)
        acStatus = try values.decodeIfPresent(String.self, forKey: .acStatus)
        profiles = try values.decodeIfPresent([PubnubProfiles].self, forKey: .profiles)
        sid = try values.decodeIfPresent(String.self, forKey: .sid)
        entitlements = try values.decodeIfPresent([Entitlements].self, forKey: .entitlements)
        rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
        isPVR = try values.decodeIfPresent(Bool.self, forKey: .isPVR)
    }
}

struct PubnubDeviceInfo: Codable {
    let status: String?
    let logout: Bool?
    let bingeAccountStatus: String?
    let serialNo: String?
    let deviceList: [String]?
    let deviceType: String?
    let baId: String?
    let contentPlayBackHybrid: Bool?
    let logoutHybrid: Bool?
    let contentPlayBack: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        logout = try values.decodeIfPresent(Bool.self, forKey: .logout)
        bingeAccountStatus = try values.decodeIfPresent(String.self, forKey: .bingeAccountStatus)
        serialNo = try values.decodeIfPresent(String.self, forKey: .serialNo)
        deviceList = try values.decodeIfPresent([String].self, forKey: .deviceList)
        deviceType = try values.decodeIfPresent(String.self, forKey: .deviceType)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        contentPlayBack = try values.decodeIfPresent(Bool.self, forKey: .contentPlayBack)
        contentPlayBackHybrid = try values.decodeIfPresent(Bool.self, forKey: .contentPlayBackHybrid)
        logoutHybrid = try values.decodeIfPresent(Bool.self, forKey: .logoutHybrid)
    }
}

struct Entitlements : Codable {
    let pkgId : String?
    let type : String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pkgId = try values.decodeIfPresent(String.self, forKey: .pkgId)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
}

struct PubnubProfiles: Codable {
    let appProfileLanguage: String?
    let profileName: String?
    let id: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        appProfileLanguage = try values.decodeIfPresent(String.self, forKey: .appProfileLanguage)
        profileName = try values.decodeIfPresent(String.self, forKey: .profileName)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }
}




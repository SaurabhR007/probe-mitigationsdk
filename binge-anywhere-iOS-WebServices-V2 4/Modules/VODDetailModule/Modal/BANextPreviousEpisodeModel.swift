//
//  BANextPreviousEpisodeModel.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BANextPreviousEpisodeModel: Codable {
    let code: Int?
    let message: String?
    let data: NextPreviousEpisodeData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(NextPreviousEpisodeData.self, forKey: .data)
    }
}

struct NextPreviousEpisodeData: Codable {
    let nextEpisodeExists: Bool?
    let previousEpisodeExists: Bool?
    let nextEpisode: Episodes?
    let previousEpisode: Episodes?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        nextEpisodeExists = try values.decodeIfPresent(Bool.self, forKey: .nextEpisodeExists)
        previousEpisodeExists = try values.decodeIfPresent(Bool.self, forKey: .previousEpisodeExists)
        nextEpisode = try values.decodeIfPresent(Episodes.self, forKey: .nextEpisode)
        previousEpisode = try values.decodeIfPresent(Episodes.self, forKey: .previousEpisode)
    }
}

//struct NextEpisode: Codable {
//    let id: Int?
//    let provider: String?
//    let contentType: String?
//    let entitlements: [String]?
//    let title: String?
//    let boxCoverImage: String?
//    let posterImage: String?
//    let genres: [String]?
//    let languages: [String]?
//    let description: String?
//    let totalDuration: String?
//    let watchedDuration: String?
//    let episodeId: Int?
//    let playerDetail: PlayerDetail?
//    let partnerDeepLinkUrl: String?
//    let providerContentId: String?
//    let contractName: String?
//    let hd: Bool?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        provider = try values.decodeIfPresent(String.self, forKey: .provider)
//        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
//        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        boxCoverImage = try values.decodeIfPresent(String.self, forKey: .boxCoverImage)
//        posterImage = try values.decodeIfPresent(String.self, forKey: .posterImage)
//        genres = try values.decodeIfPresent([String].self, forKey: .genres)
//        languages = try values.decodeIfPresent([String].self, forKey: .languages)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        totalDuration = try values.decodeIfPresent(String.self, forKey: .totalDuration)
//        watchedDuration = try values.decodeIfPresent(String.self, forKey: .watchedDuration)
//        episodeId = try values.decodeIfPresent(Int.self, forKey: .episodeId)
//        playerDetail = try values.decodeIfPresent(PlayerDetail.self, forKey: .playerDetail)
//        partnerDeepLinkUrl = try values.decodeIfPresent(String.self, forKey: .partnerDeepLinkUrl)
//        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
//        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
//        hd = try values.decodeIfPresent(Bool.self, forKey: .hd)
//    }
//
//}

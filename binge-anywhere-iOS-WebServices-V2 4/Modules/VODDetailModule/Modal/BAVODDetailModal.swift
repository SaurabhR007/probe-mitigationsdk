//
//  BAVODDetailModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 31/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

struct BAVODDetailModal: Codable {
    let code: Int?
    let message: String?
    var data: ContentDetailData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ContentDetailData.self, forKey: .data)
    }
}

// MARK: Content Detail Modal
struct ContentDetailData: Codable {
    var meta: Meta?
    var detail: Detail?
    var seriesList: [SeriesYearsList]?
    var lastWatch: LastWatch?
    var nextEpisodeId: Int?

    init() {}
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        meta = try values.decodeIfPresent(Meta.self, forKey: .meta)
        detail = try values.decodeIfPresent(Detail.self, forKey: .detail)
        seriesList = try values.decodeIfPresent([SeriesYearsList].self, forKey: .seriesList)
        lastWatch = try values.decodeIfPresent(LastWatch.self, forKey: .lastWatch)
        nextEpisodeId = try values.decodeIfPresent(Int.self, forKey: .nextEpisodeId)
    }

    mutating func updateSeasons(_ seriesDetail: SeriesDetailData?, seasonId: Int, contentType: String, parentContentType: String) {
        
        if contentType == ContentType.series.rawValue {
            if seriesList?.count == 0 {
                //seriesList = [SeriesYearsList.makeEmptySeries()]
                var series = SeriesYearsList.makeEmptySeries()
                series.id = seasonId
                seriesList = [series]
            }
        }
        
        if contentType == ContentType.tvShows.rawValue && parentContentType == ContentType.series.rawValue {
            if seriesList?.count == 0 {
                //seriesList = [SeriesYearsList.makeEmptySeries()]
                var series = SeriesYearsList.makeEmptySeries()
                series.id = seasonId
                seriesList = [series]
            }
        }
        
        for index in 0..<(seriesList ?? [SeriesYearsList]()).count {
            var series = seriesList?[index]
            if series?.id ?? -1 == seasonId {
                if series?.seriesDetail == nil {
                    series?.seriesDetail = SeriesDetailData()
                }
                series?.seriesDetail?.addNewEpisodesFromSeriesDetail(seriesDetail)
                seriesList?[index] = series ?? SeriesYearsList()
            }
        }
    }

}

// MARK: Series List Modal
struct SeriesYearsList: Codable {
    var id: Int?
    var seriesName: String?
    var seriesDetail: SeriesDetailData?

    init() {}

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        seriesName = try values.decodeIfPresent(String.self, forKey: .seriesName)
    }
}

extension SeriesYearsList {
    static func makeEmptySeries() -> SeriesYearsList {
        return SeriesYearsList()
    }
}

// MARK: Meta Detail Modal
struct Meta: Codable {
    let vodId: Int?
    let brandId: Int?
    let seriesId: Int?
    let brandTitle: String?
    let vodTitle: String?
    let seriesTitle: String?
    let vodAssetId: String?
    let downloadExpiry: Int?
    let description: String?
    let brandDescription: String?
    let seriesDescription: String?
    let vodDescription: String?
    let producer: [String]?
    let director: [String]?
    let rating: String?
    let actor: [String]?
    let writer: [String]?
    let audio: [String]?
    let boxCoverImage: String?
    let imageUrlApp: String?
    let releaseYear: String?
    let genre: [String]?
    let seasonCount: Int?
    let expiry: String?
    let duration: Int?
    var contentType: String?
    let parentContentType: String?
    let provider: String?
    let providerContentId: String?
    let posterImage: String?
    let taShowType: String?
    let showCase: Bool?
    let isPlaybackStarted: Bool?
    let purchaseExpiry: Int?
    let displayFingerPrint: Bool?
    let partnerDeepLinkUrl: String?
    let partnerSubscriptionType: String?
    let partnerWebUrl: String?
    let vodContentType: String?
    let partnerTrailerInfo: String?
    let hd: Bool?
    let favourite: Bool?
    let allowedForKids: Bool?
    let downloadable: Bool?
    let hotstarAppDeeplink: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        seriesId = try values.decodeIfPresent(Int.self, forKey: .seriesId)
        brandId = try values.decodeIfPresent(Int.self, forKey: .brandId)
        vodId = try values.decodeIfPresent(Int.self, forKey: .vodId)
        brandTitle = try values.decodeIfPresent(String.self, forKey: .brandTitle)
        vodTitle = try values.decodeIfPresent(String.self, forKey: .vodTitle)
        seriesTitle = try values.decodeIfPresent(String.self, forKey: .seriesTitle)
        vodAssetId = try values.decodeIfPresent(String.self, forKey: .vodAssetId)
        downloadExpiry = try values.decodeIfPresent(Int.self, forKey: .downloadExpiry)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        brandDescription = try values.decodeIfPresent(String.self, forKey: .brandDescription)
        seriesDescription = try values.decodeIfPresent(String.self, forKey: .seriesDescription)
        vodDescription = try values.decodeIfPresent(String.self, forKey: .vodDescription)
        producer = try values.decodeIfPresent([String].self, forKey: .producer)
        director = try values.decodeIfPresent([String].self, forKey: .director)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        actor = try values.decodeIfPresent([String].self, forKey: .actor)
        writer = try values.decodeIfPresent([String].self, forKey: .writer)
        audio = try values.decodeIfPresent([String].self, forKey: .audio)
        boxCoverImage = try values.decodeIfPresent(String.self, forKey: .boxCoverImage)
        releaseYear = try values.decodeIfPresent(String.self, forKey: .releaseYear)
        genre = try values.decodeIfPresent([String].self, forKey: .genre)
        seasonCount = try values.decodeIfPresent(Int.self, forKey: .seasonCount)
        expiry = try values.decodeIfPresent(String.self, forKey: .expiry)
        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        parentContentType = try values.decodeIfPresent(String.self, forKey: .parentContentType)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
        posterImage = try values.decodeIfPresent(String.self, forKey: .posterImage)
        taShowType = try values.decodeIfPresent(String.self, forKey: .taShowType)
        displayFingerPrint = try values.decodeIfPresent(Bool.self, forKey: .displayFingerPrint)
        hd = try values.decodeIfPresent(Bool.self, forKey: .hd)
        favourite = try values.decodeIfPresent(Bool.self, forKey: .favourite)
        allowedForKids = try values.decodeIfPresent(Bool.self, forKey: .allowedForKids)
        downloadable = try values.decodeIfPresent(Bool.self, forKey: .downloadable)
        partnerDeepLinkUrl = try values.decodeIfPresent(String.self, forKey: .partnerDeepLinkUrl)
        imageUrlApp = try values.decodeIfPresent(String.self, forKey: .imageUrlApp)
        partnerTrailerInfo = try values.decodeIfPresent(String.self, forKey: .partnerTrailerInfo)
        partnerWebUrl = try values.decodeIfPresent(String.self, forKey: .partnerWebUrl)
        vodContentType = try values.decodeIfPresent(String.self, forKey: .vodContentType)
        hotstarAppDeeplink = try values.decodeIfPresent(String.self, forKey: .hotstarAppDeeplink)
        partnerSubscriptionType = try values.decodeIfPresent(String.self, forKey: .partnerSubscriptionType)
        purchaseExpiry = try values.decodeIfPresent(Int.self, forKey: .purchaseExpiry)
        showCase = try values.decodeIfPresent(Bool.self, forKey: .showCase)
        isPlaybackStarted = try values.decodeIfPresent(Bool.self, forKey: .isPlaybackStarted)
    }

}

// MARK: - Details Modal
struct Detail: Codable {
    let contractName: String?
    let entitlements: [String]?
    var playUrl: String?
    let trailerUrl: String?
    let licenseUrl: String?
    let dashPlayreadyPlayUrl: String?
    let dashPlayreadyLicenseUrl: String?
    let dashWidewinePlayUrl: String?
    let dashWidewineLicenseUrl: String?
    let ssPlayreadyPlayUrl: String?
    let ssPlayreadyLicenseUrl: String?
    let dashWidewineTrailerUrl: String?
    let dashPlayreadyTrailerUrl: String?
    let partnerDeepLinkUrl: String?
    let fairplayUrl: String?
    let fairplayTrailerUrl: String?
    let authorizedCookies: String?
    let offerId: OfferID?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
        playUrl = try values.decodeIfPresent(String.self, forKey: .playUrl)
        trailerUrl = try values.decodeIfPresent(String.self, forKey: .trailerUrl)
        licenseUrl = try values.decodeIfPresent(String.self, forKey: .licenseUrl)
        dashPlayreadyPlayUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyPlayUrl)
        dashPlayreadyLicenseUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyLicenseUrl)
        dashWidewinePlayUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewinePlayUrl)
        dashWidewineLicenseUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewineLicenseUrl)
        ssPlayreadyPlayUrl = try values.decodeIfPresent(String.self, forKey: .ssPlayreadyPlayUrl)
        ssPlayreadyLicenseUrl = try values.decodeIfPresent(String.self, forKey: .ssPlayreadyLicenseUrl)
        dashWidewineTrailerUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewineTrailerUrl)
        dashPlayreadyTrailerUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyTrailerUrl)
        fairplayUrl = try values.decodeIfPresent(String.self, forKey: .fairplayUrl)
        fairplayTrailerUrl = try values.decodeIfPresent(String.self, forKey: .fairplayTrailerUrl)
        authorizedCookies = try values.decodeIfPresent(String.self, forKey: .authorizedCookies)
        partnerDeepLinkUrl = try values.decodeIfPresent(String.self, forKey: .partnerDeepLinkUrl)
        offerId = try values.decodeIfPresent(OfferID.self, forKey: .offerId) 
    }
}

struct OfferID: Codable {
    let key: String?
    let epids: [EpiDetail]
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        epids = try values.decodeIfPresent([EpiDetail].self, forKey: .epids) ?? []
    }
}

struct EpiDetail: Codable {
    let epid: String
    let bid: String
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        epid = try values.decodeIfPresent(String.self, forKey: .epid) ?? ""
        bid = try values.decodeIfPresent(String.self, forKey: .bid) ?? ""
    }
}

// MARK: - Last Watched Model
struct LastWatch: Codable {
    let id: Int?
    let contentId: Int?
    let episodeId: Int?
    let provider: String?
    let contentType: String?
    let contentTitle: String?
    let entitlements: [String]?
    let title: String?
    let boxCoverImage: String?
    let posterImage: String?
    let genres: [String]?
    let languages: [String]?
    let description: String?
    let totalDuration: Int?
    let durationInSeconds: Int?
    let secondsWatched: Int?
    let watchedDuration: Int?
    let season: Int?
    let vodId: Int?
    var playerDetail: PlayerDetail?
    let isFavourite: Bool?
    let partnerDeepLinkUrl: String?
    let providerContentId: String?
    let hotstarAppDeeplink: String?
    let contractName: String?
    let seriesId: String?
    let hd: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        vodId = try values.decodeIfPresent(Int.self, forKey: .vodId)
        contentId = try values.decodeIfPresent(Int.self, forKey: .contentId)
        contentTitle = try values.decodeIfPresent(String.self, forKey: .contentTitle)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        boxCoverImage = try values.decodeIfPresent(String.self, forKey: .boxCoverImage)
        posterImage = try values.decodeIfPresent(String.self, forKey: .posterImage)
        genres = try values.decodeIfPresent([String].self, forKey: .genres)
        languages = try values.decodeIfPresent([String].self, forKey: .languages)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        totalDuration = try values.decodeIfPresent(Int.self, forKey: .totalDuration)
        watchedDuration = try values.decodeIfPresent(Int.self, forKey: .watchedDuration)
        episodeId = try values.decodeIfPresent(Int.self, forKey: .episodeId)
        playerDetail = try values.decodeIfPresent(PlayerDetail.self, forKey: .playerDetail)
        partnerDeepLinkUrl = try values.decodeIfPresent(String.self, forKey: .partnerDeepLinkUrl)
        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
        season = try values.decodeIfPresent(Int.self, forKey: .season)
        secondsWatched = try values.decodeIfPresent(Int.self, forKey: .secondsWatched)
        durationInSeconds = try values.decodeIfPresent(Int.self, forKey: .durationInSeconds)
        isFavourite = try values.decodeIfPresent(Bool.self, forKey: .isFavourite)
        seriesId = try values.decodeIfPresent(String.self, forKey: .seriesId)
        hd = try values.decodeIfPresent(Bool.self, forKey: .hd)
        hotstarAppDeeplink = try values.decodeIfPresent(String.self, forKey: .hotstarAppDeeplink)
    }
}

struct PlayerDetail: Codable {
    let contractName: String?
    let entitlements: [String]?
    var playUrl: String?
    let trailerUrl: String?
    let partnerDeepLinkUrl: String?
    let licenseUrl: String?
    let dashPlayreadyPlayUrl: String?
    let dashPlayreadyLicenseUrl: String?
    let dashWidewinePlayUrl: String?
    let dashWidewineLicenseUrl: String?
    let ssPlayreadyPlayUrl: String?
    let ssPlayreadyLicenseUrl: String?
    let dashWidewineTrailerUrl: String?
    let dashPlayreadyTrailerUrl: String?
    let fairplayUrl: String?
    let fairplayTrailerUrl: String?
    let authorizedCookies: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
        playUrl = try values.decodeIfPresent(String.self, forKey: .playUrl)
        trailerUrl = try values.decodeIfPresent(String.self, forKey: .trailerUrl)
        licenseUrl = try values.decodeIfPresent(String.self, forKey: .licenseUrl)
        dashPlayreadyPlayUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyPlayUrl)
        dashPlayreadyLicenseUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyLicenseUrl)
        dashWidewinePlayUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewinePlayUrl)
        dashWidewineLicenseUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewineLicenseUrl)
        ssPlayreadyPlayUrl = try values.decodeIfPresent(String.self, forKey: .ssPlayreadyPlayUrl)
        ssPlayreadyLicenseUrl = try values.decodeIfPresent(String.self, forKey: .ssPlayreadyLicenseUrl)
        dashWidewineTrailerUrl = try values.decodeIfPresent(String.self, forKey: .dashWidewineTrailerUrl)
        dashPlayreadyTrailerUrl = try values.decodeIfPresent(String.self, forKey: .dashPlayreadyTrailerUrl)
        fairplayUrl = try values.decodeIfPresent(String.self, forKey: .fairplayUrl)
        fairplayTrailerUrl = try values.decodeIfPresent(String.self, forKey: .fairplayTrailerUrl)
        authorizedCookies = try values.decodeIfPresent(String.self, forKey: .authorizedCookies)
        partnerDeepLinkUrl = try values.decodeIfPresent(String.self, forKey: .partnerDeepLinkUrl)
    }
}



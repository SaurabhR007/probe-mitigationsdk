//
//  BASeriesDetailModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 06/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BASeriesDetailModal: Codable {
    let code: Int?
    let message: String?
    let data: SeriesDetailData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SeriesDetailData.self, forKey: .data)
    }
}

// MARK: Series Detail Modal
struct SeriesDetailData: Codable {
    var items: [Episodes]?
    var total: Int?
    var offset: Int?
    var limit: Int?

    init() {
        items = [Episodes]()
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([Episodes].self, forKey: .items)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        offset = try values.decodeIfPresent(Int.self, forKey: .offset)
        limit = try values.decodeIfPresent(Int.self, forKey: .limit)
    }

   mutating func addNewEpisodesFromSeriesDetail(_ seriesDetail: SeriesDetailData?) {
        let currentItemsCount = items?.count ?? 0
        guard let seriesDetail = seriesDetail, let kOffset = seriesDetail.offset else { return }
        if currentItemsCount > kOffset { return }
        guard let items = seriesDetail.items else { return }
        self.items?.append(contentsOf: items)
        total = seriesDetail.total
        offset = seriesDetail.offset
        limit = seriesDetail.limit
    }
}

// MARK: Episodes Modal
class Episodes: Codable {
    let id: Int?
    let title: String?
    let boxCoverImage: String?
    let posterImage: String?
    let duration: Int?
    let description: String?
    let contractName: String?
    let entitlements: [String]?
    let provider: String?
    let providerContentId: String?
    var isDetailExpanded = false
    let contentType: String?
    let episodeId: Int?
    let season: Int?
    let playerDetail: PlayerDetail?
    let partnerWebUrl: String?
    var watchedDuration: String?
    var totalDuration: String?
    let partnerDeepLinkUrl: String?
    let hotstarAppDeeplink: String?
    let languages: [String]?
    let genres: [String]?
    let hd: Bool?

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posterImage = try values.decodeIfPresent(String.self, forKey: .posterImage)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        boxCoverImage = try values.decodeIfPresent(String.self, forKey: .boxCoverImage)
        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        episodeId = try values.decodeIfPresent(Int.self, forKey: .episodeId)
        playerDetail = try values.decodeIfPresent(PlayerDetail.self, forKey: .playerDetail)
        watchedDuration = try values.decodeIfPresent(String.self, forKey: .watchedDuration)
        totalDuration = try values.decodeIfPresent(String.self, forKey: .totalDuration)
        partnerDeepLinkUrl = try values.decodeIfPresent(String.self, forKey: .partnerDeepLinkUrl)
        languages = try values.decodeIfPresent([String].self, forKey: .languages)
        genres = try values.decodeIfPresent([String].self, forKey: .genres)
        partnerWebUrl = try values.decodeIfPresent(String.self, forKey: .partnerWebUrl)
        hotstarAppDeeplink = try values.decodeIfPresent(String.self, forKey: .hotstarAppDeeplink)
        season = try values.decodeIfPresent(Int.self, forKey: .season)
        hd = try values.decodeIfPresent(Bool.self, forKey: .hd)
    }
}

// MARK: Player DTO Model
//struct PlayerDetailDTO: Codable {
//    let contractName: String?
//    let entitlements: [String]?
//    let playUrl: String?
//    let trailerUrl: String?
//    let licenseUrl: String?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
//        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
//        playUrl = try values.decodeIfPresent(String.self, forKey: .playUrl)
//        trailerUrl = try values.decodeIfPresent(String.self, forKey: .trailerUrl)
//        licenseUrl = try values.decodeIfPresent(String.self, forKey: .licenseUrl)
//    }
//}

//
//  BASwitchAccountModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 19/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BASwitchAccountModal: Codable {
    let code: Int?
    let message: String?
    let data: AccountProfile?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AccountProfile.self, forKey: .data)
    }
}

struct AccountProfile: Codable {
    let baId: String?
    let defaultProfile: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        defaultProfile = try values.decodeIfPresent(String.self, forKey: .defaultProfile)
    }
}

//
//  BASwitchAccountRepo.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 19/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASwitchAccountRepoProtocol {
    var apiEndPoint: String {get set}
    func switchAccount(completion: @escaping (APIServicResult<BASwitchAccountModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BASwitchAccountRepo: BASwitchAccountRepoProtocol {
    var apiEndPoint: String = BAKeychainManager().baId
    func switchAccount(completion: @escaping (APIServicResult<BASwitchAccountModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiParams = APIParams()
        var target = ServiceRequestDetail.init(endpoint: .switchAccount(param: apiParams, header: nil, endUrlString: apiEndPoint))
        if BAKeychainManager().targetBaId == "0" {
           target.detail.headers["dsn"] = BAKeychainManager().dsn
        } else {
           target.detail.headers["targetBaId"] = BAKeychainManager().targetBaId
        }
        target.detail.headers["locale"] = "en"
        target.detail.headers["platform"] = "ios"
		target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
		target.detail.headers["subscriberId"] = BAKeychainManager().sId
        return APIService().request(target) { (response: APIResult<APIServicResult<BASwitchAccountModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BASwitchAccountViewModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 19/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
protocol BASwitchAccountVMProtocol {
    func switchAccount(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BASwitchAccountViewModal: BASwitchAccountVMProtocol {
    var requestToken: ServiceCancellable?
    var repo: BASwitchAccountRepoProtocol?

    init(repo: BASwitchAccountRepoProtocol) {
        self.repo = repo
    }

    func switchAccount(completion: @escaping(Bool, String?, Bool) -> Void) {
        repo?.apiEndPoint = BAKeychainManager().baId
        requestToken = repo?.switchAccount(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if BAKeychainManager().targetBaId == "0" {
                            BAKeychainManager().targetBaId = _configData.parsed.data?.baId ?? ""
                        }
                        completion(true, _configData.parsed.message, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }

}

//
//  ExtensionSwitchAccount.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

extension SwitchAccountVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userProfileData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kBASwitchAccountTableViewCell) as? BASwitchAccountTableViewCell
        cell?.configureCell(data: userProfileData[indexPath.row]!)
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator()
            if let switchAccountVM = switchAccountModel {
                for user in self.userProfileData {
                    if user?.accountType == "PRIMARY" {
                        BAUserDefaultManager().baId = user?.baId ?? ""
                    }
                }
                BAUserDefaultManager().sId = self.userProfileData[indexPath.row]?.baId ?? ""
                switchAccountVM.switchAccount(completion: { (flag, message) in
                    if flag {
                        self.hideActivityIndicator()
                        BAUserDefaultManager().baId = BAUserDefaultManager().sId
                        MoengageManager.shared.setUniqueUser()
                        kAppDelegate.createHomeTabRootView()
                    } else {
                        self.showOkAlert(message ?? "Failed to select profile")
                    }
                })
            } else {
                self.showOkAlert("Failed to select profile")
            }
            self.hideActivityIndicator()
        } else {
            self.showOkAlert("The Internet Connection appears to be offline")
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

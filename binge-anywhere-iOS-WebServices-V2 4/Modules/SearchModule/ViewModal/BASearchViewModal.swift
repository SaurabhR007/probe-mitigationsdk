//
//  BASearchViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASearchViewModalProtocol {
    var searchString: String {get set}
    var dataModel: SearchScreenDataModel? {get set}
    var filterModel: [FilterType] {get set}
    func getPreSearchContentData(_ completion: @escaping (Bool, String?, Bool) -> Void)
    func searchScreenData(_ completion: @escaping (Bool, String?, Bool) -> Void)
    func searchScreenFilterData(_ completion: @escaping (Bool, String?, Bool) -> Void)
}

class BASearchViewModal: BASearchViewModalProtocol {
    
    var requestToken: ServiceCancellable?
    var filterModel = [FilterType]()
    var repo: BASearchContentRailRepo
    var preSearchData: SearchDataModel?
    var dataModel: SearchScreenDataModel?
    var endPoint: String = ""
    var pageOffset = 0
    var pageNumber = 1
    var nextPageAvailable = false
    var pageLimit = 10
    var searchString = ""
    var preferredLanguage = [String]()
    var preferredGenre = [String]()
    var apiParams = APIParams()
    var languageFilterArray = [String]()
    var genreFilterArray = [String]()
    var totalSearchDataCount = 0
    var totalPreSearchDataCount = 0
    
    init(repo: BASearchContentRailRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }
    
    func getPreSearchContentData(_ completion: @escaping (Bool, String?, Bool) -> Void) {
        if preferredLanguage.isEmpty && preferredGenre.isEmpty {
            apiParams["pageNumber"] = String(pageNumber)
        } else if preferredLanguage.isEmpty && !preferredGenre.isEmpty {
            apiParams["pageNumber"] = String(pageNumber)
            apiParams["preferGenre"] = preferredGenre
        } else if !preferredLanguage.isEmpty && preferredGenre.isEmpty {
            apiParams["pageNumber"] = String(pageNumber)
            apiParams["preferLang"] = preferredLanguage
        } else {
            apiParams["pageNumber"] = String(pageNumber)
            apiParams["preferGenre"] = preferredGenre
            apiParams["preferLang"] = preferredLanguage
        }
        
        requestToken = repo.getPreSearchContentData(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if self.pageNumber == 1 {
                            self.preSearchData = _configData.parsed
                            self.totalPreSearchDataCount = _configData.parsed.data?.contentList?.count ?? 0
                        } else {
                            for content in (_configData.parsed.data?.contentList)! {
                                self.preSearchData?.data?.contentList?.append(content)
                            }
                            self.totalPreSearchDataCount += _configData.parsed.data?.contentList?.count ?? 0
                        }
                        if let screenData = self.preSearchData?.data {
                            if self.totalPreSearchDataCount == screenData.totalSearchCount || _configData.parsed.data?.contentList?.isEmpty ?? false {
                                self.preSearchData?.data?.continuePaging = false
                                self.nextPageAvailable = false
                            } else {
                                self.preSearchData?.data?.continuePaging = true
                                self.pageNumber += 1
                                self.nextPageAvailable = screenData.continuePaging ?? true
                            }
                            if let data = self.preSearchData?.data?.contentList {
                                self.preSearchData?.data?.contentList = UtilityFunction.shared.filterTAData(data, false)
                            }
                        }
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func searchScreenData(_ completion: @escaping (Bool, String?, Bool) -> Void) {
        if preferredLanguage.isEmpty && preferredGenre.isEmpty {
            repo.apiParams["pageNumber"] = String(pageNumber)
            repo.apiParams["queryString"] = searchString
        } else if preferredLanguage.isEmpty && !preferredGenre.isEmpty {
            repo.apiParams["pageNumber"] = String(pageNumber)
            repo.apiParams["queryString"] = searchString
            repo.apiParams["preferGenre"] = preferredGenre
        } else if !preferredLanguage.isEmpty && preferredGenre.isEmpty {
            repo.apiParams["pageNumber"] = String(pageNumber)
            repo.apiParams["queryString"] = searchString
            repo.apiParams["preferLang"] = preferredLanguage
        } else {
            repo.apiParams["pageNumber"] = String(pageNumber)
            repo.apiParams["queryString"] = searchString
            repo.apiParams["preferGenre"] = preferredGenre
            repo.apiParams["preferLang"] = preferredLanguage
        }
        
        if pageNumber != 1 {
            repo.apiParams["intentUrl"] = dataModel?.data?.intentUrl
        }
        requestToken = repo.searchScreenData(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if _configData.parsed.data?.contentResults?.isEmpty ?? true && self.pageNumber == 1 {
                            self.dataModel = _configData.parsed
                            self.totalSearchDataCount = _configData.parsed.data?.contentResults?.count ?? 0
                        } else {
                            if self.pageNumber == 1 {
                                self.dataModel = _configData.parsed
                                self.totalSearchDataCount = _configData.parsed.data?.contentResults?.count ?? 0
                            } else {
                                self.dataModel?.data?.contentResults?.append(contentsOf: ((_configData.parsed.data?.contentResults)!))
                                self.totalSearchDataCount += ((_configData.parsed.data?.contentResults)!).count
                                if self.totalSearchDataCount == self.dataModel?.data?.totalSearchCount || _configData.parsed.data?.contentResults?.isEmpty ?? false {
                                   self.dataModel?.data?.continuePaging = false
                                } else {
                                    self.dataModel?.data?.continuePaging = true
                                }
                                
                            }
                        }
                        if let _ = self.dataModel?.data {
                            if self.totalSearchDataCount == self.dataModel?.data?.totalSearchCount || _configData.parsed.data?.contentResults?.isEmpty ?? false {
                                self.nextPageAvailable = false
                            } else {
                                self.pageNumber += 1
                                self.nextPageAvailable = true
                            }
                            let filteredValue = UtilityFunction.shared.filterTAData((self.dataModel?.data?.contentResults)!, false)
                            self.dataModel?.data?.contentResults = filteredValue
                        }
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            }
        })
    }
    
    func searchScreenFilterData(_ completion: @escaping (Bool, String?, Bool) -> Void) {
        requestToken = repo.searchScreenFilterData(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.filterModel[0].user?.languageFilter = _configData.parsed.data?.languageFilters ?? []
                        self.filterModel[0].user?.genreFilter = _configData.parsed.data?.genreFilters ?? []
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

//
//  BAPreferredSearchRepo.swift
//  BingeAnywhere
//
//  Created by Rohan Kanoo on 15/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAPreferredSearchRepoProtocol {
    var apiEndPoint: String {get set}
    var apiParams: [AnyHashable: Any] {get set}
    
    func getPreferredLanguageData(completion: @escaping(APIServicResult<BAPreferredSearchModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func getPreferredGenreData(apiParams: APIParams, completion: @escaping(APIServicResult<BAPreferredSearchModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAPreferredSearchRepo: BAPreferredSearchRepoProtocol {
    var apiEndPoint: String = ""
    var apiParams: [AnyHashable : Any] = APIParams()
    
    func getPreferredLanguageData(completion: @escaping (APIServicResult<BAPreferredSearchModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .preferredLanguage(param: apiParams, header: nil, endPoint: apiEndPoint))
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        return APIService().request(target) { (response: APIResult<APIServicResult<BAPreferredSearchModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
    
    func getPreferredGenreData(apiParams: APIParams, completion: @escaping (APIServicResult<BAPreferredSearchModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .preferredGenre(param: apiParams, header: nil, endPoint: apiEndPoint))
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["profileId"] = BAKeychainManager().profileId
        target.detail.headers["platform"] = "dongle"
        return APIService().request(target) { (response: APIResult<APIServicResult<BAPreferredSearchModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

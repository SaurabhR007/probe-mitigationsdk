//
//  BASearchPreferredViewModel.swift
//  BingeAnywhere
//
//  Created by Rohan Kanoo on 15/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASearchPreferredViewModelProtocol {
    var preferredLanguageModel: BAPreferredSearchModel? {get set}
    var preferredGenreModel: BAPreferredSearchModel? {get set}
    func getPreferredLanguageData(_ completion: @escaping (Bool, String?, Bool) -> Void)
    func getPreferredGenreData(_ completion: @escaping (Bool, String?, Bool) -> Void)
    
}

class BASearchPreferredViewModel: BASearchPreferredViewModelProtocol {
    var preferredLanguageModel: BAPreferredSearchModel?
    var preferredGenreModel: BAPreferredSearchModel?
    var requestToken: ServiceCancellable?
    var repo: BAPreferredSearchRepoProtocol
    var apiParams = APIParams()
    
    init(repo: BAPreferredSearchRepoProtocol, apiEndPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = apiEndPoint
    }
    
    func getPreferredLanguageData(_ completion: @escaping (Bool, String?, Bool) -> Void) {
        requestToken = repo.getPreferredLanguageData(completion: { [weak self] (data, error) in
            self?.requestToken = nil
            if let _data = data, let statusCode = data?.parsed.code {
                if statusCode == 0 {
                    self?.preferredLanguageModel = _data.parsed
//                    print("------->Language Data \(String(describing: _data.parsed.data?.languageList))")
                }
                completion(true, nil, false)
            } else {
                completion(false, nil, true)
            }
        })
    }
    
    func getPreferredGenreData(_ completion: @escaping (Bool, String?, Bool) -> Void) {
        apiParams["layout"] = "LANDSCAPE"
        requestToken = repo.getPreferredGenreData(apiParams: apiParams, completion: { [weak self] (data, error) in
            self?.requestToken = nil
            if let _data = data, let statusCode = data?.parsed.code {
                if statusCode == 0 {
                    self?.preferredGenreModel = _data.parsed
//                    print("------->Genre Data \(String(describing: _data.parsed.data?.contentList))")
                }
                completion(true, nil, false)
            } else {
                completion(false, nil, true)
            }
        })
    }

}

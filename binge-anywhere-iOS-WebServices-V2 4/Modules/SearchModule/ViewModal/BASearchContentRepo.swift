//
//  BASearchContentRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASearchContentRailRepo {
    var apiEndPoint: String {get set}
    var apiParams: [AnyHashable: Any] {get set}
    func getPreSearchContentData(apiParams: APIParams, completion: @escaping(APIServicResult<SearchDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func searchScreenData(completion: @escaping(APIServicResult<SearchScreenDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func searchScreenFilterData(completion: @escaping(APIServicResult<FilterModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BASearchContentRepo: BASearchContentRailRepo {    
    var apiEndPoint: String = ""//"https://api.myjson.com/bins/17ceku"
    var apiParams = APIParams()
    
    func getPreSearchContentData(apiParams: APIParams, completion: @escaping(APIServicResult<SearchDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        
        let target = ServiceRequestDetail.init(endpoint: .preSearchContentData(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<SearchDataModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }

    func searchScreenData(completion: @escaping(APIServicResult<SearchScreenDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .searchScreenData(param: apiParams, headers: nil, endUrlString: ""))
        return APIService().request(target) { (response: APIResult<APIServicResult<SearchScreenDataModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }

    func searchScreenFilterData(completion: @escaping(APIServicResult<FilterModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .searchFilterData(param: apiParams, headers: nil))
        return APIService().request(target) { (response: APIResult<APIServicResult<FilterModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }

}

//
//  BAPrefferedSearchDataModel.swift
//  BingeAnywhere
//
//  Created by Rohan Kanoo on 15/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAPreferredSearchModel: Codable {
    let code: Int?
    let message: String?
    let data: UserDetailData?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(UserDetailData.self, forKey: .data)
    }
}

struct BALanguageModel: Codable {
    let id: Int?
    let name: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
}

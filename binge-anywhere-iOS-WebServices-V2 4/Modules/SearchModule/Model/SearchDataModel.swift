//
//  SearchDataModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 14/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

enum FilterType {
    case language
    case genre
    case filterArray

    static let filterArrays = [filterArray]

    static var user: FilterSearchModel?
    var user: FilterSearchModel? {
        get {return FilterType.user}
        set {FilterType.user = newValue}
    }

    var cellCount: Int {
        switch self {
        case .language, .genre: return cellTitle.count
        default:
            return 12
        }
    }

    var cellImage: [UIImage] {
        switch self {
        case .language:
            return [UIImage.init(named: "LanguagesAssamese")!, UIImage.init(named: "LanguagesBangla")!,
                    UIImage.init(named: "LanguagesBhojpuri")!, UIImage.init(named: "LanguagesEnglish")!,
                    UIImage.init(named: "LanguagesGujrati")!, UIImage.init(named: "LanguagesHindi")!,
                    UIImage.init(named: "LanguagesKannada")!, UIImage.init(named: "LanguagesMalayalam")!,
                    UIImage.init(named: "LanguagesMarathi")!, UIImage.init(named: "LanguagesOdia")!,
                    UIImage.init(named: "LanguagesPunjabi")!, UIImage.init(named: "LanguagesTamil")!,
                    UIImage.init(named: "LanguagesTelugu")!, UIImage.init(named: "LanguagesUrdu")!]
        case .genre:
            return [UIImage.init(named: "GenresAction")!, UIImage.init(named: "GenresAdventure")!,
                    UIImage.init(named: "GenresBiopic")!, UIImage.init(named: "GenresComedy")!,
                    UIImage.init(named: "GenresCrime")!, UIImage.init(named: "GenresDance")!,
                    UIImage.init(named: "GenresDebate")!, UIImage.init(named: "GenresDrama")!,
                    UIImage.init(named: "GenresHorror")!, UIImage.init(named: "GenresKids")!,
                    UIImage.init(named: "GenresRomance")!, UIImage.init(named: "GenresThriller")!]
        default:
            return [UIImage()]
        }
    }

    var cellTitle: [String] {
        switch self {
        case .language:
            return ["Assamese", "Bangla", "Bhojpuri", "English", "Gujrati", "Hindi", "Kannada",
                    "Malayalam", "Marathi", "Odia", "Punjabi", "Tamil", "Telugu", "Urdu"]
        case .genre:
            return ["Action", "Adventure", "Biopic", "Comedy", "Crime", "Dance",
                    "Debate", "Drama", "Horror", "Kids", "Romance", "Thriller"]
        default:
            return [""]
        }
    }

    var searchTitle: [String] {
        switch self {
        case .language:
            return user?.languageFilter ?? [""]
        case .genre:
            return user?.genreFilter ?? [""]
        default:
            return [""]
        }
    }

}

struct FilterSearchModel {
    var languageFilter = [String]()
    var selectedLanguageValue = -1
    var genreFilter = [String]()
    var selectedGenereValue = -1
}

struct SearchDataModel: Codable {
    let code: Int?
    let message: String?
    let status: Int?
    let timestamp: Int?
    let error: String?
    let statusCode: Int?
    var data: SearchData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SearchData.self, forKey: .data)
        timestamp = try values.decodeIfPresent(Int.self, forKey: .timestamp)
        error = try values.decodeIfPresent(String.self, forKey: .error)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
    }
}

struct SearchData: Codable {
    let id: Int?
    let title: String?
    let sectionType: String?
    let layoutType: String?
    var contentList: [BAContentListModel]?
    let itemCount: Int?
    let maxCount: Int?
    let totalSearchCount: Int?
    let autoScroll: Bool?
    var continuePaging: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        itemCount = try values.decodeIfPresent(Int.self, forKey: .itemCount)
        maxCount = try values.decodeIfPresent(Int.self, forKey: .maxCount)
        autoScroll = try values.decodeIfPresent(Bool.self, forKey: .autoScroll)
        continuePaging = try values.decodeIfPresent(Bool.self, forKey: .continuePaging)
        totalSearchCount = try values.decodeIfPresent(Int.self, forKey: .totalSearchCount)
    }
}

//// Code Commented as BAContentListModel is now in use however if any major issues came then just need to uncomment this line and change BAContentListModel to this content list model
//struct PreSearchContentList: Codable {
//    let id: Int?
//    let title: String?
//    let image: String?
//    let contentType: String?
//    let provider: String?
//    let categoryType: String?
//    let subTitles: [String]?
//    let contractName: String?
//    let entitlements: [String]?
//    let providerContentId: String?
//    let rentalPrice: String?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        image = try values.decodeIfPresent(String.self, forKey: .image)
//        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
//        provider = try values.decodeIfPresent(String.self, forKey: .provider)
//        categoryType = try values.decodeIfPresent(String.self, forKey: .categoryType)
//        subTitles = try values.decodeIfPresent([String].self, forKey: .subTitles)
//        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
//        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
//        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
//        rentalPrice = try values.decodeIfPresent(String.self, forKey: .rentalPrice)
//    }
//}

struct SearchScreenDataModel: Codable {
    var data: SearchScreenData?
    let message: String?
    let code: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(SearchScreenData.self, forKey: .data)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
    }
}

struct SearchScreenData: Codable {
    let id: Int?
    let title: String?
    let sectionType: String?
    let layoutType: String?
    var contentResults: [BAContentListModel]?
    let intentUrl: String?
    let itemCount: Int?
    var continuePaging: Bool?
    let totalCount: Int?
    let totalSearchCount: Int?
    let maxCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case intentUrl = "intentUrl"
        case sectionType = "sectionType"
        case layoutType = "layoutType"
        case totalCount = "totalCount"
        case contentResults = "contentList"
        case continuePaging = "continuePaging"
        case itemCount = "itemCount"
        case maxCount = "maxCount"
        case totalSearchCount = "totalSearchCount"
    }
    

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        contentResults = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentResults)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        continuePaging = try values.decodeIfPresent(Bool.self, forKey: .continuePaging)
        intentUrl = try values.decodeIfPresent(String.self, forKey: .intentUrl)
        itemCount = try values.decodeIfPresent(Int.self, forKey: .itemCount)
        maxCount = try values.decodeIfPresent(Int.self, forKey: .maxCount)
        totalSearchCount = try values.decodeIfPresent(Int.self, forKey: .totalSearchCount)
    }
}

struct FilterModel: Codable {
    let data: FilterData?
    let message: String?
    let code: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(FilterData.self, forKey: .data)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
    }
}

struct FilterData: Codable {
    let languageFilters: [String]?
    let genreFilters: [String]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        languageFilters = try values.decodeIfPresent([String].self, forKey: .languageFilters)
        genreFilters = try values.decodeIfPresent([String].self, forKey: .genreFilters)
    }
}

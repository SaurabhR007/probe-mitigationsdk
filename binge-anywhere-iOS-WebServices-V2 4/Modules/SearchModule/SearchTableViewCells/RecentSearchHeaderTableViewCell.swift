//
//  RecentSearchHeaderTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol RecentSeacrhClearAllDelegate: class {
    func clearAllButtonTapped()
}

class RecentSearchHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var recentSearchLabel: CustomLabel!
    @IBOutlet weak var clearAllButton: CustomButton!

    weak var delegate: RecentSeacrhClearAllDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        recentSearchLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 18 * screenScaleFactorForWidth), color: .BAwhite)
        clearAllButton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BABlueColor)
    }

    @IBAction func clearAllButtonTapped() {
        if let del = delegate {
            del.clearAllButtonTapped()
        }
    }

}

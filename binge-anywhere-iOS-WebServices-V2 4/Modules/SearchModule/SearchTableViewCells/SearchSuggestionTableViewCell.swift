//
//  SearchSuggestionTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel

protocol AutoSearchTableViewDelegate: class {
    func searchTableCelltapped(_ index: Int)
}

class SearchSuggestionTableViewCell: UITableViewCell {

    @IBOutlet weak var searchIconImageView: UIImageView!
    @IBOutlet weak var searchTitleLabel: CustomLabel!
    weak var delegate: AutoSearchTableViewDelegate?
    var idx: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        searchTitleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
    }

    func setLabelTitle(_ title: String, cellIndex: Int) {
        idx = cellIndex
        searchTitleLabel.text = title
    }

    func cellTapped() {
        if let del = delegate {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.search.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.recentSearch.rawValue])
            del.searchTableCelltapped(idx)
        }
    }
}

//
//  SearchVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class SearchLandingVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noDataFoundLabel: CustomLabel!
    @IBOutlet weak var noDataLabelHeightConstraint: NSLayoutConstraint!

    // MARK: - Cascadable
//    typealias GridViewType = UICollectionView
//    typealias ViewCellType = UICollectionViewCell
//    var cascadeQueue: OperationQueue = OperationQueue()
//    var stopCollectionViewAnimationOnScroll: Bool = true
    var needAnimationFlag: Bool = true

    // MARK: - Variables
    var preSearchlandingVM: BASearchViewModal?
    var preferredLanguageViewModel: BASearchPreferredViewModel?
    var preferredGenreViewModel: BASearchPreferredViewModel?
    var searchBar = UISearchBar()
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var backAdded: Bool = false
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var browseByViewModal: BABrowseByViewModal?
    var contentDetail: ContentDetailData?
    var searchBarText = ""
    var langaugeModal: BABrowseByModal?
    var genreModal: BABrowseByModal?
    var browseByDetailVM: BABrowseByModuleDetailVM?
    var intent: String?
    var isCallAlreadyMade = false
    var isSelectionMade = false
    var isFromTabChange: Bool = true
    let dispatchGroup = DispatchGroup()
    var isTrendingAPICompleted: Bool = false
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "Search")
    }()
    //var didSelectTime = Date()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(configureSearchTab), name: NSNotification.Name(rawValue: "ConfigureSearchTab"), object: nil)
        addSearchBar()
        recheckVM()
        setUpCollectionView()
        setNoDataLabelText(false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchStart.rawValue)
        BAKeychainManager().isOnEditProfileScreen = false
        self.navigationController?.view.setNeedsLayout()
        self.navigationController?.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func configureSearchTab() {
        addSearchBar()
        searchBarText = ""
    }
    
    func recheckVM() {
        preferredLanguageViewModel = BASearchPreferredViewModel(repo: BAPreferredSearchRepo(), apiEndPoint: "\(BAKeychainManager().baId)")
        preferredGenreViewModel = BASearchPreferredViewModel(repo: BAPreferredSearchRepo(), apiEndPoint: "genre/UC_GET_GENRE_PROFILE_1")
        browseByViewModal = BABrowseByViewModal(repo: BABrowseByRepo(), endPoint: "language", intent: "LANGUAGE")
        callSearchScreenLandingData()
    }

    func callSearchScreenLandingData() {
        isTrendingAPICompleted = false
        
        dispatchGroup.enter()
        getPreferredLanguageData {
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        getPreferredGenreData {
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        getBrowseByList(intent: "LANGUAGE") {
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            self.hideActivityIndicator()
            self.fetchPreSearchData()
        }
    }

    func getPreferredLanguageData(completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = preferredLanguageViewModel {
//                showActivityIndicator(isUserInteractionEnabled: true)
                dataModel.getPreferredLanguageData { (flagValue, message, isApiError) in }
                completion()
            }
        } else {
            completion()
            noInternet()
        }
    }
    
    func getPreferredGenreData(completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = preferredGenreViewModel {
                dataModel.getPreferredGenreData { (flagValue, message, isApiError) in }
                completion()
            }
        } else {
            completion()
            noInternet()
        }
    }
    
    
    func getBrowseByList(intent: String? ,_ completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
            report_memory()
            if browseByViewModal != nil {
                showActivityIndicator(isUserInteractionEnabled: true)
                browseByViewModal?.getBrowseByData(completion: {(flagValue, message, isApiError) in
                    if flagValue {
                        if intent == "LANGUAGE" {
                            self.langaugeModal = self.browseByViewModal?.browseByModal
                            self.browseByViewModal = BABrowseByViewModal(repo: BABrowseByRepo(), endPoint: "genre", intent: "GENRE")
                            self.getBrowseByList(intent: "GENRE", {
                                completion()
                            })
                        } else {
                            self.genreModal = self.browseByViewModal?.browseByModal
                            completion()
                        }
                    } else if isApiError {
                        if message == "The Internet connection appears to be offline." {
                            // No action to be performed
                        } else {
                            self.apiError(message, title: kSomethingWentWrong)
                        }
                        completion()
                    } else {
                        self.apiError(message, title: "")
                        completion()
                    }
                })
            }
        } else {
            noInternet()
            completion()
        }
    }
    
    func report_memory() {
        var taskInfo = mach_task_basic_info()
        var count = mach_msg_type_number_t(MemoryLayout<mach_task_basic_info>.size)/4
        let kerr: kern_return_t = withUnsafeMutablePointer(to: &taskInfo) {
            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
                task_info(mach_task_self_, task_flavor_t(MACH_TASK_BASIC_INFO), $0, &count)
            }
        }

        if kerr == KERN_SUCCESS {
            print("Memory used in bytes: \(taskInfo.resident_size)")
        }
        else {
            print("Error with task_info(): " +
                (String(cString: mach_error_string(kerr), encoding: String.Encoding.ascii) ?? "unknown error"))
        }
    }
    
    func fetchPreSearchData() {
        preSearchlandingVM = BASearchViewModal(repo: BASearchContentRepo(), endPoint: "search/results")
        let preferredLanguage: [String]? = (preferredLanguageViewModel?.preferredLanguageModel?.data?.languageList)?.compactMap {$0.name}
        preSearchlandingVM?.preferredLanguage = preferredLanguage ?? []
        preSearchlandingVM?.preferredGenre = preferredGenreViewModel?.preferredGenreModel?.data?.contentList ?? []
        getPreSearchData()
    }

    func getPreSearchData() {
        if let dataModel = preSearchlandingVM {
            showActivityIndicator(isUserInteractionEnabled: true)
            dataModel.getPreSearchContentData { (flagValue, message, isApiError) in
                self.isCallAlreadyMade = false
                self.hideActivityIndicator()
                if flagValue {
					self.collectionView.reloadData()
                } else if isApiError {
                    if message == "The Internet connection appears to be offline." {
                        // No action to be performed
                    } else {
                        self.apiError(message, title: kSomethingWentWrong)
                    }
                } else {
                    self.apiError(message, title: "")
                }
                self.isTrendingAPICompleted = true
            }
        } else {
            self.hideActivityIndicator()
        }
    }

    func setNoDataLabelText(_ boolVal: Bool) {
        noDataLabelHeightConstraint.constant = boolVal ? 80 : 0
    }

    private func setUpCollectionView() {
        collectionView.backgroundColor = .BAdarkBlueBackground
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
        collectionView.register(UINib(nibName: "SearchFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SearchFilterCollectionViewCell")
        collectionView.register(UINib(nibName: "BAHomeRailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BAHomeRailCollectionViewCell")
        collectionView.register(UINib(nibName: kBAAppsHeaderCollectionViewCell, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kBAAppsHeaderCollectionViewCell)
        collectionView.register(UINib(nibName: "BABrowseByHomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BABrowseByHomeCollectionViewCell")
    }

    
    // MARK: - Functions
    func createSearchBar(_ backType: Bool, _ searchText: String) {
        searchBarText = searchText
        backType ? addSearchBar2() : addSearchBar()
        if let searchView = self.navigationItem.titleView as? UISearchBar {
            searchBar = searchView
        }
        if backType {
            searchBar.setImage(UIImage(), for: .search, state: [])
            searchBar.text = searchText
        }
    }
    
    private func addSearchBar() {
        self.configureNavigationBar(with: .searchBar, nil, nil, nil)
        if let searchView = self.navigationItem.titleView as? UISearchBar {
            searchBar = searchView
        }
        searchBar.setImage(UIImage(named: "voiceSearchButton"), for: .bookmark, state: [])
        searchBar.showsBookmarkButton = true
    }

    private func addSearchBar2() {
        self.configureNavigationBar(with: .backAndSearchBar, #selector(backButtonAction), nil, self)
    }

    @objc func backButtonAction() {
        setNoDataLabelText(false)
        self.createSearchBar(false, "")
    }

    func moveToDetailScreen(vodId: Int?, contentType: String?) {
        fetchContentDetail(vodId: vodId, contentType: contentType)
    }

    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId)
    }

    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                   // self.hideActivityIndicator()
                    if flagValue {
                        let genreResult = dataModel.contentDetail?.meta?.genre?.joined(separator: ",")
                         MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.vodRail.rawValue: "Trending", MixpanelConstants.ParamName.contentTitle.rawValue: dataModel.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentType.rawValue: dataModel.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: dataModel.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.search.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.origin.rawValue: MixpanelConstants.ParamValue.search.rawValue])
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId)
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
               // hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = "Search"
                        contentDetailVC.configSource = "Search"
                        contentDetailVC.railName = "Trending"
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = "Search"
                        contentDetailVC.configSource = "Search"
                        contentDetailVC.railName = "Trending"
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = "Search"
                        contentDetailVC.configSource = "Search"
                        contentDetailVC.railName = "Trending"
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                       // self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

// MARK: - Extension
extension SearchLandingVC: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        let searchVC: SearchPageVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.search.fileName, identifierVC: ViewControllers.searchVC.rawValue, type: SearchPageVC.self)
        searchVC.searchText = searchBarText
        searchVC.dataPassClosure = {(flagValue, searchText) in
            print("Recent Searches 1")
            self.setNoDataLabelText(flagValue)
            self.createSearchBar(flagValue, searchText)
        }
        //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchStart.rawValue)
        self.navigationController?.pushViewController(searchVC, animated: true)
        return false
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if BAReachAbility.isConnectedToNetwork() {
            let speechRecognitionVC: SpeechRecognitionVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.search.fileName, identifierVC: "SpeechRecognitionVC", type: SpeechRecognitionVC.self)
            speechRecognitionVC.callBack = { parsedText in
                if parsedText.length > 0 {
                    let searchVC: SearchPageVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.search.fileName, identifierVC: ViewControllers.searchVC.rawValue, type: SearchPageVC.self)
                    searchVC.searchText = parsedText
                    searchVC.isSpeechSearch = true
                    searchVC.dataPassClosure = {(flagValue, searchText) in
                        self.setNoDataLabelText(flagValue)
                        self.createSearchBar(flagValue, searchText)
                    }
                    //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchStart.rawValue)
                    self.navigationController?.pushViewController(searchVC, animated: true)
                }
            }
            navigationController?.present(speechRecognitionVC, animated: true)
        } else {
            noInternet()
        }
    }
}

//
//  ExtentionSearchVC+CollectionView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 15/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension SearchLandingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, /*CascadableScroll,*/ LoadMoreDelegate, SearchFilterSelectedProtocol {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
		var count = 0
		if !(langaugeModal?.data?.contentList?.isEmpty ?? true) {
			count += 1
		}
		if !(genreModal?.data?.contentList?.isEmpty ?? true) {
			count += 1
		}
		if !(preSearchlandingVM?.preSearchData?.data?.contentList?.isEmpty ?? true) {
			count += 1
		}
		return count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if !(langaugeModal?.data?.contentList?.isEmpty ?? true) && section == 0 {
			return 1
		} else if !(genreModal?.data?.contentList?.isEmpty ?? true) && (section == 0 || section == 1) {
			return 1
		} else {
            if self.preSearchlandingVM?.nextPageAvailable ?? false {
              return (preSearchlandingVM?.preSearchData?.data?.contentList?.count ?? 0) + 1
            } else {
                return (preSearchlandingVM?.preSearchData?.data?.contentList?.count ?? 0)
            }
		}
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
		if !(langaugeModal?.data?.contentList?.isEmpty ?? true) && indexPath.section == 0 {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchFilterCollectionViewCell", for: indexPath) as? SearchFilterCollectionViewCell
			cell?.configureCell(with: .language, content: langaugeModal)
			cell?.delegate = self
			return cell!
        } else if !(genreModal?.data?.contentList?.isEmpty ?? true) && (indexPath.section == 0 || indexPath.section == 1) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchFilterCollectionViewCell", for: indexPath) as? SearchFilterCollectionViewCell
            cell?.configureCell(with: .genre, content: genreModal)
            cell?.delegate = self
            return cell!
        } else {
			guard let data = preSearchlandingVM?.preSearchData else {
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
				return cell
			}
            if preSearchlandingVM?.preSearchData?.data?.continuePaging ?? false && indexPath.row == (data.data?.contentList?.count ?? 0) {
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kLoadMoreCollectionViewCell, for: indexPath) as! LoadMoreCollectionViewCell
				cell.delegate = self
				return cell
			} else {
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as? BAHomeRailCollectionViewCell
				cell?.configureCell(preSearchlandingVM?.preSearchData?.data?.contentList?[indexPath.row], preSearchlandingVM?.preSearchData?.data?.layoutType ?? "")
				return cell!
			}
		}
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let requiredNumOfColumns: CGFloat = 2
        let deviceWidth: CGFloat = UIScreen.main.bounds.width
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
        let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing

        let leftRightSpace = sectionInsetLeft + sectionInsetRight
        
        let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns
        
        
        if !(langaugeModal?.data?.contentList?.isEmpty ?? true) && indexPath.section == 0 {
            return CGSize(width: collectionView.frame.width - leftRightSpace, height: 95)
        } else if !(genreModal?.data?.contentList?.isEmpty ?? true) && (indexPath.section == 0 || indexPath.section == 1) {
            return CGSize(width: collectionView.frame.width - leftRightSpace, height: 95)
        } else {
            if preSearchlandingVM?.preSearchData?.data?.continuePaging ?? false && indexPath.row == (preSearchlandingVM?.preSearchData?.data?.contentList?.count ?? 0) {
                let padding: CGFloat =  28
                let collectionViewSize = collectionView.frame.size.width - padding
                return CGSize(width: collectionViewSize, height: 70)
            } else {
//                let cellWidth = ((ScreenSize.screenWidth - 42) / 2)
                return CGSize(width: cellWidth, height: cellWidth)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.isUserInteractionEnabled = false
            if indexPath.section == (collectionView.numberOfSections - 1) {
                if preSearchlandingVM?.preSearchData?.data?.contentList?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                    self.startPrimeNavigationJourney(preSearchlandingVM?.preSearchData?.data?.contentList?[indexPath.row] ?? BAContentListModel())
                } else {
                moveToDetailScreen(vodId: preSearchlandingVM?.preSearchData?.data?.contentList?[indexPath.row].id, contentType: preSearchlandingVM?.preSearchData?.data?.contentList?[indexPath.row].contentType)
                }
            }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.collectionView.isUserInteractionEnabled = true
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kBAAppsHeaderCollectionViewCell, for: indexPath) as! BAAppsHeaderCollectionViewCell
			if !(langaugeModal?.data?.contentList?.isEmpty ?? true) && indexPath.section == 0 {
                reusableView.configureHeader(.withTitle(langaugeModal?.data?.title ?? "Browse By Language"))
			} else if !(genreModal?.data?.contentList?.isEmpty ?? true) && (indexPath.section == 0 || indexPath.section == 1) {
                reusableView.configureHeader(.withTitle(genreModal?.data?.title ?? "Browse By Genre"))
			} else {
				reusableView.configureHeader(.trending)
			}
            return reusableView
        default: fatalError("Unexpected element kind")
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if !(langaugeModal?.data?.contentList?.isEmpty ?? true) && section == 0 {
            return 0
        } else if !(genreModal?.data?.contentList?.isEmpty ?? true) && (section == 0 || section == 1) {
            return 0
        } else {
            return 14
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if !(langaugeModal?.data?.contentList?.isEmpty ?? true) && section == 0 {
            return 0
        } else if !(genreModal?.data?.contentList?.isEmpty ?? true) && (section == 0 || section == 1) {
            return 0
        } else {
            return 10
        }
    }
    
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }
    
    func loadMoreTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if isCallAlreadyMade == false {
                isCallAlreadyMade = true
                if self.preSearchlandingVM?.nextPageAvailable ?? false {
                    self.getPreSearchData()
                }
            }
        } else {
            noInternet()
        }
    }
    
    func navigateToBrowseByDetail(content: BrowseByDetailData?, intent: String?, genre: String?, language: String?) {
        let searchListingVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.search.fileName, identifierVC: ViewControllers.searchListingVC.rawValue, type: SearchListingVC.self)
        searchListingVC.parentNavigation = self.navigationController
        if intent == "GENRE" {
            searchListingVC.selectedFilter = genre
            searchListingVC.railTitle = genre ?? ""
            searchListingVC.pageSource = MixpanelConstants.ParamValue.browseByGenre.rawValue
        } else {
            searchListingVC.selectedFilter = language
            searchListingVC.railTitle = language ?? ""
            searchListingVC.pageSource = MixpanelConstants.ParamValue.browseByLanguage.rawValue
        }
        searchListingVC.intent = intent
        searchListingVC.configSource = "Search"
        searchListingVC.browseByDetailVM = self.browseByDetailVM
        searchListingVC.contentResults = content?.contentList
        self.navigationController?.pushViewController(searchListingVC, animated: true)
    }
    
    func moveToBrowseByDetail(intent: String?, genre: String?, language: String?) {
        fetchBrowseByData(intent: intent, genre: genre, language: language)
    }
        
    // MARK: Get Browse By Detail
    func fetchBrowseByData(intent: String?, genre: String?, language: String?) {
        browseByDetailVM = BABrowseByModuleDetailVM(repo: BABrowseByModuleDetailRepo(), endPoint: "results", intent: intent ?? "", genre: genre ?? "", language: language ?? "", pageName: "")
        getBrowseByDetails(intent: intent, genre: genre, language: language)
    }
    
    // MARK: Browse By Detail API Calling
    func getBrowseByDetails(intent: String?, genre: String?, language: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if browseByDetailVM != nil {
                browseByDetailVM?.getBrowseByDetail(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.navigateToBrowseByDetail(content: self.browseByDetailVM?.browseByData?.data, intent: intent, genre: genre, language: language)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
			noInternet()
        }
    }
}

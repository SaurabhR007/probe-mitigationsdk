//
//  SearchListingVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 26/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class SearchListingVC: BaseViewController, LoadMoreDelegate {
    
    // MARK: - Outlet
    @IBOutlet weak var noContentLabel: UILabel!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var dataCollectionView: UICollectionView!
    @IBOutlet weak var filterButton: CustomButton!
    @IBOutlet weak var filterCollectionViewHeightContraint: NSLayoutConstraint!
    
    // MARK: - Variables
    var parentNavigation: UINavigationController?
    var contentResults: [BAContentListModel]?
    var filterType: FilterType?
    var filterData: BABrowseByModal?
    var sectionType: String?
    var selectedFilter: String?
    var intent: String?
    var browseByViewModal: BABrowseByViewModal?
    var browseByDetailVM: BABrowseByModuleDetailVM?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var selectedIndex = -1
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var isSelectionMade = false
    var pageSource = ""
    var configSource = ""
    var railTitle = ""
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "Search")
    }()
    var isCallAlreadyMade = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        registerCell()
        if intent == "GENRE" {
            fetchBrowseByLanguage()
        } else {
            fetchBrwoseByGenre()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUI()
        showHideFilterView()
        if contentResults?.isEmpty ?? false {
            noContentLabel.isHidden = false
            self.filterButton.isHidden = false
            dataCollectionView.isHidden = true
        } else {
            noContentLabel.isHidden = true
            dataCollectionView.isHidden = false
            self.filterButton.isHidden = false
        }
    }
    
    func showHideFilterView() {
        if !filterButton.isSelected {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.filterCollectionView.alpha = 0
                self?.filterCollectionViewHeightContraint.constant = 0
                self?.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.filterCollectionView.alpha = 1
                self?.filterCollectionViewHeightContraint.constant = 35
                self?.view.layoutIfNeeded()
            })
        }
    }
    
    // MARK: - Function
    func configureUI() {
        self.view.backgroundColor = .BAdarkBlueBackground
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backButtonAction), nil, self, selectedFilter ?? "")
        filterCollectionView.backgroundColor = .BAdarkBlueBackground
        dataCollectionView.backgroundColor = .BAdarkBlueBackground
        filterButton.tintColor = .BABlueColor
        //if contentResults?.isEmpty ?? false {
        filterCollectionViewHeightContraint.constant = 0
//        } else {
//            filterCollectionViewHeightContraint.constant = 35
//        }
        extendedLayoutIncludesOpaqueBars = true
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerCell() {
        filterCollectionView.delegate = self
        filterCollectionView.dataSource = self
        dataCollectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
        filterCollectionView.register(UINib(nibName: "ResultFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ResultFilterCollectionViewCell")
        dataCollectionView.delegate = self
        dataCollectionView.dataSource = self
        dataCollectionView.register(UINib(nibName: "BAHomeRailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BAHomeRailCollectionViewCell")
    }
    
    func fetchBrowseByLanguage() {
        browseByViewModal = BABrowseByViewModal(repo: BABrowseByRepo(), endPoint: "language", intent: "LANGUAGE")
        getBrowseByList(intent: "LANGUAGE")
    }
    
    func fetchBrwoseByGenre() {
        browseByViewModal = BABrowseByViewModal(repo: BABrowseByRepo(), endPoint: "genre", intent: "GENRE")
        getBrowseByList(intent: "GENRE")
    }
    
    func getBrowseByList(intent: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            if browseByViewModal != nil {
                self.showActivityIndicator(isUserInteractionEnabled: false)
                browseByViewModal?.getBrowseByData(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.filterData = self.browseByViewModal?.browseByModal
                        self.filterCollectionView.reloadData()
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
			noInternet()
        }
    }
    
    @IBAction func showHideFilters() {
        filterButton.isSelected = !filterButton.isSelected
        if !filterButton.isSelected {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.filterCollectionView.alpha = 0
                self?.filterCollectionViewHeightContraint.constant = 0
                self?.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.filterCollectionView.alpha = 1
                self?.filterCollectionViewHeightContraint.constant = 35
                self?.view.layoutIfNeeded()
            })
        }
    }
    
    // MARK: Get Browse By Detail
    func fetchBrowseByData(intent: String?, genre: String?, language: String?) {
        browseByDetailVM = BABrowseByModuleDetailVM(repo: BABrowseByModuleDetailRepo(), endPoint: "results", intent: intent ?? "", genre: genre, language: language, pageName: "")
        self.contentResults = []
        self.dataCollectionView.reloadData {
            self.getBrowseByDetails(intent: intent, genre: genre, language: language)
        }
    }
    
    // MARK: Browse By Detail API Calling
    func getBrowseByDetails(intent: String?, genre: String?, language: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: true)
            if browseByDetailVM != nil {
                browseByDetailVM?.getBrowseByDetail(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    self.isCallAlreadyMade = false
                    if flagValue {
                        if self.browseByDetailVM?.browseByData?.data?.contentList?.isEmpty ?? true {
                            if self.contentResults?.isEmpty ?? false {
                                self.noContentLabel.isHidden = false
                                self.filterButton.isHidden = false
                                self.dataCollectionView.isHidden = true
                            } else {
                                self.noContentLabel.isHidden = true
                                self.dataCollectionView.isHidden = false
                            }
                        } else {
                            if let model = self.browseByDetailVM?.browseByData?.data?.contentList, let _ = self.contentResults {
                               self.contentResults = model
                            }
                            if self.contentResults?.isEmpty ?? false {
                                self.noContentLabel.isHidden = false
                                self.filterButton.isHidden = false
                                
                                self.dataCollectionView.isHidden = true
                            } else {
                                self.noContentLabel.isHidden = true
                                self.dataCollectionView.isHidden = false
                                self.dataCollectionView.reloadData()
                            }
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
			noInternet()
        }
    }
    
    func moveToDetailScreenFromBrowseBy(vodId: Int?, contentType: String?) {
        fetchContentDetail(vodId: vodId, contentType: contentType)
    }

    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId)
    }

    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                   // self.hideActivityIndicator()
                    if flagValue {
                        self.viewContentDetailEvent(contentData: dataModel.contentDetail, pageSource: self.pageSource, configSource: self.configSource, railName: self.railTitle)
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId, pageSource: self.railTitle, configSource: self.configSource, railName: "")
//                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
//                        contentDetailVC.parentNavigation = self.navigationController
//                        contentDetailVC.contentDetail = dataModel.contentDetail
//                        contentDetailVC.id = vodId
//                        self.navigationController?.pushViewController(contentDetailVC, animated: true)
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
			noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?, pageSource: String?, configSource: String?, railName: String?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int, pageSource: String?, configSource: String?, railName: String?) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail, pageSource: pageSource, configSource: configSource, railName: railName)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?, pageSource: String?, configSource: String?, railName: String?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
               // hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        //contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = pageSource ?? ""
                        contentDetailVC.configSource = configSource ?? ""
                        contentDetailVC.railName = railName ?? ""
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                       // }
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                         contentDetailVC.parentNavigation = self.navigationController
                         contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = pageSource ?? ""
                        contentDetailVC.configSource = configSource ?? ""
                        contentDetailVC.railName = railName ?? ""
                         contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                         contentDetailVC.id = vodId
                        self.hideActivityIndicator()
                         //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                             self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        // }
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                         contentDetailVC.parentNavigation = self.navigationController
                         contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = pageSource ?? ""
                        contentDetailVC.configSource = configSource ?? ""
                        contentDetailVC.railName = railName ?? ""
                         contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                         contentDetailVC.id = vodId
                        self.hideActivityIndicator()
                         //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                             self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        // }
                        //self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func viewContentDetailEvent(contentData: ContentDetailData?, pageSource: String, configSource: String, railName: String) {
        var title = ""
        if contentData?.meta?.contentType == ContentType.brand.rawValue {
            title = contentData?.meta?.brandTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.series.rawValue {
            title = contentData?.meta?.seriesTitle ?? ""
        } else if contentData?.meta?.contentType == ContentType.tvShows.rawValue {
            title = contentData?.meta?.vodTitle ?? ""
        } else {
            title = contentData?.meta?.vodTitle ?? ""
        }
        let genreResult = contentData?.meta?.genre?.joined(separator: ",")
        let languageResult = contentData?.meta?.audio?.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: contentData?.meta?.contentType ?? MixpanelConstants.ParamValue.vod.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.partnerName.rawValue: contentData?.meta?.provider ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
    }
}

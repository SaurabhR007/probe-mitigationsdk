//
//  ExtentionSearchListingVC+CollectionView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 27/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension SearchListingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == filterCollectionView {
            return filterData?.data?.contentList?.count ?? 0
        } else if collectionView == dataCollectionView {
            if self.browseByDetailVM?.nextPageAvailable ?? false {
                return (contentResults?.count ?? 0) + 1
            } else {
                return contentResults?.count ?? 0
            }
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        if collectionView == filterCollectionView {
            let cell = filterCollectionView.dequeueReusableCell(withReuseIdentifier: "ResultFilterCollectionViewCell", for: indexPath) as? ResultFilterCollectionViewCell
            if selectedIndex == indexPath.row {
                cell?.configureCell(filterData?.data?.contentList?[indexPath.row].title ?? "", selectedIndex)
                let rec = (cell?.frame)!
                scrollToNextCell(rec)
            } else {
                cell?.configureCell(filterData?.data?.contentList?[indexPath.row].title ?? "", -1)
            }
            return cell!
        } else {
            if self.browseByDetailVM?.nextPageAvailable ?? false && indexPath.row == (contentResults?.count ?? 0) {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kLoadMoreCollectionViewCell, for: indexPath) as! LoadMoreCollectionViewCell
                cell.delegate = self
                return cell
            } else {
                let cell = dataCollectionView.dequeueReusableCell(withReuseIdentifier: "BAHomeRailCollectionViewCell", for: indexPath) as? BAHomeRailCollectionViewCell
                // to avoid crash added this check
                if (contentResults?.count ?? 0) > indexPath.row {
                      cell?.configureCell(contentResults?[indexPath.row], "LANDSCAPE")
                }
                return cell!
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == filterCollectionView {
            let cell =  collectionView.cellForItem(at: indexPath) as? ResultFilterCollectionViewCell
            let labelWidth = cell?.filterLabel.bounds.width ?? 40
            return CGSize(width: labelWidth + 70, height: 35)
        } else {
            if self.browseByDetailVM?.nextPageAvailable ?? false && indexPath.row == (contentResults?.count ?? 0){
                let padding: CGFloat =  28
                let collectionViewSize = collectionView.frame.size.width - padding
                return CGSize(width: collectionViewSize, height: 70)
            } else {
                let cellWidth = ((ScreenSize.screenWidth - 42) / 2)
                return CGSize(width: cellWidth, height: cellWidth)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           self.dataCollectionView.isUserInteractionEnabled = false
            if collectionView == filterCollectionView {
                if selectedIndex != indexPath.row {
                    selectedIndex = indexPath.row
                    filterCollectionView.reloadData()
                    if intent == "GENRE" {
                        fetchBrowseByData(intent: "GENRE_LANGUAGE", genre: selectedFilter, language: filterData?.data?.contentList?[indexPath.row].title)
                    } else {
                        fetchBrowseByData(intent: "GENRE_LANGUAGE", genre: filterData?.data?.contentList?[indexPath.row].title, language: selectedFilter)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        self.filterCollectionView.isUserInteractionEnabled = true
                        self.dataCollectionView.isUserInteractionEnabled = true
                    }
                } else if selectedIndex == indexPath.row {
                    selectedIndex = -1
                    filterCollectionView.reloadData()
                    if intent == "GENRE" {
                        fetchBrowseByData(intent: "GENRE_LANGUAGE", genre: selectedFilter, language: nil)
                    } else {
                        fetchBrowseByData(intent: "GENRE_LANGUAGE", genre: nil, language: selectedFilter)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        self.filterCollectionView.isUserInteractionEnabled = true
                        self.dataCollectionView.isUserInteractionEnabled = true
                    }
                }
            } else {
                if contentResults?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                    self.startPrimeNavigationJourney(contentResults?[indexPath.row] ?? BAContentListModel())
                } else {
                moveToDetailScreenFromBrowseBy(vodId: contentResults?[indexPath.row].id, contentType: contentResults?[indexPath.row].contentType)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                    self.dataCollectionView.isUserInteractionEnabled = true
                }
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func loadMoreTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if isCallAlreadyMade == false {
                isCallAlreadyMade = true
                if self.browseByDetailVM?.nextPageAvailable ?? false {
                    if intent == "GENRE" {
                        self.getBrowseByDetails(intent: intent, genre: selectedFilter, language: "")
                    } else {
                        self.getBrowseByDetails(intent: intent, genre: "", language: selectedFilter)
                    }
                } else {
                    self.dataCollectionView.reloadData()
                }
            } else {
                
            }
        } else {
            noInternet()
        }
    }
    
    func scrollToNextCell(_ cellFrame: CGRect) {
        //get cell size
        let contentOffset = filterCollectionView.contentOffset
        filterCollectionView.scrollRectToVisible(CGRect(cellFrame.origin.x, contentOffset.y, cellFrame.width, cellFrame.height), animated: true)
    }
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }
}

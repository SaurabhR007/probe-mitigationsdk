//
//  SearchPageVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 20/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class SearchPageVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var resultCountLabel: CustomLabel!
    @IBOutlet weak var showFilterButton: CustomButton!
    @IBOutlet weak var languageTitleLabel: CustomLabel!
    @IBOutlet weak var genreTitleLabel: CustomLabel!
    @IBOutlet weak var languageCollectionView: UICollectionView!
    @IBOutlet weak var genreCollectionView: UICollectionView!
    @IBOutlet weak var searchDataCollectionView: UICollectionView!
    @IBOutlet weak var filterTitleView: UIView!
    @IBOutlet weak var languageStackView: UIStackView!
    @IBOutlet weak var genreStackView: UIStackView!
    lazy var primeIntegrationObj: PrimeAppIntegration = {
      return  PrimeAppIntegration(continueWatchingVM: BAWatchingViewModal(repo: BAWatchingRepo()), primeServiceVM: PrimeIntegrationViewModal(repo: PrimeResponseRepo()), pageSource: "Search")
    }()
    
//    // MARK: - Cascadable
//    typealias GridViewType = UICollectionView
//    typealias ViewCellType = UICollectionViewCell
//    var cascadeQueue: OperationQueue = OperationQueue()
//    var stopCollectionViewAnimationOnScroll: Bool = true
//    var needAnimationFlag: Bool = true
    
    var searchBar = UISearchBar()
    var dataPassClosure: ((Bool, String) -> Void)?
    var filterDataModel: FilterSearchModel?
    var searchViewModel: BASearchViewModal?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var searchText = ""
    var searchedString = ""
    var isCallAlreadyMade = false
    var isSearchButtonClicked: Bool = false
    var recentSearchArray = [String]()
    var searchTimer: Timer?
    var isSpeechSearch = false
    var isFromContentScreen = false
    //var didSelectTime = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationBar(with: .backAndSearchBar, #selector(backButtonAction), nil, self)
        registerCell()
        recheckVM()
        configureSearchScreenView()
        if let searchView = self.navigationItem.titleView as? UISearchBar {
            searchBar = searchView
        }
        searchBar.setImage(UIImage(), for: .search, state: [])
        searchBar.setImage(UIImage(named: "voiceSearchButton"), for: .bookmark, state: [])
        searchBar.showsBookmarkButton = true
        searchBar.text = searchText
        if searchText != "" && isSpeechSearch {
            voiceMixPanelEvent(searchText, "Result Page")
            searchBar.resignFirstResponder()
            callSearchAPIOnKeyStroke(searchText)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if searchViewModel?.dataModel != nil {
            print("Data Model is not nil")
            searchBar.resignFirstResponder()
        } else {
            print("Data Model is nil")
            searchBar.becomeFirstResponder()
        }
        recentSearchArray = BAKeychainManager().localSearchArray
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func recheckVM() {
        if searchViewModel == nil {
            searchViewModel = BASearchViewModal(repo: BASearchContentRepo(), endPoint: "")
            searchViewModel?.filterModel = FilterType.filterArrays
            searchViewModel?.filterModel[0].user = FilterSearchModel()
        }
    }
    
    private func registerCollectionCells() {
        searchDataCollectionView.register(UINib(nibName: kBAHomeRailCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeRailCollectionViewCell)
        searchDataCollectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
    }
    
    func configureSearchScreenView() {
        searchDataCollectionView.backgroundColor = .BAdarkBlueBackground
        searchDataCollectionView.delegate = self
        searchDataCollectionView.dataSource = self
        showFilterOptions(show: false)
        registerCollectionCells()
    }
    
    private func registerCell() {
        UtilityFunction.shared.registerCell(tableView, "RecentSearchHeaderTableViewCell")
        UtilityFunction.shared.registerCell(tableView, "SearchSuggestionTableViewCell")
        languageCollectionView.register(UINib(nibName: "ResultFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ResultFilterCollectionViewCell")
        genreCollectionView.register(UINib(nibName: "ResultFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ResultFilterCollectionViewCell")
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 40
    }
    
    @objc func backButtonAction() {
        self.dataPassClosure?(false, "")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showHideFilters() {
        languageStackView.isHiddenAnimated(value: showFilterButton.isSelected, duration: 0.33)
        genreStackView.isHiddenAnimated(value: showFilterButton.isSelected, duration: 0.33)
        showFilterButton.isSelected = !showFilterButton.isSelected
        
    }
    
    func showFilterOptions(show: Bool) {
        filterTitleView.isHidden = !show
        showFilterButton.isSelected = false
        showFilterButton.isHidden = true
        languageStackView.isHidden = true
        genreStackView.isHidden = true
        searchDataCollectionView.isHidden = !show
    }
    
    func searchContentData(_ searchString: String) {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = searchViewModel {
                searchViewModel?.searchString = searchString
                viewModel.searchScreenData { (flagValue, message, isApiError) in
                    //self.updateLocalSearchData(searchString)
                    self.hideActivityIndicator()
                    self.isCallAlreadyMade = false
                    if flagValue {
                        self.checkSearchData()
                    } else {
                        self.showFilterOptions(show: false)
                        if self.searchViewModel?.searchString.isEmpty ?? true {
                            print("************ Search String is Empty *****************")
                            // Do nothing
                        } else if isApiError {
                            //self.apiError(message, title: kSomethingWentWrong)
                        } else {
                            self.apiError(message, title: "")
                        }
                    }
                }
            }
        } else {
			noInternet()
        }
    }
    
    @objc func searchFilterData(_ searchString: String, _ isRecentSearch: Bool = false) {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = searchViewModel {
                if isRecentSearch {
                    showActivityIndicator(isUserInteractionEnabled: true)
                }
                viewModel.searchScreenFilterData {[searchString] (flagValue, _, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.genreCollectionView.reloadData{
                            self.languageCollectionView.reloadData{
                                //self.updateLocalSearchData(searchString)
                                self.searchContentData(searchString)
                            }
                        }
                    } else {
                        self.backToSearchLandingScreen()
                    }
                }
            }
        } else {
			noInternet()
        }
    }
    
    func checkSearchData() {
        if let viewModel = searchViewModel {
            if (viewModel.dataModel?.data?.contentResults?.count ?? 0) > 0 {
                self.resultCountLabel.text = "\(viewModel.dataModel?.data?.totalSearchCount ?? 0) Results Found"
                self.showFilterOptions(show: true)
                self.searchDataCollectionView.isHidden = false
                self.mixPanelEvents(true, viewModel)
                self.searchDataCollectionView.reloadData{
                    self.hideActivityIndicator()
                    self.changeSearchBarColor()
                }
            } else {
                backToSearchLandingScreen()
            }
        }
    }
    
    func backToSearchLandingScreen() {
        self.mixPanelEvents(false, nil)
        self.hideActivityIndicator()
        self.dataPassClosure?(true, searchBar.text ?? "")
        self.navigationController?.popViewController(animated: true)
    }
    
    func mixPanelEvents(_ resultFound: Bool, _ viewModel: BASearchViewModal?) {
        let searchType = isSpeechSearch ? MixpanelConstants.ParamValue.voice.rawValue : MixpanelConstants.ParamValue.text.rawValue
        isSpeechSearch = false
        if resultFound {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchResult.rawValue, properties: [MixpanelConstants.ParamName.keyWord.rawValue: searchViewModel?.searchString ?? "", MixpanelConstants.ParamName.searchCount.rawValue: viewModel!.dataModel?.data?.totalSearchCount ?? 0, MixpanelConstants.ParamName.searchType.rawValue: searchType, MixpanelConstants.ParamName.source.rawValue: "All"])
        } else {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.noSearchResult.rawValue, properties: [MixpanelConstants.ParamName.keyWord.rawValue: searchViewModel?.searchString ?? "", MixpanelConstants.ParamName.searchType.rawValue: searchType])
        }
    }
    
    
    func changeSearchBarColor() {
        if #available(iOS 13.0, *) {
            if !searchBar.searchTextField.isEditing {
                searchBar.setImage(UIImage(named: "closeLight"), for: .clear, state: .normal)
                if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
                    textfield.backgroundColor = .BAmidBlue
                    textfield.textColor = .BAwhite
                    textfield.keyboardType = .asciiCapable
                }
            } else {
                searchBar.setImage(UIImage(named: "close"), for: .clear, state: .normal)
                if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
                    textfield.backgroundColor = .BAwhite
                    textfield.textColor = .black
                    textfield.keyboardType = .asciiCapable
                }
            }
        } else {
            if !(searchBar.textField?.isEditing ?? false) {
                searchBar.setImage(UIImage(named: "closeLight"), for: .clear, state: .normal)
                if let textfield = searchBar.value(forKey: "_searchField") as? UITextField {
                    textfield.backgroundColor = .BAmidBlue
                    textfield.textColor = .BAwhite
                    textfield.keyboardType = .asciiCapable
                }
            } else {
                searchBar.setImage(UIImage(named: "close"), for: .clear, state: .normal)
                if let textfield = searchBar.value(forKey: "_searchField") as? UITextField {
                    textfield.backgroundColor = .BAwhite
                    textfield.textColor = .black
                    textfield.keyboardType = .asciiCapable
                }
            }
        }
    }
    
    func moveToDetailScreen(vodId: Int?, contentType: String?) {
        fetchContentDetail(vodId: vodId, contentType: contentType)
        self.updateLocalSearchData(self.searchBar.text!)
    }
    
    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId)
    }
    
    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    //self.hideActivityIndicator()
                    if flagValue {
                        let genreResult = dataModel.contentDetail?.meta?.genre?.joined(separator: ",")
                        let languageResult = dataModel.contentDetail?.meta?.audio?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.vodRail.rawValue: "", MixpanelConstants.ParamName.contentTitle.rawValue: dataModel.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentType.rawValue: dataModel.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: dataModel.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.search.rawValue, MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.origin.rawValue: MixpanelConstants.ParamValue.search.rawValue, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId)
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
               // hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = "Search"
                        contentDetailVC.configSource = "Search"
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "Search"
                        contentDetailVC.configSource = "Search"
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = "Search"
                        contentDetailVC.configSource = "Search"
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        self.hideActivityIndicator()
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                       // self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

//
//  ExtensionSearchPageVC+DelegateMethods.swift
//  BingeAnywhere
//
//  Created by Shivam on 27/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

extension SearchPageVC: RecentSeacrhClearAllDelegate, LoadMoreDelegate {
    func clearAllButtonTapped() {
        UtilityFunction.shared.performTaskInMainQueue {
            BAKeychainManager().localSearchArray.removeAll()
            self.recentSearchArray.removeAll()
            self.tableView.reloadData()
        }
    }

    func loadMoreTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if isCallAlreadyMade == false {
                isCallAlreadyMade = true
                if self.searchViewModel?.requestToken == nil {
                    if self.searchViewModel?.nextPageAvailable ?? false {
                        self.searchContentData(searchViewModel?.searchString ?? "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

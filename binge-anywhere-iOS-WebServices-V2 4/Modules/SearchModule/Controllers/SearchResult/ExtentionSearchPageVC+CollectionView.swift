//
//  ExtentionSearchPageVC+CollectionView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 27/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension SearchPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout/*, CascadableScroll*/ {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == languageCollectionView || collectionView == genreCollectionView {
            guard let data = searchViewModel?.filterModel[0].user else {
                return 0
            }
            if collectionView == languageCollectionView {
                return data.languageFilter.count
            } else if collectionView == genreCollectionView {
                return data.genreFilter.count
            } else {
                return 0
            }
        } else if collectionView == searchDataCollectionView {
            guard let data = searchViewModel?.dataModel else {
                return 0
            }
            guard let count = data.data?.contentResults?.count else {
                return 0
            }
            if searchViewModel?.nextPageAvailable ?? false {
                    return count + 1
            } else {
                return count
            }

        } else {
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == languageCollectionView {
            let cell = languageCollectionView.dequeueReusableCell(withReuseIdentifier: "ResultFilterCollectionViewCell", for: indexPath) as! ResultFilterCollectionViewCell
            if indexPath.row == searchViewModel?.filterModel[0].user?.selectedLanguageValue ?? -1 {
                cell.configureCell(searchViewModel?.filterModel[0].user?.languageFilter[indexPath.row] ?? "", searchViewModel?.filterModel[0].user?.selectedLanguageValue ?? -1)
            } else {
                cell.configureCell(searchViewModel?.filterModel[0].user?.languageFilter[indexPath.row] ?? "", -1)
            }
            return cell
        } else if collectionView == genreCollectionView {
            let cell = genreCollectionView.dequeueReusableCell(withReuseIdentifier: "ResultFilterCollectionViewCell", for: indexPath) as! ResultFilterCollectionViewCell
            if indexPath.row == searchViewModel?.filterModel[0].user?.selectedGenereValue ?? -1 {
                cell.configureCell(searchViewModel?.filterModel[0].user?.genreFilter[indexPath.row] ?? "", searchViewModel?.filterModel[0].user?.selectedGenereValue ?? -1)
            } else {
                cell.configureCell(searchViewModel?.filterModel[0].user?.genreFilter[indexPath.row] ?? "", -1)
            }
            return cell
        } else {
            guard let data = searchViewModel?.dataModel else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
                return cell
            }
            if searchViewModel?.nextPageAvailable ?? false && indexPath.row == data.data?.contentResults?.count {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kLoadMoreCollectionViewCell, for: indexPath) as! LoadMoreCollectionViewCell
                cell.delegate = self
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
                cell.configureCell(data.data?.contentResults?[indexPath.row], data.data?.layoutType ?? "")
                return cell
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = searchViewModel?.filterModel[0].user else {
            return
        }
        print(data)
        if collectionView == genreCollectionView {
            searchViewModel?.filterModel[0].user?.selectedGenereValue = indexPath.row
        } else if collectionView == languageCollectionView {
            searchViewModel?.filterModel[0].user?.selectedLanguageValue = indexPath.row
        } else if collectionView == searchDataCollectionView {
            searchBar.resignFirstResponder()
            self.view.endEditing(true)
            searchDataCollectionView.isUserInteractionEnabled = false
            if searchViewModel?.dataModel?.data?.contentResults?[indexPath.row].provider?.lowercased() == SectionSourceType.prime.rawValue.lowercased() {
                self.startPrimeNavigationJourney(searchViewModel?.dataModel?.data?.contentResults?[indexPath.row] ?? BAContentListModel())
            } else {
                moveToDetailScreen(vodId: searchViewModel?.dataModel?.data?.contentResults?[indexPath.row].id ?? 0, contentType: searchViewModel?.dataModel?.data?.contentResults?[indexPath.row].contentType)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                self.searchDataCollectionView.isUserInteractionEnabled = true
            }
            return
        }
        reloadCollectionCells(collectionView, index: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView != searchDataCollectionView {
            let cell =  collectionView.cellForItem(at: indexPath) as? ResultFilterCollectionViewCell
            var labelWidth: CGFloat = 40
            if let label = cell?.value(forKey: "filterLabel") as? CustomLabel {
                labelWidth = label.bounds.width
            }
            return CGSize(width: labelWidth + 36, height: 35)
        } else {
            guard searchViewModel?.dataModel != nil else {
                return CGSize.zero
            }
            if searchViewModel?.nextPageAvailable ?? false && indexPath.row == searchViewModel?.dataModel?.data?.contentResults?.count {
                let padding: CGFloat =  28
                let collectionViewSize = collectionView.frame.size.width - padding
                return CGSize(width: collectionViewSize, height: 70)
            } else {
                let padding: CGFloat =  42
                let collectionViewSize = collectionView.frame.size.width - padding
                return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
            }
        }
    }

    func reloadCollectionCells(_ collectionView: UICollectionView, index: IndexPath) {
        guard searchViewModel?.filterModel[0].user != nil else {
            return
        }
        collectionView.reloadData()
    }
    
    func startPrimeNavigationJourney(_ content: BAContentListModel) {
        primeIntegrationObj.presentPrimeContentAlert(content)
    }
}

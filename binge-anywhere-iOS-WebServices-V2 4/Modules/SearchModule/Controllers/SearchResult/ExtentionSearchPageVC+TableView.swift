//
//  ExtentionSearchPageVC+Searchbar.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 20/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension SearchPageVC: UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentSearchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuggestionTableViewCell") as? SearchSuggestionTableViewCell
        cell?.selectionStyle = .none
        cell?.setLabelTitle(recentSearchArray[indexPath.row], cellIndex: indexPath.row)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if recentSearchArray.isEmpty {
            return 0.01
        } else {
            return 44*screenScaleFactorForHeight
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentSearchHeaderTableViewCell") as? RecentSearchHeaderTableViewCell
        cell?.delegate = self
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedVal = recentSearchArray[indexPath.row]
        searchBar.text = selectedVal
        callSearchAPIOnKeyStroke(selectedVal, true)
        mixPanelEvents(searchBar.text ?? "", MixpanelConstants.ParamValue.recentSearch.rawValue)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        changeSearchBarColor()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("did end called")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        isSearchButtonClicked = false
        let updatedSearchText = UtilityFunction.shared.trimWhiteSpaces(value: searchText)
        //callSearchAPIOnKeyStroke(updatedSearchText)
        if updatedSearchText.isEmpty {
            //searchBar.becomeFirstResponder()
            searchTimer?.invalidate()
            showFilterOptions(show: false)
            recentSearchArray = BAKeychainManager().localSearchArray
            tableView.reloadData()
            print("********************** Search String is Empty Ret *******************************")
            // DO Nothing
        } else {
            callSearchAPIOnKeyStroke(searchBar.text ?? "")
        }
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if BAReachAbility.isConnectedToNetwork() {
            let speechRecognitionVC: SpeechRecognitionVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.search.fileName, identifierVC: "SpeechRecognitionVC", type: SpeechRecognitionVC.self)
            speechRecognitionVC.callBack = { parsedText in
                if parsedText.length > 0 {
                    self.isSpeechSearch = true
                    searchBar.text = parsedText
                    self.voiceMixPanelEvent(parsedText, "Search Page")
                    self.callSearchAPIOnKeyStroke(parsedText)
                }
            }
            navigationController?.present(speechRecognitionVC, animated: true)
        } else {
            noInternet()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let updatedSearchText = UtilityFunction.shared.trimWhiteSpaces(value: searchBar.text)
        if searchedString == updatedSearchText {
            // Do not make another api call
        } else {
            isSearchButtonClicked = true
            if updatedSearchText.isEmpty {
                apiError("Please enter atleast 1 character to search", title: "WoHo!")
            } else {
                callSearchAPIOnKeyStroke(searchBar.text ?? "", true)
            }
        }
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    func updateLocalSearchData(_ searchString: String) {
        if let viewModel = searchViewModel {
            let updatedSearchText = UtilityFunction.shared.trimWhiteSpaces(value: searchString)
            if updatedSearchText.length == 0 {
                searchTimer?.invalidate()
                viewModel.requestToken = nil
                return
            }
            let arr = BAKeychainManager().localSearchArray
            if !arr.contains(updatedSearchText) {
                if arr.count == 10 {
                    BAKeychainManager().localSearchArray.insert(updatedSearchText, at: 0)
                    BAKeychainManager().localSearchArray.removeLast()
                } else {
                    BAKeychainManager().localSearchArray.insert(updatedSearchText, at: 0)
                }
            } else {
                let index = arr.firstIndex(of: searchString)
                BAKeychainManager().localSearchArray.remove(at: index ?? 0)
                BAKeychainManager().localSearchArray.insert(updatedSearchText, at: 0)
            }
        }
    }
    
    func callSearchAPIOnKeyStroke(_ searchText: String, _ isRecentSearch: Bool = false) {
        if BAReachAbility.isConnectedToNetwork() {
            searchedString = searchText
            if let viewModel = searchViewModel {
                if searchText.count > 25 {
                    //viewModel.requestToken = nil
                    searchBar.text = String(searchText.dropLast(searchText.count - 25))
                    //searchTimer?.invalidate()
                    //return
                } //else {
                    viewModel.requestToken = nil
                    if isRecentSearch {
                        showActivityIndicator(isUserInteractionEnabled: false)
                    }
                    searchTimer?.invalidate()
                    searchTimer = Timer.scheduledTimer(withTimeInterval: isRecentSearch ? 0 : 0.5, repeats: false, block: { (timer) in
                        self.mixPanelEvents(searchText, self.isSpeechSearch ? MixpanelConstants.ParamValue.voice.rawValue : MixpanelConstants.ParamValue.manual.rawValue)
                        viewModel.pageOffset = 0
                        viewModel.pageNumber = 1
                        if self.isSpeechSearch {
                            self.searchBar.resignFirstResponder()
                        }
                        self.searchViewModel?.dataModel?.data?.contentResults = []
                        self.searchViewModel?.filterModel = FilterType.filterArrays
                        self.searchViewModel?.filterModel[0].user = FilterSearchModel()
                        self.resultCountLabel.text = ""
                        self.searchViewModel?.nextPageAvailable = false
                        self.searchDataCollectionView.reloadData {
                            self.searchContentData(searchText)
                            //self.searchFilterData(searchText)
                        }
                    })
                    if let timer = searchTimer {
                        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
                    }
                //}
            }
        } else {
            noInternet()
        }
    }
    
    func mixPanelEvents(_ searchText: String, _ source: String) {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.search.rawValue, properties: [MixpanelConstants.ParamName.keyWord.rawValue: searchText, MixpanelConstants.ParamName.screenName.rawValue: MixpanelConstants.ParamValue.searchPage.rawValue, MixpanelConstants.ParamName.source.rawValue: source])
    }
    
    func voiceMixPanelEvent(_ searchText: String, _ source: String) {
        //let screenName = MixpanelConstants.ParamValue.resultPage.rawValue
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchMic.rawValue, properties: [MixpanelConstants.ParamName.screenName.rawValue: source])
    }
}

//
//  BaseLayer.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 30/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

class BaseLayer: CALayer {

    private let maxBorderWidth = CGFloat(15.0)
    private let minBorderWidth = CGFloat(0.0)
    private let layerBorderOpacity = Float(0.2)
    private var layerBaseRadius = CGFloat.zero
    private let firstAnimationDuration = 10.0
    private let continuousAnimationDuration = 0.2
    var delay = 0.0

    var initialGroupAnimation = CAAnimationGroup()
    var continuousGroupAnimation = CAAnimationGroup()

    var animationSetupQueue = DispatchQueue.global(qos: .default)

    override init(layer: Any) {
        super.init(layer: layer)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init(radius: CGFloat) {
        super.init()

        layerBaseRadius = radius

        bounds = CGRect(x: 0, y: 0, width: layerBaseRadius * 2, height: layerBaseRadius * 2)
        cornerRadius = layerBaseRadius
        position = CGPoint(x: layerBaseRadius, y: layerBaseRadius)
        opacity = layerBorderOpacity
        borderWidth = minBorderWidth
        borderColor = UIColor.BAlightBlueGrey.cgColor

        animationSetupQueue.async {

            self.fireInitialAnimation()
            self.fireContinuousAnimation()

            DispatchQueue.main.async {
                self.add(self.initialGroupAnimation, forKey: nil)
            }

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) {
                self.add(self.continuousGroupAnimation, forKey: nil)
            }

        }
    }

    fileprivate func fireInitialAnimation() {
        initialGroupAnimation.animations = [
            animateBoundsWith(keyPath: KeyPathString.bounds.rawValue, initialRadius: 40.0, delta: 15.0),
            keyFrameAnimationWith(keyPath: KeyPathString.cornerRadius.rawValue, initialValue: 40.0, finalValue: 55.0, divisionsForInterpolation: 4),
            keyFrameAnimationWith(keyPath: KeyPathString.borderWidth.rawValue, initialValue: 0.0, finalValue: 15.0, divisionsForInterpolation: 4)
        ]

        initialGroupAnimation.duration = firstAnimationDuration
        initialGroupAnimation.repeatCount = 1.0
        initialGroupAnimation.autoreverses = false
        initialGroupAnimation.isRemovedOnCompletion = true
    }

    fileprivate func fireContinuousAnimation() {
        continuousGroupAnimation.animations = [
            animateBoundsWith(keyPath: KeyPathString.bounds.rawValue, initialRadius: 55.0, delta: -5.0),
            keyFrameAnimationWith(keyPath: KeyPathString.cornerRadius.rawValue, initialValue: 55.0, finalValue: 50.0, divisionsForInterpolation: 4),
            keyFrameAnimationWith(keyPath: KeyPathString.borderWidth.rawValue, initialValue: 15.0, finalValue: 10.0, divisionsForInterpolation: 4)
        ]
        continuousGroupAnimation.duration = continuousAnimationDuration
        continuousGroupAnimation.repeatCount = Float.infinity
        continuousGroupAnimation.autoreverses = true
    }
}

//
//  SpeechRecognitionVC.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 28/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Speech

@available(iOS 10.0, *)
class SpeechRecognitionVC: BaseViewController {
    // MARK: Properties

    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-IN"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()

    private var audioSetupQueue = DispatchQueue.global(qos: .userInteractive)

    private let introText = "Try Saying"
    private let initialText = "Listening..."
    private let preRecordingText = """
    Try Saying

    "Avengers"
    "Comedy Movies"
    "Marati"
    """
    private let retryErrorDescriptionText = "Retry"
    private let microphoneDirectionText = "Tap on micropone to try again"

    private let inactiveRecorderBlueColor = UIColor.BABlueColor
    private let recorderEnabledIconName = "micIconRecording"
    private let recorderDisabledIconName = "micIconWhite"

    private var buttonRetapped: Bool = true
    private var micEnabled = Bool()
    var timer: Timer?

    private lazy var appName: String = (Bundle.main.object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String) ?? ""

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var microphoneButton: UIButton!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var helperLabel: UILabel!
    var callBack: ((String) -> Void)?

    // MARK: View Controller Lifecycle

    public override func viewDidLoad() {
        super.viewDidLoad()
        // Disable the record buttons until authorization has been granted.
        microphoneButton.isEnabled = false
        helperLabel.text = preRecordingText
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Configure the SFSpeechRecognizer object already
        // stored in a local member variable.
        speechRecognizer.delegate = self
        // Make the authorization Request for Microphone
        AVAudioSession.sharedInstance().requestRecordPermission({ (granted) -> Void in
            if !granted {
                UtilityFunction.shared.performTaskInMainQueue {
                    self.showSettingsAppForMicrophonePermissionDenied()
                }
                return
            } else {
                //self.mixPanelCancelEvent(true)
            }
        })

        // Asynchronously make the authorization request.
        SFSpeechRecognizer.requestAuthorization { authStatus in
            // Divert to the app's main thread so that the UI
            // can be updated.
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.microphoneButton.isEnabled = true
                    self.micEnabled = true
                    self.recordButtonTapped()
                case .denied:
                    self.microphoneButton.isEnabled = false
                    self.micEnabled = false
                    self.showSettingsAppForSpeechRecognitionDenied()
                case .restricted:
                    self.microphoneButton.isEnabled = false
                    self.micEnabled = false
                    self.showAlertForUnavailableSpeechRecognition()
                case .notDetermined:
                    self.microphoneButton.isEnabled = false
                    SFSpeechRecognizer.requestAuthorization { (status) in
                        if status == .authorized {
                            self.microphoneButton.isEnabled = true
                            self.micEnabled = true
                            self.recordButtonTapped()
                        }
                    }
                default:
                    self.microphoneButton.isEnabled = false
                }
                BAKeychainManager().isMicEnabled = self.micEnabled
            }
        }
    }

    func showAlertForUnavailableSpeechRecognition() {
        let alertController = UIAlertController(title: "Voice Search Unavailable", message: "Speech Recognition is restricted on this device", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }

    func showSettingsAppForSpeechRecognitionDenied() {
        let alertController = UIAlertController(title: "Speech Recognition Disabled", message: "Enable Speech Recognition in the Settings App under 'Settings' -> '\(appName)' -> 'Speech Recognition'", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {(_) in
            self.mixPanelCancelEvent(false)
            self.dismiss(animated: true, completion: nil)
        }
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    debugPrint("Settings opened: \(success)") // Prints true
                    self.mixPanelCancelEvent(true)
                })
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        present(alertController, animated: true, completion: nil)
    }

    func showSettingsAppForMicrophonePermissionDenied() {
        let alertController = UIAlertController(title: "Microphone Disabled", message: "Enable Microphone in the Settings App under 'Settings' -> '\(appName)' -> 'Microphone'", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {(_) in
            self.mixPanelCancelEvent(false)
            self.dismiss(animated: true, completion: nil)
        }
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    self.mixPanelCancelEvent(true)
                    debugPrint("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func mixPanelCancelEvent(_ boolvalue: Bool) {
        let permission = boolvalue ? MixpanelConstants.ParamValue.allow.rawValue : MixpanelConstants.ParamValue.deny.rawValue
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchVoicePermission.rawValue, properties: [MixpanelConstants.ParamName.searchAccess.rawValue: permission])
    }

    // MARK: Interface Builder actions

    @IBAction func cancelButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = nil
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func recordButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            if !BAReachAbility.isConnectedToNetwork() {
                kAppDelegate.window?.hideAllToasts()
                kAppDelegate.window?.makeToast("The Internet Connection appears to be offline")
                return
            }
            self.mixPanelEvent()
            self.timer?.invalidate()
            self.timer = nil
            if audioEngine.isRunning {
                let textValue = textView.text ?? ""
                audioSetupQueue.async {
                    self.audioEngine.stop()
                }
                self.recognitionRequest?.endAudio()
                microphoneButton.isEnabled = false
                microphoneButton.layer.sublayers?.removeAll()
                helperLabel.text = microphoneDirectionText
                iconImage.backgroundColor = inactiveRecorderBlueColor
                iconImage.image = UIImage(named: recorderDisabledIconName)
                iconImage.layer.shadowOpacity = 0
                if textValue == initialText || textValue == preRecordingText {
                    textView.text = preRecordingText
                } else {
                    DispatchQueue.main.async {
                        self.dismiss(animated: false) {
                            if let call = self.callBack {
                                call(textValue)
                            }
                        }
                    }
                }
            } else {
                audioSetupQueue.async(flags: .barrier) {
                    do {
                        try self.startRecording()
                        self.buttonRetapped = !self.buttonRetapped
                        if self.buttonRetapped {
                        }
                    } catch {
                        // handle error for no
                    }
                }
            }
            if microphoneButton.isEnabled && textView.text == preRecordingText {
                self.mixPanelEvent()
            }
        } else {
            noInternet()
        }
        
    }
    
    
    func mixPanelEvent() {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.searchReactivateVoice.rawValue)
    }

    private func startRecording() throws {

        // Cancel the previous task if it's running.
        recognitionTask?.cancel()
        self.recognitionTask = nil

        // Configure the audio session for the app.
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(.record, mode: .measurement, options: .duckOthers)
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        let inputNode = audioEngine.inputNode

        // Create and configure the speech recognition request.
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to create a SFSpeechAudioBufferRecognitionRequest object") }
        recognitionRequest.shouldReportPartialResults = true

        // Keep speech recognition data on device
        if #available(iOS 13, *) {
            recognitionRequest.requiresOnDeviceRecognition = false
        }

        // Create a recognition task for the speech recognition session.
        // Keep a reference to the task so that it can be canceled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false

            if let result = result {
                // Update the text view with the results.

                isFinal = result.isFinal

                if isFinal && (self.textView.text == self.initialText || self.textView.text == self.preRecordingText) {
                    self.textView.text = self.preRecordingText
                } else if !isFinal && !self.audioEngine.isRunning {
                    self.textView.text = self.preRecordingText
                } else {
                    self.textView.text = result.bestTranscription.formattedString
                    self.helperLabel.text = nil
                }

            }

            if error != nil || isFinal {
                // Stop recognizing speech if there is a problem.

                if let description = error?.localizedDescription {
                    if description == self.retryErrorDescriptionText {
                        self.textView.text = self.preRecordingText
                    }
                }

                self.microphoneButton.isEnabled = true
                self.microphoneButton.layer.sublayers?.removeAll()

                self.audioSetupQueue.async {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                }
            }
        }

        // Configure the microphone input.
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, _: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }

        audioEngine.prepare()
        try audioEngine.start()

        // Let the user know to start talking.

        DispatchQueue.main.async {
            self.setPreRecordingValues()
            self.timer = Timer.scheduledTimer(withTimeInterval: 6.0, repeats: false) { [weak self] _ in
                self?.recordButtonTapped()
            }
            RunLoop.current.add(self.timer!, forMode: RunLoop.Mode.common)
        }
    }

    fileprivate func setPreRecordingValues() {
        textView.text = initialText

        let animationLayer = BaseLayer(radius: microphoneButton.frame.height / 2.0)
        let rippleLayer = RippleLayer(delay: 0.0, radius: microphoneButton.frame.height / 2.0)
        let rippleLayerCopy = RippleLayer(delay: 1.5, radius: microphoneButton.frame.height / 2.0)

        microphoneButton.layer.insertSublayer(animationLayer, below: microphoneButton.layer)
        microphoneButton.layer.insertSublayer(rippleLayer, below: microphoneButton.layer)
        microphoneButton.layer.insertSublayer(rippleLayerCopy, below: microphoneButton.layer)

        helperLabel.text = preRecordingText
        iconImage.backgroundColor = UIColor.white
        iconImage.image = UIImage(named: recorderEnabledIconName)
        iconImage.dropShadow()
    }

    deinit {
        timer?.invalidate()
        timer = nil
        if audioEngine.isRunning {
            self.audioEngine.stop()
        }

    }
}

@available(iOS 10.0, *)
extension SpeechRecognitionVC: SFSpeechRecognizerDelegate {
    // MARK: SFSpeechRecognizerDelegate
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            microphoneButton.isEnabled = true
        } else {
            microphoneButton.isEnabled = false
        }
    }
}

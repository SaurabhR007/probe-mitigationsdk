//
//  RippleLayer.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 30/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

class RippleLayer: CALayer {

    typealias FloatTriplet = (CGFloat, CGFloat, CGFloat)

    var continuousGroupAnimation = CAAnimationGroup()
    var animationSetupQueue = DispatchQueue.global(qos: .default)

    private var layerBaseRadius = CGFloat.zero

    private let minBorderWidth = CGFloat(0.0)
    private let layerBorderOpacity = Float(0.2)

    var initialColorComponents = (CGFloat(130.0/255.0), CGFloat(47.0/255.0), CGFloat(129.0/255.0))
    var finalColorComponents = (CGFloat(28.0/255.0), CGFloat(64.0/255.0), CGFloat(147.0/255.0))

    override init(layer: Any) {
        super.init(layer: layer)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init(delay: Double, radius: CGFloat) {
        super.init()

        layerBaseRadius = radius

        bounds = CGRect(x: 0, y: 0, width: layerBaseRadius * 2, height: layerBaseRadius * 2)
        cornerRadius = layerBaseRadius
        position = CGPoint(x: layerBaseRadius, y: layerBaseRadius)
        opacity = layerBorderOpacity
        borderWidth = minBorderWidth

        borderColor = UIColor(red: initialColorComponents.0, green: initialColorComponents.1, blue: initialColorComponents.2, alpha: 0.5).cgColor

        animationSetupQueue.async {
            self.fireContinuousAnimation()

            if delay > 0.0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.add(self.continuousGroupAnimation, forKey: nil)
                }
            } else {
                DispatchQueue.main.async {
                    self.add(self.continuousGroupAnimation, forKey: nil)
                }
            }
        }
    }

    fileprivate func fireContinuousAnimation() {

        continuousGroupAnimation.animations = [
            animateBoundsWith(keyPath: KeyPathString.bounds.rawValue, initialRadius: 40.0, delta: 90.0),
            keyFrameAnimationWith(keyPath: KeyPathString.cornerRadius.rawValue, initialValue: 40.0, finalValue: 135.0, divisionsForInterpolation: 3),
            keyFrameAnimationWith(keyPath: KeyPathString.borderWidth.rawValue, initialValue: 0.0, finalValue: 4.0, divisionsForInterpolation: 5)
        ]

        continuousGroupAnimation.duration = 3.0
        continuousGroupAnimation.repeatCount = Float.infinity
        continuousGroupAnimation.autoreverses = false

    }
}

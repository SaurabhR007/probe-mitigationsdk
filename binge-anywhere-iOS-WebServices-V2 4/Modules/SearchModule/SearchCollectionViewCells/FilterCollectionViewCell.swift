//
//  BrowseByCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 13/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class FilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundGenreImage: UIImageView!
    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var contentTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // TODO: Remove Once API integrated
    //    func configureCell(title: String, image: UIImage) {
    //        contentTitle.text = title
    //        contentImage.image = image
    //    }
    
    func configurCell(content: BAContentListModel?, intent: String?) {
        contentTitle.text = content?.title
        
        let cellWidth = Int(contentImage.frame.width)
        let cellHeight = Int(contentImage.frame.height)
        let imageUrl = (content?.image ?? "")
        let imageURL = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: imageUrl, width: cellWidth*3, height: cellHeight*3, trim: (imageUrl.contains("transparent") ))
        let url = URL.init(string: imageURL ?? "")
        
        UtilityFunction.shared.performTaskInMainQueue {
            if let url = url {
                self.contentImage.alpha = (self.contentImage.image == nil) ? 0 : 1
                self.contentImage.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { result in
                    switch result {
                    case .success(let value):
                        break
//                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("FilterCollectionViewCell Error: \(error)")
                    }
                    UIView.animate(withDuration: 1, animations: { self.contentImage.alpha = 1 })
                })
            } else {
                self.contentImage.image = nil
            }
        }
    }
}

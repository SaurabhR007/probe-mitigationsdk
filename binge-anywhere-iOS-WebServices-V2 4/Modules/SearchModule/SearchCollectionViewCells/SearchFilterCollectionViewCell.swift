//
//  SearchFilterCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 15/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol SearchFilterSelectedProtocol: AnyObject {
	//func filterSelected(_ selectedValue: String, _ type: FilterType)
    func moveToBrowseByDetail(intent: String?, genre: String?, language: String?)
}

class SearchFilterCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    var browseByViewModal: BABrowseByViewModal?
    var langaugeModal: BABrowseByModal?
    var genreModal: BABrowseByModal?
    var intent: String?
    var isSelectionMade = false
   // var didSelectTime = Date()

    // MARK: - Variables
    var filterType: FilterType?
    var delegate: SearchFilterSelectedProtocol?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        collectionViewSetup()
    }

    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground

    }

    private func collectionViewSetup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "FilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")
        collectionView.register(UINib(nibName: "BAHomeRailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BAHomeRailCollectionViewCell")
        collectionView.register(UINib(nibName: "BABrowseByHomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BABrowseByHomeCollectionViewCell")
    }

    func configureCell(with type: FilterType, content: BABrowseByModal?) {
        collectionView.reloadData()
        filterType = type
        switch filterType {
        case .language:
            // TODO: Make Language API call
            langaugeModal = content
            intent = "LANGUAGE"
            print("Language")
        case .genre:
            // TODO: Make Genre API call
            genreModal = content
            intent = "GENRE"
            print("Genre")
        default:
            break
        }
    }
}

// MARK: - Extentions
extension SearchFilterCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if intent == "GENRE" {
            return self.genreModal?.data?.contentList?.count ?? 0
        } else {
            return self.langaugeModal?.data?.contentList?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if intent == "GENRE" {
            if genreModal?.data?.layoutType == LayoutType.landscape.rawValue  {
                return CGSize(width: 137, height: 78)
            } else {
                return UICollectionViewFlowLayout.automaticSize
            }
        } else {
               return CGSize(width: 75, height: 95)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        if intent == "GENRE" {
            if genreModal?.data?.layoutType == LayoutType.landscape.rawValue {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BABrowseByHomeCollectionViewCell", for: indexPath) as! BABrowseByHomeCollectionViewCell
                cell.addGradient()
                cell.configureCell(genreModal?.data?.contentList?[indexPath.row] ?? nil, intent: "GENRE")
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
               cell.configurCell(content: genreModal?.data?.contentList?[indexPath.row], intent: intent)
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
            cell.configurCell(content: langaugeModal?.data?.contentList?[indexPath.row], intent: intent)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.isUserInteractionEnabled = false
            if let del = delegate {
                if intent == "GENRE" {
                    del.moveToBrowseByDetail(intent: intent, genre: self.genreModal?.data?.contentList?[indexPath.row].title ?? "", language: "")
                } else {
                    del.moveToBrowseByDetail(intent: intent, genre: "", language: self.langaugeModal?.data?.contentList?[indexPath.row].title ?? "")
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
             self.collectionView.isUserInteractionEnabled = true
            }
    }
}

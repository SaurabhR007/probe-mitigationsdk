//
//  ResultFilterCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 27/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class ResultFilterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var background: UIView!
    @IBOutlet weak var filterLabel: CustomLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        configUI()
    }

    private func configUI() {
        background.backgroundColor = .BAGreyButtonColor
        filterLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 15), color: .BAwhite)
    }

    func configureCell(_ title: String, _ selectedIndex: Int) {
        filterLabel.text = title
        background.makeCircular(roundBy: .height)
        if selectedIndex != -1 {
            background.backgroundColor = .BApinkColor
        } else {
            background.backgroundColor = .BAGreyButtonColor
        }
    }
}

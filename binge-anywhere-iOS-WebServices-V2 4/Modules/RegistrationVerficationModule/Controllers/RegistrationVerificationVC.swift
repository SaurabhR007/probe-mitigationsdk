//
//  VerificationVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

class RegistrationVerificationVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Variables
    var registrationVerificationDataModel: RegistrationVerificationVM?
    var registrationModel: [AnyHashable: Any] = [:]
    var subscriberIdList = [String]()
    var linkMyAccountFlag = false
    var selectedSegment: Int?
    var isTermsAccepted = false

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setNavigationBar()
        reCheckVM()
        setUpTableView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: - Functions
    private func setNavigationBar() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

    private func configureUI() {
        self.view.backgroundColor = UIColor.BAdarkBlueBackground
        tableView.allowsSelection = false
//        AlertController.initialization().showAlertWithOkButton(aStrMessage: "OTP Generated Successfully") { (index, title) in
//            print(index, title)
//        }
    }

    private func reCheckVM() {
        if registrationVerificationDataModel == nil {
            registrationVerificationDataModel = RegistrationVerificationVM()
            registrationVerificationDataModel?.subscriberList = subscriberIdList
            registrationVerificationDataModel?.apiParams = registrationModel
            registrationVerificationDataModel?.linkMyAccountFlag = linkMyAccountFlag
        }
    }

    private func setUpTableView() {
        let cells = RegistrationVerificationVcDataSource.CellType.typeOfCells
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension

        for item in cells {
            UtilityFunction.shared.registerCell(tableView, item.cellIndentifier)
        }
    }

    func showResendCountAlert() {
		showOkAlert(AppStringConstant.otpResendLimit.rawValue)
    }

    func checkForTermAndCondition() {
        if let viewModel = registrationVerificationDataModel {
            viewModel.checkForTermAndCondition(isSelected: isTermsAccepted) { (flagValue, message) in
                if flagValue {
                    self.validateOtpCall()
                } else {
                    self.apiError(message, title: "")
                }
            }
        }
    }

    func validateOtpCall() {
        if BAReachAbility.isConnectedToNetwork() {
            if let viewModel = registrationVerificationDataModel {
                showActivityIndicator(isUserInteractionEnabled: false)
                viewModel.verifyRegistrationOtp { (flagValue, message) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.moveToSuccessScreen()
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
			noInternet() {
				self.validateOtpCall()
			}
        }
    }

    func moveToSuccessScreen() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: "CommonAlert", identifierVC: "ApiSuccessViewController", type: ApiSuccessViewController.self)
        pushView.successTitleText = "Sign Up Successful"
        pushView.nextButtonTitle = "Next"
        //BAUserDefaultManager().isAutoPlayOn = true
        self.navigationController?.pushViewController(pushView, animated: true)
    }

//    func showOkAlert(_ message: String) {
//        AlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, value) in
//            self.hideActivityIndicator()
//            self.view.endEditing(true)
//            print(index, value)
//        }
//    }
}

// MARK: - Extention
extension RegistrationVerificationVC: RegistrationVerificationInputTableViewCellDelegate, ResendOtpButtonActionDelegate, NextButtonActionDelegate, SignUpAlreadyUserTableViewCellProtocol {

    func valueUpdated(_ text: String) {
//        registrationVerificationDataModel?.dataModel.otp = text
    }

    func resendButtonTapped() {
        otpResent("OTP Resent") {
            self.tableView.reloadRows(at: [IndexPath(item: 5, section: 0)], with: .fade)
        }
    }

    func isAccepted(_ value: Bool) {
        isTermsAccepted = value
    }
    
    func openTermsAndCondition() {
        // Do Nothing
    }

    func nextButtonTapped() {
        if let viewModel = registrationVerificationDataModel {
            viewModel.regisTrationOtpValidation { (flagValue) in
                if flagValue {
                    self.checkForTermAndCondition()
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowOtpAlert"), object: nil)
                    self.view.endEditing(true)
                    return
                }
            }
        }
    }
}

//
//  ExtensionVerification+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 19/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

// MARK: - Extention
extension RegistrationVerificationVC: UITableViewDataSource, UITableViewDelegate {

    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RegistrationVerificationVcDataSource.CellType.typeOfCells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let  cell = tableView.dequeueReusableCell(withIdentifier: kPageIndicatorTableViewCell) as! PageIndicatorTableViewCell
            cell.configureCell(with: .second)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as? TitleHeaderTableViewCell
            cell?.configureCell(source: .verification)
            cell?.infoLabel.text = "\(cell?.infoLabel.text ?? "") +91 \(registrationModel["rmn"] ?? "")"
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RegistrationVerificationTextFieldTableViewCell") as? RegistrationVerificationTextFieldTableViewCell
            cell?.delegate = self
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpAlreadyUserTableViewCell") as! SignUpAlreadyUserTableViewCell
            cell.delegate = self
            cell.configureCell(source: .verification)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NextButtonTableViewCell") as? NextButtonTableViewCell
            cell?.delegate = self
            cell?.configureCell(source: .verification)
            return cell!
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResendOtpTableViewCell") as? ResendOtpTableViewCell
            cell?.delegate = self
            return cell!
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomBingeLogoTableViewCell") as? BottomBingeLogoTableViewCell
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

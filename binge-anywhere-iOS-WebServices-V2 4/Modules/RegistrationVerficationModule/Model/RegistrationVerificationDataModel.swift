//
//  VerificationDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

//struct VerificationDataModel {
//    var rmn: String?
//    var formType: String?
//    var otp: String?
//    var source: String?
//    var name: String?
//    var email: String?
//}
//struct VerificationServerResponse: Codable {
//    let code: Int?
//    let message: String?
//    let data: VerificationData?
//
//    enum CodingKeys: String, CodingKey {
//        case code = "code"
//        case message = "message"
//        case data = "data"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        code = try values.decodeIfPresent(Int.self, forKey: .code)
//        message = try values.decodeIfPresent(String.self, forKey: .message)
//        data = try values.decodeIfPresent(VerificationData.self, forKey: .data)
//    }
//
//}
//struct VerificationData: Codable {
//    let refreshToken: String?
//    let accessToken: String?
//    let expiresIn: String?
//    let dsnAccessToken: String?
//    let dsnRefreshToken: String?
//    let dsnExpiresIn: String?
//    let firstTimeLogin: Bool?
//    let userDetails: UserDetails?
//    let deviceDetails: [String]?
//
//    enum CodingKeys: String, CodingKey {
//
//        case refreshToken = "refreshToken"
//        case accessToken = "accessToken"
//        case expiresIn = "expiresIn"
//        case dsnAccessToken = "dsnAccessToken"
//        case dsnRefreshToken = "dsnRefreshToken"
//        case dsnExpiresIn = "dsnExpiresIn"
//        case firstTimeLogin = "firstTimeLogin"
//        case userDetails = "userDetails"
//        case deviceDetails = "deviceDetails"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        refreshToken = try values.decodeIfPresent(String.self, forKey: .refreshToken)
//        accessToken = try values.decodeIfPresent(String.self, forKey: .accessToken)
//        expiresIn = try values.decodeIfPresent(String.self, forKey: .expiresIn)
//        dsnAccessToken = try values.decodeIfPresent(String.self, forKey: .dsnAccessToken)
//        dsnRefreshToken = try values.decodeIfPresent(String.self, forKey: .dsnRefreshToken)
//        dsnExpiresIn = try values.decodeIfPresent(String.self, forKey: .dsnExpiresIn)
//        firstTimeLogin = try values.decodeIfPresent(Bool.self, forKey: .firstTimeLogin)
//        userDetails = try values.decodeIfPresent(UserDetails.self, forKey: .userDetails)
//        deviceDetails = try values.decodeIfPresent([String].self, forKey: .deviceDetails)
//    }
//
//}
//
//struct UserDetails: Codable {
//    let sid: String?
//    let sName: String?
//    let rmn: String?
//    let acStatus: String?
//    let emailId: String?
//    let entitlements: [String]?
//    let devices: [String]?
//
//    enum CodingKeys: String, CodingKey {
//        case sid = "sid"
//        case sName = "sName"
//        case rmn = "rmn"
//        case acStatus = "acStatus"
//        case emailId = "emailId"
//        case entitlements = "entitlements"
//        case devices = "devices"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        sid = try values.decodeIfPresent(String.self, forKey: .sid)
//        sName = try values.decodeIfPresent(String.self, forKey: .sName)
//        rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
//        acStatus = try values.decodeIfPresent(String.self, forKey: .acStatus)
//        emailId = try values.decodeIfPresent(String.self, forKey: .emailId)
//        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
//        devices = try values.decodeIfPresent([String].self, forKey: .devices)
//    }
//
//}

enum RegistrationVerificationVcDataSource {
    case title
    case segment
    case textfield
    case next
    case resendOtp
    case signUp
    case footer

    static let numberOfCells = [title, segment, textfield, next, resendOtp, signUp, footer]

    enum CellType {
        case pageIndicator
        case header
        case textfield
        case next
        case resendOtp
        case footer
        case termsAndConditions

        static let typeOfCells = [pageIndicator, header, textfield, next, resendOtp, footer, termsAndConditions]

        var cellIndentifier: String {
            switch self {
            case .pageIndicator:
                return kPageIndicatorTableViewCell
            case .header:
                return kTitleHeaderTableViewCell
            case .textfield:
                return kRegistrationVerificationTextFieldTableViewCell
            case .next:
                return kNextButtonTableViewCell
            case .resendOtp:
                return kResendOtpTableViewCell
            case .footer:
                return kBottomBingeLogoTableViewCell
            case .termsAndConditions:
                return kSignUpAlreadyUserTableViewCell
            }
        }
    }
}

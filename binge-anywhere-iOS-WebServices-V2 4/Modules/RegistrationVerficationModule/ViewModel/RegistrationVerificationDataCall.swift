//
//  VerificationDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol VerifySignUpOtpDataCall {
    var apiParams: [AnyHashable: Any] {get set}
//    func makeAPI(completion: @escaping (Bool) -> Void) -> ServiceCancellable?
}

// MARK: - Extention
extension VerifySignUpOtpDataCall {
//    func makeAPI(completion: @escaping (Bool) -> Void) -> ServiceCancellable? {
//        let target = ServiceRequestDetail.init(endpoint: .validateRegistrationOtp(param: apiParams, headers: nil))
//        return APIService().request(target) { (response: APIResult<APIServicResult<VerificationServerResponse>, ServiceProviderError>) in
//            switch response {
//            case .success(let result):
//                completion(true)
//                print(result.parsed)
//            case .error(let error):
//                completion(false)
//                print(error)
//            }
//        }
//    }
}

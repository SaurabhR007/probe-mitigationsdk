//
//  VerificationVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol RegistrationVerificationVMProtocol {
//    var dataModel: VerificationDataModel {get set}
    var linkMyAccountFlag: Bool {get set}
    var subscriberList: [String] {get set}
    func regisTrationOtpValidation(completion: @escaping (Bool) -> Void)
    func checkForTermAndCondition(isSelected: Bool, completion: @escaping (Bool, String?) -> Void)
    func verifyRegistrationOtp(completion: @escaping (Bool, String?) -> Void)
}

class RegistrationVerificationVM: RegistrationVerificationVMProtocol, LoginDataCallProtocol, ErrorHandler {
    var apiEndPoint: String = ""

    var apiParams = [AnyHashable: Any]()
    var subscriberList: [String] = []
    var linkMyAccountFlag = false
    var requestToken: ServiceCancellable?
//    var dataModel = VerificationDataModel()

    func verifyRegistrationOtp(completion: @escaping (Bool, String?) -> Void) {
        createApiParams()
//        requestToken = confirmRegistrationOtpApiCall(completion: { (configData, error) in
//            self.requestToken = nil
//            if let _configData = configData {
//                if let statusCode = _configData.parsed.code {
//                    if statusCode == 0 {
//                        BAUserDefaultManager().loginDetail = _configData.parsed
//                        BAUserDefaultManager().baId = _configData.parsed.data?.userDetails?.bingeSubscription?[0].baId ?? ""
//                        BAUserDefaultManager().accessToken = _configData.parsed.data?.accessToken ?? ""
//                        BAUserDefaultManager().profileId = _configData.parsed.data?.userDetails?.bingeSubscription?[0].profileId ?? "584c81e8-3326-4f6b-a08a-9ba6db8b4e6c"
//                        BAUserDefaultManager().aliasName = _configData.parsed.data?.userDetails?.bingeSubscription?[0].aliasName ?? ""
//                        BAUserDefaultManager().fullName = _configData.parsed.data?.userDetails?.fullName ?? "Sudhanshu Tyagi"
//                        BAUserDefaultManager().mobileNumber = _configData.parsed.data?.userDetails?.rmn ?? "90 9090 3344"
//                        BAUserDefaultManager().customerName = "Guest User"
//                        completion(true, nil)
//                    } else {
//                        if let errorMessage = _configData.parsed.message {
//                            completion(false, errorMessage)
//                        } else {
//                            completion(false, "Some Error Occurred")
//                        }
//                    }
//                } else {
//                    completion(false, "Some Error Occurred")
//                }
//            } else if let error = error {
//                completion(false, error.error.localizedDescription)
//            } else {
//                completion(false, "Some Error Occurred")
//            }
//        })
    }

    func regisTrationOtpValidation(completion: @escaping (Bool) -> Void) {
//        guard let otpString = dataModel.otp else {
//            completion(false)
//            return
//        }
//        if otpString.length < 6 {
//            completion(false)
//        }
        completion(true)
    }

    func checkForTermAndCondition(isSelected: Bool, completion: @escaping (Bool, String?) -> Void) {
        apiParams["eulaChecked"] = isSelected
        completion(isSelected, isSelected ? nil : "Please accept terms and conditions.")
    }

    func createApiParams() {
//        apiParams["otp"] = dataModel.otp ?? ""
        if !subscriberList.isEmpty && linkMyAccountFlag {
            apiParams["accountList"] = subscriberList
        }
    }
}

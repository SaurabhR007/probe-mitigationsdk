//
//  SignUpTextFieldTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 24/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

protocol RegistrationVerificationInputTableViewCellDelegate: AnyObject {
    func valueUpdated(_ text: String)
}

class RegistrationVerificationTextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var inputTextField: CustomtextField!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var errorInfoLabel: CustomLabel!

    // MARK: - Variables
    weak var delegate: RegistrationVerificationInputTableViewCellDelegate?
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
        NotificationCenter.default.addObserver(self, selector: #selector(showOTPError), name: NSNotification.Name(rawValue: "ShowOtpAlert"), object: nil)
        self.contentView.backgroundColor = .BAdarkBlueBackground
        inputTextField.delegate = self
        inputTextField.text = ""
        inputTextField.placeholder = "6 Digits"
        inputTextField.backgroundColor = .BAdarkBlue1Color
        inputTextField.textColor = .BAwhite
        inputTextField.tintColor = .BAwhite
        inputTextField.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
        inputTextField.keyboardType = .numberPad
        inputTextField.addDoneButtonOnKeyBoardWithControl()
        inputTextField.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth))
        inputTextField.cornerRadius = 2
        inputTextField.setViewShadow(opacity: 0.12, blur: 2)
        inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)

        titleLabel.text = kEnterLogin6DigitOTP//"Enter Your Code"
        titleLabel.textColor = .BAwhite
        titleLabel.font = .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth)

        errorInfoLabel.text = ""
        errorInfoLabel.textColor = .BAerrorRed
        errorInfoLabel.font = .skyTextFont(.medium, size: 10 * screenScaleFactorForWidth)
    }

    @objc private func showOTPError(notification: NSNotification) {
        textFieldHasError(true, with: "6 Digits", errorMessage: "OTP cannot be blank")
    }

    private func isTextFieldHighlighted(_ highlighted: Bool) {
        inputTextField.borderWidth = 0.0
        if highlighted {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAlightBlueGrey
        }
    }

    private func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
        if hasError {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAerrorRed
            inputTextField.placeholder = Placeholder
            errorInfoLabel.text = errorMessage
        } else {
            inputTextField.borderWidth = 0.0
            errorInfoLabel.text = ""
        }
    }

    private func validateData(_ tag: Int, _ text: String) -> Bool {
        return text.length <= 6
    }

    // MARK: - Actions
    @IBAction private func txtFieldBeginEditing(_ sender: CustomtextField) {
        textFieldHasError(false)
        isTextFieldHighlighted(true)
    }

    @IBAction private func txtFieldEndEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(false)
        if !sender.text!.isEmpty && sender.text!.count < 6 {
            textFieldHasError(true, with: "6 Digits", errorMessage: "Please enter a valid OTP")
        } else if  sender.text! == "" {
            textFieldHasError(true, with: "6 Digits", errorMessage: "OTP cannot be blank")
        }
    }
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let del = delegate {
            del.valueUpdated(sender.text ?? "")
        }
    }

}

// MARK: - Extension
extension RegistrationVerificationTextFieldTableViewCell {

    // MARK: - TableView Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        return validateData(textField.tag, txtAfterUpdate)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.superview?.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

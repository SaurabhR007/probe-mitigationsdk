//
//  WatchlistVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class WatchlistVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var watchListCollectionView: UICollectionView!
    @IBOutlet weak var noContentView: UIView!

    // MARK: - Variables
    //    var watchListViewModal: BAWatchlistViewModalProtocol?
    var watchlistDataVM: BAWatchlistViewModal?
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    //var didSelectTime = Date()
    /// CascadableScroll - Protocol AssociatedType explicite value
//    typealias GridViewType = UICollectionView
//    typealias ViewCellType = UICollectionViewCell
//    var cascadeQueue: OperationQueue = OperationQueue()
//    var stopCollectionViewAnimationOnScroll: Bool = true
//    var needAnimationFlag: Bool = true
    var vodID: Int?
    var isLoadMore = false
    var isSelectionMade = false
    private let refreshControl = UIRefreshControl()

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeWatchlist.rawValue)
        //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewWatchList.rawValue)
        configureUI()
        conformDelegated()
        registerCells()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewWatchList.rawValue)
        isLoadMore = false
        recheckVM()
       //getWatchListContent() //refresh watch list data
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        ARSLineProgress.hide()
//    }

    // MARK: - Function
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

//    @objc private func refreshWatchListContent(_ sender: Any) {
//        getWatchListContent(true)
//    }

    func configureUI() {
        super.configureNavigationBar(with: .centerTitle, nil, nil, nil, "Watchlist")
        self.watchListCollectionView.isHidden = true
        self.noContentView.isHidden = true
        self.noContentView.backgroundColor = .BAdarkBlueBackground
        self.watchListCollectionView.backgroundColor = .BAdarkBlueBackground
    }

    func conformDelegated() {
        watchListCollectionView.delegate = self
        watchListCollectionView.dataSource = self
    }

    func registerCells() {
        watchListCollectionView.register(UINib(nibName: kBAHomeRailCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeRailCollectionViewCell)
        watchListCollectionView.register(UINib(nibName: kLoadMoreCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kLoadMoreCollectionViewCell)
    }

    func recheckVM() {
        watchlistDataVM = BAWatchlistViewModal(repo: BaWatchListRepo(), endPoint: "listing", baId: BAKeychainManager().baId)
        getWatchListContent()
    }

    func getWatchListContent() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            showActivityIndicator(isUserInteractionEnabled: false)
            if watchlistDataVM != nil {
                watchlistDataVM?.loadMore = isLoadMore
                if isLoadMore {
                    watchlistDataVM?.pagingState = watchlistDataVM?.watchListData?.data?.pagingState ?? ""
                } else {
                    watchlistDataVM?.pagingState = ""
                }
                watchlistDataVM?.getWatchListData(completion: {(flagValue, message, isApiError) in
                    self.refreshControl.endRefreshing()
                    self.hideActivityIndicator()
                    if flagValue {
                        if self.watchlistDataVM?.watchListData?.data?.list?.isEmpty ?? false {
                            self.watchListCollectionView.isHidden = true
                            self.noContentView.isHidden = false
                        } else {
                            if let data = self.watchlistDataVM?.watchListData?.data?.list {
                                self.watchlistDataVM?.watchListData?.data?.list = UtilityFunction.shared.filterTAData(data)
                            }
                            if self.watchlistDataVM?.watchListData?.data?.list?.isEmpty ?? false {
                                self.watchListCollectionView.isHidden = true
                                self.noContentView.isHidden = false
                            } else {
                                self.watchListCollectionView.isHidden = false
                                self.noContentView.isHidden = true
                                self.watchListCollectionView.reloadData()
                                if !self.isLoadMore {
                                    self.watchListCollectionView.setContentOffset(.zero, animated: false)
                                }
                            }
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
			noInternet()
        }
    }

    func moveToDetailScreenFromWatchList(vodId: Int?, contentType: String?, timeStamp: Int?) {
        if vodId != nil {
            fetchContentDetail(vodId: vodId, contentType: contentType, timeStamp: timeStamp)
        }
    }

    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?, timeStamp: Int?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.customBrand.rawValue:
            railContentType = "brand"
        case ContentType.customSeries.rawValue:
            railContentType = "series"
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId, timeStamp: timeStamp)
    }

    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?, timeStamp: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    //self.hideActivityIndicator()
                    if flagValue {
                        let genreResult = dataModel.contentDetail?.meta?.genre?.joined(separator: ",")
                        let languageResult = dataModel.contentDetail?.meta?.audio?.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewContentDetail.rawValue, properties: [MixpanelConstants.ParamName.vodRail.rawValue: "", MixpanelConstants.ParamName.contentTitle.rawValue: dataModel.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentType.rawValue: dataModel.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: dataModel.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.source.rawValue: "Watchlist", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.origin.rawValue: "Editorial", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId, timeStamp: timeStamp)
                    } else if isApiError {
                        self.hideActivityIndicator()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.hideActivityIndicator()
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
			noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?, timeStamp: Int?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId, timeStamp: timeStamp ?? 0)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int, timeStamp: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail, expiry: timeStamp)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?, expiry: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            //showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
               // hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.pageSource = "Watchlist"
                        contentDetailVC.configSource = "Editorial"
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "Watchlist"
                        contentDetailVC.configSource = "Editorial"
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                        contentDetailVC.pageSource = "Watchlist"
                        contentDetailVC.configSource = "Editorial"
                        if contentDetail?.detail?.contractName == "RENTAL" {
                            contentDetailVC.timestamp = expiry ?? 0
                        }
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideActivityIndicator()
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                        //}
                        //self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }

    override func retryButtonAction() {
        getWatchListContent(/*false*/)
    }
}

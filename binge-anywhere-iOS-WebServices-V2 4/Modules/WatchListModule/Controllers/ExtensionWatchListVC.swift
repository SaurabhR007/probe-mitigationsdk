//
//  ExtensionContinueWatchVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension WatchlistVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, /*CascadableScroll,*/LoadMoreDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = watchlistDataVM?.watchListData else {
            return 0
        }
        guard let count = data.data?.list?.count else {
            return 0
        }
//        if self.watchlistDataVM?.watchListData?.data?.continuePaging ?? false {
//            return count + 1
//        } else {
            return count
//        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        guard let data = watchlistDataVM?.watchListData else {
//            return CGSize.zero
//        }

//        if self.watchlistDataVM?.watchListData?.data?.continuePaging ?? false && indexPath.row == data.data?.list?.count {
//            return CGSize(width: collectionView.frame.size.width, height: 40)
//        } else {
            let layoutType = "LANDSCAPE"
            let requiredNumOfColumns: CGFloat = isPhone ? 2 : (layoutType == LayoutType.potrait.rawValue) ? 5 : 4
            let deviceWidth: CGFloat = UIScreen.main.bounds.width
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

            let sectionInsetLeft: CGFloat = flowLayout.sectionInset.left
            let sectionInsetRight: CGFloat = flowLayout.sectionInset.right
            let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing

            let cellWidth = (deviceWidth - sectionInsetLeft - sectionInsetRight - (requiredNumOfColumns-1)*cellSpacing)/requiredNumOfColumns
            
            switch layoutType {
            case LayoutType.potrait.rawValue:
                return CGSize(width: cellWidth, height: cellWidth*(16/9))
            case LayoutType.landscape.rawValue:
                return CGSize(width: cellWidth, height: cellWidth)
            default:
                let padding: CGFloat =  42
                let collectionViewSize = collectionView.frame.size.width - padding
                return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
            }
//        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        isSelectionMade = false
        guard let data = watchlistDataVM?.watchListData else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
            return cell
            
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeRailCollectionViewCell, for: indexPath) as! BAHomeRailCollectionViewCell
		if data.data?.list?[indexPath.row].secondsWatched ?? 0 > 0 {
			cell.addGradient()
			cell.showProgress(from: data.data?.list?[indexPath.row].secondsWatched ?? 0, to: data.data?.list?[indexPath.row].durationInSeconds ?? 0)
		}
        cell.configureCell(data.data?.list?[indexPath.row] ?? nil, LayoutType.landscape.rawValue)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 14.0, bottom: 14.0, right: 14.0)
    }

//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        cascadeCollectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
//    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if watchlistDataVM?.watchListData?.data?.list?[indexPath.row].contentType ==
//            ContentType.tvShows.rawValue {
//            moveToDetailScreenFromWatchList(vodId: watchlistDataVM?.watchListData?.data?.list?[indexPath.row].contentId, contentType: watchlistDataVM?.watchListData?.data?.list?[indexPath.row].parentContentType)
//        } else {
//          if Date().timeIntervalSince(self.didSelectTime) < 2.0 {
//              self.didSelectTime = Date()
//              return
//          }
        watchListCollectionView.isUserInteractionEnabled = false
        //CustomLoader.shared.showLoader(false)
//        if isSelectionMade {
//            return
//        } else {
//            isSelectionMade = true
        moveToDetailScreenFromWatchList(vodId: watchlistDataVM?.watchListData?.data?.list?[indexPath.row].contentId, contentType: watchlistDataVM?.watchListData?.data?.list?[indexPath.row].contentType, timeStamp: watchlistDataVM?.watchListData?.data?.list?[indexPath.row].rentalExpiry)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
           self.watchListCollectionView.isUserInteractionEnabled = true
        }
      //  }
            
       // }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func loadMoreTapped() {
//        if self.watchlistDataVM?.watchListData?.data?.continuePaging ?? false {
//            isLoadMore = true
//            self.getWatchListContent(/*true*/)
//        }
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sixtyP = CGFloat(80) * scrollView.contentSize.height / CGFloat(100)
        if scrollView.bounds.maxY > sixtyP {
            if self.watchlistDataVM?.nextPageAvailable ?? false {
                isLoadMore = true
                self.getWatchListContent()
                self.watchlistDataVM?.nextPageAvailable = false
            }
        }
    }
}


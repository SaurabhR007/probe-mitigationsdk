//
//  BaWatchListRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BaWatchListScreenRepo {
    var apiEndPoint: String {get set}
    func getWatchListData(apiParams: APIParams, completion: @escaping(APIServicResult<BAWatchListModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BaWatchListRepo: BaWatchListScreenRepo {
    var apiEndPoint: String = ""
    func getWatchListData(apiParams: APIParams, completion: @escaping(APIServicResult<BAWatchListModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
         //let apiParams = APIParams()
        // TODO: Remove once kong is setup
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .watchListData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAWatchListModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

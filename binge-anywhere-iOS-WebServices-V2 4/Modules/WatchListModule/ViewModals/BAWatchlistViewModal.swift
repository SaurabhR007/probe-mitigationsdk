//
//  BAWatchlistViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAWatchlistViewModalProtocol {
    func getWatchListData(completion: @escaping(Bool, String?, Bool) -> Void)
    var watchListData: BAWatchListModal? {get set}
    var nextPageAvailable: Bool {get set}
}

class BAWatchlistViewModal: BAWatchlistViewModalProtocol {

    var requestToken: ServiceCancellable?
    var repo: BaWatchListScreenRepo
    var tvodVM: BATVODViewModal?
    var tvodData: TVODData?
    var baId: String = "0"
    var endPoint: String = ""
    var nextPageAvailable = false
    var pageLimit = 20
    var pageOffset = 0
    var watchListData: BAWatchListModal?
    var apiParams = APIParams()
    var loadMore = false
    var pagingState = ""

    init(repo: BaWatchListScreenRepo, endPoint: String, baId: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.baId = baId
    }

    func getWatchListData(completion: @escaping(Bool, String?, Bool) -> Void) {
        self.checkForTVODData {
            self.apiParams = APIParams() //To clear values in API params made in previous API calls
            self.apiParams["subscriberId"] = BAKeychainManager().sId
            self.apiParams["profileId"] = BAKeychainManager().profileId//"584c81e8-3326-4f6b-a08a-9ba6db8b4e6c"
            if self.loadMore {
                self.apiParams["pagingState"] = self.pagingState
            } else {
                // Do Nothing
            }
            self.requestToken = self.repo.getWatchListData(apiParams: self.apiParams, completion: { (configData, error) in
                self.requestToken = nil
                if let _configData = configData {
                    if let statusCode = _configData.parsed.code {
                        if statusCode == 0 {
                            if self.loadMore == false {
                                self.watchListData = _configData.parsed
                                self.watchListData?.data?.list?.removeAll()
                                for watchData in _configData.parsed.data?.list ?? [] {
                                    if watchData.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == watchData.title}) ?? true {
                                                for item in self.tvodData?.items ?? [] {
                                                    if item.title == watchData.title {
                                                        watchData.rentalExpiry = item.rentalExpiry
                                                        self.watchListData?.data?.list?.append(watchData)
                                                    }
                                                }
//          wrong logic was implemented here which cause wrong data to be fetched
//                                                watchData.rentalExpiry = self.tvodData?.items?[0].rentalExpiry
//                                                self.watchListData?.data?.list?.append(watchData)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        self.watchListData?.data?.list?.append(watchData)
                                    }
                                }
                            } else {
                                self.watchListData?.data?.continuePaging = _configData.parsed.data?.continuePaging
                                self.watchListData?.data?.pagingState = _configData.parsed.data?.pagingState
                                for watchData in _configData.parsed.data?.list ?? [] {
                                    if watchData.contractName == "RENTAL" {
                                        if self.tvodData?.items?.count ?? 0 > 0 {
                                            if self.tvodData?.items?.contains(where: {$0.title == watchData.title}) ?? true {
                                                for item in self.tvodData?.items ?? [] {
                                                    if item.title == watchData.title {
                                                        watchData.rentalExpiry = item.rentalExpiry
                                                        self.watchListData?.data?.list?.append(watchData)
                                                    }
                                                }
//                                                wrong logic was implemented here which cause wrong data to be fetched
//                                                self.watchListData?.data?.list?.append(watchData)
                                            } else {
                                                // Do Nothing
                                            }
                                        }
                                    } else {
                                        self.watchListData?.data?.list?.append(watchData)
                                    }
                                }
                            }
                            if let screenData = self.watchListData?.data {
                                if let watchlistData = screenData.list {
                                    self.pageOffset = watchlistData.count
                                    self.nextPageAvailable = self.watchListData?.data?.continuePaging ?? false
                                }
                            }
                            completion(true, nil, false)
                        } else {
                            if _configData.serviceResponse.statusCode != 200 {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, true)
                                } else {
                                    completion(false, "Some Error Occurred", true)
                                }
                            } else {
                                if let errorMessage = _configData.parsed.message {
                                    completion(false, errorMessage, false)
                                } else {
                                    completion(false, "Some Error Occurred", false)
                                }
                            }
                        }
                    } else if _configData.serviceResponse.statusCode == 401 {
                        completion(false, kSessionExpire, false)
                    } else {
                        completion(false, "Some Error Occurred", true)
                    }
                } else if let error = error {
                    completion(false, error.error.localizedDescription, true)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            })
        }
    }
    
    // MARK: Get TVOD Content List Of Active Ones
    func checkForTVODData( _ completion: @escaping () -> Void) {
        tvodVM = BATVODViewModal(repo: BATVODRepo(), endPoint: 0)
        if let tVODVM = tvodVM {
            tVODVM.getTVODListingData { (flagValue, _, isApiError) in
                if flagValue {
                    self.tvodData = tVODVM.tvodModal?.data
                    completion()
                } else {
                    completion()
                }
            }
            return
        }
        completion()
    }
}

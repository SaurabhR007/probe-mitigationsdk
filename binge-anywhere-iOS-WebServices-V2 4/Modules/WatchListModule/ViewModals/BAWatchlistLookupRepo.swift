//
//  BAWatchlistLookupRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAWatchlistLookupScreenRepo {
    var apiEndPoint: String {get set}
    func watchlistLookUp(apiParams: APIParams, completion: @escaping(APIServicResult<BAWatchlistLookUpModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAWatchlistLookupRepo: BAWatchlistLookupScreenRepo {
    var apiEndPoint: String = ""
    func watchlistLookUp(apiParams: APIParams, completion: @escaping(APIServicResult<BAWatchlistLookUpModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": "bearer \(BAKeychainManager().userAccessToken)", "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .watchlistLookup(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAWatchlistLookUpModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

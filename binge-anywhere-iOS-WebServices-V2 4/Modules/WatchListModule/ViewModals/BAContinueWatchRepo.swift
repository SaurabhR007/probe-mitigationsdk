//
//  BAContinueWatchRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 17/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAContinueWatchScreenRepo {
    var apiEndPoint: String {get set}
    func getContinueWatchData(apiParams: APIParams, completion: @escaping(APIServicResult<BAContinueWatchingModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAContinueWatchRepo: BAContinueWatchScreenRepo {
    var apiEndPoint: String = ""
    func getContinueWatchData(apiParams: APIParams, completion: @escaping(APIServicResult<BAContinueWatchingModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
         //let apiParams = APIParams()
        // TODO: Remove once kong is setup
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": BAKeychainManager().userAccessToken, "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE", "profileId": BAKeychainManager().profileId]
        let target = ServiceRequestDetail.init(endpoint: .continueWatchData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAContinueWatchingModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

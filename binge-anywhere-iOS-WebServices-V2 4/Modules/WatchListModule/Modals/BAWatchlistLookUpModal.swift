//
//  BAWatchlistLookUpModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAWatchlistLookUpModal: Codable {
    let code: Int?
    let message: String?
    let data: LastWatch?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(LastWatch.self, forKey: .data)
    }
}

struct BAWatchlistLookupData: Codable {
    let subscriberId: String?
    let profileId: String?
    let contentId: Int?
    let contentType: String?
    let secondsWatched: Int?
    let durationInSeconds: Int?
    let favourite: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subscriberId = try values.decodeIfPresent(String.self, forKey: .subscriberId)
        profileId = try values.decodeIfPresent(String.self, forKey: .profileId)
        contentId = try values.decodeIfPresent(Int.self, forKey: .contentId)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        secondsWatched = try values.decodeIfPresent(Int.self, forKey: .secondsWatched)
        durationInSeconds = try values.decodeIfPresent(Int.self, forKey: .durationInSeconds)
        favourite = try values.decodeIfPresent(Bool.self, forKey: .favourite)
    }

}

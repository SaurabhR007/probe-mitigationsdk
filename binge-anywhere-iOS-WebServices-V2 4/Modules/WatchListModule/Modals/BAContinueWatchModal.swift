//
//  BAContinueWatchModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 17/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAContinueWatchModal: Codable {
    let code: Int?
    let message: String?
    var data: ContinueWatchData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ContinueWatchData.self, forKey: .data)
    }
}

struct ContinueWatchData: Codable {
    let id: String?
    let title: String?
    let sectionType: String?
    let layoutType: String?
    var contentList: [BAContentListModel]?
    let autoScroll: Bool?
    let totalCount: Int?
    let position: Int?
    let configType: String?
    let pageType: String?
    let pageState: String?
    let continueWatching: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        autoScroll = try values.decodeIfPresent(Bool.self, forKey: .autoScroll)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        position = try values.decodeIfPresent(Int.self, forKey: .position)
        configType = try values.decodeIfPresent(String.self, forKey: .configType)
        pageType = try values.decodeIfPresent(String.self, forKey: .pageType)
        pageState = try values.decodeIfPresent(String.self, forKey: .pageState)
        continueWatching = try values.decodeIfPresent(Bool.self, forKey: .continueWatching)
    }
}

//// Code Commented as BAContentListModel is now in use however if any major issues came then just need to uncomment this line and change BAContentListModel to this content list model
//struct ContinueWatchContentList: Codable {
//    let id: Int?
//    let title: String?
//    let image: String?
//    let contentType: String?
//    let subTitles: [String]?
//    let contractName: String?
//    let entitlements: String?
//    let duration: String?
//    let provider: String?
//    let providerContentId: String?
//    let categoryType: String?
//    let secondsWatched: Int?
//    let durationInSeconds: Int?
//    let description: String?
//    let lastWatched: Int?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        image = try values.decodeIfPresent(String.self, forKey: .image)
//        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
//        subTitles = try values.decodeIfPresent([String].self, forKey: .subTitles)
//        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
//        entitlements = try values.decodeIfPresent(String.self, forKey: .entitlements)
//        duration = try values.decodeIfPresent(String.self, forKey: .duration)
//        provider = try values.decodeIfPresent(String.self, forKey: .provider)
//        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
//        categoryType = try values.decodeIfPresent(String.self, forKey: .categoryType)
//        secondsWatched = try values.decodeIfPresent(Int.self, forKey: .secondsWatched)
//        durationInSeconds = try values.decodeIfPresent(Int.self, forKey: .durationInSeconds)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        lastWatched = try values.decodeIfPresent(Int.self, forKey: .lastWatched)
//    }
//}

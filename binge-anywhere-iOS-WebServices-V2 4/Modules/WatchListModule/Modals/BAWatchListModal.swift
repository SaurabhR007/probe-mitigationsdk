//
//  BAWatchListModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAWatchListModal: Codable {
    let code: Int?
    let message: String?
    var data: WatchListData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(WatchListData.self, forKey: .data)
    }
}

struct WatchListData: Codable {
    let totalCount: Int?
    var continuePaging: Bool?
    var pagingState: String?
    var list: [BAContentListModel]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        continuePaging = try values.decodeIfPresent(Bool.self, forKey: .continuePaging)
        pagingState = try values.decodeIfPresent(String.self, forKey: .pagingState)
        list = try values.decodeIfPresent([BAContentListModel].self, forKey: .list)
    }

}

//// Code Commented as BAContentListModel is now in use however if any major issues came then just need to uncomment this line and change BAContentListModel to this content list model
//struct WatchListContentList: Codable {
//    let id: Int?
//    let contentId: Int?
//    let contentType: String?
//    let title: String?
//    let description: String?
//    let image: String?
//    let subTitles: [String]?
//    let contractName: String?
//    let entitlements: [String]?
//    let airedDate: String?
//    let secondsWatched: Int?
//    let durationInSeconds: Int?
//    let provider: String?
//    let providerContentId: String?
//    let categoryType: String?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        contentId = try values.decodeIfPresent(Int.self, forKey: .contentId)
//        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        image = try values.decodeIfPresent(String.self, forKey: .image)
//        subTitles = try values.decodeIfPresent([String].self, forKey: .subTitles)
//        contractName = try values.decodeIfPresent(String.self, forKey: .contractName)
//        entitlements = try values.decodeIfPresent([String].self, forKey: .entitlements)
//        airedDate = try values.decodeIfPresent(String.self, forKey: .airedDate)
//        secondsWatched = try values.decodeIfPresent(Int.self, forKey: .secondsWatched)
//        durationInSeconds = try values.decodeIfPresent(Int.self, forKey: .durationInSeconds)
//        provider = try values.decodeIfPresent(String.self, forKey: .provider)
//        providerContentId = try values.decodeIfPresent(String.self, forKey: .providerContentId)
//        categoryType = try values.decodeIfPresent(String.self, forKey: .categoryType)
//    }
//}

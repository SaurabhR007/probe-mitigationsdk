//
//  BABrowseByDetailModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BABrowseByDetailModal: Codable {
    var data: BrowseByDetailData?
    let status: Int?
    let code: Int?
    let message: String?
    let timestamp: Int?
    let error: String?
    let statusCode: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(BrowseByDetailData.self, forKey: .data)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        timestamp = try values.decodeIfPresent(Int.self, forKey: .timestamp)
        error = try values.decodeIfPresent(String.self, forKey: .error)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
    }
}

struct BrowseByDetailData: Codable {
    let id: Int?
    let title: String?
    let sectionType: String?
    let layoutType: String?
    let totalCount: Int?
    let itemCount: Int?
    let totalSearchCount: Int?
    let maxCount: Int?
    var contentList: [BAContentListModel]?
    var continuePaging: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        sectionType = try values.decodeIfPresent(String.self, forKey: .sectionType)
        layoutType = try values.decodeIfPresent(String.self, forKey: .layoutType)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        contentList = try values.decodeIfPresent([BAContentListModel].self, forKey: .contentList)
        continuePaging = try values.decodeIfPresent(Bool.self, forKey: .continuePaging)
        itemCount = try values.decodeIfPresent(Int.self, forKey: .itemCount)
        maxCount = try values.decodeIfPresent(Int.self, forKey: .maxCount)
        totalSearchCount = try values.decodeIfPresent(Int.self, forKey: .totalSearchCount)
    }
}


//
//  BABrowseByModuleDetailRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BABrowseByModuleDetailScreenRepo {
    var apiEndPoint: String {get set}
    func getBrowseByDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BABrowseByDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BABrowseByModuleDetailRepo: BABrowseByModuleDetailScreenRepo {
    var apiEndPoint: String = ""
    func getBrowseByDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BABrowseByDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        // TODO: Remove once kong is setup
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "deviceType": "IOS", "deviceId": kDeviceId , "platform": "BINGE_ANYWHERE"]
        let target = ServiceRequestDetail.init(endpoint: .getBrowseByDetail(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BABrowseByDetailModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

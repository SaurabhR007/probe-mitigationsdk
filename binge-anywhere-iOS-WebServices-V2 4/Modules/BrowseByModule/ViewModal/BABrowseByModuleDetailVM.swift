//
//  BABrowseByModuleDetailVM.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BABrowseByModuleDetailVMProtocol {
    func getBrowseByDetail(completion: @escaping(Bool, String?, Bool) -> Void)
    var browseByData: BABrowseByDetailModal? {get set}
}

class BABrowseByModuleDetailVM: BABrowseByModuleDetailVMProtocol {

    var requestToken: ServiceCancellable?
    var repo: BABrowseByModuleDetailScreenRepo
    var browseByData: BABrowseByDetailModal?
    var endPoint: String = ""
    var genre: String?
    var homePageName: String = ""
    var language: String?
    var intent: String?
    var pageLimit = 20
    var pageOffset = 1
    var apiParams = APIParams()
    var totalSearchDataCount = 0
    var nextPageAvailable = false

    init(repo: BABrowseByModuleDetailScreenRepo, endPoint: String, intent: String, genre: String?, language: String?, pageName: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.genre = genre
        self.language = language
        self.intent = intent
        self.homePageName = pageName
    }

    func getBrowseByDetail(completion: @escaping(Bool, String?, Bool) -> Void) {
        if intent == "GENRE" {
            self.apiParams["filterGenre"] = [self.genre]
        } else if intent == "LANGUAGE" {
            self.apiParams["filterLanguage"] = [self.language]
        } else {
            if self.genre != nil {
                self.apiParams["filterGenre"] = [self.genre]
            }
            if self.language != nil {
                self.apiParams["filterLanguage"] = [self.language]
            }
        }
        self.apiParams["pageName"] = self.homePageName
        self.apiParams["pageNumber"] = pageOffset
        
        self.requestToken = self.repo.getBrowseByDetail(apiParams: self.apiParams, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if _configData.parsed.data?.contentList?.isEmpty ?? true {
                            self.browseByData = _configData.parsed
                            self.totalSearchDataCount = _configData.parsed.data?.contentList?.count ?? 0
                        } else {
                            if self.pageOffset == 1 {
                                self.browseByData = _configData.parsed
                                self.totalSearchDataCount = _configData.parsed.data?.contentList?.count ?? 0
                            } else {
                                self.browseByData?.data?.contentList?.append(contentsOf: ((_configData.parsed.data?.contentList)!))
                                self.totalSearchDataCount += _configData.parsed.data?.contentList?.count ?? 0
//                                if self.totalSearchDataCount == self.browseByData?.data?.totalSearchCount {
//                                   self.browseByData?.data?.continuePaging = false
//                                } else {
//                                    self.browseByData?.data?.continuePaging = true
//                                }
                            }
                        }
                        
                        if let _ = self.browseByData?.data {
                            if self.totalSearchDataCount == self.browseByData?.data?.totalSearchCount {
                                self.nextPageAvailable = false
                                self.browseByData?.data?.continuePaging = false
                            } else {
                                self.pageOffset += 1
                                self.nextPageAvailable = true
                                self.browseByData?.data?.continuePaging = true
                            }
                            if let data = self.browseByData?.data?.contentList {
                                self.browseByData?.data?.contentList = UtilityFunction.shared.filterTAData(data, false)
                            }
                        }
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

//
//  BAWatchLearnActionRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 08/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAWatchLearnActionScreenRepo {
    var apiEndPoint: String {get set}
    func makeWatchLearnActionCall(apiParams: APIParams, completion: @escaping(APIServicResult<BALearnActionModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAWatchLearnActionRepo: BAWatchLearnActionScreenRepo {
    var apiEndPoint: String = ""
    func makeWatchLearnActionCall(apiParams: APIParams, completion: @escaping(APIServicResult<BALearnActionModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
        let SID = BAKeychainManager().sId
        let PID = BAKeychainManager().profileId

        let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "\(accessToken)", "profileId": "\(PID)", "platform": "binge_anywhere_ios", "subscriberid": "\(SID)", "provider": "TATASKY"]
        let target = ServiceRequestDetail.init(endpoint: .laWatchAction(param: apiParams, header: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BALearnActionModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

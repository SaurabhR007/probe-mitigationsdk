//
//  BAWatchLearnActionViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 08/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAWatchLearnActionViewModalProtocol {
    func makeWatchLearnActionCall(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAWatchLearnActionViewModal: BAWatchLearnActionViewModalProtocol {
    
    var requestToken: ServiceCancellable?
    var repo: BAWatchLearnActionScreenRepo
    var endPoint: String = ""
    var apiParams = APIParams()

    init(repo: BAWatchLearnActionScreenRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }

    func makeWatchLearnActionCall(completion: @escaping(Bool, String?, Bool) -> Void) {
        self.requestToken = self.repo.makeWatchLearnActionCall(apiParams: self.apiParams, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if _configData.serviceResponse.statusCode == 200 {
                    print("WATCH Learn Action trigerred")
                    completion(true, "Learn Action Trigrred", false)
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            }
        })
    }
}

//
//  BAViewCountRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 30/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
protocol BAViewCountScreenRepo {
    var apiEndPoint: String {get set}
    func makeViewCountActionCall(apiParams: APIParams, completion: @escaping(APIServicResult<BALearnActionModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAViewCountRepo: BAViewCountScreenRepo {
    var apiEndPoint: String = ""
    func makeViewCountActionCall(apiParams: APIParams, completion: @escaping(APIServicResult<BALearnActionModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
        let SID = BAKeychainManager().sId
        let PID = BAKeychainManager().profileId

        let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "\(accessToken)", "profileId": "\(PID)", "platform": "dongle", "subscriberid": "\(SID)", "provider": "TATASKY"]
        let target = ServiceRequestDetail.init(endpoint: .laFavouriteAction(param: apiParams, header: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BALearnActionModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

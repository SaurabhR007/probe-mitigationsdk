//
//  BAViewCountViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 30/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
protocol BAViewCountViewModalProtocol {
    func makeViewCountActionCall(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAViewCountViewModal: BAViewCountViewModalProtocol {

    var requestToken: ServiceCancellable?
    var repo: BAViewCountScreenRepo
    var endPoint: String = ""
    var contentTypes: String = ""
    var contentId: String = ""
    var apiParams = APIParams()

    init(repo: BAViewCountScreenRepo, endPoint: String, contentType: String, id: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.contentTypes = contentType
        self.contentId = id
    }

    func makeViewCountActionCall(completion: @escaping(Bool, String?, Bool) -> Void) {
        self.requestToken = self.repo.makeViewCountActionCall(apiParams: self.apiParams, completion: { (configData, error) in
            self.requestToken = nil
            self.apiParams["contentType"] = self.contentTypes
            self.apiParams["id"] = self.contentId
            self.apiParams["subscriberId"] = BAKeychainManager().sId
            self.apiParams["profileId"] = BAKeychainManager().profileId
            if let _configData = configData {
                if _configData.serviceResponse.statusCode == 200 {
                    print("View Count Action Triggered")
                    completion(true, "View Count Trigrred", false)
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            }
        })
    }
}

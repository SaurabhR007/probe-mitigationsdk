//
//  BALearnActionModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 08/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

class BALearnActionModal: Codable {
    let code: Int?
    let status: String?
    let message: String?
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
}


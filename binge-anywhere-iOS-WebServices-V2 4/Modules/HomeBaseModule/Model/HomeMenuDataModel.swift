//
//  HomeMenuDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 27/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

struct HomeMenuDataModel: Codable {
    let code: Int?
    let message: String?
    let data: HomeMenuData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(HomeMenuData.self, forKey: .data)
    }
}

struct HomeMenuData: Codable {
    let items: [HomeTabItems]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([HomeTabItems].self, forKey: .items)
    }
}

struct HomeTabItems: Codable {
    let pageName: String?
    let pageType: String?
    let position: Int?
    let searchPageName: String?
    let subPage: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pageName = try values.decodeIfPresent(String.self, forKey: .pageName)
        pageType = try values.decodeIfPresent(String.self, forKey: .pageType)
        position = try values.decodeIfPresent(Int.self, forKey: .position)
        searchPageName = try values.decodeIfPresent(String.self, forKey: .searchPageName)
        subPage = try values.decodeIfPresent(Bool.self, forKey: .subPage)
    }
}

//
//  ExtensionHomeLandingViewController.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 15/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

extension HomeLandingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let labelWidth = stringWidth(string: titleArr[indexPath.row].title, font: .skyTextFont(.regular, size: 16))
        let count = CGFloat(titleArr.count) // To fix the width of label for having max 4 tabs on screen hard code the count to value 4
        let screenWidth = (collectionView.frame.width / count)
        var cellWidth: CGFloat = 0.0
        if (labelWidth > screenWidth) {
            cellWidth = labelWidth + 20.0
        } else {
            if labelWidth < (screenWidth - 20) {
                cellWidth = screenWidth
            } else {
                cellWidth = labelWidth + 20.0
            }
        }
        return CGSize(width: cellWidth, height: 30.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kBAHomeTabMenuHeaderCollectionViewCell, for: indexPath) as! HomeTabMenuCollectionViewCell
        cell.backgroundColor = .BAdarkBlue1Color
        cell.configureSeasonsList(titleArr[indexPath.row].title, titleArr[indexPath.row].isSelected)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        removeFirstSelectedMenu(collectionView, indexPath)
        markAllDeselect()
        if let cell = collectionView.cellForItem(at: indexPath) as? HomeTabMenuCollectionViewCell {
            _ = cell.frame
            print("This is selected \(indexPath.row)")
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homePageView.rawValue, properties: [MixpanelConstants.ParamName.name.rawValue: titleArr[indexPath.row]])
            titleArr[indexPath.row].isSelected = true
            BAKeychainManager().pageNameOnHome = titleArr[indexPath.row].searchPageName
            cell.updateSelectedCell(true)
            if let _pageMenu = self.pageMenu {
                if indexPath.row == 0 {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "makeContinueWatchCall"), object: nil)
                }
                _pageMenu.moveToPage(indexPath.row)
            }
            scrollToSelectedPage(indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        UtilityFunction.shared.performTaskInMainQueue {
            collectionView.reloadItems(at: [indexPath])
        }
    }
    
    func mixPanelEvent(_ index: Int) {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homePageView.rawValue, properties: [MixpanelConstants.ParamName.name.rawValue: titleArr[index].title])
    }
    
    // to deselect first selected header menu so that select and deselect logic will work.
    func removeFirstSelectedMenu(_ collectionView: UICollectionView, _ indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? HomeTabMenuCollectionViewCell {
            print(cell)
            UtilityFunction.shared.performTaskInMainQueue {
                let _indexPath = IndexPath(item: 0, section: 0)
                collectionView.reloadItems(at: [_indexPath])
            }
        }
    }
    
    func scrollToNextCell(_ cellFrame: CGRect) {
        let contentOffset = menuHeaderCollectionView.contentOffset
        menuHeaderCollectionView.scrollRectToVisible(CGRect(cellFrame.origin.x + cellFrame.width, contentOffset.y, cellFrame.width, cellFrame.height), animated: true)
    }
    
    
    func scrollToSelectedPage(_ idx: Int) {
        let index = IndexPath.init(row: idx, section: 0)
        UtilityFunction.shared.performTaskInMainQueue {
            self.menuHeaderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            if let _ = self.menuHeaderCollectionView.cellForItem(at: index) as? HomeTabMenuCollectionViewCell {
                self.menuHeaderCollectionView.selectItem(at: index, animated: true, scrollPosition: .centeredHorizontally)
            }
        }
    }
    
    
    func markAllDeselect() {
        for i in 0..<titleArr.count {
            titleArr[i].isSelected = false
        }
    }
}

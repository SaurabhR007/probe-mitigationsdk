//
//  HomeLandingViewController.swift
//  BingeAnywhere
//
//  Created by Shivam on 20/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

class HomeLandingViewController: BaseViewController, CAPSPageMenuDelegate {
    
    var homeBaseViewModel: HomeBaseViewModelProtocol?
    @IBOutlet weak var menuHeaderCollectionView: UICollectionView!
    var titleArr = [PageMenuTitle]()
    var pageMenuTitleArr = [String]()
    var searchPageNameArr = [String]()
    var firstTabSelected = false
    var interSpace: CGFloat = 0.0
    let disPatchGroup = DispatchGroup()
    var subscriptionVM: PackSubscriptionVM?
    var isPackSubscriptionCalled = false
    var isUserProfileCalled = false
    var userDetailVM: BAUserDetailViewModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionViewCell()
        super.configureNavigationBar(with: .centreImage, nil, nil, nil)
        self.view.backgroundColor = .BAdarkBlueBackground
        reCheckVM()
//        fetchSubscriptionDetails()
        // Do any additional setup after loading the view.
        
        if BAKeychainManager().mobileNumber.length > 5{
            
            ProbeInterface.registerForMitigationSession(rmn : BAKeychainManager().mobileNumber, enableTestLogs: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        super.configureNavigationBar(with: .centreImage, nil, nil, nil)
        self.tabBarController?.tabBar.isHidden = false
        self.fetchSubscriptionDetails()
        if let _pageMenu = self.pageMenu {
            // to refresh continue watch only for first page on home tab
            let index = _pageMenu.currentPageIndex
            if index == 0 {
                if !firstTabSelected {
                    (_pageMenu.controllerArray[index] as? HomeVC)?.viewWillAppear(true)
                } else {
                    firstTabSelected = false
                    (_pageMenu.controllerArray[index] as? HomeVC)?.viewWillAppear(true)
                    //(_pageMenu.controllerArray[index] as? HomeVC)?.refreshHomeData()
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        checkForLogOutState()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    private func reCheckVM() {
        if self.homeBaseViewModel == nil {
            self.homeBaseViewModel = HomeBaseVM(repo: HomeRepo())
            self.callForHomeMenu()
        }
    }
    
    @objc func backButtonTapped() {
        kAppDelegate.createLoginRootView()
    }
    
    func registerCollectionViewCell() {
        menuHeaderCollectionView.backgroundColor = .BAdarkBlueBackground
        menuHeaderCollectionView.setViewShadow(opacity: 0.3, blur: 6.0, shadowOffsetWidth: 0.0, shadowOffsetHeight: 1.0)
        menuHeaderCollectionView.register(UINib(nibName: kBAHomeTabMenuHeaderCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kBAHomeTabMenuHeaderCollectionViewCell)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        if let _pageMenu = self.pageMenu {
            let currentIndex = _pageMenu.currentPageIndex
            if currentIndex == index {
                if let controller = controller as? HomeVC {
                    if !controller.isHomeDataCalled {
                        if BAReachAbility.isConnectedToNetwork() {
                            controller.reCheckVM()
                        } else {
                            noInternet()
                        }
                    }
                }
            }
        }
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("this is controller 1", controller)
    }
    
    private func openFSPopUp() {
        let subscriptionDetail = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
        let accountDetails = BAKeychainManager().acccountDetail
        let delaySeconds = 5.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delaySeconds) {
            if !(BAKeychainManager().isFSPopUpShown) {
                if subscriptionDetail?.fsEligibility ?? false &&
                    !(accountDetails?.fsTaken ?? true) &&
                    subscriptionDetail?.ocsFlag?.caseInsensitiveCompare("Y") == .orderedSame {
                    self.popupForFireTv() { (flag) in
                        BAKeychainManager().isFSPopUpShown = true
                        if flag {
                            let fireTvVC:FireTvViewController  = UIStoryboard.loadViewController(storyBoardName: "FireTV", identifierVC: "FireTvViewController", type: FireTvViewController.self)
                            BAKeychainManager().isFSPopUpShown = true
                            self.navigationController?.pushViewController(fireTvVC, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func fetchSubscriptionDetails() {
        subscriptionVM = PackSubscriptionVM()
        getSubscription()
        getUserDetail()
    }
    
    private func getUserDetail() {
        self.userDetailVM = BAUserDetailViewModal(BAUserDetailRepo())
        fetchUserDetails()
    }
    
    func getSubscription() {
        if BAReachAbility.isConnectedToNetwork() && !isPackSubscriptionCalled {
            if subscriptionVM != nil {
                subscriptionVM?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                    self.isPackSubscriptionCalled = true
                    if flagValue {
                        self.openFSPopUp()
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError("", title: message)
                    }
                })
            }
        } else {
        }
    }
    
    
    func fetchUserDetails() {
        if BAReachAbility.isConnectedToNetwork() && !isUserProfileCalled {
            if userDetailVM != nil {
                userDetailVM?.getUserDetail(completion: {(flagValue, message, isApiError) in
                    self.isUserProfileCalled = true
                    if flagValue {
                        let notifStatus = self.userDetailVM?.userDetail?.watchNotification ?? true
                        MoengageManager.shared.setMoengageNotificationReceiverFlag(!notifStatus)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
        }
    }
    
    func callForHomeMenu() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            showActivityIndicator(isUserInteractionEnabled: true)
            if let homeVM = homeBaseViewModel {
                homeVM.fetchHomeMenuData {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.configureHomePageView(homeVM.tabItem!)
                    } else if isApiError {
                        if message == "ignoreCase" {
                            self.noInternet() {
                                self.callForHomeMenu()
                            }
                        } else {
                           self.apiError(message, title: kSomethingWentWrong)
                        }
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } 
        } else {
            noInternet()
        }
    }
    
    func configureHomePageView(_ arr: [HomeTabItems]) {
        var controllerArray: [UIViewController] = []
        pageMenuTitleArr = arr.map {($0.pageName ?? "")} // ["Home", "Movies", "TVShows", "Kids" ]//
        searchPageNameArr = arr.map{($0.searchPageName ?? "")}
        createHomeTitleArray()
        for item in arr {
            let controller: HomeVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.homeTab.fileName, identifierVC: ViewControllers.homeVc.rawValue, type: HomeVC.self)
            controller.title = item.pageName
            controller.parentNavigationController = self.navigationController
            controller.pageType = item.pageType
            controller.pageName = item.pageName
            controller.disPatchGroup = disPatchGroup
            controller.view.backgroundColor = .BAdarkBlueBackground
            controllerArray.append(controller)
        }
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .centerMenuItems(true),
            .scrollMenuBackgroundColor(.BAdarkBlueBackground),
            .viewBackgroundColor(.BAdarkBlueBackground),
            .selectionIndicatorColor(.BABlueColor),
            .selectedMenuItemLabelColor(.BAwhite),
            .unselectedMenuItemLabelColor(.BAlightBlueGrey),
            .menuItemFont(UIFont.skyTextFont(.medium, size: 15 * screenScaleFactorForWidth)),
            .menuHeight(0.0),
            //.menuMargin(40.0 * screenScaleFactorForWidth),
            .selectionIndicatorHeight(3.0),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .addBottomMenuHairline(true),
            .bottomMenuHairlineColor(.clear),
            .scrollAnimationDurationOnMenuItemTap(150)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 30.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        self.pageMenu?.delegate = self
        view.addSubview(pageMenu!.view)
        self.view.bringSubviewToFront(menuHeaderCollectionView)
        
        /*
         
         // to use page menu default header just remove this commentted line and comment above line with y axis value variation.
         
         pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
         view.addSubview(pageMenu!.view)
         
         
         // Uncomment these lines and give menuHeight default value of 30.0
         //.menuHeight(30.0),
         //.menuMargin(40.0 * screenScaleFactorForWidth),
         
         
         if we do so what is written above then the collection view used for header on home landing page will be removed by default page menu header
         */
    }
    
    override func retryButtonAction() {
        self.callForHomeMenu()
    }
    
    func manageInterSpace() {
        var totalMenuLabelWidth: CGFloat = 0.0
        let count = CGFloat(titleArr.count)
        var padding: CGFloat = 0.0
        
        for item in titleArr {
            let labelWidth = stringWidth(string: item.title, font: .skyTextFont(.regular, size: 16))
            totalMenuLabelWidth += labelWidth
        }
        padding += 20.0*count
        totalMenuLabelWidth += padding
        if totalMenuLabelWidth < kDeviceWidth {
            let widthDiff = kDeviceWidth - totalMenuLabelWidth
            interSpace = widthDiff/count
        }
        menuHeaderCollectionView.reloadData()
    }
    
    func stringWidth(string: String, font: UIFont) -> CGFloat {
        let attributes = NSDictionary(object: font, forKey: NSAttributedString.Key.font as NSCopying)
        let sizeOfText = string.size(withAttributes: (attributes as! [NSAttributedString.Key: AnyObject]))
        return sizeOfText.width
    }
    
    func createHomeTitleArray() {
        titleArr = []
        for i in 0..<pageMenuTitleArr.count {
            var pageMenu = PageMenuTitle()
            pageMenu.title = pageMenuTitleArr[i]
            pageMenu.searchPageName = searchPageNameArr[i]
            pageMenu.isSelected = (i == 0)
            titleArr.append(pageMenu)
        }
        menuHeaderCollectionView.reloadData()
        //        manageInterSpace()
    }
}

struct PageMenuTitle {
    var title: String = ""
    var searchPageName: String = ""
    var isSelected: Bool = false
}

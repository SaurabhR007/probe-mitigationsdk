//
//  HomeBaseVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 27/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

protocol HomeBaseViewModelProtocol {
    var tabItem: [HomeTabItems]? {get set}
    func fetchHomeMenuData(completion: @escaping(Bool, String?, Bool) -> Void)
}

class HomeBaseVM: HomeBaseViewModelProtocol {
    var tabItem: [HomeTabItems]?
    var requestToken: ServiceCancellable?
    let repo: HomeRepoType
    init(repo: HomeRepoType) {
        self.repo = repo
    }

    func fetchHomeMenuData(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo.fetchHomeTopMenu(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        if let arr = _configData.parsed.data?.items {
                            self.tabItem = arr
                            completion(true, nil, false)
                        } else {
                            completion(false, "Some Error Occurred", false)
                        }
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                if error.error.urlError?.code.rawValue == -1009 {
                    completion(false, "ignoreCase", true)
                } else if error.error.urlError?.code.rawValue == -1018 {
                    completion(false, "ignoreCase", true)
                    ///Ignore this case to avoid Internation roaming not allowed popup.
                } else {
                    completion(false, error.error.localizedDescription, true)
                }
                
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

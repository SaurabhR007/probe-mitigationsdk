//
//  HomeBaseDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam on 27/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation

private enum Constants {
    enum ApiPaths {
        static let homeMenu = "pages/BINGE_ANYWHERE"
    }
}

protocol HomeRepoType {
    func fetchHomeTopMenu(completion: @escaping (APIServicResult<HomeMenuDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct HomeRepo: HomeRepoType {
    func fetchHomeTopMenu(completion: @escaping (APIServicResult<HomeMenuDataModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiEndUrl = Constants.ApiPaths.homeMenu
        //let apiHeader: APIHeaders = ["rule": "BA"]
        let apiParams = APIParams()
        let target = ServiceRequestDetail.init(endpoint: .homeScreenData(param: apiParams, headers: nil, endUrlString: apiEndUrl))
        return APIService().request(target) { (response: APIResult<APIServicResult<HomeMenuDataModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

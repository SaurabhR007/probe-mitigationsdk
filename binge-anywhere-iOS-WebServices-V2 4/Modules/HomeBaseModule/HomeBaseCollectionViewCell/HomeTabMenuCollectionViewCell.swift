//
//  HomeTabMenuCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 15/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol HomeTabMenuCollectionDelegate: class {
    func switchToPage(index: Int)
}

class HomeTabMenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet weak var selectionIndicator: UIView!
    weak var delegate: HomeTabMenuCollectionDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        // Initialization code
    }

    func configureSeasonsList(_ title: String, _ isSelected: Bool) {
        self.menuTitle.textColor = isSelected ? .BAwhite : .BAlightBlueGrey
        self.selectionIndicator.isHidden = !isSelected
        menuTitle.text = title
    }

    func updateSelectedCell(_ isSelected: Bool) {
        self.menuTitle.textColor = isSelected ? .BAwhite : .BAlightBlueGrey
        self.selectionIndicator.isHidden = !isSelected
    }
}

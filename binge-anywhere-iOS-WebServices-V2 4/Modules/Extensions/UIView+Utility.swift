//
//  UIView+Utility.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 14/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import AVKit

extension UIView {
    func fillParentView( edgeIndest: UIEdgeInsets = .zero) {
        guard let parent = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leftAnchor.constraint(equalTo: parent.leftAnchor, constant: edgeIndest.left),
            rightAnchor.constraint(equalTo: parent.rightAnchor, constant: -edgeIndest.right),
            topAnchor.constraint(equalTo: parent.topAnchor, constant: -edgeIndest.top),
            bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: -edgeIndest.bottom)
        ])
    }

    func zoomParentView( edgeIndest: UIEdgeInsets = .zero) {
        guard let parent = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.deactivate(parent.constraints)
        NSLayoutConstraint.activate([
            leftAnchor.constraint(equalTo: parent.leftAnchor, constant: edgeIndest.left),
            rightAnchor.constraint(equalTo: parent.rightAnchor, constant: -edgeIndest.right),
            topAnchor.constraint(equalTo: parent.topAnchor, constant: -edgeIndest.top),
            bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: -edgeIndest.bottom)
        ])
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

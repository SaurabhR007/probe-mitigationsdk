//
//  BAContactUsViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 21/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import MessageUI

class BAContactUsViewController: BaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var supportAssistanceLabel: CustomLabel!
    @IBOutlet weak var requestCallBackBtn: UIButton!
    @IBOutlet weak var requestCallBackButton: UIButton!
    @IBOutlet weak var requestCallBackLabel: UILabel!
    @IBOutlet weak var requestCallBackView: UIView!
    @IBOutlet weak var supportStaffLabel: UILabel!
    private var isRequestCallBackGenerated = false
    var moveToFaq = false
    var termsTextRange = NSMakeRange(0, 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        //configureRequestCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.contactUs.rawValue)
        configureSupportLabel()
        if moveToFaq {
            navigateToFaq()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        moveToFaq = false
    }
    
    func configureSupportLabel() {
        let labelString = "For any further support, you can reach out to us at help@tatasky.com"
        let attributedText = NSMutableAttributedString(string: labelString)
        let substr = "help@tatasky.com"
        let textRange = attributedText.mutableString.range(of: substr, options: NSString.CompareOptions.caseInsensitive)
        termsTextRange = textRange
        let wholerange = attributedText.mutableString.range(of: labelString)
        let textColor: UIColor = .BABlueColor
        let underLineStyle = NSUnderlineStyle.single.rawValue
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        attributedText.addAttribute(.foregroundColor, value: textColor, range: textRange)
        attributedText.addAttribute(.underlineColor, value: textColor, range: textRange)
        attributedText.addAttribute(.underlineStyle, value: underLineStyle, range: textRange)
        attributedText.addAttribute(.font, value: UIFont.skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), range: wholerange)
        //attributedText.addAttribute(.link, value: substr, range: textRange)
        supportAssistanceLabel.attributedText = attributedText
        supportAssistanceLabel.isUserInteractionEnabled = true
        supportAssistanceLabel.addGestureRecognizer(tap)
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        if sender.didTapAttributedTextInLabel(label: supportAssistanceLabel, inRange: termsTextRange) {
            print("Tapped targetRange1")
            if BAReachAbility.isConnectedToNetwork() {
                // TODO: This needs to be Dynamic as per Client Inputs. Needs to be changed
                let recipientEmail = "help@tatasky.com"
                let subject = "Tata Sky Support email support"
                let body = "This is a sample Emai to test for TataSky suppot team."
                
                if MFMailComposeViewController.canSendMail() {
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.emailUs.rawValue)
                    let composeVC = MFMailComposeViewController()
                    composeVC.mailComposeDelegate = self
                    composeVC.setToRecipients(["help@tatasky.com"])
                    self.present(composeVC, animated: true, completion: nil)
                } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                    UIApplication.shared.open(emailUrl)
                }
            } else {
                noInternet()
            }
        } else {
            print("Tapped none")
        }
        
        print("tap working")
    }
    
    func configureNavigation() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Contact Us")
    }
    
    func configureRequestCall() {
        requestCallBackBtn.isUserInteractionEnabled = true
        requestCallBackButton.isHidden = true
        supportStaffLabel.isHidden = true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func requestCallBackButtonAction(_ sender: Any) {
        apiError("Page under construction", title: "Soon!")
        /*
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.raiseRequest.rawValue)
        //            Mixpanel.mainInstance().track(event: "RAISE-REQUEST")
        requestCallBackBtn.isUserInteractionEnabled = false
        requestCallBackButton.isHidden = false
        supportStaffLabel.isHidden = false
        requestCallBackLabel.text = "Callback Requested"
        requestCallBackButton.setImage(UIImage(named: "callbackrequested"), for: .normal)
        requestCallBackView.backgroundColor = .BASelectedBlue
        */
    }
    
    @IBAction func faqButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            navigateToFaq()
        } else {
            noInternet()
        }
    }
    
    func navigateToFaq() {
        let faqVC: BAFAQViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.faqViewController.rawValue, type: BAFAQViewController.self)
        self.navigationController?.pushViewController(faqVC, animated: false)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.faqView.rawValue, properties: [MixpanelConstants.ParamName.railPosition.rawValue: "1"])
    }
    
    @IBAction func emailButtonAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            // TODO: This needs to be Dynamic as per Client Inputs. Needs to be changed
            let recipientEmail = "test@email.com"
            let subject = "Tata Sky Support email support"
            let body = "This is a sample Emai to test for TataSky suppot team."
            
            if MFMailComposeViewController.canSendMail() {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.emailUs.rawValue)
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                composeVC.setToRecipients(["test@email.com"])
                self.present(composeVC, animated: true, completion: nil)
            } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                UIApplication.shared.open(emailUrl)
            }
        } else {
            noInternet()
        }
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.chatWithTS.rawValue)
        apiError("Page under construction", title: "Soon!")
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: Open the external Email Urls if On Emai App is installed in the device.
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        return defaultUrl
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
}

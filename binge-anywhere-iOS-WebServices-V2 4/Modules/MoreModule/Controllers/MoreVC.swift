//
//  MoreVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class MoreVC: BaseViewController {
    
    @IBOutlet weak var autoPlayTrailer: UISwitch!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var logoutButton: CustomButton!
    
    var signOutVM: BASignOutViewModal?
    var parentNavigationController: UINavigationController?
    var navigateToFAQ = false
    var notificationSettingsVM: BANotificationSettingsViewModal?
    var userDetailVM: BAUserDetailViewModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.configureNavigationBar(with: .centerTitle, nil, nil, self, "More")
        // Do any additional setup after loading the view. 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .BAdarkBlueBackground
        logoutButton.underline()
        appVersionLabel.text = "Version " + kAppVersion //getAppVersion()
        if navigateToFAQ {
            navigateToContactUS()
        }
        getUserDetail(isFromViewLoaded: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigateToFAQ = false
    }
    
    private func getUserDetail(isFromViewLoaded: Bool) {
        self.userDetailVM = BAUserDetailViewModal(BAUserDetailRepo())
        fetchUserDetails(isFromScreenLanding: isFromViewLoaded)
    }
    
    func fetchUserDetails(isFromScreenLanding: Bool) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: true)
            if userDetailVM != nil {
                userDetailVM?.getUserDetail(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.autoPlayTrailer.isOn = self.userDetailVM?.userDetail?.autoPlayTrailer ?? true
                        BAKeychainManager().isAutoPlayOn = self.userDetailVM?.userDetail?.autoPlayTrailer ?? true
                        if isFromScreenLanding {
                            
                        } else {
                            if BAKeychainManager().isAutoPlayOn {
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.autoPlayTrailer.rawValue, properties: [MixpanelConstants.ParamName.enabled.rawValue: "YES"])
                            } else {
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.autoPlayTrailer.rawValue, properties: [MixpanelConstants.ParamName.enabled.rawValue: "NO"])
                            }
                           
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.logoutButton.isUserInteractionEnabled = true
        }
        if BAReachAbility.isConnectedToNetwork() {
            if BAKeychainManager().customerName == kGuestUser {
                //showOkAlertWithCustomTitle(<#T##message: String##String#>, title: <#T##String#>)
                showAlertForLogout(message: klogoutConfirmation)
            } else {
                // Do Nothing
            }
        } else {
            noInternet()
        }
    }
    
    @IBAction func notificationSettingBtnAction(_ sender: Any) {
         //testAPiAlert()
        if BAReachAbility.isConnectedToNetwork() {
            let notificationSetting: NotificationSettingsVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.notificationSettings.rawValue, type: NotificationSettingsVC.self)
            self.navigationController?.pushViewController(notificationSetting, animated: true)
        } else {
            noInternet()
        }
    }
    
    @IBAction func autoPlayAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            updateNotificationSettings()
        } else {
            noInternet()
        }
    }
    
    @IBAction func termsBtnAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.terms.rawValue)
            let webViewVC: BAWebViewViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.webViewVC.rawValue, type: BAWebViewViewController.self)
            webViewVC.isPrivacyPolicy = false
            webViewVC.isFAQ = true
            self.navigationController?.pushViewController(webViewVC, animated: true)
        } else {
            noInternet()
        }
    }
    
    @IBAction func privacyPolicyBtnAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.privacyPolicy.rawValue)
            let webViewVC: BAWebViewViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.webViewVC.rawValue, type: BAWebViewViewController.self)
            webViewVC.isPrivacyPolicy = true
            webViewVC.isFAQ = false
            self.navigationController?.pushViewController(webViewVC, animated: true)
        } else {
            noInternet()
        }
    }
    
    @IBAction func contactUsBtnAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            navigateToContactUS()
        } else {
            noInternet()
        }
    }
    
    /*
    func testAPiAlert() {
        let url = URL(string: "https://tatasky-uat-tsmore-kong-proxy.videoready.tv/homescreen-client/exception")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                self.hideActivityIndicator()
                self.apiError(error.localizedDescription, title: "")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
                if response.statusCode == 500 {
                    self.apiError("Test API Failure", title: kSomethingWentWrong)
                }
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
                do {
                    // Do Nothing
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    */
    
    func navigateToContactUS() {
        let contactUsVC: BAContactUsViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.contactUsVC.rawValue, type: BAContactUsViewController.self)
        contactUsVC.moveToFaq = self.navigateToFAQ
        self.navigationController?.pushViewController(contactUsVC, animated: false)
    }
    
    func updateNotificationSettings() {
        notificationSettingsVM = BANotificationSettingsViewModal(repo: BANotificationSettingsRepo(), endPoint: "status", baId: BAKeychainManager().baId, settingsType: "AUTO_PLAY_TRAILER")
        configureAutoPlaySwitch()
    }
    
    func configureAutoPlaySwitch() {
        showActivityIndicator(isUserInteractionEnabled: true)
        if let dataModel = notificationSettingsVM {
            dataModel.notificationSettingsCall {(flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.autoPlayTrailer.rawValue, properties: [MixpanelConstants.ParamName.enabled.rawValue: BAKeychainManager().isAutoPlayOn])
                    self.getUserDetail(isFromViewLoaded: false)
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        } else {
            self.hideActivityIndicator()
        }
    }
    
    // MARK: - Alert For Logout
    func showAlert(message: String?) {
        self.hideActivityIndicator()
        DispatchQueue.main.async { [weak self] in
			AlertController.initialization().showAlert(.doubleButton(.yes, .no, .withTopImage(.alert), .bingeAnywhere, .withMessage(message ?? "Logout"), .hidden, .hidden)) { (isOkay) in
				if isOkay {
					self?.signOut()
				}
			}
        }
    }
    
    func showAlertForLogout(message: String?) {
        self.hideActivityIndicator()
        DispatchQueue.main.async { [weak self] in
            AlertController.initialization().showAlert(.doubleButton(.yes, .no, .withTopImage(.alert), .withTitle("Sign Out"), .withMessage(message ?? "Logout"), .hidden, .hidden)) { (isOkay) in
                if isOkay {
                    self?.signOut()
                }
            }
        }
    }
    
    func signOut() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            signOutVM = BASignOutViewModal(repo: BASignOutRepo())
            if let signOutVM = signOutVM {
                signOutVM.signOut {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        BAKeychainManager().isFSPopUpShown = false
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
			noInternet()
        }
    }
}

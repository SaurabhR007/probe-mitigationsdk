//
//  BAWebViewViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 21/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import WebKit
import ARSLineProgress

class BAWebViewViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!

    var isPrivacyPolicy: Bool?
    var isFAQ: Bool?
    var isPushedView = false

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.isOpaque = false
        webView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadView), name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .BAdarkBlueBackground
        configureWebView()
        configureNavigation()
    }

    func configureNavigation() {
        if isPrivacyPolicy ?? false {
            super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Privacy Policy")
        } else if isFAQ ?? false {
            super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Terms & Conditions")
        } else {
            super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "License Agreement")
        }
    }

    func configureWebView() {
        webView.backgroundColor = .BAdarkBlueBackground
        if isPrivacyPolicy ?? false {
            let url = URL (string: BAConfigManager.shared.configModel?.data?.config?.url?.privacyPolicyUrlHybrid ?? "")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        } else if isFAQ ?? false {
            let url = URL (string: BAConfigManager.shared.configModel?.data?.config?.url?.termsConditionsUrlHybrid ?? "")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        } else {
            let url = URL (string: BAConfigManager.shared.configModel?.data?.config?.url?.eulaUrlHybrid ?? "")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        }
    }
    
    @objc func reloadView() {
        webView.backgroundColor = .BAdarkBlueBackground
        configureWebView()
        configureNavigation()
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        showActivityIndicator(isUserInteractionEnabled: true)
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        hideActivityIndicator()
        let scrollableSize = CGSize(width: view.frame.size.width, height: webView.scrollView.contentSize.height)
        self.webView?.scrollView.contentSize = scrollableSize
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = request.url, navigationType == .linkClicked else { return true }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        return false
    }

    @objc func backButtonAction() {
        if isPushedView && isFAQ ?? false {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else if isFAQ ?? false {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

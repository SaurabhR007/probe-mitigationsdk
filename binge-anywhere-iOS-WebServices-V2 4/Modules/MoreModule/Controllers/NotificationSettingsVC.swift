//
//  NotificationSettingsVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 04/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel

class NotificationSettingsVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var transactionalSwitch: UISwitch!
    @IBOutlet weak var watchSwitch: UISwitch!
    @IBOutlet weak var offersSwitch: UISwitch!
    @IBOutlet weak var updatesButton: UIButton!
    @IBOutlet weak var offersButton: UIButton!
    @IBOutlet weak var surveyButton: UIButton!

    var notificationSettingsVM: BANotificationSettingsViewModal?
    var userDetailVM: BAUserDetailViewModal?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.notificationSetting.rawValue)
       // Mixpanel.mainInstance().track(event: <#T##String?#>)
        configUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserDetail()
    }

    // MARK: - Functions
    func configUI() {
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "Notification Settings")
        self.view.backgroundColor = .BAdarkBlueBackground
    }

    // MARK: - Actions
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func getUserDetail() {
        self.userDetailVM = BAUserDetailViewModal(BAUserDetailRepo())
        fetchUserDetails()
    }
    
    func fetchUserDetails() {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: true)
            if userDetailVM != nil {
                userDetailVM?.getUserDetail(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.watchSwitch.isOn = self.userDetailVM?.userDetail?.watchNotification ?? true
                        MoengageManager.shared.setMoengageNotificationReceiverFlag(!self.watchSwitch.isOn)
//                        if self.userDetailVM?.userDetail?.watchNotification ?? true {
//                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.notificationSetting.rawValue, properties: [MixpanelConstants.ParamName.allowed.rawValue: "Yes"])
//                        } else {
//                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.notificationSetting.rawValue, properties: [MixpanelConstants.ParamName.allowed.rawValue: "No"])
//                        }
                        self.transactionalSwitch.isOn = self.userDetailVM?.userDetail?.transactionalNotification ?? true
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
//                {
//                self.fetchUserDetails()
//            }
        }
    }

    @IBAction func watchSwitchNotificationAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            updateNotificationSettings(notificationType: "WATCH_NOTIFICATION")
            if watchSwitch.isOn {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.notificationSetting.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: "YES"])
            } else {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.notificationSetting.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: "NO"])
            }
        } else {
            noInternet()
        }
    }

    @IBAction func offerSwitchNotificationAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            updateNotificationSettings(notificationType: "WATCH_NOTIFICATION")
            if offersSwitch.isOn {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.allowOfferNotificaion.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: MixpanelConstants.ParamValue.trueValue.rawValue ])
            } else {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.allowOfferNotificaion.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: MixpanelConstants.ParamValue.falseValue.rawValue ])
            }
        } else {
            noInternet()
        }
    }

    @IBAction func transactionNotificationAction(_ sender: Any) {
        if BAReachAbility.isConnectedToNetwork() {
            updateNotificationSettings(notificationType: "TRANSACTIONAL_NOTIFICATION")
            if transactionalSwitch.isOn {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.allowTransactionNotification.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: MixpanelConstants.ParamValue.trueValue.rawValue ])
            } else {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.allowTransactionNotification.rawValue, properties: [MixpanelConstants.ParamName.enableNotification.rawValue: MixpanelConstants.ParamValue.falseValue.rawValue ])
            }
        } else {
            noInternet()
        }
    }

    @IBAction func updatesButtonAction() {
        updatesButton.isSelected = !updatesButton.isSelected
    }

    @IBAction func offersButtonAction() {
        offersButton.isSelected = !offersButton.isSelected
    }

    @IBAction func surveyButtonAction() {
        surveyButton.isSelected = !surveyButton.isSelected
    }
    
    func  updateNotificationSettings(notificationType: String?) {
        notificationSettingsVM = BANotificationSettingsViewModal(repo: BANotificationSettingsRepo(), endPoint: "status", baId: BAKeychainManager().baId, settingsType: notificationType ?? "")
        configureSettingsSwitch(notificationType: notificationType)
    }
    
    func configureSettingsSwitch(notificationType: String?) {
        showActivityIndicator(isUserInteractionEnabled: false)
        if let dataModel = notificationSettingsVM {
            dataModel.notificationSettingsCall {(flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    self.getUserDetail()
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        } else {
            self.hideActivityIndicator()
        }
    }

}

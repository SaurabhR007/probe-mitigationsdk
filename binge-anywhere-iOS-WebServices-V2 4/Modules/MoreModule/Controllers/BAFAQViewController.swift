//
//  BAFAQViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 09/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAFAQViewController: BaseViewController {

    @IBOutlet weak var faqTableView: UITableView!
    
    var faqViewModal: BAFAQViewModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        conformDelegates()
        setUpUI()
        configureNavigation()
        getFAQQuestions()
        // Do any additional setup after loading the view.
    }
    
    private func conformDelegates() {
        faqTableView.dataSource = self
        faqTableView.delegate = self
    }
    
    private func setUpUI() {
        faqTableView.backgroundColor = .BAdarkBlueBackground
        faqTableView.rowHeight = UITableView.automaticDimension
        faqTableView.estimatedRowHeight = 100
        faqTableView.separatorStyle = .none
        registerCells()
    }
    
    private func getFAQQuestions() {
        self.faqViewModal = BAFAQViewModal(repo: BAFAQRepo(), endPoint: "tsmore")
        configureFAQDetails()
    }
    
    private func configureFAQDetails() {
        showActivityIndicator(isUserInteractionEnabled: false)
        if let dataModel = faqViewModal {
            dataModel.faqApiCall{(flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    self.faqTableView.reloadData()
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        } else {
            self.hideActivityIndicator()
        }
    }
    
    private func configureNavigation() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "FAQs")
    }
    
    private func registerCells() {
        UtilityFunction.shared.registerCell(faqTableView, "BAFAQTableViewCell")
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

}

// MARK: - TableViewExtension
extension BAFAQViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.faqViewModal?.faqModal?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BAFAQTableViewCell") as? BAFAQTableViewCell
        cell?.configureFAQView(self.faqViewModal?.faqModal?[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        faqViewModal?.faqModal?[indexPath.row].isDetailExpanded = !(self.faqViewModal?.faqModal?[indexPath.row].isDetailExpanded ?? false)
//        faqTableView.reloadData()
        faqTableView.beginUpdates()
        faqTableView.reloadRows(at: [indexPath], with: .none)
        faqTableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(0, 0, tableView.frame.width, 16)) //To add bottom space as inset
    }
}

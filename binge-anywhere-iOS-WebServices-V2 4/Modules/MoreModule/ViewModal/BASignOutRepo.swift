//
//  BASignOutRepo.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 25/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASignOutScreenRepo {
    func signOut(completion: @escaping(APIServicResult<SignOutModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BASignOutRepo: BASignOutScreenRepo {
    func signOut(completion: @escaping (APIServicResult<SignOutModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiParams = APIParams()
		let endStr = BAKeychainManager().baId
        var target = ServiceRequestDetail.init(endpoint: .signOut(param: apiParams, headers: nil, endUrlString: endStr ))
		target.detail.headers["authorization"] = "bearer " + (BAKeychainManager().userAccessToken)
		target.detail.headers["deviceToken"] = BAKeychainManager().deviceAccessToken
		target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["platform"] = "ios"
        target.detail.headers["locale"] = "en"

        return APIService().request(target) { (response: APIResult<APIServicResult<SignOutModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

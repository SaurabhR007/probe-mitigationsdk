//
//  BANotificationSettingsViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BANotificationSettingsViewModalProtocol {
    func notificationSettingsCall(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BANotificationSettingsViewModal: BANotificationSettingsViewModalProtocol {

    var requestToken: ServiceCancellable?
    var repo: BANotificationSettingsScreenRepo
    var endPoint: String = ""
    var baId: String = "0"
    var type: String = ""
    var notificationSettingsModal: BANotificationSettingsModal?
    var apiParams = APIParams()
    init(repo: BANotificationSettingsScreenRepo, endPoint: String, baId: String, settingsType: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
        self.type = settingsType
        self.baId = baId
    }

    func notificationSettingsCall(completion: @escaping(Bool, String?, Bool) -> Void) {
        apiParams["baId"] = BAKeychainManager().baId
        apiParams["type"] = self.type
        requestToken = repo.notificationSettingsCall(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.notificationSettingsModal = _configData.parsed
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

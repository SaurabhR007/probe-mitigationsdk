//
//  BAFAQRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 11/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAFAQScreenRepo {
    var apiEndPoint: String {get set}
    func faqApiCall(completion: @escaping(APIServicResult<BAFAQModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAFAQRepo: BAFAQScreenRepo {
    var apiEndPoint: String = ""
    let apiParams = APIParams()
    func faqApiCall(completion: @escaping (APIServicResult<BAFAQModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .faqCallback(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAFAQModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BANotificationSettingsRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BANotificationSettingsScreenRepo {
    var apiEndPoint: String {get set}
    func notificationSettingsCall(apiParams: APIParams, completion: @escaping(APIServicResult<BANotificationSettingsModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BANotificationSettingsRepo: BANotificationSettingsScreenRepo {
    var apiEndPoint: String = ""

    func notificationSettingsCall(apiParams: APIParams, completion: @escaping (APIServicResult<BANotificationSettingsModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", /*"x-authenticated-userid": BAUserDefaultManager().sId,*/ "authorization": "bearer \(BAKeychainManager().userAccessToken)", "subscriberid": BAKeychainManager().sId, "platform": "BINGE_ANYWHERE"]
        let target = ServiceRequestDetail.init(endpoint: .notificationSettingsData(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<BANotificationSettingsModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

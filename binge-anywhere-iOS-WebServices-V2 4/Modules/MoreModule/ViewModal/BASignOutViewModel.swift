//
//  BASignOutViewModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 25/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BASignOutViewModalProtocol {
    func signOut(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BASignOutViewModal: BASignOutViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BASignOutScreenRepo
    var signOutMessage: SignOutModel?

    init(repo: BASignOutRepo?) {
        self.repo = (repo ?? nil)!
    }

    func signOut(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo.signOut(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.customerName.rawValue)
                        UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.userAccessToken.rawValue)
						UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.deviceAccessToken.rawValue)
						UserDefaults.standard.removeObject(forKey: UserDefault.UserInfo.accountDetailServerResponse.rawValue)
						MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.logout.rawValue)
                        //kAppDelegate.createLoginRootView()
                        completion(true, nil, false)
                    } else {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.logoutFailed.rawValue, properties: [MixpanelConstants.ParamName.reason.rawValue: _configData.parsed.message ?? ""])
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

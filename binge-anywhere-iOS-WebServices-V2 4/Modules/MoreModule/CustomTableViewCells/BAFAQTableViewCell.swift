//
//  BAFAQTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 09/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAFAQTableViewCell: UITableViewCell {

    @IBOutlet weak var faqDetailLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var faqTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureFAQView(_ faqData: Faq?) {
        faqTitleLabel.text = faqData?.question
        faqDetailLabel.text = faqData?.answer
        arrowImageView.image = UIImage(named: (faqData?.isDetailExpanded ?? false) ? "closeArrow" : "expandArrow")
        faqDetailLabel.isHidden = !(faqData?.isDetailExpanded ?? false)
        layoutIfNeeded()
    }
}

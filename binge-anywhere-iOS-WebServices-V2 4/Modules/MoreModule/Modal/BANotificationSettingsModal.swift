//
//  BANotificationSettingsModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BANotificationSettingsModal: Codable {
    let code: Int?
    let message: String?
    let data: BANotificationSettingData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(BANotificationSettingData.self, forKey: .data)
    }
}

struct BANotificationSettingData: Codable {
    let code: Int?
    let message: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

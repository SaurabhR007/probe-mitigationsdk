//
//  BASignOutModal.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 25/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct SignOutModel: Codable {
    var code: Int?
    var message: String?
    var data: SignOut?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SignOut.self, forKey: .data)
    }
}

struct SignOut: Codable {
    var baId: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
    }
}

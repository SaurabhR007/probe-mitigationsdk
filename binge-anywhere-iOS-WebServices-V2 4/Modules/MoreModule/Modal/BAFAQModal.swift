//
//  BAFAQModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 11/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAFAQModal: Codable {
    let code: Int?
    let message: String?
    let data: [Faq]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([Faq].self, forKey: .data)
    }

}

//struct FAQData: Codable {
//    let title: String?
//    let text1: String?
//    let text2: String?
//    let text3: String?
//    let help: FAQHelp?
//    var faqs: FAQQuestionData?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        help = try values.decodeIfPresent(FAQHelp.self, forKey: .help)
//        faqs = try values.decodeIfPresent(FAQQuestionData.self, forKey: .faqs)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        text1 = try values.decodeIfPresent(String.self, forKey: .text1)
//        text2 = try values.decodeIfPresent(String.self, forKey: .text2)
//        text3 = try values.decodeIfPresent(String.self, forKey: .text3)
//    }
//}

//struct FAQQuestionData: Codable {
//    var title: String?
//    var content: [Faq]?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        content = try values.decodeIfPresent([Faq].self, forKey: .content)
//    }
//}

//struct FAQEmail: Codable {
//    let id: String?
//    let subject: String?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        subject = try values.decodeIfPresent(String.self, forKey: .subject)
//    }
//}


class Faq: Codable {
    let question: String?
    var isDetailExpanded = false
    let answer: String?

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        question = try values.decodeIfPresent(String.self, forKey: .question)
        answer = try values.decodeIfPresent(String.self, forKey: .answer)
    }

}

//struct FAQHelp: Codable {
//    let email: FAQEmail?
//    let call: String?
//    //let raise_request: RaiseRequest?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        email = try values.decodeIfPresent(FAQEmail.self, forKey: .email)
//        call = try values.decodeIfPresent(String.self, forKey: .call)
//        //raise_request = try values.decodeIfPresent(RaiseRequest.self, forKey: .raise_request)
//    }
//
//}

//struct RaiseRequest: Codable {
//    let url: String?
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        url = try values.decodeIfPresent(String.self, forKey: .url)
//    }
//
//}



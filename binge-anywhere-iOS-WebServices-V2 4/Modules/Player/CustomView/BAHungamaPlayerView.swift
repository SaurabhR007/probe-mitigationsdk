//
//  BAHungamaPlayerView.swift
//  BingeAnywhere
//
//  Created by Shivam on 15/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import AVKit
import HungamaPlayer

class BAHungamaPlayerView: UIView, BAAutoPlayProtocol {
    
    var content: IContentVO?
    
    @IBOutlet var playerView: HungamaPlayerView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var replayLabel: UILabel!
    @IBOutlet weak var markFavButton: UIButton!
    @IBOutlet weak var volumeButton: UIButton!
    @IBOutlet weak var secondVolumeButton: UIButton!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var fullScreenButton: UIButton!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var timeElapsedLabel: UILabel! // TBAA-5184 now is hidden
    @IBOutlet weak var progressSegue: BACustomSliderView!
    @IBOutlet weak var addToFavButton: UIButton!
    @IBOutlet weak var audioTrackButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var playNextVideoButton: UIButton!
    @IBOutlet weak var zoomInOutButton: UIButton!
    @IBOutlet weak var previousVideoButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomActionsView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var volumeButtonContainer: UIView!
    @IBOutlet weak var volumeButtonContainerWidthConstraint: NSLayoutConstraint!
    
    var addRemoveWatchlistVM: BAAddRemoveWatchlistViewModal?
    var nextEpisodeVM: BANextPreviousEpisodeVM?
    var removeDeviceView: BAUserRemoveView?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var nextEpisodeData: BANextPreviousEpisodeModel?
    var contentDetail: ContentDetailData?
    var vootPlaybackViewModal: BAVootViewModal?
    var contentType: String?
    var episode: Episodes?
    var autoPlayView: BAAutoPlay?
    var noInternetView: BANoInternetOnPlayerView?
    var languageSupportView: BALanguageSupportView?
    var videoQualityView: BAVideoQualityView?
    var isAlertShownOnce: Bool = false
    var isEpisodeCompleted: Bool = false
    var isPlayerInBackground = false
    var startTime: String?
    var stopTime: String?
    var isEpisode: Bool?
    var continueWatchTimer: Timer?
    var isReplay: Bool?
    var videoId: Int?
    var selectedAudiotrack = 0
    var selectedSubtitle = 0
    var resumeCount = 0
    var pauseCount = 0
    var selectedAudioTrackName = ""
    var selectedSubtitleTrackName = ""
    var isAudioOrVideoQualityOpenend = false
    var playerVideoTitle: String?
    var contentVideoType: String?
    var isPlayerFullScreen = true
    var isTrailerFullScreen = false
    var isVideoEnded = false
    var isAddedToFavourite: Bool?
    var isVolume = BAKeychainManager().isVolume
    var previousPlayingState: Bool?
    var selectedQualityVideoName = ""
    var selectedQualityIndex = 0
    var startTimeDate: Date?
    var stopTimeDate: Date?
    var noInternetimer: Timer?
    var videoQualityArr: [String] = []
    var videoQualityArrayMatrix: [String] = []
    var quatityInfoArray: [[String: Any]] = []
    var isPausedMethodCalled = false
    var isScreenRecordingPopUpShown = false
    var isZoomedIn: Bool = false {
        didSet {
            let imageName: String = isZoomedIn ? "zoomOut" : "zoomIn"
            zoomInOutButton.setImage(UIImage(named: imageName), for: .normal)
        }
    }
    var isPlayerPlaying = true {
        didSet {
            delegate?.didChangePlayerPlayingState(isPlayerPlaying)
        }
    }
    var elapsedTimeAfterForward: CMTime?
    weak var delegate: BAPlayerViewDelegate?
    fileprivate var seekDuration: Float64 = 0
    var timeObserver: AnyObject!
    var sliderTimer: Timer!
    var isSliderSeek = false
    var bufferTimer: Timer?
    var bufferTime = 0
    var seekedDuration: Float = 0.0
    var isAutoPlayTimerChecked = false
    var isTrailer: Bool = false
    var isPortrait: Bool {
        return !isTrailerFullScreen//!UIDevice.current.orientation.isLandscape
    }
    
    init(frame: CGRect, url: URL, cookie: String, content: ContentDetailData?, episodeData: Episodes?, isReplayContent: Bool?, videoContentType: String?, contentId: Int?, isTrailerPlaying: Bool, isEpisodePlaying: Bool) {
        super.init(frame: frame)
        //self.url = url
        contentDetail = content
        episode = episodeData
        isEpisode = isEpisodePlaying
        isReplay = isReplayContent
        videoId = contentId
        contentVideoType = videoContentType
        self.isTrailer = isTrailerPlaying
        self.addNoInternetTimer()
        self.continueTimer()
        if !isTrailer {
            self.startBufferTimer()
            startWatchingTime()
        }
        setUp()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        bottomViewHeightConstraint.constant = (isTrailer || isPortrait) ? 0.0 : 38.0
        totalLabelWidthConstraint.constant = isTrailer ? 43.0 : 70.0
        //elapsedTimeWidthConstraint.constant = (!isTrailer && isPortrait) ? 75.0 : 46.0
        volumeButtonContainerWidthConstraint.constant = isPortrait ? 0 : 35
        noInternetView?.configureConstraint(isPortrait: isPortrait)
        autoPlayView?.configureConstraints(isPortrait: isPortrait)
        if languageSupportView != nil && isPortrait {
            languageSupportView?.removeFromSuperview()
            languageSupportView = nil
        }
        if videoQualityView != nil && isPortrait {
            videoQualityView?.removeFromSuperview()
            videoQualityView = nil
        }
        hideControls()
        //configureFavButton()
        progressSegue.delegate = self
        if isPlayerPlaying == true {
            self.togglePlayPauseHungamaPlayer(true) // play
            if !isTrailer {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
            }
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
                fullScreenButton.isHidden = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "syncReplayPIButton"), object: nil)
            } else {
                if !isTrailer {
                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                }
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
        if !isTrailer {
            if isPlayerInBackground {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            } else {
                print("----------------------------------1")
            }
        }
        // in new hungama sdk its playerlayer object is removed hence commenting this line
        //playerView.playerLayer.frame = bounds
    }
    
    func addObserverForScreenRecording() {
        if #available(iOS 11.0, * ) {
            ScreenRecordingDetector.sharedInstance.triggerDetectorTimer()
            NotificationCenter.default.addObserver(self, selector: #selector(screenCaptureStatsDidChange), name: Notification.Name(kScreenRecordingDetectorRecordingStatusChangedNotification), object: nil)
        }
    }
    
    func startBufferTimer() {
        bufferTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateBufferTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateBufferTimer() {
        bufferTime += 1
    }
    
    @objc func screenCaptureStatsDidChange() {
        if ScreenRecordingDetector.sharedInstance.isRecording() {
            //recordingEnabledState()
            recordingDetected()
            CustomLoader.shared.hideLoader()
            debugPrint("Capturing start.")
        }  else {
            debugPrint("Capturing stop")
        }
    }
    
    func recordingEnabledState() {
        recordingDetected()
        CustomLoader.shared.hideLoader()
    }
    
    func recordingDetected() {
        if !isScreenRecordingPopUpShown {
            self.togglePlayPauseHungamaPlayer(false)
            HungamaPlayerManager.shared.stop()
//            HungamaPlayerManager.shared.releasePlayer()
            CustomLoader.shared.hideLoader()
            DispatchQueue.main.async {
                CustomLoader.shared.hideLoader()
                kAppDelegate.window?.makeToastOnlyForMessage(kScreenRecordingDetected) { (boolVal) in
                    print("Screen Recording")
                    //self.hideActivityIndicator()
                    self.isScreenRecordingPopUpShown = false
                    if self.isTrailerFullScreen {
                        self.backAction()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeHungamaPlayer"), object: nil)
                        self.removeScreenRecordingObserver()
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popPIScreen"), object: nil)
                    }
                }
            }
        }
    }
    
    func initialBufferTimeEvent() {
        if bufferTimer != nil {
            self.bufferTimer?.invalidate()
            self.bufferTimer = nil
            if contentDetail != nil {
                let genreResult = contentDetail?.meta?.genre?.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.initialBufferTime.rawValue, properties: [MixpanelConstants.ParamName.duraionMinunte.rawValue: bufferTime / 60, MixpanelConstants.ParamName.contentTitle.rawValue: contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.durationSeconds.rawValue: bufferTime])
            } else {
                let genreResult = episode?.genres?.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.initialBufferTime.rawValue, properties: [MixpanelConstants.ParamName.duraionMinunte.rawValue: bufferTime / 60, MixpanelConstants.ParamName.contentTitle.rawValue: episode?.title ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: episode?.contentType ?? "", MixpanelConstants.ParamName.durationSeconds.rawValue: bufferTime])
            }
        }
    }
    
    func removeScreenRecordingObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kScreenRecordingDetectorRecordingStatusChangedNotification), object: nil)
    }
    
    @objc func addFavFlag() {
        isAddedToFavourite = true
        configureBottomFavActionButton()
    }
    
    @objc func removeFavFlag() {
        isAddedToFavourite = false
        configureBottomFavActionButton()
    }
    
    func configureBottomFavActionButton() {
        if isAddedToFavourite ?? false {
            self.addToFavButton.isSelected = true
            self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
            self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
        } else {
            self.addToFavButton.isSelected = false
            self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
            self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
        }
    }
    
    func checkForVideogravity() {
        playerView.videoGravity = isPortrait ? .resizeAspectFill : .resizeAspect
        self.addObserverForScreenRecording()
        self.continueTimer()
    }
    
    func setUp() {
        Bundle.main.loadNibNamed("BAHungamaPlayerView", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(contentView)
        contentView.fillParentView()
        if contentDetail?.lastWatch != nil {
            self.titleLabel.text = contentDetail?.lastWatch?.contentTitle
        } else {
            self.titleLabel.text = episode?.title
        }
        NotificationCenter.default.addObserver(self, selector: #selector(addFavFlag), name: NSNotification.Name(rawValue: "addFav"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeFavFlag), name: NSNotification.Name(rawValue: "removeFav"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeUser), name: NSNotification.Name(rawValue: "logoutFromPlayer"), object: nil)
//        checkForVideogravity()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.progressSegue.addGestureRecognizer(tapGestureRecognizer)
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showHideControls(gestureRecognizer:)))
        self.playerView.addGestureRecognizer(viewTapGestureRecognizer)
        addTargetActions()
        progressSegue.isContinuous = true
        progressSegue.maximumTrackTintColor = UIColor.init(red: 254.0/255.0, green: 254.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    }
    
    func testPlay() {
        ScreenRecordingDetector.sharedInstance.lastRecordingState = false
        
        print("this is Hungama final contentID \(String(describing: content?.getId()))")
        print("this is Hungama final contentTitle \(String(describing: content?.getTitle()))")
        print("this is Hungama final contentType \(String(describing: content?.getType()))")
        print("this is Hungama final contentPoster \(String(describing: content?.getPosterURL()))")
        
        self.stopPlayback()
        self.releasePlayer()
        do {
            try HungamaPlayerManager.shared.load(content:content!, withContentLoadListener: self)
        }
        catch { //TODO: Handle this
            handleVideoPlayError(true)
            print(error)
        }
    }
    
    
    
    func startPlayback() {
        DispatchQueue.main.async {
            do {
                try HungamaPlayerManager.shared.initializePlayer()
                try HungamaPlayerManager.shared.preparePlayer(forPlayerView: self.playerView, withStateChangeListener: self)
                self.volumeButtonAction()
                //this delay is just for showing thumbnail.
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.hideControls()
                    HungamaPlayerManager.shared.start()
                    HungamaPlayerManager.shared.setSubtitleEnabled(false)
                    if self.contentDetail == nil && self.episode == nil {
                        // This is Trailer Playback
                    } else {
                        self.configureContentPlayback(isNextPreviousEpisode: false)
                    }
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    
    func configureContentPlayback(isNextPreviousEpisode: Bool?) {
        fetchWatchlistLookUp()
        if contentDetail != nil {
            
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
        } catch(let error) {
            print(error.localizedDescription)
        }
        self.togglePlayPauseHungamaPlayer(true) // play
        if isNextPreviousEpisode ?? false {
            showControls()
        } else {
            hideControls()
        }
        if contentDetail != nil {
            if contentDetail?.lastWatch != nil && isReplay == false {
                seekToResumeTime()
                if contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                    getNextEpisodeData(contentId: contentDetail?.lastWatch?.vodId)
                }
            } else {
                HungamaPlayerManager.shared.seek(to: 0)
                if contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                    getNextEpisodeData(contentId: contentDetail?.lastWatch?.vodId)
                }
            }
        } else {
            if isEpisodeCompleted {
                isEpisodeCompleted = false
                HungamaPlayerManager.shared.seek(to: 0)
            } else {
                if episode?.watchedDuration != nil && isReplay == false {
                    self.seekToResumeEpisode()
                } else {
                    HungamaPlayerManager.shared.seek(to: 0)
                }
            }
            getNextEpisodeData(contentId: episode?.id)
        }
    }
    
    @objc func removeUser() {
        if isTrailerFullScreen {
            if removeDeviceView == nil {
                removeDeviceView = BAUserRemoveView(frame: .zero)
                self.contentView.addSubview(removeDeviceView!)
                self.togglePlayPauseHungamaPlayer(true)
                removeDeviceView?.fillParentView()
            }
        } else {

        }
    }

    func playNextEpisode(episode: Episodes?) {
        autoPlayView = nil
        isAutoPlayTimerChecked = false
        bottomViewHeightConstraint.constant = (isTrailer || isPortrait) ? 0.0 : 38.0
        totalLabelWidthConstraint.constant = isTrailer ? 43.0 : 70.0
        contentDetail = nil
        seekedDuration = 0.0
        isVideoEnded = false
        isAlertShownOnce = false
        self.episode = self.nextEpisodeData?.data?.nextEpisode
        fetchWatchlistLookUp()
        var hungamaContent: IContentVO?
        var contentTitle = ""
        if episode?.contentType == ContentType.brand.rawValue {
            contentTitle = contentDetail?.meta?.brandTitle ?? ""
        } else if episode?.contentType == ContentType.series.rawValue {
            contentTitle = contentDetail?.meta?.seriesTitle ?? ""
        } else if episode?.contentType == ContentType.tvShows.rawValue {
            contentTitle = contentDetail?.meta?.vodTitle ?? ""
        } else {
            contentTitle = contentDetail?.meta?.vodTitle ?? ""
        }
        let contentId = episode?.providerContentId ?? ""
        let contentPosterImage = episode?.boxCoverImage
        continueTimer()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
        let contentType = HungamaPlayer.ContentType.returnStringValue(episode?.contentType ?? "")
        hungamaContent = Content(withId: contentId, title: contentTitle, posterURL: contentPosterImage ?? "", type: contentType ?? HungamaPlayer.ContentType.unknown)
        content = hungamaContent
        testPlay()
        UtilityFunction.shared.performTaskInMainQueue {
            self.configureContentPlayback(isNextPreviousEpisode: true)
        }
    }
    
    func continueTimer() {
        if self.continueWatchTimer == nil {
            self.continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(makeContinueWatchingCall), userInfo: nil, repeats: true)
            RunLoop.current.add(self.continueWatchTimer!, forMode: .common)
        }
    }
    
    func cancelContinueTimer() {
        guard self.continueWatchTimer != nil else {
            return
        }
        self.continueWatchTimer!.invalidate()
        self.continueWatchTimer = nil
    }
    
    func watchCredits() {
        // TODO: On Hold as of now.
    }
    
    func seekToResumeTime() {
        let seconds: Int64 = Int64((contentDetail?.lastWatch?.secondsWatched ?? 0))
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
        HungamaPlayerManager.shared.seek(to: Int(targetTimeInterval))
        print("Target time is ----->", Int(targetTimeInterval))
    }
    
    func seekToResumeEpisode() {
        let seconds: Int64 = Int64((Int(episode?.watchedDuration ?? "0") ?? 0))
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
        HungamaPlayerManager.shared.seek(to: Int(targetTimeInterval))
    }
    
    // MARK: Get Next/Prev Episode Data
    func getNextEpisodeData(contentId: Int?) {
        nextEpisodeVM = BANextPreviousEpisodeVM(repo: BANextPreviousEpisodeRepo(), endPoint: "info", id: contentId ?? 0)
        if let nextEpiVM = nextEpisodeVM {
            nextEpiVM.getNextPreviousEpisodeData { (flagValue, _) in
                if flagValue {
                    self.nextEpisodeData = nextEpiVM.nextPreviousData
                    self.configurePrevNextButton()
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    // MARK: Configure Prev/Next Button
    func configurePrevNextButton() {
        fetchWatchlistLookUp()
        if contentDetail?.lastWatch != nil {
            self.titleLabel.text = contentDetail?.lastWatch?.title ?? contentDetail?.lastWatch?.contentTitle
        } else {
            self.titleLabel.text = episode?.title
        }
        
        if isTrailer {
            // Do Not show previous episode button
        } else {
            if isTrailerFullScreen {
                // Already this line is getting executed in show controls and this line causing button to be visible even buffering state is on
//                if self.nextEpisodeData?.data?.nextEpisodeExists ?? false && self.audioTrackButton.isHidden == false {
//                    self.playNextVideoButton.isHidden = false
//                } else {
//                    self.playNextVideoButton.isHidden = true
//                }
//
//                if self.nextEpisodeData?.data?.previousEpisodeExists ?? false && self.audioTrackButton.isHidden == false {
//                    self.previousVideoButton.isHidden = false
//                } else {
//                    self.previousVideoButton.isHidden = true
//                }
            } else {
                previousVideoButton.isHidden = true
            }
        }
    }
    
    func fetchWatchlistLookUp() {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentVideoType ?? "", contentId: "\(videoId ?? 0)")
        confgureWatchlistIcon()
    }
    
    func confgureWatchlistIcon() {
        if let dataModel = watchlistLookupVM {
            dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                if flagValue {
                    if dataModel.watchlistLookUp?.data?.isFavourite ?? false {
                        self.addToFavButton.isSelected = true
                        self.isAddedToFavourite = true
                        self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                        self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
                    } else {
                        self.addToFavButton.isSelected = false
                        self.isAddedToFavourite = false
                        self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                        self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                    }
                } else if isApiError {
                    // Do Nothing
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    
    // MARK: Hide Player Controls
    func hideControls() {
        hideOptionalControls()
        progressSegue.isHidden = true
        totalTimeLabel.isHidden = true
        backButton.isHidden = true
        previousVideoButton.isHidden = true
        forwardButton.isHidden = true
        rewindButton.isHidden = true
        if isVideoEnded {
            fullScreenButton.isHidden = false
            playButton.isHidden = false
            replayLabel.isHidden = false
            fullScreenButton.isHidden = true
            //            markFavButton.isHidden = false
            if isTrailerFullScreen {
                backButton.isHidden = false
            }
            playButton.setImage(UIImage(named: "replay"), for: .normal)
        } else {
            fullScreenButton.isHidden = true
            playButton.isHidden = true
            replayLabel.isHidden = true
        }
        progressSegue.sliderToolTip.alpha = 0.0
        progressSegue.showToolTipViewAnimated(animated: false)
    }
    
    func hideOptionalControls() {
        zoomInOutButton.isHidden = true
        titleLabel.isHidden = true
        bottomActionsView.isHidden = true
        markFavButton.isHidden = true
        volumeButton.isHidden = true
        volumeButtonContainer.isHidden = true
        timeElapsedLabel.isHidden = true
    }
    
    func showOptionalControls() {
        zoomInOutButton.isHidden = (!isTrailer && !isPortrait) ? false : true
        titleLabel.isHidden = (!isTrailer && !isPortrait) ? false : true
        bottomActionsView.isHidden = (!isTrailer && !isPortrait) ? false : true
        markFavButton.isHidden = (isTrailer && !isPortrait) ? false : true
        volumeButton.isHidden = !isPortrait
        volumeButtonContainer.isHidden = isPortrait

//        timeElapsedLabel.isHidden = (!isTrailerFullScreen && !isTrailer) ? false : true
    }
    
    // MARK: Show Player Controls
    func showControls() {
        if isVideoEnded {
            volumeButton.isHidden = true
            volumeButtonContainer.isHidden = true
            progressSegue.isHidden = true
            fullScreenButton.isHidden = true
            //markFavButton.isHidden = false
            if isTrailerFullScreen {
                backButton.isHidden = false
            }
            bottomActionsView.isHidden = true
            timeElapsedLabel.isHidden = true
            totalTimeLabel.isHidden = true
            forwardButton.isHidden = true
            replayLabel.isHidden = false
            rewindButton.isHidden = true
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            playButton.isHidden = false
            if isEpisode ?? false && nextEpisodeData?.data?.nextEpisodeExists ?? false  {
            } else {
                delegate?.videoDidFinish()
            }
        } else {
            if isTrailer {
                
            } else {
                if isTrailerFullScreen {
                    if nextEpisodeData?.data != nil && nextEpisodeData?.data?.previousEpisodeExists ?? false {
                        previousVideoButton.isHidden = false
                    } else {
                        previousVideoButton.isHidden = true
                    }
                    
                    if nextEpisodeData?.data != nil && nextEpisodeData?.data?.nextEpisodeExists ?? false {
                        playNextVideoButton.isHidden = false
                    } else {
                        playNextVideoButton.isHidden = true
                    }
                } else {
                    previousVideoButton.isHidden = true
                }
            }
            showOptionalControls()
            progressSegue.isHidden = false
            fullScreenButton.isHidden = false
            totalTimeLabel.isHidden = false
            if isTrailerFullScreen {
                backButton.isHidden = false
                fullScreenButton.isHidden = true
            } else {
                backButton.isHidden = true
                fullScreenButton.isHidden = false
            }
            if HungamaPlayerManager.shared.isPlaying() == true {
                //self.player.play()
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            } else {
                //self.player.pause()
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
            replayLabel.isHidden = true
            forwardButton.isHidden = false
            //            if playerView.playerLayer.player != nil {
            let currentTime = HungamaPlayerManager.shared.getCurrentPosition()
            let currentPlayBackTime = Int(currentTime)
            if currentPlayBackTime < 10 {
                rewindButton.isHidden = true
            } else {
                rewindButton.isHidden = false
            }
            //            } else {
            //                rewindButton.isHidden = false
            //            }
            playButton.isHidden = false
        }
    }
    
    func stopPlayback() {
        self.togglePlayPauseHungamaPlayer(false) // pause
        HungamaPlayerManager.shared.stop()
        isVolume = false // hot fix to reset the value of volume so it wont mute when we play next button
    }
    
    func releasePlayer() {
        HungamaPlayerManager.shared.releasePlayer()
    }
    
    func onClosedCaptionClicked() {
        if (!HungamaPlayerManager.shared.isSubtitleAvailable()) {
            return;
        }
        HungamaPlayerManager.shared.setSubtitleEnabled(!HungamaPlayerManager.shared.isSubtitleEnabled())
    }
    
    func onVideoQualityClicked() {
        let playbackVariants = HungamaPlayerManager.shared.getPlaybackVarints()
        if playbackVariants.count > 0 {
            let variant = playbackVariants[playbackVariants.count - 1]
            HungamaPlayerManager.shared.setPlaybackVariant(variant)
        }
    }
    
    
    @IBAction func audioSubtitleButtonAction(_ sender: Any) {
        CustomLoader.shared.hideLoader()
        isAudioOrVideoQualityOpenend = true
        previousPlayingState = HungamaPlayerManager.shared.isPlaying()
        isPlayerPlaying = false
        languageSupportView = BALanguageSupportView(frame: .zero)
        languageSupportView?.delegate = self
        languageSupportView?.selectedSubtitleTrackIndex = selectedSubtitle
        
        if selectedAudioTrackName.isEmpty {
            if contentDetail != nil {
                languageSupportView?.selectedAudioTrackName = contentDetail?.meta?.audio?[0] ?? ""
            } else {
                languageSupportView?.selectedAudioTrackName = episode?.languages?[0] ?? ""
            }
        } else {
            languageSupportView?.selectedAudioTrackName = selectedAudioTrackName
        }
        if contentDetail != nil {
            languageSupportView?.audioTracks = contentDetail?.meta?.audio ?? []
        } else {
            languageSupportView?.audioTracks = episode?.languages ?? []
        }
        
        if HungamaPlayerManager.shared.getSubtitleLanguages().count < 1 {
            languageSupportView?.subtitleTracks = ["None"]
            languageSupportView?.selectedSubtitleTrackName = languageSupportView?.subtitleTracks[0]
        } else {
            languageSupportView?.subtitleTracks = ["None"]
            if HungamaPlayerManager.shared.getSubtitleLanguages().count >= 1 {
                if let _ = languageSupportView {
                    languageSupportView?.subtitleTracks.append(contentsOf: HungamaPlayerManager.shared.getSubtitleLanguages())
                }
            }
            if selectedSubtitleTrackName.isEmpty {
                languageSupportView?.selectedSubtitleTrackName = languageSupportView?.subtitleTracks[0]
            } else {
                languageSupportView?.selectedSubtitleTrackName = selectedSubtitleTrackName
            }
        }
        addSubview(languageSupportView!)
        languageSupportView?.fillParentView()
    }
    
    @IBAction func zoomInOutAction(_ sender: Any) {
        if isZoomedIn {
            playerView.videoGravity = .resizeAspect
        } else {
            playerView.videoGravity = .resizeAspectFill
        }
        isZoomedIn = !isZoomedIn
    }
    
    @IBAction func previousEpisodeButtonAction(_ sender: Any) {
        if BAKeychainManager().contentPlayBack == false {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
        } else {
            contentDetail = nil
            seekedDuration = 0.0
            isAlertShownOnce = false
            seekedDuration = 0.0
            episode = self.nextEpisodeData?.data?.previousEpisode
            fetchWatchlistLookUp()
            var hungamaContent: IContentVO?
            var contentTitle = ""
            if episode?.contentType == ContentType.brand.rawValue {
                contentTitle = contentDetail?.meta?.brandTitle ?? ""
            } else if episode?.contentType == ContentType.series.rawValue {
                contentTitle = contentDetail?.meta?.seriesTitle ?? ""
            } else if episode?.contentType == ContentType.tvShows.rawValue {
                contentTitle = contentDetail?.meta?.vodTitle ?? ""
            } else {
                contentTitle = contentDetail?.meta?.vodTitle ?? ""
            }
            let contentId = episode?.providerContentId ?? ""
            let contentPosterImage = episode?.boxCoverImage
            self.continueTimer()
            let contentType = HungamaPlayer.ContentType.returnStringValue(episode?.contentType ?? "")
            hungamaContent = Content(withId: contentId, title: contentTitle, posterURL: contentPosterImage ?? "", type: contentType ?? HungamaPlayer.ContentType.unknown)
            content = hungamaContent
            testPlay()
            UtilityFunction.shared.performTaskInMainQueue {
                self.configureContentPlayback(isNextPreviousEpisode: true)
            }
        }
    }
    
    // MARK: Volume Button Action
    @IBAction func volumeButtonAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if HungamaPlayerManager.shared.isPlaying() {
                self.hideControls()
            }
        }
        if !isVolume {
            volumeButton.setImage(UIImage(named: "volumeup"), for: .normal)
            secondVolumeButton.setImage(UIImage(named: "volumeup"), for: .normal)
            HungamaPlayerManager.shared.setMuted(false)
            BAKeychainManager().isVolume = isVolume
            isVolume = true
        } else {
            volumeButton.setImage(UIImage(named: "volumedown"), for: .normal)
            secondVolumeButton.setImage(UIImage(named: "volumedown"), for: .normal)
            HungamaPlayerManager.shared.setMuted(true)
            BAKeychainManager().isVolume = isVolume
            isVolume = false
        }
    }
    
    
    // MARK: Slider Tap Gesture
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        isSliderSeek = true
        isVideoEnded = false
        previousPlayingState = HungamaPlayerManager.shared.isPlaying()
        self.togglePlayPauseHungamaPlayer(false) //pause
        if sliderTimer != nil {
            sliderTimer.invalidate()
            sliderTimer = nil
        }
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.contentView)
        let positionOfSlider: CGPoint = progressSegue.frame.origin
        let widthOfSlider: CGFloat = progressSegue.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(progressSegue.maximumValue) / widthOfSlider)
        seekedDuration = Float(newValue) //To handle seekbar jumping on tap
        progressSegue.setValue(Float(newValue), animated: true)
        let seconds: Int64 = Int64(newValue)
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
        HungamaPlayerManager.shared.seek(to: Int(targetTimeInterval))
//        timeElapsedLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(targetTime))
        _ = HungamaPlayerManager.shared.getTotalDuration() - Int(targetTimeInterval)
        let currentPlayBackTime = HungamaPlayerManager.shared.getCurrentPosition()
        if currentPlayBackTime < 10 {
            rewindButton.isHidden = true
        } else {
            rewindButton.isHidden = false
        }
        forwardButton.isHidden = false
        handlePlayerState(playingState: previousPlayingState)
        if self.sliderTimer != nil {
            self.sliderTimer.fire()
        }
        self.isSliderSeek = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if HungamaPlayerManager.shared.isPlaying() {
                self.hideControls()
            }
        }
    }
    
    // MARK: Notification for Show/Hide Controls
    @objc func showHideControls(gestureRecognizer: UIGestureRecognizer) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(configureControls), object: nil)
        perform(#selector(configureControls), with: nil, afterDelay: 0.5)
    }
    
    @objc func configureControls() {
        if fullScreenButton.isHidden == true {
            if HungamaPlayerManager.shared.isPlaying() == true {
                showControls()
                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    if HungamaPlayerManager.shared.isPlaying() {
                        self.hideControls()
                    }
                }
                self.togglePlayPauseHungamaPlayer(true) // play
                if isVideoEnded {
                    playButton.setImage(UIImage(named: "replay"), for: .normal)
                    forwardButton.isHidden = true
                } else {
                    playButton.setImage(UIImage(named: "playicon"), for: .normal)
                }
            } else {
                showControls()
                self.togglePlayPauseHungamaPlayer(false) //pause
                if isVideoEnded {
                    playButton.setImage(UIImage(named: "replay"), for: .normal)
                    forwardButton.isHidden = true
                } else {
                    playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
                }
            }
            
        } else {
            hideControls()
        }
    }
    
    // MARK: Convert total duration in mm:ss format
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        if interval.isNaN {
            return ""
        } else {
            let interval = Int(interval)
            let seconds = interval % 60
            let minutes = (interval / 60) % 60
            let hours = interval / 3600
            if hours > 0 {
                return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
            } else {
                return String(format: "%02d:%02d", minutes, seconds)
            }
        }
    }
    
    @IBAction func rewindButtonAction(_ sender: Any) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(rewindTime), object: nil)
        isVideoEnded = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if HungamaPlayerManager.shared.isPlaying(){
                self.hideControls()
            }
        }
        if HungamaPlayerManager.shared.isPlaying() {
            self.togglePlayPauseHungamaPlayer(true) //play
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        seekDuration += Float64(10)
        perform(#selector(rewindTime), with: nil, afterDelay: 1)
    }
    
    @objc func rewindTime() {
        let currentPlayerTime = HungamaPlayerManager.shared.getCurrentPosition()
        let currentCMTime = CMTime(seconds: Double(currentPlayerTime), preferredTimescale: 1000000)
        let playerCurrentTime = CMTimeGetSeconds(currentCMTime)
        var newTime = playerCurrentTime - seekDuration
        
        if newTime < 0 {
            newTime = 0
        }
        
        if newTime < 10 {
            rewindButton.isHidden = true
        } else {
            rewindButton.isHidden = false
        }
        
        let rewindTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        let rewindTimeInterval = Float(CMTimeGetSeconds(rewindTime))
        HungamaPlayerManager.shared.seek(to: Int(rewindTimeInterval))
        seekDuration = Float64(0)
    }
    
    @IBAction func forwardButtonAction(_ sender: Any) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(forwardTime), object: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if HungamaPlayerManager.shared.isPlaying() {
                self.hideControls()
            }
        }
        if HungamaPlayerManager.shared.isPlaying() == true {
            self.togglePlayPauseHungamaPlayer(true) //play
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        seekDuration += Float64(10)
        perform(#selector(forwardTime), with: nil, afterDelay: 1)
    }
    
    @objc func forwardTime() {
        
        let durationtimeIntvl = HungamaPlayerManager.shared.getTotalDuration()
        let durationCmTime = CMTime(seconds: Double(durationtimeIntvl), preferredTimescale: 1000000)
        let currentCMTime = CMTime(seconds: Double(HungamaPlayerManager.shared.getCurrentPosition()), preferredTimescale: 1000000)
        let playerCurrentTime = CMTimeGetSeconds(currentCMTime)
        let newTime = playerCurrentTime + seekDuration
        let timeLeft = CMTimeGetSeconds(durationCmTime) - newTime
        forwardButton.isHidden = false

        if timeLeft < 10 {
            let totalDuration = HungamaPlayerManager.shared.getTotalDuration()
            HungamaPlayerManager.shared.seek(to: Int(totalDuration))
            //player.seek(to: player.duration)
        } else {
            if newTime < CMTimeGetSeconds(durationCmTime) {
                let forwardTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
                let forwardTimeInterval = Float(CMTimeGetSeconds(forwardTime))
                HungamaPlayerManager.shared.seek(to: Int(forwardTimeInterval))
//                timeElapsedLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(forwardTime))
            }
        }
        seekDuration = Float64(0)
    }
    
    @IBAction func addToWatchlistButtonAction(_ sender: Any) {
        var vodId = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentDetail?.meta?.contentType ?? "", contentId: vodId)
        addRemoveFromWatchlist()
    }
    
    // MARK: AddRemoveToWatchlist
    func addRemoveFromWatchlist() {
        if let dataModel = addRemoveWatchlistVM {
            dataModel.addRemoveWatchlist {(flagValue, _, isApiError) in
                if flagValue {
                    if self.addToFavButton.isSelected {
                        self.addToFavButton.isSelected = false
                        self.isAddedToFavourite = false
                        self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                        self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                        self.makeToast(message: "Removed from Watchlist", imageName: "WatchlistActive")
                        let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                        if self.episode != nil {
                            let genreResult = self.episode?.genres?.joined(separator: ",")
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.episode?.title ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.episode?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.episode?.provider ?? ""])
                        } else {
                            let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentDetail?.meta?.provider ?? ""])
                        }
                    } else {
                        if self.episode != nil {
                            let genreResult = self.episode?.genres?.joined(separator: ",")
                            let languageResult = self.episode?.languages?.joined(separator: ",")
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.episode?.title ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.episode?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.episode?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        } else {
                            let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                            let languageResult = self.contentDetail?.meta?.audio?.joined(separator: ",")
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                        }
                        self.addToFavButton.isSelected = true
                        self.isAddedToFavourite = true
                        self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                        self.addToFavButton.setTitle(" Added To Watchlist", for: .normal)
                        self.makeToast(message: "Added to Watchlist", imageName: "WatchlistActive")
                    }
                } else {
                }
            }
        }
    }
    
    func makeToast(message: String, imageName: String) {
        kAppDelegate.window?.makeToast(message, duration: 1.0, point: .bottom, title: "", image: UIImage.init(named: imageName), toastYPosition: tabBarHeight) { _ in
        }
    }
    
    @IBAction func nextEpisodeButtonAction(_ sender: Any) {
        if BAKeychainManager().contentPlayBack == false {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
        } else {
            contentDetail = nil
            seekedDuration = 0.0
            isAlertShownOnce = false
            episode = self.nextEpisodeData?.data?.nextEpisode
            fetchWatchlistLookUp()
            var hungamaContent: IContentVO?
            var contentTitle = ""
            if episode?.contentType == ContentType.brand.rawValue {
                contentTitle = contentDetail?.meta?.brandTitle ?? ""
            } else if episode?.contentType == ContentType.series.rawValue {
                contentTitle = contentDetail?.meta?.seriesTitle ?? ""
            } else if episode?.contentType == ContentType.tvShows.rawValue {
                contentTitle = contentDetail?.meta?.vodTitle ?? ""
            } else {
                contentTitle = contentDetail?.meta?.vodTitle ?? ""
            }
            let contentId = episode?.providerContentId ?? ""
            let contentPosterImage = episode?.boxCoverImage
            self.continueTimer()
            let contentType = HungamaPlayer.ContentType.returnStringValue(episode?.contentType ?? "")
            hungamaContent = Content(withId: contentId, title: contentTitle, posterURL: contentPosterImage ?? "", type: contentType ?? HungamaPlayer.ContentType.unknown)
            content = hungamaContent
            testPlay()
            UtilityFunction.shared.performTaskInMainQueue {
                self.configureContentPlayback(isNextPreviousEpisode: true)
            }
        }
    }
    
    @IBAction func videoQualityButtonAction(_ sender: Any) {
        CustomLoader.shared.hideLoader()
        getStreamQuality()
        isAudioOrVideoQualityOpenend = true
        previousPlayingState = HungamaPlayerManager.shared.isPlaying()
        isPlayerPlaying = false
        videoQualityView = BAVideoQualityView(frame: .zero)
        if videoQualityArr.isEmpty {
            videoQualityView?.videoQualityArray = ["Auto", "High", "Medium", "Low"]
        } else {
            videoQualityArrayMatrix = videoQualityArrayMatrix.removeDuplicates()
            videoQualityView?.videoQualityArray = videoQualityArrayMatrix
        }
        if selectedQualityVideoName.isEmpty {
            videoQualityView?.selectedQualityName = videoQualityView?.videoQualityArray[0]
        } else {
            videoQualityView?.selectedQualityName = selectedQualityVideoName
        }
        addSubview(videoQualityView ?? BAVideoQualityView(frame: .zero))
        videoQualityView?.fillParentView()
        videoQualityView?.delegate = self
    }
    
    func getStreamQuality() {
        videoQualityArr = HungamaPlayerManager.shared.getPlaybackVarints()
        videoQualityArrayMatrix = HungamaPlayerManager.shared.getPlaybackVarints()
        let videoQualityArrayInInt = videoQualityArr.compactMap { Int($0) }
        if !videoQualityArrayInInt.isEmpty {
            for i in 0..<videoQualityArrayInInt.count {
                if videoQualityArrayInInt[i] == 0 {
                    videoQualityArrayMatrix[i] = "Auto"
                } else if videoQualityArrayInInt[i] <= 100000 {
                    videoQualityArrayMatrix[i] = "144p"
                } else if videoQualityArrayInInt[i] > 100000 && videoQualityArrayInInt[i] <= 400000 {
                    videoQualityArrayMatrix[i] = "240p"
                } else if videoQualityArrayInInt[i] > 400000 && videoQualityArrayInInt[i] <= 750000 {
                    videoQualityArrayMatrix[i] = "360p"
                } else if videoQualityArrayInInt[i] > 750000 && videoQualityArrayInInt[i] <= 1000000 {
                    videoQualityArrayMatrix[i] = "480p"
                } else if videoQualityArrayInInt[i] > 1000000 && videoQualityArrayInInt[i] <= 1600000 {
                    videoQualityArrayMatrix[i] = "576p"
                } else if videoQualityArrayInInt[i] > 1600000 && videoQualityArrayInInt[i] <= 3000000 {
                    videoQualityArrayMatrix[i] = "720p"
                } else if videoQualityArrayInInt[i] > 3000000 && videoQualityArrayInInt[i] <= 6500000 {
                    videoQualityArrayMatrix[i] = "1080p"
                } else {
                    videoQualityArrayMatrix[i] = "1080p"
                }
                //videoQualityArr[i] = videoQualityArr[i] + "p"
            }
        }
    }
    
    
    @IBAction func fullscreenButtonAction(_ sender: Any) {
        print("Full screen Button Action In Hungama Player Method")
        isPlayerPlaying = HungamaPlayerManager.shared.isPlaying()
        if HungamaPlayerManager.shared.isPlaying() == true {
            self.togglePlayPauseHungamaPlayer(true) //play
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            }
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
        delegate?.moveToFullScreen(isFullScreen: isPlayerFullScreen)
        if !isPlayerFullScreen {
            isPlayerFullScreen = true
            fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
        } else {
            isPlayerFullScreen = false
            fullScreenButton.setImage(UIImage(named: "minimise"), for: .normal)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if !isTrailer {
            stopWatchingTime()
//            playbackEventTracking() commented this line with coordination of Harsh sir as we are calling this from bavod back button action
        }
        backAction()
    }
    
    func startWatchingTime() {
        let date = Date()
        let format = DateFormatter()
        startTimeDate = date
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        startTime = formattedDate
    }
    
    func stopWatchingTime() {
        let date = Date()
        let format = DateFormatter()
        stopTimeDate = date
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        stopTime = formattedDate
    }
    
    func convertToEventTime(_ time: String, _ conversionType: String) -> String {
        let delimiter = ":"
        var expectedTime = 0
        let timeArr = time.components(separatedBy: delimiter)
        if timeArr.count > 2 {
            expectedTime = (Int(timeArr[0]) ?? 0)*60
            expectedTime += expectedTime + (Int(timeArr[1]) ?? 0)
            if conversionType == "Seconds" && timeArr.indices.contains(2){
                expectedTime = expectedTime*60  + (Int(timeArr[2]) ?? 0)
            }
        }
        return String(expectedTime)
    }
    
    func offsetFrom(date: Date) -> Int {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: stopTimeDate ?? Date())

        let seconds = difference.second ?? 0
        if let second = difference.second, second > 0 { return seconds }
        return 0
    }
    
    func playbackEventTracking(railName: String, pageSource: String, configSource: String) {
        if !isTrailer {
            stopWatchingTime()
            if episode != nil {
                let title = "S\(episode?.season ?? 1) E\(episode?.episodeId ?? 1) - \(episode?.title ?? "")"
                let contentGenre = episode?.genres ?? []
                let videoContentType = episode?.contentType ?? ""
                let playbackDuration = offsetFrom(date: startTimeDate ?? Date())
                let genreResult = episode?.genres?.joined(separator: ",")
                let languageResult = episode?.languages?.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.startTime.rawValue: startTime ?? "", MixpanelConstants.ParamName.stopTime.rawValue: stopTime ?? "", MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: bufferTime/60, MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: bufferTime, MixpanelConstants.ParamName.numberOfResume.rawValue: resumeCount, MixpanelConstants.ParamName.numberOfPause.rawValue: pauseCount, MixpanelConstants.ParamName.duraionMinunte.rawValue: "\(playbackDuration/60) Minutes", MixpanelConstants.ParamName.durationSeconds.rawValue: "\(playbackDuration) Seconds", MixpanelConstants.ParamName.partnerName.rawValue: episode?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource,MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
            } else {
                var title = ""
                if contentDetail?.lastWatch != nil || contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                    title = contentDetail?.lastWatch?.contentTitle ?? ""
                } else {
                   title = contentDetail?.meta?.vodTitle ?? ""
                }
                let genreResult = contentDetail?.meta?.genre?.joined(separator: ",")
                let languageResult = contentDetail?.meta?.audio?.joined(separator: ",")
                let contentGenre = contentDetail?.meta?.genre ?? []
                let videoContentType = contentDetail?.meta?.contentType ?? ""
                let playbackDuration = offsetFrom(date: startTimeDate ?? Date())
                let partnerName = contentDetail?.meta?.provider ?? ""
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.startTime.rawValue: startTime ?? "", MixpanelConstants.ParamName.stopTime.rawValue: stopTime ?? "", MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: bufferTime/60, MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: bufferTime, MixpanelConstants.ParamName.numberOfResume.rawValue: resumeCount, MixpanelConstants.ParamName.numberOfPause.rawValue: pauseCount, MixpanelConstants.ParamName.duraionMinunte.rawValue: "\(playbackDuration/60) Minutes", MixpanelConstants.ParamName.durationSeconds.rawValue: "\(playbackDuration) Seconds", MixpanelConstants.ParamName.partnerName.rawValue: partnerName, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? "",MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
            }
        }
    }
    
    @objc func makeContinueWatchingCall() {
        if BAReachAbility.isConnectedToNetwork() {
            print("Hungama plyaer current position--->", HungamaPlayerManager.shared.getCurrentPosition())
            let currentPlayBackTime = HungamaPlayerManager.shared.getCurrentPosition()
            let totalDuration = HungamaPlayerManager.shared.getTotalDuration()
            if contentDetail != nil {
                if contentDetail?.lastWatch != nil {
                    delegate?.makeContinueWatchCallForContent(contentId: contentDetail?.lastWatch?.vodId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: Int(currentPlayBackTime), totalDuration: Int(totalDuration))
                } else {
                    var vodId = 0
                    switch contentDetail?.meta?.contentType {
                    case ContentType.series.rawValue:
                        vodId = contentDetail?.meta?.seriesId ?? 0
                    case ContentType.brand.rawValue:
                        vodId = contentDetail?.meta?.brandId ?? 0
                    case ContentType.customBrand.rawValue:
                        vodId = contentDetail?.meta?.brandId ?? 0
                    case ContentType.customSeries.rawValue:
                        vodId = contentDetail?.meta?.seriesId ?? 0
                    default:
                        vodId = contentDetail?.meta?.vodId ?? 0
                    }
                    delegate?.makeContinueWatchCallForContent(contentId: vodId, contentType: contentDetail?.meta?.contentType, watchedDuration: Int(currentPlayBackTime), totalDuration: Int(totalDuration))
                }
            } else {
                delegate?.makeContinueWatchCallForContent(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(currentPlayBackTime), totalDuration: Int(totalDuration))
            }
        }
    }
    
    // Back Button Action
    func backAction() {
        autoPlayView?.backButton.isHidden = true
        autoPlayView?.configureConstraints(isPortrait: true)
        autoPlayView = nil
        isPlayerPlaying = HungamaPlayerManager.shared.isPlaying()
        if HungamaPlayerManager.shared.isPlaying() == true {
            self.togglePlayPauseHungamaPlayer(true) //play
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
                fullScreenButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            }
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
                fullScreenButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
        delegate?.moveToFullScreen(isFullScreen: isPlayerFullScreen)
        if !isPlayerFullScreen {
            isPlayerFullScreen = true
            fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
        } else {
            isPlayerFullScreen = false
            fullScreenButton.setImage(UIImage(named: "minimise"), for: .normal)
        }
    }
    
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                handlePlayheadSliderTouchBegin()
                print("began")
                // handle drag began
                break
            case .moved:
                handlePlayheadSliderValueChanged()
                print("moved")
                // handle drag moved
                break
            case .ended:
                self.handlePlayheadSliderTouchEnd()
                print("ended")
                // handle drag ended
                break
            default:
                break
            }
        }
    }
    
    // MARK: Target Methods For Slider Actions
    func handlePlayheadSliderTouchBegin() {
        previousPlayingState = HungamaPlayerManager.shared.isPlaying()
        self.togglePlayPauseHungamaPlayer(false) // pause
        isSliderSeek = true
        if sliderTimer != nil {
            sliderTimer.invalidate()
            sliderTimer = nil
        }
        if HungamaPlayerManager.shared.isPlaying() == true {
            self.togglePlayPauseHungamaPlayer(true) //play
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
    }
    
    // MARK: Slider Value Change Button Action
    func handlePlayheadSliderValueChanged() {
        if sliderTimer != nil {
            sliderTimer.invalidate()
            sliderTimer = nil
        }
        if HungamaPlayerManager.shared.isPlaying() == true {
            self.togglePlayPauseHungamaPlayer(true) //play
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
    }
    
    // MARK: Slider Value Chnage End Button Action
    func handlePlayheadSliderTouchEnd() {
        isSliderSeek = true
        isVideoEnded = false
        if previousPlayingState == true {
            self.togglePlayPauseHungamaPlayer(true) //play
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        let seconds: Int64 = Int64(progressSegue.value)
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
        HungamaPlayerManager.shared.seek(to: Int(targetTimeInterval))
//        timeElapsedLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(targetTime))
        let currentTime = HungamaPlayerManager.shared.getCurrentPosition()
        let currentPlayBackTime = Int(currentTime)
        if currentPlayBackTime < 10 {
            rewindButton.isHidden = true
        } else {
            rewindButton.isHidden = false
        }
        forwardButton.isHidden = false
        handlePlayerState(playingState: previousPlayingState)
        if self.sliderTimer != nil {
            self.sliderTimer.fire()
        }
        self.isSliderSeek = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if HungamaPlayerManager.shared.isPlaying() {
                self.hideControls()
                self.setupSlider()
            }
        }
    }
    
    private func observeTime(elapsedTime: CMTime) {
        //        let duration = self.getDuration(playerView.playerLayer.player!)
        let duration = HungamaPlayerManager.shared.getTotalDuration()
        let elapsedTime = CMTimeGetSeconds(elapsedTime)
        updateTimeLabel(elapsedTime: elapsedTime, duration: Float64(duration))
    }
    
    private func handlePlayerState(playingState: Bool?) {
        if playingState == true {
            self.togglePlayPauseHungamaPlayer(true) //play
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
        }
    }
    
    func cancelTimer() {
        checkForAutoPlayTimer()
        guard self.sliderTimer != nil else {
            return
        }
        self.sliderTimer!.invalidate()
        self.sliderTimer = nil
    }
    
    func checkForAutoPlayTimer() {
        if let view = autoPlayView {
                view.cancelTimer()
        }
        guard let subviews = superview?.subviews else {return}
        for view in subviews {
            if view is BAAutoPlay {
                view.removeFromSuperview()
            }
        }
    }
    
    func togglePlayPauseHungamaPlayer(_ isForPlay: Bool) {
        var genres: [String] = []
        var title = ""
        var partnerName = ""
        if contentDetail != nil {
            genres = contentDetail?.meta?.genre ?? []
            title = contentDetail?.meta?.vodTitle ?? ""
            partnerName = contentDetail?.meta?.provider ?? ""
        } else {
            genres = episode?.genres ?? []
            title = episode?.title ?? ""
            partnerName = episode?.provider ?? ""
        }
        if !isForPlay && HungamaPlayerManager.shared.isPlaying() {
            if !isTrailer {
                HungamaPlayerManager.shared.togglePlayPause()
                self.pauseCount += 1
                let genreResult = genres.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
            }
        } else if isForPlay && !HungamaPlayerManager.shared.isPlaying() {
            if !isTrailer {
                self.resumeCount += 1
                let genreResult = genres.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.resumeContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
                HungamaPlayerManager.shared.togglePlayPause()
            }
        }
    }
    
    func setupSlider() {
        if !isSliderSeek {
            let totalDuration = HungamaPlayerManager.shared.getTotalDuration()
            print("this is total time 23 \(totalDuration)")
            progressSegue.minimumValue = 0
            progressSegue.maximumValue =  Float(totalDuration)
            progressSegue.isContinuous = true
            timerStart()
        }
    }
    
    func addNoInternetTimer() {
        if self.noInternetimer == nil {
            self.noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
            RunLoop.current.add(self.noInternetimer!, forMode: .common)
        }
    }
    
    func timerStart() {
        if self.sliderTimer == nil {
            self.sliderTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
            RunLoop.current.add(self.sliderTimer!, forMode: .common)
        }
    }
    
    @objc func showNoInternetView() {
        if BAReachAbility.isConnectedToNetwork() {
            // Do Nothing
        } else {
            if noInternetView == nil {
                if noInternetimer != nil {
                    noInternetimer?.invalidate()
                    noInternetimer = nil
                }
                previousPlayingState = HungamaPlayerManager.shared.isPlaying()
                isPlayerPlaying = false
                self.togglePlayPauseHungamaPlayer(false) //pause
                CustomLoader.shared.hideLoader()
                noInternetView = BANoInternetOnPlayerView(frame: .zero)
                noInternetView?.configureConstraint(isPortrait: isPortrait)
                noInternetView?.backButton.isHidden = true
                self.contentView.addSubview(noInternetView!)
                if !isTrailerFullScreen {
                    noInternetView?.backButton.isHidden = true
                } else {
                    noInternetView?.backButton.isHidden = false
                }
                noInternetView?.fillParentView()
                noInternetView?.delegate = self
            }
        }
    }
    
    @objc func updateSlider() {
        if sliderTimer != nil {
            let currentPlayBackTime = HungamaPlayerManager.shared.getCurrentPosition()
            let totalDuration = HungamaPlayerManager.shared.getTotalDuration()
            // print("this is total time \(self.getDuration(playerView.playerLayer.player!))")
            if !isSliderSeek {
                progressSegue.setValue((seekedDuration == 0 ? Float(currentPlayBackTime) : seekedDuration) + Float(sliderTimer?.timeInterval ?? 0), animated: true) //If just scrolled the seekbar values will be used until video is buffered
            }
            self.updateTimeLabel(elapsedTime: Double(currentPlayBackTime) + self.sliderTimer.timeInterval, duration: Float64(totalDuration))
            if isVideoEnded {
                forwardButton.isHidden = true
            } else {
                if playButton.isHidden == true {
                    forwardButton.isHidden = true
                } else {
                    forwardButton.isHidden = false
                }
            }
            if isVideoEnded {
                rewindButton.isHidden = true
            } else {
                let currentTime = HungamaPlayerManager.shared.getCurrentPosition()
                let currentPlayBackTime = Int(currentTime)
                if currentPlayBackTime < 10 {
                    rewindButton.isHidden = true
                } else {
                    if playButton.isHidden == true {
                        rewindButton.isHidden = true
                    } else {
                        rewindButton.isHidden = false
                    }
                }
            }
        }
    }

    // elapsed time label is hidden from portrait screen so total time label is showing same time as it is showing on full screen landscape mode updated on bug number TBAA-5588
    private func updateTimeLabel(elapsedTime: Float64, duration: Float64) {
        let elapsedDuration = duration - elapsedTime
        if elapsedDuration > 0 {
            totalTimeLabel.text = self.stringFromTimeInterval(interval: elapsedDuration)
        } else {
            totalTimeLabel.text = "0:00:00"
        }
        
        //        if isTrailerFullScreen && !isTrailer {
        //            let elapsedDuration = duration - elapsedTime
        //            if elapsedDuration > 0 {
        //                totalTimeLabel.text = self.stringFromTimeInterval(interval: elapsedDuration)
        //            } else {
        //                totalTimeLabel.text = "0:00:00"
        //            }
        //        } else {
        //        totalTimeLabel.text = self.stringFromTimeInterval(interval: duration)
        //            timeElapsedLabel.text = self.stringFromTimeInterval(interval: elapsedTime)
        //        }
    }
    
    // MARK: Play/Pause Button Action
    @IBAction func playPauseButtonAction(_ sender: Any) {
        if isVideoEnded {
            if BAKeychainManager().contentPlayBack == false {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
            } else {
                isVideoEnded = false
                showControls()
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
                HungamaPlayerManager.shared.seek(to: 0)
                if isTrailer {
                    // Do not chnage PI screen button state
                } else {
                    delegate?.videoBeganInReplay()
                }
                seekedDuration = 0.0
                self.togglePlayPauseHungamaPlayer(true) //play
            }
        } else {
            if HungamaPlayerManager.shared.isPlaying() {
                self.togglePlayPauseHungamaPlayer(false) // pause
                if isTrailer {
                   // Do not chnage PI screen button state
                } else {
                    var genres: [String] = []
                    var title = ""
                    if contentDetail != nil {
                        genres = contentDetail?.meta?.genre ?? []
                        title = contentDetail?.meta?.vodTitle ?? ""
                    } else {
                        genres = episode?.genres ?? []
                        title = episode?.title ?? ""
                    }
                    let genreResult = genres.joined(separator: ",")
                    if BAReachAbility.isConnectedToNetwork() {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.online.rawValue])
                    } else {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.offline.rawValue])
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                }
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
                isPlayerPlaying = false
            } else {
                self.togglePlayPauseHungamaPlayer(true) //play
                if isTrailer {
                    
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
                }
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
                isPlayerPlaying = true
            }
            delegate?.didChangePlayerPlayingState(isPlayerPlaying)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if HungamaPlayerManager.shared.isPlaying() {
                self.hideControls()
            }
        }
    }
}


//
//  BAFullScreebMoviePlayerView+Delegate.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 01/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import VideoPlayer
import Mixpanel
import Kingfisher
import Firebase
import ARSLineProgress

extension BAFullScreenMoviePlayerView {
    // MARK: Setup Player View
    func setup() {
        Bundle.main.loadNibNamed(String(describing: BAFullScreenMoviePlayerView.self), owner: self, options: nil)
        layer.addSublayer(player.layer!)
        addSubview(contentView)
        contentView.fillParentView()
        player.layer?.videoGravity = .resizeAspect
        NotificationCenter.default.addObserver(self, selector: #selector(configureControls), name: NSNotification.Name(rawValue: "HideControls"), object: nil)
        configurePlayer(isNextPreviousEpisode: false)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.progressSegue.addGestureRecognizer(tapGestureRecognizer)
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showHideControls(gestureRecognizer:)))
        self.contentView.addGestureRecognizer(viewTapGestureRecognizer)
        addTargetActions()
        progressSegue.isContinuous = true
        progressSegue.maximumTrackTintColor = UIColor.init(red: 254.0/255.0, green: 254.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    }
    
    func hidePlayerControlsOnEnd() {
        rewindButton.isHidden = true
        forwardButton.isHidden = true
        progressSegue.isHidden = true
        totalTimeLabel.isHidden = true
        settingsButton.isHidden = true
        audioTrackButton.isHidden = true
        playPauseButton.isHidden = false
        addToFavButton.isHidden = true
        backButton.isHidden = false
        playerTitle.isHidden = false
        playNextVideoButton.isHidden = true
        previousVideoButton.isHidden = true
        playPauseButton.setImage(UIImage(named: "replay"), for: .normal)
        replayLabel.isHidden = false
        playyerEndImageView.isHidden = false
        var url = URL.init(string: "")
        if contentDetail != nil {
            if let image = contentDetail?.meta?.imageUrlApp {
                url = URL.init(string: image)
            } else {
                url = URL.init(string: contentDetail?.meta?.boxCoverImage ?? "")
            }
            // let url = URL.init(string: contentDetail?.meta?.boxCoverImage ?? "")
            if let url = url {
                playyerEndImageView.alpha = 0
                playyerEndImageView.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { _ in
                    UIView.animate(withDuration: 1, animations: {
                        self.playyerEndImageView.alpha = 1
                    })
                })
            } else {
                self.playyerEndImageView.image = nil
            }
        } else {
            url = URL.init(string: coverImage ?? "")
            if let url = url {
                playyerEndImageView.alpha = 0
                playyerEndImageView.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { _ in
                    UIView.animate(withDuration: 1, animations: {
                        self.playyerEndImageView.alpha = 1
                    })
                })
            } else {
                self.playyerEndImageView.image = nil
            }
        }
    }
    
    @objc func trackPlayedTime() {
        let progress = player.currentPlaybackTime*1000 / player.duration
        if !isSliderSeek {
            progressSegue.setValue(Float(progress), animated: true)
        }
    }
    
    private func updateTimeLabel(elapsedTime: Float64, duration: Float64) {
        let elapsedDuration = duration - elapsedTime
        if elapsedDuration > 0 {
            totalTimeLabel.text = self.stringFromTimeInterval(interval: elapsedDuration)
        } else {
            totalTimeLabel.text = "0:00:00"
        }
    }
    
    // MARK: Slider Tap Gesture
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        isSliderSeek = true
        previousPlayeingState = player.isPlayerPlaying
        progressSegue.isUserInteractionEnabled = false
        
        if sliderTimer != nil {
            sliderTimer?.invalidate()
        }
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.contentView)
        let positionOfSlider: CGPoint = progressSegue.frame.origin
        let widthOfSlider: CGFloat = progressSegue.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(progressSegue.maximumValue) / widthOfSlider)
        seekedDuration = Float(newValue) //To handle seekbar jumping on tap
        progressSegue.setValue(Float(newValue), animated: true)
        let seconds: Int64 = Int64(newValue)
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
        player.seek(to: TimeInterval(targetTimeInterval))
        
        handleFullScreenPlayerState(playingState: previousPlayeingState)
        if self.sliderTimer != nil {
            self.sliderTimer?.fire()
            self.progressSegue.isUserInteractionEnabled = true
        }
        self.isSliderSeek = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            if self.player.isPlayerPlaying {
                self.hideControls()
            }
        }
    }
    
    // MARK: Notification for Show/Hide Controls
    @objc func showHideControls(gestureRecognizer: UIGestureRecognizer) {
        if isVideoEnded {
            replayLabel.isHidden = false
            playyerEndImageView.isHidden = false
            playPauseButton.isHidden = false
            hidePlayerControlsOnEnd()
        } else {
            if self.player.playbackState == TTNFairPlayerPlaybackState.seeking {
                DispatchQueue.main.async {
                    CustomLoader.shared.showLoader(true)
                }
                //CustomLoader.shared.showLoader(true)
//                forwardButton.isHidden = true
//                rewindButton.isHidden = true
//                playPauseButton.isHidden = true
            } else {
                CustomLoader.shared.hideLoader()
                if player.isPlayerPlaying == true {
                    configureControls()
                } else {
                    showControls()
                }
            }
            
        }
    }
    
    @objc func configureControls() {
        if audioTrackButton.isHidden == true {
            if player.isPlayerPlaying == true {
                showControls()
                do {
                   try AVAudioSession.sharedInstance().setCategory(.playback)
                } catch(let error) {
                    print(error.localizedDescription)
                }
                self.player.play()
                playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                    if self.player.isPlayerPlaying {
                        self.hideControls()
                    }
                }
            } else {
                self.player.pause()
                playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        } else {
            hideControls()
        }
    }
    
    // MARK: Convert total duration in mm:ss format
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let hour = Int(interval) / 3600
        let minute = Int(interval) / 60 % 60
        let second = Int(interval) % 60
        if hour > 0 {
            return String(format: "%02d:%02d:%02d", hour, minute, second)
        } else {
            return String(format: "%02d:%02d", minute, second)
        }
    }
    
    // MARK: Target Methods For Slider Actions
    @IBAction func handlePlayheadSliderTouchBegin(_ sender: UISlider) {
        previousPlayeingState = player.isPlayerPlaying
        player.pause()
        isSliderSeek = true
        if sliderTimer != nil {
            sliderTimer?.invalidate()
        }
        if player.isPlayerPlaying == true {
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.player.pause()
            playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
    }
    
    // MARK: Slider Value Change Button Action
    @IBAction func handlePlayheadSliderValueChanged(_ sender: UISlider) {
        if sliderTimer != nil {
            sliderTimer?.invalidate()
        }
        if player.isPlayerPlaying == true {
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.player.pause()
            playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
    }
    
    // MARK: Slider Value Chnage End Button Action
    @IBAction func handlePlayheadSliderTouchEnd(_ sender: UISlider) {
        isSliderSeek = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            if self.isVideoEnded {
            } else {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        }
        if previousPlayeingState == true {
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        let seconds: Int64 = Int64(progressSegue.value)
        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
        handleFullScreenPlayerState(playingState: previousPlayeingState)
        player.seek(to: TimeInterval(targetTimeInterval))
        if self.sliderTimer != nil {
            self.sliderTimer?.fire()
        }
        self.isSliderSeek = false
    }
    
    private func observeTime(elapsedTime: CMTime) {
        let duration = player.duration//CMTimeGetSeconds(player.currentItem!.duration)
        let elapsedTime = CMTimeGetSeconds(elapsedTime)
        updateTimeLabel(elapsedTime: elapsedTime, duration: duration)
    }
    
    private func handleFullScreenPlayerState(playingState: Bool?) {
        if playingState == true {
//            forwardButton.isHidden = true
//            rewindButton.isHidden = true
//            playPauseButton.isHidden = true
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
        } else {
//            rewindButton.isHidden = false
//            playPauseButton.isHidden = false
            self.player.pause()
        }
    }
    
    // MARK: Progress Segue Dragging Actions
    func addTargetActions() {
        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchBegin), for: .touchDown)
        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchEnd), for: .touchUpInside)
        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchEnd), for: .touchUpOutside)
        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderValueChanged), for: .valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(showExpiryAlert), name: NSNotification.Name(rawValue: "sessionExpireAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureContentPlayback), name: NSNotification.Name(rawValue: "PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoDidEnd), name: NSNotification.Name(NOTIF_PlayerDidFinish), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePlayingStatus), name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackStateDidChange), name: NSNotification.Name(NOTIF_PlayerPlaybackStateDidChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerLoadStateDidChange), name: NSNotification.Name(NOTIF_PlayerLoadStateDidChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newAccessLogEntry), name: NSNotification.Name(NOTIF_NewAccessLogEntry), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackStalled), name: NSNotification.Name(NOTIF_PlaybackStalled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackErrorState), name: NSNotification.Name(NOTIF_LicenseAcqusitionStatus), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(recordingEnabledState), name: NSNotification.Name(NOTIF_ScreenRecordingEnabled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemStatusDidChanged), name: NSNotification.Name(NOTIF_PlayerItemStatusDidChange), object: nil)
    }
    
    func removeObservers() {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlayerDidFinish), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlayerLoadStateDidChange), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_NewAccessLogEntry), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlaybackStalled), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_LicenseAcqusitionStatus), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_ScreenRecordingEnabled), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("sessionExpireAlert"), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PausePlayer"), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ConfigureView"), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PlayerPlayerOnEnteringForeground"), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlayerItemStatusDidChange), object: nil)
    }
    
    @objc func videoDidEnd(notification: NSNotification) {
        debugPrint("video ended")
        CustomLoader.shared.hideLoader()
        let error = notification.userInfo?["playbackErrorInfo"] as? Error
        if error != nil {
            CustomLoader.shared.hideLoader()
            if error?.localizedDescription == "The request timed out." {
                
            }
        } else {
            hidePlayerControlsOnEnd()
            isVideoEnded = true
            if isEpisode ?? false && nextEpisodeData?.data?.nextEpisodeExists ?? false {
                player.stop()
                self.continueWatchTimer?.invalidate()
                self.continueWatchTimer = nil
                let autoPlayView = BAAutoPlay.init(frame: .zero, data: nextEpisodeData?.data?.nextEpisode)
                isEpisodeCompleted = true
                autoPlayView.delegate = self
                autoPlayView.tag = 999
                addSubview(autoPlayView)
                autoPlayView.fillParentView()
            } else if isEpisode ?? false && !(nextEpisodeData?.data?.nextEpisodeExists ?? false) {
                hidePlayerControlsOnEnd()
            }
        }
    }
    
    @objc func playerItemStatusDidChanged(notification: NSNotification) {
        if let status = notification.userInfo?["playerItemStatus"] as? TTNFairPlayerItemStatus{
            if status == .failed {
                if self.isAlertShownOnce == false {
                    self.isAlertShownOnce = true
                    CustomLoader.shared.hideLoader()
                    hideControls()
                    DispatchQueue.main.async {
                        AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                            self.backToDetailScreen()
                        }
                    }
                } else {
                    
                }
                
                debugPrint("-------- Failed", notification.userInfo)
            } else if status == .readyToPlay {
            }
        }
    }
    
    @objc func playbackErrorState(notification: NSNotification) {
        let userInfo = notification.userInfo
        print("PlayeBack Erro State")
        if userInfo?["HttpStatusCode"] as? Int == 403 || userInfo?["HttpStatusCode"] as? Int == 401 {
            UtilityFunction.shared.performTaskInMainQueue {
                self.backToDetailScreen()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showValidationAlert"), object: nil)
            }
        } else {
            // Do Nothing
        }
    }
    
    @objc func recordingEnabledState(notification: NSNotification) {
        
    }
    
    @objc func playbackStateDidChange(notification: NSNotification) {
        
        guard let loadStateInfo = notification.userInfo else {
            return
        }
        if let status = loadStateInfo["playbackStatus"] as? TTNFairPlayerPlaybackState, status == .playing {
            if bufferTimer != nil {
                self.bufferTimer?.invalidate()
                self.bufferTimer = nil
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.initialBufferTime.rawValue, properties: [MixpanelConstants.ParamName.duraionMinunte.rawValue: bufferTime / 60, MixpanelConstants.ParamName.contentTitle.rawValue: contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: contentDetail?.meta?.genre ?? "", MixpanelConstants.ParamName.contentType.rawValue: contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.durationSeconds.rawValue: bufferTime])
            }
            DispatchQueue.main.async {
                CustomLoader.shared.hideLoader()
            }
            if sliderTimer != nil {
                self.sliderTimer?.fire()
            }
        }
        
        if let status = loadStateInfo["playbackStatus"] as? TTNFairPlayerPlaybackState, status == .seeking {
            print("Staus is------------------>", status)
        }
        //print("Staus is11111111------------------>", loadStateInfo["playbackStatus"])
    }
    
    @objc func playerLoadStateDidChange(notification: NSNotification) {
        
        guard let loadStateInfo = notification.userInfo else {
            return
        }
        if self.player.playbackState == TTNFairPlayerPlaybackState.playing || self.player.playbackState == TTNFairPlayerPlaybackState.paused {
            CustomLoader.shared.hideLoader()
        } else {

        }
        //print("Load State is ------------>", loadStateInfo["loadState"])
        switch loadStateInfo["loadState"] as! TTNFairPlayerLoadState {
        case .playable:
            setupSlider()
            let duration = player.duration//player.currentItem!.asset.duration
            let _: Float64 = duration//CMTimeGetSeconds(duration)
            break
        case .playthroughOk:
            CustomLoader.shared.hideLoader()
            let seconds: Int64 = Int64(progressSegue.value)
            if seconds < 10 {
                rewindButton.isHidden = true
            } else {
                
            }
            seekedDuration = 0 //To reset seekedDuration so seekbar can use curentPlayback duration
        case .unknown:
            if self.player.playbackState != TTNFairPlayerPlaybackState.playing || self.player.playbackState != TTNFairPlayerPlaybackState.paused {
                if isAlertShownOnce {
                    debugPrint("is alert  shown once")
                    // Do not show Loader
                } else {
                    DispatchQueue.main.async {
                        CustomLoader.shared.showLoader(true)
                    }
                }
            } else if self.player.isPlayerPlaying {
                DispatchQueue.main.async {
                    CustomLoader.shared.showLoader(true)
                }
            }
        @unknown default:
            debugPrint("---Fatal Error")
            seekedDuration = 0 //To reset seekedDuration so seekbar can use curentPlayback duration
            fatalError()
        }
    }
    
    @objc func newAccessLogEntry(notification: NSNotification) {
    }
    
    @objc func playbackStalled(notification: NSNotification) {
    }
    
    func setupSlider() {
        if !isSliderSeek {
            progressSegue.minimumValue = 0
            progressSegue.maximumValue = Float(player.duration)
            progressSegue.isContinuous = true
            timerStart()
        }
    }
    
    func timerStart() {
        sliderTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
    }
    
    @objc func makeContinueWatchingCall() {
        if contentDetail != nil {
            if contentDetail?.lastWatch != nil {
                delegate?.makeContinueWatchCall(contentId: contentDetail?.lastWatch?.contentId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: contentDetail?.lastWatch?.durationInSeconds)
            } else {
                var vodId = 0
                switch contentType {
                case ContentType.series.rawValue:
                    vodId = contentDetail?.meta?.seriesId ?? 0
                case ContentType.brand.rawValue:
                    vodId = contentDetail?.meta?.brandId ?? 0
                case ContentType.customBrand.rawValue:
                    vodId = contentDetail?.meta?.brandId ?? 0
                case ContentType.customSeries.rawValue:
                    vodId = contentDetail?.meta?.seriesId ?? 0
                default:
                    vodId = contentDetail?.meta?.vodId ?? 0
                }
                delegate?.makeContinueWatchCall(contentId: vodId, contentType: contentDetail?.meta?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: contentDetail?.meta?.duration)
            }
        } else {
            if player.currentPlaybackTime.isNaN {
                
            } else {
                delegate?.makeContinueWatchCall(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: Int(episode?.totalDuration ?? "0"))
            }
        }
    }
    
    @objc func updateSlider() {
        if !isSliderSeek {
            progressSegue.setValue((seekedDuration == 0 ? Float(player.currentPlaybackTime) : seekedDuration) + Float(sliderTimer?.timeInterval ?? 0), animated: true) //If just scrolled the seekbar values will be used until video is buffered
        }
        updateTimeLabel(elapsedTime: player.currentPlaybackTime + (sliderTimer?.timeInterval ?? 0), duration: player.duration)
        let timeLeft = player.duration - player.currentPlaybackTime
            if audioTrackButton.isHidden == true {
                forwardButton.isHidden = true
                playPauseButton.isHidden = true
            } else {
                forwardButton.isHidden = false
                playPauseButton.isHidden = false
            }
        
        if player.currentPlaybackTime < 10 {
            rewindButton.isHidden = true
            if audioTrackButton.isHidden == false {
                playPauseButton.isHidden = false
            } else {
                playPauseButton.isHidden = true
            }
        } else {
            if audioTrackButton.isHidden == true {
                rewindButton.isHidden = true
            } else {
                rewindButton.isHidden = false
            }
        }
//        debugPrint("The Player State in Update Slider is --------->", self.player.playbackState)
//        debugPrint("The Player Playing in Update Slider is --------->", self.player.isPlayerPlaying)
        if self.player.playbackState == TTNFairPlayerPlaybackState.seeking || self.player.playbackState == TTNFairPlayerPlaybackState.pending {
            DispatchQueue.main.async {
                CustomLoader.shared.showLoader(true)
            }
        } else {
            if isVideoEnded {
                hidePlayerControlsOnEnd()
            } else {
                // Do Nothing
            }
        }
    }
    
    func setVideoQuality(selectedIndex: Int?, selectedTite: String?) {
        if selectedIndex == 0 {
            var bandWidth: Double = 0.0
            if !quatityInfoArray.isEmpty {
                let quality = quatityInfoArray[0]
                let strBandWidth = quality["BANDWIDTH"] as? String ?? "0.0"
                bandWidth = Double(strBandWidth) ?? 500000
            } else {
                bandWidth = 500000.0
            }
            player.setVideoQuality(quality: .auto(value: bandWidth))
        } else {
            let qualityIndex = (selectedIndex ?? 1) - 1
            let quality = selectedIndex ?? 0 < quatityInfoArray.count ? quatityInfoArray[qualityIndex] : ["QUALITY":"Auto"]
            let strBandWidth = quality["BANDWIDTH"] as? String ?? "0.0"
            let bandWidth = Double(strBandWidth) ?? 500000
            player.setVideoQuality(quality: .auto(value: bandWidth))
        }
    }
}

extension BAFullScreenMoviePlayerView: BAVideoQualityViewDelegate {
    
    func sliderWillChangeSeekedDuration(value: Float) {
        seekedDuration = value == 0.0 ? 1.0 : value
        debugPrint(seekedDuration)
    }
    
    func moveToPlayer(selectedVideoQualityIndex: Int?, selectedVideoQualityName: String?) {
        isPlayerPlaying = previousPlayeingState ?? false
        showControls()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideControls()
        }
        selectedQualityIndex = selectedVideoQualityIndex ?? 0
        selectedQualityVideoName = selectedVideoQualityName ?? ""
        self.setVideoQuality(selectedIndex: selectedQualityIndex, selectedTite: selectedQualityVideoName)
    }
}

extension BAFullScreenMoviePlayerView: BACustomSliderViewDelegate {
    func sliderWillDisplayPopUpView(slider: BACustomSliderView) {
        slider.sliderToolTip.alpha = 1.0
        slider.showToolTipViewAnimated(animated: true)
    }
    
    func sliderWillHidePopUpView(slider: BACustomSliderView) {
        slider.sliderToolTip.alpha = 0.0
        slider.showToolTipViewAnimated(animated: false)
    }
}

extension BAFullScreenMoviePlayerView: BALanguageSupportViewDelegate {
    func popToPlayer(selectedSubTitle: Int?, selectedAudioTrack: Int?, selectedAudioName: String?, selectedSubtitleName: String?) {
        showControls()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideControls()
        }
        isPlayerPlaying = previousPlayeingState ?? false
        selectedAudiotrack = selectedAudioTrack ?? 0
        selectedSubtitle = selectedSubTitle ?? 0
        selectedAudioTrackName = selectedAudioName ?? ""
        selectedSubtitleTrackName = selectedSubtitleName ?? ""
        updateSubtitleTrack()
    }
}

extension BAFullScreenMoviePlayerView: BANoInternetOnPlayerViewDelegate {
    func popToDetailScreen() {
        backToDetailScreen()
    }
    
    func retryAction() {
        if BAReachAbility.isConnectedToNetwork() {
            noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
            isPlayerPlaying = previousPlayeingState ?? false
            showControls()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.hideControls()
            }
            //player.play()
            //configurePlayer(isNextPreviousEpisode: true)
            // TODO: Reload Player
        } else {
            CustomLoader.shared.hideLoader()
            let view = BANoInternetOnPlayerView(frame: .zero)
            addSubview(view)
            view.fillParentView()
            view.delegate = self
        }
    }
}

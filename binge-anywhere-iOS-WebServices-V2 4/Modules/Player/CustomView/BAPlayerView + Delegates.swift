//
//  BAPlayerView + Delegates.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import VideoPlayer
import Mixpanel
import Kingfisher
import Firebase
import ARSLineProgress






extension BAPlayerView {
    // MARK: Progress Segue Dragging Actions ans Other Notification Observers
    func addTargetActions() {
        progressSegue.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        //            progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchBegin), for: .touchDown)
        //            progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchEnd), for: .touchUpInside)
        //            progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderValueChanged), for: .valueChanged)
//        self.player.player.addObserver(self, forKeyPath: "currentItem.presentationSize", options: [.initial, .new], context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoDidEnd), name: NSNotification.Name(NOTIF_PlayerDidFinish), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackStateDidChange), name: NSNotification.Name(NOTIF_PlayerPlaybackStateDidChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showExpiryAlert), name: NSNotification.Name(rawValue: "sessionExpireAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pausePlayerIcon), name: NSNotification.Name(rawValue: "pausePlayerIcons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playPlayerIcon), name: NSNotification.Name(rawValue: "playPlayerIcons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerLoadStateDidChange), name: NSNotification.Name(NOTIF_PlayerLoadStateDidChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureContentPlay), name: NSNotification.Name(rawValue: "PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeUser), name: NSNotification.Name(rawValue: "logoutFromPlayer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configurePlayNextState), name: NSNotification.Name(rawValue: "PlayNowState"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePlayingStatus), name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newAccessLogEntry), name: NSNotification.Name(NOTIF_NewAccessLogEntry), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackStalled), name: NSNotification.Name(NOTIF_PlaybackStalled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(preventScreenshot), name: UIApplication.userDidTakeScreenshotNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(playbackStateDidChange), name: NSNotification.Name(NOTIF_PlayerPlaybackStateDidChange), object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(playerLoadStateDidChange), name: NSNotification.Name(NOTIF_PlayerLoadStateDidChange), object: nil)
       // NotificationCenter.default.addObserver(self, selector: #selector(newAccessLogEntry), name: NSNotification.Name(NOTIF_NewAccessLogEntry), object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(playbackStalled), name: NSNotification.Name(NOTIF_PlaybackStalled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackErrorState), name: NSNotification.Name(NOTIF_LicenseAcqusitionStatus), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemStatusDidChanged), name: NSNotification.Name(NOTIF_PlayerItemStatusDidChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureControls), name: NSNotification.Name(rawValue: "HideControls"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resumeContent), name: NSNotification.Name(rawValue: "ResumeContent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseContent), name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureFullScreenIcon), name: NSNotification.Name(rawValue: "changeFullScreenIcon"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureSmallScreenIcon), name: NSNotification.Name(rawValue: "changeSmallScreenIcon"), object: nil)
    }
    
    
    @objc func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlayerDidFinish), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlayerLoadStateDidChange), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_NewAccessLogEntry), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlaybackStalled), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_LicenseAcqusitionStatus), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("sessionExpireAlert"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PausePlayer"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ConfigureView"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PlayNowState"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("popPIScreen"), object: nil)
        //            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("pausePlayerIcons"), object: nil)
        //            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("playPlayerIcons"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NOTIF_PlayerItemStatusDidChange), object: nil)
    }
    
    @objc func preventScreenshot() {
        //if isTrailerFullScreen {
        //kAppDelegate.window?.makeToastOnlyForMessage(kScreenshotDetected)
//        } else {
//
//        }
        print("Screenshot triggered")
    }
    
//    @objc func testSaurabh(notification: NSNotification) {
//        print("Saurabh Test %@",notification.userInfo);
//
//
//
//    }
    
    @objc func configurePlayNextState() {
        //autoPlayView?.isPlayNowTapped = true
        autoPlayView?.playNowAction()
        //playNextEpisode(episode: episode)
    }
    
    @objc func showExpiryAlert() {
        if continueWatchTimer != nil {
            continueWatchTimer?.invalidate()
            continueWatchTimer = nil
            player.stop()
            hideControls()
            if isTrailerFullScreen {
                if removeDeviceView == nil {
                    previousPlayeingState = player.isPlayerPlaying
                    isPlayerPlaying = false
                    player.pause()
                    CustomLoader.shared.hideLoader()
                    removeDeviceView = BAUserRemoveView(frame: .zero)
                    self.contentView.addSubview(removeDeviceView!)
                    self.player.pause()
                    removeDeviceView?.fillParentView()
                }
            } else {
                DispatchQueue.main.async {
                    AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kDeviceRemovedHeader), .withMessage(kSessionExpire), .hidden, .hidden)) { _ in
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    }
                }
            }
        }
    }
    
    @objc func pausePlayerIcon() {
        var genres: [String] = []
        var title = ""
        var partnerName = ""
        if contentDetail != nil {
            genres = contentDetail?.meta?.genre ?? []
            title = contentDetail?.meta?.vodTitle ?? ""
            partnerName = contentDetail?.meta?.provider ?? ""
        } else {
            genres = episode?.genres ?? []
            title = episode?.title ?? ""
            partnerName = episode?.provider ?? ""
        }
        self.pauseCount += 1
        let genreResult = genres.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
        playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
    }
    
    @objc func playPlayerIcon() {
        var genres: [String] = []
        var title = ""
        var partnerName = ""
        if contentDetail != nil {
            genres = contentDetail?.meta?.genre ?? []
            title = contentDetail?.meta?.vodTitle ?? ""
            partnerName = contentDetail?.meta?.provider ?? ""
        } else {
            genres = episode?.genres ?? []
            title = episode?.title ?? ""
            partnerName = episode?.provider ?? ""
        }
        let genreResult = genres.joined(separator: ",")
        self.resumeCount += 1
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.resumeContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
        playButton.setImage(UIImage(named: "playicon"), for: .normal)
    }
    
    @objc func playbackErrorState(notification: NSNotification) {
        let userInfo = notification.userInfo
        print("Userinfo--------->", userInfo)
        if let errorString = userInfo?["HttpStatusCode"] as? Int {
            var errorMessage = ""
            var errorName = ""
            if let error1 = userInfo?["PlaybackErrorInfo"] as? [String:Any]{
                if let failure = error1["FailureReason"] as? String{
                    errorMessage = failure
                }
                errorName = error1.debugDescription
            }
            self.probeInterface?.sendEvent(eventType: .ERROR, eventDetailsIfAny:["ErrorCode" :"\(errorString)", "ErrorName":errorName, "ErrorDetails":errorMessage])
        }
        if contentDetail?.detail?.contractName == "RENTAL" {
            if userInfo?["HttpStatusCode"] as? Int == ACPlayerHTTPStatusCode.Error403.rawValue {
//                CustomLoader.shared.hideLoader()
//                hideControls()
//                player.stop()
                if let type = userInfo?["PlaybackErrorType"] as? Int {
                    if type == ACPlayerErrorType.GeoLocationBlockingStream.rawValue {
                        if self.isAlertShownOnce == false {
                            print("********* Geo Location Blocking **********")
                           self.isAlertShownOnce = true
                            DispatchQueue.main.async {
                                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("Something went wrong. Unable to play content."), .hidden, .hidden)) { _ in
                                    self.player.stop()
                                    if !self.isTrailer && self.isTrailerFullScreen {
                                        self.backAction()
                                    } else {
                                        // Do Nothing
                                    }
                                }
                            }
                        }
                    } else if type == ACPlayerErrorType.ProxyError.rawValue {
                        print("********* Proxy Blocking **********")
                        if self.isAlertShownOnce == false {
                           self.isAlertShownOnce = true
                            DispatchQueue.main.async {
                                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("Something went wrong. Unable to play content."), .hidden, .hidden)) { _ in
                                    self.player.stop()
                                    if !self.isTrailer && self.isTrailerFullScreen {
                                        self.backAction()
                                    } else {
                                        // Do Nothing
                                    }
                                }
                            }
                        }
                    } else if type == ACPlayerErrorType.ConcurrentStreamError.rawValue {
                        print("********* Concurrent Stream Error **********")
                        if self.isAlertShownOnce == false {
                           self.isAlertShownOnce = true
                            DispatchQueue.main.async {
                                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("You have exceeded the limit of screens on which you can watch this video. Please stop playback on one of the other devices to watch here."), .hidden, .hidden)) { _ in
                                    self.player.stop()
                                    if !self.isTrailer && self.isTrailerFullScreen {
                                        self.backAction()
                                    } else {
                                        // Do Nothing
                                    }
                                }
                            }
                        }
                    } else {
                        print("********* Error in 403 **********")
                        if self.isAlertShownOnce == false {
                           self.isAlertShownOnce = true
                            DispatchQueue.main.async {
                                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                                    self.player.stop()
                                    if !self.isTrailer && self.isTrailerFullScreen {
                                        self.backAction()
                                    } else {
                                        // Do Nothing
                                    }
                                }
                            }
                        }
                    }
                } else {
                    print("********* Else Blocking **********")
                    if self.isAlertShownOnce == false {
                       self.isAlertShownOnce = true
                        DispatchQueue.main.async {
                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("You have no longer access to view this content"), .hidden, .hidden)) { _ in
                                self.player.stop()
                                if !self.isTrailer && self.isTrailerFullScreen {
                                    self.backAction()
                                } else {
                                    // Do Nothing
                                }
                            }
                        }
                    }
                }
            } else if userInfo?["HttpStatusCode"] as? Int == ACPlayerHTTPStatusCode.Error401.rawValue {
                print("************ In 401 Error Codes ***********")
                if let type = userInfo?["PlaybackErrorType"] as? Int {
                    if self.isAlertShownOnce == false {
                       self.isAlertShownOnce = true
                        if type == ACPlayerErrorType.SessionFailure.rawValue {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showValidationAlertOnSessionFailure"), object: nil)
                        } else{
                            DispatchQueue.main.async {
                                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                                    if !self.isTrailer && self.isTrailerFullScreen {
                                        self.backAction()
                                    } else {
                                        // Do Nothing
                                    }
                                }
                            }
                        }
                    }
                } else {
                    var errorMessage = ""
                    if let error = userInfo?["PlaybackErrorInfo"] as? [String:Any]{
                        if let failure = error["FailureReason"] as? String{
                            errorMessage = failure
                        }
                    }
                    
                    if self.isAlertShownOnce == false {
                       self.isAlertShownOnce = true
                        DispatchQueue.main.async {
                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage(errorMessage), .hidden, .hidden)) { _ in
                                self.player.stop()
                                if !self.isTrailer && self.isTrailerFullScreen {
                                    self.backAction()
                                } else {
                                    // Do Nothing
                                }
                            }
                        }
                    }
                }
            } else {
                print("*************", userInfo?["HttpStatusCode"])
            }
            print("*************", userInfo?["HttpStatusCode"])
        } else {
            if userInfo?["HttpStatusCode"] as? Int == 403 || userInfo?["HttpStatusCode"] as? Int == 401 {
                UtilityFunction.shared.performTaskInMainQueue {
                    if !self.isTrailer && self.isTrailerFullScreen {
                        self.backAction()
                    } else {
                        
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showValidationAlert"), object: nil)
                }
            } else {
                // Do Nothing
            }
        }
    }
    
    @objc func playerItemStatusDidChanged(notification: NSNotification) {
        print("*************** Playaback Status Did CHange--------->", notification)
        if let status = notification.userInfo?["playerItemStatus"] as? TTNFairPlayerItemStatus{
            if status == .failed {
                if !isTrailer {
                    self.playbackFailureEventTracking(message: "We are unable to play your video right now. Please try again in a few minutes")
                }
                if self.isAlertShownOnce == false {
                    self.isAlertShownOnce = true
                    CustomLoader.shared.hideLoader()
                    hideControls()
                    if contentDetail?.meta?.provider?.uppercased() == ProviderType.zee.rawValue {
                        
                    } else {
                        
                    }
                    DispatchQueue.main.async {
                        AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                            if !self.isTrailer && self.isTrailerFullScreen {
                                self.backAction()
                            } else {
                                // Do Nothing
                            }
                        }
                    }
                } else {
                    
                }
            } else if status == .readyToPlay {
                // Do Nothing
            }
        }
    }
    
    func playbackFailureEventTracking(message: String) {
        if contentDetail != nil {
            let title = contentDetail?.meta?.vodTitle ?? ""
            let contentGenre = contentDetail?.meta?.genre ?? []
            let videoContentType = contentDetail?.meta?.contentType ?? ""
            let genreResult = contentGenre.joined(separator: ",")
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.failurePlayback.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.reason.rawValue: message])
        } else {
            let title = episode?.id ?? 0
            let contentGenre = episode?.genres ?? []
            let videoContentType = episode?.contentType ?? ""
            let genreResult = contentGenre.joined(separator: ",")
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.failurePlayback.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.reason.rawValue: message])
        }
    }
    
    @objc func recordingEnabledState(notification: NSNotification) {
      //  recordingDetected()
    //    CustomLoader.shared.hideLoader()
    }
    
    func recordingDetected() {
        if !isScreenRecordingPopUpShown {
            isScreenRecordingPopUpShown = true
            player.playbackState = .stopped
            player.stop()
            CustomLoader.shared.hideLoader()
            DispatchQueue.main.async {
                CustomLoader.shared.hideLoader()
                kAppDelegate.window?.makeToastOnlyForMessage(kScreenRecordingDetected) { (boolVal) in
                    print("Screen Recording")
                   // self.hideActivityIndicator()
                    self.isScreenRecordingPopUpShown = false
                    if self.isTrailerFullScreen {
                        self.backAction()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                    } else {
                        self.removeScreenRecordingObserver()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeHungamaPlayer"), object: nil)
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "popPIScreen"), object: nil)
                    }
                }
            }
        }
    }
    
    
    func removeScreenRecordingObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIF_ScreenRecordingEnabled), object: nil)
    }
    
    func initialBufferTimeEvent() {
        if bufferTimer != nil {
            self.bufferTimer?.invalidate()
            self.bufferTimer = nil
            if contentDetail != nil {
                let genreResult = contentDetail?.meta?.genre?.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.initialBufferTime.rawValue, properties: [MixpanelConstants.ParamName.duraionMinunte.rawValue: bufferTime / 60, MixpanelConstants.ParamName.contentTitle.rawValue: contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.durationSeconds.rawValue: bufferTime])
            } else {
                let genreResult = episode?.genres?.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.initialBufferTime.rawValue, properties: [MixpanelConstants.ParamName.duraionMinunte.rawValue: bufferTime / 60, MixpanelConstants.ParamName.contentTitle.rawValue: episode?.title ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: episode?.contentType ?? "", MixpanelConstants.ParamName.durationSeconds.rawValue: bufferTime])
            }
        }
    }
    
    @objc func trackPlayedTime() {
        let progress = player.currentPlaybackTime*1000 / player.duration
        if !isSliderSeek {
            progressSegue.setValue(Float(progress), animated: true)
        }
    }
    
    @objc func resumeContent() {
        if previousPlayeingState == true {
            player.play()
        } else {
            player.pause()
        }
    }
    
    @objc func configureFullScreenIcon() {
        fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
        backButton.isHidden = true
        isTrailerFullScreen = false
        if autoPlayView != nil && autoPlayView?.isHidden == false {
            autoPlayView?.backButton.isHidden = true
        }
        
        if noInternetView != nil && noInternetView?.isHidden == false {
            noInternetView?.backButton.isHidden = true
        }
        
        if languageSupportView != nil && languageSupportView?.isHidden == false {
            player.pause()
        }
        
        if videoQualityView != nil && videoQualityView?.isHidden == false {
            player.pause()
        }
        delegate?.configureFavButton(isAddedToFav: isAddedToFavourite)
    }
    
    @objc func configureSmallScreenIcon() {
        print("called two times")
        fullScreenButton.setImage(UIImage(named: "minimise"), for: .normal)
        isTrailerFullScreen = true
        backButton.isHidden = false
        if autoPlayView != nil && autoPlayView?.isHidden == false {
            autoPlayView?.backButton.isHidden = false
        }
        if noInternetView != nil && noInternetView?.isHidden == false {
            noInternetView?.backButton.isHidden = false
        }
    }
    
    @objc func pauseContent() {
        print("Pause Content")
        if isVideoEnded {
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            fullScreenButton.isHidden = true
        } else {
            previousPlayeingState = player.isPlayerPlaying
            player.pause()
        }
    }
    
    @objc func videoDidEnd(notification: NSNotification) {
        debugPrint("video ended")
        if isTrailer {
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
            seekedDuration = 0
            forwardButton.isHidden = true
            isVideoEnded = true
            isPlayerPlaying = false
            showControls()
            //delegate?.videoDidFinish()
            if let error = notification.userInfo?["playbackErrorInfo"] as? Error {
                print(error.localizedDescription)
                print((error as NSError).code)
            }
        } else {
            debugPrint("video ended for Player")
            CustomLoader.shared.hideLoader()
            let error = notification.userInfo?["playbackErrorInfo"] as? Error
            if error != nil {
                CustomLoader.shared.hideLoader()
                if error?.localizedDescription == "The request timed out." {
                    
                }
            } else {
                isVideoEnded = true
                showControls()
                if isEpisode ?? false && nextEpisodeData?.data?.nextEpisodeExists ?? false {
                    if BAKeychainManager().contentPlayBack == false {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
                    } else {
                        player.stop()
                        self.continueWatchTimer?.invalidate()
                        self.continueWatchTimer = nil
                        delegate?.configurePlayButtonOnPI()
                        if autoPlayView == nil {
                            autoPlayView = BAAutoPlay.init(frame: .zero, data: nextEpisodeData?.data?.nextEpisode)
                            autoPlayView?.configureConstraints(isPortrait: isPortrait)
                            isEpisodeCompleted = true
                            autoPlayView?.delegate = self
                            autoPlayView?.tag = 999
                            self.contentView.addSubview(autoPlayView!)
                            if !isTrailerFullScreen {
                                autoPlayView?.backButton.isHidden = true
                                autoPlayView?.playNowButton.isHidden = true
                            } else {
                                autoPlayView?.backButton.isHidden = false
                                autoPlayView?.playNowButton.isHidden = false
                            }
                            autoPlayView?.fillParentView()
                        }
                    }
                } else if isEpisode ?? false && !(nextEpisodeData?.data?.nextEpisodeExists ?? false) {
                    showControls()
                } else if !(isEpisode ?? false) {
                    showControls()
                }
            }
        }
    }
    
    @objc func playbackStateDidChange(notification: NSNotification) {
        print("Playback State Did Change")
        if !isTrailer {
            guard let loadStateInfo = notification.userInfo else {
                return
            }
            if let status = loadStateInfo["playbackStatus"] as? TTNFairPlayerPlaybackState, status == .playing {
                self.initialBufferTimeEvent()
                DispatchQueue.main.async {
                    CustomLoader.shared.hideLoader()
                }
                if sliderTimer != nil {
                    self.sliderTimer?.fire()
                }
            }
            
            if let status = loadStateInfo["playbackStatus"] as? TTNFairPlayerPlaybackState, status == .seeking {
              
                print("Staus is------------------>", status)
            }
            print("Staus is11111111------------------>", loadStateInfo["playbackStatus"] ?? [""])
        } else {
            guard let loadStateInfo = notification.userInfo else {
                return
            }
            debugPrint("playbackStatusDidChange ::>>>>>> \(loadStateInfo)")
            
            if let status = loadStateInfo["playbackStatus"] as? TTNFairPlayerPlaybackState, status == .playing {
                if sliderTimer != nil {
                    sliderTimer.fire()
                }
            }
        }
    }
    
    @objc func playerLoadStateDidChange(notification: NSNotification) {
        debugPrint("playerLoadStateDidChange")
        if !isTrailer {
            guard let loadStateInfo = notification.userInfo else {
                return
            }
            if self.player.playbackState == TTNFairPlayerPlaybackState.playing || self.player.playbackState == TTNFairPlayerPlaybackState.paused {
                CustomLoader.shared.hideLoader()
            } else {
                
            }
            switch loadStateInfo["loadState"] as! TTNFairPlayerLoadState {
            case .playable:
                setupSlider()
                if noInternetView != nil && noInternetView?.isHidden == false {
                    noInternetView?.removeFromSuperview()
                    noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
                    isPlayerPlaying = previousPlayeingState ?? false
                    player.play()
                    showControls()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        self.hideControls()
                    }
                }
                let duration = player.duration//player.currentItem!.asset.duration
                let _: Float64 = duration//CMTimeGetSeconds(duration)
                break
            case .playthroughOk:
                setupSlider()
                debugPrint("-----------------------Play")
                CustomLoader.shared.hideLoader()
                let seconds: Int64 = Int64(progressSegue.value)
                if seconds < 10 {
                    rewindButton.isHidden = true
                } else {
                    
                }
                seekedDuration = 0 //To reset seekedDuration so seekbar can use curentPlayback duration
            case .unknown:
                if self.player.playbackState != TTNFairPlayerPlaybackState.playing || self.player.playbackState != TTNFairPlayerPlaybackState.paused {
                    if isAlertShownOnce {
                        debugPrint("is alert  shown once")
                    } else {
                        DispatchQueue.main.async {
                            CustomLoader.shared.showLoader(true)
                        }
                    }
                } else if self.player.playbackState == TTNFairPlayerPlaybackState.playing {
                    DispatchQueue.main.async {
                        CustomLoader.shared.hideLoader()
                    }
                } else if self.player.isPlayerPlaying {
                    DispatchQueue.main.async {
                        CustomLoader.shared.showLoader(true)
                    }
                }
                debugPrint("-----Player State----", self.player.isPlayerPlaying)
                debugPrint("---Unknown", self.player.playbackState)
            @unknown default:
                debugPrint("---Fatal Error")
                seekedDuration = 0 //To reset seekedDuration so seekbar can use curentPlayback duration
                fatalError()
            }
        } else {
            guard let loadStateInfo = notification.userInfo else {
                return
            }
            switch loadStateInfo["loadState"] as! TTNFairPlayerLoadState {
            case .playable:
                setupSlider()
                let duration = player.duration//player.currentItem!.asset.duration
                let seconds: Float64 = duration//CMTimeGetSeconds(duration)
                totalTimeLabel.text = self.stringFromTimeInterval(interval: seconds)
            case .playthroughOk:
                debugPrint("---PlaythroughOk")
                seekedDuration = 0 //To reset seekedDuration so seekbar can use curentPlayback duration
            case .unknown:
                debugPrint("---Unknown")
            @unknown default:
                seekedDuration = 0 //To reset seekedDuration so seekbar can use curentPlayback duration
                fatalError()
            }
        }
    }
    
    @objc func newAccessLogEntry(notification: NSNotification) {
        debugPrint("newAccessLogEntry")
    }
    
    @objc func playbackStalled(notification: NSNotification) {
        debugPrint("playbackStalled")
    }
}

extension BAPlayerView: BACustomSliderViewDelegate {

    func sliderWillDisplayPopUpView(slider: BACustomSliderView) {
        self.probeInterface?.sendEvent(eventType: .SEEKED)
        slider.sliderToolTip.alpha = 1.0
        slider.showToolTipViewAnimated(animated: true)
    }

    func sliderWillHidePopUpView(slider: BACustomSliderView) {
        slider.sliderToolTip.alpha = 0.0
        slider.showToolTipViewAnimated(animated: false)
    }
}

extension BAPlayerView: BAVideoQualityViewDelegate {
    
    func sliderWillChangeSeekedDuration(value: Float) {
        seekedDuration = value == 0.0 ? 1.0 : value
        debugPrint(seekedDuration)
    }
    
    func moveToPlayer(selectedVideoQualityIndex: Int?, selectedVideoQualityName: String?) {
        if isTrailerFullScreen {
            videoQualityView?.removeFromSuperview()
            videoQualityView = nil
        }
        isPlayerPlaying = previousPlayeingState ?? false
        showControls()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideControls()
        }
        selectedQualityIndex = selectedVideoQualityIndex ?? 0
        selectedQualityVideoName = selectedVideoQualityName ?? ""
        self.setVideoQuality(selectedIndex: selectedQualityIndex, selectedTite: selectedQualityVideoName)
    }
}

extension BAPlayerView: BALanguageSupportViewDelegate {
    func popToPlayer(selectedSubTitle: Int?, selectedAudioTrack: Int?, selectedAudioName: String?, selectedSubtitleName: String?) {
        if isTrailerFullScreen {
            languageSupportView?.removeFromSuperview()
            languageSupportView = nil
        }
        showControls()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideControls()
        }
        isPlayerPlaying = previousPlayeingState ?? false
        selectedAudiotrack = selectedAudioTrack ?? 0
        selectedSubtitle = selectedSubTitle ?? 0
        selectedAudioTrackName = selectedAudioName ?? ""
        selectedSubtitleTrackName = selectedSubtitleName ?? ""
        updateSubtitleTrack()
    }
}

extension BAPlayerView: BANoInternetOnPlayerViewDelegate {
    func popToDetailScreen() {
        noInternetView = nil
        noInternetView?.backButton.isHidden = true
        if !isTrailer && isTrailerFullScreen {
            backAction()
        }
    }
    
    func retryAction() {
        noInternetView = nil
        if !isTrailerFullScreen {
            noInternetView?.backButton.isHidden = true
        } else {
            noInternetView?.backButton.isHidden = false
        }
        if BAReachAbility.isConnectedToNetwork() {
            noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
            isPlayerPlaying = previousPlayeingState ?? false
            noInternetView?.removeFromSuperview()
            player.play()
            if !isTrailer {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
            }
            
            showControls()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.hideControls()
            }
        } else {
            showNoInternetView()
        }
    }
}





//
//  BACustomSliderView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 03/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Foundation

protocol BACustomSliderViewDelegate: class {
    func sliderWillDisplayPopUpView(slider: BACustomSliderView)
    func sliderWillHidePopUpView(slider: BACustomSliderView)
    func sliderWillChangeSeekedDuration(value: Float)
}

public class BACustomSliderView: UISlider {

    weak var delegate: BACustomSliderViewDelegate?
    var epgStartTime: Double = 0.0
    public var sliderTooltipPaddingX: CGFloat = 0.0
    public var isSFullScreenPlayer = false

    fileprivate let paddingX: CGFloat = 15.0
    fileprivate let paddingY: CGFloat = 25.0

    private let timerFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.maximumUnitCount=0
        formatter.allowedUnits = [.hour, .minute, .second]
        return formatter
    }()

    var sliderToolTip: TSSliderToolTip!

    @IBInspectable open var trackHeight: CGFloat = 6.0 {
        didSet {setNeedsDisplay()}
    }

    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y,
            width: defaultBounds.size.width,
            height: trackHeight
        )
    }

    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setMinimumTrackImage(UIImage(named: "slider"), for: .normal)
        if isPhone {self.setMaximumTrackImage(UIImage(named: "sliderEnd"), for: .normal)}
                let thumbRect = self.currentThumbRect()
               // Initial Frame
        if sliderToolTip == nil {
            sliderToolTip = TSSliderToolTip.init(frame: CGRect.init(self.frame.origin.x, self.frame.origin.y-self.paddingY*2, 91.0, 42.0))
            sliderToolTip.textColor = UIColor.white
            sliderToolTip.textAlignment = .center
            sliderToolTip.backgroundColor = UIColor.clear
            sliderToolTip.alpha = 0.0
            sliderToolTip.center = CGPoint(x: thumbRect.origin.x + (self.paddingX)*2, y: self.frame.origin.y-self.paddingY*3)
        }
    }

    // Update Postion for ToolTip
     func updateFrameForSlider() {

        self.bringSubviewToFront(sliderToolTip)
        DispatchQueue.main.async {

            let thumbRect = self.currentThumbRect()
            self.updateToolTipTimeLabel(self.value, forLabel: self.sliderToolTip)

            /* =================== TataSky =====================
             * Restricting SliderToolTip to go beyond the UISlider bounds
             */
            let maxCenterOfToolTip = self.frame.origin.x + (self.frame.size.width + self.sliderToolTip.frame.width / 2.0)
            let minCenterOfToolTip =  self.frame.origin.x - (self.sliderToolTip.frame.width / 2.0)
            let thumbRectCenter = self.frame.origin.x +  thumbRect.midX
            if !self.isSFullScreenPlayer {
                guard thumbRectCenter >= minCenterOfToolTip && thumbRectCenter <= maxCenterOfToolTip  else {
                    guard isPhone else {return}
                    if (self.sliderToolTip.frame.origin.x + self.sliderToolTip.frame.size.width + 30) > self.frame.size.width {
                        if #available(iOS 11.0, *), (UIDevice.current.orientation == .portrait || UIDevice.current.orientation == .portraitUpsideDown) && kAppDelegate.window!.safeAreaInsets.bottom  > 0 {
                            self.sliderToolTip.center = CGPoint(x: self.sliderTooltipPaddingX + self.frame.size.width - (self.sliderToolTip.frame.size.width/2), y: self.frame.origin.y-self.paddingY + kAppDelegate.window!.safeAreaInsets.bottom - 20)
                        } else {
                            if #available(iOS 11.0, *), kAppDelegate.window!.safeAreaInsets.bottom > 0 {
                                self.sliderToolTip.center = CGPoint(x: kAppDelegate.window!.safeAreaInsets.bottom + 30 + self.sliderTooltipPaddingX + self.frame.size.width - (self.sliderToolTip.frame.size.width/2), y: self.frame.origin.y-self.paddingY)
                            } else {
                                self.sliderToolTip.center = CGPoint(x: self.sliderTooltipPaddingX + self.frame.size.width - (self.sliderToolTip.frame.size.width/2), y: self.frame.origin.y-self.paddingY)
                            }
                        }
                    } else {
                        if #available(iOS 11.0, *), (UIDevice.current.orientation == .portrait || UIDevice.current.orientation == .portraitUpsideDown) && kAppDelegate.window!.safeAreaInsets.bottom  > 0 {
                            self.sliderToolTip.center = CGPoint(x: self.sliderTooltipPaddingX + (self.sliderToolTip.frame.size.width/2), y: self.frame.origin.y-self.paddingY + kAppDelegate.window!.safeAreaInsets.bottom - 20)
                        } else {
                            if #available(iOS 11.0, *), kAppDelegate.window!.safeAreaInsets.bottom  > 0 {
                                self.sliderToolTip.center = CGPoint(x: kAppDelegate.window!.safeAreaInsets.bottom + 30 + self.sliderTooltipPaddingX + (self.sliderToolTip.frame.size.width/2), y: self.frame.origin.y-self.paddingY)
                            } else {
                                self.sliderToolTip.center = CGPoint(x: self.sliderTooltipPaddingX + (self.sliderToolTip.frame.size.width/2), y: self.frame.origin.y-self.paddingY)
                            }
                        }
                    }
                    return
                }
            }

            //Fix tooltip positioning for notch devices
            if #available(iOS 11.0, *), (UIDevice.current.orientation == .portrait || UIDevice.current.orientation == .portraitUpsideDown) {
                if UIDevice.current.orientation == .portraitUpsideDown {
                    self.sliderToolTip.center = CGPoint(x: thumbRect.origin.x + self.frame.origin.x + self.paddingX, y: self.frame.origin.y - self.paddingY )
                } else {
                    self.sliderToolTip.center = CGPoint(x: thumbRect.origin.x + self.frame.origin.x + self.paddingX, y: self.frame.origin.y-self.paddingY + kAppDelegate.window!.safeAreaInsets.bottom - 20)
                }
            } else {
                // Fallback on earlier versions
                self.sliderToolTip.center = CGPoint(x: thumbRect.origin.x + self.frame.origin.x + self.paddingX, y: self.frame.origin.y-self.paddingY)
            }

        }
    }

    func updateFrameForTooltipOnSizeChange() {
        if isPhone {
            showToolTipViewAnimated(animated: false)
        }
        self.bringSubviewToFront(sliderToolTip)

        DispatchQueue.main.async {

            let thumbRect = self.currentThumbRect()
            self.updateToolTipTimeLabel(self.value, forLabel: self.sliderToolTip)

            /* =================== TataSky =====================
             * Restricting SliderToolTip to go beyond the UISlider bounds
             */

            let maxCenterOfToolTip = self.frame.origin.x + (self.frame.size.width - self.sliderToolTip.frame.width/2.0)
            let minCenterOfToolTip = self.frame.origin.x + (self.sliderToolTip.frame.width/2.0)
            let thumbRectCenter = self.frame.origin.x + thumbRect.midX

            if thumbRectCenter <= minCenterOfToolTip {
                self.sliderToolTip.center = CGPoint(x: minCenterOfToolTip, y: self.frame.origin.y-self.paddingY)
            } else if thumbRectCenter >= maxCenterOfToolTip {
                self.sliderToolTip.center = CGPoint(x: maxCenterOfToolTip, y: self.frame.origin.y-self.paddingY)
            } else {
                self.sliderToolTip.center = CGPoint(x: thumbRect.origin.x + self.frame.origin.x + self.paddingX, y: self.frame.origin.y-self.paddingY)
            }
        }
    }

    func showToolTipViewAnimated(animated: Bool) {

//        sliderToolTip.fadeAnimation(fadeIn: animated, duration: CONTROLS_FADE_ANIMATION_DURATION)
//        sliderToolTip.fade
    }

    open override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let begin = super.beginTracking(touch, with: event)
        if begin {
            UIApplication.shared.keyWindow?.addSubview(sliderToolTip)
            //showToolTipViewAnimated(animated: true)
            updateFrameForSlider()
            delegate?.sliderWillDisplayPopUpView(slider: self)
        }
        return begin
    }

    open override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let continueTracking = super.continueTracking(touch, with: event)
        if continueTracking { updateFrameForSlider() }
        delegate?.sliderWillChangeSeekedDuration(value: self.value) //To handle seekbar jumping on seeking first time
        return continueTracking
    }

    // Touch is sometimes nil if cancelTracking calls through to this.
    open override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        sliderToolTip.removeFromSuperview()
//        showToolTipViewAnimated(animated: false)
        delegate?.sliderWillChangeSeekedDuration(value: self.value) //To get final seeked value
        delegate?.sliderWillHidePopUpView(slider: self)
    }

    // Event may be nil if cancelled for non-event reasons, e.g. removed from window
    open override func cancelTracking(with event: UIEvent?) {
        super.cancelTracking(with: event)
        //showToolTipViewAnimated(animated: false)
        delegate?.sliderWillHidePopUpView(slider: self)
    }

    func currentThumbRect() -> CGRect {
        let trackRect = self.trackRect(forBounds: self.bounds)
        let thumbRect = self.thumbRect(forBounds: self.bounds, trackRect: trackRect, value: self.value)
        return thumbRect
    }

    func showTimeOnToolTipForVodContent(value: Float, label: UILabel) {
        let duration: TimeInterval = TimeInterval(value)
        DispatchQueue.main.async {
            if duration<60 {
                label.text = String(format: "00:%02d", Int(duration))
            } else {
                let labelTime = self.timerFormatter.string(from: duration)
                label.text = labelTime
            }
        }
    }

//    func showTimeOnToolTipForLiveContent(value:Float,label:UILabel) {
//        let epgStartTimeWithBufferTime = self.epgStartTime/1000 - (DataManager.sharedInstance.liveContentOffset/1000)
//        let currentTime = epgStartTimeWithBufferTime + Double(value)
//        let localTime = TSUtility.convertToHourMinSec(currentTime:currentTime)
//        DispatchQueue.main.async {
//            label.text = localTime
//        }
//    }

    fileprivate func updateToolTipTimeLabel(_ value: Float, forLabel label: UILabel) {
//        if playerType == .live && !TSUtility.isKidProfile() {
//            showTimeOnToolTipForLiveContent(value: value, label: label)
//        }else {
            showTimeOnToolTipForVodContent(value: value, label: label)
//        }
    }
}

class TSSliderToolTip: UILabel {

    var roundRect: CGRect!

    override func drawText(in rect: CGRect) {
        super.drawText(in: roundRect)
    }

    override func draw(_ rect: CGRect) {
        roundRect = CGRect(x: rect.minX, y: rect.minY, width: rect.width, height: rect.height)
        let roundRectBez = UIBezierPath(roundedRect: roundRect, cornerRadius: 6.0)
        let triangleBez = UIBezierPath()
        triangleBez.move(to: CGPoint(x: roundRect.minX + roundRect.width, y: roundRect.maxY))
        //triangleBez.addLine(to: CGPoint(x:rect.midX,y:rect.maxY))
        //triangleBez.addLine(to: CGPoint(x: roundRect.maxX - roundRect.width, y:roundRect.maxY))
        triangleBez.close()
        roundRectBez.append(triangleBez)
        let bez = roundRectBez
        UIColor(red: 229.0/255.0, green: 0/255.0, blue: 125.0/255.0, alpha: 1.0).setFill()
        bez.fill()
        super.draw(rect)
    }
}

extension CGRect {
    init(_ x: CGFloat, _ y: CGFloat, _ w: CGFloat, _ h: CGFloat) {
        self.init(x: x, y: y, width: w, height: h)
    }
}

//
//  BAVideoQualityView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 28/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel

enum VideoQuality {
    case Auto
    case High
    case Medium
    case Low
}

protocol BAVideoQualityViewDelegate: class {
   // func setVideoQuality(quality: VideoQuality)
    func moveToPlayer(selectedVideoQualityIndex: Int?, selectedVideoQualityName: String?)
}

class BAVideoQualityView: UIView {

    @IBOutlet weak var vdeoQualityView: UIView!
    @IBOutlet weak var videoQualityTableView: UITableView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet var contentView: UIView!
    weak var delegate: BAVideoQualityViewDelegate?
    var videoQualityArray = [String]()
    var selectedVideoQualityIndex = 0
    var selectedQualityName: String?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUp() {
        Bundle.main.loadNibNamed(String(describing: BAVideoQualityView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.fillParentView()
        videoQualityTableView.delegate = self
        videoQualityTableView.dataSource = self
        contentView.backgroundColor = .BAdarkBlueBackground
        videoQualityTableView.backgroundColor = .BAdarkBlueBackground
        registerCell()
    }

    func registerCell() {
        UtilityFunction.shared.registerCell(videoQualityTableView, kBAPlayerSettingsTableViewCell)
    }

    @IBAction func dismissButtonAction(_ sender: Any) {
        delegate?.moveToPlayer(selectedVideoQualityIndex: selectedVideoQualityIndex, selectedVideoQualityName: selectedQualityName)
        self.removeFromSuperview()
    }
}

extension BAVideoQualityView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Count ------>", videoQualityArray)
        return videoQualityArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = videoQualityTableView.dequeueReusableCell(withIdentifier: kBAPlayerSettingsTableViewCell) as? BAPlayerSettingsTableViewCell
        cell?.backgroundColor = .BAdarkBlueBackground
        cell?.configureVideoQualityCell(displayName: videoQualityArray[indexPath.row], selectedVideoQualityName: selectedQualityName)
        //cell?.delegate = self
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedQuality = videoQualityArray[indexPath.row]
        selectedVideoQualityIndex = indexPath.row
        selectedQualityName = videoQualityArray[indexPath.row]
        videoQualityTableView.reloadData()
        print(selectedQuality)
    }
}

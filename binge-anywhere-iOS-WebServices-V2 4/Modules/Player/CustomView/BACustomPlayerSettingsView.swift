//
//  BACustomPlayerSettingsView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 28/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BACustomPlayerSettingsView: UIView {

    @IBOutlet weak var genericTableView: UITableView!
    @IBOutlet var contentView: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUp() {
        Bundle.main.loadNibNamed(String(describing: BACustomPlayerSettingsView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.fillParentView()
        genericTableView.delegate = self
        genericTableView.dataSource = self
        contentView.backgroundColor = .BAdarkBlueBackground
        genericTableView.backgroundColor = .BAdarkBlueBackground
        registerCell()
    }

    func registerCell() {
        UtilityFunction.shared.registerCell(genericTableView, kBAPlayerSettingsTableViewCell)
    }

}

extension BACustomPlayerSettingsView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }

}

//
//  BAUserRemoveView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 25/02/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit

class BAUserRemoveView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButton: CustomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setUp() {
        Bundle.main.loadNibNamed(String(describing: BAUserRemoveView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.fillParentView()
        contentView.backgroundColor = .BAdarkBlueBackground
        okButton.makeCircular(roundBy: .height)
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        UtilityFunction.shared.logoutUser()
        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
    }
}

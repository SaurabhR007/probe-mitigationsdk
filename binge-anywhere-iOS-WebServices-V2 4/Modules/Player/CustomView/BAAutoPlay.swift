//
//  BAAutoPlay.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 22/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

protocol BAAutoPlayProtocol: class {
    func playNextEpisode(episode: Episodes?)
    func watchCredits()
    func backAction()
}

class BAAutoPlay: UIView {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var startingTextLabel: CustomLabel!
    @IBOutlet weak var startingTimerLabel: CustomLabel!
    @IBOutlet weak var episodeName: CustomLabel!
    @IBOutlet weak var episodeTimeRating: CustomLabel!
    @IBOutlet weak var episodeDescription: CustomLabel!
    @IBOutlet weak var playNowButton: CustomButton!
    @IBOutlet weak var watchCreditButton: CustomButton!
    @IBOutlet weak var seasonCoverImageView: UIImageView!

    @IBOutlet weak var startingInLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionTrailingConstraint: NSLayoutConstraint!
    var contentDetails: Episodes?
    var isPlayNowTapped = false
    var timer: Timer?
    var timerCanceled: Bool?
    var resetCount = 10
    var viewFrame = CGRect(0, 0, 568, 320)
    weak var delegate: BAAutoPlayProtocol?

    init(frame: CGRect, data: Episodes?) {
        super.init(frame: frame)
        contentDetails = data
        //self.viewFrame = frame
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        Bundle.main.loadNibNamed("BAAutoPlay", owner: self, options: nil)
        loadData()
        setupUI()
    }

    func setupUI() {
        self.backgroundColor = .BAdarkBlueBackground
        //contentView.frame = viewFrame
        addSubview(contentView)
        contentView.fillParentView()
        startingTextLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 14), color: .BAwhite)
        startingTimerLabel.setupCustomFontAndColor(font: .skyTextFont(.bold, size: 14), color: .BAwhite)
        episodeName.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 20), color: .BAwhite)
        episodeTimeRating.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAlightBlueGrey)
        episodeDescription.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 14), color: .BAwhite)
        playNowButton.makeCircular(roundBy: .height)
        watchCreditButton.makeCircular(roundBy: .height)
        if true {
            watchCreditButton.isHidden = true //change this condition later
        }

        if let gradientLayer = seasonCoverImageView.layer.sublayers?.first as? CAGradientLayer {
            gradientLayer.removeFromSuperlayer()
        }
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.BAdarkBlueBackground.withAlphaComponent(1.0).cgColor,
                           UIColor.BAdarkBlueBackground.withAlphaComponent(0.86).cgColor,
                           UIColor.BAdarkBlueBackground.withAlphaComponent(0.4).cgColor,
                           UIColor.BAdarkBlueBackground.withAlphaComponent(0.0).cgColor]
        gradient.locations = [0.0, 0.4, 0.7, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: seasonCoverImageView.frame.size.width, height: seasonCoverImageView.frame.size.height)
        seasonCoverImageView.layer.insertSublayer(gradient, at: UInt32(seasonCoverImageView.layer.sublayers?.count ?? 0))
        runCodeExpiryTimer()
    }

    func loadData() {
        episodeName.text = contentDetails?.title
        if let duration =  contentDetails?.duration {
            episodeTimeRating.text = "\(duration / 60)m"
            if let rating = contentDetails?.genres {
                episodeTimeRating.text?.append(" | \(rating[0])")
            }
        }
        episodeDescription.text = contentDetails?.description
        if let url = contentDetails?.boxCoverImage {
            let url = URL.init(string: url)
			seasonCoverImageView.alpha = 0
			seasonCoverImageView.kf.setImage(with: ImageResource(downloadURL: url!), completionHandler: { _ in
				UIView.animate(withDuration: 1, animations: {
					self.seasonCoverImageView.alpha = 1
				})
			})
        }
    }

    func runCodeExpiryTimer() {
        cancelTimer()
        if self.timer == nil && timerCanceled == nil {
            timerCanceled = false
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            RunLoop.current.add(self.timer!, forMode: .common)
        }
    }
    
    @objc func updateTimer() {
        if !(timerCanceled ?? true) {
            if resetCount > 1 {
                resetCount -= 1
                self.startingTimerLabel.text = "\(resetCount) seconds"
            } else {
                self.playNowAction()
                cancelTimer()
            }
        }
    }

    @IBAction func backAction() {
//        cancelTimer()
        delegate?.backAction()
    }

    @IBAction func playNowAction() {
        if isPlayNowTapped == false && !(timerCanceled ?? true) {
            isPlayNowTapped = true
            timerCanceled = true
            cancelTimer()
            guard let subviews = superview?.subviews else {return}
            for view in subviews {
                if view is BAAutoPlay {
                   view.removeFromSuperview()
               }
            }
            print("************** In Play Action ***************")
            delegate?.playNextEpisode(episode: contentDetails)
        }        
    }
    
    func cancelTimer() {
        guard self.timer != nil else {
            return
        }
        self.timer!.invalidate()
        self.timerCanceled = nil
        self.timer = nil
    }

    func configureConstraints(isPortrait: Bool) {
        if isPortrait {
            startingInLabelTopConstraint.constant = 30
            titleTrailingConstraint.constant = 20
            descriptionTrailingConstraint.constant = 20
            playNowButton.isHidden = true
        } else {
            startingInLabelTopConstraint.constant = 66
            titleTrailingConstraint.constant = 110
            descriptionTrailingConstraint.constant = 118
            playNowButton.isHidden = false
        }
    }

    @IBAction func watchcredit() {
        delegate?.watchCredits()
    }
}

//
//  BANoInternetOnPlayerView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 04/05/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BANoInternetOnPlayerViewDelegate: class {
    func popToDetailScreen()
    func retryAction()
}

class BANoInternetOnPlayerView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var errorDetailLabel: UILabel!
    @IBOutlet weak var errorTitleLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var netwokSettingsButton: UIButton!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    
    weak var delegate: BANoInternetOnPlayerViewDelegate?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUp() {
        Bundle.main.loadNibNamed(String(describing: BANoInternetOnPlayerView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.fillParentView()
        contentView.backgroundColor = .BAdarkBlueBackground
        netwokSettingsButton.makeCircular(roundBy: .height)
        retryButton.makeCircular(roundBy: .height)
    }
    
    func configureConstraint(isPortrait: Bool) {
        imageViewTopConstraint.constant = isPortrait ? 30 : 68
    }

    @IBAction func networrkSettingButtonAction(_ sender: Any) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }

    @IBAction func retryButtonAction(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.retryAction()
    }

    @IBAction func backButtonActtion(_ sender: Any) {
        delegate?.popToDetailScreen()
    }
}

//
//  BAFullScreenMoviePlayerView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import AVKit
import VideoPlayer
import Mixpanel
import Kingfisher
import Foundation
import Firebase
import ARSLineProgress
import CommonCrypto

protocol BAFullScreenMoviePlayerViewDelegate: class {
     func makeContinueWatchCall(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?)
    func popToParent()
    func addRemoveToWatchlistFromFullScreen()
}

class BAFullScreenMoviePlayerView: UIView, BAAutoPlayProtocol {
    
    @IBOutlet weak var playyerEndImageView: UIImageView!
    @IBOutlet weak var playNextVideoButton: UIButton!
    @IBOutlet weak var previousVideoButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var zoomInOutButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var audioTrackButton: UIButton!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var progressSegue: BACustomSliderView!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var playerTitle: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var addToFavButton: UIButton!
    @IBOutlet weak var replayLabel: UILabel!
    var isuserMovedBack: Bool = false
    weak var delegate: BAFullScreenMoviePlayerViewDelegate?
    var selectedIndex: Int?
    var contentDetail: ContentDetailData?
    var coverImage: String?
    var nextEpisodeData: BANextPreviousEpisodeModel?
    var episode: Episodes?
    var contentType: String?
    var isPlayerInBackground = false
    var isAlertShownOnce: Bool = false
    var cookieString: String = ""
    var vootPlaybackViewModal: BAVootViewModal?
    var startTime: String?
    var stopTime: String?
    var continueWatchTimer: Timer?
    var currentProvider: String?
    var nextEpisodeVM: BANextPreviousEpisodeVM?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var addRemoveWatchlistVM: BAAddRemoveWatchlistViewModal?
    var isVideoEnded: Bool = false
    var isEpisodeCompleted: Bool = false
    var player: TTNFairPlayer
    var timeObserver: AnyObject!
    var sliderTimer: Timer?
    var bufferTimer: Timer?
    var isSliderSeek = false
    var isPlayerPlaying = true
    var isAddedToFavourite: Bool?
    var isAddedToFavForEpisodes = false
    var isEpisode: Bool?
    var isReplay: Bool?
    var selectedAudiotrack = 0
    var selectedSubtitle = 0
    var selectedAudioTrackName = ""
    var selectedSubtitleTrackName = ""
    var isAudioOrVideoQualityOpenend = false
    var playerVideoTitle: String?
    var contentId: String?
    var videoId: Int?
    var contentVideoType: String?
    var bufferTime = 0
    var nextIndex = 0
    var resumeCount = 0
    var pauseCount = 0
    var videoQualityArr: [String] = []
    var previousPlayeingState: Bool?
    var selectedQualityVideoName = ""
    var selectedQualityIndex = 0
    var quatityInfoArray: [[String: Any]] = []
    private let url: URL
    var noInternetimer: Timer?
    fileprivate let seekDuration: Float64 = 10
    var seekedDuration: Float = 0.0
    var isZoomedIn: Bool = false {
        didSet {
            let imageName: String = isZoomedIn ? "zoomOut" : "zoomIn"
            zoomInOutButton.setImage(UIImage(named: imageName), for: .normal)
        }
    }
    
    init(frame: CGRect, url: URL, cookie: String, content: ContentDetailData?, episodeData: Episodes?, isReplayContent: Bool?, videoContentType: String?, contentId: Int?) {
        self.url = url
        contentDetail = content
        episode = episodeData
        isReplay = isReplayContent
        videoId = contentId
        contentVideoType = videoContentType
        player = TTNFairPlayer.init(withFrame: frame)
        super.init(frame: frame)
        if cookie == "" {
            let urlAsset = AVURLAsset(url: url)
            player.prepareToPlay(withAsset: urlAsset)
        } else {
            let result = convertStringToDictionary(text: cookie)
            var cookies = [HTTPCookie]()
            if let cookie = result as? [String: String] {
                for key in cookie.keys {
                    let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                    let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url)
                    cookies.append(contentsOf: cookie)
                }
            }
            let values = HTTPCookie.requestHeaderFields(with: cookies)
            let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
            let urlAsset = AVURLAsset(url: url, options: cookieOptions)
            player.prepareToPlay(withAsset: urlAsset)
        }
        setup()
        playyerEndImageView.isHidden = true
        startBufferTimer()
        addObserver()
        player.hideSubtitle()
        startWatchingTime()
        //configurePlayer(isNextPreviousEpisode: true)
        contentView.backgroundColor = UIColor(white: 1, alpha: 0.0)
        kAppDelegate.window?.self.isUserInteractionEnabled = true
        noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
        continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(makeContinueWatchingCall), userInfo: nil, repeats: true)
        self.backgroundColor = .black
        if player.isPlayerPlaying {
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        } else {
            showControls()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    deinit {
        NotificationCenter.default.removeObserver(self, name: .notificationPackSubscription, object: nil)
    }
     */
    
    override func layoutSubviews() {
        hideControls()
        progressSegue.delegate = self
        if isPlayerPlaying == true {
            if player.allAudioTracks.count > 1 {
                player.changeAudioTrackWith(index: selectedAudiotrack)
            }
            if player.allSubTitles.count > 1 {
            }
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            if player.allAudioTracks.count > 1 {
                player.changeAudioTrackWith(index: selectedAudiotrack)
            }
            self.pausePlayer()
        }
        player.layer?.frame = bounds
        if isAudioOrVideoQualityOpenend {
            showControls()
            self.setVideoQuality(selectedIndex: selectedQualityIndex, selectedTite: selectedQualityVideoName)
        } else {
            if isVideoEnded {
            } else if isPlayerInBackground {
                playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
                //isPlayerInBackground = false
            } else {
                print("----------------------------------2")
            }
        }
    }
    
    func fetchWatchlistLookUp() {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentVideoType ?? "", contentId: "\(videoId ?? 0)")
        confgureWatchlistIcon()
    }

    func confgureWatchlistIcon() {
        if let dataModel = watchlistLookupVM {
            dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                if flagValue {
                    if dataModel.watchlistLookUp?.data?.isFavourite ?? false {
                        self.addToFavButton.isSelected = true
                        self.isAddedToFavourite = true
                        self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                        self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
                    } else {
                        self.addToFavButton.isSelected = false
                        self.isAddedToFavourite = false
                        self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                        self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                    }
                } else if isApiError {
                    // Do Nothing
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    func updateSubtitleTrack() {
        if player.allSubTitles.count >= 1 {
            if selectedSubtitle == 0 {
                player.hideSubtitle()
            } else {
                player.changeSubtitleTrackWith(index: selectedSubtitle - 1)
            }
        } else if player.allSubTitles.count == 0 {
            player.hideSubtitle()
        }
    }
    
    func startBufferTimer() {
        bufferTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateBufferTimer), userInfo: nil, repeats: true)
    }
    
    @objc func showNoInternetView() {
        if BAReachAbility.isConnectedToNetwork() {
            // Do Nothing
        } else {
            if noInternetimer != nil {
                noInternetimer?.invalidate()
                noInternetimer = nil
            }
            previousPlayeingState = player.isPlayerPlaying
            isPlayerPlaying = false
            //player.pause() //Not working
            CustomLoader.shared.hideLoader()
            let view = BANoInternetOnPlayerView(frame: .zero)
            addSubview(view)
            view.fillParentView()
            view.delegate = self
        }
    }
    
    @objc func showExpiryAlert() {
        if continueWatchTimer != nil {
            continueWatchTimer?.invalidate()
            continueWatchTimer = nil
            player.stop()
            player.isMuted = true
            hideControls()
            DispatchQueue.main.async {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(""), .withMessage(kSessionExpire), .hidden, .hidden)) { _ in
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
            }
        }
    }
    
    @objc func updatePlayingStatus() {
        isPlayerInBackground = true
        //player.isMuted = false
    }
    
    @objc func configureContentPlayback() { 
        player.play()
        showControls()
        playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if self.player.isPlayerPlaying {
                self.hideControls()
            }
        }
    }
    
    @objc func updateBufferTimer() {
        bufferTime += 1
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateSubscribedApps), name: .notificationPackSubscription, object: nil)
    }
    
    @objc func updateSubscribedApps(_ notification: NSNotification) {
        UtilityFunction.shared.performTaskInMainQueue {
            self.checkForProviderSubscription()
        }
    }
    
    func checkForProviderSubscription() {
        if let _ = contentDetail {
            var appName = ""
            if contentDetail?.meta?.provider == "Sun_Nxt" || contentDetail?.meta?.provider == "Sun_nxt" || contentDetail?.meta?.provider == "sun_Nxt" || contentDetail?.meta?.provider == "sun_nxt"  {
                appName = "SunNxt"
            }
            var subscribedApp = "Sun"
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name == "Sun_Nxt"}) ?? false {
                subscribedApp = "SunNxt"
            }
            if !(BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name?.uppercased() == contentDetail?.meta?.provider?.uppercased()}) ?? false) || appName != subscribedApp {
                pausePlayer()
                showSubscriptionExpiredAlert()
            }
        } else if let episodeData = episode {
            var appName = ""
            if episodeData.provider == "Sun_Nxt" || episodeData.provider == "Sun_nxt" || episodeData.provider == "sun_Nxt" || episodeData.provider == "sun_nxt"  {
                appName = "SunNxt"
            }
            var subscribedApp = "Sun"
            if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name == "Sun_Nxt"}) ?? false {
                subscribedApp = "SunNxt"
            }
            if !(BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.contains(where: {$0.name?.uppercased() == episodeData.provider?.uppercased()}) ?? false) || appName != subscribedApp {
                pausePlayer()
                showSubscriptionExpiredAlert()
            }
            
        }
    }
    
    func pausePlayer() {
        self.player.pause()
        self.playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
    }
    
    func showSubscriptionExpiredAlert() {
        continueWatchTimer?.invalidate()
        continueWatchTimer = nil
        player.stop()
        player.isMuted = true
        hideControls()
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("No Subscription"), .withMessage("Your Subscription Expired, Please Subscribe Content To Enjoy Uninterrupted Service"), .hidden, .hidden)) { _ in
                self.backToDetailScreen()
            }
        }
    }
    
    func configureVootPlayer(episodeDetail: Episodes?) {
        if contentDetail != nil && contentDetail?.lastWatch != nil {
            vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: contentDetail?.lastWatch?.providerContentId, contentType: contentDetail?.lastWatch?.contentType, partner: contentDetail?.lastWatch?.provider)
        } else {
            vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: episodeDetail?.providerContentId, contentType: episodeDetail?.contentType, partner: episodeDetail?.provider)
        }
        configureVootStream(episodeDetail: episodeDetail)
    }
    
    func configureVootStream(episodeDetail: Episodes?) {
        if let dataModel = vootPlaybackViewModal {
            DispatchQueue.main.async {
                CustomLoader.shared.showLoader(false)
            }
            playNextVideoButton.isUserInteractionEnabled = false
            dataModel.makePlayabaleContentCall {(flagValue, message, isApiError) in
                CustomLoader.shared.hideLoader()
                if flagValue {
                    self.playNextVideoButton.isUserInteractionEnabled = true
                    print(self.vootPlaybackViewModal?.playbackData)
                    if self.vootPlaybackViewModal?.playbackData != nil {
                        self.configureVootPlayback(vootPlayback: self.vootPlaybackViewModal?.playbackData, episodeDetail:episodeDetail)
                    } else {
                    }
                } else if isApiError {
                    //self.apiError(message, title: kSomethingWentWrong)
                } else {
                    if self.isAlertShownOnce == false {
                        self.isAlertShownOnce = true
                        self.continueWatchTimer?.invalidate()
                        self.continueWatchTimer = nil
                        self.player.stop()
                        self.player.isMuted = true
                        self.hideControls()
                        DispatchQueue.main.async {
                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                                self.backToDetailScreen()
                            }
                        }
                    } else {
                        
                    }
                }
            }
        }
    }
    
    func configureVootPlayback(vootPlayback: VootPlaybackData?, episodeDetail: Episodes?) {
        let playableUrl = vootPlayback?.url ?? ""
        if playableUrl.isEmpty {
            DispatchQueue.main.async {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                    self.backToDetailScreen()
                }
            }
        } else {
            let kTataSkyFPSCertificateURL = vootPlayback?.drm?.first?.licenseURL ?? ""
            TTNPlayerConfiguration.sharedInstance.fpsCertificateUrl(kTataSkyFPSCertificateURL)
            TTNPlayerConfiguration.sharedInstance.drmInfo(data: vootPlayback?.drm?.first?.certificate ?? "", drmType: .kaltura)
            let urlAsset = AVURLAsset(url: URL(string: playableUrl)!)
            self.player.prepareToPlay(withAsset: urlAsset)
            self.hideControls()
            self.progressSegue.delegate = self
            continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(makeContinueWatchingCall), userInfo: nil, repeats: true)
            self.configurePlayer(isNextPreviousEpisode: true)
        }
        
    }
    
    func startWatchingTime() {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        startTime = formattedDate
    }
    
    func stopWatchingTime() {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        stopTime = formattedDate
    }
    
    // MARK: Configure Player to resume Or
    func configurePlayer(isNextPreviousEpisode: Bool?) {
//            DispatchQueue.main.async {
//                CustomLoader.shared.showLoader(true)
//            }
            fetchWatchlistLookUp()
            if contentDetail != nil {
                if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue {
                    // Show Default Options
                } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                    if self.url == nil {
                        // Show Default Options
                    } else if self.url.absoluteString == nil || self.url.absoluteString.isEmpty {
                        // Show Default Options
                    } else {
                        getResolutionInfo(streamUrl: self.url.absoluteString)
                    }
                } else {
                    getResolutionInfo(streamUrl: self.url.absoluteString)
                }
            } else if episode?.provider?.uppercased() == ProviderType.hungama.rawValue {
                
            } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue {
                
            } else if episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                if self.url == nil {
                    // Show Default Options
                } else if self.url.absoluteString == nil || self.url.absoluteString.isEmpty {
                    // Show Default Options
                } else {
                    getResolutionInfo(streamUrl: self.url.absoluteString)
                }
            } else {
                getResolutionInfo(streamUrl: self.url.absoluteString)
            }
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
       // CustomLoader.shared.hideLoader()
            player.play()
            if isNextPreviousEpisode ?? false {
                showControls()
            } else {
                hideControls()
            }
            if contentDetail != nil {
                if contentDetail?.lastWatch != nil && isReplay == false {
                    let seconds: Int64 = Int64((contentDetail?.lastWatch?.watchedDuration ?? 0))
                    let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
                    let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
                    player.seek(to: TimeInterval(targetTimeInterval))
                    if contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                        getNextEpisodeData(contentId: contentDetail?.lastWatch?.id)
                    }
                } else {
                    player.seek(to: 0)
                    if contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                        getNextEpisodeData(contentId: contentDetail?.lastWatch?.id)
                    }
                }
            } else {
                if isEpisodeCompleted {
                    isEpisodeCompleted = false
                    player.seek(to: 0)
                } else {
                    if episode?.watchedDuration != nil && isReplay == false {
                        let seconds: Int64 = Int64((Int(episode?.watchedDuration ?? "0") ?? 0))
                        let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
                        let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
                        player.seek(to: TimeInterval(targetTimeInterval))
                    } else {
                        player.seek(to: 0)
                    }
                }
                getNextEpisodeData(contentId: episode?.id)
            }
    }
    
    // MARK: Get Next/Prev Episode Data
    func getNextEpisodeData(contentId: Int?) {
        nextEpisodeVM = BANextPreviousEpisodeVM(repo: BANextPreviousEpisodeRepo(), endPoint: "info", id: contentId ?? 0)
        if let nextEpiVM = nextEpisodeVM {
            nextEpiVM.getNextPreviousEpisodeData { (flagValue, _) in
                if flagValue {
                    self.nextEpisodeData = nextEpiVM.nextPreviousData
                    self.configurePrevNextButton()
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    // MARK: Configure Prev/Next Button
    func configurePrevNextButton() {
        fetchWatchlistLookUp()
        if contentDetail?.lastWatch != nil {
            self.playerTitle.text = contentDetail?.lastWatch?.title
        } else {
            self.playerTitle.text = episode?.title
        }
        
        if self.nextEpisodeData?.data?.nextEpisodeExists ?? false && self.audioTrackButton.isHidden == false {
            self.playNextVideoButton.isHidden = false
        } else {
            self.playNextVideoButton.isHidden = true
        }
        
        if self.nextEpisodeData?.data?.previousEpisodeExists ?? false && self.audioTrackButton.isHidden == false {
            self.previousVideoButton.isHidden = false
        } else {
            self.previousVideoButton.isHidden = true
        }
    }
    
    // MARK: Show Player Controls
    func showControls() {
        contentView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        progressSegue.isHidden = false
        settingsButton.isHidden = false
        audioTrackButton.isHidden = false
        let seconds: Int64 = Int64(progressSegue.value)
        totalTimeLabel.isHidden = false
        if self.player.playbackState == TTNFairPlayerPlaybackState.seeking {
            DispatchQueue.main.async {
                CustomLoader.shared.showLoader(true)
            }
        } else {
            CustomLoader.shared.hideLoader()
            forwardButton.isHidden = false
            playPauseButton.isHidden = false
            if seconds < 10 {
                rewindButton.isHidden = true
            } else {
                rewindButton.isHidden = false
            }
        }
        addToFavButton.isHidden = false
        backButton.isHidden = false
        playerTitle.isHidden = false
        zoomInOutButton.isHidden = false
            if nextEpisodeData?.data != nil && nextEpisodeData?.data?.previousEpisodeExists ?? false {
                previousVideoButton.isHidden = false
            } else {
                previousVideoButton.isHidden = true
            }
            
            if nextEpisodeData?.data != nil && nextEpisodeData?.data?.nextEpisodeExists ?? false {
                playNextVideoButton.isHidden = false
            } else {
                playNextVideoButton.isHidden = true
            }
    }
    
    // MARK: Hide Player Controls
    func hideControls() {
        contentView.backgroundColor = UIColor(white: 1, alpha: 0.0)
        progressSegue.isHidden = true
        settingsButton.isHidden = true
        audioTrackButton.isHidden = true
        totalTimeLabel.isHidden = true
        forwardButton.isHidden = true
        rewindButton.isHidden = true
        playPauseButton.isHidden = true
        addToFavButton.isHidden = true
        backButton.isHidden = true
        zoomInOutButton.isHidden = true
        playerTitle.isHidden = true
        playNextVideoButton.isHidden = true
        previousVideoButton.isHidden = true
        progressSegue.sliderToolTip.alpha = 0.0
        progressSegue.showToolTipViewAnimated(animated: false)
    }
    
    func configureFavButton() {
        if isAddedToFavourite ?? false {
            self.addToFavButton.isSelected = true
            self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
            self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
        } else {
            self.addToFavButton.isSelected = false
            self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
            self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
        }
    }
    
    @IBAction func settingsButtonAction(_ sender: Any) {
        CustomLoader.shared.hideLoader()
        //loaderView.isHidden = true
        isAudioOrVideoQualityOpenend = true
        previousPlayeingState = player.isPlayerPlaying
        isPlayerPlaying = false
        let view = BAVideoQualityView(frame: .zero)
        if videoQualityArr.isEmpty {
            view.videoQualityArray = ["Auto", "High", "Medium", "Low"]
        } else {
            videoQualityArr = videoQualityArr.removeDuplicates()
            view.videoQualityArray = videoQualityArr
        }
        if selectedQualityVideoName.isEmpty {
            view.selectedQualityName = view.videoQualityArray[0]
        } else {
            view.selectedQualityName = selectedQualityVideoName
        }
        addSubview(view)
        view.fillParentView()
        view.delegate = self
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
       // CustomLoader.shared.hideLoader()
        backToDetailScreen()
    }
    
    @IBAction func zoomInOutButtonAction(_ sender: Any) {
        if isZoomedIn {
            player.layer?.videoGravity = .resizeAspect
        } else {
            player.layer?.videoGravity = .resizeAspectFill
        }
        isZoomedIn = !isZoomedIn
    }
    
    
    func backToDetailScreen() {
        CustomLoader.shared.hideLoader()
        NotificationCenter.default.removeObserver(self, name: .notificationPackSubscription, object: nil)
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        removeObservers()
        stopWatchingTime()
        sliderTimer?.invalidate()
        sliderTimer = nil
        continueWatchTimer?.invalidate()
        continueWatchTimer = nil
        player.pause()
        player.isMuted = true
        let title = contentDetail?.meta?.vodTitle ?? ""
        let contentGenre = contentDetail?.meta?.genre ?? []
        let videoContentType = contentDetail?.meta?.contentType ?? ""
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: contentGenre, MixpanelConstants.ParamName.startTime.rawValue: startTime ?? "", MixpanelConstants.ParamName.stopTime.rawValue: stopTime ?? "", MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: bufferTime / 60, MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: bufferTime, MixpanelConstants.ParamName.numberOfResume.rawValue: resumeCount, MixpanelConstants.ParamName.numberOfPause.rawValue: pauseCount, MixpanelConstants.ParamName.duraionMinunte.rawValue: convertToEventTime(totalTimeLabel.text ?? "", "Minutes"), MixpanelConstants.ParamName.durationSeconds.rawValue: convertToEventTime(totalTimeLabel.text ?? "", "Seconds"),MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
        delegate?.popToParent()
    }
    
    func dismissAlert() {
        
    }
    
    func convertToEventTime(_ time: String, _ conversionType: String) -> String {
        let delimiter = ":"
        var expectedTime = 0
        let timeArr = time.components(separatedBy: delimiter)
        if timeArr.count > 2 {
            expectedTime = (Int(timeArr[0]) ?? 0)*60
            expectedTime += expectedTime + (Int(timeArr[1]) ?? 0)
            if conversionType == "Seconds" && timeArr.indices.contains(2){
                expectedTime = expectedTime*60  + (Int(timeArr[2]) ?? 0)
            }
        }
        return String(expectedTime)
    }
    
    @IBAction func addToWatchlistButtonAction(_ sender: Any) {
        addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentVideoType ?? "", contentId: videoId ?? 0)
        addRemoveFromWatchlist()
//        if episode != nil {
//            addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAUserDefaultManager().baId, contentType: contentVideoType ?? "", contentId: episode?.id ?? 0)
//            addRemoveFromWatchlist()
//        } else {
//            if contentDetail != nil && contentDetail?.lastWatch != nil {
//                addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAUserDefaultManager().baId, contentType: contentVideoType ?? "", contentId: contentDetail?.lastWatch?.id ?? 0)
//                addRemoveFromWatchlist()
//            } else {
//                var vodId = 0
//                switch contentVideoType {
//                case ContentType.series.rawValue:
//                    vodId = contentDetail?.meta?.seriesId ?? 0
//                case ContentType.brand.rawValue:
//                    vodId = contentDetail?.meta?.brandId ?? 0
//                default:
//                    vodId = contentDetail?.meta?.vodId ?? 0
//                }
//                addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAUserDefaultManager().baId, contentType: contentVideoType ?? "", contentId: videoId)
//                addRemoveFromWatchlist()
//            }
//        }
    }
    
    @IBAction func rewindButtonAction(_ sender: Any) {
        
        if player.isPlayerPlaying == true {
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        } else {
            self.pausePlayer()
        }
        let currentCMTime = CMTime(seconds: player.currentPlaybackTime, preferredTimescale: 1000000)
        let playerCurrentTime = CMTimeGetSeconds(currentCMTime)
        var newTime = playerCurrentTime - seekDuration
        
        if newTime < 0 {
            newTime = 0
        }
        
        if newTime < 10 {
            rewindButton.isHidden = true
        } else {
            rewindButton.isHidden = false
        }
        let rewindTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        let rewindTimeInterval = Float(CMTimeGetSeconds(rewindTime))
        player.seek(to: TimeInterval(rewindTimeInterval))
    }
    
    @IBAction func forwardButtonAction(_ sender: Any) {
        
        if player.isPlayerPlaying == true {
            do {
               try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        } else {
            self.pausePlayer()
        }
        
        let durationtimeIntvl = player.duration
        let durationCmTime = CMTime(seconds: durationtimeIntvl, preferredTimescale: 1000000)
        let currentCMTime = CMTime(seconds: player.currentPlaybackTime, preferredTimescale: 1000000)
        let playerCurrentTime = CMTimeGetSeconds(currentCMTime)
        let newTime = playerCurrentTime + seekDuration
        let timeLeft = CMTimeGetSeconds(durationCmTime) - newTime
        forwardButton.isHidden = false
        if timeLeft < 10 {
            player.seek(to: player.duration)
        } else {
           if newTime < CMTimeGetSeconds(durationCmTime) {
                let forwardTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
                let forwardTimeInterval = Float(CMTimeGetSeconds(forwardTime))
                player.seek(to: TimeInterval(forwardTimeInterval))
                totalTimeLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(forwardTime))
            }
        }
    }
    
    @objc func makeAsFav() {
        self.addToFavButton.isSelected = true
        self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
        self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
    }
    
    @objc func removeFromFav() {
        self.addToFavButton.isSelected = false
        self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
        self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
    }
    
    @IBAction func playButtonAction(_ sender: Any) {
        if isVideoEnded {
            isVideoEnded = false
            showControls()
            seekedDuration = 0.0
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
            replayLabel.isHidden = true
            if contentDetail?.meta?.contentType != ContentType.brand.rawValue && contentDetail?.meta?.contentType != ContentType.series.rawValue {
                seekedDuration = 0.0
                self.player.seek(to: 0)
                do {
                   try AVAudioSession.sharedInstance().setCategory(.playback)
                } catch(let error) {
                    print(error.localizedDescription)
                }
                self.player.play()
            }
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.restartClick.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: playerVideoTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: MixpanelConstants.ParamValue.drama.rawValue, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue])
            if BAReachAbility.isConnectedToNetwork() {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.restartClick.rawValue, properties: [MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.online.rawValue])
            } else {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.restartClick.rawValue, properties: [MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.offline.rawValue])
            }
            playyerEndImageView.isHidden = true
        } else {
            if player.isPlayerPlaying {
                self.pauseCount += 1
                if BAReachAbility.isConnectedToNetwork() {
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: playerVideoTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: contentDetail?.meta?.genre?[0] ?? "", MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.online.rawValue])
                } else {
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: playerVideoTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: contentDetail?.meta?.genre?[0] ?? "", MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.offline.rawValue])
                }
                self.pausePlayer()
            } else {
                self.resumeCount += 1
                do {
                   try AVAudioSession.sharedInstance().setCategory(.playback)
                } catch(let error) {
                    print(error.localizedDescription)
                }
                self.player.play()
                if BAReachAbility.isConnectedToNetwork() {
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.resumeContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: playerVideoTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: MixpanelConstants.ParamValue.drama.rawValue, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.online.rawValue ])
                } else {
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.resumeContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: playerVideoTitle ?? "", MixpanelConstants.ParamName.contentGenre.rawValue: MixpanelConstants.ParamValue.drama.rawValue, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.playingHome.rawValue: MixpanelConstants.ParamValue.offline.rawValue ])
                }
                playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
            }
            
            if player.isPlayerPlaying {
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    if self.player.isPlayerPlaying {
                        self.hideControls()
                    }
                }
            } else {
                showControls()
            }
        }
    }
    
    @IBAction func audioButtonAction(_ sender: Any) {
        CustomLoader.shared.hideLoader()
        isAudioOrVideoQualityOpenend = true
        previousPlayeingState = player.isPlayerPlaying
        isPlayerPlaying = false
        let view = BALanguageSupportView(frame: .zero)
        view.delegate = self
        view.selectedSubtitleTrackIndex = selectedSubtitle
        if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue || episode?.provider?.uppercased() == ProviderType.shemaro.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue || episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            if player.allAudioTracks.count < 1 {
                if selectedAudioTrackName.isEmpty {
                    if contentDetail != nil {
                        view.selectedAudioTrackName = contentDetail?.meta?.audio?[0] ?? ""
                    } else {
                        view.selectedAudioTrackName = episode?.languages?[0] ?? ""
                    }
                } else {
                    view.selectedAudioTrackName = selectedAudioTrackName
                }
                if contentDetail != nil {
                    view.audioTracks = contentDetail?.meta?.audio ?? []
                } else {
                    view.audioTracks = episode?.languages ?? []
                }
            } else {
                for tracks in self.player.allAudioTracks {
                    let locale =  NSLocale(localeIdentifier: tracks.languageCode)
                    if let fullName = locale.localizedString(forLanguageCode: tracks.languageCode) {
                      view.audioTracks.append(fullName)
                    }
                }
                if selectedAudioTrackName.isEmpty {
                    view.selectedAudioTrackName = view.audioTracks[0]
                } else {
                    view.selectedAudioTrackName = selectedAudioTrackName
                }
            }
            if player.allSubTitles.count < 1 {
                view.subtitleTracks = ["None"]
                view.selectedSubtitleTrackName = view.subtitleTracks[0]
            } else {
                view.subtitleTracks = ["None"]
                if self.player.allSubTitles.count >= 1 {
                    // DO Nothing
                    for subtitle in self.player.allSubTitles {
                        if let title = ((subtitle as AnyObject).value(forKey: "title") as? String) {
                           view.subtitleTracks.append(title)
                        }
                    }
                }
                if selectedSubtitleTrackName.isEmpty {
                    view.selectedSubtitleTrackName = view.subtitleTracks[0]
                } else {
                    view.selectedSubtitleTrackName = selectedSubtitleTrackName
                }
            }
        } else {
            view.audioTracks = ["Hindi", "English", "Tamil", "Telgu", "Punjabi", "Bhojpuri"]
            if selectedAudioTrackName.isEmpty {
                view.selectedAudioTrackName = view.audioTracks[0]
            } else {
                view.selectedAudioTrackName = selectedAudioTrackName
            }
            view.subtitleTracks = ["None", "Hindi", "English", "Tamil", "Telgu"]
            if selectedSubtitleTrackName.isEmpty {
                view.selectedSubtitleTrackName = view.subtitleTracks[0]
            } else {
                view.selectedSubtitleTrackName = selectedSubtitleTrackName
            }
        }
        addSubview(view)
        view.fillParentView()
    }
    
    @IBAction func playNextVideoAction(_ sender: Any) {
        contentDetail = nil
        seekedDuration = 0.0
        isAlertShownOnce = false
        episode = self.nextEpisodeData?.data?.nextEpisode
        fetchWatchlistLookUp()
        if episode?.provider?.uppercased() == ProviderType.shemaro.rawValue {
            let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
            let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
            print("md5SignatureHex: \(md5SignatureHex)")
            let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
            self.getPlayableUrl(signedUrl: signedUrl)
        } else if episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
            let result = convertStringToDictionary(text: episode?.playerDetail?.authorizedCookies ?? "")
            let url = URL(string: episode?.playerDetail?.playUrl ?? "");
            var cookies = [HTTPCookie]()
            if let cookie = result as? [String: String] {
                for key in cookie.keys {
                    let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                    let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url!)
                    cookies.append(contentsOf: cookie)
                }
            }
            let values = HTTPCookie.requestHeaderFields(with: cookies)
            let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
            let urlAsset = AVURLAsset(url: url!, options: cookieOptions)
            //let urlAsset = AVURLAsset(url: URL(string: url)!)
            player.prepareToPlay(withAsset: urlAsset)
            self.contentView.isUserInteractionEnabled = true
            hideControls()
            progressSegue.delegate = self
            configurePlayer(isNextPreviousEpisode: true)
        } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            configureVootPlayer(episodeDetail: self.nextEpisodeData?.data?.nextEpisode)
        } else {
            let urlAsset = AVURLAsset(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!)
            player.prepareToPlay(withAsset: urlAsset)
            self.contentView.isUserInteractionEnabled = true
            hideControls()
            progressSegue.delegate = self
            configurePlayer(isNextPreviousEpisode: true)
        }
    }
    
    @IBAction func previousVideoAction(_ sender: Any) {
        contentDetail = nil
        seekedDuration = 0.0
        isAlertShownOnce = false
        seekedDuration = 0.0
        episode = self.nextEpisodeData?.data?.previousEpisode
        fetchWatchlistLookUp()
        if episode?.provider?.uppercased() == ProviderType.shemaro.rawValue {
            let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
            let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
            print("md5SignatureHex: \(md5SignatureHex)")
            let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
            self.getPlayableUrl(signedUrl: signedUrl)
        } else if episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
            let result = convertStringToDictionary(text: episode?.playerDetail?.authorizedCookies ?? "")
            let url = URL(string: episode?.playerDetail?.playUrl ?? "");
            var cookies = [HTTPCookie]()
            if let cookie = result as? [String: String] {
                for key in cookie.keys {
                    let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                    let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url!)
                    cookies.append(contentsOf: cookie)
                }
            }
            let values = HTTPCookie.requestHeaderFields(with: cookies)
            let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
            let urlAsset = AVURLAsset(url: url!, options: cookieOptions)
            //let urlAsset = AVURLAsset(url: URL(string: episode?.playerDetail?.playUrl ?? "")!)
            player.prepareToPlay(withAsset: urlAsset)
            showControls()
            progressSegue.delegate = self
            configurePlayer(isNextPreviousEpisode: true)
        } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            configureVootPlayer(episodeDetail: self.nextEpisodeData?.data?.previousEpisode)
        } else {
            let urlAsset = AVURLAsset(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4")!)
            player.prepareToPlay(withAsset: urlAsset)
            showControls()
            progressSegue.delegate = self
            configurePlayer(isNextPreviousEpisode: true)
        }
    }
    
    func playNextContent(episode: Episodes?) {
        
    }
    
    //Autoplay delegate
    func playNextEpisode(episode: Episodes?) {
        /*
        if isuserMovedBack {
            // Do Nothing
        } else {
            seekedDuration = 0.0
            isAlertShownOnce = false
            replayLabel.isHidden = true
            seekedDuration = 0.0
            playPauseButton.setImage(UIImage(named: "playicon"), for: .normal)
            playyerEndImageView.isHidden = true
            isVideoEnded = false
            contentDetail = nil
            self.episode = episode
            if episode?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
                let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                print("md5SignatureHex: \(md5SignatureHex)")
                let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
                self.getPlayableUrl(signedUrl: signedUrl)
            } else if episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                let result = convertStringToDictionary(text: episode?.playerDetail?.authorizedCookies ?? "")
                let url = URL(string: episode?.playerDetail?.playUrl ?? "");
                var cookies = [HTTPCookie]()
                if let cookie = result as? [String: String] {
                    for key in cookie.keys {
                        let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                        let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url!)
                        cookies.append(contentsOf: cookie)
                    }
                }
                let values = HTTPCookie.requestHeaderFields(with: cookies)
                let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
                let urlAsset = AVURLAsset(url: url!, options: cookieOptions)
                //let urlAsset = AVURLAsset(url: URL(string: episode?.playerDetail?.playUrl ?? "")!)
                player.prepareToPlay(withAsset: urlAsset)
                showControls()
                progressSegue.delegate = self
                self.continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.makeContinueWatchingCall), userInfo: nil, repeats: true)
                configurePlayer(isNextPreviousEpisode: true)
            } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                configureVootPlayer(episodeDetail: episode)
            } else {
                let urlAsset = AVURLAsset(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!)
                player.prepareToPlay(withAsset: urlAsset)
                CustomLoader.shared.hideLoader()
                showControls()
                progressSegue.delegate = self
                configurePlayer(isNextPreviousEpisode: true)
                do {
                   try AVAudioSession.sharedInstance().setCategory(.playback)
                } catch(let error) {
                    print(error.localizedDescription)
                }
                player.play()
            }
        }
        */
    }
    
    // MARK: Get Playabkle Content For Shemaroo Me
    func getPlayableUrl(signedUrl: String?) {
        // Create URL
        let url = URL(string: signedUrl ?? "")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
                do {
                    let shemarooContent = try JSONDecoder().decode(ShemarooContentModal.self, from: data)
                    print(shemarooContent)
                    let playbackUrl = shemarooContent.adaptive_urls?[0].playback_url ?? ""//"http://sample.vodobox.com/planete_interdite/planete_interdite_alternate.m3u8"////
                    if playbackUrl.isEmpty {
                        DispatchQueue.main.async {
                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                                self.backToDetailScreen()
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            let urlAsset = AVURLAsset(url: URL(string: playbackUrl)!)
                            self.player.prepareToPlay(withAsset: urlAsset)
                            self.hideControls()
                            self.progressSegue.delegate = self
                            self.continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.makeContinueWatchingCall), userInfo: nil, repeats: true)
                            self.configurePlayer(isNextPreviousEpisode: true)
                            
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        CustomLoader.shared.hideLoader()
                    }
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    
    
    func watchCredits() {
        //Watch creadits code goes here
    }
    
    func backAction() {
        CustomLoader.shared.hideLoader()
        isuserMovedBack = true
        backButtonAction(self)
    }
    
    // MARK: AddRemoveToWatchlist
    func addRemoveFromWatchlist() {
        if let dataModel = addRemoveWatchlistVM {
            dataModel.addRemoveWatchlist {(flagValue, _, isApiError) in
                if flagValue {
                    if self.addToFavButton.isSelected {
                        self.addToFavButton.isSelected = false
                        self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                        self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: self.contentDetail?.meta?.genre ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? ""])
                    } else {
                        self.addToFavButton.isSelected = true
                        self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                        self.addToFavButton.setTitle(" Added To Watchlist", for: .normal)
                    }
                } else {
                }
            }
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
    func getResolutionInfo(streamUrl: String) {
        var qualityInfo = [String: String]()
        let session = URLSession.shared
        let url = URL(string: streamUrl)!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                return
            }
            print("Status Code for get resolution api : \(httpResponse.statusCode)")
            if error == nil && httpResponse.statusCode == 200 {
                let responseData = String(data: data!, encoding: String.Encoding.utf8)
                var stringArr: [String]? = responseData?.components(separatedBy: "#EXT-X-STREAM-INF:")
                var indexToDelete: [Int] = []
                if let metaDataForVideo = stringArr {
                    for index in 0...metaDataForVideo.count-1 {
                        if !metaDataForVideo[index].contains("RESOLUTION") || (metaDataForVideo[index].contains("#EXT-X-I-FRAME-STREAM-INF:")) {
                            indexToDelete.append(index)
                        }
                    }
                    stringArr?.remove(at: indexToDelete)
                    
                    for index in 0...stringArr!.count - 1 {
                        if let bandwidth = stringArr![index].sliceByString(from: "BANDWIDTH=", to: ",") {
                            qualityInfo["BANDWIDTH"] = bandwidth
                        }
                        if let resolution = stringArr![index].sliceByString(from: "RESOLUTION=", to: ",") {
                            qualityInfo["RESOLUTION"] = resolution
                        }
                        if let quality = stringArr![index].sliceByString(from: "RESOLUTION=", to: "x") {
                            qualityInfo["QUALITY"] = quality
                        }
                        self.quatityInfoArray.append(qualityInfo)
                    }
                    
                    print(self.quatityInfoArray)
                    self.videoQualityArr.removeAll()
                    self.videoQualityArr.append("Auto")
                    for quality in self.quatityInfoArray {
                        self.videoQualityArr.append(quality["QUALITY"] as! String + "p")
                    }
                    print("Video Quality Array is ----->", self.videoQualityArr)
                }
            }
        })
        task.resume()
    }
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using: .utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData
    }
}

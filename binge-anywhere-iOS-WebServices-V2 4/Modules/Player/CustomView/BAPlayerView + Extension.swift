//
//  BAPlayerView + Extension.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 21/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import VideoPlayer
import Mixpanel
import Kingfisher
import Firebase
import ARSLineProgress
import ErosNow_iOS_SDK

extension BAPlayerView {
    // MARK: Setup Player View
    func setup() {
        Bundle.main.loadNibNamed(String(describing: BAPlayerView.self), owner: self, options: nil)
        layer.addSublayer(player.layer!)
        addSubview(contentView)
        contentView.fillParentView()
        timeElapsedLabel.isHidden = true
        if contentDetail == nil && episode == nil {
            //self.titleLabel.text = contentDetail?.lastWatch?.contentTitle
            // This is Trailer Playback
        } else {
            startBufferTimer()
            configureContentPlayback(isNextPreviousEpisode: false)
        }
        player.isMuted = false
        if contentDetail?.lastWatch != nil {
            self.titleLabel.text = contentDetail?.lastWatch?.contentTitle
        } else {
            self.titleLabel.text = episode?.title
        }
        NotificationCenter.default.addObserver(self, selector: #selector(addFavFlag), name: NSNotification.Name(rawValue: "addFav"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeFavFlag), name: NSNotification.Name(rawValue: "removeFav"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeObservers), name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
//        checkForVideogravity()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.progressSegue.addGestureRecognizer(tapGestureRecognizer)
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showHideControls(gestureRecognizer:)))
        self.contentView.addGestureRecognizer(viewTapGestureRecognizer)
        addTargetActions()
        progressSegue.isContinuous = true
        progressSegue.maximumTrackTintColor = UIColor.init(red: 254.0/255.0, green: 254.0/255.0, blue: 254.0/255.0, alpha: 1.0)
        if let data = erosNowContent {
            startPlayback(with: data)
        }
        volumeButtonAction()
    }
    
    func startPlayback(with contentProfile: ENContentProfile) {
        if let streamUrl = contentProfile.streamUrl {
            let assetInfo = ENPlaybackAssetInfo(
                playbackURL: streamUrl.absoluteString,
                assetId: contentProfile.assetId,
                assetTitle: contentProfile.assetTitle,
                contentId: contentProfile.contentId,
                contentType: contentProfile.contentTypeId
            )
            print("this is assest \(assetInfo)")
            print("\(contentProfile.contentId)")
            print("\(contentProfile.assetId)")
            ENSDK.shared.initializePlayer(withAssetInfo: assetInfo, avPlayer: player.player)
            player.play()
        }
    }
    
    
    // MARK: Slider Tap Gesture
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        if !isTrailer && isTrailerFullScreen {
            isSliderSeek = true
            previousPlayeingState = player.isPlayerPlaying
            progressSegue.isUserInteractionEnabled = false
            
            if sliderTimer != nil {
                sliderTimer?.invalidate()
            }
            let pointTapped: CGPoint = gestureRecognizer.location(in: self.contentView)
            let positionOfSlider: CGPoint = progressSegue.frame.origin
            let widthOfSlider: CGFloat = progressSegue.frame.size.width
            let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(progressSegue.maximumValue) / widthOfSlider)
            seekedDuration = Float(newValue) //To handle seekbar jumping on tap
            progressSegue.setValue(Float(newValue), animated: true)
            let seconds: Int64 = Int64(newValue)
            let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
            let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
            player.seek(to: TimeInterval(targetTimeInterval))
            
            handleFullScreenPlayerState(playingState: previousPlayeingState)
            if self.sliderTimer != nil {
                self.sliderTimer?.fire()
                self.progressSegue.isUserInteractionEnabled = true
            }
            self.isSliderSeek = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        } else {
            isSliderSeek = true
            isVideoEnded = false
            previousPlayeingState = player.isPlayerPlaying
            player.pause()
            if sliderTimer != nil {
                sliderTimer.invalidate()
                sliderTimer = nil
            }
            let pointTapped: CGPoint = gestureRecognizer.location(in: self.contentView)
            let positionOfSlider: CGPoint = progressSegue.frame.origin
            let widthOfSlider: CGFloat = progressSegue.frame.size.width
            let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(progressSegue.maximumValue) / widthOfSlider)
            seekedDuration = Float(newValue) //To handle seekbar jumping on tap
            let seconds: Int64 = Int64(newValue)
            let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
            let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
            player.seek(to: TimeInterval(targetTimeInterval))
//            timeElapsedLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(targetTime))
            let timeleft = player.duration - Double(targetTimeInterval)
            if seconds < 10 {
                rewindButton.isHidden = true
            } else {
                rewindButton.isHidden = false
            }
            forwardButton.isHidden = false
            handlePlayerState(playingState: previousPlayeingState)
            if self.sliderTimer != nil {
                self.sliderTimer.fire()
            }
            self.isSliderSeek = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        }
     
    }
    
    // MARK: Notification for Show/Hide Controls
    @objc func showHideControls(gestureRecognizer: UIGestureRecognizer) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(configureControls), object: nil)
        perform(#selector(configureControls), with: nil, afterDelay: 0.5)
    }
    
    @objc func configureControls() {
        if fullScreenButton.isHidden == true {
            if player.isPlayerPlaying == true {
                showControls()
                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    if self.player.isPlayerPlaying {
                        self.hideControls()
                    }
                }
                self.player.play()
                if isVideoEnded {
                    playButton.setImage(UIImage(named: "replay"), for: .normal)
                    forwardButton.isHidden = true
                } else {
                    playButton.setImage(UIImage(named: "playicon"), for: .normal)
                }
            } else {
                showControls()
                self.player.pause()
                if isVideoEnded {
                    playButton.setImage(UIImage(named: "replay"), for: .normal)
                    forwardButton.isHidden = true
                } else {
                    playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
                }
            }
            
        } else {
            hideControls()
        }
    }
    
    // MARK: Convert total duration in mm:ss format
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        if interval.isNaN {
            return ""
        } else {
            let interval = Int(interval)
            let seconds = interval % 60
            let minutes = (interval / 60) % 60
            let hours = interval / 3600
            if hours > 0 {
                return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
            } else {
                return String(format: "%02d:%02d", minutes, seconds)
            }
        }
    }
    
    
    
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                changeValueBeganForSlider()
                // handle drag began
                break
            case .moved:
                valueChangedForSlider()
                // handle drag moved
                break
            case .ended:
                self.eventEndedForSlider()
                // handle drag ended
                break
            default:
                break
            }
        }
    }
    
    func changeValueBeganForSlider() {
        print("Player value changed")
        previousPlayeingState = player.isPlayerPlaying
        player.pause()
        isSliderSeek = true
        if sliderTimer != nil {
            sliderTimer.invalidate()
            sliderTimer = nil
        }
        if player.isPlayerPlaying == true {
            self.player.play()
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.player.pause()
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
    }
    
    func valueChangedForSlider() {
        print("Player value changed")
        if sliderTimer != nil {
            sliderTimer.invalidate()
            sliderTimer = nil
        }
        if player.isPlayerPlaying == true {
            self.player.play()
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.player.pause()
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
    }
    
    func eventEndedForSlider() {
        print("Player Reaches End")
        if !isTrailer && isTrailerFullScreen {
            isSliderSeek = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
                if self.isVideoEnded {
                } else {
                    if self.player.isPlayerPlaying {
                        self.hideControls()
                    }
                }
            }
            if previousPlayeingState == true {
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            } else {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
            let seconds: Int64 = Int64(progressSegue.value)
            let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
            let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
            handleFullScreenPlayerState(playingState: previousPlayeingState)
            player.seek(to: TimeInterval(targetTimeInterval))
            if self.sliderTimer != nil {
                self.sliderTimer?.fire()
            }
            self.isSliderSeek = false
        } else {
            isSliderSeek = true
            isVideoEnded = false
            if previousPlayeingState == true {
                self.player.play()
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            } else {
                self.player.pause()
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
            
            let videoDuration = player.duration
          //  let elapsedTime: Float64 = videoDuration * Float64(progressSegue.value)
            let seconds: Int64 = Int64(progressSegue.value)
            let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
            let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
          //  let timeleft = player.duration - Double(targetTimeInterval)
            player.seek(to: TimeInterval(targetTimeInterval))
//            timeElapsedLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(targetTime))
            //updateTimeLabel(elapsedTime: elapsedTime, duration: videoDuration)
            if seconds < 10 {
                rewindButton.isHidden = true
            } else {
                rewindButton.isHidden = false
            }
            forwardButton.isHidden = false
            
            handlePlayerState(playingState: previousPlayeingState)
            if self.sliderTimer != nil {
                self.sliderTimer.fire()
            }
            self.isSliderSeek = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        }
    }
    
    private func handleFullScreenPlayerState(playingState: Bool?) {
        if playingState == true {
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            self.player.play()
        } else {
            self.player.pause()
        }
    }
    
    private func observeTime(elapsedTime: CMTime) {
        let duration = player.duration//CMTimeGetSeconds(player.currentItem!.duration)
        let elapsedTime = CMTimeGetSeconds(elapsedTime)
        updateTimeLabel(elapsedTime: elapsedTime, duration: duration)
    }
    
    
    // elapsed time label is hidden from portrait screen so total time label is showing same time as it is showing on full screen landscape mode updated on bug number TBAA-5588
    private func updateTimeLabel(elapsedTime: Float64, duration: Float64) {
        let elapsedDuration = duration - elapsedTime
        if elapsedDuration > 0 {
            totalTimeLabel.text = self.stringFromTimeInterval(interval: elapsedDuration)
        } else {
            totalTimeLabel.text = "0:00:00"
        }
        //        if isTrailerFullScreen && !isTrailer {
        //            if elapsedDuration > 0 {
        //                totalTimeLabel.text = self.stringFromTimeInterval(interval: elapsedDuration)
        //            } else {
        //                totalTimeLabel.text = "0:00:00"
        //            }
        //        } else {
        ////            totalTimeLabel.text = self.stringFromTimeInterval(interval: duration)
        //            totalTimeLabel.text = self.stringFromTimeInterval(interval: elapsedDuration)
        ////            timeElapsedLabel.text = self.stringFromTimeInterval(interval: elapsedTime)
        //        }
    }
    
    private func handlePlayerState(playingState: Bool?) {
        if playingState == true {
            self.player.play()
        } else {
            self.player.pause()
        }
    }
    
    func setupSlider() {
        if !isSliderSeek {
            progressSegue.minimumValue = 0
            progressSegue.maximumValue = Float(player.duration)
            progressSegue.isContinuous = true
            timerStart()
        }
    }
    
    func timerStart() {
        if self.sliderTimer == nil {
            self.sliderTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
            RunLoop.current.add(self.sliderTimer!, forMode: .common)
        }
    }
    
    @objc func updateSlider() {
        if !isSliderSeek {
            progressSegue.setValue((seekedDuration == 0 ? Float(player.currentPlaybackTime) : seekedDuration) + Float(sliderTimer?.timeInterval ?? 0), animated: true) //If just scrolled the seekbar values will be used until video is buffered
        }
        updateTimeLabel(elapsedTime: player.currentPlaybackTime + sliderTimer.timeInterval, duration: player.duration)
        let _ = player.duration - player.currentPlaybackTime
        if isVideoEnded {
            forwardButton.isHidden = true
        } else {
            if playButton.isHidden == true {
                forwardButton.isHidden = true
            } else {
                forwardButton.isHidden = false
            }
        }
        if isVideoEnded {
            rewindButton.isHidden = true
        } else {
            if player.currentPlaybackTime < 10 {
                rewindButton.isHidden = true
            } else {
                if playButton.isHidden == true {
                    rewindButton.isHidden = true
                } else {
                    rewindButton.isHidden = false
                }
            }
        }
    }
    
    func setVideoQuality(selectedIndex: Int?, selectedTite: String?) {
        if selectedIndex == 0 {
            var bandWidth: Double = 0.0
            if !quatityInfoArray.isEmpty {
                let quality = quatityInfoArray[0]
                let strBandWidth = quality["BANDWIDTH"] as? String ?? "0.0"
                bandWidth = Double(strBandWidth) ?? 500000
            } else {
                bandWidth = 500000.0
            }
            player.setVideoQuality(quality: .auto(value: bandWidth))
        } else {
            let qualityIndex = (selectedIndex ?? 1) - 1
            let quality = qualityIndex < quatityInfoArray.count ? quatityInfoArray[qualityIndex] : ["QUALITY":"Auto"]
            let strBandWidth = quality["BANDWIDTH"] as? String ?? "0.0"
            let bandWidth = Double(strBandWidth) ?? 500000
            player.setVideoQuality(quality: .auto(value: bandWidth))
        }
    }
}


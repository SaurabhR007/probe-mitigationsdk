//
//  BALanguageSupportView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 28/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

//enum AudioTrack {
//    case Hindi
//    case English
//    case Tamil
//    case Telgu
//    case Punjabi
//    case Bhojpuri
//}
//
//enum SubTitleTrack {
//    case None
//    case Hindi
//    case English
//    case Tamil
//    case Telgu
//}

protocol BALanguageSupportViewDelegate: class {
    func popToPlayer(selectedSubTitle: Int?, selectedAudioTrack: Int?, selectedAudioName: String?, selectedSubtitleName: String?)
}

class BALanguageSupportView: UIView {

    @IBOutlet weak var noSupportedAudioLabel: UILabel!
    @IBOutlet weak var subtitleTableView: UITableView!
    @IBOutlet weak var audioTableView: UITableView!
    @IBOutlet weak var subtitlesLabel: UILabel!
    @IBOutlet weak var audioTitleLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dismissButton: UIButton!
    weak var delegate: BALanguageSupportViewDelegate?
    var subtitleTracks = [String]()//["None", "Hindi", "English", "Tamil", "Telgu"]
    var audioTracks = [String]()
    var selectedAudioTrackIndex = 0
    var selectedAudioTrackName: String?
    var selectedSubtitleTrackName: String?
    var selectedSubtitleTrackIndex = 0
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUp() {
        Bundle.main.loadNibNamed(String(describing: BALanguageSupportView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.fillParentView()
        subtitleTableView.delegate = self
        subtitleTableView.dataSource = self
        audioTableView.delegate = self
        audioTableView.dataSource = self
        registerCell()
        contentView.backgroundColor = .BAdarkBlueBackground
        subtitleTableView.backgroundColor = .BAdarkBlueBackground
        audioTableView.backgroundColor = .BAdarkBlueBackground
    }

    func registerCell() {
        UtilityFunction.shared.registerCell(subtitleTableView, kBAPlayerSettingsTableViewCell)
        UtilityFunction.shared.registerCell(audioTableView, kBAPlayerSettingsTableViewCell)
    }

    @IBAction func dismissButtonAction(_ sender: Any) {
        delegate?.popToPlayer(selectedSubTitle: selectedSubtitleTrackIndex, selectedAudioTrack: selectedAudioTrackIndex, selectedAudioName: selectedAudioTrackName, selectedSubtitleName: selectedSubtitleTrackName )
        self.removeFromSuperview()
    }
}

extension BALanguageSupportView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == subtitleTableView {
            return subtitleTracks.count
        } else {
            return audioTracks.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == subtitleTableView {
            let cell = subtitleTableView.dequeueReusableCell(withIdentifier: kBAPlayerSettingsTableViewCell) as? BAPlayerSettingsTableViewCell
            cell?.backgroundColor = .BAdarkBlueBackground
            cell?.configureSubtitleTrackCells(displayName: subtitleTracks[indexPath.row], selectedSubtitleName: selectedSubtitleTrackName)
            return cell!
        } else {
            if audioTracks.isEmpty {
                audioTableView.isHidden = true
                noSupportedAudioLabel.isHidden = false
                return UITableViewCell()
            } else {
                noSupportedAudioLabel.isHidden = true
                audioTableView.isHidden = false
                let cell = audioTableView.dequeueReusableCell(withIdentifier: kBAPlayerSettingsTableViewCell) as? BAPlayerSettingsTableViewCell
                 cell?.backgroundColor = .BAdarkBlueBackground
                 cell?.configureAudioTrackCells(displayName: audioTracks[indexPath.row], selectedAudioName: selectedAudioTrackName)
                // cell?.delegate = self
                 return cell!
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == subtitleTableView {
            let selectedQuality = subtitleTracks[indexPath.row]
            selectedSubtitleTrackIndex = indexPath.row
            selectedSubtitleTrackName = subtitleTracks[indexPath.row]
            subtitleTableView.reloadData()
            print(selectedQuality)
        } else {
            let selectedQuality = audioTracks[indexPath.row]
            selectedAudioTrackIndex = indexPath.row
            selectedAudioTrackName = audioTracks[indexPath.row]
            audioTableView.reloadData()
            print(selectedQuality)
        }
    }
}

//
//  BAPlayerView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 14/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//
import UIKit
import AVKit
import VideoPlayer
import CommonCrypto
import ErosNow_iOS_SDK

protocol BAPlayerViewDelegate: class {
    func moveToFullScreen(isFullScreen: Bool)
    func makeContinueWatchCallForContent(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?)
    func didChangePlayerPlayingState(_ isPlayerPlaying: Bool)
    func configureFavButton(isAddedToFav: Bool?)
    func addRemoveToWatchlistFromFullScreen()
    func videoBeganInReplay()
    func configurePlayButtonOnPI()
    func videoDidFinish()
}

    //Player Error type
    public enum ACPlayerErrorType: Int {
        case SessionFailure = 100202
        case GeoLocationBlockingStream = 130301
        case ProxyError = 130302
        case ConcurrentStreamError = 130401
        case SKEEntitlementError = 0000
    }

    //Player Error code
    public enum ACPlayerHTTPStatusCode: Int {
        case success = 200
        case Error401 = 401
        case Error403 = 403
    }

class BAPlayerView: UIView, BAAutoPlayProtocol {
    
    @objc let player: TTNFairPlayer
    private let url: URL
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var replayLabel: UILabel!
    @IBOutlet weak var markFavButton: UIButton!
    @IBOutlet weak var volumeButton: UIButton!
    @IBOutlet weak var secondVolumeButton: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var fullScreenButton: UIButton!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var timeElapsedLabel: UILabel! // TBAA-5184 now is hidden
    @IBOutlet weak var progressSegue: BACustomSliderView!
    @IBOutlet weak var addToFavButton: UIButton!
    @IBOutlet weak var audioTrackButton: UIButton!
    var bufferTimer: Timer?
    var bufferTime = 0
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var playNextVideoButton: UIButton!
    @IBOutlet weak var zoomInOutButton: UIButton!
    @IBOutlet weak var previousVideoButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomActionsView: UIView!
    @IBOutlet weak var volumeButtonContainer: UIView!
    @IBOutlet weak var volumeButtonContainerWidthConstraint: NSLayoutConstraint!
    
    var addRemoveWatchlistVM: BAAddRemoveWatchlistViewModal?
    var nextEpisodeVM: BANextPreviousEpisodeVM?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var nextEpisodeData: BANextPreviousEpisodeModel?
    var contentDetail: ContentDetailData?
    var vootPlaybackViewModal: BAVootViewModal?
    var isScreenRecordingPopUpShown = false
    var contentType: String?
    var episode: Episodes?
    var startTime: String?
    var stopTime: String?
    var isAlertShownOnce: Bool = false
    var isEpisodeCompleted: Bool = false
    var isPlayerInBackground = false
    var isEpisode: Bool?
    var continueWatchTimer: Timer?
    var isReplay: Bool?
    var videoId: Int?
    var selectedAudiotrack = 0
    var resumeCount = 0
    var pauseCount = 0
    var selectedSubtitle = 0
    var selectedAudioTrackName = ""
    var selectedSubtitleTrackName = ""
    var isAudioOrVideoQualityOpenend = false
    var playerVideoTitle: String?
    var contentVideoType: String?
    var isPlayerFullScreen = true
    var isTrailerFullScreen = false
    var isVideoEnded = false
    var isAddedToFavourite: Bool?
    var autoPlayView: BAAutoPlay?
    var noInternetView: BANoInternetOnPlayerView?
    var removeDeviceView: BAUserRemoveView?
    var languageSupportView: BALanguageSupportView?
    var videoQualityView: BAVideoQualityView?
    var isVolume = BAKeychainManager().isVolume
    var previousPlayeingState: Bool?
    var selectedQualityVideoName = ""
    var selectedQualityIndex = 0
    var startTimeDate: Date?
    var stopTimeDate: Date?
    var noInternetimer: Timer?
    var videoQualityArr: [String] = []
    var quatityInfoArray: [[String: Any]] = []
    var isNextEpisodeButtonVisible: Bool = false
    var erosNowContent: ENContentProfile?
    var erosNowLoginAttempt = 0
    var isZoomedIn: Bool = false {
        didSet {
            let imageName: String = isZoomedIn ? "zoomOut" : "zoomIn"
            zoomInOutButton.setImage(UIImage(named: imageName), for: .normal)
        }
    }
    
    var isPlayerPlaying = true {
        didSet {
            delegate?.didChangePlayerPlayingState(isPlayerPlaying)
        }
    }
    
    var elapsedTimeAfterForward: CMTime?
    weak var delegate: BAPlayerViewDelegate?
    fileprivate let seekDuration: Float64 = 10
    var timeObserver: AnyObject!
    var sliderTimer: Timer!
    var isSliderSeek = false
    var seekedDuration: Float = 0.0
    var isTrailer: Bool = true
    
    var isPortrait: Bool {
        return !isTrailerFullScreen//!UIDevice.current.orientation.isLandscape
    }
    
    var lastIndicatedBitrate: Double = 0.0
    var probeInterface:ProbeInterface?
    
    
    init(frame: CGRect, url: URL, cookie: String, content: ContentDetailData?, episodeData: Episodes?, isReplayContent: Bool?, videoContentType: String?, contentId: Int?, isTrailerPlaying: Bool, isEpisodePlaying: Bool, isAddedToFav: Bool, erosNowContent: ENContentProfile?) {
        print("URL SAURABH RODE \(url.absoluteString)")
        self.url = url
        contentDetail = content
        episode = episodeData
        isEpisode = isEpisodePlaying
        isReplay = isReplayContent
        videoId = contentId
        isAddedToFavourite = isAddedToFav
        contentVideoType = videoContentType
        self.erosNowContent = erosNowContent
        player = TTNFairPlayer.init(withFrame: frame)
        let urlAsset = AVURLAsset(url: url)
        self.player.prepareToPlay(withAsset: urlAsset)
       
        super.init(frame: frame)

        if contentDetail == nil && episode == nil {
            noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
            // This is Trailer Playback
        } else {
            startWatchingTime()
            if cookie == "" {
                let urlAsset = AVURLAsset(url: url)
                self.ConfigureProbe()
                player.prepareToPlay(withAsset: urlAsset)
            } else {
                let result = convertStringToDictionary(text: cookie)
                var cookies = [HTTPCookie]()
                if let cookie = result as? [String: String] {
                    for key in cookie.keys {
                        let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                        let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url)
                        cookies.append(contentsOf: cookie)
                    }
                }
                let values = HTTPCookie.requestHeaderFields(with: cookies)
                let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
                let urlAsset = AVURLAsset(url: url, options: cookieOptions)
                player.prepareToPlay(withAsset: urlAsset)
            }
            noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
            continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(makeContinueWatchingCall), userInfo: nil, repeats: true)
        }
        self.isTrailer = isTrailerPlaying
        setup()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if self.player.isPlayerPlaying {
                self.hideControls()
            }
        }
       // self.ConfigAIML()
    }
    
    func ConfigureProbe(){
        String.getCDN(url: self.url)
        var partnerName = ""
        var videoTitle = ""
        var videoContentType = ""
        if contentDetail != nil {
            partnerName = contentDetail?.meta?.provider ?? ""
            videoTitle = contentDetail?.meta?.vodTitle ?? ""
            videoContentType = contentDetail?.meta?.contentType ?? ""
            
        } else {
            partnerName = episode?.provider ?? ""
            videoTitle = episode?.title ?? ""
            videoContentType = contentDetail?.meta?.contentType ?? ""
        }
            
      
        self.probeInterface = ProbeInterface(player: self.player.player, requiredInfo: EventModel(videoID: String(self.videoId ?? 0), contentType: videoContentType, videoTitle: videoTitle, provider: partnerName, drm: "Fairplay", url: self.url))
        
       // self.probeInterface = ProbeInterface(player: self.player.player, requiredInfo: EventModel(videoID:String(self.videoId ?? 0), provider:partnerName, drm:"Fairplay", url:self.url))
        self.probeInterface?.sendEvent(eventType: .PLAYCLICKED)
        self.probeInterface?.configureProbe(videoQuality: true, bitrate: true, stalledCount: true)
  //by Rahuld      //self.probeInterface?.sendEvent(eventType: .ERROR, eventDetailsIfAny:["ErrorCode" :"401", "ErrorName":"AVAuthorization", "ErrorDetails":"Could not recognnized device"])
    }
    
    
    
    func watchCredits() {
        // DO Nothing
    }
    
    func startBufferTimer() {
        bufferTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateBufferTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateBufferTimer() {
        bufferTime += 1
    }
    
    func startWatchingTime() {
        let date = Date()
        let format = DateFormatter()
        startTimeDate = date
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        startTime = formattedDate
    }
    
    func stopWatchingTime() {
        let date = Date()
        let format = DateFormatter()
        stopTimeDate = date
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        stopTime = formattedDate
    }
    
    func backAction() {
        autoPlayView?.backButton.isHidden = true
        autoPlayView?.configureConstraints(isPortrait: true)
        autoPlayView = nil
        if !isTrailer {
//            self.playbackEventTracking() commented this line with coordination of Harsh sir as we are calling this from bavod back button action
        }
        isPlayerPlaying = player.isPlayerPlaying
        if player.isPlayerPlaying == true {
            self.player.play()
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
                fullScreenButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            }
        } else {
            self.player.pause()
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
                fullScreenButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
        delegate?.moveToFullScreen(isFullScreen: isPlayerFullScreen)
        if isTrailerFullScreen {
            isPlayerFullScreen = true
            fullScreenButton.setImage(UIImage(named: "minimise"), for: .normal)
        } else {
            isPlayerFullScreen = false
            fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
        }
    }
    
    func offsetFrom(date: Date) -> Int {

        let dayHourMinuteSecond: Set<Calendar.Component> = [.second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: stopTimeDate ?? Date())

        let seconds = difference.second ?? 0
        if let second = difference.second, second > 0 { return seconds }
        return 0
    }
    
    func playbackEventTracking(railName: String, pageSource: String, configSource: String) {
        if !isTrailer {
            stopWatchingTime()
            if episode != nil {
                let title = "S\(episode?.season ?? 1) E\(episode?.episodeId ?? 1) - \(episode?.title ?? "")"
                let contentGenre = episode?.genres ?? []
                let videoContentType = episode?.contentType ?? ""
                let playbackDuration = offsetFrom(date: startTimeDate ?? Date())
                let genreResult = contentGenre.joined(separator: ",")
                let languageResult = episode?.languages?.joined(separator: ",")
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.startTime.rawValue: startTime ?? "", MixpanelConstants.ParamName.stopTime.rawValue: stopTime ?? "", MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: bufferTime/60, MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: bufferTime, MixpanelConstants.ParamName.numberOfResume.rawValue: resumeCount, MixpanelConstants.ParamName.numberOfPause.rawValue: pauseCount, MixpanelConstants.ParamName.duraionMinunte.rawValue: "\(playbackDuration/60) Minutes", MixpanelConstants.ParamName.durationSeconds.rawValue: "\(playbackDuration) Seconds", MixpanelConstants.ParamName.partnerName.rawValue: episode?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? "", MixpanelConstants.ParamName.vodRail.rawValue: railName, MixpanelConstants.ParamName.origin.rawValue: configSource, MixpanelConstants.ParamName.source.rawValue: pageSource,MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
            } else {
                var title = ""
                if contentDetail?.lastWatch != nil || contentDetail?.lastWatch?.secondsWatched ?? 0 > 0 {
                    title = contentDetail?.lastWatch?.contentTitle ?? ""
                } else {
                   title = contentDetail?.meta?.vodTitle ?? ""
                }
                
                let contentGenre = contentDetail?.meta?.genre ?? []
                let videoContentType = contentDetail?.meta?.contentType ?? ""
                let playbackDuration = offsetFrom(date: startTimeDate ?? Date())
                let genreResult = contentGenre.joined(separator: ",")
                let languageResult = contentDetail?.meta?.audio?.joined(separator: ",")
                let partnerName = contentDetail?.meta?.provider ?? ""
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentType.rawValue: videoContentType, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.startTime.rawValue: startTime ?? "", MixpanelConstants.ParamName.stopTime.rawValue: stopTime ?? "", MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: bufferTime/60, MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: bufferTime, MixpanelConstants.ParamName.numberOfResume.rawValue: resumeCount, MixpanelConstants.ParamName.numberOfPause.rawValue: pauseCount, MixpanelConstants.ParamName.duraionMinunte.rawValue: "\(playbackDuration/60) Minutes", MixpanelConstants.ParamName.durationSeconds.rawValue: "\(playbackDuration) Seconds", MixpanelConstants.ParamName.partnerName.rawValue: partnerName, MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? "",MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
            }
        }
    }
    
    func convertToEventTime(_ time: String, _ conversionType: String) -> String {
        let delimiter = ":"
        var expectedTime = 0
        let timeArr = time.components(separatedBy: delimiter)
        if timeArr.count > 2 {
            expectedTime = (Int(timeArr[0]) ?? 0)*60
            expectedTime += expectedTime + (Int(timeArr[1]) ?? 0)
            if conversionType == "Seconds" && timeArr.indices.contains(2){
                expectedTime = expectedTime*60  + (Int(timeArr[2]) ?? 0)
            }
        }
        return String(expectedTime)
    }
    
    override func layoutSubviews() {
        bottomViewHeightConstraint.constant = isPortrait ? 0.0 : 38.0
        totalLabelWidthConstraint.constant = isTrailer ? 43.0 : 70.0
        volumeButtonContainerWidthConstraint.constant = isPortrait ? 0 : 35
        noInternetView?.configureConstraint(isPortrait: isPortrait)
        autoPlayView?.configureConstraints(isPortrait: isPortrait)
        if languageSupportView != nil && isPortrait {
            languageSupportView?.removeFromSuperview()
            languageSupportView = nil
        }
        if videoQualityView != nil && isPortrait {
            videoQualityView?.removeFromSuperview()
            videoQualityView = nil
        }
        hideControls()
        configureFavButton()
        configureBottomFavActionButton()
        progressSegue.delegate = self
        if isVideoEnded {
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            forwardButton.isHidden = true
            fullScreenButton.isHidden = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "syncReplayPIButton"), object: nil)
        } else {
            if isPlayerPlaying == true {
                self.player.play()
                if !isTrailer {
                    //self.resumeCount += 1
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
                }
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            } else {
                self.player.pause()
                if isVideoEnded {
                    playButton.setImage(UIImage(named: "replay"), for: .normal)
                    forwardButton.isHidden = true
                    fullScreenButton.isHidden = true
                } else {
                    if !isTrailer {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                    }
                    playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
                }
            }
        }
        
        if !isTrailer {
            if isPlayerInBackground {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            } else {
                print("----------------------------------3")
            }
        }
        player.layer?.frame = bounds
    }
    
    @objc func addFavFlag() {
        isAddedToFavourite = true
        configureFavButton()
        configureBottomFavActionButton()
    }
    
    @objc func removeFavFlag() {
        isAddedToFavourite = false
        configureFavButton()
        configureBottomFavActionButton()
    }
    
    func configureFavButton() {
        if isAddedToFavourite ?? false {
            self.markFavButton.isSelected = true
            self.markFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
        } else {
            self.markFavButton.isSelected = false
            self.markFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
        }
    }
    
    func configureBottomFavActionButton() {
        if isAddedToFavourite ?? false {
            self.addToFavButton.isSelected = true
            self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
            self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
        } else {
            self.addToFavButton.isSelected = false
            self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
            self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
        }
    }
    
    func checkForVideogravity() {
        player.layer?.videoGravity = isPortrait ? .resizeAspectFill : .resizeAspect
        NotificationCenter.default.addObserver(self, selector: #selector(recordingEnabledState), name: NSNotification.Name(NOTIF_ScreenRecordingEnabled), object: nil)
        self.continueTimer()
    }
    
    func configureContentPlayback(isNextPreviousEpisode: Bool?) {
        fetchWatchlistLookUp()
        if contentDetail != nil {
            if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue {
                // Show Default Options
            } else if contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                if self.url == nil {
                    // Show Default Options
                } else if self.url.absoluteString == nil || self.url.absoluteString.isEmpty {
                    // Show Default Options
                } else {
                    getResolutionInfo(streamUrl: self.url.absoluteString)
                }
            } else {
                getResolutionInfo(streamUrl: self.url.absoluteString)
            }
        } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue {
            
        } else if episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            if self.url == nil {
                // Show Default Options
            } else if self.url.absoluteString == nil || self.url.absoluteString.isEmpty {
                // Show Default Options
            } else {
                getResolutionInfo(streamUrl: self.url.absoluteString)
            }
        } else {
            getResolutionInfo(streamUrl: self.url.absoluteString)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
        } catch(let error) {
            print(error.localizedDescription)
        }
        player.play()
        if isNextPreviousEpisode ?? false {
            showControls()
        } else {
            hideControls()
        }
        if contentDetail != nil {
            if contentDetail?.lastWatch != nil && isReplay == false {
                let seconds: Int64 = Int64((contentDetail?.lastWatch?.secondsWatched ?? 0))
                let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
                let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
                player.seek(to: TimeInterval(targetTimeInterval))
                if contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                    getNextEpisodeData(contentId: contentDetail?.lastWatch?.vodId)
                }
            } else {
                player.seek(to: 0)
                if contentDetail?.lastWatch?.contentType == ContentType.tvShows.rawValue {
                    getNextEpisodeData(contentId: contentDetail?.lastWatch?.vodId)
                }
            }
        } else {
            if isEpisodeCompleted {
                isEpisodeCompleted = false
                player.seek(to: 0)
            } else {
                if episode?.watchedDuration != nil && isReplay == false {
                    let seconds: Int64 = Int64((Int(episode?.watchedDuration ?? "0") ?? 0))
                    let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
                    let targetTimeInterval = Float(CMTimeGetSeconds(targetTime))
                    player.seek(to: TimeInterval(targetTimeInterval))
                } else {
                    player.seek(to: 0)
                }
            }
            getNextEpisodeData(contentId: episode?.id)
        }
    }
    
    // MARK: Get Next/Prev Episode Data
    func getNextEpisodeData(contentId: Int?) {
        nextEpisodeVM = BANextPreviousEpisodeVM(repo: BANextPreviousEpisodeRepo(), endPoint: "info", id: contentId ?? 0)
        if let nextEpiVM = nextEpisodeVM {
            nextEpiVM.getNextPreviousEpisodeData { (flagValue, _) in
                if flagValue {
                    self.nextEpisodeData = nextEpiVM.nextPreviousData
                    self.configurePrevNextButton()
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    // MARK: Configure Prev/Next Button
    func configurePrevNextButton() {
        fetchWatchlistLookUp()
        if contentDetail?.lastWatch != nil {
            self.titleLabel.text = contentDetail?.lastWatch?.contentTitle
        } else {
            self.titleLabel.text = episode?.title
        }
        
        if isTrailer {
            // Do Not show previous episode button
        } else {
            if isTrailerFullScreen {
                if self.nextEpisodeData?.data?.nextEpisodeExists ?? false && self.audioTrackButton.isHidden == false {
                    self.isNextEpisodeButtonVisible = true
                } else {
                    self.isNextEpisodeButtonVisible = false
                }
                
                if self.nextEpisodeData?.data?.previousEpisodeExists ?? false && self.audioTrackButton.isHidden == false {
                    self.previousVideoButton.isHidden = false
                } else {
                    self.previousVideoButton.isHidden = true
                }
            } else {
                previousVideoButton.isHidden = true
            }
        }
    }
    
    func fetchWatchlistLookUp() {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentVideoType ?? "", contentId: "\(videoId ?? 0)")
        confgureWatchlistIcon()
    }
    
    func confgureWatchlistIcon() {
        if let dataModel = watchlistLookupVM {
            dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                if flagValue {
                    if dataModel.watchlistLookUp?.data?.isFavourite ?? false {
                        self.addToFavButton.isSelected = true
                        self.isAddedToFavourite = true
                        self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                        self.addToFavButton.setTitle("Added To Watchlist", for: .normal)
                    } else {
                        self.addToFavButton.isSelected = false
                        self.isAddedToFavourite = false
                        self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                        self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                    }
                } else if isApiError {
                    // Do Nothing
                } else {
                    // Do Nothing
                }
            }
        }
    }
    
    @objc func makeContinueWatchingCall() {
        let erosNowValue = getErosNowContentTotalDuration()
        if contentDetail != nil {
            if contentDetail?.lastWatch != nil {
                if player.currentPlaybackTime.isNaN {
                    
                } else {
                    delegate?.makeContinueWatchCallForContent(contentId: contentDetail?.lastWatch?.vodId, contentType: contentDetail?.lastWatch?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: contentDetail?.lastWatch?.durationInSeconds)
                }
            } else {
                var vodId = 0
                switch contentDetail?.meta?.contentType {
                case ContentType.series.rawValue:
                    vodId = contentDetail?.meta?.seriesId ?? 0
                case ContentType.brand.rawValue:
                    vodId = contentDetail?.meta?.brandId ?? 0
                case ContentType.customBrand.rawValue:
                    vodId = contentDetail?.meta?.brandId ?? 0
                case ContentType.customSeries.rawValue:
                    vodId = contentDetail?.meta?.seriesId ?? 0
                default:
                    vodId = contentDetail?.meta?.vodId ?? 0
                }
                if !player.currentPlaybackTime.isNaN {
                    // Uncomment this line as some times eros now content duration doest not come in our api so in that case used time duration provided by eros now SDK
                    //delegate?.makeContinueWatchCallForContent(contentId: vodId, contentType: contentDetail?.meta?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: contentDetail?.meta?.duration)
                    delegate?.makeContinueWatchCallForContent(contentId: vodId, contentType: contentDetail?.meta?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: erosNowValue.0 ? erosNowValue.1 : contentDetail?.meta?.duration)
                    
                }
            }
        } else {
            if !player.currentPlaybackTime.isNaN {
                delegate?.makeContinueWatchCallForContent(contentId: episode?.id, contentType: episode?.contentType, watchedDuration: Int(player.currentPlaybackTime), totalDuration: erosNowValue.0 ? erosNowValue.1 : Int(episode?.totalDuration ?? "0"))
            }
        }
    }
    
    // Method to get Eros Now Total Duration
    func getErosNowContentTotalDuration() -> (Bool, Int) {
        var totalDuration = 0
        // Eros Now Total Duration
        if let data = erosNowContent {
            totalDuration = data.totalDuration
            return(true, totalDuration)
        }
        return(false, totalDuration)
    }
    
    func checkIsErosNowContent() -> Bool {
        if let _ = erosNowContent {
            return true
        }
        return false
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func cancelTimer() {
        self.checkForAutoPlayTimer()
        guard self.sliderTimer != nil else {
            return
        }
        self.sliderTimer!.invalidate()
        self.sliderTimer = nil
    }
    
    func checkForAutoPlayTimer() {
        if let view = autoPlayView {
            view.cancelTimer()
        }
        guard let subviews = superview?.subviews else {return}
        for view in subviews {
            if view is BAAutoPlay {
                view.removeFromSuperview()
            }
        }
    }
    
    func continueTimer() {
        if self.continueWatchTimer == nil {
            self.continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(makeContinueWatchingCall), userInfo: nil, repeats: true)
            RunLoop.current.add(self.continueWatchTimer!, forMode: .common)
        }
    }
    
    func cancelContinueTimer() {
        guard self.continueWatchTimer != nil else {
            return
        }
        self.continueWatchTimer!.invalidate()
        self.continueWatchTimer = nil
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        backAction()
    }
    
    @IBAction func markFavButtonAction(_ sender: Any) {
        var vodId = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            vodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            vodId = contentDetail?.meta?.brandId ?? 0
        default:
            vodId = contentDetail?.meta?.vodId ?? 0
        }
        addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentDetail?.meta?.contentType ?? "", contentId: vodId)
        addRemoveFromWatchlist()
    }
    
    
    // MARK: Full Screen Button Action
    @IBAction func fullScreenButtonAction(_ sender: Any) {
        isPlayerPlaying = player.isPlayerPlaying
        if player.isPlayerPlaying == true {
            self.player.play()
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            }
        } else {
            self.player.pause()
            if isVideoEnded {
                playButton.setImage(UIImage(named: "replay"), for: .normal)
                forwardButton.isHidden = true
            } else {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
        delegate?.moveToFullScreen(isFullScreen: isPlayerFullScreen)
        if !isPlayerFullScreen {
            isPlayerFullScreen = true
            fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
        } else {
            isPlayerFullScreen = false
            fullScreenButton.setImage(UIImage(named: "minimise"), for: .normal)
        }
    }
    
    // MARK: Volume Button Action
    @IBAction func volumeButtonAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if self.player.isPlayerPlaying {
                self.hideControls()
            }
        }
        if !isVolume {
            volumeButton.setImage(UIImage(named: "volumeup"), for: .normal)
            secondVolumeButton.setImage(UIImage(named: "volumeup"), for: .normal)
            player.isMuted = false
            BAKeychainManager().isVolume = isVolume
            isVolume = true
        } else {
            volumeButton.setImage(UIImage(named: "volumedown"), for: .normal)
            secondVolumeButton.setImage(UIImage(named: "volumedown"), for: .normal)
            player.isMuted = true
            BAKeychainManager().isVolume = isVolume
            isVolume = false
        }
    }
    
    @IBAction func progressSegueButtonAction(_ sender: Any) {
    }
    
    @IBAction func addToFavButton(_ sender: Any) {
        if isTrailer {
            var vodId = 0
            switch contentDetail?.meta?.contentType {
            case ContentType.series.rawValue:
                vodId = contentDetail?.meta?.seriesId ?? 0
            case ContentType.brand.rawValue:
                vodId = contentDetail?.meta?.brandId ?? 0
            default:
                vodId = contentDetail?.meta?.vodId ?? 0
            }
            addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentDetail?.meta?.contentType ?? "", contentId: vodId)
            addRemoveFromWatchlist()
        } else  {
            addRemoveWatchlistVM = BAAddRemoveWatchlistViewModal(repo: AddRemoveWatchlistRepo(), endPoint: "favourite", baId: BAKeychainManager().baId, contentType: contentVideoType ?? "", contentId: videoId ?? 0)
            addRemoveFromWatchlist()
        }
    }
    
    @IBAction func audioButtonAction(_ sender: Any) {
        CustomLoader.shared.hideLoader()
        isAudioOrVideoQualityOpenend = true
        previousPlayeingState = player.isPlayerPlaying
        isPlayerPlaying = false
        languageSupportView = BALanguageSupportView(frame: .zero)
        languageSupportView?.delegate = self
        languageSupportView?.selectedSubtitleTrackIndex = selectedSubtitle
        if contentDetail?.meta?.provider?.uppercased() == ProviderType.shemaro.rawValue || episode?.provider?.uppercased() == ProviderType.shemaro.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.curosityStream.rawValue || episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.vootSelect.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue || contentDetail?.meta?.provider?.uppercased() == ProviderType.eros.rawValue || episode?.provider?.uppercased() == ProviderType.eros.rawValue || contentDetail?.detail?.contractName == "RENTAL" {
            if contentDetail?.meta?.provider?.uppercased() == ProviderType.eros.rawValue || episode?.provider?.uppercased() == ProviderType.eros.rawValue {
                if selectedAudioTrackName.isEmpty {
                    if contentDetail != nil {
                        languageSupportView?.selectedAudioTrackName = contentDetail?.meta?.audio?[0] ?? ""
                    } else {
                        languageSupportView?.selectedAudioTrackName = episode?.languages?[0] ?? ""
                    }
                } else {
                    languageSupportView?.selectedAudioTrackName = selectedAudioTrackName
                }
                if contentDetail != nil {
                    languageSupportView?.audioTracks = contentDetail?.meta?.audio ?? []
                } else {
                    languageSupportView?.audioTracks = episode?.languages ?? []
                }
            } else {
                if player.allAudioTracks.count < 1 {
                    if selectedAudioTrackName.isEmpty {
                        if contentDetail != nil {
                            languageSupportView?.selectedAudioTrackName = contentDetail?.meta?.audio?[0] ?? ""
                        } else {
                            languageSupportView?.selectedAudioTrackName = episode?.languages?[0] ?? ""
                        }
                    } else {
                        languageSupportView?.selectedAudioTrackName = selectedAudioTrackName
                    }
                    if contentDetail != nil {
                        languageSupportView?.audioTracks = contentDetail?.meta?.audio ?? []
                    } else {
                        languageSupportView?.audioTracks = episode?.languages ?? []
                    }
                } else {
                    for tracks in self.player.allAudioTracks {
                        let locale =  NSLocale(localeIdentifier: tracks.languageCode)
                        if let fullName = locale.localizedString(forLanguageCode: tracks.languageCode) {
                            languageSupportView?.audioTracks.append(fullName)
                        }
                    }
                    if selectedAudioTrackName.isEmpty {
                        languageSupportView?.selectedAudioTrackName = languageSupportView?.audioTracks[0]
                    } else {
                        languageSupportView?.selectedAudioTrackName = selectedAudioTrackName
                    }
                }
            }
            if player.allSubTitles.count < 1 {
                languageSupportView?.subtitleTracks = ["None"]
                languageSupportView?.selectedSubtitleTrackName = languageSupportView?.subtitleTracks[0]
            } else {
                languageSupportView?.subtitleTracks = ["None"]
                if self.player.allSubTitles.count >= 1 {
                    // DO Nothing
                    for subtitle in self.player.allSubTitles {
                        if let title = ((subtitle as AnyObject).value(forKey: "title") as? String) {
                            languageSupportView?.subtitleTracks.append(title)
                        }
                    }
                }
                if selectedSubtitleTrackName.isEmpty {
                    languageSupportView?.selectedSubtitleTrackName = languageSupportView?.subtitleTracks[0]
                } else {
                    languageSupportView?.selectedSubtitleTrackName = selectedSubtitleTrackName
                }
            }
        } else {
            languageSupportView?.audioTracks = ["Hindi", "English", "Tamil", "Telgu", "Punjabi", "Bhojpuri"]
            if selectedAudioTrackName.isEmpty {
                languageSupportView?.selectedAudioTrackName = languageSupportView?.audioTracks[0]
            } else {
                languageSupportView?.selectedAudioTrackName = selectedAudioTrackName
            }
            languageSupportView?.subtitleTracks = ["None", "Hindi", "English", "Tamil", "Telgu"]
            if selectedSubtitleTrackName.isEmpty {
                languageSupportView?.selectedSubtitleTrackName = languageSupportView?.subtitleTracks[0]
            } else {
                languageSupportView?.selectedSubtitleTrackName = selectedSubtitleTrackName
            }
        }
        addSubview(languageSupportView!)
        languageSupportView?.fillParentView()
    }
    
    @IBAction func settingsButtonAction(_ sender: Any) {
        CustomLoader.shared.hideLoader()
        //loaderView.isHidden = true
        isAudioOrVideoQualityOpenend = true
        previousPlayeingState = player.isPlayerPlaying
        isPlayerPlaying = false
        self.videoQualityView = BAVideoQualityView(frame: .zero)
        if videoQualityArr.isEmpty {
            videoQualityView?.videoQualityArray = ["Auto", "High", "Medium", "Low"]
        } else {
            videoQualityArr = videoQualityArr.removeDuplicates()
            videoQualityView?.videoQualityArray = videoQualityArr
        }
        if selectedQualityVideoName.isEmpty {
            videoQualityView?.selectedQualityName = videoQualityView?.videoQualityArray[0]
        } else {
            videoQualityView?.selectedQualityName = selectedQualityVideoName
        }
        addSubview(videoQualityView ?? BAVideoQualityView(frame: .zero))
        videoQualityView?.fillParentView()
        videoQualityView?.delegate = self
    }
    
    @IBAction func playNextVideoAction(_ sender: Any) {
        if BAKeychainManager().contentPlayBack == false {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
        } else {
            contentDetail = nil
            seekedDuration = 0.0
            isAlertShownOnce = false
            bottomViewHeightConstraint.constant = isPortrait ? 0.0 : 38.0
            totalLabelWidthConstraint.constant = isTrailer ? 43.0 : 70.0
            volumeButtonContainerWidthConstraint.constant = isPortrait ? 0 : 35
            // elapsedTimeWidthConstraint.constant = (!isTrailer && isPortrait) ? 75.0 : 46.0
            episode = self.nextEpisodeData?.data?.nextEpisode
            fetchWatchlistLookUp()
            if episode?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
                let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                print("md5SignatureHex: \(md5SignatureHex)")
                let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
                self.getPlayableUrl(signedUrl: signedUrl)
            } else if episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                let result = convertStringToDictionary(text: episode?.playerDetail?.authorizedCookies ?? "")
                let url = URL(string: episode?.playerDetail?.playUrl ?? "");
                var cookies = [HTTPCookie]()
                if let cookie = result as? [String: String] {
                    for key in cookie.keys {
                        let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                        let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url!)
                        cookies.append(contentsOf: cookie)
                    }
                }
                let values = HTTPCookie.requestHeaderFields(with: cookies)
                let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
                let urlAsset = AVURLAsset(url: url!, options: cookieOptions)
                //let urlAsset = AVURLAsset(url: URL(string: url)!)
                player.prepareToPlay(withAsset: urlAsset)
                self.contentView.isUserInteractionEnabled = true
                hideControls()
                progressSegue.delegate = self
                configureContentPlayback(isNextPreviousEpisode: true)
            } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                configureVootPlayer(episodeDetail: self.nextEpisodeData?.data?.nextEpisode)
            } else if episode?.provider?.uppercased() == ProviderType.eros.rawValue {
                configureErosNow(episodeDetail: self.nextEpisodeData?.data?.nextEpisode)
            } else {
                let urlAsset = AVURLAsset(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!)
                player.prepareToPlay(withAsset: urlAsset)
                self.contentView.isUserInteractionEnabled = true
                hideControls()
                progressSegue.delegate = self
                configureContentPlayback(isNextPreviousEpisode: true)
            }
        }
    }
    
    func configureErosNow(episodeDetail: Episodes?) {
        var contentID = ""
        contentID = episodeDetail?.providerContentId ?? ""
        if BAKeychainManager().isErosNowLoggedIn {
            UtilityFunction.shared.getErosNowContentProfile(for: contentID) { (flagValue, contentProfile, stringMessage) in
                // self.hideActivityIndicator()
                if flagValue {
                    if let _ = contentProfile?.streamUrl {
                        self.erosNowContent = contentProfile
                    } else {
                        self.erosNowPlayError()
                        return
                    }
                    UtilityFunction.shared.performTaskInMainQueue {
                        self.playErosNextEpisode()
                    }
                } else {
                    self.erosNowPlayError()
                }
            }
        }
    }
    
    
    func playErosNextEpisode() {
        if let data = erosNowContent {
            changeErosEpisodesContentPlay(with: data)
            configureContentPlayback(isNextPreviousEpisode: true)
        }
    }
    
    func changeErosEpisodesContentPlay(with contentProfile: ENContentProfile) {
        if let streamUrl = contentProfile.streamUrl {
            let assetInfo = ENPlaybackAssetInfo(
                playbackURL: streamUrl.absoluteString,
                assetId: contentProfile.assetId,
                assetTitle: contentProfile.assetTitle,
                contentId: contentProfile.contentId,
                contentType: contentProfile.contentTypeId
            )
            print("this is assest \(assetInfo)")
            print("\(contentProfile.contentId)")
            print("\(contentProfile.assetId)")
            let urlAsset = AVURLAsset(url: URL(string: streamUrl.absoluteString)!)
            ENSDK.shared.initializePlayer(withAssetInfo: assetInfo, avPlayer: player.player)
            player.prepareToPlay(withAsset: urlAsset)
        }
    }
    
    
    func erosNowPlayError() {
        if self.isAlertShownOnce == false {
            self.isAlertShownOnce = true
            self.cancelTimer()
            self.player.stop()
            self.player.isMuted = true
            //self.player = nil
            self.hideControls()
            DispatchQueue.main.async {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                    if !self.isTrailer && self.isTrailerFullScreen {
                        self.backAction()
                    }
                }
            }
        } else {
            
        }
    }
    
    @IBAction func zoomInOutAction(_ sender: Any) {
        if isZoomedIn {
            player.layer?.videoGravity = .resizeAspect
        } else {
            player.layer?.videoGravity = .resizeAspectFill
        }
        isZoomedIn = !isZoomedIn
    }
    
    @IBAction func previousVideoAction(_ sender: Any) {
        if BAKeychainManager().contentPlayBack == false {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
        } else {
            contentDetail = nil
            seekedDuration = 0.0
            isAlertShownOnce = false
            seekedDuration = 0.0
            bottomViewHeightConstraint.constant = isPortrait ? 0.0 : 38.0
            totalLabelWidthConstraint.constant = isTrailer ? 43.0 : 70.0
            volumeButtonContainerWidthConstraint.constant = isPortrait ? 0 : 35
            // elapsedTimeWidthConstraint.constant = (!isTrailer && isPortrait) ? 75.0 : 46.0
            episode = self.nextEpisodeData?.data?.previousEpisode
            fetchWatchlistLookUp()
            if episode?.provider?.uppercased() == ProviderType.shemaro.rawValue {
                let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
                let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
                print("md5SignatureHex: \(md5SignatureHex)")
                let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
                self.getPlayableUrl(signedUrl: signedUrl)
            } else if episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
                let result = convertStringToDictionary(text: episode?.playerDetail?.authorizedCookies ?? "")
                let url = URL(string: episode?.playerDetail?.playUrl ?? "");
                var cookies = [HTTPCookie]()
                if let cookie = result as? [String: String] {
                    for key in cookie.keys {
                        let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                        let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url!)
                        cookies.append(contentsOf: cookie)
                    }
                }
                let values = HTTPCookie.requestHeaderFields(with: cookies)
                let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
                let urlAsset = AVURLAsset(url: url!, options: cookieOptions)
                player.prepareToPlay(withAsset: urlAsset)
                showControls()
                progressSegue.delegate = self
                configureContentPlayback(isNextPreviousEpisode: true)
            } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
                configureVootPlayer(episodeDetail: self.nextEpisodeData?.data?.previousEpisode)
            } else if episode?.provider?.uppercased() == ProviderType.eros.rawValue {
                configureErosNow(episodeDetail: self.nextEpisodeData?.data?.previousEpisode)
            } else {
                let urlAsset = AVURLAsset(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4")!)
                player.prepareToPlay(withAsset: urlAsset)
                showControls()
                progressSegue.delegate = self
                configureContentPlayback(isNextPreviousEpisode: true)
            }
        }
    }
    
    func updateSubtitleTrack() {
        if player.allSubTitles.count >= 1 {
            if selectedSubtitle == 0 {
                player.hideSubtitle()
            } else {
                player.changeSubtitleTrackWith(index: selectedSubtitle - 1)
            }
        } else if player.allSubTitles.count == 0 {
            player.hideSubtitle()
        }
    }
    
    @objc func configureContentPlay() {
        if BAReachAbility.isConnectedToNetwork() {
            player.play()
            showControls()
            // player.isMuted = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        } else {
            // Do Nothing
        }
    }
    
    @objc func updatePlayingStatus() {
        isPlayerInBackground = true
        if player.isPlayerPlaying && !isTrailer {
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
            isPlayerPlaying = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
        } else {
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            isPlayerPlaying = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
        }
        //player.isMuted = false
    }
    
    // MARK: Show Player Controls
    func showControls() {
        if isVideoEnded {
            volumeButton.isHidden = true
            volumeButtonContainer.isHidden = true
            progressSegue.isHidden = true
            fullScreenButton.isHidden = true
            if isTrailerFullScreen {
                backButton.isHidden = false
            }
            bottomActionsView.isHidden = true
            timeElapsedLabel.isHidden = true
            totalTimeLabel.isHidden = true
            forwardButton.isHidden = true
            replayLabel.isHidden = false
            rewindButton.isHidden = true
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            playButton.isHidden = false
            if isEpisode ?? false && nextEpisodeData?.data?.nextEpisodeExists ?? false  {
                
            } else {
                if isTrailer {
                    
                } else {
                   delegate?.videoDidFinish()
                }
            }
        } else {
            if isTrailer {
                
            } else {
                if isTrailerFullScreen {
                    if nextEpisodeData?.data != nil && nextEpisodeData?.data?.previousEpisodeExists ?? false {
                        previousVideoButton.isHidden = false
                    } else {
                        previousVideoButton.isHidden = true
                    }
                    
                    if nextEpisodeData?.data != nil && nextEpisodeData?.data?.nextEpisodeExists ?? false {
                        self.isNextEpisodeButtonVisible = true
                    } else {
                        self.isNextEpisodeButtonVisible = false
                    }
                } else {
                    previousVideoButton.isHidden = true
                }
            }
            showOptionalControls()
            progressSegue.isHidden = false
            fullScreenButton.isHidden = false
            totalTimeLabel.isHidden = false
            if isTrailerFullScreen {
                backButton.isHidden = false
                fullScreenButton.isHidden = true
            } else {
                backButton.isHidden = true
                fullScreenButton.isHidden = false
            }
            if player.isPlayerPlaying == true {
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
            } else {
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
            replayLabel.isHidden = true
            forwardButton.isHidden = false
            let seconds: Int64 = Int64(progressSegue.value)
            if seconds < 10 {
                rewindButton.isHidden = true
            } else {
                rewindButton.isHidden = false
            }
            playButton.isHidden = false
        }
    }
    
    func configureVootPlayer(episodeDetail: Episodes?) {
        if contentDetail != nil && contentDetail?.lastWatch != nil && isReplay == false {
            vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: contentDetail?.lastWatch?.providerContentId, contentType: contentDetail?.lastWatch?.contentType, partner: contentDetail?.lastWatch?.provider)
        } else {
            vootPlaybackViewModal = BAVootViewModal(repo: BAVootRepo(), endPoint: "playback", contentId: episodeDetail?.providerContentId, contentType: episodeDetail?.contentType, partner: episodeDetail?.provider)
        }
        configureVootStream(episodeDetail:episodeDetail)
    }
    
    func configureVootStream(episodeDetail: Episodes?) {
        if let dataModel = vootPlaybackViewModal {
            DispatchQueue.main.async {
                CustomLoader.shared.showLoader(false)
            }
            playNextVideoButton.isUserInteractionEnabled = false
            dataModel.makePlayabaleContentCall {(flagValue, message, isApiError) in
                CustomLoader.shared.hideLoader()
                if flagValue {
                    self.playNextVideoButton.isUserInteractionEnabled = true
                    if self.vootPlaybackViewModal?.playbackData != nil {
                        self.configureVootPlayback(vootPlayback: self.vootPlaybackViewModal?.playbackData, episodeDetail: episodeDetail)
                    } else {
                    }
                } else if isApiError {
                    //self.apiError(message, title: kSomethingWentWrong)
                } else {
                    if self.isAlertShownOnce == false {
                        self.isAlertShownOnce = true
                        self.continueWatchTimer?.invalidate()
                        self.continueWatchTimer = nil
                        self.player.stop()
                        self.player.isMuted = true
                        //self.player = nil
                        self.hideControls()
                        DispatchQueue.main.async {
                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                                if !self.isTrailer && self.isTrailerFullScreen {
                                    self.backAction()
                                }
                            }
                        }
                    } else {
                        
                    }
                }
            }
        }
    }
    
    func configureVootPlayback(vootPlayback: VootPlaybackData?, episodeDetail: Episodes?) {
        let playableUrl = vootPlayback?.url ?? ""
        if playableUrl.isEmpty {
            DispatchQueue.main.async {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                    self.backAction()
                    //self.backToDetailScreen()
                }
            }
        } else {
            let kTataSkyFPSCertificateURL = vootPlayback?.drm?.first?.licenseURL ?? ""
            TTNPlayerConfiguration.sharedInstance.fpsCertificateUrl(kTataSkyFPSCertificateURL)
            TTNPlayerConfiguration.sharedInstance.drmInfo(data: vootPlayback?.drm?.first?.certificate ?? "",
                                                          drmType: episodeDetail?.provider?.uppercased() == ProviderType.vootSelect.rawValue ? .kaltura : .kidskaltura)
            let urlAsset = AVURLAsset(url: URL(string: playableUrl)!)
            self.player.prepareToPlay(withAsset: urlAsset)
            self.hideControls()
            self.progressSegue.delegate = self
            continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(makeContinueWatchingCall), userInfo: nil, repeats: true)
            self.configureContentPlayback(isNextPreviousEpisode: true)
        }
        
    }
    
    // MARK: Hide Player Controls
    func hideControls() {
        hideOptionalControls()
        progressSegue.isHidden = true
        totalTimeLabel.isHidden = true
        backButton.isHidden = true
        previousVideoButton.isHidden = true
        forwardButton.isHidden = true
        rewindButton.isHidden = true
        if isVideoEnded {
            fullScreenButton.isHidden = false
            playButton.isHidden = false
            replayLabel.isHidden = false
            fullScreenButton.isHidden = true
            if isTrailerFullScreen {
                backButton.isHidden = false
            }
            playButton.setImage(UIImage(named: "replay"), for: .normal)
        } else {
            fullScreenButton.isHidden = true
            playButton.isHidden = true
            replayLabel.isHidden = true
        }
        progressSegue.sliderToolTip.alpha = 0.0
        progressSegue.showToolTipViewAnimated(animated: false)
    }
    
    func showOptionalControls() {
        zoomInOutButton.isHidden = (!isPortrait) ? false : true
        titleLabel.isHidden = (!isPortrait) ? false : true
        bottomActionsView.isHidden = isPortrait //? false : true
        audioTrackButton.isHidden = (!isTrailer && !isPortrait) ? false : true
        settingsButton.isHidden = (!isTrailer && !isPortrait) ? false : true
        playNextVideoButton.isHidden = (!isTrailer && !isPortrait) && isNextEpisodeButtonVisible ? false : true
        volumeButton.isHidden = !isPortrait //((isPortrait || !isPortrait) && !isVideoEnded) ? false : true
        volumeButtonContainer.isHidden = isPortrait
//        timeElapsedLabel.isHidden = (!isTrailer && !isPortrait) ? true : false
    }
    
    func hideOptionalControls() {
        zoomInOutButton.isHidden = true
        titleLabel.isHidden = true
        bottomActionsView.isHidden = true
        audioTrackButton.isHidden = true
        settingsButton.isHidden = true
        playNextVideoButton.isHidden = true
        volumeButton.isHidden = true
        volumeButtonContainer.isHidden = true
        timeElapsedLabel.isHidden = true
    }
    
    // MARK: Rewind Button Action
    @IBAction func rewindButtonAction(_ sender: Any) {
        isVideoEnded = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if self.player.isPlayerPlaying {
                self.hideControls()
            }
        }
        if player.isPlayerPlaying == true {
            self.player.play()
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.player.pause()
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        let currentCMTime = CMTime(seconds: player.currentPlaybackTime, preferredTimescale: 1000000)
        let playerCurrentTime = CMTimeGetSeconds(currentCMTime)
        var newTime = playerCurrentTime - seekDuration
        
        if newTime < 0 {
            newTime = 0
        }
        
        if newTime < 10 {
            rewindButton.isHidden = true
        } else {
            rewindButton.isHidden = false
        }
        
        let rewindTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        let rewindTimeInterval = Float(CMTimeGetSeconds(rewindTime))
        player.seek(to: TimeInterval(rewindTimeInterval))
    }
    
    // MARK: Forward Button Action
    @IBAction func forwardButtonAction(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        }
        if player.isPlayerPlaying == true {
            self.player.play()
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            self.player.pause()
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        
        let durationtimeIntvl = player.duration
        let durationCmTime = CMTime(seconds: durationtimeIntvl, preferredTimescale: 1000000)
        let currentCMTime = CMTime(seconds: player.currentPlaybackTime, preferredTimescale: 1000000)
        let playerCurrentTime = CMTimeGetSeconds(currentCMTime)
        let newTime = playerCurrentTime + seekDuration
        let timeLeft = CMTimeGetSeconds(durationCmTime) - newTime
        forwardButton.isHidden = false
        if timeLeft < 10 {
            player.seek(to: player.duration)
        } else {
            if newTime < CMTimeGetSeconds(durationCmTime) {
                let forwardTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
                let forwardTimeInterval = Float(CMTimeGetSeconds(forwardTime))
                player.seek(to: TimeInterval(forwardTimeInterval))
//                timeElapsedLabel.text = self.stringFromTimeInterval(interval: CMTimeGetSeconds(forwardTime))
            }
        }
    }
    
    // MARK: Play/Pause Button Action
    @IBAction func playPauseButtonAction(_ sender: Any) {
        if isVideoEnded {
            if BAKeychainManager().contentPlayBack == false {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
            } else {
                isVideoEnded = false
                showControls()
                seekedDuration = 0.0
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
                if isTrailer {
                    // Do not change PI page Button state
                } else {
                    delegate?.videoBeganInReplay()
                }
                self.player.seek(to: 0)
                self.player.play()
            }
        } else {
            if player.isPlayerPlaying {
               // self.probeInterface?.sendEventFor(eventType: .PAUSED)
                self.player.pause()
                if isTrailer {
                    // Do not change PI page button state
                } else {
                    var genres: [String] = []
                    var title = ""
                    var partnerName = ""
                    if contentDetail != nil {
                        genres = contentDetail?.meta?.genre ?? []
                        title = contentDetail?.meta?.vodTitle ?? ""
                        partnerName = contentDetail?.meta?.provider ?? ""
                    } else {
                        genres = episode?.genres ?? []
                        title = episode?.title ?? ""
                        partnerName = episode?.provider ?? ""
                    }
                        self.pauseCount += 1
                        let genreResult = genres.joined(separator: ",")
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                }
                playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
                isPlayerPlaying = false
            } else {
              //  self.probeInterface?.sendEventFor(eventType: .RESUMED)
                self.player.play()
                if isTrailer {
                    
                } else {
                    var genres: [String] = []
                    var title = ""
                    var partnerName = ""
                    if contentDetail != nil {
                        genres = contentDetail?.meta?.genre ?? []
                        title = contentDetail?.meta?.vodTitle ?? ""
                        partnerName = contentDetail?.meta?.provider ?? ""
                    } else {
                        genres = episode?.genres ?? []
                        title = episode?.title ?? ""
                        partnerName = episode?.provider ?? ""
                    }
                    self.resumeCount += 1
                    let genreResult = genres.joined(separator: ",")
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.resumeContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
                }
                playButton.setImage(UIImage(named: "playicon"), for: .normal)
                isPlayerPlaying = true
            }
            delegate?.didChangePlayerPlayingState(isPlayerPlaying)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if self.player.isPlayerPlaying {
                    self.hideControls()
                }
            }
        }
    }
    
    // MARK: AddRemoveToWatchlist
    func addRemoveFromWatchlist() {
        if let dataModel = addRemoveWatchlistVM {
            dataModel.addRemoveWatchlist {(flagValue, _, isApiError) in
                if flagValue {
                    if self.isTrailer {
                        if self.addToFavButton.isSelected  {
                            self.addToFavButton.isSelected = false
                            self.isAddedToFavourite = false
                            self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                            self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                            self.makeToast(message: "Removed from Watchlist", imageName: "WatchlistActive")
                        } else {
                            self.addToFavButton.isSelected = true
                            self.isAddedToFavourite = true
                            self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                            self.addToFavButton.setTitle(" Added To Watchlist", for: .normal)
                            self.makeToast(message: "Added to Watchlist", imageName: "WatchlistActive")
                        }
                    } else {
                        if self.addToFavButton.isSelected {
                            self.addToFavButton.isSelected = false
                            self.isAddedToFavourite = false
                            self.addToFavButton.setImage(UIImage(named: "followIcon"), for: .normal)
                            self.addToFavButton.setTitle(" Add To Watchlist", for: .normal)
                            self.makeToast(message: "Removed from Watchlist", imageName: "WatchlistActive")
                            if self.episode != nil {
                                let genreResult = self.episode?.genres?.joined(separator: ",")
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.episode?.title ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.episode?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.episode?.provider ?? ""])
                            } else {
                                let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteFavorite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentDetail?.meta?.provider ?? ""])
                            }
                        } else {
                            if self.episode != nil {
                                let genreResult = self.episode?.genres?.joined(separator: ",")
                                let languageResult = self.episode?.languages?.joined(separator: ",")
                                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.episode?.title ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.episode?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.episode?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                            } else {
                                let genreResult = self.contentDetail?.meta?.genre?.joined(separator: ",")
                                let languageResult = self.contentDetail?.meta?.audio?.joined(separator: ",")
                                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.addContentFavourite.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: self.contentDetail?.meta?.vodTitle ?? "", MixpanelConstants.ParamName.genre.rawValue: genreResult ?? "", MixpanelConstants.ParamName.contentType.rawValue: self.contentDetail?.meta?.contentType ?? "", MixpanelConstants.ParamName.partnerName.rawValue: self.contentDetail?.meta?.provider ?? "", MixpanelConstants.ParamName.contentLanguage.rawValue: languageResult ?? ""])
                            }
                            self.addToFavButton.isSelected = true
                            self.isAddedToFavourite = true
                            self.addToFavButton.setImage(UIImage(named: "favSelected"), for: .normal)
                            self.addToFavButton.setTitle(" Added To Watchlist", for: .normal)
                            self.makeToast(message: "Added to Watchlist", imageName: "WatchlistActive")
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func makeToast(message: String, imageName: String) {
        kAppDelegate.window?.makeToast(message, duration: 1.0, point: .bottom, title: "", image: UIImage.init(named: imageName), toastYPosition: tabBarHeight) { _ in
        }
    }
    
    func getResolutionInfo(streamUrl: String) {
        var qualityInfo = [String: String]()
        let session = URLSession.shared
        let url = URL(string: streamUrl)!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                return
            }
            print("Status Code for get resolution api : \(httpResponse.statusCode)")
            if error == nil && httpResponse.statusCode == 200 {
                let responseData = String(data: data!, encoding: String.Encoding.utf8)
                var stringArr: [String]? = responseData?.components(separatedBy: "#EXT-X-STREAM-INF:")
                var indexToDelete: [Int] = []
                if let metaDataForVideo = stringArr {
                    for index in 0...metaDataForVideo.count-1 {
                        if !metaDataForVideo[index].contains("RESOLUTION") || (metaDataForVideo[index].contains("#EXT-X-I-FRAME-STREAM-INF:")) {
                            indexToDelete.append(index)
                        }
                    }
                    stringArr?.remove(at: indexToDelete)
                    if stringArr?.count ?? 0 > 0 {
                        for index in 0...stringArr!.count - 1 {
                            if let bandwidth = stringArr![index].sliceByString(from: "BANDWIDTH=", to: ",") {
                                qualityInfo["BANDWIDTH"] = bandwidth
                            }
                            if let resolution = stringArr![index].sliceByString(from: "RESOLUTION=", to: ",") {
                                qualityInfo["RESOLUTION"] = resolution
                            }
                            if let quality = stringArr![index].sliceByString(from: "RESOLUTION=", to: "x") {
                                qualityInfo["QUALITY"] = quality
                            }
                            self.quatityInfoArray.append(qualityInfo)
                        }
                        print(self.quatityInfoArray)
                        self.videoQualityArr.removeAll()
                        self.videoQualityArr.append("Auto")
                        for quality in self.quatityInfoArray {
                            self.videoQualityArr.append(quality["QUALITY"] as! String + "p")
                        }
                    } else {
                        // Show default options
                    }
                    
                    print("Video Quality Array is ----->", self.videoQualityArr)
                }
            }
        })
        task.resume()
    }
    
    @objc func showNoInternetView() {
        if BAReachAbility.isConnectedToNetwork() {
            // Do Nothing
        } else {
            if noInternetView == nil {
                if noInternetimer != nil {
                    noInternetimer?.invalidate()
                    noInternetimer = nil
                }
                previousPlayeingState = player.isPlayerPlaying
                isPlayerPlaying = false
              //  player.pause()
                CustomLoader.shared.hideLoader()
                noInternetView = BANoInternetOnPlayerView(frame: .zero)
                noInternetView?.configureConstraint(isPortrait: isPortrait)
                noInternetView?.backButton.isHidden = true
                self.contentView.addSubview(noInternetView!)
               // self.player.pause()
                if !isTrailerFullScreen {
                    noInternetView?.backButton.isHidden = true
                } else {
                    noInternetView?.backButton.isHidden = false
                }
                noInternetView?.fillParentView()
                noInternetView?.delegate = self
            }
        }
    }
    
    @objc func removeUser() {
        self.continueWatchTimer?.invalidate()
        self.continueWatchTimer = nil
        self.player.stop()
        self.player.isMuted = true
        if isTrailerFullScreen {
            if removeDeviceView == nil {
                previousPlayeingState = player.isPlayerPlaying
                isPlayerPlaying = false
                player.pause()
                CustomLoader.shared.hideLoader()
                removeDeviceView = BAUserRemoveView(frame: .zero)
                self.contentView.addSubview(removeDeviceView!)
                self.player.pause()
                removeDeviceView?.fillParentView()
            }
        } else {

        }
    }
    
    //Autoplay delegate
    func playNextEpisode(episode: Episodes?) {
        autoPlayView = nil
        bottomViewHeightConstraint.constant = isPortrait ? 0.0 : 38.0
        totalLabelWidthConstraint.constant = isTrailer ? 43.0 : 70.0
        volumeButtonContainerWidthConstraint.constant = isPortrait ? 0 : 35
        seekedDuration = 0.0
        isAlertShownOnce = false
        replayLabel.isHidden = true
        seekedDuration = 0.0
        playButton.setImage(UIImage(named: "playicon"), for: .normal)
        isVideoEnded = false
        contentDetail = nil
        player.stop()
        player.pause()
        // player = nil
        self.episode = episode
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
        if episode?.provider?.uppercased() == ProviderType.shemaro.rawValue {
            let md5SignatureData = MD5(string: smarturl_accesskey + (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters)
            let md5SignatureHex =  md5SignatureData.map { String(format: "%02hhx", $0) }.joined()
            print("md5SignatureHex: \(md5SignatureHex)")
            let signedUrl = (episode?.partnerDeepLinkUrl ?? "") + "?" + smarturl_parameters + md5SignatureHex
            self.getPlayableUrl(signedUrl: signedUrl)
        } else if episode?.provider?.uppercased() == ProviderType.curosityStream.rawValue {
            let result = convertStringToDictionary(text: episode?.playerDetail?.authorizedCookies ?? "")
            let url = URL(string: episode?.playerDetail?.playUrl ?? "");
            var cookies = [HTTPCookie]()
            if let cookie = result as? [String: String] {
                for key in cookie.keys {
                    let cookieField = ["Set-Cookie": "\(key)=\(cookie[key] ?? "")"]
                    let cookie = HTTPCookie.cookies(withResponseHeaderFields: cookieField, for: url!)
                    cookies.append(contentsOf: cookie)
                }
            }
            let values = HTTPCookie.requestHeaderFields(with: cookies)
            let cookieOptions = ["AVURLAssetHTTPHeaderFieldsKey": values]
            let urlAsset = AVURLAsset(url: url!, options: cookieOptions)
            //let urlAsset = AVURLAsset(url: URL(string: episode?.playerDetail?.playUrl ?? "")!)
            player.prepareToPlay(withAsset: urlAsset)
            showControls()
            progressSegue.delegate = self
            self.continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.makeContinueWatchingCall), userInfo: nil, repeats: true)
            configureContentPlayback(isNextPreviousEpisode: true)
        } else if episode?.provider?.uppercased() == ProviderType.vootKids.rawValue || episode?.provider?.uppercased() == ProviderType.vootSelect.rawValue {
            configureVootPlayer(episodeDetail: episode)
        } else if episode?.provider?.uppercased() == ProviderType.eros.rawValue {
            configureErosNow(episodeDetail: self.nextEpisodeData?.data?.nextEpisode)
        } else {
            let urlAsset = AVURLAsset(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!)
            player.prepareToPlay(withAsset: urlAsset)
            CustomLoader.shared.hideLoader()
            showControls()
            progressSegue.delegate = self
            configureContentPlayback(isNextPreviousEpisode: true)
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch(let error) {
                print(error.localizedDescription)
            }
            player.play()
        }
        //}
    }
    
    // MARK: Get Playabkle Content For Shemaroo Me
    func getPlayableUrl(signedUrl: String?) {
        // Create URL
        let url = URL(string: signedUrl ?? "")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
                do {
                    let shemarooContent = try JSONDecoder().decode(ShemarooContentModal.self, from: data)
                    print(shemarooContent)
                    let playbackUrl = shemarooContent.adaptive_urls?[0].playback_url ?? ""//"http://sample.vodobox.com/planete_interdite/planete_interdite_alternate.m3u8"////
                    if playbackUrl.isEmpty {
                        DispatchQueue.main.async {
                            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                                self.backAction()
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            let urlAsset = AVURLAsset(url: URL(string: playbackUrl)!)
                            self.player.prepareToPlay(withAsset: urlAsset)
                            self.hideControls()
                            self.progressSegue.delegate = self
                            self.continueWatchTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.makeContinueWatchingCall), userInfo: nil, repeats: true)
                            self.configureContentPlayback(isNextPreviousEpisode: true)
                            
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        CustomLoader.shared.hideLoader()
                    }
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using: .utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData
    }
}


//
//  Extension+BAHungamaPlayerView.swift
//  BingeAnywhere
//
//  Created by Shivam on 22/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import HungamaPlayer
import AVKit
import UIKit


extension BAHungamaPlayerView: OnPlayerContentLoadListener {
    
    func onContentLoadSuccess() {
        startPlayback()
        DispatchQueue.main.async {
            CustomLoader.shared.hideLoader()
        }
    }
    
    func onContentLoadFailed(withError errorMessage: String) {
        handleVideoPlayError(false)
    }
    
    
    func handleVideoPlayError(_ initialStart: Bool) {
        DispatchQueue.main.async {
            CustomLoader.shared.hideLoader()
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Video unavailable"), .withMessage("We are unable to play your video right now. Please try again in a few minutes"), .hidden, .hidden)) { _ in
                if initialStart {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeHungamaPlayer"), object: nil)
                } else {
                    self.trigerPlayPauseNotification()
                    self.togglePlayPauseHungamaPlayer(false)
                }
            }
        }
    }
}

extension BAHungamaPlayerView: OnPlayerStateChangeListener {
    func onPlayer(stateChanged state: PlayerState) {
        print(state)
        switch state {
        case .buffering:
            DispatchQueue.main.async {
                CustomLoader.shared.showLoader(true)
            }
            break
        case .loading:
            DispatchQueue.main.async {
                CustomLoader.shared.showLoader(true)
            }
            break
        case .ended:
            videoEndedNotification()
            break
        case .idle:
            break
        case .ready:
            initialBufferTimeEvent()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.setupSlider()
                CustomLoader.shared.hideLoader()
                self.hideControls()
            }
            break
        default:
            DispatchQueue.main.async {
                CustomLoader.shared.hideLoader()
            }
            break
        }
    }
    
    func onPlayer(error: HungamaPlayerError) {
        print(error)
        DispatchQueue.main.async {
            CustomLoader.shared.hideLoader()
        }
    }
    
    func videoEndedNotification() {
        debugPrint("video ended")
        cancelTimer()
        if isTrailer {
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            CustomLoader.shared.hideLoader()
            fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
            seekedDuration = 0
            forwardButton.isHidden = true
            isVideoEnded = true
            isPlayerPlaying = false
            showControls()
            //delegate?.videoDidFinish()
        } else {
            debugPrint("video ended for Player")
            CustomLoader.shared.hideLoader()
            showControls()
            isVideoEnded = true
            if isEpisode ?? false && nextEpisodeData?.data?.nextEpisodeExists ?? false {
                HungamaPlayerManager.shared.stop()
                self.cancelTimer()
//                self.continueWatchTimer?.invalidate()
//                self.continueWatchTimer = nil
                if BAKeychainManager().contentPlayBack == false {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayer"), object: nil)
                } else {
                    if autoPlayView == nil {
                        autoPlayView = BAAutoPlay.init(frame: .zero, data: nextEpisodeData?.data?.nextEpisode)
                        autoPlayView?.configureConstraints(isPortrait: isPortrait)
                        isEpisodeCompleted = true
                        autoPlayView?.delegate = self
                        autoPlayView?.tag = 999
                        self.contentView.addSubview(autoPlayView!)
                        if !isTrailerFullScreen {
                            autoPlayView?.backButton.isHidden = true
                        } else {
                            autoPlayView?.backButton.isHidden = false
                        }
                        autoPlayView?.fillParentView()
                    }
                }
            } else if isEpisode ?? false && !(nextEpisodeData?.data?.nextEpisodeExists ?? false) {
                showControls()
            } else if !(isEpisode ?? false) {
                showControls()
            }
        }
    }
    
}


extension BAHungamaPlayerView {
    // MARK: Progress Segue Dragging Actions ans Other Notification Observers
    func addTargetActions() {
        progressSegue.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        //        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchBegin), for: .touchDown)
        //        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderTouchEnd), for: .touchUpInside)
        //        progressSegue.addTarget(self, action: #selector(self.handlePlayheadSliderValueChanged), for: .valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(removeObservers), name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureControls), name: NSNotification.Name(rawValue: "HideControls"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pausePlayerIcon), name: NSNotification.Name(rawValue: "pausePlayerIcons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playPlayerIcon), name: NSNotification.Name(rawValue: "playPlayerIcons"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resumeContent), name: NSNotification.Name(rawValue: "ResumeContent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseContent), name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureFullScreenIconOnHungama), name: NSNotification.Name(rawValue: "changeFullScreenIcon"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configurePlayNextState), name: NSNotification.Name(rawValue: "PlayNowState"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureSmallScreenIconOnHungama), name: NSNotification.Name(rawValue: "changeSmallScreenIcon"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureContentPlay), name: NSNotification.Name(rawValue: "PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePlayingStatus), name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
    }
    
    @objc func resumeContent() {
        if previousPlayingState == true {
            self.togglePlayPauseHungamaPlayer(true) // play
        } else {
            self.togglePlayPauseHungamaPlayer(false) //pause
        }
    }
    
    @objc func configurePlayNextState() {
        autoPlayView?.playNowAction()
    }
    
    @objc func pausePlayerIcon() {
        var genres: [String] = []
        var title = ""
        var partnerName = ""
        if contentDetail != nil {
            genres = contentDetail?.meta?.genre ?? []
            title = contentDetail?.meta?.vodTitle ?? ""
            partnerName = contentDetail?.meta?.provider ?? ""
        } else {
            genres = episode?.genres ?? []
            title = episode?.title ?? ""
            partnerName = episode?.provider ?? ""
        }
        self.pauseCount += 1
        let genreResult = genres.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.pauseContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
        playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
    }
    
    @objc func playPlayerIcon() {
        var genres: [String] = []
        var title = ""
        var partnerName = ""
        if contentDetail != nil {
            genres = contentDetail?.meta?.genre ?? []
            title = contentDetail?.meta?.vodTitle ?? ""
            partnerName = contentDetail?.meta?.provider ?? ""
        } else {
            genres = episode?.genres ?? []
            title = episode?.title ?? ""
            partnerName = episode?.provider ?? ""
        }
        self.resumeCount += 1
        let genreResult = genres.joined(separator: ",")
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.resumeContent.rawValue, properties: [MixpanelConstants.ParamName.contentTitle.rawValue: title, MixpanelConstants.ParamName.contentGenre.rawValue: genreResult, MixpanelConstants.ParamName.contentType.rawValue: MixpanelConstants.ParamValue.onDemand.rawValue, MixpanelConstants.ParamName.partnerName.rawValue: partnerName])
        playButton.setImage(UIImage(named: "playicon"), for: .normal)
    }
    
    @objc func pauseContent() {
        if isPausedMethodCalled {return} // to maintain last state of player as this method is called twice from app delegate notification
        isAutoPlayTimerChecked = false
        isPausedMethodCalled = true
        cancelContinueTimer()
        if isVideoEnded {
            playButton.setImage(UIImage(named: "replay"), for: .normal)
            fullScreenButton.isHidden = true
        } else {
            previousPlayingState = HungamaPlayerManager.shared.isPlaying() //one
            self.togglePlayPauseHungamaPlayer(false) //pause
        }
    }
    
    func reRunAutoPlayTimer() {
        if let view = autoPlayView {
            if !isAutoPlayTimerChecked {
                isAutoPlayTimerChecked = true
                view.runCodeExpiryTimer()
            }
        }
    }
    
    @objc func updatePlayingStatus() {
        isPlayerInBackground = true
        isPausedMethodCalled = false
        reRunAutoPlayTimer()
        if HungamaPlayerManager.shared.isPlaying() && !isTrailer {
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
            //isPlayerPlaying = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
        } else {
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            //isPlayerPlaying = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
        }
    }
    
    @objc func configureContentPlay() {
        isPausedMethodCalled = false
        reRunAutoPlayTimer()
        if BAReachAbility.isConnectedToNetwork() {
            self.continueTimer()
            UtilityFunction.shared.performTaskInMainQueue {
                self.showControls()
                self.trigerPlayPauseNotification()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                if HungamaPlayerManager.shared.isPlaying() {
                    self.hideControls()
                }
            }
        } else {
            // Do Nothing
        }
    }
    
    func trigerPlayPauseNotification() {
        let state = self.previousPlayingState ?? false
        if state {
            self.togglePlayPauseHungamaPlayer(true) // Play
        }
        if state {
            playButton.setImage(UIImage(named: "playicon"), for: .normal)
        } else {
            playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        }
        let piImageName = state ? "pausePIIcons" : "playPIIcons"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: piImageName), object: nil)
    }
    
    @objc func configureSmallScreenIconOnHungama() {
        print("Configure Small Screen Icon For Hungama Hungama Player View")
        fullScreenButton.setImage(UIImage(named: "minimise"), for: .normal)
        isTrailerFullScreen = true
        backButton.isHidden = false
        if autoPlayView != nil && autoPlayView?.isHidden == false {
            autoPlayView?.backButton.isHidden = false
        }
    }
    
    @objc func configureFullScreenIconOnHungama() {
        print("Configure Full Screen For Hungama Player")
        fullScreenButton.setImage(UIImage(named: "fullscreen"), for: .normal)
        backButton.isHidden = true
        isTrailerFullScreen = false
        if autoPlayView != nil && autoPlayView?.isHidden == false {
            autoPlayView?.backButton.isHidden = true
        }
        if languageSupportView != nil && languageSupportView?.isHidden == false {
            self.togglePlayPauseHungamaPlayer(false)
        }
        if videoQualityView != nil && videoQualityView?.isHidden == false {
            self.togglePlayPauseHungamaPlayer(false)
        }
        delegate?.configureFavButton(isAddedToFav: isAddedToFavourite)
    }
    
    @objc func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("sessionExpireAlert"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PausePlayer"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ConfigureView"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("popPIScreen"), object: nil)
    }
    
    @objc func showExpiryAlert() {
        if continueWatchTimer != nil {
            continueWatchTimer?.invalidate()
            continueWatchTimer = nil
            stopPlayback()
            hideControls()
            if isTrailerFullScreen {
                if removeDeviceView == nil {
                    isPlayerPlaying = false
                    CustomLoader.shared.hideLoader()
                    removeDeviceView = BAUserRemoveView(frame: .zero)
                    self.contentView.addSubview(removeDeviceView!)
                     removeDeviceView?.fillParentView()
                }
            } else {
            DispatchQueue.main.async {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kDeviceRemovedHeader), .withMessage(kSessionExpire), .hidden, .hidden)) { _ in
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
              }
            }
        }
    }
}

extension BAHungamaPlayerView: BAVideoQualityViewDelegate {
    
    func sliderWillChangeSeekedDuration(value: Float) {
        seekedDuration = value == 0.0 ? 1.0 : value
        debugPrint(seekedDuration)
    }
    
    func moveToPlayer(selectedVideoQualityIndex: Int?, selectedVideoQualityName: String?) {
        if isTrailerFullScreen {
            videoQualityView?.removeFromSuperview()
            videoQualityView = nil
        }
        isPlayerPlaying = previousPlayingState ?? false
        showControls()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideControls()
        }
        selectedQualityIndex = selectedVideoQualityIndex ?? 0
        selectedQualityVideoName = selectedVideoQualityName ?? ""
        let qualityName = videoQualityArr[selectedVideoQualityIndex ?? 0]//selectedQualityVideoName.replacingOccurrences(of: "p", with: "")
        HungamaPlayerManager.shared.setPlaybackVariant(qualityName)
    }
}

extension BAHungamaPlayerView: BALanguageSupportViewDelegate {
    func popToPlayer(selectedSubTitle: Int?, selectedAudioTrack: Int?, selectedAudioName: String?, selectedSubtitleName: String?) {
        if isTrailerFullScreen {
            languageSupportView?.removeFromSuperview()
            languageSupportView = nil
        }
        showControls()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideControls()
        }
        isPlayerPlaying = previousPlayingState ?? false
        selectedAudiotrack = selectedAudioTrack ?? 0
        selectedSubtitle = selectedSubTitle ?? 0
        selectedAudioTrackName = selectedAudioName ?? ""
        selectedSubtitleTrackName = selectedSubtitleName ?? ""
        updateSubtitleTrack()
    }
    
    func updateSubtitleTrack() {
        if HungamaPlayerManager.shared.getSubtitleLanguages().count >= 1 {
            if selectedSubtitle == 0 {
                HungamaPlayerManager.shared.setSubtitleEnabled(false)
            } else {
                HungamaPlayerManager.shared.setSubtitleEnabled(true)
                HungamaPlayerManager.shared.setSubtitleLanguage(selectedSubtitleTrackName)
            }
        } else if HungamaPlayerManager.shared.getSubtitleLanguages().count == 0 {
            HungamaPlayerManager.shared.setSubtitleEnabled(false)
        }
    }
}

class Content: IContentVO
{
    private let id: String
    private let title: String
    private let posterURL: String
    private let type: HungamaPlayer.ContentType
    
    init(withId id:String, title:String, posterURL:String, type:HungamaPlayer.ContentType)
    {
        self.id = id
        self.title = title
        self.posterURL = posterURL
        self.type = type
    }
    
    func getId() -> String
    {
        return self.id
    }
    
    func getTitle() -> String
    {
        return self.title
    }
    
    func getType() -> HungamaPlayer.ContentType
    {
        return self.type
    }
    
    func getPosterURL() -> String
    {
        return self.posterURL
    }
}

extension BAHungamaPlayerView: BACustomSliderViewDelegate {
    
    func sliderWillDisplayPopUpView(slider: BACustomSliderView) {
        slider.sliderToolTip.alpha = 1.0
        slider.showToolTipViewAnimated(animated: true)
    }
    
    func sliderWillHidePopUpView(slider: BACustomSliderView) {
        slider.sliderToolTip.alpha = 0.0
        slider.showToolTipViewAnimated(animated: false)
    }
}


extension BAHungamaPlayerView: BANoInternetOnPlayerViewDelegate {
    func popToDetailScreen() {
        noInternetView?.backButton.isHidden = true
        if !isTrailer && isTrailerFullScreen {
            backAction()
        }
    }
    
    func retryAction() {
        noInternetView = nil
        if BAReachAbility.isConnectedToNetwork() {
            if !isTrailer {
                noInternetimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(showNoInternetView), userInfo: nil, repeats: true)
                isPlayerPlaying = previousPlayingState ?? false
                self.togglePlayPauseHungamaPlayer(true) //play
                if isPlayerPlaying && !isTrailer {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pausePIIcons"), object: nil)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playPIIcons"), object: nil)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    self.hideControls()
                }
            }
            //player.play()
            //configurePlayer(isNextPreviousEpisode: true)
            // TODO: Reload Player
        } else {
            showNoInternetView()
        }
    }
}



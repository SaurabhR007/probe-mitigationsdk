//
//  ProbePublicIP.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 10/02/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation

#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

typealias CompletionHandler = (String?, Error?) -> Void

func getPublicIPAddress(requestURL: URL, completion: @escaping CompletionHandler) {
    URLSession.shared.dataTask(with: requestURL) { (data, _, error) in
        if let error = error {
            completion(nil, CustomError.error(error))
            return
        }
        guard let data = data else {
            completion(nil, CustomError.noData)
            return
        }
        guard let result = String(data: data, encoding: .utf8) else {
            completion(nil, CustomError.undecodeable)
            return
        }
        let ipAddress = String(result.filter { !" \n\t\r".contains($0) })
        completion(ipAddress, nil)
    }.resume()
}

enum CustomError: LocalizedError {
    case noData
    case error(Error)
    case undecodeable

    public var errorDescription: String? {
        switch self {
        case .noData:
            return "No data response."
        case .error(let err):
            return err.localizedDescription
        case .undecodeable:
            return "Data undecodeable."
        }
    }
}

enum PublicIPAPIURLs {

    enum Hybrid: String, CaseIterable {
        case icanhazip = "https://icanhazip.com"
        case ipv6test = "https://v4v6.ipv6-test.com/api/myip.php"
        case seeip = "https://ip.seeip.org"
        case whatismyipaddress = "https://bot.whatismyipaddress.com"
        case ident = "https://ident.me/"
    }

    enum IPv4: String, CaseIterable {
        case icanhazip = "https://ipv4.icanhazip.com"
        case ipv6test = "https://v4.ipv6-test.com/api/myip.php"
        case seeip = "https://ip4.seeip.org"
        case whatismyipaddress = "https://ipv4bot.whatismyipaddress.com"
        case ident = "https://v4.ident.me/"

        case ipify = "https://api.ipify.org"

        case amazonaws = "https://checkip.amazonaws.com"
        case ipecho = "https://ipecho.net/plain"
    }

    enum IPv6: String, CaseIterable {
        case icanhazip = "https://ipv6.icanhazip.com"
        case ipv6test = "https://v6.ipv6-test.com/api/myip.php"
        case seeip = "https://ip6.seeip.org"
        case whatismyipaddress = "https://ipv6bot.whatismyipaddress.com"
        case ident = "https://v6.ident.me/"

        case ipify = "https://api6.ipify.org"
    }
}

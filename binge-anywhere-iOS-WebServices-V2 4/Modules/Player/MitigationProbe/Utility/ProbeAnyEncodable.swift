//
//  Probe.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 27/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct AnyEncodable: Encodable {
        let encodingProxy: (Encoder) throws -> Void
        
        init<T: Encodable>(_ value: T) {
                encodingProxy = {encoder in
                        try value.encode(to: encoder)
                }
        }
        
        func encode(to encoder: Encoder) throws {
                try encodingProxy(encoder)
        }
}

struct SyncData: Encodable {
        let sync: [AnyEncodable]
    
}







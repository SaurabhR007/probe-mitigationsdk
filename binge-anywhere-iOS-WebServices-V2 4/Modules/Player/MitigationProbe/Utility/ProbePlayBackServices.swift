//
//  ProbePlayBackServices.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 24/01/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation
import AVFoundation

//sourcery: AutoMockable
protocol PlaybackObservingService {
    var onPlaybackStalled: (() -> Void)? { get set }
    var onPlayToEndTime: (() -> Void)? { get set }
    var onFailedToPlayToEndTime: (() -> Void)? { get set }
}

final class ProbePlaybackObservingService: PlaybackObservingService {
    
    // MARK: - Input
    
    private weak var player: AVPlayer?
    
    // MARK: - Outputs
    
    var onPlaybackStalled: (() -> Void)?
    var onPlayToEndTime: (() -> Void)?
    var onFailedToPlayToEndTime: (() -> Void)?
    
    // MARK: - Init
    
    init(player: AVPlayer?) {
        self.player = player
        NotificationCenter.default.addObserver(self, selector: #selector(ProbePlaybackObservingService.itemPlaybackStalled),
                                               name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProbePlaybackObservingService.itemPlayToEndTime),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProbePlaybackObservingService.itemFailedToPlayToEndTime),
                                               name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemPlaybackStalled,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime,
                                                  object: nil)
    }
    
    private func hasReallyReachedEndTime(player: AVPlayer?) -> Bool {
        guard
            let duration = player?.currentItem?.duration.seconds
            else { return false }
        
        /// item current time when receive end time notification
        /// is not so accurate according to duration
        /// added +1 make sure about the computation
        let currentTime = player?.currentTime().seconds ?? 0 + 1
        return currentTime.rounded() >= duration.rounded()
    }
    
    @objc
    private func itemPlaybackStalled() {
        onPlaybackStalled?()
    }
    
    ///
    ///  AVPlayerItemDidPlayToEndTime notification can be triggered when buffer is empty and network is out.
    ///  We manually check if item has really reached his end time.
    ///
    @objc
    private func itemPlayToEndTime() {
        if let player = self.player{
            guard hasReallyReachedEndTime(player: player) else { itemFailedToPlayToEndTime(); return }
            onPlayToEndTime?()
        }
        
    }
    
    @objc
    private func itemFailedToPlayToEndTime() {
        onFailedToPlayToEndTime?()
    }
}

//
//  ProbeProbeManager.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 28/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import AVKit

import CryptoKit
import CommonCrypto
import os


public  var isTestLogsEnabled = true
var enableLogs = true

func sdkLogs(_ logs:String){
    if enableLogs{
        os_log(.default, "#MitigationProbeSDKLogs %{public}s", logs)
    }
}

func testLogs(_ logs:String){
    if isTestLogsEnabled {
        os_log(.default, "#ProbeMitigationSDK %{public}s", logs)
    }
    
}



extension String {
 //   @available(iOS 13.0, *)
    var MD5:String {
           get{
               let messageData = self.data(using:.utf8)!
               var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))

               _ = digestData.withUnsafeMutableBytes {digestBytes in
                   messageData.withUnsafeBytes {messageBytes in
                       CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
                   }
               }

               return digestData.map { String(format: "%02hhx", $0) }.joined()
           }
       }
      
}


extension JSONSerialization {
    
    static func loadJSON(withFilename filename: String) throws -> Any? {
        let fm = FileManager.default
        let urls = fm.urls(for: .documentDirectory, in: .userDomainMask)
        if let url = urls.first {
            var fileURL = url.appendingPathComponent(filename)
            fileURL = fileURL.appendingPathExtension("json")
            let data = try Data(contentsOf: fileURL)
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers, .mutableLeaves])
            return jsonObject
        }
        return nil
    }
    
    static func save(jsonObject: Any, toFilename filename: String) throws -> Bool{
        let fm = FileManager.default
        let urls = fm.urls(for: .documentDirectory, in: .userDomainMask)
        if let url = urls.first {
            var fileURL = url.appendingPathComponent(filename)
            fileURL = fileURL.appendingPathExtension("json")
            let data = try JSONSerialization.data(withJSONObject: jsonObject, options: [.prettyPrinted])
            try data.write(to: fileURL, options: [.atomicWrite])
            return true
        }
        
        return false
    }
}


extension CMTime {
    var roundedSeconds: TimeInterval {
        return seconds.rounded()
    }
    var hours:  Int { return Int(roundedSeconds / 3600) }
    var minute: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 3600) / 60) }
    var second: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 60)) }
    var positionalTime: String {
        return hours > 0 ?
            String(format: "%d:%02d:%02d",
                   hours, minute, second) :
            String(format: "%02d:%02d",
                   minute, second)
    }
}



extension CGFloat{
    func roundOffVideoQuality()->Int{
        let value = Int(self)
        switch value {
        case let value where value == 0:
            return 0
        case let value where value >= 100 && value <= 200 :
            return 144
        case let value where value > 200 && value <= 300:
            return 256
        case let value where value > 300 && value <= 400:
            return 360
        case let value where value > 400 && value <= 500 :
            return 480
        case let value where value > 500 && value <= 600 :
            return 576
        case let value where value > 600 && value <= 700 :
            return  640
        case let value where value > 850 && value <= 950 :
            return 900
        case let value where value >= 1000 && value <= 1100 :
            return 1080
        default:
            return value
            
        }
    }
}


extension AVAsset {
   /**
    * Get FPS from AVAsset
    */
   var fps: Float? {
      self.tracks(withMediaType: .video).first?.nominalFrameRate
   }
}

extension AVAssetTrack {
    var mediaFormat: String {
        var format = ""
        let descriptions = self.formatDescriptions as! [CMFormatDescription]
        for (index, formatDesc) in descriptions.enumerated() {
            // Get a string representation of the media type.
            let type =
                CMFormatDescriptionGetMediaType(formatDesc).toString()
            // Get a string representation of the media subtype.
            let subType =
                CMFormatDescriptionGetMediaSubType(formatDesc).toString()
            // Format the string as type/subType, such as vide/avc1 or soun/aac.
            format += "\(type)/\(subType)"
            // Comma-separate if there's more than one format description.
            if index < descriptions.count - 1 {
                format += ","
            }
        }
        return format
    }
    
}
 
extension FourCharCode {
    // Create a string representation of a FourCC.
    func toString() -> String {
        let bytes: [CChar] = [
            CChar((self >> 24) & 0xff),
            CChar((self >> 16) & 0xff),
            CChar((self >> 8) & 0xff),
            CChar(self & 0xff),
            0
        ]
        let result = String(cString: bytes)
        let characterSet = CharacterSet.whitespaces
        return result.trimmingCharacters(in: characterSet)
    }
}


extension NSDate{
   static var currentEpochTime:Int{
        get{
            return Int(NSDate().timeIntervalSince1970)
        }
    }
    
    static func getDurationInSeconds(startTime:TimeInterval,endTime:TimeInterval)->Int{
        let time1 = Date(timeIntervalSince1970: startTime)
        let time2 = Date(timeIntervalSince1970: endTime)
        let difference = Calendar.current.dateComponents([.second], from: time1, to: time2)
        let duration = difference.second! * 1000
        return Int(duration)
    }
}



extension AVPlayer{
    var playerCurrentTimeInSec:Int{
        get{
             let playerTime = self.currentTime()
             return Int(self.cmTimeToSeconds(playerTime))
        }
    }
    
    func cmTimeToSeconds(_ time: CMTime) -> Float{
        let seconds = CMTimeGetSeconds(time)
        if seconds.isNaN {
            return 0
        }
        return Float(seconds)
    }
    
   var currentBitRate:Int{
        get{
            if let event = self.currentItem?.accessLog()?.events.last {
               let value = event.indicatedBitrate
                guard !(value.isNaN || value.isInfinite) else {
                    return 0
                }
                return Int(value)
//                let bitsTransferred = Double(event.numberOfBytesTransferred * 8)
//                let bitrate =  bitsTransferred / Double(event.segmentsDownloadedDuration)
//                return bitrate
            }else{
                return 0
            }
        }
    }
    
    var isLiveVideoPlaying:Bool{
        get{
            if let event = self.currentItem?.accessLog()?.events.last {
                return event.playbackType?.uppercased() == "LIVE" ?  true : false
            }else{
                return false
            }
        }
    }
    
    var currentFrameRate:Int{
        get{
//            if self.currentItem?.tracks.count ?? 0 > 0{
//                return Int(self.currentItem?.tracks.last?.currentVideoFrameRate ?? 0)
//            }
//            return 0
            
            let item = self.currentItem // Your current item
            var fps: Float = 0.00
            for track in item?.tracks ?? [] {
                if (track.assetTrack?.mediaType == .video) {
                    fps = track.currentVideoFrameRate
                }
            }

            return Int(fps)
        }
    }
    
    func tracks()->(String?,String?){
        let video = 1635148593
        var audioCodec:String?
        var videoCodec:String?
        if let tracks = self.currentItem?.tracks{
            for track in tracks{
                if let formats = track.assetTrack?.formatDescriptions as? [CMFormatDescription] {
                    for format in formats {
                      
                       let fcc = CMFormatDescriptionGetMediaSubType(format)
                       let codecType = printFourCC(fcc)
                        if fcc == video{
                           videoCodec = codecType
                        }else{
                            audioCodec = codecType
                        }
                    }
                }
            }
        }
        return(videoCodec,audioCodec)
    }
    
    func printFourCC(_ fcc: FourCharCode)->String{
        let bytes: [CChar] = [
            CChar((fcc >> 24) & 0xff),
            CChar((fcc >> 16) & 0xff),
            CChar((fcc >> 8) & 0xff),
            CChar(fcc & 0xff),
            0]

        let result = String(cString: bytes)
        let characterSet = CharacterSet.whitespaces
        return result.trimmingCharacters(in: characterSet)
    }
    
    var videoDuration:Float{
        get{
            if let value = self.currentItem?.asset.duration{
                return cmTimeToSeconds(value)
            }
            return 0
        }
    }
    
    var currentResolution:String{
        get{
            let presentationSize = self.currentItem?.presentationSize
            let width = presentationSize?.width ?? 0
            let height = presentationSize?.height ?? 0
            return "\(width)X\(height)"
        }
    }
    
    var throughPut:Int{
        get{
            let obBitrate = self.currentItem?.accessLog()?.events.last?.observedBitrate
            return Int(obBitrate ?? 0)
        }
    
    }
}

extension UIDevice {
    
    /**
     Returns device ip address. Nil if connected via celluar.
     */
   static func getIP() -> String? {
        
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        
        if getifaddrs(&ifaddr) == 0 {
            
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next } // memory has been renamed to pointee in swift 3 so changed memory to pointee
                
                guard let interface = ptr?.pointee else {
                    return nil
                }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    guard let ifa_name = interface.ifa_name else {
                        return nil
                    }
                    let name: String = String(cString: ifa_name)
                    
                    if name == "en0" {  // String.fromCString() is deprecated in Swift 3. So use the following code inorder to get the exact IP Address.
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                    
                }
            }
            freeifaddrs(ifaddr)
        }
        
        return address
    }
    
}

extension String{
    static var CDN = ""
    
    static func getCDN(url: URL){
        let config: URLSessionConfiguration = URLSessionConfiguration.default
        let _: URLSession = URLSession(configuration: config)

        let dataTask: URLSessionDataTask = URLSession.shared.dataTask(with: url, completionHandler: {(data: Data!, urlResponse: URLResponse!, error: Error!) -> Void in

            if let httpUrlResponse = urlResponse as? HTTPURLResponse
            {
                if (error != nil) {
                    sdkLogs("Error Occurred: \(error.localizedDescription)")
                } else {
                    sdkLogs(" saurabh reponse \(httpUrlResponse.allHeaderFields)") // Error
                    if let xDemAuth = httpUrlResponse.allHeaderFields["Server"] as? String {
                           // use X-Dem-Auth here
                        String.CDN = xDemAuth
                    }
                 
                }
            }
            })

           dataTask.resume()
        }
}


extension TimeInterval {

    var seconds: Int {
        return Int(self.rounded())
    }

    var milliseconds: Int {
        return Int(self * 1_000)
    }
}

extension Date {
    var millisecondsSince1970: Int64 {
        Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}









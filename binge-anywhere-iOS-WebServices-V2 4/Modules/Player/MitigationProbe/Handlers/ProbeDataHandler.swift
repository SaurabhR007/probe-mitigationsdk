//
//  ProbeInterfaceDataHandler.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 28/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit
import os


protocol ProbeProtocol:AnyObject{
    func setFrameRateWithLoss(isEvent:Bool)
    func setPlayBackPostion(isEvent:Bool)
    func setCodecType(isEvent:Bool)
    func setDRMType(isEvent:Bool)
    func setAssetDuration(isEvent:Bool)
    func setBitrate(isEvent:Bool)
    func setThroughPut(isEvent:Bool)
    func setResolution(isEvent:Bool)
    func setLiveVideo(isEvent: Bool)
    func setIP(isEvent: Bool)
    func setUeid(isEvent:Bool)
    func setDurationOfPlayBack(isEvent:Bool)
    func setTimeStamp(isEvent: Bool)
    func setPreviousEvent(isEvent: Bool)
    func setTotalDurationOfPlayback(isEvent:Bool)
    func setTotalStallDuration(isEvent:Bool)
    func setTotalSwitchesUp(isEvent:Bool)
    func setTotalSwitchesDown(isEvent:Bool)
    func setLatency(isEvent:Bool)
    func setStartupAndRebufferDuration(isEvent:Bool)
    func setStallCountAndDuration(isEvent:Bool)
    func cleanUpData()
    
    func setPlatForm(isEvent:Bool)
}


final class ProbeDataHandler:NSObject {
    
    

    private var analysisAPIRespModel : AnalysisAPIResp?

    private var syncData: [AnyEncodable] = []
    var probePingDataSource:ProbePingModel = ProbePingModel()
    var probeEventDataSource:ProbeEventModel = ProbeEventModel()
    private var timer:Timer?
    private var service:ServiceHandler = ServiceHandler()
    private var shouldSyncData = true
    weak var delegate:ProbeProtocol?
    var lastPingDate:Int?
    
    override init() {
        super.init()
        self.syncWithServerWithTimeInterval(timeInterval: TimeInterval(MitigationDefaults.kaTimeInterval))
    }
    
    private func cleanDataSource(){
        self.probePingDataSource.ping?.pingSwitch?.up = nil
        self.probePingDataSource.ping?.pingSwitch?.down = nil
    }
    
    func stopTimer(){
        self.setDataBeforePingToServer(isEvent: false)
        self.formatAndSaveData(rawData: self.probePingDataSource)
        self.timer?.invalidate()
    }
    
    private func setDataBeforePingToServer(isEvent:Bool){
        
            delegate?.setPlayBackPostion(isEvent: isEvent)
            delegate?.setCodecType(isEvent: isEvent)
            delegate?.setDRMType(isEvent: isEvent)
            delegate?.setAssetDuration(isEvent: isEvent)
            delegate?.setBitrate(isEvent: isEvent)
            delegate?.setThroughPut(isEvent: isEvent)
            delegate?.setResolution(isEvent: isEvent)
            delegate?.setIP(isEvent: isEvent)
            delegate?.setLiveVideo(isEvent: isEvent)
            delegate?.setUeid(isEvent: isEvent)
            delegate?.setDurationOfPlayBack(isEvent: isEvent)
            delegate?.setTimeStamp(isEvent: isEvent)
            delegate?.setPreviousEvent(isEvent: isEvent)
            delegate?.setTotalDurationOfPlayback(isEvent:isEvent)
            delegate?.setTotalStallDuration(isEvent:isEvent)
            delegate?.setTotalSwitchesUp(isEvent:isEvent)
            delegate?.setTotalSwitchesDown(isEvent:isEvent)
            delegate?.setLatency(isEvent: isEvent)
            delegate?.setFrameRateWithLoss(isEvent: isEvent)
            delegate?.setStartupAndRebufferDuration(isEvent: isEvent)
            delegate?.setStallCountAndDuration(isEvent: isEvent)
        

    }
    
    private func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                sdkLogs("Something went wrong")
            }
        }
        return nil
    }
    
    private func formatAndSaveData<T:Encodable>(rawData:T){
        do {
    
            let data = try JSONEncoder().encode(AnyEncodable(rawData))
           //print("# Rahuld1 Probe Logs \(String(decoding: data, as: UTF8.self))")
            let printLog = String(decoding: data, as: UTF8.self)
            sdkLogs("Payload \(printLog)")
            let paramData = convertStringToDictionary(text: (String(decoding: data, as: UTF8.self))) ?? [:]
            if let pingDate = self.lastPingDate{
                let interval = TimeInterval(pingDate)
                let currentInterval = TimeInterval(NSDate.currentEpochTime)
                self.probePingDataSource.ping?.diffTime = NSDate.getDurationInSeconds(startTime:interval, endTime:currentInterval)
            }else{
                self.probePingDataSource.ping?.diffTime = 0
            }
            ServiceHandler().CallApiService(baseUrl: kBaseUrlProbe, paramDict: paramData, urlEndpoint: URLEndpoint.analysisURl, requestType: .POST, dataModelType: AnalysisAPIResp.self) {[weak self] (info, success,_)  in
                if success {
                    self?.analysisAPIRespModel = info
                    if let isDisable_qoe_beacons = self?.analysisAPIRespModel?.disable_qoe_beacons {
                        MitigationProbeSdkDefaults.isDisable_qoe_beacons = isDisable_qoe_beacons
                    }
                    if let isDisable_mitigation_poll = self?.analysisAPIRespModel?.disable_mitigation_poll {
                        MitigationProbeSdkDefaults.isDisable_mitigation_poll = isDisable_mitigation_poll
                    }
                }
               
                self?.lastPingDate = NSDate.currentEpochTime
                self?.cleanDataSource()
                self?.delegate?.cleanUpData()
            }
        
        } catch {
            sdkLogs("\(error)")
        }
    }
    
 
    
    private func syncWithServerWithTimeInterval(timeInterval:TimeInterval){
        self.timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) {[weak self] _ in
            if ((self?.shouldSyncData) != nil){
                
                if MitigationProbeSdkDefaults.isDisable_qoe_beacons {
                    self?.formatAndSaveData(rawData: ProbePingDisableModel())
                    
                }else {
                    self?.setDataBeforePingToServer(isEvent: false)
                    self?.formatAndSaveData(rawData: self!.probePingDataSource)
                }
                

            }
        }
    }
    
    func syncEventsWithServer(){
        if let value = self.probeEventDataSource.event?.event{
            testLogs("\(value) triggered successful")
        }
        
        if !MitigationProbeSdkDefaults.isDisable_qoe_beacons {
            
            self.setDataBeforePingToServer(isEvent: true)
            self.formatAndSaveData(rawData: self.probeEventDataSource)
        }
       
    }
}


    

//
//  MitigationHandler.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 10/02/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation

class MitigationHandler {
    
    var mitigationRegModel:MitigationRegModel?
    private var timer:Timer?
    
    func config(mitigationInterval:Int,mitigationRegModel:MitigationRegModel){
        self.mitigationRegModel = mitigationRegModel
        self.timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(mitigationInterval), repeats: true) {[weak self] _ in
            if !MitigationProbeSdkDefaults.isDisable_mitigation_poll {
                self?.getConfigForMitigation(rawData:self?.mitigationRegModel) { sucess in
                    if sucess{
                        
                    }
                }
            }
        
        }
    }
    
    private func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                sdkLogs("Something went wrong")
            }
        }
        return nil
    }
        
    func registerForMitigation<T:Encodable>(rawData:T,completionHandler:@escaping (Bool)->()){
        do {
    
            let data = try JSONEncoder().encode(AnyEncodable(rawData))
            let paramData = convertStringToDictionary(text: (String(decoding: data, as: UTF8.self)))
            sdkLogs("# formatAndSaveData \(String(describing: paramData))")
            
            ServiceHandler().CallApiService(baseUrl:kBaseUrlMitigation, paramDict: paramData!, urlEndpoint: URLEndpoint.register_session, requestType: .POST, dataModelType: MetigationRegRespModel.self) { (info, success , _)  in
                
                if success{
                    testLogs("Registration for mitigation successful")
                    if let resp = info{
                        MitigationDefaults.saveMitigationAppTime(time: NSDate.currentEpochTime)
                        MitigationDefaults.startUpBufferDuration = resp.registrationResponse?.cfg?.mc?.startupBuffDuration ?? 10
                        MitigationDefaults.reBufferDuration = resp.registrationResponse?.cfg?.mc?.rebufferingDuration ?? 10
                        MitigationDefaults.videoBitrate = resp.registrationResponse?.cfg?.mc?.estimatedDownloadRate ?? 0
                        MitigationDefaults.bu = resp.registrationResponse?.bu ?? "https://beacon.tskytech.com" //"http://qoe-beacon-86163666.ap-south-1.elb.amazonaws.com:8000"
                        MitigationDefaults.kaTimeInterval = resp.registrationResponse?.kaInterval ?? 30
                        MitigationDefaults.kcTimeInterval = resp.registrationResponse?.kcInterval ?? 30
                        MitigationDefaults.location = resp.registrationResponse?.deviceMapping?.location ?? "Unknown"
                        MitigationDefaults.saveMitigationID(id:resp.registrationResponse?.cfg?.mc?.mitigationID)
                        MitigationDefaults.saveMitigationAppTime(time: resp.registrationResponse?.cfg?.mc?.mitigationTimestamp ?? 0)
                    }
                    completionHandler(true)
                }else{
                    testLogs("Registration for mitigation Failed")
                    completionHandler(false)
                }
                
                sdkLogs("registerForMetigation \(String(describing: info))")
            }
        
        } catch {
            sdkLogs("\(error)")
        }
    }
    
    func getConfigForMitigation<T:Encodable>(rawData:T,completionHandler:@escaping (Bool)->()){
        do {
    
            let data = try JSONEncoder().encode(AnyEncodable(rawData))
           print("# formatAndSaveData \(String(decoding: data, as: UTF8.self))")
            
            let paramData = convertStringToDictionary(text: (String(decoding: data, as: UTF8.self))) ?? [:]
            print("paramData----->",paramData)
            ServiceHandler().CallApiService(baseUrl:kBaseUrlMitigation, paramDict: paramData, urlEndpoint: URLEndpoint.get_mitigation_config, requestType: .POST, dataModelType: MetigationConfigRespModel.self) { (info, success,_) in
               
                if success{
                    if let resp = info{
                        MitigationDefaults.saveMitigationAppTime(time: NSDate.currentEpochTime)
                        MitigationDefaults.startUpBufferDuration = resp.registrationResponse?.mc?.startupBuffDuration ?? 10
                        MitigationDefaults.reBufferDuration = resp.registrationResponse?.mc?.rebufferingDuration ?? 10
                        MitigationDefaults.videoBitrate = resp.registrationResponse?.mc?.estimatedDownloadRate ?? 0
                        MitigationDefaults.saveMitigationID(id:resp.registrationResponse?.mc?.mitigationID)
                        MitigationDefaults.saveMitigationAppTime(time:resp.registrationResponse?.mc?.mitigationTimestamp)
                        
                    }
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
                sdkLogs("registerForMetigation \(String(describing: info))")
            }
        
        } catch {
            print(error)
        }
    }
    
}

//
//  ProbeServiceHandler.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 01/10/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

class ServiceHandler {
    
    func CallApiService<T: Decodable>(baseUrl:String,paramDict: [String: Any], skipLoader:Bool = false, urlEndpoint:String, requestType :KHTTPMethod, dataModelType: T.Type, completion: @escaping (T?,Bool,[String:Any]?) -> Void) {
        TANetworkManager().requestApi(baseUrl: baseUrl, serviceName: urlEndpoint, method: requestType, postData: paramDict, dataModelType: dataModelType) { (result, error , param) in
          
            if error == nil {
                if let result1 = result{
                    completion(result1, true, param)
                } else {
                    completion(nil, true, param)
                }
            } else {
                completion(nil, false, param)
            }
        }
    }
    
    
}

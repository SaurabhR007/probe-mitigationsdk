//
//  MitigationResponseModel.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 17/02/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation
import Foundation

// MARK: - MetigationConfigRespModel
import Foundation

// MARK: - MetigationConfigRespModel
struct MetigationConfigRespModel: Codable {
    var registrationResponse: RegistrationResponse?

    enum CodingKeys: String, CodingKey {
        case registrationResponse = "mitigation_config_response"
    }
}

struct MetigationRegRespModel: Codable {
    var registrationResponse: RegistrationResponse?

    enum CodingKeys: String, CodingKey {
        case registrationResponse = "registration_response"
    }
}

// MARK: - RegistrationResponse
struct RegistrationResponse: Codable {
    var version: String?
    var bu: String?
    var kaInterval: Int?
    var kcInterval: Int?
    var serverClkOffset: Int?
    var cfg: CFG?
    var deviceMapping: DeviceMapping?
    var mc: Mc?

    enum CodingKeys: String, CodingKey {
        case version = "version"
        case bu = "bu"
        case kaInterval = "kaInterval"
        case serverClkOffset = "serverClkOffset"
        case cfg = "cfg"
        case deviceMapping = "deviceMapping"
        case kcInterval =  "kcInterval"
        case mc = "mc"
    }
}

// MARK: - CFG
struct CFG: Codable {
    var pc: PC?
    var mc: Mc?

    enum CodingKeys: String, CodingKey {
        case pc = "pc"
        case mc = "mc"
    }
}

// MARK: - Mc
struct Mc: Codable {
    var startupBuffDuration: Int?
    var rebufferingDuration: Int?
    var estimatedDownloadRate: Int?
    var bufferingStyle: String?
    var mitigationID: String?
    var mitigationTimestamp: Int?

    enum CodingKeys: String, CodingKey {
        case startupBuffDuration = "startupBuffDuration"
        case rebufferingDuration = "rebufferingDuration"
        case estimatedDownloadRate = "estimatedDownloadRate"
        case bufferingStyle = "bufferingStyle"
        case mitigationID = "mitigationID"
        case mitigationTimestamp = "mitigationTimestamp"
    }
}

// MARK: - PC
struct PC: Codable {
    var renditionSwitch: String?
    var stalls: String?
    var userActions: String?
    var qualityChanged: String?

    enum CodingKeys: String, CodingKey {
        case renditionSwitch = "renditionSwitch"
        case stalls = "stalls"
        case userActions = "userActions"
        case qualityChanged = "qualityChanged"
    }
}

// MARK: - DeviceMapping
struct DeviceMapping: Codable {
    var location: String?
    var deviceType: String?

    enum CodingKeys: String, CodingKey {
        case location = "location"
        case deviceType = "deviceType"
    }
}

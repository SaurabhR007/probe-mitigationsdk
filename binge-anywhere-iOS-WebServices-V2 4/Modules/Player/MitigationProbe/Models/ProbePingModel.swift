//
//  ProbeMainModel.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 29/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import CoreTelephony



// MARK: - ProbePingModel
struct ProbePingModel: Codable {
    var ping: PINGModel?
    init(){}
    enum CodingKeys: String, CodingKey {
        case ping = "ping"
    }
}

struct ProbePingDisableModel: Codable {
    var ping: PINGDisableModel = PINGDisableModel(timeStamp:NSDate.currentEpochTime)
    init(){}
    enum CodingKeys: String, CodingKey {
        case ping = "ping"
    }
}

struct PINGDisableModel: Codable {
    
    var platform: String = "iOS"
    var deviceType: String = "Mobile"
    var timestamp: Int?
    
    init(timeStamp:Int) {
        self.timestamp = timeStamp
    }
    enum CodingKeys: String, CodingKey {
        case platform = "platform"
        case timestamp = "timestamp"
        case deviceType = "deviceType"
    }
    
}

// MARK: - Ping
struct PINGModel: Codable {


    var version: String = "1.0.0"
    var sdkVersion: String =  "1.0.0"
    var player: String = "AVPlayer"
    var platform: String = "iOS"
    var deviceType: String = "Mobile"
    var manufacturer: String = "Apple"
    var model: String = UIDevice.current.model
    var udid: String = UIDevice.current.identifierForVendor?.uuidString ?? ""
    var networkType: String = PINGModel.getConnectionType()
    var sessionID: String?
    var playerApp: String = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? "Tata Sky Binge"
    var provider: String?
    var videoID: String?
    var has: String?
    //Probe Dependent properties
    var aCodec: String?
    var vCodec: String?
    var cdn: String? = String.CDN
    var ip: String?
    var timestamp: Int?
    var playbackPosInSEC: Int?
    var assetDuration: Int?
    var frameRate: Int?
    var bitrate: Int?
    var resolution: String?
    var throughput: Int?
    var durationOfPlayback: Int?
    var stall: Stall? = Stall()
    var pingSwitch: Switch? = Switch()
    var drm: String?
    var live: String = "false"
    var event: String?
    var ueid: String?
    var totalDurationOfPlayback:Int?
    var totalStallDuration:Int = 0
    var totalSwitchesUp:Int?
    var totalSwitchesDown:Int?
    var diffTime:Int?
    var mitigationID:String?
    var location:String = MitigationDefaults.location
    var ua:String = "iOS App"
    var frameLoss:Int = 0
    var sbl:Int = MitigationDefaults.startUpBufferDuration
    var rbl:Int = MitigationDefaults.reBufferDuration
    var contentTitle:String?
    var contentType:String?
    
    //Upadte the param based on backend
    var videoTitle:String?
    var playerVersion:String = "1.3.0.4"
    
    public init(sessionID:String?,videoTitle:String?,contentType:String?,playerApp: String?,provider: String?,videoID:String?,has:String?,drm:String?) {
        self.sessionID = sessionID
        self.playerApp = playerApp ?? ""
        self.provider = provider
        self.videoID = videoID
        self.has = has
        self.videoTitle = videoTitle
        self.contentType = contentType
        self.drm = drm
    }
    
    enum CodingKeys: String, CodingKey {
        case version = "version"
        case sdkVersion = "sdkVersion"
        case player = "player"
        case playerApp = "playerApp"
        case cdn = "cdn"
        case ip = "ip"
        case provider = "provider"
        case udid = "udid"
        case platform = "platform"
        case deviceType = "deviceType"
        case manufacturer = "manufacturer"
        case model = "model"
        case networkType = "networkType"
        case sessionID = "sessionId"
        case timestamp = "timestamp"
        case playbackPosInSEC = "playbackPosInSec"
        case videoID = "videoId"
        case assetDuration = "assetDuration"
        case frameRate = "frameRate"
        case aCodec = "aCodec"
        case vCodec = "vCodec"
        case bitrate = "bitrate"
        case resolution = "resolution"
        case throughput = "throughput"
        case durationOfPlayback = "durationOfPlayback"
        case stall = "stall"
        case pingSwitch = "switch"
        case has = "has"
        case drm = "drm"
        case live = "live"
        case event = "event"
        case ueid = "ueid"
        case totalDurationOfPlayback = "totalDurationOfPlayback"
        case totalStallDuration = "totalStallDuration"
        case totalSwitchesUp = "totalSwitchesUp"
        case totalSwitchesDown = "totalSwitchesDown"
        case diffTime = "diffTime"
        case mitigationID = "mitigationID"
        case location = "location"
        case ua = "ua"
        case frameLoss = "frameLoss"
        case sbl = "sbl"
        case rbl = "rbl"
        case contentTitle = "contentTitle"
        case contentType = "contentType"
        
        case videoTitle = "videoTitle"
        case playerVersion = "playerVersion"
      
    }
    
    static func getConnectionType() -> String {
            guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
                return "NO INTERNET"
            }

            var flags = SCNetworkReachabilityFlags()
            SCNetworkReachabilityGetFlags(reachability, &flags)

            let isReachable = flags.contains(.reachable)
            let isWWAN = flags.contains(.isWWAN)

            if isReachable {
                if isWWAN {
                    let networkInfo = CTTelephonyNetworkInfo()
                    let carrierType = networkInfo.serviceCurrentRadioAccessTechnology

                    guard let carrierTypeName = carrierType?.first?.value else {
                        return "UNKNOWN"
                    }

                    switch carrierTypeName {
                    case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                        return "Cellular-2G"
                    case CTRadioAccessTechnologyLTE:
                        return "Cellular-4G"
                    default:
                        return "Cellular-3G"
                    }
                } else {
                    return "WiFi"
                }
            } else {
                return "No_Internet"
            }
        }
}



// MARK: - Switch
struct Switch: Codable {
    var up: [String:Int]?
    var down: [String: Int]?
    init(){}
    enum CodingKeys: String, CodingKey {
        case up = "up"
        case down = "down"
    }
}

// MARK: - Stall
struct Stall: Codable {
    var count: Int?
    var duration: Int?
    
    init(){}

    enum CodingKeys: String, CodingKey {
        case count = "count"
        case duration = "duration"
    }
}



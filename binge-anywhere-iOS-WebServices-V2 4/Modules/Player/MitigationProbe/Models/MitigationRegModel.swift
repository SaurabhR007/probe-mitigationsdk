//
//  MitigationRegModel.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 10/02/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation

struct MitigationRegModel: Codable {
    var req: Req?

    enum CodingKeys: String, CodingKey {
        case req = "req"
    }
}

struct MitigationConfigModel: Codable {
    var req: Req?
    
    enum CodingKeys: String, CodingKey {
        case req = "configRequest"
    }
}

// MARK: - Req
struct Req: Codable {
    
    var version: String = "1.0.0"
    var ueid: String?
    var uuid: String?
    var mitigationCFGID: String?
    var mitigationApplTime: Int?
    var clientIP: String?
    var ua: String?
    var clientClock:Int = NSDate.currentEpochTime
    

    enum CodingKeys: String, CodingKey {
        case version = "version"
        case ueid = "ueid"
        case uuid = "udid"
        case mitigationCFGID = "mitigationCfgID"
        case mitigationApplTime = "mitigationApplTime"
        case clientIP = "clientIP"
        case ua = "ua"
        case clientClock = "clientClock"
    }
}



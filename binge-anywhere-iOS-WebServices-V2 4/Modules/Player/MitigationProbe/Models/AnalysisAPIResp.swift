

import Foundation
import UIKit


struct AnalysisAPIResp : Codable {
    
	let disable_qoe_beacons : Bool?
	let disable_mitigation_poll : Bool?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case disable_qoe_beacons = "disable_qoe_beacons"
		case disable_mitigation_poll = "disable_mitigation_poll"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		disable_qoe_beacons = try values.decodeIfPresent(Bool.self, forKey: .disable_qoe_beacons)
		disable_mitigation_poll = try values.decodeIfPresent(Bool.self, forKey: .disable_mitigation_poll)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}

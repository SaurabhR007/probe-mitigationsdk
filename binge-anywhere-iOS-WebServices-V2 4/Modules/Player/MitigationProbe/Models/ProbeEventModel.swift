//
//  ProbeEventModel.swift
//  SampleAVPlayer
//
//  Created by Saurabh Rode on 21/01/22.
//

import Foundation
import UIKit

// MARK: - ProbeEvent
struct ProbeEventModel: Codable {
    var event: EventModel?

    enum CodingKeys: String, CodingKey {
        case event = "event"
    }
}

// MARK: - Event
public struct EventModel: Codable {
    

    var version: String = "1.0.0"
    var sdkVersion: String =  "1.0.0"
    var player: String = "AVPlayer"
    var platform: String = "iOS"
    var deviceType: String = "Mobile"
    var manufacturer: String = "Apple"
    var model: String = UIDevice.current.model
    var udid: String = UIDevice.current.identifierForVendor?.uuidString ?? ""
    var networkType: String = PINGModel.getConnectionType()
    var playerApp: String = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? "Tata Sky Binge"
    var cdn: String? = String.CDN
    var ip: String?
    var provider: String?
    var ueid: String?
    var sessionID: String?
    var timestamp: Int?
    var playbackPosInSEC: Int?
    var videoID: String?
    var assetDuration: Int?
    var frameRate: Int?
    var aCodec: String?
    var vCodec: String?
    var bitrate: Int?
    var resolution: String?
    var throughput: Int?
    var has: String?
    var drm: String?
    var live: String?
    var event: String?
    var eventData: EventData? = EventData()
    var eventPrev: String?
    var errorDetails: String?
    var url:URL?
    var mitigationID:String?
    var location:String?
    var ua:String = "iOS User Agent"
    var contentTitle:String?
    var contentType:String?
    
    //Upadte the param based on backend
    var videoTitle:String?
    var playerVersion:String = "1.3.0.4"//String(VideoPlayerVersionNumber)
    
    var durationOfPlayback: Int?
    var stall: Stall? = Stall()
    var totalDurationOfPlayback:Int?
    var totalStallDuration:Int = 0
    
    
   public init(videoID:String?,contentType:String?,videoTitle:String?,provider: String?,drm:String?,url:URL?) {
        self.provider = provider
        self.videoID = videoID
        self.contentType = contentType
        self.videoTitle = videoTitle
        self.drm = drm
        if let url1 = url {
            self.has = url1.pathExtension == "m3u8" ? "HLS" : "MSS"
        }else{
            self.has = "HLS"
        }
    }
    
    public init(videoID:String?,provider: String?,drm:String?,url:URL?) {
        self.provider = provider
        self.videoID = videoID
        self.contentType = nil
        self.contentTitle = nil
        if drm == nil{
            self.drm = "Fairplay"
        }else{
            self.drm = drm
        }
        
        if let url1 = url {
            self.has = url1.pathExtension == "m3u8" ? "HLS" : "MSS"
        }else{
            self.has = "HLS"
        }
    }

    enum CodingKeys: String, CodingKey {
        case version = "version"
        case sdkVersion = "sdkVersion"
        case player = "player"
        case playerApp = "playerApp"
        case cdn = "cdn"
        case ip = "ip"
        case provider = "provider"
        case ueid = "ueid"
        case udid = "udid"
        case platform = "platform"
        case deviceType = "deviceType"
        case manufacturer = "manufacturer"
        case model = "model"
        case networkType = "networkType"
        case sessionID = "sessionId"
        case timestamp = "timestamp"
        case playbackPosInSEC = "playbackPosInSec"
        case videoID = "videoId"
        case assetDuration = "assetDuration"
        case frameRate = "frameRate"
        case aCodec = "aCodec"
        case vCodec = "vCodec"
        case bitrate = "bitrate"
        case resolution = "resolution"
        case throughput = "throughput"
        case has = "has"
        case drm = "drm"
        case live = "live"
        case event = "event"
        case eventData = "eventData"
        case eventPrev = "eventPrev"
        case errorDetails = "errorDetails"
        case mitigationID = "mitigationID"
        case location = "location"
        case ua = "ua"
        case contentTitle = "contentTitle"
        case contentType = "contentType"
        
        case videoTitle = "videoTitle"
        case playerVersion = "playerVersion"
        case durationOfPlayback = "durationOfPlayback"
        case stall = "stall"
        case totalDurationOfPlayback = "totalDurationOfPlayback"
        case totalStallDuration = "totalStallDuration"

    }
}

// MARK: - EventData
struct EventData: Codable {
    var latency: Int?
    var vrt:Int = 0
    var desc:[String:String]?
    
    public init(){}
    enum CodingKeys: String, CodingKey {
        case latency = "latency"
        case desc = "desc"
        case vrt = "vrt"
    }
}


////Added by Rahuld on 26th Aug
//
//// MARK: - Stall
//struct Stall: Codable {
//    var count: Int?
//    var duration: Int?
//
//    init(){}
//
//    enum CodingKeys: String, CodingKey {
//        case count = "count"
//        case duration = "duration"
//    }
//}



//
//  ProbeInterface.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 29/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit
import AVKit

public class ProbeInterface:NSObject,ProbeProtocol{
    private weak var player:AVPlayer?
    private var mitigationInterface:MitigationInterface?
    private var playerItemBufferEmptyObserver: NSKeyValueObservation?
    private var playerItemBufferKeepUpObserver: NSKeyValueObservation?
    private var playerItemBufferFullObserver: NSKeyValueObservation?
    private var probePlaybackObservingService:ProbePlaybackObservingService?
    
    private var dataHandler:ProbeDataHandler? = ProbeDataHandler()
    
    private var lastIndicatedBitrate:Double = 1
    private var currentBufferStatus:ProbePlayerStatus = .bufferEnd
    private var stallStartTime:Int?
    private var playBackTime:Int = 0
    private var lastProbeEvent:ProbeEventType = .STOPPED
    private var playbackStarted:Bool = false
    private var totalDurationOfPlayback = -2
    private var totalSwitchesUP = 0
    private var totalSwitchesDown = 0
    private var totalStalls = 0
    private var totalStallDuration = 0
    private var currentTotalStalls = 0
    private var currentTotalStallDuration = 0
    private var latency = 0
    private var latencyDate:Int?
    private var videoRestartTime = 0
    private var isSeeked = false
    
    
    
    
    public static func registerForMitigationSession(rmn:String,enableTestLogs:Bool){
       // UserDefaults.standard.setValue(ueid, forKey:ProbeUserDefaults.ueid.rawValue)
       // MitigationInterface(player:nil).register(ueid: ueid)
        isTestLogsEnabled = enableTestLogs
        let udid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        MitigationDefaults.ueid = rmn
        MitigationDefaults.udid = udid
        getPublicIPAddress(requestURL:URL(string: PublicIPAPIURLs.IPv4.ipify.rawValue)!) { (ip, error) in
            if let ipAdr = ip{
                MitigationDefaults.ip = ipAdr
                let param =  MitigationRegModel(req: Req(ueid:rmn.MD5, uuid:udid, mitigationCFGID:MitigationDefaults.getMitigationID(), mitigationApplTime:MitigationDefaults.getMitigationAppTime(), clientIP:ipAdr, ua:"iOS User Agent"))
                
                MitigationHandler().registerForMitigation(rawData:param, completionHandler:{ sucess in
                    if sucess{
                        testLogs("Registration for mitigation successful")
                    }else{
                        testLogs("Registration for mitigation unsuccessful")
                    }
                    
                })
            }
        }
        
    }
    
    private func setUpModels(requiredInfo:EventModel){
        var pingModel = ProbePingModel()
        var eventModel = ProbeEventModel()
        let udid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let sessionID = "\(NSDate.currentEpochTime)\(String(describing:udid))".MD5
        let playerApp = requiredInfo.playerApp
        let provider = requiredInfo.provider
        let videoID = requiredInfo.videoID
        let has = requiredInfo.has
        let drm = requiredInfo.drm
                
        pingModel.ping = PINGModel(sessionID: sessionID ,videoTitle: requiredInfo.videoTitle,contentType: requiredInfo.contentType, playerApp: playerApp, provider: provider, videoID: videoID, has: has,drm: drm)
        
        eventModel.event = requiredInfo
        eventModel.event?.sessionID = sessionID
        self.dataHandler?.probePingDataSource = pingModel
        self.dataHandler?.probeEventDataSource = eventModel
        self.dataHandler?.delegate = self
    }
    
    public init(player:AVPlayer?,requiredInfo:EventModel) {
        super.init()
        self.player = player
        self.setUpModels(requiredInfo: requiredInfo)
        let interval = CMTimeMake(value:10, timescale:10)
        self.probePlaybackObservingService = ProbePlaybackObservingService(player: self.player)
        self.addEventObservers()
        self.mitigationInterface = MitigationInterface(player: self.player)
  
        self.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { [weak self] time in
            sdkLogs("#Probe Mitigation segmentsDownloadedDuration \(String(describing: self?.player?.playerCurrentTimeInSec)) ... \(CMTimeGetSeconds(self?.availableDuration() ?? CMTime.zero))")

            guard let lastEvent = self?.player?.currentItem?.accessLog()?.events.last
            else {
                return
            }
            
            if let value = self?.player?.videoDuration{
                if let _ = self?.player?.playerCurrentTimeInSec{
                    
                if Int(value) == self?.player?.playerCurrentTimeInSec{
                    self?.sendEventFor(eventType: .COMPLETED)
                }
                }
            }
           
            
            
            let currentIndicatedBitrate = lastEvent.indicatedBitrate
            guard !(currentIndicatedBitrate.isNaN || currentIndicatedBitrate.isInfinite) else {
                return
            }
            if self?.lastIndicatedBitrate != currentIndicatedBitrate {
                sdkLogs("#Probe bitrateSwitchObserver bitrate changed from \(self?.lastIndicatedBitrate ?? 0) to \(currentIndicatedBitrate )  ###")
                if currentIndicatedBitrate > self?.lastIndicatedBitrate ?? 0{
                    if (self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.up != nil){
                        if let _ = self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.up?[String(NSDate.currentEpochTime)]{
                            
                        }else{
                            self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.up?[String(NSDate.currentEpochTime)] = Int(currentIndicatedBitrate )
                            self?.totalSwitchesUP = self!.totalSwitchesUP + 1
                        }
                       
                    }else{
                        self?.totalSwitchesUP = self!.totalSwitchesUP + 1
                        self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.up  = [String(NSDate.currentEpochTime) : Int(currentIndicatedBitrate)]
                    }
                    sdkLogs("#Probe switch UP \([String(NSDate.currentEpochTime):Int(currentIndicatedBitrate )])")
                }else{
                    if (self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.down != nil){
                        if let _ = self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.down?[String(NSDate.currentEpochTime)]{
                            
                        }else{
                            self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.down?[String(NSDate.currentEpochTime)] = Int(currentIndicatedBitrate )
                            self?.totalSwitchesDown = self!.totalSwitchesDown  + 1
                        }
                     
                    }else{
                        self?.dataHandler?.probePingDataSource.ping?.pingSwitch?.down  = [String(NSDate.currentEpochTime) : Int(currentIndicatedBitrate)]
                        self?.totalSwitchesDown = self!.totalSwitchesDown  + 1
                    }
                    sdkLogs("#Probe switch Down \([String(NSDate.currentEpochTime):Int(currentIndicatedBitrate )])")
                }
                self?.lastIndicatedBitrate = currentIndicatedBitrate
            }
            
            if self?.player?.rate == 1{
                
                if !(self?.playbackStarted ?? true) {
                    self?.addBufferObserver()
                    self?.mitigationInterface?.addBufferObserver()
                    self?.playbackStarted = true
                    let endTime = NSDate.currentEpochTime
                    if let startTime = self?.latencyDate{
                        self?.latency =  NSDate.getDurationInSeconds(startTime:TimeInterval(startTime), endTime: TimeInterval(endTime))
                    }
                    do{
                        sleep(1)
                    }
                    self?.sendEventFor(eventType: .STARTED)
                }
                self?.totalDurationOfPlayback =  self!.totalDurationOfPlayback + 1
    
            }
        })
        
        
    }
    
    
    func availableDuration() -> CMTime
    {
        if let range = self.player?.currentItem?.loadedTimeRanges.first {
            return CMTimeRangeGetEnd(range.timeRangeValue)
        }
        return .zero
    }
        
    func addBufferObserver() {
        
        playerItemBufferEmptyObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackBufferEmpty, options: [.new]) { [weak self] (_, _) in
            self?.sendEventFor(eventType: .BUFFERING)
            self?.playBackStallStarted()
        }
        
        playerItemBufferKeepUpObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackLikelyToKeepUp, options: [.new]) { [weak self] (_, _) in
            self?.playBackStallStopped()
        }
        
        playerItemBufferFullObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackBufferFull, options: [.new]) {  [weak self] (_, _) in
            self?.playBackStallStopped()
        }
        self.player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if let status = player?.timeControlStatus {
                switch status{
                case .paused:
                    if self.lastProbeEvent != .PAUSED{
                        self.sendEventFor(eventType: .PAUSED)
                    }
                break
                case .waitingToPlayAtSpecifiedRate:
                break
                case .playing:
                    if isSeeked {
                      let startTime = self.videoRestartTime
                      let endTime = NSDate.currentEpochTime
                      let seekTime =   NSDate.getDurationInSeconds(startTime:TimeInterval(startTime), endTime: TimeInterval(endTime))
                        self.dataHandler?.probeEventDataSource.event?.eventData?.vrt = seekTime
                       
                        self.videoRestartTime = 0
                    }
                    if self.lastProbeEvent == .PAUSED{
                       self.sendEventFor(eventType: .RESUMED)
            
                    }else if isSeeked{
                        self.sendEventFor(eventType: .RESUMED)
                        self.isSeeked = false
                    }
                    if isSeeked{
                        self.isSeeked = false
                    }
                    
                break
                @unknown default:
                    sdkLogs("For future versions")
                }
            }
        }
       
//        if keyPath == "status", playerItem.status == .failed{
//
//            if let error = playerItem.error as NSError? {
//                       let errorCode = error.code
//                self.sendEvent(eventType: .ERROR)
//            }
//        }
    }
    
    func playBackStallStarted(){
        if !isSeeked {
            if self.currentBufferStatus != .playbackStalled{
                if self.currentTotalStalls > 0{
                    self.currentTotalStalls = self.currentTotalStalls + 1
                }else{
                    self.currentTotalStalls = 1
                }
                
                if  self.currentTotalStallDuration > 0 {
                }else{
                    self.currentTotalStallDuration = 0
                }
                self.currentBufferStatus = .playbackStalled
                self.stallStartTime = NSDate.currentEpochTime
                self.totalStalls = self.totalStalls + 1
            }
        }
       
    }
    
    func playBackStallStopped(){
        if self.currentBufferStatus == .playbackStalled{
            let endTime = NSDate.currentEpochTime
            let startTime = self.stallStartTime ?? 0
            let duration = NSDate.getDurationInSeconds(startTime: TimeInterval.init(startTime), endTime:TimeInterval.init(endTime))
            if self.currentTotalStallDuration > 0 {
                self.currentTotalStallDuration  = self.currentTotalStallDuration + duration
            }else{
                self.currentTotalStallDuration = duration
            }
            self.currentBufferStatus = .bufferEnd
            self.setStallCountAndDuration(isEvent: false)
            self.stallStartTime = nil
        }
    }
    
    func addEventObservers(){
        self.player?.currentItem?.addObserver(self, forKeyPath: "status", options: .new, context: nil)
        self.probePlaybackObservingService?.onPlaybackStalled = { [weak self] in
            self?.playBackStallStarted()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object:nil, queue: OperationQueue.main) { (notification) in
            self.sendEventFor(eventType: .STOPPED)
        }
    }
    
    
    public func configureProbe(videoQuality:Bool,bitrate:Bool,stalledCount:Bool){
        sdkLogs("#Probe Configured")
        if videoQuality {
        }
        if bitrate {
            NotificationCenter.default.addObserver(self, selector: #selector(bitrateSwitchObserver(notification:)), name: NSNotification.Name(NSNotification.Name.AVPlayerItemNewAccessLogEntry.rawValue), object: nil)
        }
        if stalledCount{
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemNewAccessLogEntry,
                                                  object: nil)
        self.playerItemBufferEmptyObserver = nil
        self.playerItemBufferKeepUpObserver = nil
        self.playerItemBufferFullObserver = nil
    }
    
    public func sendEvent(eventType:ProbeUserEvent,eventDetailsIfAny:[String:String] = [:]){
        let event =  ProbeEventType(rawValue: eventType.rawValue) ?? .BUFFERING
        self.sendEventFor(eventType: event, eventDetailsIfAny:eventDetailsIfAny)
    }
  
    func sendEventFor(eventType:ProbeEventType,eventDetailsIfAny:[String:String] = [:]){
        if eventType == .PLAYCLICKED{
            self.latencyDate = NSDate.currentEpochTime
        }
        if eventType == .SEEKED{
            self.playBackStallStopped()
            self.isSeeked = true
            self.mitigationInterface?.setBufferuration(bufferType: .ReBuffer)
            self.videoRestartTime = NSDate.currentEpochTime
        }
        
        if !eventDetailsIfAny.isEmpty{
            self.dataHandler?.probeEventDataSource.event?.eventData?.desc = eventDetailsIfAny
        }
        self.dataHandler?.probeEventDataSource.event?.event = eventType.rawValue
        self.dataHandler?.syncEventsWithServer()
        if self.lastProbeEvent == .RESUMED
        {
            self.dataHandler?.probeEventDataSource.event?.eventData?.vrt = 0
        }
        self.lastProbeEvent = eventType
        
        if eventType == .STOPPED || eventType == .COMPLETED {
            self.releaseProb()
        }
        
        
    }

    func releaseProb(){
        self.dataHandler?.stopTimer()
        self.mitigationInterface?.stopApplyingMitigation()
        self.player = nil
        self.lastIndicatedBitrate = 0
        self.dataHandler = nil
        self.mitigationInterface = nil
        self.probePlaybackObservingService = nil
    }
    
    @objc private func bitrateSwitchObserver(notification: Notification){
        sdkLogs("#Probe bitrateSwitchObserver")
        
    }
    
}

extension ProbeInterface{
    
    func setTimeStamp(isEvent: Bool){
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.timestamp =  NSDate.currentEpochTime
        }else{
            self.dataHandler?.probePingDataSource.ping?.timestamp =   NSDate.currentEpochTime
        }
    }
    
    func setFrameRateWithLoss(isEvent: Bool) {
//        if self.lastProbeEvent ==  .STOPPED{
//            if isEvent{
//                self.dataHandler?.probeEventDataSource.event?.frameRate = 0
//            }else{
//                self.dataHandler?.probePingDataSource.ping?.frameRate =  0
//            }
//        }
        
        if let value = self.player?.currentFrameRate {
            if value > 0 {
                if isEvent{
                    self.dataHandler?.probeEventDataSource.event?.frameRate = value
                }else{
                    if let lastFrameRate = self.dataHandler?.probePingDataSource.ping?.frameRate{
                        if lastFrameRate >= value{
                            self.dataHandler?.probePingDataSource.ping?.frameLoss = lastFrameRate - value
                        }else{
                            self.dataHandler?.probePingDataSource.ping?.frameLoss = 0
                        }
                    }
                    self.dataHandler?.probePingDataSource.ping?.frameRate = value
                }
            }else{
                if isEvent {
                    if let lastFrameRate = self.dataHandler?.probePingDataSource.ping?.frameRate{
                        self.dataHandler?.probeEventDataSource.event?.frameRate = lastFrameRate
                        
                    }else {
                        
                        self.dataHandler?.probeEventDataSource.event?.frameRate = 0
                    }

                }else {
                    if let lastFrameRate = self.dataHandler?.probePingDataSource.ping?.frameRate{
                        self.dataHandler?.probePingDataSource.ping?.frameRate = lastFrameRate

                    }else {
                        
                        self.dataHandler?.probePingDataSource.ping?.frameRate = 0

                    }

                }

            }
        }else {
            
            if isEvent {
                if let lastFrameRate = self.dataHandler?.probePingDataSource.ping?.frameRate{
                    self.dataHandler?.probeEventDataSource.event?.frameRate = lastFrameRate
                    
                }else {
                    
                    self.dataHandler?.probeEventDataSource.event?.frameRate = 0
                }

            }else {
                if let lastFrameRate = self.dataHandler?.probePingDataSource.ping?.frameRate{
                    self.dataHandler?.probePingDataSource.ping?.frameRate = lastFrameRate

                }else {
                    
                    self.dataHandler?.probePingDataSource.ping?.frameRate = 0

                }
         
            }
        }
   
    }
    
    
    func setPlayBackPostion(isEvent: Bool) {
        
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.playbackPosInSEC = self.player?.playerCurrentTimeInSec
            self.dataHandler?.probeEventDataSource.event?.mitigationID = MitigationDefaults.getMitigationID()
        }else{
            self.dataHandler?.probePingDataSource.ping?.mitigationID = MitigationDefaults.getMitigationID()
            self.dataHandler?.probePingDataSource.ping?.playbackPosInSEC = self.player?.playerCurrentTimeInSec
        }
        
    }
    
    func setCodecType(isEvent: Bool) {
        let value = self.player?.tracks()
        if isEvent{
            if value?.1 != .none{
               self.dataHandler?.probeEventDataSource.event?.aCodec = value?.1
            }
            if value?.0 != .none{
               self.dataHandler?.probeEventDataSource.event?.vCodec = value?.0
            }
           
           
        }else{
            if value?.1 != .none{
                self.dataHandler?.probePingDataSource.ping?.aCodec = value?.1
            }
            if value?.0 != .none{
                self.dataHandler?.probePingDataSource.ping?.vCodec = value?.0
            }
        }
        
    }
    
    func setDRMType(isEvent: Bool){
//        let value = self.player?.currentItem?.asset.hasProtectedContent ?? false
//        if isEvent{
//            self.dataHandler?.probeEventDataSource.event?.drm = value ? "Fairplay" : "Fairplay"
//        }else{
//            self.dataHandler?.probePingDataSource.ping?.drm = value ? "Fairplay" : "Fairplay"
//        }
        
    }
    
    func setAssetDuration(isEvent: Bool){
        let value = self.player?.videoDuration
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.assetDuration = Int(value ?? 0)
        }else{
            self.dataHandler?.probePingDataSource.ping?.assetDuration = Int(value ?? 0)
        }
        
    }
    
    func setResolution(isEvent: Bool){
        let value = self.player?.currentResolution
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.resolution = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.resolution = value
        }
        
    }
    
    //Rahuld
    
    func setPlatForm(isEvent: Bool){
        let value =  dataHandler?.probePingDataSource.ping?.platform ?? "iOS"
        if isEvent{
            self.dataHandler?.probePingDataSource.ping?.platform = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.platform = value
        }
        
    }
    //Rahuld
    func setThroughPut(isEvent: Bool){
        let value = self.player?.throughPut
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.throughput = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.throughput = value
        }
    }
    
    func setBitrate(isEvent: Bool){
        let value = self.player?.currentBitRate
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.bitrate = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.bitrate = value
        }
    }
    
    func setLiveVideo(isEvent: Bool){
        let value = self.player?.isLiveVideoPlaying ?? false ? "true" : "false"
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.live = value
        }else{
            self.dataHandler?.probePingDataSource.ping?.live = value
        }
    }
    
    func setIP(isEvent: Bool){
        let value = UIDevice.getIP()
        sdkLogs("Probe IP \(String(describing: UIDevice.getIP()))")
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.ip = value?.MD5
        }else{
            self.dataHandler?.probePingDataSource.ping?.ip = value?.MD5
        }
    }
    
    func setUeid(isEvent:Bool){
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.ueid = MitigationDefaults.ueid.MD5
        }else{
            self.dataHandler?.probePingDataSource.ping?.ueid = MitigationDefaults.ueid.MD5
        }
    }
    
    func setDurationOfPlayBack(isEvent:Bool){
    
        if !isEvent{
            var value = self.totalDurationOfPlayback -  self.playBackTime
            if value < 0{
               value = 0
            }
            self.dataHandler?.probePingDataSource.ping?.durationOfPlayback = value
            if self.totalDurationOfPlayback > 0{
                self.playBackTime = self.totalDurationOfPlayback
            }
           
        }else {
            var value = self.totalDurationOfPlayback -  self.playBackTime
            if value < 0{
               value = 0
            }
            self.dataHandler?.probeEventDataSource.event?.durationOfPlayback = value
            if self.totalDurationOfPlayback > 0{
                self.playBackTime = self.totalDurationOfPlayback
            }
           
        }
        
    }
    
    func setPreviousEvent(isEvent: Bool){
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.eventPrev = self.lastProbeEvent.rawValue
        }
    }
    
    func setTotalDurationOfPlayback(isEvent:Bool){
        if !isEvent{
            if totalDurationOfPlayback > 0{
                self.dataHandler?.probePingDataSource.ping?.totalDurationOfPlayback = self.totalDurationOfPlayback
            }else{
                self.dataHandler?.probePingDataSource.ping?.totalDurationOfPlayback = 0
            }
            
        }

    }
    func setTotalStallDuration(isEvent:Bool){
//        if !isEvent{
//
//        }
    }
    func setTotalSwitchesUp(isEvent:Bool){
        if !isEvent{
            self.dataHandler?.probePingDataSource.ping?.totalSwitchesUp = self.totalSwitchesUP
        }
    }
    func setTotalSwitchesDown(isEvent:Bool){
        if !isEvent{
            self.dataHandler?.probePingDataSource.ping?.totalSwitchesDown = self.totalSwitchesDown
        }
    }
    
    func setLatency(isEvent:Bool){
        if isEvent{
            self.dataHandler?.probeEventDataSource.event?.eventData?.latency = self.latency
        }
    }
    
    func setStallCountAndDuration(isEvent:Bool){
        if !isEvent{
            let endTime = NSDate.currentEpochTime
            var diffTime = 0
            if let startTime = self.stallStartTime {
               diffTime =  NSDate.getDurationInSeconds(startTime:TimeInterval(startTime), endTime: TimeInterval(endTime))
                let value = self.currentTotalStallDuration + diffTime
               self.dataHandler?.probePingDataSource.ping?.stall?.duration = value
               if let _ = self.dataHandler?.probePingDataSource.ping?.totalStallDuration{
                self.dataHandler?.probePingDataSource.ping?.totalStallDuration += value
               }
              
            }else{
                self.dataHandler?.probePingDataSource.ping?.stall?.duration = self.currentTotalStallDuration
                self.dataHandler?.probePingDataSource.ping?.totalStallDuration += self.currentTotalStallDuration
            }
            self.dataHandler?.probePingDataSource.ping?.stall?.count = self.currentTotalStalls
        }else {
            let endTime = NSDate.currentEpochTime
            var diffTime = 0
            if let startTime = self.stallStartTime {
               diffTime =  NSDate.getDurationInSeconds(startTime:TimeInterval(startTime), endTime: TimeInterval(endTime))
                let value = self.currentTotalStallDuration + diffTime
               self.dataHandler?.probeEventDataSource.event?.stall?.duration = value
               if let _ = self.dataHandler?.probeEventDataSource.event?.totalStallDuration{
                self.dataHandler?.probeEventDataSource.event?.totalStallDuration += value
               }
              
            }else{
                self.dataHandler?.probeEventDataSource.event?.stall?.duration = self.currentTotalStallDuration
                self.dataHandler?.probeEventDataSource.event?.totalStallDuration += self.currentTotalStallDuration
            }
            self.dataHandler?.probeEventDataSource.event?.stall?.count = self.currentTotalStalls
        }
    }
    
    func setStartupAndRebufferDuration(isEvent:Bool){
        if !isEvent{
            self.dataHandler?.probePingDataSource.ping?.sbl =  MitigationDefaults.startUpBufferDuration
            self.dataHandler?.probePingDataSource.ping?.rbl = MitigationDefaults.reBufferDuration
        }
    }
    
    
    func cleanUpData(){
        self.currentTotalStallDuration = 0
        if self.currentBufferStatus == .playbackStalled{
            self.currentTotalStalls = 1
        }else
        {
            self.currentTotalStalls = 0
        }
    }
}


//
//  MitigationInterface.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 15/02/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation
import AVKit



class MitigationInterface{
    
    private weak var player:AVPlayer?
    private var playerItemBufferEmptyObserver: NSKeyValueObservation?
    private var playerItemBufferKeepUpObserver: NSKeyValueObservation?
    private var playerItemBufferFullObserver: NSKeyValueObservation?
    private var timer:Timer?
    private var mitigationParam:MitigationRegModel?
    private var lastBitrate:Int = 0
    
    init(player:AVPlayer?) {
        self.player = player
        if let _ = player{
            self.config()
            self.addBufferObserver()
        }
    }
    
    func register(ueid:String){
        let udid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        MitigationDefaults.ueid = ueid
        MitigationDefaults.udid = udid
        getPublicIPAddress(requestURL:URL(string: PublicIPAPIURLs.IPv4.ipify.rawValue)!) { [weak self] (ip, error) in
            if let ipAdr = ip{
                MitigationDefaults.ip = ipAdr
                let param =  MitigationRegModel(req: Req(ueid:ueid, uuid:udid, mitigationCFGID:MitigationDefaults.getMitigationID(), mitigationApplTime:MitigationDefaults.getMitigationAppTime(), clientIP:ipAdr, ua:"iOS User Agent"))
                //print("RahulTestParam",param)
                MitigationHandler().registerForMitigation(rawData:param, completionHandler:{ sucess in
                    if sucess{
                        testLogs("Registration for mitigation successful")
                        self?.setBitrate()
                    }else{
                        testLogs("Registration for mitigation unsuccessful")
                    }
                    
                })
            }
        }
    }
    
    func config(){
        self.setBitrate()
        self.setBufferuration(bufferType: .StartUpBuffer)
        self.timer = Timer.scheduledTimer(withTimeInterval:TimeInterval(MitigationDefaults.kcTimeInterval), repeats: true) {[weak self] _ in
            if !MitigationProbeSdkDefaults.isDisable_mitigation_poll {
                
                let param =  MitigationConfigModel(req: Req(ueid:MitigationDefaults.ueid.MD5, uuid: MitigationDefaults.udid, mitigationCFGID:MitigationDefaults.getMitigationID(), mitigationApplTime:MitigationDefaults.getMitigationAppTime(), clientIP: MitigationDefaults.ip, ua:"iOS User Agent"))
             
                MitigationHandler().getConfigForMitigation(rawData: param) { sucess in
                    self?.setBitrate()
                }

            }
        }
    }
    
    func addBufferObserver() {
        
        playerItemBufferEmptyObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackBufferEmpty, options: [.new]) { [weak self] (_, _) in
            sdkLogs("#Mitigation playerItemBufferEmptyObserver")
            self?.setBufferuration(bufferType: .ReBuffer)
        }
        
        playerItemBufferKeepUpObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackLikelyToKeepUp, options: [.new]) { (_, _) in
            sdkLogs("#Mitigation playerItemBufferKeepUpObserver")
        }
        
        playerItemBufferFullObserver = player?.currentItem?.observe(\AVPlayerItem.isPlaybackBufferFull, options: [.new]) { [weak self] (_, _) in
            sdkLogs("#Mitigation playerItemBufferFullObserver")
            self?.player?.play()
            if BufferType.currentBufferState == .ReBuffer{
               self?.setBufferuration(bufferType: .StartUpBuffer)
            }
        }
    }
  
    func setBitrate(){
        if !MitigationProbeSdkDefaults.isDisable_mitigation_poll {
            
            sdkLogs("#Mitigation Bitrate \(MitigationDefaults.videoBitrate)")
            if self.lastBitrate != MitigationDefaults.videoBitrate {
                self.player?.currentItem?.preferredPeakBitRate = Double(MitigationDefaults.videoBitrate)
                self.lastBitrate = MitigationDefaults.videoBitrate
            }
        }
    
    }
    
    func setBufferuration(bufferType:BufferType){
                
        if !MitigationProbeSdkDefaults.isDisable_mitigation_poll {
            
            var value = 0
            switch bufferType
            {
                   case .DefaultBuffer:
                    value = MitigationDefaults.defaultBufferDuartion
                   break
                   case .StartUpBuffer:
                    value = MitigationDefaults.startUpBufferDuration
                   break
                   case .ReBuffer:
                    value = MitigationDefaults.reBufferDuration
                   break
            }
            sdkLogs("#Mitigation setBufferuration \(value) for \(bufferType)")
            self.player?.automaticallyWaitsToMinimizeStalling = false
            self.player?.currentItem?.preferredForwardBufferDuration = TimeInterval(value)
            BufferType.currentBufferState = bufferType
            
        }

    }
    
    func stopApplyingMitigation(){
        self.timer?.invalidate()
        self.player = nil
        self.playerItemBufferEmptyObserver = nil
        self.playerItemBufferKeepUpObserver = nil
        self.playerItemBufferFullObserver = nil
    }
    
    deinit {
     
    }
    
}



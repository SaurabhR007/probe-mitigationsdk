//
//  Probe_Constants.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 28/09/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

let ProbePlayerPlaybackLikelyToKeepUp = "playbackLikelyToKeepUp"

enum ProbeKeyPaths:String {
    case videoQuality = "currentItem.presentationSize"
    case videoRate = "rate"
}

enum ProbeUserDefaults:String{
    case ueid = "ueid"
}

enum ProbeMetrices {
    case videoQuality
}

enum ProbePlayerStatus: Int {
    case playbackStalled
    case bufferBegin
    case bufferEnd
}

enum ProbeEventType: String {
    case STARTED = "STARTED"
    case STOPPED = "STOPPED"
    case SEEKED = "SEEKED"
    case BUFFERING = "BUFFERING"
    case PLAYCLICKED = "PLAYCLICKED"
    case PAUSED = "PAUSED"
    case RESUMED = "RESUMED"
    case ERROR = "ERROR"
    case COMPLETED = "COMPLETED"
}

public enum ProbeUserEvent: String {
    case STOPPED = "STOPPED"
    case SEEKED = "SEEKED"
    case PLAYCLICKED = "PLAYCLICKED"
    case ERROR = "ERROR"
}



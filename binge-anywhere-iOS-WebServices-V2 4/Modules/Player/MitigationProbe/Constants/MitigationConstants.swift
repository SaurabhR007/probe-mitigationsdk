//
//  MitigationDefaults.swift
//  BingeAnywhere
//
//  Created by Saurabh Rode on 17/02/22.
//  Copyright © 2022 ttn. All rights reserved.
//

import Foundation

enum BufferType{
    case DefaultBuffer
    case StartUpBuffer
    case ReBuffer
    static var currentBufferState:BufferType = .StartUpBuffer
}

class MitigationProbeSdkDefaults {
    
    static var isDisable_qoe_beacons: Bool = false
    static var isDisable_mitigation_poll: Bool = false

}

class MitigationDefaults {
    static var startUpBufferDuration:Int = 10
    static var reBufferDuration = 5
    static var defaultBufferDuartion = 10
    static var videoBitrate = 0
    static var kaTimeInterval = 30
    static var kcTimeInterval = 30
    static var bu =  "https://beacon.tskytech.com"
    //"https://beacon-qoe.tataplaybinge.com"
    //"http://qoe-beacon-86163666.ap-south-1.elb.amazonaws.com:8000"
    //"http://3.108.121.176:8000"
    //"https://beacon.tskytech.com/api/analysis"
    //"http://65.1.227.110:8080/api/analysis"//
    static var location = "Unknown"
    static var ip = ""
    static var ueid = ""
    static var udid = ""

    
    static func saveMitigationID(id:String?){
        if let id1 = id{
           UserDefaults.standard.setValue(id1, forKey:MitigationUserDefaults.MitigationID.rawValue)
        }
            
    }
    
    static func getMitigationID()->String?{
        return UserDefaults.standard.string(forKey:MitigationUserDefaults.MitigationID.rawValue)
        
    }
    
    static func saveMitigationAppTime(time:Int?){
        if let time1 = time{
            UserDefaults.standard.setValue(time1, forKey:MitigationUserDefaults.MitigationAppTime.rawValue)
        }
    
    }
    
    static func getMitigationAppTime()->Int?{
        return UserDefaults.standard.integer(forKey:MitigationUserDefaults.MitigationAppTime.rawValue)
    }
}

enum MitigationUserDefaults:String {
    case MitigationID = "mitigationID"
    case MitigationAppTime  = "mitigationAppTime"
}


//
//  TANetworkManager.swift
//  TANetworkingSwift

//  Created by Rahul Dubey on 03/01/22.
//  Copyright © 2022 Rahul Dubey. All rights reserved.
//


import UIKit


enum KHTTPMethod: String {
    case GET, POST, PUT, DELETE
}

class TANetworkManager {
    
    
     func requestApi<T: Decodable>(baseUrl:String,serviceName: String, method: KHTTPMethod, postData: [String:Any], skipBaseUrl: Bool = false,dataModelType:T.Type,completionClosure: @escaping (_ result: T?, _ error: Error? ,_ param:[String:Any]?) -> ()) -> Void {
        let serviceUrl = skipBaseUrl ? serviceName : (baseUrl + serviceName)
        
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["Content-Type": "application/json",
                                        "Accept": "application/json"]
        config.timeoutIntervalForRequest = 15
        config.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        
        let session = URLSession(configuration: config)
        
        let url = URL(string:serviceUrl)!
        var urlRequest = URLRequest(url: url)
        if method == .POST{
            urlRequest.httpMethod = "POST"
            guard let postData = try? JSONSerialization.data(withJSONObject: postData, options: []) else {
                return
            }
            urlRequest.httpBody = postData
        }
  
        let task = session.dataTask(with: urlRequest) { data, response, error in
            
            // ensure there is no error for this HTTP response
            guard error == nil else {
                sdkLogs("Payload Response :: Json Error: \(String(describing: error?.localizedDescription))")
                completionClosure(nil, error,postData)
                return
            }
            
            guard let content = data else {
                sdkLogs("Payload Response :: Success withiout Result")
                completionClosure(nil, nil,postData)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(T.self, from: content)
                sdkLogs("Payload Response :: Success with Result -> \(model)")
                completionClosure(model, nil,postData)

            } catch let error as NSError {
                sdkLogs("Payload Response :: Json Error: \(error.localizedDescription)")
                completionClosure(nil, error,postData)
            }
            
        }
        
        task.resume()
    }
    
}

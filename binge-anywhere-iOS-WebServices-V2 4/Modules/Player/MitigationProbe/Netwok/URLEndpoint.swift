//
//  URLEndpoint.swift

//
//  Created by Rahul Dubey on 03/01/22.
//  Copyright © 2022 Rahul Dubey. All rights reserved.
//


struct URLEndpoint {
    
    static let analysisURl = "/api/analysis"
    static let register_session = "/register-session"
    static let get_mitigation_config = "/get-mitigation-config"

}

//
//  NetworkUrl.swift
//  TANetworkingSwift
//
//  Created by Rahul Dubey on 03/01/22.
//  Copyright © 2022 Rahul Dubey. All rights reserved.
//

import Foundation
import UIKit

/** --------------------------------------------------------
* HTTP Basic Authentication
*	--------------------------------------------------------
*/

let kHTTPUsername           =   ""
let kHTTPPassword           =   ""
//let kDeviceName             = "iOS" //ios/ Andoid
let OS                      =   UIDevice.current.systemVersion
let kDeviceModel            =   UIDevice.current.model
let kAPIKEY                 =   ""
let kOk                =   "Ok"
let kClose             =   "Close"
let kGotIt             =   "Got It"
let kContentValue   = "application/json" //"application/x-www-form-urlencoded"let kToken          = "token"
let kLanguage       = "language"
let kBundleVersion  = "BundleVersion"
let kContentType    = "Content-Type"

/** --------------------------------------------------------
*		Techahead API Base URL defined by Targets.
*	--------------------------------------------------------
*/

// Change isProdBuild flag to switch production and development envirements
let isProdBuild = false
let kBaseUrlProbe           = MitigationDefaults.bu //"https://beacon-qoe.tataplaybinge.com"
let kBaseUrlMitigation    = "https://register.tskytech.com"

//"https://register-qoe.tataplaybinge.com"
//"http://qoe-fargate-nlb-86d2b60d0a7af686.elb.ap-south-1.amazonaws.com:8004"
//"https://register.tskytech.com"

let kAuthentication          =   "Authentication"   //Header key of request  encrypt data
let kEncryptionKey           =   ""      //Encryption key replace this with your projectname
let baseUrlForCurrencyImage =  ""
let kAppStorePurchaseUrl    =   "https://finance-app.itunes.apple.com/purchases"
let kGoogleStorePurchaseUrl    =   "https://finance-app.itunes.apple.com/purchases"


struct NetworkUrl {
    
    static func getURL() -> String {
        var url = ""
        
        if isProdBuild {
            url = "LIVE"
        }
        else {
            url = "UAT"
        }
        
        //print("----> URL -> \(url)")
        return url
    }
}

enum EnvirementType {
    case UAT
    case WWW
    case LCLSRV
}

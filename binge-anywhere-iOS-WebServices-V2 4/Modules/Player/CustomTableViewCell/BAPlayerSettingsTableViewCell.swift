//
//  BAPlayerSettingsTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 28/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAPlayerSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedIconImageView: UIImageView!
    @IBOutlet weak var settingsLabelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureVideoQualityCell(displayName: String?, selectedVideoQualityName: String?) {
        settingsLabelText.text = displayName
        if displayName == selectedVideoQualityName {
            self.backgroundColor = .BAmidBlue
            selectedIconImageView.isHidden = false
            settingsLabelText.textColor = .white
        } else {
            self.backgroundColor = .BAdarkBlueBackground
            selectedIconImageView.isHidden = true
            settingsLabelText.textColor = .lightGray
        }
    }

    func configureAudioTrackCells(displayName: String?, selectedAudioName: String?) {
        settingsLabelText.text = displayName
        if displayName == selectedAudioName {
            self.backgroundColor = .BAmidBlue
            selectedIconImageView.isHidden = false
            settingsLabelText.textColor = .white
        } else {
            self.backgroundColor = .BAdarkBlueBackground
            selectedIconImageView.isHidden = true
            settingsLabelText.textColor = .lightGray
        }
    }

    func configureSubtitleTrackCells(displayName: String?, selectedSubtitleName: String?) {
        settingsLabelText.text = displayName
        if displayName == selectedSubtitleName {
            self.backgroundColor = .BAmidBlue
            selectedIconImageView.isHidden = false
            settingsLabelText.textColor = .white
        } else {
            self.backgroundColor = .BAdarkBlueBackground
            selectedIconImageView.isHidden = true
            settingsLabelText.textColor = .lightGray
        }
    }
}

//
//  TVODExpiryModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 12/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit

struct TVODExpiryModal: Codable {
    let code: Int?
    let message: String?
    let data: ExpiryData?
    let localizedMessage: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ExpiryData.self, forKey: .data)
        localizedMessage = try values.decodeIfPresent(String.self, forKey: .localizedMessage)
    }
}

struct ExpiryData: Codable {
    let message: String?
    let purchaseExpiry: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        purchaseExpiry = try values.decodeIfPresent(Int.self, forKey: .purchaseExpiry)
    }
}


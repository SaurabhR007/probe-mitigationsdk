//
//  ShemarooContentModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

struct ShemarooContentModal: Codable {
    let hd_available: Bool?
    let playback_urls: [Playbackurls]?
    let adaptive_urls: [Adaptiveurls]?
    let sprite: Sprite?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        hd_available = try values.decodeIfPresent(Bool.self, forKey: .hd_available)
        playback_urls = try values.decodeIfPresent([Playbackurls].self, forKey: .playback_urls)
        adaptive_urls = try values.decodeIfPresent([Adaptiveurls].self, forKey: .adaptive_urls)
        sprite = try values.decodeIfPresent(Sprite.self, forKey: .sprite)
    }
}

struct Adaptiveurls: Codable {
    let playback_url: String?
    let profiles: [String]?
    let cdn: String?
    let label: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        playback_url = try values.decodeIfPresent(String.self, forKey: .playback_url)
        profiles = try values.decodeIfPresent([String].self, forKey: .profiles)
        cdn = try values.decodeIfPresent(String.self, forKey: .cdn)
        label = try values.decodeIfPresent(String.self, forKey: .label)
    }
}

struct Playbackurls: Codable {
    let sal_id: Int?
    let playback_url: String?
    let profile: String?
    let vwidth: Int?
    let vheight: Int?
    let vbitrate: Int?
    let abitrate: Int?
    let status: String?
    let display_name: String?
    let size: Int?
    let label: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sal_id = try values.decodeIfPresent(Int.self, forKey: .sal_id)
        playback_url = try values.decodeIfPresent(String.self, forKey: .playback_url)
        profile = try values.decodeIfPresent(String.self, forKey: .profile)
        vwidth = try values.decodeIfPresent(Int.self, forKey: .vwidth)
        vheight = try values.decodeIfPresent(Int.self, forKey: .vheight)
        vbitrate = try values.decodeIfPresent(Int.self, forKey: .vbitrate)
        abitrate = try values.decodeIfPresent(Int.self, forKey: .abitrate)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        display_name = try values.decodeIfPresent(String.self, forKey: .display_name)
        size = try values.decodeIfPresent(Int.self, forKey: .size)
        label = try values.decodeIfPresent(String.self, forKey: .label)
    }
}

struct Sprite: Codable {
    let sprite_url: String?
    let width: String?
    let height: String?
    let layout: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sprite_url = try values.decodeIfPresent(String.self, forKey: .sprite_url)
        width = try values.decodeIfPresent(String.self, forKey: .width)
        height = try values.decodeIfPresent(String.self, forKey: .height)
        layout = try values.decodeIfPresent(String.self, forKey: .layout)
    }
}

//
//  BAFullScreenMoviePlayerViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel

protocol BAFullScreenMoviePlayerViewControllerDelegate: class {
    func configureFavouriteButton(isAdddedToFav: Bool?)
    func isDismissed()
}

class BAFullScreenMoviePlayerViewController: UIViewController, LandscapeSupportable {

    // MARK: - Outlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var fullScreenPlayerView: UIView!

    // MARK: - Variables
    var player: BAPlayerFullScreenViewController!
    weak var delegate: BAFullScreenMoviePlayerViewControllerDelegate?
    var isFavourite: Bool?
    var isReplay: Bool?
    var isEpisode: Bool?
    var videoContentTitle: String?
    var cookieString: String?
    var coverImage: String?
    var contentId: String?
    var contentType: String?
    var continueWatchingVM: BAWatchingViewModal?
    var contentDetail: ContentDetailData?
    var episode: Episodes?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var parentNavigation: UINavigationController?
    var pinchGesture = UIPinchGestureRecognizer()

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        updatePlayerData()
        view.addSubview(player.view)
        player.delegate = self
        player.view.fillParentView()
        self.player.view.isUserInteractionEnabled = true
        self.player.view.isMultipleTouchEnabled = true
        self.pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchRecognized))
        self.player.view.addGestureRecognizer(self.pinchGesture)
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //fetchWatchlistLookUp()
    }

    override func viewWillDisappear(_ animated: Bool) {
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
        super.viewWillDisappear(animated)
        //if let del = delegate {
        //delegate?.isDismissed()
        //}
    }
    
    // MARK: - Functions
    func updatePlayerData() {
        player.isFav = isFavourite
        player.isEpisode = isEpisode
        player.cookieStr = cookieString
        player.contentId = contentId
        player.coverImahe = coverImage
        player.contentType = contentType
        player.videoTitle = videoContentTitle
        player.contentDetail = contentDetail
        player.isReplay = isReplay
        player.episode = episode
        self.navigationController?.navigationBar.isHidden = true
    }

    @objc func pinchRecognized(gesture: UIPinchGestureRecognizer) {
        if pinchGesture.state == .began {
            player.playerView.player.layer?.videoGravity = pinchGesture.scale < 1 ? .resizeAspect : .resizeAspectFill
            player.playerView.isZoomedIn = pinchGesture.scale > 1
            print(pinchGesture.scale < 1 ? "Pinch inward" : "Pinch outward")
        }
    }

    // MARK: - Landscape Delegates
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeRight
    }

    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    func fetchWatchlistLookUp() {
        var vodId = 0
        if contentDetail != nil && contentDetail?.lastWatch != nil {
            vodId = contentDetail?.lastWatch?.id ?? 0
        } else if contentDetail != nil {
            switch contentType {
            case ContentType.series.rawValue:
                vodId = contentDetail?.meta?.seriesId ?? 0
            case ContentType.brand.rawValue:
                vodId = contentDetail?.meta?.brandId ?? 0
            default:
                vodId = contentDetail?.meta?.vodId ?? 0
            }
        } else {
            vodId = episode?.episodeId ?? 0
        }
        
        //fullscreenVodId = String(vodId)
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType ?? "", contentId: String(vodId))
        confgureWatchlistIcon()
    }

    func confgureWatchlistIcon() {
        if let dataModel = watchlistLookupVM {
            dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                if flagValue {
                    if dataModel.watchlistLookUp?.data?.isFavourite ?? false {
                        self.isFavourite = true
                    } else {
                        self.isFavourite = false
                    }
                } else if isApiError {
                    //self.apiError(message, title: kSomethingWentWrong)
                } else {
                    //self.apiError(message, title: "")
                }
                self.updatePlayerData()
            }
        }
    }
}

// MARK: - Delegate
extension BAFullScreenMoviePlayerViewController: BAPlayerFullScreenViewControllerDelegate {
    func popToParentView() {
//        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
//        UIViewController.attemptRotationToDeviceOrientation()
        delegate?.isDismissed()
        self.dismiss(animated: false, completion: nil)
    }

    func makeContinueWatchCall(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?) {
        continueWatchingVM = BAWatchingViewModal(repo: BAWatchingRepo())
        continueWatchingVM?.apiParams["id"] = contentId
        continueWatchingVM?.apiParams["contentType"] = contentType
        continueWatchingVM?.apiParams["watchDuration"] = (watchedDuration ?? 0)
        continueWatchingVM?.apiParams["totalDuration"] = totalDuration ?? 0
        continueWatchingVM?.getWatchingtData({ flagValue, message, isApiError  in
            if flagValue {
                
            } else {
                if message == kSessionExpire {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sessionExpireAlert"), object: nil)
                }
            }
        })
    }

    func configureFavButton(isFav: Bool?) {
        delegate?.configureFavouriteButton(isAdddedToFav: isFav)
    }

    func addRemoveFromWatchlistFullScreenController() {
        //delegate?.addRemoveFromWatchlistFullScreen()
        // TODO: Make API Call to add to watchlistBAPlayerFullScreenViewControllerDelegate
    }

}

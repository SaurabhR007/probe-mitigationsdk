//
//  BAFullScreenPlayerViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 15/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BAFullScreenPlayerViewControllerDelegate: class {
    func fullScreenPlayerViewControllerDidDismiss(_ controller: BAFullScreenPlayerViewController, _ isAutoRotated: Bool)
    func fullScreenPlayerDidChangePlayingState(_ isPlayerPlaying: Bool)
    //func configureFavButtonFromTrailer(isAddedToFav: Bool?)
}

class BAFullScreenPlayerViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet weak var fullScreenPlayerView: UIView!

    // MARK: - Variable
    var player: BAPlayerViewController!
    var pinchGesture = UIPinchGestureRecognizer()
    weak var delegate: BAFullScreenPlayerViewControllerDelegate?
    var contentDetail: ContentDetailData?
    var isAddedToWatchlist: Bool?
    var isFirstRotation = false
    var isAutoRotated = true

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.orientation.isLandscape {
            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
            } else {
                UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
            }
            print("Landscape")
        } else {
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
            isFirstRotation = true
        }
        UIViewController.attemptRotationToDeviceOrientation()
        self.player.view.isUserInteractionEnabled = true
        self.player.view.isMultipleTouchEnabled = true
        self.pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchRecognized))
        self.player.view.addGestureRecognizer(self.pinchGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.player.isAddedToWatch = self.isAddedToWatchlist
        self.player.contentDetail = self.contentDetail
        configurePlayer()
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        print("orientation change 1")
        if !isFirstRotation && isAutoRotated {
            screenRotated()
        } else {
            isFirstRotation = false
        }
    }

    // MARK: - Function
    private func configurePlayer() {
        self.view.addSubview(player.view)
        player.view.fillParentView()
        player.delegate = self
    }

    @objc func pinchRecognized(gesture: UIPinchGestureRecognizer) {
        if pinchGesture.state == .began && UIDevice.current.orientation.isLandscape {
            if player.isHungamaContent {
                player.hungamaPlayer.playerView.videoGravity = pinchGesture.scale < 1 ? .resizeAspect : .resizeAspectFill
            } else {
                player.playerView.player.layer?.videoGravity = pinchGesture.scale < 1 ? .resizeAspect : .resizeAspectFill
            }
            print(pinchGesture.scale < 1 ? "Pinch inward" : "Pinch outward")
        }
    }

    @objc func screenRotated() {
        if UIDevice.current.orientation.isLandscape {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeSmallScreenIcon"), object: nil)
            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
            } else {
                UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
            }
            print("Landscape")
        } else if UIDevice.current.orientation.isPortrait {
            print(" Is In Potrait Mode -------")
            self.delegate?.fullScreenPlayerViewControllerDidDismiss(self, true)
            dismiss(animated: true, completion: nil)
            if player.isHungamaContent {
                player.hungamaPlayer.playerView.videoGravity = .resizeAspectFill
            } else {
                player.playerView.player.layer?.videoGravity = .resizeAspectFill
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeFullScreenIcon"), object: nil)
        }
    }
}

// MARK: - Extension
extension BAFullScreenPlayerViewController: BAPlayerViewControllerDelegate {
    func videoDidBeginAfterReplay() {
        // Do Nothing
    }
    
    func configureContentDetailButtonOnUpNext() {
        // Do Nothing
    }
    
    func configureFavButtonForTrailer(isAddedToFavorite: Bool?) {
        // DO Nothing
    }
    
    func videoEndNotification() {
        // Do Nothing
//        dismiss(animated: false, completion: {
//            self.delegate?.fullScreenPlayerViewControllerDidDismiss(self)
//        })
    }

    func playerViewControllerWillChangeLayout(isButtonTapped: Bool) {
        print("In Full Screen Player View")
        print("Is button Tapped", isButtonTapped)
        isAutoRotated = isButtonTapped
        self.delegate?.fullScreenPlayerViewControllerDidDismiss(self, false)
        dismiss(animated: false, completion: nil)
    }

    func didChangePlayerPlayingState(_ isPlayerPlaying: Bool) {
        delegate?.fullScreenPlayerDidChangePlayingState(isPlayerPlaying)
    }
}

extension UINavigationController {
    open override var childForHomeIndicatorAutoHidden: UIViewController? {
        return topViewController
    }
}

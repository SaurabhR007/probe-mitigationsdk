//
//  BAPlayerFullScreenViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 27/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BAPlayerFullScreenViewControllerDelegate: class {
    func configureFavButton(isFav: Bool?)
    func popToParentView()
    func addRemoveFromWatchlistFullScreenController()
    func makeContinueWatchCall(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?)
}

class BAPlayerFullScreenViewController: UIViewController {

    private let url: URL
    private let cookie: String
    var videoContentType: String?
//    private let contentDetails: ContentDetailData
//    private let episodeDetails: Episodes
    weak var delegate: BAPlayerFullScreenViewControllerDelegate?
    var shouldHideHomeIndicator = false
    var isFav: Bool?
    var coverImahe: String?
    var isEpisode: Bool?
    var videoContentId: Int?
    var isReplay: Bool?
    var videoTitle: String?
    var contentId: String?
    var contentType: String?
    var cookieStr: String?
    var contentDetail: ContentDetailData?
    var episode: Episodes?
    var nextEpisodeData: BANextPreviousEpisodeModel?

    lazy var playerView: BAFullScreenMoviePlayerView = {
        let view = BAFullScreenMoviePlayerView(frame: .zero, url: url, cookie: cookie, content: contentDetail, episodeData: episode, isReplayContent: isReplay, videoContentType: videoContentType, contentId: videoContentId)
        view.delegate = self
        return view
    }()

    // MARK: - LifeCycle
    override func loadView() {
        playerView.contentDetail = self.contentDetail
        view = playerView
        updatePlayerData()
    }

    init(_ url: URL, cookieString: String, contentData: ContentDetailData?, episodeDetail: Episodes?, isReplayContent: Bool?, videoType: String?, contentId: Int?) {
        self.url = url
        self.cookie = cookieString
        self.contentDetail = contentData
        self.episode = episodeDetail
        self.isReplay = isReplayContent
        self.videoContentType = videoType
        self.videoContentId = contentId
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        playerView.fillParentView()
        // Do any additional setup after loading the view.
    }

    func prefersHomeIndicatorAutoHidden() -> Bool {
        return shouldHideHomeIndicator
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updatePlayerData()
    }
    
    func updatePlayerData() {
        playerView.isAddedToFavourite = isFav
        playerView.cookieString = cookieStr ?? ""
        playerView.contentId = contentId
        playerView.contentType = contentType
        playerView.isEpisode = isEpisode
        playerView.coverImage = coverImahe
        playerView.playerVideoTitle = videoTitle
        playerView.contentDetail = contentDetail
        playerView.episode = episode
        playerView.isReplay = isReplay
        if isEpisode ?? false {
            playerView.playerTitle.text = episode?.title
        } else {
            playerView.playerTitle.text = videoTitle
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseVideoPlayer), name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        self.shouldHideHomeIndicator = true
        if #available(iOS 11.0, *) {
            self.setNeedsUpdateOfHomeIndicatorAutoHidden()
        } else {
            // Fallback on earlier versions
        }
    }

    @objc func pauseVideoPlayer() {
        pauseVideo()
    }

    func pauseVideo() {
        playerView.player.pause()
        playerView.player.isMuted = true
        playerView.playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func viewWillDisappear(_ animated: Bool) {
        playerView.player.pause()
        //playerView.playPauseButton.setImage(UIImage(named: "pauseicon"), for: .normal)
        super.viewWillDisappear(animated)
    }
}

extension BAPlayerFullScreenViewController: BAFullScreenMoviePlayerViewDelegate {
    func popToParent() {
        delegate?.popToParentView()
    }
    
    func makeContinueWatchCall(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?) {
        delegate?.makeContinueWatchCall(contentId: contentId, contentType: contentType, watchedDuration: watchedDuration, totalDuration: totalDuration)
    }

    func configureFavButton(isAddedToFav: Bool?) {
        delegate?.configureFavButton(isFav: isAddedToFav)
    }

    func addRemoveToWatchlistFromFullScreen() {
        delegate?.addRemoveFromWatchlistFullScreenController()
    }
}

extension UINavigationController {
    func childViewControllerForHomeIndicatorAutoHidden() -> UIViewController? {
        return topViewController
    }
}

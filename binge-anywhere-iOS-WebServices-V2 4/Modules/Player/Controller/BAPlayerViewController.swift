//
//  BAPlayerViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 14/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import HungamaPlayer
import ErosNow_iOS_SDK

protocol BAPlayerViewControllerDelegate: class {
    func playerViewControllerWillChangeLayout(isButtonTapped: Bool)
    func didChangePlayerPlayingState(_ isPlayerPlaying: Bool)
    func videoEndNotification()
    func configureContentDetailButtonOnUpNext()
    func videoDidBeginAfterReplay()
    func configureFavButtonForTrailer(isAddedToFavorite: Bool?)
}

class BAPlayerViewController: UIViewController {

    // MARK: - Variables
    private let url: URL
    private let cookie: String
    var videoContentType: String?
    var contentType: String?
    var isPlayerPlaying = false
    var contentDetail: ContentDetailData?
    var continueWatchingVM: BAWatchingViewModal?
    var episode: Episodes?
    var isAddedToWatch: Bool?
    var isEpisode: Bool?
    var videoContentId: Int?
    var isReplay: Bool?
    var isTrailerPlaying: Bool = true
    var hungamaContent: IContentVO?
    var isHungamaContent = false
    var erosNowContent: ENContentProfile?
    
    weak var delegate: BAPlayerViewControllerDelegate?
    
    lazy var playerView: BAPlayerView = {
        let view = BAPlayerView(frame: .zero, url: url, cookie: cookie, content: contentDetail, episodeData: episode, isReplayContent: isReplay, videoContentType: videoContentType, contentId: videoContentId, isTrailerPlaying: isTrailerPlaying, isEpisodePlaying: isEpisode ?? false, isAddedToFav: isAddedToWatch ?? false, erosNowContent: erosNowContent)
        view.delegate = self
        return view
    }()
    
    
    lazy var hungamaPlayer: BAHungamaPlayerView = {
        var testFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 200)
        testFrame = .zero
        let view = BAHungamaPlayerView(frame: testFrame, url: url, cookie: cookie, content: contentDetail, episodeData: episode, isReplayContent: isReplay, videoContentType: videoContentType, contentId: videoContentId, isTrailerPlaying: isTrailerPlaying, isEpisodePlaying: isEpisode ?? false)
        view.content = self.hungamaContent
        self.isHungamaContent = !(hungamaContent != nil)
        view.delegate = self
        return view
    }()

    // MARK: - LifeCycle
    override func loadView() {
        self.isHungamaContent = (hungamaContent != nil)
        view = isHungamaContent ? hungamaPlayer : playerView
    }

    init(_ url: URL, cookieString: String, contentData: ContentDetailData?, episodeDetail: Episodes?, isReplayContent: Bool?, videoType: String?, contentId: Int?, isTrailerPlaying: Bool, hungamaContent: IContentVO?, isEpisodePlay: Bool?, isAddedToFav: Bool?, erosNowContent: ENContentProfile?) {
        self.url = url
        self.cookie = cookieString
        self.contentDetail = contentData
        self.episode = episodeDetail
        self.isReplay = isReplayContent
        self.videoContentType = videoType
        self.videoContentId = contentId
        self.isEpisode = isEpisodePlay
        self.isTrailerPlaying = isTrailerPlaying
        self.hungamaContent = hungamaContent
        self.isAddedToWatch = isAddedToFav
        self.erosNowContent = erosNowContent
        super.init(nibName: nil, bundle: nil)
    }
     
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isHungamaContent = (hungamaContent != nil)
        isHungamaContent ? hungamaPlayer.fillParentView() : playerView.fillParentView()
        if isHungamaContent {
            hungamaPlayer.testPlay()
        }
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isHungamaContent {
            hungamaPlayer.isPlayerPlaying = isPlayerPlaying
            hungamaPlayer.checkForVideogravity()
            hungamaPlayer.contentDetail = contentDetail
        } else {
            playerView.isPlayerPlaying = isPlayerPlaying
            self.playerView.checkForVideogravity()
            self.playerView.contentDetail = self.contentDetail
        }
        NotificationCenter.default.addObserver(self, selector: #selector(pauseVideoPlayer), name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func pauseVideoPlayer() {
       pauseVideo()
    }

    override func viewWillDisappear(_ animated: Bool) {
        if isHungamaContent {
            hungamaPlayer.stopPlayback()
            hungamaPlayer.cancelContinueTimer()
            hungamaPlayer.removeScreenRecordingObserver()
            if hungamaPlayer.isVideoEnded {
                hungamaPlayer.playButton.setImage(UIImage(named: "replay"), for: .normal)
            } else {
                hungamaPlayer.playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        } else {
            playerView.player.pause()
            playerView.cancelContinueTimer()
            playerView.removeScreenRecordingObserver()
            if playerView.isVideoEnded {
                playerView.playButton.setImage(UIImage(named: "replay"), for: .normal)
            } else {
                playerView.playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
        super.viewWillDisappear(animated)
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }

    func pauseVideo() {
        if isHungamaContent {
            hungamaPlayer.togglePlayPauseHungamaPlayer(false)
            hungamaPlayer.cancelTimer()
            hungamaPlayer.cancelContinueTimer()
            isPlayerPlaying = false
            hungamaPlayer.isPlayerPlaying = isPlayerPlaying
            if hungamaPlayer.isVideoEnded {
                hungamaPlayer.playButton.setImage(UIImage(named: "replay"), for: .normal)
            } else {
                hungamaPlayer.playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        } else {
            playerView.player.pause()
           // playerView.player.isMuted = true
            playerView.cancelTimer()
            playerView.cancelContinueTimer()
            isPlayerPlaying = false
            playerView.isPlayerPlaying = isPlayerPlaying
            if playerView.isVideoEnded {
                playerView.playButton.setImage(UIImage(named: "replay"), for: .normal)
            } else {
                playerView.playButton.setImage(UIImage(named: "pauseicon"), for: .normal)
            }
        }
    }
}

// MARK: - Extension
extension BAPlayerViewController: BAPlayerViewDelegate {
    func configurePlayButtonOnPI() {
        delegate?.configureContentDetailButtonOnUpNext()
    }
    
    func addRemoveToWatchlistFromFullScreen() {
        // TODO
    }
    
    func videoBeganInReplay() {
        delegate?.videoDidBeginAfterReplay()
    }
    
    func makeContinueWatchCallForContent(contentId: Int?, contentType: String?, watchedDuration: Int?, totalDuration: Int?) {
        continueWatchingVM = BAWatchingViewModal(repo: BAWatchingRepo())
        continueWatchingVM?.apiParams["id"] = contentId
        continueWatchingVM?.apiParams["contentType"] = contentType
        continueWatchingVM?.apiParams["watchDuration"] = (watchedDuration ?? 0)
        continueWatchingVM?.apiParams["totalDuration"] = totalDuration ?? 0
        continueWatchingVM?.getWatchingtData({ flagValue, message, isApiError  in
            if flagValue {
                
            } else {
                if message == kSessionExpire {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sessionExpireAlert"), object: nil)
                }
            }
        })
    }
    
    func configureFavButton(isAddedToFav: Bool?) {
        delegate?.configureFavButtonForTrailer(isAddedToFavorite: isAddedToFav)
    }
    
    func videoDidFinish() {
        delegate?.videoEndNotification()
    }

    func moveToFullScreen(isFullScreen: Bool) {
        //if playerView.removeDeviceView?.isHidden == true {
        print("Move to full screen method")
            delegate?.playerViewControllerWillChangeLayout(isButtonTapped: true)
//        } else {
//            return
//        }
        
    }

    func didChangePlayerPlayingState(_ isPlayerPlaying: Bool) {
        delegate?.didChangePlayerPlayingState(isPlayerPlaying)
    }
}

//
//  ShemarooContentRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol ShemarooContentScreenRepo {
    var apiEndPoint: String {get set}
    func getPlayableContent(completion: @escaping(APIServicResult<ShemarooContentModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct ShemarooContentRepo: ShemarooContentScreenRepo {
    var apiEndPoint: String = ""
    let apiParams = APIParams()
    func getPlayableContent(completion: @escaping(APIServicResult<ShemarooContentModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .getPlayableContent(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<ShemarooContentModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

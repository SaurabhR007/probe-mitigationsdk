//
//  TVODExpiryViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 12/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol TVODExpiryViewModalProtocol {
    func getExpiryDate(completion: @escaping(Bool, String?, Bool) -> Void)
}

class TVODExpiryViewModal: TVODExpiryViewModalProtocol {
    var repo: TVODExpiryScreenRepo
    var requestToken: ServiceCancellable?
    var endPoint: String = ""
    var expiryModal: TVODExpiryModal?
    var apiParams = APIParams()

    init(repo: TVODExpiryScreenRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }

    func getExpiryDate(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo.getExpiryDate(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                self.expiryModal = _configData.parsed
                completion(true, nil, false)
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

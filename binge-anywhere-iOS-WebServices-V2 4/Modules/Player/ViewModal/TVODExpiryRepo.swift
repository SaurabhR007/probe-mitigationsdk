//
//  TVODExpiryRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 12/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol TVODExpiryScreenRepo {
    var apiEndPoint: String {get set}
    func getExpiryDate(completion: @escaping(APIServicResult<TVODExpiryModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct TVODExpiryRepo: TVODExpiryScreenRepo {
    var apiEndPoint: String = ""
    let accessToken =  "bearer \(BAKeychainManager().userAccessToken)"
    let SID = BAKeychainManager().sId
    let PID = BAKeychainManager().profileId
    let apiParams = APIParams()
    let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "bearer \(BAKeychainManager().userAccessToken)", "subscriberid": "\(BAKeychainManager().sId)"]
    func getExpiryDate(completion: @escaping(APIServicResult<TVODExpiryModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .tvodExpiry(param: apiParams, headers: apiHeader, endUrlString: apiEndPoint))
        return APIService().request(target) { (response: APIResult<APIServicResult<TVODExpiryModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

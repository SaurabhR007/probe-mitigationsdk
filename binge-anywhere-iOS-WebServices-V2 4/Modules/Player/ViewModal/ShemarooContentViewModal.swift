//
//  ShemarooContentViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 23/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol ShemarooContentViewModalProtocol {
    func getPlayableContent(completion: @escaping(Bool, String?, Bool) -> Void)
}

class ShemarooContentViewModal: ShemarooContentViewModalProtocol {
    var repo: ShemarooContentScreenRepo
    var requestToken: ServiceCancellable?
    var endPoint: String = ""
    var shemarooContent: ShemarooContentModal?
    var apiParams = APIParams()

    init(repo: ShemarooContentScreenRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }

    func getPlayableContent(completion: @escaping(Bool, String?, Bool) -> Void) {
        requestToken = repo.getPlayableContent(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                self.shemarooContent = _configData.parsed
                completion(true, nil, false)
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

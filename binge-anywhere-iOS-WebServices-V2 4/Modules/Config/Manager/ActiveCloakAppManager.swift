//
//  ActiveCloakAppManager.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/01/21.
//  Copyright © 2021 TTN. All rights reserved.
//

import Foundation
import UIKit


// Copied error codes from Android, to keep the mixpanel in sync
enum ITAC_Code : Int{
    case ITAC_S_NONE = 0
    case ITAC_S_ROOT_DETECTED = 1
    case ITAC_S_IV_CHECK_FAILED = 16
    case ITAC_S_HOOK_DETECTED = 4096
    
    //Custom
    case ITAC_S_INIT_FAILED = 5005
    case ITAC_S_DEFAULT_FAILED = 5006
}


class ActiveCloakAppManager {
    
    static let sharedInstance = ActiveCloakAppManager()
    
   // static var iacHandle: ITAC_Agent_Handle?
    
    private init () {
        
//        let acvFilepath = Bundle.main.url(forResource: "data/ACV", withExtension: "dat")
//        let voucherFilePath = acvFilepath!.path
//        
//        let fileManager = FileManager.default
//        if fileManager.fileExists(atPath: voucherFilePath) {
//            debugPrint("acv.dat exist")
//        } else {
//            debugPrint("acv.dat does not exist")
//        }
//        
//        let voucherFilePathCString = (voucherFilePath as NSString).utf8String!
//        let voucherFilePathCPointer = UnsafeMutablePointer<Int8>(mutating:voucherFilePathCString)
//        let _ = String(cString: voucherFilePathCPointer)
//        
//        var status: ITAC_Status = ITAC_Status(isJailbroken: 0, isIVCheckFailed: 0, isHookDetected: 0)
//        var options: ITAC_InitOptions = ITAC_InitOptions(voucherFilePath:voucherFilePathCPointer, pAndroidEnv:nil)
//        
//        let result:uint = ITAC_Agent_Init(&ActiveCloakAppManager.iacHandle, &options, &status)
//        debugPrint("Result of ITAC_Agent_Init ::>>>>\(result)")
    }
    
    func setup() {
//        var acaVersion: UnsafeMutablePointer<CChar>? = nil ;
//        acaVersion = ITAC_Agent_GetVersionString()
//        let versionString:String = String(cString:acaVersion!)
//        debugPrint("ITAC_Agent_GetVersionString :::>>>  \(versionString)")
    }
    
    deinit {
//        let result:uint = ITAC_Agent_Close(ActiveCloakAppManager.iacHandle)
//        debugPrint("ITAC_Agent_Close \(result)")
    }
    
    func getUniqueDeviceID() -> String {
        if UIDevice.current.isSimulator {
            return String.random()
        }else {
            var pDeviceId: UnsafeMutablePointer<CChar>? = nil ;
          //  ITAC_Agent_GetDeviceID(ActiveCloakAppManager.iacHandle, &pDeviceId);
            let deviceId:String = String(cString:pDeviceId!)
            return deviceId
        }
    }
    
    
//    func statusValidation(status:ITAC_Status) -> (Bool, String){
//        var message = ""
//        // Its not the requirement of the ACA
//        // Since ACA is not supported on Simulator, we consider success
//        if UIDevice.current.isSimulator {
//            return (true, "")
//        }
//        else if ActiveCloakAppManager.iacHandle == nil {
//            NSLog(" ACA initialization Failed ")
//            message = "We have encountered some technical issues. Please contact customer care"
//            return (false, message)
//        } else if status.isJailbroken == 0 &&
//                    status.isIVCheckFailed == 0 &&
//                    status.isHookDetected == 0 {
//            return (true, "")
//        } else if status.isJailbroken != 0 {
//            debugPrint("🔥🔥 Jailbreak Device Detected 🔥🔥")
//            message = "We have detected a jailbroken device. Please switch to a non-jailbroken device to use this application."
//            return (false, message)
//        } else if status.isHookDetected != 0 || status.isIVCheckFailed != 0 {
//            debugPrint("🧷🧷 Hook/IVCheckFailed Device Detected 🧷🧷")
//            message = "We have detected security issues on this device. Switch to a secure device to use this application. Please contact customer care for more."
//            return (false, message)
//        }
//        return (false, "We have encountered some technical issues.Please contact customer care")
//    }
}

extension String {
    
    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}

//
//  ConfigDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 08/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol ConfigDataRepo {
    func getConfigData(apiParams: APIParams, completion: @escaping(APIServicResult<BAConfigModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct ConfigData: ConfigDataRepo {

    func getConfigData(apiParams: APIParams, completion: @escaping (APIServicResult<BAConfigModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let target = ServiceRequestDetail.init(endpoint: .configData(param: apiParams, headers: nil))
        return APIService().request(target) { (response: APIResult<APIServicResult<BAConfigModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BAConfigModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 15/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAConfigModal: Codable {
    let code: Int?
    let message: String?
    let data: BAConfigData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(BAConfigData.self, forKey: .data)
    }

}

struct BAConfigData: Codable {
    let app: App?
    let config: Config?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        app = try values.decodeIfPresent(App.self, forKey: .app)
        config = try values.decodeIfPresent(Config.self, forKey: .config)
    }

}

struct Config: Codable {
    let taHeroBanner: [TaHeroBanner]?
    let tvodDetails: TVODDetails?
    let profile: Profile?
    let rateLimit: String?
    let cw_interval: Int?
    let bitrate: Bitrate?
    let otpResentCount: Int?
    let url: Url?
    let taLearnAction: [TaLearnAction]?
    let taDetails: TADetails?
    let subscriberImage: Subscriberimage?
    let serverTime: Int?
    let providerLogo: ProviderLogo?
    let taRelatedRail: [TaRelatedRail]?
    let cwPosition: Int?
    let otpDuration: Int?
    let passwordRedirectionTimeInSecs: Int?
    let loaderDelayTime: Int?
    let noActivePrimeIOSMessage: String?
    let unSubscribedTitle: String?
    let subscribedTitle: String?
    
    let showMarketingScreen: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        taHeroBanner = try values.decodeIfPresent([TaHeroBanner].self, forKey: .taHeroBanner)
        tvodDetails = try values.decodeIfPresent(TVODDetails.self, forKey: .tvodDetails)
        profile = try values.decodeIfPresent(Profile.self, forKey: .profile)
        cw_interval = try values.decodeIfPresent(Int.self, forKey: .cw_interval)
        bitrate = try values.decodeIfPresent(Bitrate.self, forKey: .bitrate)
        otpResentCount = try values.decodeIfPresent(Int.self, forKey: .otpResentCount)
        url = try values.decodeIfPresent(Url.self, forKey: .url)
        taLearnAction = try values.decodeIfPresent([TaLearnAction].self, forKey: .taLearnAction)
        taDetails = try values.decodeIfPresent(TADetails.self, forKey: .taDetails)
        subscriberImage = try values.decodeIfPresent(Subscriberimage.self, forKey: .subscriberImage)
        serverTime = try values.decodeIfPresent(Int.self, forKey: .serverTime)
        providerLogo = try values.decodeIfPresent(ProviderLogo.self, forKey: .providerLogo)
        taRelatedRail = try values.decodeIfPresent([TaRelatedRail].self, forKey: .taRelatedRail)
        cwPosition = try values.decodeIfPresent(Int.self, forKey: .cwPosition)
        otpDuration = try values.decodeIfPresent(Int.self, forKey: .otpDuration)
        passwordRedirectionTimeInSecs = try values.decodeIfPresent(Int.self, forKey: .passwordRedirectionTimeInSecs)
        loaderDelayTime = try values.decodeIfPresent(Int.self, forKey: .loaderDelayTime)
        rateLimit = try values.decodeIfPresent(String.self, forKey: .rateLimit)
        unSubscribedTitle = try values.decodeIfPresent(String.self, forKey: .unSubscribedTitle)
        subscribedTitle = try values.decodeIfPresent(String.self, forKey: .subscribedTitle)
        noActivePrimeIOSMessage = try values.decodeIfPresent(String.self, forKey: .noActivePrimeIOSMessage)
        showMarketingScreen = try values.decodeIfPresent(Bool.self, forKey: .showMarketingScreen)
    }
}

struct Subscriberimage: Codable {
    let subscriberImageBaseUrl: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subscriberImageBaseUrl = try values.decodeIfPresent(String.self, forKey: .subscriberImageBaseUrl)
    }

}

struct TADetails: Codable {
    let subsciberId: String?
    let profileId: String?
    let token: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subsciberId = try values.decodeIfPresent(String.self, forKey: .subsciberId)
        profileId = try values.decodeIfPresent(String.self, forKey: .profileId)
        token = try values.decodeIfPresent(String.self, forKey: .token)
    }
}

struct TVODDetails: Codable {
    let subsciberId: String?
    let token: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subsciberId = try values.decodeIfPresent(String.self, forKey: .subsciberId)
        token = try values.decodeIfPresent(String.self, forKey: .token)
    }
}

struct ProviderLogo: Codable {
    let SONYLIV: SONYLIVEData?
    let ZEE5: ZEE5Data?
    let PRIME: PRIMEData?
    let SHEMAROOME: SHEMAROOMEData?
    let HUNGAMA: HUNGAMAData?
    let NETFLIX: NETFLIXData?
    let HOTSTAR: HOTSTARData?
    let SUNNXT: SUNNXTData?
    let EROSNOW: EROSNOWData?
    let VOOTKIDS: VOOTKIDSData?
    let VOOTSELECT: VOOTSELECTData?
    let CURIOSITYSTREAM: CURIOSITYSTREAMData?
    let TATASKY: TATASKYData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        SONYLIV = try values.decodeIfPresent(SONYLIVEData.self, forKey: .SONYLIV)
        ZEE5 = try values.decodeIfPresent(ZEE5Data.self, forKey: .ZEE5)
        PRIME = try values.decodeIfPresent(PRIMEData.self, forKey: .PRIME)
        SHEMAROOME = try values.decodeIfPresent(SHEMAROOMEData.self, forKey: .SHEMAROOME)
        HUNGAMA = try values.decodeIfPresent(HUNGAMAData.self, forKey: .HUNGAMA)
        NETFLIX = try values.decodeIfPresent(NETFLIXData.self, forKey: .NETFLIX)
        HOTSTAR = try values.decodeIfPresent(HOTSTARData.self, forKey: .HOTSTAR)
        SUNNXT = try values.decodeIfPresent(SUNNXTData.self, forKey: .SUNNXT)
        EROSNOW = try values.decodeIfPresent(EROSNOWData.self, forKey: .EROSNOW)
        VOOTKIDS = try values.decodeIfPresent(VOOTKIDSData.self, forKey: .VOOTKIDS)
        VOOTSELECT = try values.decodeIfPresent(VOOTSELECTData.self, forKey: .VOOTSELECT)
        CURIOSITYSTREAM = try values.decodeIfPresent(CURIOSITYSTREAMData.self, forKey: .CURIOSITYSTREAM)
        TATASKY = try values.decodeIfPresent(TATASKYData.self, forKey: .TATASKY)
    }
}

struct EROSNOWData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct TATASKYData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct CURIOSITYSTREAMData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct VOOTKIDSData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct VOOTSELECTData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct HOTSTARData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct HUNGAMAData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct NETFLIXData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct SHEMAROOMEData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct SUNNXTData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct SONYLIVEData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct ZEE5Data: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}

struct PRIMEData: Codable {
    let unsubscribedMob: String?
    let unsubscribedWeb: String?
    let logoRectangular: String?
    let logoCircular: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unsubscribedMob = try values.decodeIfPresent(String.self, forKey: .unsubscribedMob)
        unsubscribedWeb = try values.decodeIfPresent(String.self, forKey: .unsubscribedWeb)
        logoRectangular = try values.decodeIfPresent(String.self, forKey: .logoRectangular)
        logoCircular = try values.decodeIfPresent(String.self, forKey: .logoCircular)
    }
}


struct AgeRange: Codable {
    let value: String?
    let isKids: Bool?
    let isRegular: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        value = try values.decodeIfPresent(String.self, forKey: .value)
        isKids = try values.decodeIfPresent(Bool.self, forKey: .isKids)
        isRegular = try values.decodeIfPresent(Bool.self, forKey: .isRegular)
    }
}

struct AndroidStick: Codable {
    let addPackImage: String?
    let bundle_offer: [BundleOffer]?
    let addPackImageV2: String?
    let addPackMessage: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addPackImage = try values.decodeIfPresent(String.self, forKey: .addPackImage)
        bundle_offer = try values.decodeIfPresent([BundleOffer].self, forKey: .bundle_offer)
        addPackImageV2 = try values.decodeIfPresent(String.self, forKey: .addPackImageV2)
        addPackMessage = try values.decodeIfPresent(String.self, forKey: .addPackMessage)
    }
}

struct App: Codable {
    let appUpgrade: AppUpgrade?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        appUpgrade = try values.decodeIfPresent(AppUpgrade.self, forKey: .appUpgrade)
    }
}

struct AppUpgrade: Codable {
    let ios: Ios?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        ios = try values.decodeIfPresent(Ios.self, forKey: .ios)
    }
}

struct BingePrimeSetting: Codable {
    let enableContent: Bool?
    let position: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        enableContent = try values.decodeIfPresent(Bool.self, forKey: .enableContent)
        position = try values.decodeIfPresent(Int.self, forKey: .position)
    }
}

struct Bitrate: Codable {
    let vod: Vod?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        vod = try values.decodeIfPresent(Vod.self, forKey: .vod)
    }
}

struct BundleOffer: Codable {
    let salesType: String?
    let image: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        salesType = try values.decodeIfPresent(String.self, forKey: .salesType)
        image = try values.decodeIfPresent(String.self, forKey: .image)
    }
}

struct Hd: Codable {
    let low: Int?
    let medium: Int?
    let high: Int?
    let def: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        low = try values.decodeIfPresent(Int.self, forKey: .low)
        medium = try values.decodeIfPresent(Int.self, forKey: .medium)
        high = try values.decodeIfPresent(Int.self, forKey: .high)
        def = try values.decodeIfPresent(Int.self, forKey: .def)
    }
}

struct Image: Codable {
    let cloudAccountUrl: String?
    let cloudSubAccountUrl: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cloudAccountUrl = try values.decodeIfPresent(String.self, forKey: .cloudAccountUrl)
        cloudSubAccountUrl = try values.decodeIfPresent(String.self, forKey: .cloudSubAccountUrl)
    }
}

struct Ios: Codable {
    let recommendedVersion: String?
    let forceUpgradeVersion: String?
    let recommendedMessage: String?
    let forceUpgradeMessage: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        recommendedVersion = try values.decodeIfPresent(String.self, forKey: .recommendedVersion)
        forceUpgradeVersion = try values.decodeIfPresent(String.self, forKey: .forceUpgradeVersion)
        recommendedMessage = try values.decodeIfPresent(String.self, forKey: .recommendedMessage)
        forceUpgradeMessage = try values.decodeIfPresent(String.self, forKey: .forceUpgradeMessage)
    }
}

struct NoSearchResultSuggestions: Codable {
    let kidsProfile: [String]?
    let regularProfile: [String]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        kidsProfile = try values.decodeIfPresent([String].self, forKey: .kidsProfile)
        regularProfile = try values.decodeIfPresent([String].self, forKey: .regularProfile)
    }
}

struct Profile: Codable {
    let ageRange: [AgeRange]?
    let awsBucket: String?
    let maxProfileCount: Int?
    let genders: [String]?
    let awsUrl: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        ageRange = try values.decodeIfPresent([AgeRange].self, forKey: .ageRange)
        awsBucket = try values.decodeIfPresent(String.self, forKey: .awsBucket)
        maxProfileCount = try values.decodeIfPresent(Int.self, forKey: .maxProfileCount)
        genders = try values.decodeIfPresent([String].self, forKey: .genders)
        awsUrl = try values.decodeIfPresent(String.self, forKey: .awsUrl)
    }
}

struct Sd: Codable {
    let low: Int?
    let medium: Int?
    let high: Int?
    let def: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        low = try values.decodeIfPresent(Int.self, forKey: .low)
        medium = try values.decodeIfPresent(Int.self, forKey: .medium)
        high = try values.decodeIfPresent(Int.self, forKey: .high)
        def = try values.decodeIfPresent(Int.self, forKey: .def)
    }
}

struct TaHeroBanner: Codable {
    let pageType: String?
    let count: Int?
    let position: String?
    let placeHolder: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pageType = try values.decodeIfPresent(String.self, forKey: .pageType)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        position = try values.decodeIfPresent(String.self, forKey: .position)
        placeHolder = try values.decodeIfPresent(String.self, forKey: .placeHolder)
    }
}

struct TaLearnAction: Codable {
    let lA2: Int?
    let fallBackLA2: Int?
    let contentType: String?
    let lA1: Int?
    let fallBackLA1: Int?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lA2 = try values.decodeIfPresent(Int.self, forKey: .lA2)
        fallBackLA2 = try values.decodeIfPresent(Int.self, forKey: .fallBackLA2)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        lA1 = try values.decodeIfPresent(Int.self, forKey: .lA1)
        fallBackLA1 = try values.decodeIfPresent(Int.self, forKey: .fallBackLA1)
    }
}

struct TaRelatedRail: Codable {
    let useCase: String?
    let contentType: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        useCase = try values.decodeIfPresent(String.self, forKey: .useCase)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
    }
}

struct TermsCondition: Codable {
    init(from decoder: Decoder) throws {
        _ = try decoder.container(keyedBy: CodingKeys.self)
    }
}

struct Url: Codable {
    let selfCareSignUpUrl: String?
    let eulaUrl: String?
    let image: Image?
    let helpUrl: String?
    let termsConditionsUrl: String?
    let privacyPolicyUrl: String?
    let privacyPolicyUrlHybrid: String?
    let termsConditionsUrlHybrid: String?
    let eulaUrlHybrid: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selfCareSignUpUrl = try values.decodeIfPresent(String.self, forKey: .selfCareSignUpUrl)
        eulaUrl = try values.decodeIfPresent(String.self, forKey: .eulaUrl)
        image = try values.decodeIfPresent(Image.self, forKey: .image)
        helpUrl = try values.decodeIfPresent(String.self, forKey: .helpUrl)
        termsConditionsUrl = try values.decodeIfPresent(String.self, forKey: .termsConditionsUrl)
        privacyPolicyUrl = try values.decodeIfPresent(String.self, forKey: .privacyPolicyUrl)
        privacyPolicyUrlHybrid = try values.decodeIfPresent(String.self, forKey: .privacyPolicyUrlHybrid)
        termsConditionsUrlHybrid = try values.decodeIfPresent(String.self, forKey: .termsConditionsUrlHybrid)
        eulaUrlHybrid = try values.decodeIfPresent(String.self, forKey: .eulaUrlHybrid)
    }
}

struct Vod: Codable {
    let sd: Sd?
    let hd: Hd?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sd = try values.decodeIfPresent(Sd.self, forKey: .sd)
        hd = try values.decodeIfPresent(Hd.self, forKey: .hd)
    }

}

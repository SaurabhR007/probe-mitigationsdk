//
//  MarketingViewModel.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 25/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol MarketingViewModelProtocol {
    func getMarketingScreen(completion: @escaping(Bool, String, Bool) -> Void)
    
}

class MarketingViewModel: MarketingViewModelProtocol{
    
    var repo: MarketingScreenRepo
    var requestToken: ServiceCancellable?
    var marketingScreenDetail: MarketingDataModel?
    
    init(repo: MarketingScreenRepo, apiEndPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = apiEndPoint
    }
    
    func getMarketingScreen(completion: @escaping (Bool, String, Bool) -> Void) {
        requestToken = repo.getMarketingScreenDetail(completion: {(configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.marketingScreenDetail = _configData.parsed.data
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

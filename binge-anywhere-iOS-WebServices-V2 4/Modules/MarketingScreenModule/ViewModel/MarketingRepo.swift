//
//  MarketingRepo.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 25/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol MarketingScreenRepo {
    var apiEndPoint: String {get set}
    var apiParams: [AnyHashable: Any] {get set}
    
    func getMarketingScreenDetail(completion: @escaping(APIServicResult<MarketingScreenResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct MarketingRepo: MarketingScreenRepo {
    
    var apiEndPoint: String = ""
    var apiParams: [AnyHashable : Any] = APIParams()
    
    func getMarketingScreenDetail(completion: @escaping (APIServicResult<MarketingScreenResponse>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        
        var target = ServiceRequestDetail.init(endpoint: .marketingDetails(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        target.detail.headers["platform"] = "ios"
        return APIService().request(target) { (response: APIResult<APIServicResult<MarketingScreenResponse>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
 
}

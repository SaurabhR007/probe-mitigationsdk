//
//  MarketingVCExtension.swift
//  BingeAnywhere
//
//  Created by Shivam on 02/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import FSPagerView

extension MarketingViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    
//    func configurePagerView() {
//        self.pagerView.backgroundColor = .clear
//        self.pagerView.delegate = self
//        self.pagerView.dataSource = self
//    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return (marketingVM?.marketingScreenDetail?.list!.count) ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> UICollectionViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: kAdvertisementCollectionViewCell, at: index) as? AdvertisementCollectionViewCell
        cell!.configureData(data: (marketingVM?.marketingScreenDetail?.list![index])!)
        return cell!
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: UICollectionViewCell, forItemAt index: Int) {
        currentIndex(ind: index)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        currentIndex(ind: targetIndex)
    }
}

//
//  MarketingViewController.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 20/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit
import FSPagerView

class MarketingViewController: BaseViewController {
    
    
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var curveView: UIView!
    @IBOutlet weak var indicatorStack: UIStackView!
    var marketingVM: MarketingViewModel?
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: kAdvertisementCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kAdvertisementCollectionViewCell)
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.isInfinite = true
            self.pagerView.automaticSlidingInterval = CGFloat(((marketingVM?.marketingScreenDetail?.autoScrollTime) ?? 0)/1000)
            self.pagerView.backgroundColor = .BAdarkBlueBackground
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        curveView.alpha=0
        getMarketingDetail()
        configureUI()
     //   configurePagerView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func configureUI(){
        self.view.backgroundColor = .BAdarkBlueBackground
        loginButton.text("Login")
        loginButton.style(.primary, isActive: true)
        loginButton.fontAndColor(font: UIFont.skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .white)
        configureIndicator(number: (marketingVM?.marketingScreenDetail?.list?.count) ?? 0, selectedIndex: 0)
    }

    
    func refreshPagerView(){
        if (marketingVM?.marketingScreenDetail?.list!.count ?? 0) <= 1{
            self.pagerView.automaticSlidingInterval = 0
            self.pagerView.isInfinite = false
        }else{
            self.pagerView.isInfinite = true
            self.pagerView.automaticSlidingInterval = CGFloat(((marketingVM?.marketingScreenDetail?.autoScrollTime) ?? 0)/1000)
        }
        self.pagerView.itemSize = CGSize.init(width: pagerView.frame.width, height: pagerView.frame.height)
        self.pagerView.reloadData()
    }
    
    func getMarketingDetail(){
        self.marketingVM = MarketingViewModel(repo: MarketingRepo(), apiEndPoint: "")
        fetchMarketingDetail()
    }
    
    func prepareBackground(){
        self.curveView.alpha=1
        self.curveView.clipsToBounds=true
        self.curveView.backgroundColor = UIColor.BAlightBlueGrey.withAlphaComponent(0.1)
        self.curveView.updateConstraints()
        self.curveView.makeCircular(roundBy: .height)
    }
    
    func fetchMarketingDetail(){
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if marketingVM != nil {
                marketingVM?.getMarketingScreen(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        if let screenCount = self.marketingVM?.marketingScreenDetail?.list?.count , screenCount >= 1{
                            self.refreshPagerView()
                            self.prepareBackground()
                        }else{
                            self.moveToLoginScreen()
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong) {
                            self.moveToLoginScreen()
                        }
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    func currentIndex(ind: Int) {
        let pages = marketingVM?.marketingScreenDetail?.list?.count ?? 0
        configureIndicator(number: pages, selectedIndex: ind)
    }
    
    func moveToLoginScreen() {
        
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "LoginVC", type: LoginVC.self)
        navigationController?.pushViewController(pushView, animated: true)
    }
    
    
    @IBAction func loginTapped(_ sender: Any) {
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "LoginVC", type: LoginVC.self)
        super.navigationController?.pushViewController(pushView, animated: true)
    }
    
    func configureIndicator(number: Int, selectedIndex: Int) {
        
        if number <= 1 {
            return
        } else if number > indicatorStack.arrangedSubviews.count {
            for index in indicatorStack.arrangedSubviews.count..<number {
                let indicatorLabel = UIView(frame: CGRect(0, 0, 8, 8))
                let widthConstraint  = indicatorLabel.widthAnchor.constraint(equalToConstant: index == selectedIndex ? 32 : 8)
                indicatorLabel.backgroundColor = index == selectedIndex ? .BABlueColor : .BAmidBlue
                widthConstraint.identifier = "width"
                widthConstraint.isActive = true
                indicatorLabel.makeCircular(roundBy: .height)
                indicatorStack.addArrangedSubview(indicatorLabel)
            }
        } else if  number < indicatorStack.arrangedSubviews.count {
            for index in 0..<(indicatorStack.arrangedSubviews.count - number){
                indicatorStack.arrangedSubviews[index].removeFromSuperview()
            }
            
        }else {
            for (index,indicator) in indicatorStack.arrangedSubviews.enumerated() {
                indicator.constraints.filter({$0.identifier == "width"}).first?.constant  = index == selectedIndex ? 32 : 8
                indicator.backgroundColor = index == selectedIndex ? .BABlueColor : .BAmidBlue
                if index == (indicatorStack.arrangedSubviews.count - 1) {
                    UIView.animate(withDuration: 0.2) {
                        self.indicatorStack.layoutIfNeeded()
                    }
                }
            }
        }
    }
}


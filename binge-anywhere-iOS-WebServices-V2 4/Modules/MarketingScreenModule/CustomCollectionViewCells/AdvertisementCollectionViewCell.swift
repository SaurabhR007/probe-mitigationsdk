//
//  AdvertisementCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 20/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class AdvertisementCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var adLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    func configureUI(){
        imageView.backgroundColor = .clear
        stackView.backgroundColor = .BAdarkBlueBackground
        self.contentView.backgroundColor = .clear//.BAdarkBlueBackground
        //adLabel.backgroundColor = .BAdarkBlueBackground
        //descriptionLabel.backgroundColor = .BAdarkBlueBackground
        adLabel.font = UIFont.skyTextFont(.medium, size: 28 * screenScaleFactorForWidth)
        descriptionLabel.font = UIFont.skyTextFont(.medium, size: 17 * screenScaleFactorForWidth)
        descriptionLabel.textColor = .BAlightBlueGrey
    }
    
    func configureData(data:DetailList) {
        adLabel.text = data.title
        descriptionLabel.text = data.description
        let url = URL.init(string: data.image ?? "")
        UtilityFunction.shared.performTaskInMainQueue {
            if let _url = url {
                self.imageView.alpha = (self.imageView.image == nil) ? 0 : 1
                self.imageView.kf.setImage(with: _url, completionHandler:  { _ in
                    UIView.animate(withDuration: 0.2 , animations: { self.imageView.alpha = 1 })
                })
            }
        }
    }
}

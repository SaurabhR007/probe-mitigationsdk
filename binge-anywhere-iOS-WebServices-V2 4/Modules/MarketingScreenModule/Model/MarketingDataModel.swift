//
//  MarketingDataModel.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 25/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
struct MarketingScreenResponse : Codable {
    let code : Int?
    let message : String?
    let data : MarketingDataModel?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(MarketingDataModel.self, forKey: .data)
    }

}

struct MarketingDataModel : Codable {
    let autoScrollTime : Int?
    let list : [DetailList]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        autoScrollTime = try values.decodeIfPresent(Int.self, forKey: .autoScrollTime)
        list = try values.decodeIfPresent([DetailList].self, forKey: .list)
    }

}

struct DetailList : Codable {
    let title : String?
    let description : String?
    let image : String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image = try values.decodeIfPresent(String.self, forKey: .image)
    }

}

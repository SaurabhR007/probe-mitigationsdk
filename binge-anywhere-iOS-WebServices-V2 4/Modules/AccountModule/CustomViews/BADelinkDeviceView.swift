//
//  BADelinkDeviceView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BADelinkDeviceView: UIView {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var removeDeviceButton: UIButton!
    @IBOutlet weak var removeDeviceNameLabel: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!

    override func draw(_ rect: CGRect) {
        removeDeviceButton.makeCircular(roundBy: .height)
        cancelButton.underline()
    }

    @IBAction func cancelButtonAction(_ sender: Any) {
        self.removeFromSuperview()
    }

    @IBAction func removeDeviceButton(_ sender: Any) {
        self.removeFromSuperview()
    }

}

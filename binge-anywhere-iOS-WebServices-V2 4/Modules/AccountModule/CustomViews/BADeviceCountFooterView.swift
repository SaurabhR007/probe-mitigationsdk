//
//  BADeviceCountFooterView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 20/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BADeviceCountFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var deviceCountLabel: CustomLabel!

    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureUI(with text: String) {
        deviceCountLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 12), color: .BAwhite)
        deviceCountLabel.text = text
		self.contentView.backgroundColor = .BAdarkBlueBackground
    }

}

//
//  BADeviceHeaderView.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BADeviceHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var deviceHeaderLabel: CustomLabel!

    override class func awakeFromNib() {
		super.awakeFromNib()
    }
	
    func configureUI(_ value: String) {
		self.contentView.backgroundColor = .BAdarkBlueBackground
        self.deviceHeaderLabel.text = value
		deviceHeaderLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BASecondaryGray)
	}
}

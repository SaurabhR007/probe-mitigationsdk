//
//  BADeviceManagementVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 10/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

class BADeviceManagementVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var deviceManagementTableView: UITableView!
    @IBOutlet weak var titleDescriptionLabel: CustomLabel!
    @IBOutlet weak var noDeviceLabel: UILabel!
    
    // MARK: - Variables
    var deviceListVM: BADeviceListViewModal?
    var deviceList: [DeviceList?] = []
    var selectedIndex = 0
    var otherDeviceCount = 3
    var deviceCountFooterText = ""
    var primaryDeviceList: [DeviceList?] = []

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.listDevices.rawValue)
        getDeviceList()
        conformDelegates()
        registerCells()
        configureUI()
    }

    // MARK: - Function
    private func configureUI() {
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "Device Management")
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 20), color: .BAwhite)
        titleDescriptionLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 15), color: .BAwhite)
        view.backgroundColor = .BAdarkBlueBackground
    }

    private func conformDelegates() {
        deviceManagementTableView.backgroundColor = .BAdarkBlueBackground
        deviceManagementTableView.delegate = self
        deviceManagementTableView.dataSource = self
    }

    private func registerCells() {
        UtilityFunction.shared.registerCell(deviceManagementTableView, kBAPrimaryDeviceTableViewCell)
        UtilityFunction.shared.registerCell(deviceManagementTableView, kBASecondryDeviceTableViewCell)
        let headerNib = UINib.init(nibName: "BADeviceHeaderView", bundle: Bundle.main)
        deviceManagementTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "BADeviceHeaderView")
        let footerNib = UINib.init(nibName: "BADeviceCountFooterView", bundle: Bundle.main)
        deviceManagementTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "BADeviceCountFooterView")
    }

    private func getDeviceList() {
        deviceListVM = BADeviceListViewModal(repo: BADeviceListRepo(), endPoint: BAKeychainManager().baId)
        getDeviceListData()
    }

    // MARK: Device List API Calling
    private func getDeviceListData() {
        showActivityIndicator(isUserInteractionEnabled: true)
        if let dataModel = deviceListVM {
            dataModel.getDeviceList {(flagValue, message, isApiError) in
                self.hideActivityIndicator()
                if flagValue {
                    self.deviceList = self.deviceListVM?.deviceList?.data?.deviceList ?? []
                    self.getPrimaryDeviceList()
                    self.otherDeviceCount = self.deviceList.count
                    self.deviceCountFooterText = "\(self.otherDeviceCount) out of 3"
                    if self.deviceList.count > 0 {
                        self.noDeviceLabel.isHidden = true
                    } else {
                        self.noDeviceLabel.isHidden = true
                    }
                    self.deviceManagementTableView.reloadData()
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        } else {
            self.hideActivityIndicator()
        }
    }
    
    func getPrimaryDeviceList() {
        primaryDeviceList = self.deviceList.filter({ (device) -> Bool in
            return device?.primary ?? false
        })
        
        self.deviceList = self.deviceList.filter({ (device) -> Bool in
            return device?.primary == false
        })
    }
    
    // MARK: - Actions
    @objc func removeSelectedDevice() {
        if BAReachAbility.isConnectedToNetwork() {
            let apiParams = APIParams()
			var header: APIHeaders
            var requestToken: ServiceCancellable?
            let endStr = BAKeychainManager().baId + "/" + (self.deviceList[selectedIndex]?.deviceNumber ?? "") // crash
			header = ["authorization" : BAKeychainManager().acccountDetail?.userAuthenticateToken ?? "", "subscriberId" : BAKeychainManager().sId]
            let target = ServiceRequestDetail.init(endpoint: .deleteDevices(param: apiParams, headers: header, endUrlString: endStr ))
            requestToken = APIService().request(target) { (response: APIResult<APIServicResult<RemoveDeviceModel>, ServiceProviderError>) in
                requestToken = nil
                switch response {
                case .success(let result):
                    print(result.parsed)
                    self.otherDeviceCount -= 1
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.removeDevice.rawValue)
                    self.deviceList.remove(at: self.selectedIndex)
                    self.deviceManagementTableView.reloadData()
                case .error:
                    break
                }
            }
        } else {
			noInternet()
//                {
//				self.removeSelectedDevice()
//			}
        }
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

}

// MARK: - Extension
extension BADeviceManagementVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        deviceManagementTableView.isHidden = deviceList.isEmpty
        switch section {
        case 0:
            return self.primaryDeviceList.count
        default:
            return self.deviceList.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = deviceManagementTableView.dequeueReusableCell(withIdentifier: kBAPrimaryDeviceTableViewCell) as? BAPrimaryDeviceTableViewCell
            cell?.configureDevice(self.primaryDeviceList    [indexPath.row])
            cell?.backgroundColor = .BAdarkBlueBackground
            return cell!
        case 1:
            let cell = deviceManagementTableView.dequeueReusableCell(withIdentifier: kBASecondryDeviceTableViewCell) as? BASecondryDeviceTableViewCell
            cell?.configureDevices(deviceDetail: self.deviceList[indexPath.row], index: indexPath.row)
            cell?.delegate = self
            cell?.backgroundColor = .BAdarkBlueBackground
            return cell!
        default:
            let cell = deviceManagementTableView.dequeueReusableCell(withIdentifier: kBASecondryDeviceTableViewCell) as? BASecondryDeviceTableViewCell
            cell?.configureDevices(deviceDetail: self.deviceList[indexPath.row], index: indexPath.row)
            cell?.delegate = self
            cell?.backgroundColor = .BAdarkBlueBackground
            return cell!
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2 //(primaryDeviceList.count > 0) ? 2 : 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return (primaryDeviceList.count > 0) ? 85 : 0.01
        default:
            return 55
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return (primaryDeviceList.count > 0) ? 20 : 0.01
        default:
            return 20
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0.01
        default:
            return 20
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let primaryDeviceListExist = (primaryDeviceList.count > 0)
//        if primaryDeviceListExist {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BADeviceHeaderView") as! BADeviceHeaderView
            switch section {
            case 0:
                headerView.configureUI(kLargeDeviceHeader)
            case 1:
                headerView.configureUI(kSmallDeviceHeader)
            default:
                headerView.configureUI(kSmallDeviceHeader)
            }
            return headerView
//        } else {
//            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BADeviceHeaderView") as! BADeviceHeaderView
//            headerView.configureUI("Other Devices")
//            return headerView
//        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BADeviceCountFooterView") as! BADeviceCountFooterView
        footerView.configureUI(with: "\(self.otherDeviceCount) out of 3")
        return footerView
    }
}

extension BADeviceManagementVC: BASecondryDeviceTableViewCellDelegate {
    func removeDevice(index: Int?, deviceName: String?) {
        selectedIndex = index ?? 0
        deviceManagementTableView.reloadData()
        let custView = Bundle.main.loadNibNamed("BADelinkDeviceView", owner: self, options: nil)![0] as? BADelinkDeviceView
        custView?.center = self.view.center
//        custView?.removeDeviceNameLabel.numberOfLines = 3
//        custView?.removeDeviceNameLabel.lineBreakMode = .byWordWrapping
        custView?.removeDeviceNameLabel.text = "Are you sure you \n want to logout of \n \(deviceName ?? "") ?"
        custView?.removeDeviceNameLabel.frame.size.width = 200
        custView?.removeDeviceNameLabel.sizeToFit()
        custView?.removeDeviceButton.addTarget(self, action: #selector(removeSelectedDevice), for: .touchUpInside)
        self.view.addSubview(custView!)
    }
}

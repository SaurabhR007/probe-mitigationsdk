//
//  ExtensionEditProfileVC+tableViewDelegate.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import ARSLineProgress

extension EditProfileVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = dataSource?.count else {
            return 0
        }
        if BAKeychainManager().loginType == "OTP" {
            return count - 1
        } else {
           return count
        }
    }
    
    //Todo optimise
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if BAKeychainManager().loginType == "OTP" {
            switch indexPath.row {
               case 0:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileImageTableViewCell) as? EditProfileImageTableViewCell
                   cell?.configureView(userDetail: userDetail)
                   cell?.setUIImage(dataSource?[0].user?.profileImageURL ?? "")
                   cell?.delegate = self
                   return cell!
               case 1:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileNumberTableViewCell) as? EditProfileNumberTableViewCell
                   cell?.configureCell(title: "Name", dataSource?[indexPath.row].textValue.capitalizingFirstLetter() ?? "", separator: true)
                   return cell!
               case 2:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileInputTableViewCell) as? EditProfileInputTableViewCell
                   emailTextFieldCell = cell
                   cell?.configureUi((dataSource?[indexPath.row])!)
                   cell?.inputTextField.tag = indexPath.row
                   cell?.delegate = self
                   return cell!
               case 3:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileNumberTableViewCell) as? EditProfileNumberTableViewCell
                   cell?.configureCell(title: "Registered Mobile Number", "+91 " + (userDetail?.rmn ?? ""), separator: true)
                   return cell!
               case 4:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kNextButtonTableViewCell) as? NextButtonTableViewCell
                cell?.configureCellForAccountSection()
                   cell?.nextButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
                   cell?.delegate = self
                   cell?.selectionStyle = .none
                   cell?.configureCell(source: .updateDetails)
                   if isEmailUpdated && dataSource?.first?.user?.email.length ?? 0 > 0 {
                       cell?.nextButton.style((dataSource?.first?.user?.email ?? "").length > 0 ? .primary : .tertiary, isActive: (dataSource?.first?.user?.email ?? "").length > 0)
                       isCellReloaded = true
                   } else {
                       cell?.nextButton.style(.tertiary, isActive: false)
                       isCellReloaded = false
                   }
                   return cell!
               case 5:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kCancelTableViewCell) as? CancelTableViewCell
                   cell!.delegate = self
                   return cell!
               default:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileInputTableViewCell) as? EditProfileInputTableViewCell
                   cell?.configureUi((dataSource?[indexPath.row])!)
                   cell?.delegate = self
                   return cell!
            }
        } else {
            switch indexPath.row {
               case 0:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileImageTableViewCell) as? EditProfileImageTableViewCell
                   cell?.configureView(userDetail: userDetail)
                   cell?.setUIImage(dataSource?[0].user?.profileImageURL ?? "")
                   cell?.delegate = self
                   return cell!
               case 1:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileNumberTableViewCell) as? EditProfileNumberTableViewCell
                   cell?.configureCell(title: "Name", dataSource?[indexPath.row].textValue.capitalizingFirstLetter() ?? "", separator: true)
                   return cell!
               case 2:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileInputTableViewCell) as? EditProfileInputTableViewCell
                   emailTextFieldCell = cell
                   cell?.configureUi((dataSource?[indexPath.row])!)
                   cell?.inputTextField.tag = indexPath.row
                   cell?.delegate = self
                   return cell!
               case 3:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileNumberTableViewCell) as? EditProfileNumberTableViewCell
                   cell?.configureCell(title: "Registered Mobile Number", "+91 " + (userDetail?.rmn ?? ""), separator: false)
                   return cell!
               case 4:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfilePasswordTableViewCell) as? EditProfilePasswordTableViewCell
                   return cell!
               case 5:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kNextButtonTableViewCell) as? NextButtonTableViewCell
//                cell?.configureCellForAccountSection()
                   cell?.nextButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
                   cell?.delegate = self
                   cell?.selectionStyle = .none
                   cell?.configureCell(source: .updateDetails)
                   if isEmailUpdated {
                       cell?.nextButton.style((dataSource?.first?.user?.email ?? "").length > 0 ? .primary : .tertiary, isActive: (dataSource?.first?.user?.email ?? "").length > 0)
                   } else {
                       cell?.nextButton.style(.tertiary, isActive: false)
                   }
                   return cell!
               case 6:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kCancelTableViewCell) as? CancelTableViewCell
                   cell!.delegate = self
                   return cell!
               default:
                   let cell = tableView.dequeueReusableCell(withIdentifier: kEditProfileInputTableViewCell) as? EditProfileInputTableViewCell
                   cell?.configureUi((dataSource?[indexPath.row])!)
                   cell?.delegate = self
                   return cell!
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 210.0
        default:
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if BAKeychainManager().loginType == "OTP" {
            //  Do Nothing
        } else {
            if indexPath.row == 4 {
                if BAReachAbility.isConnectedToNetwork() {
                    let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "PasswordVC", type: PasswordVC.self)
                    pushView.isFromEditProfile = true
                    pushView.dataModel = self.updateForgotPasswordModel()
                    self.navigationController?.pushViewController(pushView, animated: true)
                } else {
                    noInternet()
                }
            }
        }
    }
    
    func updateForgotPassword() -> LoginViewModel {
        let verificationDataModel = LoginViewModel()
        verificationDataModel.dataModel.rmn = BAKeychainManager().mobileNumber
        verificationDataModel.dataModel.sid = BAKeychainManager().sId
        return verificationDataModel
    }

    func updateForgotPasswordModel() -> UpdatePasswordDataModel {
        var dataModel = UpdatePasswordDataModel()
        dataModel.headerLogoText = "Create a New Password"
        dataModel.rmn = BAKeychainManager().mobileNumber//"9990573992"
        dataModel.frstLabelDescText = "Current Password"
        dataModel.hideCurrentPasswordPeekButton = false
        dataModel.frstLabelPlaceHolderText = "Password ⃰"
        dataModel.buttonText = "Update Password"
        return dataModel
    }

}

extension EditProfileVC: NextButtonActionDelegate {
    func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            CustomLoader.shared.showLoader(false)
            if let _editProfileVM = editProfileVM {
                emailTextFieldCell?.errorInfoLabel.isHidden = true
                _editProfileVM.editProfile(completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.otpResent(message, completion: nil)
                        //self.popupNotification(message)
                        self.callback?()
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updateEmail.rawValue, properties: [MixpanelConstants.ParamName.email.rawValue: self.dataSource?.first?.user?.email ?? ""])
                        self.isEmailUpdated = false
                        if BAKeychainManager().loginType == "OTP" {
                            let indexPath = IndexPath.init(row: 4, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .none)
                        } else {
                            let indexPath = IndexPath.init(row: 5, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .none)
                        }
                    } else if isApiError {
                        if message == "cancelled" {
                            // Do nothing
                        } else {
                            self.apiError(message, title: kSomethingWentWrong)
                        }
                    } else {
                        self.emailTextFieldCell?.errorInfoLabel.isHidden = false
                        self.emailTextFieldCell?.errorInfoLabel.text = message
                    }
                })
                self.hideActivityIndicator()
            }
        } else {
			noInternet()
        }
    }
}

//
//  EditProfileVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class EditProfileVC: BaseViewController, PortaitSupportable {

    // MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Variable
    var dataSource: [EditProfileDataSource]?
   // var verificationDataModel: LoginViewModel?
    var editProfileVM: EditProfileVMProtocol?
    var userDetail: UserDetailData?
    var emailTextFieldCell: EditProfileInputTableViewCell?
    var isEmailUpdated: Bool = false
    var isCellReloaded: Bool = false
    var callback: (() -> Void)?
  
    
    // Set the shouldAutorotate to False
    override open var shouldAutorotate: Bool {
        return false
    }
    
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override public var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    

    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setDataSource()
        setUI()
        setupNavigation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        BAKeychainManager().isOnEditProfileScreen = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    // MARK: - Functions
    private func setUI() {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.editProfile.rawValue)
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        regsiterCells()
        reCheckVM()
       // reCheckVerificationVM()
    }

    private func reCheckVM() {
        if editProfileVM == nil {
            editProfileVM = EditProfileVM(EditProfileDataRepo())
        }
    }

    private func setDataModel() -> EditProfileDataModel {
        let data = EditProfileDataModel()
        if let img = userDetail?.profileImage {
            BAKeychainManager().profileImageRelativePath = img
            if let configUrl =  BAConfigManager.shared.configModel?.data?.config?.subscriberImage?.subscriberImageBaseUrl {
                data.profileImageURL = configUrl + img
            }
        }
        data.firstName = userDetail?.firstName ?? "Enter First Name"//fullName?[0] ?? ""
        data.lastName = userDetail?.lastName ?? "Enter Last Name"//fullName?[1] ?? ""
        data.email = userDetail?.email ?? "Enter Email Id"//BAUserDefaultManager().loginDetail?.data?.userDetails?.emailId ?? ""
        data.phoneNumber = "+91" + (userDetail?.rmn ?? "Mobile Number")//BAUserDefaultManager().mobileNumber
        return data
    }

    private func setDataSource() {
        dataSource = EditProfileDataSource.numberOfCells
        dataSource?[0].user = setDataModel()
    }

    private func regsiterCells() {
        UtilityFunction.shared.registerCell(tableView, kEditProfileInputTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kEditProfilePasswordTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kEditProfileImageTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kEditProfileNumberTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kCancelTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kNextButtonTableViewCell)
    }

    func setupNavigation() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Edit Profile")
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

//extension EditProfileVC: OrientationHandlable {
//    var blockRotation: Bool {
//         return true
//    }
//}

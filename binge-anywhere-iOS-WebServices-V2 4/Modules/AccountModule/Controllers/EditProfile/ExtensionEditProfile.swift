//
//  ExtensionEditProfile.swift
//  BingeAnywhere
//
//  Created by Shivam on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import ARSLineProgress

extension EditProfileVC: AddProfileImageDelegate, EditProfileTextFieldDelegate, CancelButtonDelegate {
    
    func textFieldValueChanged(_ tag: Int, _ text: String) {
        switch tag {
        case 1: self.dataSource?.first?.user?.firstName = text
        //case 2: self.dataSource?.first?.user?.lastName = text
        case 2: self.dataSource?.first?.user?.email = text
        emailTextFieldCell?.errorInfoLabel.isHidden = true
        isEmailUpdated = true
        enableUpdateProfileButton()
        default: break
        }
    }
    
    func enableUpdateProfileButton() {
        if let _ = self.dataSource?.first?.user?.email {
            if BAKeychainManager().loginType == "OTP" {
                if isCellReloaded && dataSource?.first?.user?.email.length ?? 0 > 0 {
                    // Do Nothing
                } else {
                    let indexPath = IndexPath.init(row: 4, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
            } else {
                if isCellReloaded {
                    // Do Nothing
                } else {
                    let indexPath = IndexPath.init(row: 5, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
    }
    
    func updateProfileImageButton() {
        var optionButtonArr = ["Gallery", "Camera"]
        if !(BAKeychainManager().profileImageRelativePath.isEmpty){
            optionButtonArr = ["Gallery", "Camera", "Remove Photo"]
        }
        
        UtilityFunction.shared.showActionSheetFromViewController(viewController: self, options: optionButtonArr, title: kAppName, message: "Choose Profile Picture") { (index, title) in
            print(title)
            if index == 0 { // Gallery
                UtilityFunction.shared.getImageFromGalleryFromViewController(viewController: self, callBack: { (image) in
                    if let image = image {
                        self.uploadProfileImage(image)
                    }
                })
            } else if index == 1 { // Camera
                UtilityFunction.shared.getImageFromCameraFromViewController(viewController: self, isSelfie: false, callBack: { (image) in
                    if let image = image {
                        print("*********", image)
                        self.uploadProfileImage(image)
                    }
                })
            } else if index == 2 { // Delete
                if BAReachAbility.isConnectedToNetwork() {
                    self.deleteUserImage()
                } else {
                    self.noInternet()
                }
            }
        }
    }
    
    func cancelButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func deleteUserImage() {
        if BAReachAbility.isConnectedToNetwork() {
            if editProfileVM != nil {
                showActivityIndicator(isUserInteractionEnabled: false)
                editProfileVM?.deleteProfileImage(completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updateEmail.rawValue)
                        self.tableView.reloadData{
                            self.otpResent(message, completion: nil)
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updateEmail.rawValue)
                        }
                        self.callback?()
                    } else if isApiError {
                        if message == "cancelled" {
                            // Do nothing
                        } else {
                            self.apiError(message, title: kSomethingWentWrong)
                        }
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
    
    func uploadProfileImage(_ Image: UIImage) {
        if BAReachAbility.isConnectedToNetwork() {
            if editProfileVM != nil {
                showActivityIndicator(isUserInteractionEnabled: true)
                editProfileVM?.uploadImage([Image], completion: { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updateEmail.rawValue)
                        self.dataSource?[0].user?.profileImageURL = (BAConfigManager.shared.configModel?.data?.config?.subscriberImage?.subscriberImageBaseUrl ?? "") + BAKeychainManager().profileImageRelativePath
                        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.updateEmail.rawValue)
                        self.tableView.reloadData {
                            self.otpResent(message, completion: nil)
                        }
                        self.callback?()
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                })
            }
        } else {
            noInternet()
        }
    }
}

//
//  ExtensionMySubscription+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 13/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension MySubscriptionVC: UITableViewDataSource, UITableViewDelegate, NextButtonActionDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as? BottomBingeLogoTableViewCell
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: kTitleHeaderTableViewCell) as? TitleHeaderTableViewCell
            cell?.bottomConstraint.constant = 0.0
            cell?.configureCell(source: .subscription)
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: kMySubcriptionTableViewCell) as? MySubcriptionTableViewCell
            if let data = subscriptionData {
                cell?.configureCell(data: data)
            }
            cell?.clipsToBounds = true
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: kPrimSubscriptionTableViewCell) as? PrimSubscriptionTableViewCell
            if let data = subscriptionData {
                cell?.configureCellForPrime(data: data)
            }
            cell?.clipsToBounds = true
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: kBAOrSeparatorTableViewCell) as? BAOrSeparatorTableViewCell
            if subscriptionData?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff || subscriptionData == nil {
                cell?.configureCell("You are not subscribed to Tata Sky Binge", labelIsHidden: false)
                cell?.titleLabel?.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAlightBlueGrey)
                return cell!
            } else if (subscriptionData?.fsTaken ?? false) && (subscriptionData?.cancelled ?? false) && (subscriptionData?.subscriptionInformationDTO?.bingeAccountStatus?.uppercased() == kActive.uppercased()) {
                cell?.configureCell("Your FTV will be collected back after your Binge subscription ends.", labelIsHidden: false)
                return cell!
            } else {
                if subscriptionData?.status == "DEACTIVE" {
                    cell?.configureCell("", labelIsHidden: true)
                    return cell!
                } else {
                    cell?.configureCell("Subscription amount will be deducted from your Tata Sky balance every 30 days.", labelIsHidden: true)
                    return cell!
                }
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: kNextButtonTableViewCell) as? NextButtonTableViewCell
            cell?.doNotConfigureButton()
            return cell!
        }
    }
    
    
    //subscriptionData?.providers?.isEmpty ?? true  check is removed after sync with srikant that this is of no use, in case 4 for cellfor row subscription message is to be hidden as per apple subscription check requirement app was rejected because of this
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (subscriptionData?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff) || (subscriptionData == nil) {
            if indexPath.row == 2 ||  indexPath.row == 3 {
                return 0.01
            }
        }
        if !isPrimeEnabled {
            if indexPath.row == 3 {
                return 0.01
            }
        }
        return UITableView.automaticDimension
    }
}

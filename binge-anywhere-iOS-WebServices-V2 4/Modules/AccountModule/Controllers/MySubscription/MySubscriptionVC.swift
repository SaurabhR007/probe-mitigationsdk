//
//  MySubscriptionVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 13/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class MySubscriptionVC: BaseViewController {
	
	// MARK: - Outlet
	@IBOutlet weak var tableView: UITableView!

	// MARK: - Variable
	var subscriptionData: PartnerSubscriptionDetails?
    @IBOutlet weak var proceedButton: CustomButton!
    var isFromAccountScreen = false
    var isPrimeEnabled = false
		
	// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIsPrimeEnabled()
        setNavigationBar()
		configureUI()
		setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        proceedButton.style(.primary, isActive: true)
        proceedButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        if isFromAccountScreen {
            proceedButton.isHidden = true
        } else {
            proceedButton.isHidden = false
        }
    }
    
    func checkIsPrimeEnabled() {
        if let data = subscriptionData?.primePackDetails {
            isPrimeEnabled = UtilityFunction.shared.isValidPrimeSubscription(data)
        }
    }
    
	// MARK: - Functions
	private func setNavigationBar() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self)
	}
	
	@objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
	}
	
	private func configureUI() {
		self.view.backgroundColor = UIColor.BAdarkBlueBackground
	}

	private func setUpTableView() {
		tableView.allowsSelection = false
		tableView.backgroundColor = .BAdarkBlueBackground
		tableView.rowHeight = UITableView.automaticDimension
		tableView.dataSource = self
		tableView.delegate = self
		tableView.register(UINib(nibName: kBottomBingeLogoTableViewCell, bundle: nil), forCellReuseIdentifier: kBottomBingeLogoTableViewCell)
        tableView.register(UINib(nibName: kPrimSubscriptionTableViewCell, bundle: nil), forCellReuseIdentifier: kPrimSubscriptionTableViewCell)
		tableView.register(UINib(nibName: kTitleHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: kTitleHeaderTableViewCell)
		tableView.register(UINib(nibName: kNextButtonTableViewCell, bundle: nil), forCellReuseIdentifier: kNextButtonTableViewCell)
		tableView.register(UINib(nibName: kBAOrSeparatorTableViewCell, bundle: nil), forCellReuseIdentifier: kBAOrSeparatorTableViewCell)
		tableView.register(UINib(nibName: kMySubcriptionTableViewCell, bundle: nil), forCellReuseIdentifier: kMySubcriptionTableViewCell)
	}

	func nextButtonTapped() {
        if BAReachAbility.isConnectedToNetwork() {
            navigateToHome()
        } else {
            noInternet()
        }
	}
    
    @IBAction func proceedButtonAction(_ sender: Any) {
        nextButtonTapped()
    }
    
    func navigateToHome() {
        kAppDelegate.createHomeTabRootView()
    }
}

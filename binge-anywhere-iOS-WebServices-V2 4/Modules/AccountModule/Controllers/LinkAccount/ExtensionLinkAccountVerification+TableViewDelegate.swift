//
//  ExtentionLinkAccountVerification+TableViewDelegate.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension LinkAccountVerificationVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: kPageIndicatorTableViewCell) as? PageIndicatorTableViewCell
            cell?.configureCell(with: .second)
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: kTitleHeaderTableViewCell) as? TitleHeaderTableViewCell
            cell?.configureCell(source: .linkAccountVerification)
            cell?.titleLabel.text?.append("+91 \(rmn.replaceFrom(startIndex: 2, endIndex: 5, With: "XXXX"))")
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: kRMNTableViewCell) as? RMNTableViewCell
            cell?.delegate = self
            cell?.inputTextField.tag = 1
            cell?.configureCell(.linkAccountVerification)
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: kForgetPasswordTableViewCell) as? ForgetPasswordTableViewCell //Resend OTP
            cell?.delegate = self
            cell?.configureCell(source: .linkAccountVerification)
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: kNextButtonTableViewCell) as? NextButtonTableViewCell
            cell?.delegate = self
            cell?.configureCell(source: .linkAccountVerification)
            return cell!
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: kDescriptionTableViewCell) as? DescriptionTableViewCell
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as? BottomBingeLogoTableViewCell
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 90
        } else {
            return UITableView.automaticDimension
        }
    }
}

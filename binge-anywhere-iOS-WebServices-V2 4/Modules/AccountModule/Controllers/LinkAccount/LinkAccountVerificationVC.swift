//
//  LinkAccountVerificationVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class LinkAccountVerificationVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Variables
    var rmn = ""
    var otp = ""

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        tableViewSetup()
    }

    private func configUI() {
        self.view.backgroundColor = .BAdarkBlueBackground
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "Link Tata Sky Accounts")
    }

    private func tableViewSetup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        UtilityFunction.shared.registerCell(tableView, kPageIndicatorTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kBottomBingeLogoTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kTitleHeaderTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kRMNTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kDescriptionTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kNextButtonTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kForgetPasswordTableViewCell)
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

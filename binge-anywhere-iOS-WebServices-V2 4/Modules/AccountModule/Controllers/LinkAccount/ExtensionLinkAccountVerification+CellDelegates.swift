//
//  ExtentionLinkAccountVerification+CellDelegates.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension LinkAccountVerificationVC: RMNTableViewCellProtocol {
    func valueUpdated(_ idx: Int, _ text: String) {
        otp = text
    }
}

extension LinkAccountVerificationVC: ForgetButtonActionDelegate {
    func forgetButtonTapped() {
        view.makeToast("OTP resent")
    }
}

extension LinkAccountVerificationVC: NextButtonActionDelegate {
    func nextButtonTapped() {
        if !otp.isEmpty && otp.count == 6 {
            let successVC: ApiSuccessViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.commonAlert.fileName, identifierVC: ViewControllers.successVC.rawValue, type: ApiSuccessViewController.self)
            successVC.successTitleText = "Linked Successfully"
            successVC.secondaryTitleText = "Your Tata sky Sky DTH with Subscriber ID 65446 has been successfully linked to your Tata sky Binge Account"
            successVC.nextButtonTitle = "Back to Accounts"
            self.navigationController?.pushViewController(successVC, animated: true)
        } else if otp.isEmpty {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowErrorForLink"), object: nil)
        } else {
            self.view.endEditing(true)
        }
    }
}

//
//  ExtentionLinkAccountVC+TableViewDelegate.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension LinkAccountsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: kPageIndicatorTableViewCell) as? PageIndicatorTableViewCell
            cell?.configureCell(with: .first)
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: kTitleHeaderTableViewCell) as? TitleHeaderTableViewCell
            cell?.configureCell(source: .linkAccount)
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: kRMNTableViewCell) as? RMNTableViewCell
            cell?.delegate = self
            cell?.inputTextField.tag = 0
            cell?.configureCell(.linkAccount)
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: kNextButtonTableViewCell) as? NextButtonTableViewCell
            cell?.delegate = self
            cell?.configureCell(source: .linkAccount)
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: kBottomBingeLogoTableViewCell) as? BottomBingeLogoTableViewCell
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 90
        } else {
            return UITableView.automaticDimension
        }
    }

}

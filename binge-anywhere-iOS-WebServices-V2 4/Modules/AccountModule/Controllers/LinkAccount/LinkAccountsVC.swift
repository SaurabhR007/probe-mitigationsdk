//
//  LinkAccountsVC.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 05/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class LinkAccountsVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Variables
    var rmn = ""

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        tableViewSetup()
    }

    private func configUI() {
        self.view.backgroundColor = .BAdarkBlueBackground
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "Link Tata Sky Accounts")
    }

    private func tableViewSetup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        UtilityFunction.shared.registerCell(tableView, kPageIndicatorTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kBottomBingeLogoTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kTitleHeaderTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kRMNTableViewCell)
        UtilityFunction.shared.registerCell(tableView, kNextButtonTableViewCell)
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

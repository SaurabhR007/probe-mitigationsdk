//
//  ExtentionLinkAccount+CellDelegates.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

extension LinkAccountsVC: RMNTableViewCellProtocol {
    func valueUpdated(_ idx: Int, _ text: String) {
        rmn = text
    }
}

extension LinkAccountsVC: NextButtonActionDelegate {
    func nextButtonTapped() {
        if !rmn.isEmpty && rmn.count == 10 {
            let linkAccountVerification: LinkAccountVerificationVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.fileName, identifierVC: ViewControllers.linkAccountVerificationVC.rawValue, type: LinkAccountVerificationVC.self)
            linkAccountVerification.rmn = rmn
            self.navigationController?.pushViewController(linkAccountVerification, animated: true)
        } else if rmn.isEmpty {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowErrorForLink"), object: nil)
        } else {
            self.view.endEditing(true)
        }
    }
}

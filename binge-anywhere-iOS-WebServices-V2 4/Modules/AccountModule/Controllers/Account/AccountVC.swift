//
//  AccountVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

class AccountVC: BaseViewController, MoengageProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    var aliasModel: BAAliasNameViewModal?
    var userProfileVM: BAUserProfileViewModal?
    var userDetailVM: BAUserDetailViewModal?
    var userBalanceVM: BAUserBalanceViewModal?
    var subscriptionVM: PackSubscriptionVM?
    var partnerSubscriptionDetail: PartnerSubscriptionDetails?
    var accountHeaderCell: AccountsHeaderTableViewCell?
    var userProfile: [AnyHashable: Any]?
    var isTableReloaded = false
    var isViewAppeared = false
    var noInternetViewAppeared = false
    let group = DispatchGroup()
    var shouldShowSwitchAccount = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.configureNavigationBar(with: .clearBackground, nil, nil, nil)
        setUpUI()
        conformDelegates()
        reCheckVM()
        callScreenData()
        addObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isViewAppeared {
            BAKeychainManager().isOnEditProfileScreen = false
            checkToRefreshUnreadCount()
        }
        isViewAppeared = true
        //callScreenData()
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeAccount.rawValue)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func reCheckVM() {
        if self.aliasModel == nil {
            self.aliasModel = BAAliasNameViewModal(repo: BAAddAliasRepo())
        }
    }
    
    // In order to hotfix the notification badge icon and list added all these hot fix to let it work properly nead to update once release is done
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(checkToRefreshUnreadCount),name:Notification.Name(rawValue: "updateNotificationCount"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateSubscriptionStatus), name: .notificationPackSubscription, object: nil)
    }

    
    
    @objc func onNotification(notification: Notification) {
        kAppDelegate.createHomeTabRootView()
    }
    
    @objc func updateSubscriptionStatus() {
        self.partnerSubscriptionDetail = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
        self.tableView.reloadData()
    }
    
    func conformDelegates() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setUpUI() {
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 400
        registerCells()
    }
    
    private func registerCells() {
        UtilityFunction.shared.registerCell(tableView, "AccountsTableViewCell")
        UtilityFunction.shared.registerCell(tableView, "AccountsHeaderTableViewCell")
    }
    
    func callScreenData() {
        callAccountScreenDataApi()
    }
    
    private func callAccountScreenDataApi() {
        self.getUserDetails {
            self.fetchUserDetails {
                self.getSubscription(false) {
                    self.fetchBalanceDetail {
                        self.removePlaceholderView()
                        self.hideActivityIndicator()
                        self.isTableReloaded = true
                        self.reloadTableView(false)
                    }
                }
            }
        }
    }
    
    
  @objc func checkToRefreshUnreadCount() {
        refreshUnreadCount()
    }
    
    func refreshUnreadCount() {
        var rowIndex = 0
        if shouldShowSwitchAccount {
            rowIndex = 3
        } else {
            rowIndex = 2
        }
        
        if let _ = self.tableView.cellForRow(at: IndexPath.init(row: rowIndex, section: 0)) as? AccountsTableViewCell {
            UtilityFunction.shared.performTaskInMainQueue {
                let indexPath = IndexPath.init(row: rowIndex, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
    
    func fetchUserDetails(_ completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            self.userDetailVM = BAUserDetailViewModal(BAUserDetailRepo())
            if userDetailVM != nil {
                userDetailVM?.getUserDetail(completion: {(flagValue, message, isApiError) in
                    if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else if !flagValue {
                        self.apiError(message, title: "")
                    }
                    completion()
                })
            }
        } else {
            self.reloadTableView(false)
            noInternet()
        }
    }
    
    
    func fetchSubscriptionDetails(_ navigation: Bool) {
        getSubscription(navigation) { }
    }
    
    func getSubscription(_ navigation: Bool, _ completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
//            CustomLoader.shared.showLoader(on: self.view)
            self.subscriptionVM = PackSubscriptionVM()
            if subscriptionVM != nil {
                subscriptionVM?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        self.tableView.isUserInteractionEnabled = true
                    }
                    if flagValue && navigation {
                        CustomLoader.shared.hideLoader()
                        let subscriptionVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.subscription.rawValue, type: MySubscriptionVC.self)
                        subscriptionVC.isFromAccountScreen = true
                        subscriptionVC.subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                        self.partnerSubscriptionDetail = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                        self.navigationController?.pushViewController(subscriptionVC, animated: true)
                    } else if flagValue {
                        self.partnerSubscriptionDetail = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails
                        completion()
                    } else if isApiError {
                        completion()
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        completion()
                        self.apiError("", title: message)
                    }
                })
            }
        } else {
            self.reloadTableView(false)
            noInternet()
        }
    }
    
    func getUserDetails(_ completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
            userProfileVM = BAUserProfileViewModal(repo: BAUserProfileRepo(), endPoint: BAKeychainManager().sId)
            removePlaceholderView()
            showLoaderAnimation()
            if userProfileVM != nil {
                userProfileVM?.baidLookUp(completion: { (flagValue, message, isApiError) in
                    if flagValue {
                        if self.userProfileVM?.baIdList?.count ?? 0 > 1 {
                            self.shouldShowSwitchAccount = true
                        } else {
                            self.shouldShowSwitchAccount = false
                        }
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                    completion()
                })
            }
        } else {
            if self.userProfileVM?.baIdList?.isEmpty ?? true {
                noInternetForSplash() {
                    if BAReachAbility.isConnectedToNetwork() {
                        self.callScreenData()
                    }
                }
            } else {
                self.reloadTableView(false)
                noInternet()
            }
        }
    }
    
    func fetchBalanceDetail(_ completion: @escaping () -> Void) {
        if BAReachAbility.isConnectedToNetwork() {
            self.userBalanceVM = BAUserBalanceViewModal(BAUserBalanceRepo())
            removePlaceholderView()
            if userBalanceVM != nil {
                userBalanceVM?.getCurrentBalance(completion: {(flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                    } else if isApiError {
                    } else {
                        self.apiError(message, title: "")
                    }
                    completion()
                })
            }
        } else {
            hideActivityIndicator()
            noInternet()
        }
    }
    
    
    func reloadTableView(_ toTop: Bool) {
        UtilityFunction.shared.performTaskInMainQueue {
            if toTop {
                self.tableView.reloadData {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    func moveToUserListScreen(userProfileModal: [AccountDetailListResponseData]?) {
        let switchAccountVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "BaIdSelectionVC", type: BaIdSelectionVC.self)
        switchAccountVC.isFromAccount = true
        switchAccountVC.isFromRMN = true
        switchAccountVC.bingeAccountList = userProfileModal!//BAUserDefaultManager().loginDetail?.data?.userDetails?.bingeSubscription
        CATransaction.begin()
        self.navigationController?.pushViewController(switchAccountVC, animated: false)
        CATransaction.setCompletionBlock({
            self.tableView.isUserInteractionEnabled = true
        })
        CATransaction.commit()
    }
}

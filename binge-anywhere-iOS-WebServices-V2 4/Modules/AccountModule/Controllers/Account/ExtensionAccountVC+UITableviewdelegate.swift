//
//  ExtensionAccountVC+UITableviewdelegate.swift
//  BingeAnywhere
//
//  Created by Shivam on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension AccountVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isTableReloaded {
            return shouldShowSwitchAccount ? 6 : 5
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
//            let cell = tableView.dequeueReusableCell(withIdentifier: kAccountsHeaderTableViewCell) as? AccountsHeaderTableViewCell
            let cell = Bundle.main.loadNibNamed(kAccountsHeaderTableViewCell, owner: nil, options: nil)?[0] as? AccountsHeaderTableViewCell
            accountHeaderCell = cell
            cell?.configureUserData(userProfile: userDetailVM?.userDetail, userBalance: userBalanceVM?.currentBalance, subscriptionDetail: self.partnerSubscriptionDetail, isTableReloaded: isTableReloaded)
            cell?.delegate = self
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: kAccountsTableViewCell) as? AccountsTableViewCell
            cell?.configureUI(index: indexPath.row, shouldShowSwitchAccount: shouldShowSwitchAccount)
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.isUserInteractionEnabled = false
		if BAReachAbility.isConnectedToNetwork() {
            accountHeaderCell?.editAliasButton.isSelected = false
            accountHeaderCell?.endEditing(true)
            if shouldShowSwitchAccount {
                switch indexPath.row {
                case 0:
                    return
                case 1:
                    tableView.isUserInteractionEnabled = false
                    fetchSubscriptionDetails(true)
                case 2:
                    tableView.isUserInteractionEnabled = false
                    if self.userProfileVM?.baIdList?.isEmpty ?? true {
                        self.moveToUserListScreen(userProfileModal: [BAKeychainManager().acccountDetail!])
                    } else {
                        self.moveToUserListScreen(userProfileModal: self.userProfileVM?.baIdList)
                    }
                    break
                case 3:
                    tableView.isUserInteractionEnabled = false
                    let notificationsVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.notificationVC.rawValue, type: NotificationVC.self.self)
                    CATransaction.begin()
                    self.navigationController?.pushViewController(notificationsVC, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                /*
                case 4:
                    tableView.isUserInteractionEnabled = false
                    let transactionHistoryVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.transactionHistory.rawValue, type: BATransactionHistoryViewController.self)
                    CATransaction.begin()
                    self.navigationController?.pushViewController(transactionHistoryVC, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
    //             // TODO: Need to be discussed
                 */
                case 4:
                    self.tableView.isUserInteractionEnabled = false
                    let editProfile = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.editProfileVC.rawValue, type: EditProfileVC.self)
                    editProfile.userDetail = userDetailVM?.userDetail
                    editProfile.callback = {
                        self.fetchUserDetails {}
                        self.updateUserProfileImage()
                    }
                    CATransaction.begin()
                    self.navigationController?.pushViewController(editProfile, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                case 5:
                    self.tableView.isUserInteractionEnabled = false
                    let deviceManagement = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.deviceManagementVC.rawValue, type: BADeviceManagementVC.self)
                    CATransaction.begin()
                    self.navigationController?.pushViewController(deviceManagement, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                default:
                    return
                }
            } else {
                switch indexPath.row {
                case 0:
                    return
                case 1:
                    tableView.isUserInteractionEnabled = false
                    fetchSubscriptionDetails(true)
                case 2:
                    tableView.isUserInteractionEnabled = false
                    let notificationsVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.notificationVC.rawValue, type: NotificationVC.self.self)
                    CATransaction.begin()
                    self.navigationController?.pushViewController(notificationsVC, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                /*
                case 3:
                    tableView.isUserInteractionEnabled = false
                    let transactionHistoryVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.transactionHistory.rawValue, type: BATransactionHistoryViewController.self)
                    CATransaction.begin()
                    self.navigationController?.pushViewController(transactionHistoryVC, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                 */
    //             // TODO: Need to be discussed
                case 3:
                    tableView.isUserInteractionEnabled = false
                    let editProfile = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.editProfileVC.rawValue, type: EditProfileVC.self)
                    editProfile.userDetail = userDetailVM?.userDetail
                    editProfile.callback = {
                        self.fetchUserDetails {}
                        self.updateUserProfileImage()
                    }
                    CATransaction.begin()
                    self.navigationController?.pushViewController(editProfile, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                case 4:
                    tableView.isUserInteractionEnabled = false
                    let deviceManagement = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.rawValue, identifierVC: ViewControllers.deviceManagementVC.rawValue, type: BADeviceManagementVC.self)
                    CATransaction.begin()
                    self.navigationController?.pushViewController(deviceManagement, animated: true)
                    CATransaction.setCompletionBlock({
                        self.tableView.isUserInteractionEnabled = true
                    })
                    CATransaction.commit()
                default:
                    return
                }
            }
		} else {
			noInternet()
		}
    }
    
    
    func updateUserProfileImage() {
        if let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? AccountsHeaderTableViewCell {
            UtilityFunction.shared.performTaskInMainQueue {
                if BAKeychainManager().profileImageRelativePath != "" {
                    cell.nameInitials.text = ""
                    let profileImageUrl = (BAConfigManager.shared.configModel?.data?.config?.subscriberImage?.subscriberImageBaseUrl ?? "") + BAKeychainManager().profileImageRelativePath
                    self.userDetailVM?.userDetail?.profileImage = BAKeychainManager().profileImageRelativePath
                    if let url = URL.init(string: profileImageUrl ) {
                        cell.activityIndicator.isHidden = false
                        cell.activityIndicator.startAnimating()
                        cell.userImageView.alpha = 0
                        cell.userImageView.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { _ in
                            UIView.animate(withDuration: 1, animations: {
                                cell.userImageView.alpha = 1
                            })
                            cell.activityIndicator.stopAnimating()
                            cell.activityIndicator.isHidden = true
                        })
                    }
                } else {
                    let randomColor: [UIColor] = [.BAdarkBlueBackground]
                    cell.userImageView.image = nil
                    self.userDetailVM?.userDetail?.profileImage = ""
                    cell.nameInitials.makeCircular(roundBy: .height)
                    let initials = (BAKeychainManager().acccountDetail?.firstName?.trimString(byString: "") ?? " ")
                    let index = initials.index(initials.startIndex, offsetBy: 0)
                    if index <= initials.endIndex {
                        cell.nameInitials.text = String(initials[index]).capitalizingFirstLetter()
                    }
                    UIView.animate(withDuration: 1, animations: {
                        cell.nameInitials.layer.backgroundColor = (randomColor.randomElement() ?? .BANewBlue).cgColor
                    })
                }
            }
        }
    }
}

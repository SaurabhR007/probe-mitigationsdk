//
//  BAEditProfileSuccessViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAEditProfileSuccessViewController: BaseViewController {

    @IBOutlet weak var backToAccountsButton: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        backToAccountsButton.backgroundColor = .BABlueColor
        backToAccountsButton.tintColor = .BAwhite
        backToAccountsButton.makeCircular(roundBy: .height)
        backToAccountsButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "")
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    @objc func backAction() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AccountVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

    @IBAction func backToAccountsButtonAction(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AccountVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

}

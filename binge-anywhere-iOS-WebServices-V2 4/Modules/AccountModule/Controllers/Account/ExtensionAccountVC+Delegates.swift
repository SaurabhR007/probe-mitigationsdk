//
//  ExtensionAccountVC+Delegates.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress

extension AccountVC: AccountsHeaderTableViewCellProtcol {
    
    func showAlert() {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .hidden, .bingeAnywhere, .withMessage("Module Under Development"), .hidden, .hidden)) { _ in }
        }
    }
    
    func aliasMaxReaches() {
        self.view.makeToast("Alias Name Cannot be More than 20 characters.")
    }
    
    func aliasUpdated(_ name: String) {
        aliasModel?.aliasName = name
        if BAReachAbility.isConnectedToNetwork() {
            if let aliasVM = aliasModel {
                self.showActivityIndicator(isUserInteractionEnabled: false)
                aliasVM.addAlias { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.accountHeaderCell?.addAliasTF.resignFirstResponder()
                        self.accountHeaderCell?.addAliasTF.backgroundColor = .clear
                        self.accountHeaderCell?.editAliasButton.isSelected = false
                        self.accountHeaderCell?.addAliasTF.text = name
                        self.accountHeaderCell?.updateAliasInUserProfile(aliasName: name)
                        self.userDetailVM?.userDetail?.aliasName = name
                        self.popupNotification(message ?? "Alias name updated successfully.") 
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func refreshBalance() {
        fetchBalanceDetail {
            if let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? AccountsHeaderTableViewCell {
                cell.priceLabel.set(text: self.userBalanceVM?.currentBalance?.balanceQueryRespDTO?.balance ?? "", animated: true)
                cell.refreshButton.isHidden = false
            }
        }
    }
    
}

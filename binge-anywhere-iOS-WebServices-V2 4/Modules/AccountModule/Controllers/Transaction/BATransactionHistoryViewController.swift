//
//  BATransactionHistoryViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 03/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel

class BATransactionHistoryViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var transactionsTableView: UITableView!
    @IBOutlet weak var subscriberLabel: UILabel!
    var transactionHistoryVM: BATransactionHistoryVM?

    // MARK: - Variables
    var pdfURL: URL!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        conformDelegates()
        congifureUI()
        registerCells()
        fetchTransactionHistory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.transactionHistory.rawValue)
        subscriberLabel.text = "Sub ID: \(BAKeychainManager().sId ) | \(BAKeychainManager().aliasName)"
//        if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.atv.rawValue {
//            subscriberLabel.text = "Sub ID: \(BAKeychainManager().sId ) | \(BAKeychainManager().acccountDetail?.aliasName)Tata Sky Binge ATV"
//        } else if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.binge.rawValue {
//            subscriberLabel.text = "Sub ID: \(BAKeychainManager().sId) | Tata Sky Binge Mobile"
//        } else {
//            subscriberLabel.text = "Sub ID: \(BAKeychainManager().sId) | Tata Sky Binge Fire Stick"
//        }
    }

    // MARK: - Functions
    private func conformDelegates() {
        transactionsTableView.delegate = self
        transactionsTableView.dataSource = self
        transactionsTableView.backgroundColor = .BAdarkBlueBackground
    }

    private func registerCells() {
        UtilityFunction.shared.registerCell(transactionsTableView, kBATransactionHistoryTableViewCell)
    }
    
    func fetchTransactionHistory() {
        transactionHistoryVM = BATransactionHistoryVM(repo: BATransactionHistoryRepo(), endPoint: "")
        configureTransactionHistory()
    }

    private func congifureUI() {
        super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Transaction History")
        self.view.backgroundColor = .BAdarkBlueBackground
        transactionsTableView.rowHeight = UITableView.automaticDimension
        transactionsTableView.estimatedRowHeight = 80
    }
    
    func configureTransactionHistory() {
        if BAReachAbility.isConnectedToNetwork() {
                    showActivityIndicator(isUserInteractionEnabled: false)
                    if transactionHistoryVM != nil {
                        //transactionHistoryVM?.loadMore = isLoadMore
//                        if isLoadMore {
//                            watchlistDataVM?.pagingState = watchlistDataVM?.watchListData?.data?.pagingState ?? ""
//                        } else {
//                            watchlistDataVM?.pagingState = ""
//                        }
                        transactionHistoryVM?.getTransactionHistoryDetail(completion: {(flagValue, message, isApiError) in
                            self.hideActivityIndicator()
                            if flagValue {
                                if self.transactionHistoryVM?.transactionData?.isEmpty ?? true {
//                                    self.transactionsTableView.isHidden = true
                                    self.transactionsTableView.addWatermark(with: "No Transactions Found")
                                    //self.noContentView.isHidden = false
                                } else {
                                    self.transactionsTableView.isHidden = false
                                    //self.noContentView.isHidden = true
                                    self.transactionsTableView.reloadData()
//                                    if !self.isLoadMore {
//                                        self.watchListCollectionView.setContentOffset(.zero, animated: false)
//                                    }
                                }
                            } else if isApiError {
                                self.apiError(message, title: kSomethingWentWrong)
                            } else {
                                self.apiError(message, title: "")
                            }
                        })
                    }
                } else {
                    noInternet()
                }
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Extensions
extension BATransactionHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionHistoryVM?.transactionData?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kBATransactionHistoryTableViewCell) as! BATransactionHistoryTableViewCell
        cell.delegate = self
        cell.backgroundColor = .clear
        cell.configureTransactionHistory(transactionData: transactionHistoryVM?.transactionData?[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BATransactionHistoryViewController: BATransactionHistoryTableViewCellDelegate {
    func downloadTransactiionForSelectedIndex(index: Int?) {
        guard let url = URL(string: "https://www.tutorialspoint.com/swift/swift_tutorial.pdf") else { return }
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
    }
}

extension BATransactionHistoryViewController: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            self.pdfURL = destinationURL
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}

//
//  NotificationViewModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/09/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import MoEngage

class NotificationData {
    var sectionHeaderName = "Recent"
    var rowData = [NotificationViewModel]()
}


class NotificationViewModel: NSObject {
    var sectionHeaderName = "Recent"
    var notificationDisplayMessage = ""
    var notificationDisplayTitle = ""
    var notificationMessage = ""
    var notificationTitle = ""
    var isRead = false
    var mediaType = ""
    var mediaAttachment = ""
    var screenData = [String:AnyObject]()
    var descriptionMessage = ""
    var imageUrl = ""
    var provider = ""
    var contentType = ""
    var contentTitle = ""
    var id = ""
    var screenName = ""
    var pushData = [String:AnyObject]()
    var pushTime = ""
    var pushSeconds = 0
    var isSelected = false
    var isCollapsed = true
    var recentNotification = false
    var date = Date()
    var cid = ""
    
    init(inboxModel: MOInboxModel) {
        super.init()
        isRead = inboxModel.isRead
        //isRead = (notification["isRead"] as? Bool == true)
        
        cid = inboxModel.campaignID
        
        screenData = inboxModel.screenDataDict as! [String : AnyObject]
        
        if let _notificationTitle = inboxModel.notificationTitle {
            notificationDisplayTitle = _notificationTitle
        }
        
        if let notificationMsg = inboxModel.notificationBody {
            notificationDisplayMessage = notificationMsg
        }
        
        if let mediaUrl = inboxModel.notificationMediaURL {
            mediaAttachment = mediaUrl
        }
        
        if let receivedDate = inboxModel.receivedDate {
            pushTime = secondsToHsMs(pushTime: receivedDate)
            date = receivedDate
        }
        
        if let mediaUrl = inboxModel.notificationMediaURL {
            mediaAttachment = mediaUrl
        }
        pushData  = inboxModel.notificationPayloadDict as! [String : AnyObject]
        parsePushDataDic()
    }
    
    func parsePushDataDic() {
        let jsonStr = screenData["screenData"] as? String ?? ""
        //let screenName = pushDict["screenName"]
        let data = Data(jsonStr.utf8)
        do {
            // make sure this JSON is in the format we expect
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                // try to read out a string array
                if let screen_Name = json["screenName"] as? String {
                    self.screenName = screen_Name
                }
                
                if let message = json["message"] as? String {
                    self.notificationMessage = message
                }
                
                if let pushData = json["data"] as? [String: AnyObject] {
                    if let content_Title = pushData["contentTitle"] as? String{
                        self.contentTitle = content_Title
                    }
                    
                    if contentTitle.isEmpty {
                        if let content_Title = pushData["railTitle"] as? String{
                            self.contentTitle = content_Title
                        }
                    }
                    
                    if let id = pushData["id"] as? String{
                        self.id = id
                    }
                    if let content_Type = pushData["contentType"] as? String{
                        self.contentType = content_Type
                    }
                    if let provider = pushData["provider"] as? String{
                        self.provider = provider
                    }
                    if let description = pushData["description"] as? String{
                        self.descriptionMessage = description
                    }
                    if let image = pushData["image"] as? String{
                        self.imageUrl = image
                    }
                }
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
    }
    
    func secondsToHsMs(pushTime : Date) -> String {
        let second = Int(Date().timeIntervalSince(pushTime))
        pushSeconds = second
        recentNotification = true
        if second < 60 {
            return "1m"
//            return "few sec ago"
        }
        
        let minute = second/60
        if minute < 60 {
            return "\(minute)m"
//            return "\(minute) min ago"
        }
        
        let hour = minute/60
        if hour < 24 {
            recentNotification = (hour <= 10) ? true : false
            return "\(hour)hr"
//            return "\(hour) hour ago"
        }
        
        let days = hour/24
        if days < 5 {
            return "\(days)d"
        }
        
        let localDateString = UtilityFunction.shared.formatType(form: "dd/MM/yyyy").string(from: pushTime)
        recentNotification = false
        return localDateString
    }
}

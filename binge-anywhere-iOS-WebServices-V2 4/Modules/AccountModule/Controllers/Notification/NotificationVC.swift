//
//  NotificationVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import MoEngage

class NotificationVC: BaseViewController, MoengageProtocol {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var selectAllButton: CustomButton!
    @IBOutlet weak var selectAllImage: UIImageView!
    @IBOutlet weak var readAllbutton: CustomButton!
    @IBOutlet weak var switchNotificationScreenButton: CustomButton!
    @IBOutlet weak var removeAllButton: CustomButton!
    @IBOutlet weak var optionViewBottomConstraint: NSLayoutConstraint!

    var parentNavigationController: UINavigationController?
    var notificationsViewModelArray = [NotificationViewModel]()
    var callBack : ((Bool) -> Void)?
    var notifcationArray = [NotificationData]()
    var sectionHeaderData = ["Recent", "Earlier"]
    var isEditingEnabled = false
    var isTransactionType = false
    var moInbox = MOInboxViewController()
    var contentDetailVM: BAVODDetailViewModalProtocol?
    var watchlistLookupVM: BAWatchlistLookupViewModal?
    var notificationListingType = ""
    var showCompleteDesc:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureTableUI()
//        NotificationCenter.default.addObserver(self, selector: #selector(setMOInboxObj),name:Notification.Name(rawValue: "updateNotificationList"),object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setMOInboxObj()
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewNotification.rawValue)
        self.tableView.isUserInteractionEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setMOInboxObj() {
        // Uncomment this to integrate native MOInbox View
        // self.moInbox = MOInbox.initializeInbox(on: self)
          fetchNotificationFromMoServer()
    }
    
    func updateReadAllButton() {
        postNotificationToCheckUnreadCount()
        UtilityFunction.shared.performTaskInMainQueue {
            if self.getUnreadMessageCount() == 0 {
                self.readAllbutton.setTitleColor(.BABlueColor, for: .normal)
                self.readAllbutton.isHighlighted = true
                self.readAllbutton.isUserInteractionEnabled = false
            } else {
                self.readAllbutton.setTitleColor(.white, for: .normal)
                self.readAllbutton.isHighlighted = false
                self.readAllbutton.isUserInteractionEnabled = true
            }
        }
    }
    
    // In order to hotfix the notification badge icon and list added all these hot fix to let it work properly nead to update once release is done
    // This list refresh is only getting updated when triggered notification from account view notification cell so triggerd this from there to refresh the list.
    func postNotificationToCheckUnreadCount() {
        NotificationCenter.default.post(Notification(name: Notification.Name("didReceiveNotification")))
    }

    func setupNavigation(_ showEdit: Bool) {
        if showEdit {
            super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Notifications")
        } else {
            super.configureNavigationBar(with: .leftAndRightBar, #selector(backButtonAction), #selector(editButtonAction), self, "Notifications")
        }
    }

    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func editButtonAction() {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.isEditingEnabled = !self.isEditingEnabled
            self.changeRightBarButtonTitle(self.isEditingEnabled ? "Close" : "Edit" )
            self.showHideOptionView(self.isEditingEnabled)
            self.tableView.reloadData()
        }
    }

    func changeRightBarButtonTitle(_ title: String) {
        super.configureNavigationBar(with: .updateRightBarButton, #selector(backButtonAction), #selector(editButtonAction), self, title)
    }
    
    func fetchNotificationFromMoServer() {
        MOInbox.getMessagesWithCompletionBlock { (messages) in
            UtilityFunction.shared.resetAppbadgeCount()
            UtilityFunction.shared.performTaskInMainQueue {
                if let messages = messages {
                    self.notificationsViewModelArray = []
                    self.notifcationArray = []
                    print("this is message \(messages)")
                    self.setupNavigation(messages.isEmpty)
                    if (messages.count > 0) {
                        UtilityFunction.shared.performTaskInMainQueue {
                            self.notificationsViewModelArray = self.getArrayOfViewModelFromInboxModelArray(messages:messages)
                            self.notificationsViewModelArray = self.notificationsViewModelArray.sorted(by: { $0.pushSeconds < $1.pushSeconds })
                            self.createDataModel()
                            self.updateTableView()
                        }
                    } else {
                        self.updateTableView()
                        self.updateReadAllButton()
                    }
                }
            }
        }
    }
    
    func updateTableView() {
        self.tableView.reloadData {
            self.updateReadAllButton()
            self.showHideWaterMark()
        }
    }
    
    func getArrayOfViewModelFromInboxModelArray(messages:[MOInboxModel]) -> [NotificationViewModel] {
        var notifications = [NotificationViewModel]()
        for element in messages {
            notifications.append(NotificationViewModel(inboxModel: element))
        }
        return notifications
    }
    
    func inboxCellSelected(withData dataDict: [AnyHashable : Any]!) {
        print("Data Dict : \(String(describing: dataDict))")
     }

    func inboxCellSelected(withPushDict pushDict: [AnyHashable : Any]!) {
        print("Push Dict : \(String(describing: pushDict))")
    }
    
    func showHideOptionView(_ isShown: Bool) {
        if isShown {
            self.optionViewBottomConstraint.constant = 0
            tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 141, right: 0)
            UIView.animate(withDuration: 1/3, animations: {
                self.view.layoutIfNeeded()
            })
        } else {
            self.selectDeselect(false, false)
			self.selectAllButton.text("Select all")
            self.selectAllButton.isSelected = false
            tableView.contentInset = UIEdgeInsets.zero
            UIView.animate(withDuration: 1/3, animations: {
                self.optionViewBottomConstraint.constant = 141
                self.view.layoutIfNeeded()
            })
        }
    }

    func configureTableUI() {
        tableView.backgroundColor = .BAdarkBlueBackground
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 50.0
        tableView.rowHeight = 100.0
        registerCells()
        configureOptionView()
    }

    func showHideWaterMark() {
        if notifcationArray.isEmpty {
            tableView.addWatermark(with: "No New Notifications")
        } else {
            tableView.addWatermark(with: "")
        }

        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }
    
    func registerCells() {
        UtilityFunction.shared.registerCell(tableView, "BATransactionsTableViewCell")
        UtilityFunction.shared.registerCell(tableView, "NotificationSectionHeaderTableViewCell")
        let headerNib = UINib.init(nibName: "NotificationSectionHeaderTableViewCell", bundle: Bundle.main)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "NotificationSectionHeaderTableViewCell")
    }

    func createDataModel() {
        
        let recentNotification = notificationsViewModelArray.filter { (data) -> Bool in
            data.recentNotification == true
        }
        
        let earlierNotification = notificationsViewModelArray.filter { (data) -> Bool in
            data.recentNotification == false
        }
        
        if recentNotification.count > 0 {
            let _notificationData = NotificationData()
            _notificationData.sectionHeaderName = sectionHeaderData[0]
            _notificationData.rowData.append(contentsOf: recentNotification)
            notifcationArray.append(_notificationData)
        }
        
        if earlierNotification.count > 0 {
            let _notificationData = NotificationData()
            _notificationData.sectionHeaderName = sectionHeaderData[1]
            _notificationData.rowData.append(contentsOf: earlierNotification)
            notifcationArray.append(_notificationData)
        }
    }
    
    @IBAction func selectAllButtonAction(_ sender: CustomButton) {
        if sender.tag == 0 {
            selectAllButton.setTitleColor(.white, for: .normal)
            if !sender.isSelected {
				selectAllButton.text("Unselect all")
            } else {
				selectAllButton.text("Select all")
            }
            sender.isSelected = !sender.isSelected
            self.selectDeselect(sender.isSelected, false)
        } else {
            readAllbutton.setTitleColor(.BABlueColor, for: .normal)
            self.selectDeselect(true, true)
        }
        tableView.reloadData()
    }

    func selectDeselect(_ bool: Bool, _ readActionType: Bool) {
        let _notificationDataObj = notifcationArray.map { (notificationData) -> NotificationData in
            let sectionData = notificationData
            let _rowDataArray = notificationData.rowData.map { (rowData) -> NotificationViewModel in
                let rowData = rowData
                if readActionType == true {
                    rowData.isRead = true
                    MOInbox.markNotificationClicked(withCampaignID: rowData.cid)
                } else {
                    rowData.isSelected = bool
                }
                return rowData
            }
            sectionData.rowData = _rowDataArray
            return sectionData
        }
        self.updateReadAllButton()
        notifcationArray = _notificationDataObj
    }
    
    @IBAction func removeAllButtonAction() {
        if BAReachAbility.isConnectedToNetwork() {
            notifcationArray.forEach { (notificationData) in
                notificationData.rowData.removeAll { (rowData) -> Bool in
                    if rowData.isSelected == true {
                        MOInbox.removeMessage(withCampaignID: rowData.cid)
                    }
                    return rowData.isSelected == true
                }
            }
            notifcationArray.removeAll(where: {$0.rowData.isEmpty})
            self.isEditingEnabled = false
            tableView.reloadData {
                self.showHideOptionView(false)
                self.changeRightBarButtonTitle(self.notifcationArray.isEmpty ? "" : "Edit")
                if self.notifcationArray.isEmpty {
                    MOInbox.removeMessages()
                    self.tableView.addWatermark(with: "No Data Available")
                }
            }
        } else {
            noInternet()
        }
    }

    @IBAction func viewNotificationSettingButtonAction() {
        //switchNotificationScreenButton.setTitleColor(.BABlueColor, for: .normal)
        let notificationSetting: NotificationSettingsVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.more.fileName, identifierVC: ViewControllers.notificationSettings.rawValue, type: NotificationSettingsVC.self)
        self.navigationController?.pushViewController(notificationSetting, animated: true)
    }

    func configureOptionView() {
        selectAllButton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        readAllbutton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        switchNotificationScreenButton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        removeAllButton.fontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BABlueColor)
        optionView.roundCorners([.topLeft, .topRight], radius: 10)
    }
    
    func moveToDetailScreen(vodId: Int?, contentType: String?) {
        fetchContentDetail(vodId: vodId, contentType: contentType)
    }
    
    // MARK: Fetching Content Detail API Call
    func fetchContentDetail(vodId: Int?, contentType: String?) {
        var railContentType = ""
        switch contentType {
        case ContentType.series.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        case ContentType.brand.rawValue:
            railContentType = contentType?.lowercased() ?? ""
        default:
            railContentType = "vod"
        }
        contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: railContentType + "/\(vodId ?? 0)")
        getContentDetailData(vodId: vodId)
    }
    
    // MARK: Content Detail API Calling
    func getContentDetailData(vodId: Int?) {
        if BAReachAbility.isConnectedToNetwork() {
            if let dataModel = contentDetailVM {
                showActivityIndicator(isUserInteractionEnabled: false)
                dataModel.getContentScreenData { (flagValue, message, isApiError) in
                    self.hideActivityIndicator()
                    if flagValue {
                        self.fetchLastWatch(contentDetail: dataModel.contentDetail, vodId: vodId)
//                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
//                        contentDetailVC.parentNavigation = self.navigationController
//                        contentDetailVC.contentDetail = dataModel.contentDetail
//                        contentDetailVC.id = vodId
//                        self.navigationController?.pushViewController(contentDetailVC, animated: true)
                    } else if isApiError {
                        self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        self.apiError(message, title: "")
                    }
                }
            } else {
                self.hideActivityIndicator()
            }
        } else {
            noInternet()
        }
    }
    
    func fetchLastWatch(contentDetail: ContentDetailData?, vodId: Int?) {
        var watchlistVodId: Int = 0
        switch contentDetail?.meta?.contentType {
        case ContentType.series.rawValue:
            watchlistVodId = contentDetail?.meta?.seriesId ?? 0
        case ContentType.brand.rawValue:
            watchlistVodId = contentDetail?.meta?.brandId ?? 0
        default:
            watchlistVodId = contentDetail?.meta?.vodId ?? 0
        }
        fetchWatchlistLookUp(vodId: vodId, contentDetail: contentDetail, contentType: contentDetail?.meta?.contentType ?? "", contentId: watchlistVodId)
    }
    
    func fetchWatchlistLookUp(vodId: Int?, contentDetail: ContentDetailData?, contentType: String, contentId: Int) {
        watchlistLookupVM = BAWatchlistLookupViewModal(repo: BAWatchlistLookupRepo(), endPoint: "last-watch", baId: BAKeychainManager().baId, contentType: contentType, contentId: String(contentId))
        confgureWatchlistIcon(vodId: vodId, contentDetail: contentDetail)
    }
    
    func confgureWatchlistIcon(vodId: Int?, contentDetail: ContentDetailData?) {
        if BAReachAbility.isConnectedToNetwork() {
            showActivityIndicator(isUserInteractionEnabled: false)
            if let dataModel = watchlistLookupVM {
                hideActivityIndicator()
                dataModel.watchlistLookUp {(flagValue, message, isApiError) in
                    if flagValue {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                        contentDetailVC.parentNavigation = self.navigationController
                        contentDetailVC.contentDetail = contentDetail
                        contentDetailVC.contentDetail?.lastWatch = dataModel.watchlistLookUp?.data
                        contentDetailVC.id = vodId
                            self.navigationController?.pushViewController(contentDetailVC, animated: true)
                    } else if isApiError {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                         contentDetailVC.parentNavigation = self.navigationController
                         contentDetailVC.contentDetail = contentDetail
                         contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                         contentDetailVC.id = vodId
                             self.navigationController?.pushViewController(contentDetailVC, animated: true)
                    } else {
                        let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                         contentDetailVC.parentNavigation = self.navigationController
                         contentDetailVC.contentDetail = contentDetail
                         contentDetailVC.contentDetail?.lastWatch = nil//dataModel.watchlistLookUp?.data
                         contentDetailVC.id = vodId
                             self.navigationController?.pushViewController(contentDetailVC, animated: true)
                    }
                }
            }
        } else {
            noInternet()
        }
    }
}

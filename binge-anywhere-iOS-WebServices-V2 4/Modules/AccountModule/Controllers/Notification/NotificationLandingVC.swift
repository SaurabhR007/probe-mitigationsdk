//
//  NotificationLandingVC.swift
//  BingeAnywhere
//
//  Created by Shivam on 15/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import MoEngage

class NotificationLandingVC: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var menuView: UIView!

    // MARK: - Variables
    var _pageMenu: CAPSPageMenu?
    var isViewAppeared = false
    var moInbox = MOInboxViewController()

    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        fetchNotificationFromMoServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.viewNotification.rawValue)
        if let menu = _pageMenu {
            fetchNotificationFromMoServer()
            if isViewAppeared {
                let index = menu.currentPageIndex
                (menu.controllerArray[index] as? NotificationVC)?.viewWillAppear(true)
            }
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    // MARK: - Actions
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func editButtonAction() {
        _pageMenu?.disableTouch = !(_pageMenu?.disableTouch ?? true)
        let title = (_pageMenu?.disableTouch ?? false) ? "Close" : "Edit"
        changeRightBarButtonTitle(title)
        if _pageMenu?.currentPageIndex == 0 {
            (_pageMenu?.controllerArray[0] as? NotificationVC)?.editButtonAction()
            (_pageMenu?.controllerArray[0] as? NotificationVC)?.callBack = { (boolValue) in
                self._pageMenu?.disableTouch = false
                if boolValue {
                    self.changeRightBarButtonTitle("")
                } else {
                    self.changeRightBarButtonTitle("Edit")
                }
            }
        } else {
            (_pageMenu?.controllerArray[1] as? NotificationVC)?.editButtonAction()
            (_pageMenu?.controllerArray[1] as? NotificationVC)?.callBack = { (boolValue) in
                self._pageMenu?.disableTouch = false
                self.changeRightBarButtonTitle("Edit")
            }
        }
    }

    func changeRightBarButtonTitle(_ title: String) {
        super.configureNavigationBar(with: .updateRightBarButton, #selector(backButtonAction), #selector(editButtonAction), self, title)
    }
    
    
    func fetchNotificationFromMoServer() {
        MOInbox.getMessagesWithCompletionBlock { (messages) in
            if let messages = messages{
                self.setupNavigation(messages.isEmpty)
            }
        }
    }
    
    
    func getArrayOfViewModelFromInboxModelArray(messages:[MOInboxModel]) -> [NotificationViewModel] {
        var notifications = [NotificationViewModel]()
        for element in messages {
            notifications.append(NotificationViewModel(inboxModel: element))
        }
        return notifications
    }

    // MARK: - Functions
    private func setupNavigation(_ singleButton: Bool) {
        if singleButton {
            super.configureNavigationBar(with: .backButton, #selector(backButtonAction), nil, self, "Notifications")
        } else {
            super.configureNavigationBar(with: .leftAndRightBar, #selector(backButtonAction), #selector(editButtonAction), self, "Notifications")
        }
        self.configureHomePageView()
    }

    private func configureUI() {
        self.view.backgroundColor = .BAdarkBlueBackground
    }

    func configureHomePageView() {
        var controllerArray: [UIViewController] = []
//        let titleArr = ["Watch", "Transactions"]
        let titleArr = ["Watch"]
        for i in 0..<titleArr.count {
            let notificationsVC: NotificationVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.fileName, identifierVC: ViewControllers.notificationVC.rawValue, type: NotificationVC.self)
            notificationsVC.title = titleArr[i]
            notificationsVC.isTransactionType = (i == 1)
            notificationsVC.parentNavigationController = self.navigationController
            notificationsVC.view.backgroundColor = .BAdarkBlueBackground
            controllerArray.append(notificationsVC)
        }
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .centerMenuItems(true),
            .scrollMenuBackgroundColor(.BAdarkBlueBackground),
            .viewBackgroundColor(.BAdarkBlueBackground),
            .selectionIndicatorColor(.BABlueColor),
            .selectedMenuItemLabelColor(.BAwhite),
            .unselectedMenuItemLabelColor(.BAlightBlueGrey),
            .menuItemFont(UIFont.skyTextFont(.medium, size: 15 * screenScaleFactorForWidth)),
            .menuHeight(30.0),
            .menuMargin(40.0 * screenScaleFactorForWidth),
            .selectionIndicatorHeight(3.0),
           // .menuItemWidthBasedOnTitleTextWidth(true),
//            .equalWidthForTwoSegmentControl(true),
//            .useMenuLikeSegmentedControl(true),
            .addBottomMenuHairline(false),
            .scrollAnimationDurationOnMenuItemTap(150)
        ]
        
        // Initialize scroll menu
        self._pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.menuView.frame.width, height: self.menuView.frame.height), pageMenuOptions: parameters)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.04) {
            self.isViewAppeared = true
        }
        self.menuView.addSubview(self._pageMenu!.view)
    }

}

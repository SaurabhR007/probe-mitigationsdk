//
//  ExtensionNotificationVCDelegates.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

extension NotificationVC: SelectDeselectNotificationDelegate {

    func markUnMarkCells(_ indexPath: IndexPath, _ select: Bool) {
        notifcationArray[indexPath.section].rowData[indexPath.row].isSelected = select
//        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

//
//  ExtensionNotificationVC+UITableViewDelegate.swift
//  BingeAnywhere
//
//  Created by Shivam on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import Mixpanel
import MoEngage

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifcationArray[section].rowData.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return notifcationArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BATransactionsTableViewCell") as? BATransactionsTableViewCell
        cell?.configureUI(notifcationArray[indexPath.section].rowData[indexPath.row], isEditingEnabled, indexPath, isTransactionType, isExpanded: showCompleteDesc)
        cell?.transactionDetaillabel.delegate = self
        cell?.delegate = self
        return cell!
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NotificationSectionHeaderTableViewCell") as? NotificationSectionHeaderTableViewCell
        cell?.configureUI(notifcationArray[section].sectionHeaderName)
        cell?.updateConstraint(isEditingEnabled)
        cell?.clipsToBounds = true
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.notifcationArray[section].rowData.isEmpty {
            return 0.01
        }
        return 46.0*screenScaleFactorForWidth
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if isEditingEnabled {
            return UITableViewCell.EditingStyle.none
        } else {
            return UITableViewCell.EditingStyle.delete
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if BAReachAbility.isConnectedToNetwork() {
                MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.deleteNotifcation.rawValue)
                print("Deleted")
                let itemToDelete = self.notifcationArray[indexPath.section].rowData[indexPath.row]
                self.notifcationArray[indexPath.section].rowData.remove(at: indexPath.row)
                MOInbox.removeMessage(withCampaignID: itemToDelete.cid)
                if notifcationArray[indexPath.section].rowData.isEmpty {
                    notifcationArray.removeAll(where: {$0.rowData.isEmpty})
                    let indexSet = NSMutableIndexSet()
                    indexSet.add(indexPath.section)
                    // Reload section when we are not deleting the section
                    self.tableView.reloadData()
                } else {
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            } else {
                noInternet()
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.isUserInteractionEnabled = false
        UtilityFunction.shared.performTaskAfterDelay(1.0) {
            self.tableView.isUserInteractionEnabled = true
        }
        let data = notifcationArray[indexPath.section].rowData[indexPath.row]
        let screenName = data.screenName
        data.isRead = true
        updateCellBackGround(indexPath)
        if screenName == PushScreenName.detail_Screen.rawValue {
            self.moveToDetailScreen(vodId: Int(data.id), contentType: data.contentType)
        }
    }
    
    func updateCellBackGround(_ indexPath: IndexPath) {
        if let cell = self.tableView.cellForRow(at: indexPath) as? BATransactionsTableViewCell {
            cell.backGroundView.backgroundColor = .clear
            cell.dividerLabel.isHidden = false
            MOInbox.markNotificationClicked(withCampaignID: notifcationArray[indexPath.section].rowData[indexPath.row].cid)
            postNotificationToCheckUnreadCount()
        }
    }
}

extension NotificationVC: ExpandableLabelDelegate {
    func willExpandLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        tableView.reloadRows(at: [pathCell], with: .none)
    }
    
    func didExpandLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        //showCompleteDesc = true
        notifcationArray[pathCell.section].rowData[pathCell.row].isCollapsed = false
        tableView.reloadRows(at: [pathCell], with: .none)
    }
    
    func willCollapseLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        tableView.reloadRows(at: [pathCell], with: .none)
    }
    
    func didCollapseLabel(_ label: ExpandableLabel, pathCell:IndexPath) {
        //showCompleteDesc = false
        notifcationArray[pathCell.section].rowData[pathCell.row].isCollapsed = true
        tableView.reloadRows(at: [pathCell], with: .none)
    }
}

//
//  BASwitchAccountViewController.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 17/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BASwitchAccountViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var switchAccountTableView: UITableView!

    // MARK: - Variables
    var userProfileData: [AccountDetailListResponseData]?
    var switchAccountModel: BASwitchAccountViewModal?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        reCheckVM()
        conformDelegates()
        registerCells()

        for (index, item) in (userProfileData?.enumerated())! where BAKeychainManager().baId == String(userProfileData?[index].baId ?? 0) {
            userProfileData?.remove(at: index)
            userProfileData?.insert(item, at: 0)
        }
    }

    // MARK: - Function
    func configUI() {
        self.view.backgroundColor = .BAdarkBlueBackground
        self.switchAccountTableView.backgroundColor = .BAdarkBlueBackground
        self.configureNavigationBar(with: .backButtonAndTitle, #selector(backAction), nil, self, "Switch Accounts")
    }

    private func reCheckVM() {
        if self.switchAccountModel == nil {
            self.switchAccountModel = BASwitchAccountViewModal(repo: BASwitchAccountRepo())
        }
    }

    func conformDelegates() {
        switchAccountTableView.dataSource = self
        switchAccountTableView.delegate = self
    }

    func registerCells() {
        UtilityFunction.shared.registerCell(switchAccountTableView, kBASwitchAccountTableViewCell)
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

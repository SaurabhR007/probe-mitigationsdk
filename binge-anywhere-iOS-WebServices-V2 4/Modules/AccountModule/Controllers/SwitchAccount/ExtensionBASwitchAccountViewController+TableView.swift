//
//  ExtensionBASwitchAccountViewController+TableView.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 16/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Mixpanel
import ARSLineProgress

extension BASwitchAccountViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userProfileData?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kBASwitchAccountTableViewCell) as! BASwitchAccountTableViewCell
        cell.delegate = self
        cell.configureCell(data: (userProfileData?[indexPath.row])!, firstIndex: indexPath.row)
        cell.separatorLabel.isHidden = indexPath.row == 0 ? false : true
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 80 : 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.dismissController(index: indexPath.row)
        } else if BAReachAbility.isConnectedToNetwork() {
                if let switchAccountVM = switchAccountModel {
                    showActivityIndicator(isUserInteractionEnabled: false)
                    if self.userProfileData?[indexPath.row].baId == nil {
                        BAKeychainManager().targetBaId = "0"
                        BAKeychainManager().dsn = self.userProfileData?[indexPath.row].deviceSerialNumber ?? "0"
                    } else {
                        BAKeychainManager().targetBaId = String(self.userProfileData?[indexPath.row].baId ?? 0)
                        BAKeychainManager().dsn = "0"
                    }
                    switchAccountVM.switchAccount(completion: { (flag, message, isApiError) in
						self.hideActivityIndicator()
                        if flag {
                            BAKeychainManager().baId = BAKeychainManager().targetBaId
                            BAKeychainManager().customerIndex = "\(indexPath.row)"
                            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.switchProfile.rawValue)
                            self.dismissController(index: indexPath.row)
                        } else if isApiError {
                            self.apiError(message ?? "Failed to select profile", title: kSomethingWentWrong)
                        } else {
                            self.apiError(message ?? "Failed to select profile", title: "")
                        }
                    })
                }
		} else {
				noInternet()
        }
    }
}

extension BASwitchAccountViewController: BASwitchAccountTableViewCellProtocol {
    func dismissController(index: Int?) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AccountVC.self) {
                var userProfile = [String: String]()
                let _index = index ?? 0
                userProfile["name"] = userProfileData?[_index].aliasName ?? ""
                userProfile["aliasName"] = userProfileData?[_index].aliasName ?? ""
                userProfile["baId"] = String(userProfileData?[_index].baId ?? 0)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfile"), object: nil, userInfo: userProfile)
                navigateToHome()
                //self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func navigateToHome() {
        kAppDelegate.createHomeTabRootView()
    }
}

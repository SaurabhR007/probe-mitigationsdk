//
//  EditProfileVM.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol EditProfileVMProtocol {
    func uploadImage(_ imageData: [UIImage], completion: @escaping (Bool, String, Bool) -> Void)
    func editProfile(completion: @escaping(Bool, String, Bool) -> Void)
    func deleteProfileImage(completion: @escaping(Bool, String, Bool) -> Void)
}

class EditProfileVM: EditProfileVMProtocol {

    var repo: EditProfileRepo
    var requestToken: ServiceCancellable?
    var dataModel: DeleteUseProfileImageModel?

    init(_ repo: EditProfileRepo) {
        self.repo = repo
    }

    func uploadImage(_ imageData: [UIImage], completion: @escaping (Bool, String, Bool) -> Void) {
        let param = [String: Any]()
        let baseUrl = UtilityFunction.shared.isUatBuild() ? APIEnvironment.uat.rawValue : APIEnvironment.prod.rawValue
		let urlString = baseUrl + "binge-mobile-services/api/v2/subscribers/" + String(BAKeychainManager().sId) + "/upload/image?profileId=" + String(BAKeychainManager().acccountDetail?.defaultProfile ?? "")
        repo.callSendImageAPI(param: param, arrImage: imageData, imageKey: "image", URlName: urlString, withblock: { (parsedData) in
            print(parsedData as AnyObject)
            if parsedData != nil {
                let statusCode = parsedData?["code"] as? NSInteger ?? -1
                if statusCode == 0 {
                    if let relativePath = parsedData?["data"] as? [String: Any] {
                        BAKeychainManager().profileImageRelativePath = relativePath["relativePath"] as? String ?? ""
                    }
                    if let message = parsedData?["message"] as? String {
                        print(message)
                        completion(true, message, false)
                    }
                } else {
                    if let message = parsedData?["message"] as? String {
                        print(message)
                        completion(false, message, false)
                    }
                }
            } else {
                completion(false, "Unable to update profile image", true)
            }
        })
    }

    func editProfile(completion: @escaping(Bool, String, Bool) -> Void) {
        requestToken = repo.updateUserDetails(completion: {(configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func deleteProfileImage(completion: @escaping(Bool, String, Bool) -> Void) {
        requestToken = repo.deleteUserImage(completion: {(configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.dataModel = _configData.parsed
                        BAKeychainManager().profileImageRelativePath = ""
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

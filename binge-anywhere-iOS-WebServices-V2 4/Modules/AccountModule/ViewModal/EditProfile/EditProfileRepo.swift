//
//  EditProfileRepo.swift
//  BingeAnywhere
//
//  Created by Shivam on 16/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

protocol EditProfileRepo {
    func callSendImageAPI(param: [String: Any], arrImage: [UIImage], imageKey: String, URlName: String/*, controller: UIViewController*/, withblock: @escaping (_ response: AnyObject?) -> Void)
    func updateUserDetails(completion: @escaping (APIServicResult<BAEditProfileModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
    func deleteUserImage(completion: @escaping (APIServicResult<DeleteUseProfileImageModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

class EditProfileDataRepo: EditProfileRepo {

    func callSendImageAPI(param: [String: Any], arrImage: [UIImage], imageKey: String, URlName: String, withblock: @escaping (_ response: AnyObject?) -> Void) {
        var headers: HTTPHeaders
        let dafaultHeaderVal = BAKeychainManager().sId
		headers = ["x-authenticated-userid": dafaultHeaderVal, "authorization": BAKeychainManager().userAccessToken, "subscriberId" : BAKeychainManager().sId]
        headers["locale"] = "en"
        let url = URL(string: URlName)

        AF.upload(multipartFormData: { (multipartFormData) in
            for img in arrImage {
                guard let imgData = img.jpegData(compressionQuality: 0.3) else { return }
                multipartFormData.append(imgData, withName: "image", fileName: "image" + ".jpeg", mimeType: "image/jpeg")
            }
        }, to: url!, usingThreshold: UInt64.init(),
           method: .post,
           headers: headers).response { response in
            print("response is ----->", response)
            do {
                if let jsonData = response.data {
                    let parsedData = try JSONSerialization.jsonObject(with: jsonData) as? [String: AnyObject]
                    withblock(parsedData as AnyObject)
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                withblock(nil)
            }
        }
    }

    func updateUserDetails(completion: @escaping (APIServicResult<BAEditProfileModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var apiParams = APIParams()
		apiParams["subscriberId"] = BAKeychainManager().sId
        if let email = EditProfileDataSource.user?.email.trimString(byString: "") {
            if email.isValidEmail() {
                apiParams["emailId"] = email
                BAKeychainManager().acccountDetail?.emailId = email
            }
        }
        var target = ServiceRequestDetail.init(endpoint: .editProfile(param: apiParams, headers: nil))
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        return APIService().request(target) { (response: APIResult<APIServicResult<BAEditProfileModal>, ServiceProviderError>) in
            completion(response.value, response.error)
            print(apiParams)
        }
    }
    
    func deleteUserImage(completion: @escaping (APIServicResult<DeleteUseProfileImageModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		var headers: APIHeaders
		let dafaultHeaderVal = BAKeychainManager().sId
        headers = ["x-authenticated-userid": dafaultHeaderVal, "authorization": BAKeychainManager().userAccessToken, "subscriberId" : BAKeychainManager().sId]
		headers["locale"] = "en"
        var apiParams = APIParams()
        apiParams["profileId"] = (BAKeychainManager().acccountDetail?.defaultProfile ?? "")
        let target = ServiceRequestDetail.init(endpoint: .deleteProfileImage(param: [:], headers: headers, postQueryParameters: apiParams))

        return APIService().request(target) { (response: APIResult<APIServicResult<DeleteUseProfileImageModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}


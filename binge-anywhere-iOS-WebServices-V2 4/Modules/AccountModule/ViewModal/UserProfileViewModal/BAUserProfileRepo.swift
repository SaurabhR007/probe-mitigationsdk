//
//  BAUserProfileRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 19/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAUserProfileScreenRepo {
    var apiEndPoint: String {get set}
	func baidLookUpApiCall(completion: @escaping(APIServicResult<BAUserProfileModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAUserProfileRepo: BAUserProfileScreenRepo {
    var apiEndPoint: String = ""

    func baidLookUpApiCall(completion: @escaping(APIServicResult<BAUserProfileModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let header: APIHeaders = ["authorization" : BAKeychainManager().userAccessToken, "locale" : "en", "subscriberId" : BAKeychainManager().sId, "platform" : "BINGE"]
		let target = ServiceRequestDetail.init(endpoint: .userListing(headers: header, endUrlString: apiEndPoint))
		return APIService().request(target) { (response: APIResult<APIServicResult<BAUserProfileModal>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}

}

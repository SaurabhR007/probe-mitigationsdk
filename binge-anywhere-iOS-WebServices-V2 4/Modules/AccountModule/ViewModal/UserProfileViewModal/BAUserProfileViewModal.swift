//
//  BAUserProfileViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 19/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAUserProfileViewModalProtocol {
	func baidLookUp (completion: @escaping (Bool, String?, Bool) -> Void)
}

class BAUserProfileViewModal: BAUserProfileViewModalProtocol {

    var requestToken: ServiceCancellable?
    var repo: BAUserProfileScreenRepo
    var endPoint: String = ""
	var baIdList: [AccountDetailListResponseData]?

    init(repo: BAUserProfileScreenRepo, endPoint: String) {
        self.repo = repo
        self.repo.apiEndPoint = endPoint
    }
	
	func baidLookUp (completion: @escaping (Bool, String?, Bool) -> Void) {
		repo.apiEndPoint = BAKeychainManager().sId
		requestToken = repo.baidLookUpApiCall(completion: { (configData, error)  in
			self.requestToken = nil
			if let _configData = configData {
				if let statusCode = _configData.parsed.code {
					if statusCode == 0 {
						if let baidData = _configData.parsed.data {
							self.baIdList = baidData.accountDetailList
							completion(true, nil, false)
						} else {
							completion(false, "Invalid data", false)
						}
					} else {
						if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
					}
				} else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
					completion(false, "Some Error Occurred", true)
				}
			} else if let error = error {
				completion(false, error.error.localizedDescription, true)
			} else {
				completion(false, "Some Error Occurred", true)
			}
		})
	}
}

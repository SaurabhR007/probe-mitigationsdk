//
//  BATransactionHistoryRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
protocol BATransactionHistoryScreenRepo {
    //var apiEndPoint: String {get set}
    func getTransactionHistoryDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BATransactionHistoryModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BATransactionHistoryRepo: BATransactionHistoryScreenRepo {
    //var apiEndPoint: String = ""
    func getTransactionHistoryDetail(apiParams: APIParams, completion: @escaping(APIServicResult<BATransactionHistoryModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiHeader: APIHeaders = ["Content-Type": "application/json", "authorization": "bearer \(BAKeychainManager().userAccessToken)", "platform": "BINGE_ANYWHERE"]
        let target = ServiceRequestDetail.init(endpoint: .transactionHistory(param: apiParams, headers: apiHeader, endUrlString: "/\(BAKeychainManager().sId)/transaction/history"))
        return APIService().request(target) { (response: APIResult<APIServicResult<BATransactionHistoryModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

//
//  BATransactionHistoryVM.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
protocol BATransactionHistoryVMProtocol {
    var transactionData: [TransactionData]? { get set }
    func getTransactionHistoryDetail(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BATransactionHistoryVM: BATransactionHistoryVMProtocol {
    var requestToken: ServiceCancellable?
    var repo: BATransactionHistoryRepo
    var endPoint: String = ""
    var transactionData: [TransactionData]?

    init(repo: BATransactionHistoryRepo, endPoint: String) {
        self.repo = repo
    }

    func getTransactionHistoryDetail(completion: @escaping(Bool, String?, Bool) -> Void) {
        var apiParams = APIParams()
        if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.atv.rawValue || BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.stick.rawValue {
            apiParams["dsn"] = BAKeychainManager().acccountDetail?.deviceSerialNumber
            apiParams["baId"] = ""
        } else {
            apiParams["baId"] = BAKeychainManager().baId
            apiParams["dsn"] = ""
        }
        apiParams["offSet"] = "string"
        apiParams["limit"] = "string"
        requestToken = repo.getTransactionHistoryDetail(apiParams: apiParams, completion: { (configData, error) in
            self.requestToken = nil
             if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        self.transactionData = _configData.parsed.data
                        completion(true, nil, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

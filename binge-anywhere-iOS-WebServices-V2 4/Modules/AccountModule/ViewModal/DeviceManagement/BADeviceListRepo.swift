//
//  BADeviceListRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

protocol BADeviceListScreenRepo {
    var apiEndPoint: String {get set}
    func getDeviceList(completion: @escaping(APIServicResult<BADeviceListModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BADeviceListRepo: BADeviceListScreenRepo {
    var apiEndPoint: String = ""
    func getDeviceList(completion: @escaping(APIServicResult<BADeviceListModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiParams = APIParams()
        var target = ServiceRequestDetail.init(endpoint: .getRegisteredDevices(param: apiParams, headers: nil, endUrlString: apiEndPoint))
		target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
		target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["deviceId"] = kDeviceId
        return APIService().request(target) { (response: APIResult<APIServicResult<BADeviceListModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

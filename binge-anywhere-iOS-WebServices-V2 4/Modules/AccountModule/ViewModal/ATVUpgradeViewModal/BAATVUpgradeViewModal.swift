//
//  BAATVUpgradeViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 16/04/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
protocol BAATVUpgradeViewModalProtocol {
    func switchUser(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAATVUpgradeViewModal: BAATVUpgradeViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAATVUpgradeRepoProtocol?

    init(repo: BAATVUpgradeRepoProtocol) {
        self.repo = repo
    }

    func switchUser(completion: @escaping(Bool, String?, Bool) -> Void) {
        repo?.apiEndPoint = BAKeychainManager().baId
        requestToken = repo?.switchUser(completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        BAKeychainManager().baId = _configData.parsed.data?.baId ?? ""
                       // BAKeychainManager().aliasName = _configData.parsed.data.b
                        completion(true, _configData.parsed.message, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

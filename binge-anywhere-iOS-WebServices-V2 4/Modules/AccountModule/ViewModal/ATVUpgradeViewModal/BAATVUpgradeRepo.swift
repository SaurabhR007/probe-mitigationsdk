//
//  BAATVUpgradeRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 16/04/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

protocol BAATVUpgradeRepoProtocol {
    var apiEndPoint: String {get set}
    func switchUser(completion: @escaping (APIServicResult<BASwitchAccountModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAATVUpgradeRepo: BAATVUpgradeRepoProtocol {
    var apiEndPoint: String = BAKeychainManager().baId
    func switchUser(completion: @escaping (APIServicResult<BASwitchAccountModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiParams = APIParams()
        var target = ServiceRequestDetail.init(endpoint: .atvUpgrade(param: apiParams, headers: nil, endUrlString: apiEndPoint))
        target.detail.headers["locale"] = "en"
        target.detail.headers["platform"] = "ios"
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["deviceId"] = kDeviceId
        return APIService().request(target) { (response: APIResult<APIServicResult<BASwitchAccountModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

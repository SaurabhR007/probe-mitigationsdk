//
//  BAAddAliasRepo.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAAddAliasScreenRepoProtocol {
    var apiEndPoint: String {get set}
    func addAlias(alias: String?, completion: @escaping (APIServicResult<BAAddAliasModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAAddAliasRepo: BAAddAliasScreenRepoProtocol {
    var apiEndPoint: String = ""
    func addAlias(alias: String?, completion: @escaping (APIServicResult<BAAddAliasModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        let apiParams = APIParams()
        var postQueryParam: [AnyHashable: Any]?
        if let aliasName = alias {
            postQueryParam = ["aliasName": aliasName]
        }
        var target = ServiceRequestDetail.init(endpoint: .addAlias(param: apiParams, headers: nil, endUrlString: apiEndPoint, postQueryParameters: postQueryParam))
        target.detail.headers["x-authenticated-userid"] = BAKeychainManager().sId
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
		target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["deviceName"] = kDeviceModal
        target.detail.headers["platform"] = "IOS"
        target.detail.headers["deviceId"] = kDeviceId
        target.detail.headers["locale"] = "en"
        return APIService().request(target) { (response: APIResult<APIServicResult<BAAddAliasModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

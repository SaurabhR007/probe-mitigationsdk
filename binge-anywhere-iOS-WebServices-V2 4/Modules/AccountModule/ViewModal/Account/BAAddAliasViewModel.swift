//
//  BAAddAliasModel.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAAliasNameViewModalProtocol {
    func addAlias(completion: @escaping(Bool, String?, Bool) -> Void)
}

class BAAliasNameViewModal: BAAliasNameViewModalProtocol {
    var requestToken: ServiceCancellable?
    var repo: BAAddAliasScreenRepoProtocol?
    var aliasName: String?

    init(repo: BAAddAliasScreenRepoProtocol) {
        self.repo = repo
    }

    func addAlias(completion: @escaping(Bool, String?, Bool) -> Void) {
        repo?.apiEndPoint = BAKeychainManager().baId + "/update/alias"
        requestToken = repo?.addAlias(alias: aliasName, completion: { (configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code {
                    if statusCode == 0 {
                        completion(true, _configData.parsed.message, false)
                    } else {
                        if _configData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _configData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }

}

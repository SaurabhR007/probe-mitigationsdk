//
//  BAUserBalanceRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAUserBalanceScreenRepo {
    func getCurrentBalance(completion: @escaping(APIServicResult<BAUsedBalanceModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAUserBalanceRepo: BAUserBalanceScreenRepo {
    func getCurrentBalance(completion: @escaping (APIServicResult<BAUsedBalanceModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var apiParams = [AnyHashable: Any]()
        apiParams["baId"] = BAKeychainManager().baId
        var target = ServiceRequestDetail.init(endpoint: .fetchUserBalance(param: apiParams, headers: nil, endUrlString: BAKeychainManager().sId))
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        target.detail.headers["x-authenticated-userid"] = BAKeychainManager().sId
        target.detail.headers["locale"] = "en"
        return APIService().request(target) { (response: APIResult<APIServicResult<BAUsedBalanceModal>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

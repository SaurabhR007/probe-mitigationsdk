//
//  BAUserDetailViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 26/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol BAUserDetailViewModalProtocol {
    func getUserDetail(completion: @escaping(Bool, String, Bool) -> Void)
}

class BAUserDetailViewModal: BAUserDetailViewModalProtocol {

    var repo: BAUserDetailScreenRepo
    var requestToken: ServiceCancellable?
    var userDetail: UserDetailData?

    init(_ repo: BAUserDetailScreenRepo) {
        self.repo = repo
    }

    func getUserDetail(completion: @escaping(Bool, String, Bool) -> Void) {
        requestToken = repo.getUserDetail(completion: {(configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.userDetail = _configData.parsed.data
                        BAKeychainManager().isAutoPlayOn = self.userDetail?.autoPlayTrailer ?? true
                        BAKeychainManager().mobileNumber = self.userDetail?.rmn ?? ""
                        BAKeychainManager().acccountDetail?.emailId = self.userDetail?.email ?? ""
                        
                        MixpanelManager.shared.updateEmail(self.userDetail?.email ?? "")
                        
                        MoengageManager.shared.setUserAttributes()
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

//
//  BAUserBalanceViewModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol BAUserBalanceViewModalProtocol {
    func getCurrentBalance(completion: @escaping(Bool, String, Bool) -> Void)
}

class BAUserBalanceViewModal: BAUserBalanceViewModalProtocol {

    var repo: BAUserBalanceScreenRepo
    var requestToken: ServiceCancellable?
    var currentBalance: BalanceData?

    init(_ repo: BAUserBalanceScreenRepo) {
        self.repo = repo
    }

    func getCurrentBalance(completion: @escaping(Bool, String, Bool) -> Void) {
        requestToken = repo.getCurrentBalance(completion: {(configData, error) in
            self.requestToken = nil
            if let _configData = configData {
                if let statusCode = _configData.parsed.code, let message = _configData.parsed.message {
                    if statusCode == 0 {
                        self.currentBalance = _configData.parsed.data
                        completion(true, message, false)
                    } else {
                        completion(false, message, false)
                    }
                } else if _configData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
}

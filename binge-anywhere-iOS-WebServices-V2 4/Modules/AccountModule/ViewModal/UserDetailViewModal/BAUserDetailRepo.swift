//
//  BAUserDetailRepo.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 26/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol BAUserDetailScreenRepo {
    func getUserDetail(completion: @escaping(APIServicResult<BAUserDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

struct BAUserDetailRepo: BAUserDetailScreenRepo {
	func getUserDetail(completion: @escaping (APIServicResult<BAUserDetailModal>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
		let header: APIHeaders = ["baId" : BAKeychainManager().baId,
								  "subscriberId" : BAKeychainManager().sId,
								  "authorization" : BAKeychainManager().acccountDetail?.userAuthenticateToken ?? "",
                                  "deviceId": kDeviceId,
								  "locale" : "en"]
		let target = ServiceRequestDetail.init(endpoint: .fetchUserDetail(headers: header, endUrlString: BAKeychainManager().baId))
		return APIService().request(target) { (response: APIResult<APIServicResult<BAUserDetailModal>, ServiceProviderError>) in
			completion(response.value, response.error)
		}
	}
}

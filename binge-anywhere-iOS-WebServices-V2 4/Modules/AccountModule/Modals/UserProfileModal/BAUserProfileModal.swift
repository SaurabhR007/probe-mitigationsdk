//
//  BAUserProfileModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 19/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

struct BAUserProfileModal: Codable {
    let code: Int?
    let message: String?
    var data: SubscriberDetailResponseData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SubscriberDetailResponseData.self, forKey: .data)
    }
}

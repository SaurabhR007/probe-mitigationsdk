//
//  BADeviceListModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BADeviceListModal: Codable {
    let code: Int?
    let message: String?
    let data: DeviceListData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DeviceListData.self, forKey: .data)
    }
}

struct DeviceList: Codable {
    let deviceNumber: String?
    let baId: String?
    let deviceType: String?
    let deviceStatus: String?
    let deviceName: String?
    let primary: Bool?
    let subscriptionId: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        deviceNumber = try values.decodeIfPresent(String.self, forKey: .deviceNumber)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        deviceType = try values.decodeIfPresent(String.self, forKey: .deviceType)
        deviceStatus = try values.decodeIfPresent(String.self, forKey: .deviceStatus)
        deviceName = try values.decodeIfPresent(String.self, forKey: .deviceName)
        primary = try values.decodeIfPresent(Bool.self, forKey: .primary)
        subscriptionId = try values.decodeIfPresent(String.self, forKey: .subscriptionId)
    }
}

struct DeviceListData: Codable {
    let baId: String?
    let deviceList: [DeviceList]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        deviceList = try values.decodeIfPresent([DeviceList].self, forKey: .deviceList)
    }
}

//
//  BARemoveDeviceModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct RemoveDeviceModel: Codable {
    var code: Int?
    var message: String?
    var data: RemoveDevice?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(RemoveDevice.self, forKey: .data)
    }
}

struct RemoveDevice: Codable {
    var deviceId: String?
    var baId: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        deviceId = try values.decodeIfPresent(String.self, forKey: .deviceId)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
    }
}

//
//  BATransactionHistoryModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

struct BATransactionHistoryModal: Codable {
    let code: Int?
    let message: String?
    let data: [TransactionData]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([TransactionData].self, forKey: .data)
    }
}

struct TransactionData: Codable {
    let transactionId: String?
    let amount: String?
    let description: String?
    let date: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        transactionId = try values.decodeIfPresent(String.self, forKey: .transactionId)
        amount = try values.decodeIfPresent(String.self, forKey: .amount)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        date = try values.decodeIfPresent(String.self, forKey: .date)
    }

}

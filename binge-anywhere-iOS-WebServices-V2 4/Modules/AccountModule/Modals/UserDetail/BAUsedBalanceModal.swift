//
//  BAUsedBalanceModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 24/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
struct BAUsedBalanceModal: Codable {
    let code: Int?
    let message: String?
    let data: BalanceData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(BalanceData.self, forKey: .data)
    }
}

struct BalanceData: Codable {
    let daysLeft: String?
    let proRataAmount: String?
    let balanceQueryRespDTO: BalanceQueryRespDTO?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        daysLeft = try values.decodeIfPresent(String.self, forKey: .daysLeft)
        proRataAmount = try values.decodeIfPresent(String.self, forKey: .proRataAmount)
        balanceQueryRespDTO = try values.decodeIfPresent(BalanceQueryRespDTO.self, forKey: .balanceQueryRespDTO)
    }
}

struct BalanceQueryRespDTO: Codable {
    let subscriberId: String?
    let balance: String?
    let endDate: String?
    let dbr: String?
    let mbr: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subscriberId = try values.decodeIfPresent(String.self, forKey: .subscriberId)
        balance = try values.decodeIfPresent(String.self, forKey: .balance)
        endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
        dbr = try values.decodeIfPresent(String.self, forKey: .dbr)
        mbr = try values.decodeIfPresent(String.self, forKey: .mbr)
    }
}

//
//  BAUserDetailModal.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 26/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAUserDetailModal: Codable {
    let code: Int?
    let message: String?
    let data: UserDetailData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(UserDetailData.self, forKey: .data)
    }
}

struct UserDetailData: Codable {
    let baId: String?
    let firstName: String?
    let lastName: String?
    let email: String?
    let rmn: String?
    let profileId: String?
    var aliasName: String?
    var profileImage: String?
    let autoPlayTrailer: Bool?
    let watchNotification: Bool?
    let transactionalNotification: Bool?
    let languageList: [BALanguageModel]?
    let contentList: [String]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
        profileId = try values.decodeIfPresent(String.self, forKey: .profileId)
        aliasName = try values.decodeIfPresent(String.self, forKey: .aliasName)
        profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
        autoPlayTrailer = try values.decodeIfPresent(Bool.self, forKey: .autoPlayTrailer)
        watchNotification = try values.decodeIfPresent(Bool.self, forKey: .watchNotification)
        transactionalNotification = try values.decodeIfPresent(Bool.self, forKey: .transactionalNotification)
        languageList = try values.decodeIfPresent([BALanguageModel].self, forKey: .languageList)
        contentList = try values.decodeIfPresent([String].self, forKey: .contentList)
    }
}

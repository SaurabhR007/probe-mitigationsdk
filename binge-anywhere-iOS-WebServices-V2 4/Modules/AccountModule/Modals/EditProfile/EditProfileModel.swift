//
//  EditProfileModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit

enum EditProfileDataSource {
    case firstName
    case profileImage
    //case lastName
    case emailId
    case phoneNumber
    case updatePassword
    case nextButton
    case cancelButton

    static let numberOfCells = [profileImage, firstName, /*lastName,*/ emailId, phoneNumber, updatePassword, nextButton, cancelButton]

    static var user: EditProfileDataModel?
    var user: EditProfileDataModel? {
        get {return EditProfileDataSource.user}
        set {EditProfileDataSource.user = newValue}
    }

    var labelTitle: String {
        switch self {
        case .firstName:
            return "Name"
//        case .lastName:
//            return "Last Name"
        case .phoneNumber:
            return "Mobile Number"
        case .emailId:
            return "Email ID"
        case .nextButton:
            return "Next"
        case .cancelButton:
            return "Cancel"
        default:
            return ""
        }
    }

    var textValue: String {
        switch self {
        case .firstName:
            let firstName = user?.firstName ?? ""
            let lastName = user?.lastName ?? ""
            return firstName + " " + lastName
//        case .lastName:
//            return user?.lastName ?? ""
        case .emailId:
            return user?.email ?? ""
        case .phoneNumber:
            return user?.phoneNumber ?? ""
        default:
            return ""
        }
    }

    var keyBoardReturnType: UIReturnKeyType {
        switch self {
        case .emailId:
            return .done
        default:
            return .next
        }
    }

    var keyBoardType: UIKeyboardType {
        switch self {
        case .emailId:
            return .emailAddress
        default:
            return .default
        }
    }
}

class EditProfileDataModel {
    var profileImage = UIImage()
    var profileImageURL = ""
    var firstName = ""
    var lastName = ""
    var phoneNumber = ""
    var email = ""
    var password = ""
    var verifyPassword = ""
}

struct BAEditProfileModal: Codable {
    let code: Int?
    let message: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}

struct DeleteUseProfileImageModel: Codable {
    let code: Int?
    let message: String?
    let data: DeletedProfileMessage?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DeletedProfileMessage.self, forKey: .data)
    }
}

struct DeletedProfileMessage: Codable {
    let message: String?
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}


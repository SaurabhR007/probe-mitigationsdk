//
//  BAAddAliasModal.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 14/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct BAAddAliasModal: Codable {
    let code: Int?
    let message: String?
    let data: AliasNameData?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AliasNameData.self, forKey: .data)
    }
}

struct AliasNameData: Codable {
    let baId: String?
    let aliasName: String?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        aliasName = try values.decodeIfPresent(String.self, forKey: .aliasName)
    }
}

//
//  SubscriptionProviderCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 13/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class SubscriptionProviderCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var providerImageView: UIImageView!
	
	override func awakeFromNib() {
        super.awakeFromNib()

	}

	private func configureUI() {
		providerImageView.backgroundColor = .BAdarkBlue2Color
	}
	
	func configureCell(_ content: PartnerProvider?) {
		let url = URL.init(string: content?.iconUrl?.replacingOccurrences(of: "\n", with: "") ?? "")
		providerImageView.alpha = 0
		if let _url = url {
			providerImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
				UIView.animate(withDuration: 1, animations: {
					self.providerImageView.alpha = 1
				})
			})
		}
	}
    
    func configureCellForPrime(_ content: PrimePackDetails?) {
        let url = URL.init(string: content?.imageUrl ?? "")
        providerImageView.alpha = 0
        if let _url = url {
            providerImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
                UIView.animate(withDuration: 1, animations: {
                    self.providerImageView.alpha = 1
                })
            })
        }
    }
}

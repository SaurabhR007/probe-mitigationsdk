//
//  PrimSubscriptionTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class PrimSubscriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var primeLogo: UIImageView!
    @IBOutlet weak var priceLabel: CustomLabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var expiryLabel: CustomLabel!
    @IBOutlet weak var messageLabel: CustomLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.autoresizingMask = .flexibleHeight
        configureUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        containerView.backgroundColor = .BAdarkBlue2Color
        priceLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        expiryLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        messageLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
    }
    
    func configureCellForPrime(data: PartnerSubscriptionDetails) {
        if let _data = data.primePackDetails {
            primeLogo.isHidden = false
            configureImageForPrime(_data)
            let priceValue = _data.packAmount?.clean
            priceLabel.text = (priceValue ?? "") + " per month"
            messageLabel.text = "Amazon Prime through Tata Sky"
            if UtilityFunction.shared.isValidPrimeSubscription(_data) {
                expiryLabel.text = _data.subscriptionExpiryMessage //"Already Expired" //
            }
        }
    }
    
    func configureImageForPrime(_ content: PrimePackDetails?) {
        if let url = content?.imageUrl {
            let imageURL = UtilityFunction.shared.createCloudinaryImageUrlforSameRatio(url: url, width: 180*Int(screenScaleFactorForWidth), height: 30, trim: (url.contains("transparent") ))
            let url = URL.init(string: imageURL ?? "")
            if let _url = url {
                primeLogo.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
                })
            }
        }
    }
    
    
}

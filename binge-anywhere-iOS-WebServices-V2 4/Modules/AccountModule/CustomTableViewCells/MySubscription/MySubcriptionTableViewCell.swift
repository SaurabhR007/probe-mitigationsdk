//
//  MySubcriptionTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 13/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class MySubcriptionTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var sonyLivLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var packTitleLabel: CustomLabel!
    @IBOutlet weak var packAmountLabel: CustomLabel!
    @IBOutlet weak var packExpireyLabel: CustomLabel!
    @IBOutlet weak var ftvMessageLabel: CustomLabel!
    @IBOutlet weak var canceledLabel: CustomLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTopConstraint: NSLayoutConstraint!
    var partnerSubscriptionDetails: PartnerSubscriptionDetails?
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.autoresizingMask = .flexibleHeight
        configureUI()
        collectionViewSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // MARK: - Functions
    private func configureUI() {
        self.backgroundColor = .BAdarkBlueBackground
        containerView.backgroundColor = .BAdarkBlue2Color
        canceledLabel.backgroundColor = .BApinkColor
        collectionView.backgroundColor = .BAdarkBlue2Color
        packTitleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 18), color: .BAwhite)
        packAmountLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        packExpireyLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        ftvMessageLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        canceledLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAwhite)
        canceledLabel.makeRoundedCorner(radius: 3)
    }
    
    private func collectionViewSetup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "SubscriptionProviderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SubscriptionProviderCollectionViewCell")
    }
    
    func configureCell(data: PartnerSubscriptionDetails) {
        let stringBoolValue = data.packPrice?.contains(".0") ?? false
        // TODO:
        // Hot fix for removing .0 from 199.0
        sonyLivLabel.isHidden = false
        if stringBoolValue {
            let newValue = data.packPrice?.dropLast(2) ?? ""
            packTitleLabel.text = (data.packName ?? "") //+ " " + newValue
            packAmountLabel.text = String(newValue) + " per month"
        } else {
            packAmountLabel.text = data.packPrice ?? "" + " per month"
            packTitleLabel.text = data.packName
        }
        sonyLivLabel.text = data.partnerDesc
        partnerSubscriptionDetails = nil
        if data.subscriptionInformationDTO?.complementaryPlan?.isEmpty ?? true {
            if data.subscriptionType == BASubscriptionsConstants.atv.rawValue {
                ftvMessageLabel.text = kPlanLinkedATV
                ftvMessageLabel.isHidden = false
                //ftvMessageLabel.isHidden(true, height: 50)
            } else if data.subscriptionType == BASubscriptionsConstants.stick.rawValue {
                ftvMessageLabel.text = kPlanLinkedFTV
                ftvMessageLabel.isHidden = false
                //ftvMessageLabel.isHidden(false, height: 50)
            } else {
                ftvMessageLabel.isHidden = true
            }
        } else {
            ftvMessageLabel.text = data.subscriptionInformationDTO?.complementaryPlan ?? ""
            ftvMessageLabel.isHidden = false
        }
        
        if data.status == "DEACTIVE" {
            packExpireyLabel.text = data.subscriptionExpiryMessage
//            if data.packType?.uppercased() == "FREE" {
//                packExpireyLabel.text = "Free Trial Ends on " + (data.expirationDate ?? "")
//            } else {
//               packExpireyLabel.textColor = .systemRed
//               packExpireyLabel.text = "Expired" //+ (data.expirationDate ?? "")
//            }
            if data.migrated == false {
                packExpireyLabel.isHidden = true
            } else {
                packExpireyLabel.isHidden = false
            }
            if data.cancelled == true {
                canceledLabel.isHidden(false, height: 24)
            } else {
                canceledLabel.isHidden(true, height: 24)
            }
        } else {
            packExpireyLabel.textColor = UIColor(red: 163/255, green: 166/255, blue: 194/255, alpha: 1)
            packExpireyLabel.text = data.subscriptionExpiryMessage
//            if data.packType?.uppercased() == "FREE" {
//                packExpireyLabel.text = "Free Trial Ends on " + (data.expirationDate ?? "")
//            } else {
//               packExpireyLabel.text = "Expires on " + (data.expirationDate ?? "")
//            }
            //packExpireyLabel.text = "Expires on " + (data.expirationDate ?? "")
            if data.migrated == false {
                packExpireyLabel.isHidden = true
            } else {
                packExpireyLabel.isHidden = false
            }
            canceledLabel.isHidden(true, height: 24)
        }
        //canceledLabel.isHidden(!(data.cancelled ?? false), height: 24)
        if let providerCount = data.providers?.count {
            let multiplier = CGFloat(providerCount) / 4.0
            collectionViewHeightConstraint.constant = 44 * multiplier.rounded(.up)
        }
        // TODO:
        // Hot fix for removing Space when cancelled label is hidden
        containerTopConstraint.constant = canceledLabel.isHidden ? 10.0 : 21.0
    }
    
    func configureCellForPrime(data: PartnerSubscriptionDetails) {
        canceledLabel.isHidden(true, height: 24)
        if let _data = data.primePackDetails {
            packTitleLabel.text = _data.title
            sonyLivLabel.isHidden = true
            let priceValue = _data.packAmount?.clean
            packAmountLabel.text = (priceValue ?? "") + " per month"
            partnerSubscriptionDetails = data
            ftvMessageLabel.text = "Amazon Prime  through Tata Sky" // Hard coded value added on 11th june when zeplin got updated
//            ftvMessageLabel.isHidden(true, height: 30)
            if UtilityFunction.shared.isValidPrimeSubscription(_data) {
                packExpireyLabel.text = _data.subscriptionExpiryMessage //"Already Expired" // 
            }
        }
        containerTopConstraint.constant = canceledLabel.isHidden ? 10.0 : 21.0
        collectionViewHeightConstraint.constant = 44 //* multiplier.rounded(.up)
    }
}

// MARK: - Extensions
extension MySubcriptionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let primeSubscription = partnerSubscriptionDetails {
            if let _data = primeSubscription.primePackDetails {
                return  UtilityFunction.shared.isValidPrimeSubscription(_data) ? 1 : 0
            } else {
                return 0
            }
        } else {
            return BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionProviderCollectionViewCell", for: indexPath) as! SubscriptionProviderCollectionViewCell
        if let primeSubscription = partnerSubscriptionDetails {
            if let data = primeSubscription.primePackDetails {
                cell.configureCellForPrime(data)
                return cell
            } else {
                return UICollectionViewCell()
            }
        } else {
            if let provider = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.providers {
                cell.configureCell(provider[indexPath.row])
            }
            return cell
        }
    }
}

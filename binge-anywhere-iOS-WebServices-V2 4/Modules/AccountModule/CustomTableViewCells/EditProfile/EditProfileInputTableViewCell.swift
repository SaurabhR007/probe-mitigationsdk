//
//  EditProfileInputTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol EditProfileTextFieldDelegate: class {
    func textFieldValueChanged(_ tag: Int, _ text: String)
}

class EditProfileInputTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var errorInfoLabel: UILabel!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var inputTextField: CustomtextField!
    weak var delegate: EditProfileTextFieldDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = .BAdarkBlueBackground
        inputTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configureUi(_ data: EditProfileDataSource) {
        errorInfoLabel.isHidden = true
        titleLabel.text = data.labelTitle
        inputTextField.text = data.textValue
        inputTextField.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth))
        inputTextField.cornerRadius = 2
        inputTextField.backgroundColor = .BAdarkBlue1Color
        inputTextField.textColor = .BAwhite
        inputTextField.tintColor = .BAwhite
        inputTextField.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
        inputTextField.setViewShadow(opacity: 0.12, blur: 2)
        inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)
        inputTextField.returnKeyType = data.keyBoardReturnType
        inputTextField.keyboardType = .asciiCapable
    }

    func changeTextFieldStyle() {
        titleLabel.text = ""
        inputTextField.backgroundColor = .clear
        inputTextField.borderColor = .clear
        inputTextField.text = ""
        inputTextField.isUserInteractionEnabled = false
    }

    private func isTextFieldHighlighted(_ highlighted: Bool) {
        inputTextField.borderWidth = 0.0
        if highlighted {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAlightBlueGrey
        }
    }

    @IBAction private func txtFieldBeginEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(true)
    }

    @IBAction private func txtFieldEndEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(false)
    }

    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let del = delegate {
            del.textFieldValueChanged(sender.tag, sender.text ?? "")
        }
    }
}

// MARK: - Extension
extension EditProfileInputTableViewCell {

    // MARK: - TableView Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.superview?.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

//
//  EditProfileImageTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

protocol AddProfileImageDelegate: class {
    func updateProfileImageButton()
}

class EditProfileImageTableViewCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var gradientBorderImageView: UIImageView!
    @IBOutlet weak var uploadButton: CustomButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameInitials: CustomLabel!

    weak var delegate: AddProfileImageDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle  = .none
        self.activityIndicator.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    func configureView(userDetail: UserDetailData?) {
        UtilityFunction.shared.performTaskInMainQueue {
			self.nameInitials.backgroundColor = .clear
            let randomColor: [UIColor] = [.BApinkColor, .BAmidBlue, .BAlightBlueGrey, .BATrendingPurple, .BAerrorRed, .BAExperingRed, .BAExclusiveOrange, .BANewBlue]
            self.shadowView.makeCircular(roundBy: .height)
            self.gradientBorderImageView.makeCircular(roundBy: .height)
            self.profileImageView.makeCircular(roundBy: .height)
            UtilityFunction.shared.addCircularGradient(imageView: self.gradientBorderImageView)
            self.nameInitials.makeCircular(roundBy: .height)
            self.nameInitials.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 80), color: .BAwhite)
            let initials = (userDetail?.firstName)?.trimString(byString: "")
            let index = initials?.index(initials!.startIndex, offsetBy: 0)
			if index != nil {
                self.nameInitials.text = String((initials?[index!])!).capitalizingFirstLetter()
			}
			UIView.animate(withDuration: 1, animations: {
				self.nameInitials.layer.backgroundColor = (randomColor.randomElement() ?? .BANewBlue).cgColor
			})
        }
    }

    func setUIImage(_ image: String) {
        UtilityFunction.shared.performTaskInMainQueue {
            if let url = URL(string: image) {
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
				self.profileImageView.alpha = 0
                self.profileImageView.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { _ in
					UIView.animate(withDuration: 1, animations: {
						self.profileImageView.alpha = 1
					})
                    KingfisherManager.shared.cache.clearDiskCache()
                    KingfisherManager.shared.cache.clearMemoryCache()
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                })
            }
        }
    }

    @IBAction func addImageButtonAction() {
        if let del = delegate {
            del.updateProfileImageButton()
        }
    }
}

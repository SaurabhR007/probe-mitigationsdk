//
//  EditProfilePasswordTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class EditProfilePasswordTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureUI() {
        titleLabel.text = "Update Password"
        imgView.image = UIImage(named: "padlock")
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle = .none
    }
}

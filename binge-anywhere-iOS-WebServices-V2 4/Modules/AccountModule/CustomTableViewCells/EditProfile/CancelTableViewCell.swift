//
//  CancelTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol CancelButtonDelegate: class {
    func cancelButtonTapped()
}
class CancelTableViewCell: UITableViewCell {

    @IBOutlet weak var cancelButton: CustomButton!
    weak var delegate: CancelButtonDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle = .none
        cancelButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BABlueColor)
        cancelButton.underline()
    }

    @IBAction func cancelButtonAction() {
        if let del = delegate {
            del.cancelButtonTapped()
        }
    }

}

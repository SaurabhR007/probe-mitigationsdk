//
//  EditProfileNumberTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class EditProfileNumberTableViewCell: UITableViewCell {

    @IBOutlet weak var mobileNumberLabel: CustomLabel!
	@IBOutlet weak var titleLabel: CustomLabel!
	@IBOutlet weak var separatorLabel: CustomLabel!
    @IBOutlet weak var seperatorBottomConstraints: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle = .none
        // Initialization code
    }

	func configureCell(title: String, _ mobileNumber: String, separator: Bool) {
		titleLabel.text = title
        mobileNumberLabel.text = mobileNumber
		separatorLabel.isHidden = separator
        seperatorBottomConstraints.constant = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

//
//  BATransactionsTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 03/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

protocol SelectDeselectNotificationDelegate: class {
    func markUnMarkCells(_ indexPath: IndexPath, _ select: Bool)
}

class BATransactionsTableViewCell: UITableViewCell {

    @IBOutlet weak var dividerLabel: CustomLabel!
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var providerImageView: UIImageView!
    @IBOutlet weak var transactionReceivedDuration: CustomLabel!
    @IBOutlet weak var transactionDetaillabel: ExpandableLabel!
    @IBOutlet weak var transactionTtitleLabel: CustomLabel!
    @IBOutlet weak var selectButton: CustomButton!
    @IBOutlet weak var viewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewTrailingConstraint: NSLayoutConstraint!
    weak var delegate: SelectDeselectNotificationDelegate!
    @IBOutlet weak var backGroundView: UIView!
    var indexPath: IndexPath!
    
    let kCharacterBeforReadMore =  65
    let kReadMoreText           =  " +More"
    let kReadLessText           =  "-Less"

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.backGroundView.backgroundColor = .BAdarkBlueBackground
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        notificationImageView.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        selectButton.makeCircular(roundBy: .height)
    }

    func configureUI(_ rowData: NotificationViewModel, _ isEditing: Bool, _ index: IndexPath, _ isTransactionType: Bool, isExpanded:Bool = false) {
        super.updateConstraints()
        indexPath = index
        selectButton.isSelected = rowData.isSelected
        selectButton.isHidden = !isEditing
        //transactionDetaillabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 12), color: .BAlightBlueGrey)
        self.viewLeadingConstraint.constant = isEditing ? 40 : 0
        self.viewTrailingConstraint.constant = isEditing ? -40 : 0
        UIView.animate(withDuration: 1/3) {
            self.layoutIfNeeded()
        }
        if rowData.isRead {
            self.backGroundView.backgroundColor = .clear
            self.dividerLabel.isHidden = false
        } else {
            self.backGroundView.backgroundColor = .BAdarkBlue1Color
            self.dividerLabel.isHidden = true
        }
        providerImageView.isHidden = isTransactionType
        
        if let urlStr = URL.init(string: rowData.imageUrl) {
            notificationImageView.kf.setImage(with: ImageResource(downloadURL: urlStr), placeholder: nil, options: nil, progressBlock: nil) { result in
                switch result {
                case .success: break
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            notificationImageView.image = UIImage(named: "NavigationHeaderLogo")
        }
        
        var url = URL.init(string: "")
        switch rowData.provider.uppercased() {
        case ProviderType.prime.rawValue:
            url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoRectangular ?? "")
        case ProviderType.sunnext.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoRectangular ?? "")
        case ProviderType.netflix.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.NETFLIX?.logoRectangular ?? "")
        case ProviderType.shemaro.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoRectangular ?? "")
        case ProviderType.vootSelect.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoRectangular ?? "")
        case ProviderType.vootKids.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoRectangular ?? "")
        case ProviderType.hungama.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoRectangular ?? "")
        case ProviderType.zee.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoRectangular ?? "")
        case ProviderType.sonyLiv.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoRectangular ?? "")
        case ProviderType.hotstar.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoRectangular ?? "")
        case ProviderType.eros.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoRectangular ?? "")
        case ProviderType.curosityStream.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoRectangular ?? "")
        case ProviderType.hopster.rawValue:
            providerImageView.image = UIImage(named: "hopster")
//            self.updateLogoWidth(image: logoImageView.image)
        default:
            providerImageView.image = nil
//            self.updateLogoWidth(image: logoImageView.image)
        }
        
        if let _url = url {
            providerImageView.alpha = 0
            providerImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { result in
                switch result {
                case .success:
                    UIView.animate(withDuration: 1, animations: {
                        self.providerImageView.alpha = 1
                    })
                case .failure: break
                }
            })
        }
        
        transactionTtitleLabel.text = rowData.contentTitle.isEmpty ? rowData.notificationDisplayTitle : rowData.contentTitle
        
        transactionDetaillabel.font = UIFont.skyTextFont(.regular, size: 12)
        transactionDetaillabel.textColor = .BAlightBlueGrey
        
        transactionDetaillabel.cellPath = index
        transactionDetaillabel.charatersBeforeReadMore = self.kCharacterBeforReadMore
        transactionDetaillabel.collapsedAttributedLink =  NSAttributedString(string: kReadMoreText, attributes: [NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15), NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor])
        transactionDetaillabel.expandedAttributedLink = NSAttributedString(string: kReadLessText ,attributes: [NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15), NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor])
        transactionDetaillabel.ellipsis = nil
        self.layoutIfNeeded()
        transactionDetaillabel.shouldCollapse = true
        transactionDetaillabel.textReplacementType = .character
        transactionDetaillabel.numberOfLines = 2
        transactionDetaillabel.collapsed = rowData.isCollapsed
        
        transactionDetaillabel.text = rowData.descriptionMessage.isEmpty ? rowData.notificationMessage : rowData.descriptionMessage
        
        
        transactionReceivedDuration.text = rowData.pushTime
    }
    
    // MARK: To set the constraint of logoImageView in such a way that image get aligned on left
//    func updateLogoWidth(image: UIImage?) {
//        let imageWidth = ((image?.size.width ?? 1) / (image?.size.height ?? 1)) * providerImageView.frame.size.height
//        providerImageView.constant = imageWidth
//    }

    @IBAction func selectButtonTapped(_ sender: CustomButton) {
        if let del = delegate, let index = indexPath {
            selectButton.isSelected = !selectButton.isSelected
            del.markUnMarkCells(index, selectButton.isSelected)
        }
    }
}

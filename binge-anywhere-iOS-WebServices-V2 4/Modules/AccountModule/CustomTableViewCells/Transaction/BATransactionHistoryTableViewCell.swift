//
//  BATransactionHistoryTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 03/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BATransactionHistoryTableViewCellDelegate: class {
    func downloadTransactiionForSelectedIndex(index: Int?)
}

class BATransactionHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var transactionContentView: UIView!
    @IBOutlet weak var downloadBillButton: CustomButton!
    @IBOutlet weak var transactionDatelabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var transactionCostLabel: CustomLabel!
    @IBOutlet weak var sidLabel: CustomLabel!

    weak var delegate: BATransactionHistoryTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configureTransactionHistory(transactionData: TransactionData?) {
        //self.backgroundColor = .clear
        self.transactionContentView.backgroundColor = .BAdarkBlueBackground
        sidLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 20), color: .BAwhite)
        transactionCostLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 20), color: .BAwhite)
        transactionDatelabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        descriptionLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        transactionCostLabel.text = "₹   \(transactionData?.amount ?? "0")"
        sidLabel.text = "ID \(transactionData?.transactionId ?? "")"
        descriptionLabel.text = transactionData?.description
        transactionDatelabel.text = transactionData?.date
    }

    @IBAction func downloadBuildButtonAction(_ sender: Any) {
        delegate?.downloadTransactiionForSelectedIndex(index: 0)
    }
}

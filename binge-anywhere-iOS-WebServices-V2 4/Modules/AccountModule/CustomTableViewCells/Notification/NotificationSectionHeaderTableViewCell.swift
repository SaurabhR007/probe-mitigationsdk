//
//  NotificationSectionHeaderTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class NotificationSectionHeaderTableViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = .BAdarkBlueBackground
    }

    func configureUI(_ title: String) {
        titleLabel.text = title
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
    }

    func updateConstraint(_ isEditing: Bool) {
        UtilityFunction.shared.performTaskInMainQueue {
            self.leadingConstraint.constant = isEditing ? 56 : 16
            UIView.animate(withDuration: 1/3) {
                self.layoutIfNeeded()
            }
        }
    }
}

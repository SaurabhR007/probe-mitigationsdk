//
//  AccountsTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 29/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class AccountsTableViewCell: UITableViewCell, MoengageProtocol {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var accountImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .BAdarkBlueBackground
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureUI(index: Int?, shouldShowSwitchAccount: Bool) {
        self.countLabel.isHidden = index != 3
        self.countLabel.makeCircular(roundBy: .height)
        self.countLabel.isHidden = true
        if shouldShowSwitchAccount {
            switch index {
            case 1:
                titleLabel.text = "My Subscription"
                accountImageView.image = UIImage(named: "subscription")
            case 2:
                titleLabel.text = "Switch Account"
                accountImageView.image = UIImage(named: "switchAccount")
            case 3:
                titleLabel.text = "Notifications"
                accountImageView.image = UIImage(named: "notification")
                setCountLabel()
//            case 4:
//                titleLabel.text = "Transaction History"
//                accountImageView.image = UIImage(named: "transactionhistory")
            case 4:
                titleLabel.text = "Edit Profile"
                accountImageView.image = UIImage(named: "editprofile")
            case 5:
                titleLabel.text = "Device Management"
                accountImageView.image = UIImage(named: "devicemanagement")
            default:
                return
            }
        } else {
            switch index {
            case 1:
                titleLabel.text = "My Subscription"
                accountImageView.image = UIImage(named: "subscription")
            case 2:
                titleLabel.text = "Notifications"
                accountImageView.image = UIImage(named: "notification")
                setCountLabel()
//            case 3:
//                titleLabel.text = "Transaction History"
//                accountImageView.image = UIImage(named: "transactionhistory")
            case 3:
                titleLabel.text = "Edit Profile"
                accountImageView.image = UIImage(named: "editprofile")
            case 4:
                titleLabel.text = "Device Management"
                accountImageView.image = UIImage(named: "devicemanagement")
            default:
                return
            }
        }
    }
    
 // In order to hotfix the notification badge icon and list added all these hot fix to let it work properly nead to update once release is done
    func setCountLabel() {
        if getUnreadMessageCount() > 0 {
            NotificationCenter.default.post(Notification(name: Notification.Name("didReceiveNotification")))
//            NotificationCenter.default.post(Notification(name: Notification.Name("updateNotificationList")))
            countLabel.isHidden = false
            if getUnreadMessageCount() > 99 {
                countLabel.text = "99+"
            } else {
                countLabel.text = "\(getUnreadMessageCount())"
            }
        } else {
            NotificationCenter.default.post(Notification(name: Notification.Name("resetBadgeCount")))
        }
    }

}

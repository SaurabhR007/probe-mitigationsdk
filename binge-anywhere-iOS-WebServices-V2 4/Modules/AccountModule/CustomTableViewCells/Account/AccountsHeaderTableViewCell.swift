//
//  AccountsHeaderTableViewCell.swift
//  BingeAnywhere
//
//  Created by Shivam on 03/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit
import Kingfisher

protocol AccountsHeaderTableViewCellProtcol: AnyObject {
    func aliasUpdated(_ name: String)
    func aliasMaxReaches()
    func showAlert()
    func refreshBalance()
}

class AccountsHeaderTableViewCell: UITableViewCell {
    
    // MARK: IBOutlets
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var packDetailStackView: UIStackView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: CustomLabel!
    @IBOutlet weak var mobileLabel: CustomLabel!
    @IBOutlet weak var addAliasTF: CustomtextField!
    @IBOutlet weak var priceInfoLabel: CustomLabel!
    @IBOutlet weak var priceLabel: NumberScrollView!
    @IBOutlet weak var editAliasButton: UIButton!
    @IBOutlet weak var rechargeDueDateLabel: CustomLabel!
    @IBOutlet weak var gradientBorderImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var rechargeButton: CustomButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameInitials: CustomLabel!
    @IBOutlet weak var standardLabel: CustomLabel!
    @IBOutlet weak var standardLabelExpiry: CustomLabel!
    @IBOutlet weak var amazonLabel: CustomLabel!
    @IBOutlet weak var amazonLabelExpiry: CustomLabel!
    @IBOutlet weak var refreshButton: CustomButton!
    var alias = ""
    
    weak var delegate: AccountsHeaderTableViewCellProtcol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.activityIndicator.isHidden = true
        configureUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    private func configureUI() {
        self.selectionStyle = .none
        self.contentView.backgroundColor = .BAdarkBlueBackground
        nameLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 24), color: .BAwhite)
        mobileLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        standardLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        standardLabelExpiry.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        amazonLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        amazonLabelExpiry.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        priceInfoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14), color: .BAlightBlueGrey)
        priceLabel.animationDuration = 2.0
        priceLabel.set(font: .skyTextFont(.medium, size: 22), textColor: .BAwhite)
        rechargeDueDateLabel.setupCustomFontAndColor(font: .skyTextFont(.regular, size: 12), color: .BAwhite)
        rechargeButton.fontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
        nameInitials.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 60), color: .BAwhite)
        addAliasTF.delegate = self
        addAliasTF.font = .skyTextFont(.medium, size: 14)
        addAliasTF.textColor = .BASecondaryGray
        addAliasTF.isUserInteractionEnabled = false
        addAliasTF.cornerRadius = 2
        priceLabel.text = " - - "
        rechargeDueDateLabel.text = "Recharge due on" + " - - "
        rechargeButton.makeCircular(roundBy: .height)
        userImageView.makeCircular(roundBy: .height)
        nameInitials.makeCircular(roundBy: .height)
        gradientBorderImageView.makeCircular(roundBy: .height)
        nameInitials.backgroundColor = .clear
        containerView.setRoundedCorners(cornerRadius: 30, maskCorners: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        containerView.layer.masksToBounds = false
        containerView.layer.shadowRadius = 20
        containerView.layer.shadowOpacity = 0.3
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0 , height:8)
        rechargeButton.isHidden(true)
        refreshButton.isHidden = false
        editAliasButton.isHidden = true
        UtilityFunction.shared.addCircularGradient(imageView: gradientBorderImageView)
        UtilityFunction.shared.performTaskInMainQueue {
            self.userImageView.layer.frame = self.userImageView.layer.frame.insetBy(dx: 2, dy: 2)
        }
    }
    
    func configureUserData(userProfile: UserDetailData?, userBalance: BalanceData?, subscriptionDetail: PartnerSubscriptionDetails?, isTableReloaded: Bool?) {
        
        let randomColor: [UIColor] = [/*.BApinkColor, .BAmidBlue, .BAlightBlueGrey, .BATrendingPurple, .BAerrorRed, .BAExperingRed, .BAExclusiveOrange, */.BAdarkBlueBackground]
        nameLabel.text = "\(userProfile?.firstName?.capitalizingFirstLetter() ?? "")" + " " + "\(userProfile?.lastName?.capitalizingFirstLetter() ?? "")"
        priceInfoLabel.text =  "Tata Sky Balance for Sub ID: " + (BAKeychainManager().sId)
        mobileLabel.text = "Mobile: +91 " + (userProfile?.rmn ?? "")
        // https://jira.tothenew.com/browse/TBAA-5341 if subType is bingeanywhere and comp-Plan is not nil or not empty show tvicon else if large screen then show tvicon only else deviceicon
        if subscriptionDetail?.subscriptionInformationDTO?.subscriptionType == BASubscriptionsConstants.binge.rawValue {
            if let complementaryPlan = subscriptionDetail?.subscriptionInformationDTO?.complementaryPlan {
                if complementaryPlan.isEmpty {
                    deviceImageView.image = UIImage(named: "device")
                } else {
                    deviceImageView.image = UIImage(named: "tvicon")
                }
            }
            deviceImageView.image = UIImage(named: "device")
        } else if subscriptionDetail?.subscriptionInformationDTO?.subscriptionType == BASubscriptionsConstants.atv.rawValue || subscriptionDetail?.subscriptionInformationDTO?.subscriptionType == BASubscriptionsConstants.stick.rawValue {
            deviceImageView.image = UIImage(named: "tvicon")
        } else {
            deviceImageView.image = UIImage(named: "device")
        }
        
        //        if subscriptionDetail?.subscriptionType == BASubscriptionsConstants.binge.rawValue {
        //            deviceImageView.image = UIImage(named: "device")
        //        } else {
        //            deviceImageView.image = UIImage(named: "tvicon")
        //        }
        
        if userProfile?.aliasName?.isEmpty ?? false {
            addAliasTF.text = "Add Alias"
        } else {
            alias = userProfile?.aliasName ?? ""
            addAliasTF.text = userProfile?.aliasName
        }
        UIView.animate(withDuration: 1, animations: {
            self.nameInitials.layer.backgroundColor = (randomColor.randomElement() ?? .BANewBlue).cgColor
        })
        
        configureSubscription(subscription: subscriptionDetail)
        
        if userBalance != nil && !(userBalance?.balanceQueryRespDTO?.balance?.isEmpty ?? false) {
            priceLabel.set(text: userBalance?.balanceQueryRespDTO?.balance ?? "", animated: true)
            refreshButton.isHidden = false
        } else {
            priceLabel.text = " - - "
            refreshButton.isHidden = false
        }
        
        if userBalance != nil && !(userBalance?.balanceQueryRespDTO?.endDate?.isEmpty ?? false) {
            rechargeDueDateLabel.text = "Recharge due on" + " " + (userBalance?.balanceQueryRespDTO?.endDate ?? "")
        } else {
            rechargeDueDateLabel.text = "Recharge due on" + " - - "
        }
        if isTableReloaded ?? false {
            if userProfile?.profileImage?.isEmpty ?? true {
                self.userImageView.image = nil
                nameInitials.makeCircular(roundBy: .height)
                let initials = (userProfile?.firstName?.trimString(byString: "") ?? " ")
                let index = initials.index(initials.startIndex, offsetBy: 0)
                if index <= initials.endIndex {
                    nameInitials.text = String(initials[index]).capitalizingFirstLetter()
                }
                UIView.animate(withDuration: 1, animations: {
                    self.nameInitials.layer.backgroundColor = (randomColor.randomElement() ?? .BANewBlue).cgColor
                })
            } else {
                UtilityFunction.shared.performTaskInMainQueue {
                    self.nameInitials.text = ""
                    let profileImageUrl = (BAConfigManager.shared.configModel?.data?.config?.subscriberImage?.subscriberImageBaseUrl ?? "") + (userProfile?.profileImage ?? "")
                    if let url = URL.init(string: profileImageUrl ) {
                        self.activityIndicator.isHidden = false
                        self.activityIndicator.startAnimating()
                        self.userImageView.alpha = 0
                        self.userImageView.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { _ in
                            UIView.animate(withDuration: 1, animations: {
                                self.userImageView.alpha = 1
                            })
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                        })
                    }
                }
            }
            // Do Nothing
        } else {
            if userProfile?.profileImage?.isEmpty ?? true {
                self.userImageView.image = nil
                nameInitials.makeCircular(roundBy: .height)
                let initials = (userProfile?.firstName?.trimString(byString: "") ?? " ")
                let index = initials.index(initials.startIndex, offsetBy: 0)
                if index <= initials.endIndex {
                    nameInitials.text = String(initials[index]).capitalizingFirstLetter()
                }
                UIView.animate(withDuration: 1, animations: {
                    self.nameInitials.layer.backgroundColor = (randomColor.randomElement() ?? .BANewBlue).cgColor
                })
            } else {
                UtilityFunction.shared.performTaskInMainQueue {
                    self.nameInitials.text = ""
                    let profileImageUrl = (BAConfigManager.shared.configModel?.data?.config?.subscriberImage?.subscriberImageBaseUrl ?? "") + (userProfile?.profileImage ?? "")
                    if let url = URL.init(string: profileImageUrl ) {
                        self.activityIndicator.isHidden = false
                        self.activityIndicator.startAnimating()
                        self.userImageView.alpha = 0
                        self.userImageView.kf.setImage(with: ImageResource(downloadURL: url), completionHandler: { _ in
                            UIView.animate(withDuration: 1, animations: {
                                self.userImageView.alpha = 1
                            })
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                        })
                    }
                }
            }
        }
    }
    
    func updateAliasInUserProfile(aliasName: String) {
        alias = aliasName
    }
    
    func configureSubscription(subscription: PartnerSubscriptionDetails?) {
        if subscription != nil {
            if let primeSubscriptionData = subscription?.primePackDetails {
                if let subscriptionStatus = subscription?.subscriptionInformationDTO?.bingeAccountStatus {
                    if subscriptionStatus == kWrittenOff {
                        amazonLabel.text = ""
                        amazonLabelExpiry.text = ""
                    } else {
                        configurePrimePackOnProfilePage(primeSubscriptionData)
                    }
                }
                configureStandardPackOnProfilePage(subscription)
            }  else {
                amazonLabel.text = ""
                amazonLabelExpiry.text = ""
                configureStandardPackOnProfilePage(subscription)
            }
        }
    }
    
    func configurePrimePackOnProfilePage(_ subscription: PrimePackDetails) {
        if UtilityFunction.shared.isValidPrimeSubscription(subscription) {
            amazonLabel.text = subscription.title ?? "Amazon Prime"
            amazonLabelExpiry.text = subscription.subscriptionExpiryMessage
        } else {
            amazonLabel.text = ""
            amazonLabelExpiry.text = ""
        }
    }
    
    
    func configureStandardPackOnProfilePage(_ subscription: PartnerSubscriptionDetails?) {
        if subscription?.subscriptionInformationDTO?.bingeAccountStatus == kWrittenOff {
            standardLabel.isHidden = true
            standardLabelExpiry.isHidden = true
        } else {
            standardLabel.isHidden = false
            standardLabelExpiry.isHidden = false
            let stringBoolValue = subscription?.packPrice?.contains(".0") ?? false
            if stringBoolValue {
                standardLabel.text = (subscription?.packName ?? "")
            } else {
                standardLabel.text = (subscription?.packName ?? "")
            }
            if subscription?.status == "DEACTIVE" {
                standardLabelExpiry.textColor = .BAExperingRed
                standardLabelExpiry.text = subscription?.accountScreenExpiryMessage
                if subscription?.migrated == false {
                    if subscription?.packType?.uppercased() == "FREE" {
                        standardLabelExpiry.isHidden = false
                    } else {
                        standardLabelExpiry.isHidden = true
                    }
                    //standardLabel.isHidden = true
                } else {
                    standardLabelExpiry.isHidden = false
                    //standardLabel.isHidden = false
                }
            } else {
                standardLabelExpiry.textColor = .BAlightBlueGrey
                standardLabelExpiry.text = subscription?.accountScreenExpiryMessage
                if subscription?.migrated == false {
                    standardLabelExpiry.isHidden = true
                } else {
                    standardLabelExpiry.isHidden = false
                }
            }
        }
    }
    
    @IBAction func rechargeAccountAction(_ sender: Any) {
        delegate?.showAlert()
    }
    
    @IBAction func editAliasAction() {
        editAliasButton.isSelected = !editAliasButton.isSelected
        addAliasTF.isUserInteractionEnabled = editAliasButton.isSelected
        if addAliasTF.isUserInteractionEnabled {
            addAliasTF.becomeFirstResponder()
            //            addAliasTF.selectAll(nil)
            addAliasTF.backgroundColor = .clear
        } else {
            addAliasTF.resignFirstResponder()
            addAliasTF.backgroundColor = .clear
            if addAliasTF.text?.isEmpty ?? false && alias.isEmpty {
                addAliasTF.text = "Add Alias"
            } else {
                addAliasTF.text = alias
            }
        }
    }
    
    @IBAction func refreshButtonAcrtion() {
        refreshButton.rotate360Degrees(duration: 0.3, repeatCount: 1.0)
        delegate?.refreshBalance()
    }
}

extension AccountsHeaderTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let del = delegate {
            del.aliasUpdated(addAliasTF.text ?? "")
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        //        let maxLimitReached = validateData(textField.tag, txtAfterUpdate)
        //        if !maxLimitReached {
        //            delegate?.aliasMaxReaches()
        //        } else {
        //            // DO Nothing
        //        }
        return validateData(textField.tag, txtAfterUpdate)
    }
    
    private func validateData(_ tag: Int, _ text: String) -> Bool {
        return tag == 0 ? text.length <= 20 : true
    }
}

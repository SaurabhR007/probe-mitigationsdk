//
//  RMNTableViewCell.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 06/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol RMNTableViewCellProtocol: AnyObject {
    func valueUpdated(_ idx: Int, _ text: String)
}

class RMNTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var inputTextField: CustomtextField!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var errorInfoLabel: CustomLabel!

    // MARK: - Variables
    weak var delegate: RMNTableViewCellProtocol?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

        NotificationCenter.default.addObserver(self, selector: #selector(showLinkError), name: NSNotification.Name(rawValue: "ShowErrorForLink"), object: nil)
        configureUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Functions
    private func configureUI() {
        self.contentView.backgroundColor = .BAdarkBlueBackground

        inputTextField.delegate = self
        inputTextField.backgroundColor = .BAdarkBlue1Color
        inputTextField.textColor = .BAwhite
        inputTextField.tintColor = .BAwhite
        inputTextField.font = .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth)
        inputTextField.keyboardType = .numberPad
        inputTextField.addDoneButtonOnKeyBoardWithControl()
        inputTextField.cornerRadius = 2
        inputTextField.setViewShadow(opacity: 0.12, blur: 2)
        inputTextField.setHorizontalPadding(left: 12 * screenScaleFactorForWidth, right: 12 * screenScaleFactorForWidth)

        titleLabel.text = "Enter Mobile Number or Subscriber ID"
        errorInfoLabel.text = ""

    }

    override func layoutSubviews() {
        inputTextField.placeholderColorAndFont(color: .BAlightBlueGrey, font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth))
        titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 14 * screenScaleFactorForWidth), color: .BAwhite)
        errorInfoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 10 * screenScaleFactorForWidth), color: .BAerrorRed)
    }

    private func isTextFieldHighlighted(_ highlighted: Bool) {
        inputTextField.borderWidth = 0.0
        if highlighted {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAlightBlueGrey
        }
    }

    private func textFieldHasError(_ hasError: Bool, with Placeholder: String = "", errorMessage: String = "") {
        if hasError {
            inputTextField.borderWidth = 1.0
            inputTextField.borderColor = .BAerrorRed
            inputTextField.placeholder = Placeholder
            errorInfoLabel.text = errorMessage
        } else {
            inputTextField.borderWidth = 0.0
            errorInfoLabel.text = ""
        }
    }

    private func validateData(_ tag: Int, _ text: String) -> Bool {
        return tag == 0 ? text.length <= 10 : text.length <= 6
    }

    func configureCell(_ data: TableViewCellSourceModel) {
        switch data {
        case .linkAccount:
            titleLabel.text = "Enter Mobile Number or Subscriber ID"
            inputTextField.placeholder = ""
        case .linkAccountVerification:
            titleLabel.text = "Enter OTP"
            inputTextField.placeholder = "6 Digits"
        default:
            break
        }
    }

    // MARK: - Actions
    @objc private func showLinkError(notification: NSNotification) {
        textFieldHasError(true, with: "", errorMessage: "Field cannot be left blank.")
    }

    @IBAction private func txtFieldBeginEditing(_ sender: CustomtextField) {
        textFieldHasError(false)
        isTextFieldHighlighted(true)
    }

    @IBAction private func txtFieldEndEditing(_ sender: CustomtextField) {
        isTextFieldHighlighted(false)
        if let textString = sender.text {
            switch sender.tag {
            case 0:
                if !textString.isValidContact && (textString.length > 0) {
                    textFieldHasError(true, with: "", errorMessage: "Please enter a valid number or Subscriber ID")
                }
            case 1:
                if textString.length != 6 {
                    textFieldHasError(true, with: "", errorMessage: "Please enter a valid OTP")
                }
            default:
                if textString.isEmpty {
                    textFieldHasError(false)
                }
            }
        }
    }
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let del = delegate {
            del.valueUpdated(sender.tag, sender.text ?? "")
        }
    }

}

// MARK: - Extension
extension RMNTableViewCell: UITextFieldDelegate {

    // MARK: - TableView Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        print("Login Number", txtAfterUpdate)
        return validateData(textField.tag, txtAfterUpdate)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nxtTag = textField.tag + 1
        let viewSuper = self.superview?.viewWithTag(0)
        guard let nxtTextField = viewSuper?.viewWithTag(nxtTag) else { return true }
        nxtTextField.becomeFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

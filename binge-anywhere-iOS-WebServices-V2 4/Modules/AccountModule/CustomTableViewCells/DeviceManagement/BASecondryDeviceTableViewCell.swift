//
//  BASecondryDeviceTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

protocol BASecondryDeviceTableViewCellDelegate: class {
    func removeDevice(index: Int?, deviceName: String?)
}

class BASecondryDeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var secondryDevicesView: UIView!
    @IBOutlet weak var thisDeviceLabel: UILabel!
    @IBOutlet weak var thisDeviceButton: UIButton!
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var secondryDeviceView: UIView!
    var deviceName: String? = ""
    weak var delegate: BASecondryDeviceTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureDevices(deviceDetail: DeviceList?, index: Int?) {
        secondryDevicesView.backgroundColor = .BAdarkBlue1Color
        thisDeviceButton.tag = index ?? 0
        self.contentView.backgroundColor = .BAdarkBlueBackground
        if deviceDetail?.deviceNumber == kDeviceId {
            thisDeviceButton.isHidden = true
            thisDeviceLabel.isHidden = false
        } else {
            thisDeviceButton.isHidden = false
            thisDeviceLabel.isHidden = true
        }
        deviceNameLabel.text = deviceDetail?.deviceName
        deviceName = deviceDetail?.deviceName
    }

    @IBAction func removeDeviceButtonAction(_ sender: Any) {
        delegate?.removeDevice(index: thisDeviceButton.tag, deviceName: deviceName)
    }
}

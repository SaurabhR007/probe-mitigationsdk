//
//  BAPrimaryDeviceTableViewCell.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 13/02/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BAPrimaryDeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var primaryView: UIView!
    @IBOutlet weak var primaryDeviceView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureDevice(_ deviceDetail: DeviceList?) {
        self.deviceName.text = deviceDetail?.deviceName
    }
    
    

}

//
//  PrimeAppIntegration.swift
//  BingeAnywhere
//
//  Created by Shivam on 11/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol PrimeContentNavigation {
    var contentListModel: BAContentListModel? {set get}
    var pageSource: String {get set}
    var continueWatchingVM: BAWatchingViewModal {get set}
    var primeServiceVM: PrimeIntegrationViewModal {get set}
    var primeAppSuspendedState: PrimeAppConstant? {get set}
    func presentPrimeContentAlert(_ content: BAContentListModel)
    func showPrimeAppAlertOnView(_ header: String?, _ message: String, _ numberOfButtons: Int,_ okButtonTitle: String, _ cancelButtonTitle: String, _ isSuspended: Bool, _ increasePopUpCounter: Bool, _ isErrorHeaderLogo: Bool)
    func checkToNavigateUserToPrimeAppOrAppStoreUrl()
    func playbackEventTracking(contentTitle: String, contentType: String, contentGenre: String, partnerName: String, languages: String, railName: String, pageSource: String, configSource: String)
    func primeContentPlayerMixPanelEvents()
    func mixPanelEventForPrime(_ eventType: Int, _ paramValue: String)
    func requestPrimeResumeAPi()
    func navigateToAppStorePrimeApp()
    func navigateToPrimeApp()
    func makeCWRequestCallForPrimeContent()
}

class PrimeAppIntegration: BaseViewController, PrimeContentNavigation {
    
    var primeAppSuspendedState: PrimeAppConstant?
    
    var continueWatchingVM: BAWatchingViewModal
    
    var primeServiceVM: PrimeIntegrationViewModal
    
    var pageSource: String
    
    var contentListModel: BAContentListModel?
    
    
    init(continueWatchingVM: BAWatchingViewModal, primeServiceVM: PrimeIntegrationViewModal, pageSource: String ) {
        self.pageSource = pageSource
        self.continueWatchingVM = continueWatchingVM
        self.primeServiceVM = primeServiceVM
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
     primeTitle1, primeMessage1 this key is used for installed prime case
     primeTitle2, primeMessage2 this key is used for unInstalled prime case
     */
    func presentPrimeContentAlert(_ content: BAContentListModel) {
        self.contentListModel = content
        let primeSubscriptionStatus = UtilityFunction.shared.checkForPrimeSubscription()
        if let status = primeSubscriptionStatus {
            if status {
                if UtilityFunction.shared.checkIsPrimeAppInstalledOrNot() {
                    let header = UtilityFunction.shared.getPrimeMessageStringFromPrimePopUpDataNode(0)
                    let message = UtilityFunction.shared.getPrimeMessageStringFromPrimePopUpDataNode(2)
                    showPrimeAppAlertOnView(header, message, 2, PrimeAppPopUpButtonText.Proceed.rawValue, PrimeAppPopUpButtonText.Cancel.rawValue, false, true, false)
                } else {
                    let header = UtilityFunction.shared.getPrimeMessageStringFromPrimePopUpDataNode(1)
                    let message = UtilityFunction.shared.getPrimeMessageStringFromPrimePopUpDataNode(3)
                    showPrimeAppAlertOnView(header, message, 2, PrimeAppPopUpButtonText.Proceed.rawValue, PrimeAppPopUpButtonText.Cancel.rawValue, false, true, false)
                }
            } else {
                primeAppSuspendedState = UtilityFunction.shared.checkForPrimeSuspendedState()
                let suspendedStateMessage = UtilityFunction.shared.getPrimeMessageStringFromPrimePopUpDataNode(3)
                var suspendedStateHeader: String?
                var okButtonText: String = ""
                var cancelButtonText: String = ""
                var numberOfbuttons: Int = 2
                var isErrorHeaderLogo: Bool = false
                switch primeAppSuspendedState {
                case .active:
                    okButtonText = PrimeAppPopUpButtonText.resumePrime.rawValue
                    cancelButtonText = PrimeAppPopUpButtonText.Skip.rawValue
                    suspendedStateHeader = nil
                case .inActive, .trivial:
                    okButtonText = PrimeAppPopUpButtonText.Ok.rawValue
                    isErrorHeaderLogo = true
                    numberOfbuttons = 1
                    suspendedStateHeader = UtilityFunction.shared.getPrimeMessageStringFromPrimePopUpDataNode(1)
                default:
                    break
                }
                showPrimeAppAlertOnView(suspendedStateHeader, suspendedStateMessage, numberOfbuttons, okButtonText, cancelButtonText, true, false, isErrorHeaderLogo)
            }
        } else {
            let notSubscribedPrimeStateMessage = BAConfigManager.shared.configModel?.data?.config?.noActivePrimeIOSMessage ?? "You have not\nSubscribed to Amazon\nPrime via Tata Sky."
            showPrimeAppAlertOnView(notSubscribedPrimeStateMessage, "", 2, PrimeAppPopUpButtonText.existingUser.rawValue, PrimeAppPopUpButtonText.Cancel.rawValue, false, false, false)
        }
    }
    
    func showPrimeAppAlertOnView(_ header: String?, _ message: String, _ numberOfButtons: Int,_ okButtonTitle: String, _ cancelButtonTitle: String, _ isSuspended: Bool, _ increasePopUpCounter: Bool, _ isErrorHeaderLogo: Bool) {
        if BAReachAbility.isConnectedToNetwork() {
            
            if increasePopUpCounter && BAKeychainManager().primeAppAlertCount < 2 {
                BAKeychainManager().primeAppAlertCount += 1
            } else if increasePopUpCounter && BAKeychainManager().primeAppAlertCount >= 2 {
                self.checkToNavigateUserToPrimeAppOrAppStoreUrl()
                return
            }
            
            showPrimeProceedAlertWithCallback(header, numberOfButtons, okButtonTitle: okButtonTitle, cancelButtonTitle, message, isErrorHeaderLogo) {
                alertResponse in
                if isSuspended {
                    let eventType = 0
                    var paramValue = ""
                    switch self.primeAppSuspendedState {
                    case .active:
                        if alertResponse {
                            self.requestPrimeResumeAPi()
                        } else {
                            self.checkToNavigateUserToPrimeAppOrAppStoreUrl()
                        }
                        paramValue = alertResponse ? PrimeAppPopUpButtonText.resumePrime.rawValue : PrimeAppPopUpButtonText.Skip.rawValue
                    case .inActive, .trivial:
                        paramValue = PrimeAppPopUpButtonText.Ok.rawValue
                        break
                    default:
                        break
                    }
                    self.mixPanelEventForPrime(eventType, paramValue)
                } else {
                    if alertResponse {
                        self.checkToNavigateUserToPrimeAppOrAppStoreUrl()
                    }
                }
            }
        } else {
            noInternet()
        }
    }
    
    func checkToNavigateUserToPrimeAppOrAppStoreUrl() {
        if BAReachAbility.isConnectedToNetwork() {
            makeCWRequestCallForPrimeContent()
            primeContentPlayerMixPanelEvents()
            if UtilityFunction.shared.checkIsPrimeAppInstalledOrNot() {
                self.navigateToPrimeApp()
            } else {
                self.navigateToAppStorePrimeApp()
            }
        } else {
            noInternet()
        }
    }
    
    // As per discussion with Srikant, Vagish and FS team CW will be only called if content is not brand and series
    func makeCWRequestCallForPrimeContent() {
        if let contentType = self.contentListModel?.contentType {
            if (contentType.lowercased() != ContentType.brand.rawValue.lowercased()) && (contentType.lowercased() != ContentType.series.rawValue.lowercased()) {
                var totalDuration = 10
                if let _totalDuration = self.contentListModel?.duration {
                    totalDuration = _totalDuration/1000
                }
                continueWatchingVM.apiParams["id"] = self.contentListModel?.id ?? "-1"
                continueWatchingVM.apiParams["contentType"] =  self.contentListModel?.contentType ?? ""
                continueWatchingVM.apiParams["watchDuration"] = self.contentListModel?.secondsWatched ?? 1
                continueWatchingVM.apiParams["totalDuration"] = totalDuration
                continueWatchingVM.getWatchingtData({ _, _, isApiError  in
                })
            }
        }
    }
    
    func navigateToPrimeApp() {
        if let primeApiResponseURLPath = contentListModel?.providerContentId {
            //https://app.primevideo.com/detail?gti=amzn1.dv.gti.aeb6867d-f95d-9f6e-f9a1-07d78e23f509
            let primeBaseDeepLinkUrl = "https://app.primevideo.com/detail?gti="
//            let primeBaseDeepLinkUrl = "https://app.primevideo.com/watch?gti=" earlier used this
//            let primeDeepLinkAdditionalParams = "&time=auto&ref_=atv_cf_strg_ad"
            let finalPrimeDeepLinkURL = primeBaseDeepLinkUrl + primeApiResponseURLPath //+ primeDeepLinkAdditionalParams
            if let hotstarPlayLink = URL(string: finalPrimeDeepLinkURL), UIApplication.shared.canOpenURL(hotstarPlayLink) {
                UIApplication.shared.open(hotstarPlayLink, options: [:], completionHandler: nil)
            }
        }
    }
    
    func navigateToAppStorePrimeApp() {
        let urlStr = "https://apps.apple.com/in/app/amazon-prime-video/id545519333"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    
    func requestPrimeResumeAPi() {
        primeServiceVM.primeResumeService({ (success) in
            if success {
                self.mixPanelEventForPrime(1, "")
            }
        })
    }
    
    func mixPanelEventForPrime(_ eventType: Int, _ paramValue: String) {
        var eventName = ""
        var eventProperties = [String: Any]()
        switch eventType {
        case 0:
            eventName = MixpanelConstants.Event.primeSuspended.rawValue
            eventProperties = [MixpanelConstants.ParamName.sid.rawValue: BAKeychainManager().sId, MixpanelConstants.ParamName.action.rawValue: paramValue]
            break
        case 1:
            eventName = MixpanelConstants.Event.primeResumeSuccess.rawValue
            eventProperties = [MixpanelConstants.ParamName.sid.rawValue: BAKeychainManager().sId]
            break
        default:
            break
        }
        MixpanelManager.shared.trackEventWith(eventName: eventName, properties: eventProperties)
    }
    
    
    func primeContentPlayerMixPanelEvents() {
        var contentTitle = ""
        var contentId = ""
        var contentType = ""
        var partnerName = ""
        let genreResult = self.contentListModel?.genre?.joined(separator: ",") ?? ""
        let languageResult = self.contentListModel?.language?.joined(separator: ",") ?? ""
        contentType = self.contentListModel?.contentType ?? ""
        switch contentType {
        case ContentType.series.rawValue, ContentType.customSeries.rawValue:
            contentTitle = self.contentListModel?.seriesTitle ?? ""
        default:
            contentTitle = self.contentListModel?.title ?? ""
        }
        contentId = String(self.contentListModel?.contentId ?? -1)
        print(contentId)
        partnerName = self.contentListModel?.provider ?? "Prime"
        let railTitle = BAConfigManager.shared.configModel?.data?.config?.subscribedTitle ?? "Prime Rail"
        playbackEventTracking(contentTitle: contentTitle, contentType: contentType, contentGenre: genreResult, partnerName: partnerName, languages: languageResult, railName: railTitle, pageSource: pageSource, configSource: "Recommended")
    }

    func playbackEventTracking(contentTitle: String, contentType: String, contentGenre: String, partnerName: String, languages: String, railName: String, pageSource: String, configSource: String) {
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.playContent.rawValue,
                                              properties: [MixpanelConstants.ParamName.contentTitle.rawValue: contentTitle,
                                                           MixpanelConstants.ParamName.contentType.rawValue: contentType,
                                                           MixpanelConstants.ParamName.contentGenre.rawValue: contentGenre,
                                                           MixpanelConstants.ParamName.startTime.rawValue: "",
                                                           MixpanelConstants.ParamName.stopTime.rawValue: "",
                                                           MixpanelConstants.ParamName.initialBufferTimeMinutes.rawValue: "",
                                                           MixpanelConstants.ParamName.initialBufferTimeSeconds.rawValue: "",
                                                           MixpanelConstants.ParamName.numberOfResume.rawValue: "",
                                                           MixpanelConstants.ParamName.numberOfPause.rawValue: "",
                                                           MixpanelConstants.ParamName.duraionMinunte.rawValue: "",
                                                           MixpanelConstants.ParamName.durationSeconds.rawValue: "",
                                                           MixpanelConstants.ParamName.partnerName.rawValue: partnerName,
                                                           MixpanelConstants.ParamName.contentLanguage.rawValue: languages,
                                                           MixpanelConstants.ParamName.vodRail.rawValue: railName,
                                                           MixpanelConstants.ParamName.origin.rawValue: configSource,
                                                           MixpanelConstants.ParamName.source.rawValue: pageSource,
                                                           MixpanelConstants.ParamName.packName.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? "",
                                                           MixpanelConstants.ParamName.packPrice.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? "",
                                                           MixpanelConstants.ParamName.packType.rawValue:BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""])
    }
}

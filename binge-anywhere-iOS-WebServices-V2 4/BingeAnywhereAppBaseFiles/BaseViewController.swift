//
//  BaseViewController.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import ARSLineProgress
import Kingfisher
import SonyLIVSDK

class BaseViewController: UIViewController, NetworkDataProtocol/*, NetworkDataShowLoaderProtocol*/ {
    
    var pageMenu: CAPSPageMenu?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .BAdarkBlueBackground
        //        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //        navigationItem.backBarButtonItem = UIBarButtonItem(image: UIImage(named: "NavigationBackButton"), style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.releaseSonyLivPlayer()
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        if self is InitialLandingController || self is HomeVC || self is KidsVC || self is TVShowsVC || self is MoviesVC || self is ApiSuccessViewController {
            self.navigationController?.navigationBar.isHidden = true
        } else if self is LoginVC {
            //hideActivityIndicator()
            self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            self.navigationController?.navigationBar.isHidden = false
            self.navigationItem.setHidesBackButton(true, animated: true);
        }else if self is MarketingViewController {
            //hideActivityIndicator()
            self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            self.navigationController?.navigationBar.isHidden = true
            self.navigationItem.setHidesBackButton(true, animated: true);
        } else {
            //hideActivityIndicator()
            self.navigationItem.setHidesBackButton(false, animated: true);
            self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            self.navigationController?.navigationBar.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func releaseSonyLivPlayer() {
        if BAKeychainManager().isSonyLivLoggedIn {
            SonyLIVSDKManager.stopContent()
            BAKeychainManager().isSonyLivLoggedIn = false//to get orientation lock back when user comes from Sony Liv Player
            // for testing sony liv orientation issue
            UtilityFunction.shared.performTaskAfterDelay(2) {
                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            }
        }
    }
    
    func configureNavigationBar(with navigationType: NavigationType, _ leftAction: Selector?, _ rightAction: Selector?, _ target: UIViewController?, _ title: String = "", _ provider: String = "") {
        if #available(iOS 13.4, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = .BAdarkBlueBackground
            appearance.shadowImage = UIImage()
            appearance.shadowColor = .clear
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            self.navigationController?.navigationBar.barTintColor = .BAdarkBlueBackground
            self.navigationController?.navigationBar.tintColor = .BAwhite
            self.navigationItem.leftBarButtonItem = nil
        }
        
        switch navigationType {
        case .updateRightBarButton:
            addBackButtonToNav(leftAction!, target!)
        default:
            self.navigationItem.title = title
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.skyTextFont(.medium, size: 18), NSAttributedString.Key.foregroundColor: UIColor.BAwhite]
        
        switch navigationType {
        case .backButton:
            addBackButtonToNav(leftAction!, target!)
        case .backWithText:
            addBackButtonToNavWithText(leftAction!, target!)
        case .backButtonAndCentreImage:
            addBackButtonToNav(leftAction!, target!)
            centreImageToNav(provider: provider)
        case .backButtonAndTitle:
            addBackButtonToNav(leftAction!, target!)
        case .backAndSearchBar:
            addBackButtonWithSearchBar(leftAction!, target!)
            searchBarToNav(false)
        case .clearBackground:
            clearBackgroundToNav()
        case.centreImage:
            centreImageToNav(provider: provider)
        case .leftAndRightBar:
            navBarWithLeftAndRightButton(leftAction!, rightAction!, target!)
        case .centerTitle:
            break
        case .searchBar:
            searchBarToNav(true)
        case .updateRightBarButton:
            updateRightBarButton(title, rightAction!, target!)
        }
    }
    
    func updateRightBarButton(_ title: String, _ action: Selector, _ target: UIViewController) {
        self.navigationItem.rightBarButtonItems?.removeAll()
        let rightSideBtn = UIButton()
        rightSideBtn.setTitle(title, for: .normal)
        rightSideBtn.setTitleColor(.BABlueColor, for: .normal)
        rightSideBtn.addTarget(target, action: action, for: .touchUpInside)
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: rightSideBtn)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    func addBackButtonToNav( _ action: Selector, _ target: UIViewController) {
        let leftSideBtn = UIButton()
        let backImage = UIImage(named: "backNew")?.withRenderingMode(.automatic)
        leftSideBtn.setImage(backImage, for: .normal)
        leftSideBtn.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.addTarget(target, action: action, for: .touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftSideBtn)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func addBackButtonToNavWithText( _ action: Selector, _ target: UIViewController) {
        let leftSideBtn = UIButton()
        let backImage = UIImage(named: "backNew")?.withRenderingMode(.automatic)
        leftSideBtn.setImage(backImage, for: .normal)
        leftSideBtn.setTitle(" Back To Binge", for: .normal)
        leftSideBtn.widthAnchor.constraint(equalToConstant: 142.0).isActive = true
        leftSideBtn.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.addTarget(target, action: action, for: .touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftSideBtn)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func addBackButtonWithSearchBar( _ action: Selector, _ target: UIViewController) {
        /// Custom button is used to move bar button to left for alignment, by overriding alignmentRectInsets function.
        let leftSideBtn = BarButton(type: .custom)
        let backImage = UIImage(named: "backNew")?.withRenderingMode(.automatic)
        leftSideBtn.setImage(backImage, for: .normal)
        ///Image inside the button is moved to the left to align with other elements.
        leftSideBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -22, bottom: 0, right: 0)
        leftSideBtn.translatesAutoresizingMaskIntoConstraints = false
        leftSideBtn.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.addTarget(target, action: action, for: .touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftSideBtn)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func centreImageToNav(provider: String?) {
        let logoImage = UIImage.init(named: "NavigationHeaderLogo")
        let logoImageView = UIImageView.init(image: logoImage)
        var url = URL.init(string: "")
        switch provider?.uppercased() {
        case ProviderType.prime.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.PRIME?.logoRectangular ?? "")
        case ProviderType.sunnext.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SUNNXT?.logoRectangular ?? "")
        case ProviderType.shemaro.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SHEMAROOME?.logoRectangular ?? "")
        case ProviderType.vootKids.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTKIDS?.logoRectangular ?? "")
        case ProviderType.vootSelect.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.VOOTSELECT?.logoRectangular ?? "")
        case ProviderType.hungama.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HUNGAMA?.logoRectangular ?? "")
        case ProviderType.zee.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.ZEE5?.logoRectangular ?? "")
        case ProviderType.hotstar.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.HOTSTAR?.logoRectangular ?? "")
        case ProviderType.sonyLiv.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.SONYLIV?.logoRectangular ?? "")
        case ProviderType.eros.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.EROSNOW?.logoRectangular ?? "")
        case ProviderType.curosityStream.rawValue: url = URL.init(string: BAConfigManager.shared.configModel?.data?.config?.providerLogo?.CURIOSITYSTREAM?.logoRectangular ?? "")
        case ProviderType.hopster.rawValue: logoImageView.image = UIImage(named: "hopster")
        case ProviderType.netflix.rawValue: logoImageView.image = UIImage(named: "netflix")
        default:
            logoImageView.image = UIImage(named: "NavigationHeaderLogo")
        }
        if let _url = url {
            let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 100)
            widthConstraint.isActive = true
            logoImageView.alpha = 0
            logoImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
                UIView.animate(withDuration: 1, animations: {
                    logoImageView.alpha = 1
                })
            })
        } else {
            let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 150)
            widthConstraint.isActive = true
        }
        logoImageView.contentMode = .scaleAspectFit
        navigationItem.titleView =  logoImageView
    }
    
    func clearBackgroundToNav() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
    }
    
    func navBarWithLeftAndRightButton( _ action1: Selector, _ action2: Selector, _ target: UIViewController) {
        let leftSideBtn = UIButton()
        let rightSideBtn = UIButton()
        let backImage = UIImage(named: "backNew")?.withRenderingMode(.automatic)
        leftSideBtn.setImage(backImage, for: .normal)
        rightSideBtn.setTitle("Edit", for: .normal)
        rightSideBtn.setTitleColor(.BABlueColor, for: .normal)
        leftSideBtn.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        leftSideBtn.addTarget(target, action: action1, for: .touchUpInside)
        rightSideBtn.addTarget(target, action: action2, for: .touchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftSideBtn)
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: rightSideBtn)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    func searchBarToNav(_ hideBackButton: Bool) {
        var searchBar = UISearchBar()
        let searchbarSize = ScreenSize.screenWidth - 28
        searchBar = UISearchBar(frame: CGRect(x: 28, y: 8, width: searchbarSize, height: 26))
        searchBar.placeholder = "Search for a movie or show"//"Search for a TV Show or Movie title"
        searchBar.barStyle = .default
        searchBar.tintColor = .BAdarkBlue1Color
        searchBar.searchTextPositionAdjustment = UIOffset(horizontal: 6, vertical: 0)
        searchBar.setImage(UIImage(named: "close"), for: .clear, state: .normal)
        searchBar.delegate = self as? UISearchBarDelegate
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = .BAwhite
            textfield.textColor = .black
            textfield.keyboardType = .asciiCapable
            textfield.borderStyle = .none
            textfield.font = .skyTextFont(.medium, size: 16)
            textfield.addDoneButtonOnKeyBoardWithControl()
            textfield.cornerRadius = 2
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [.foregroundColor: UIColor.BAdarkBlue1Color, NSAttributedString.Key.font: UIFont.skyTextFont(.medium, size: 16)])
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = .BAdarkBlueBackground
            }
        }
        if hideBackButton {
            navigationItem.leftBarButtonItem = nil
            navigationItem.leftBarButtonItems = nil
        }
        navigationItem.titleView = searchBar
    }
    
    func moveToSignUpScreen() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "RegistrationVC", type: RegistrationVC.self)
        navigationController?.pushViewController(pushView, animated: true)
    }
    
    func showBackAccountAlert(_ message: String?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButtonAccount(.backToAccounts, .withTopImage(.success), .withTitle(message ?? "Success"), .hidden, .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                completion?()
            }
        }
    }
    
    func showOkAlert(_ message: String) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .hidden, .bingeAnywhere, .withMessage(message), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
            }
        }
    }
    
    func showOkAlertWithCustomTitle(_ message: String, title: String) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(title), .withMessage(message), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
            }
        }
    }
    
    func popupNotification(_ message: String?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.noButton(.withTopImage(.success), .withTitle(message ?? "Success"), .hidden, .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                completion?()
            }
        }
    }
    
    func apiError(_ message: String?, title: String?, completion: (() -> Void)? = nil) {
        if !deviceLoggedOutPopUpAppear {
            deviceLoggedOutPopUpAppear = true
            DispatchQueue.main.async {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(title ?? ""), .withMessage(message ?? "The operation couldn’t be completed."), .hidden, .hidden)) { _ in
                    self.view.endEditing(true)
                    self.hideActivityIndicator()
                    deviceLoggedOutPopUpAppear = false
                    if message == kSessionExpire || message == "No device to unregister." {
                        UtilityFunction.shared.logoutUser()
                        kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    }
                    completion?()
                }
            }
        }
    }
    
    func rechargeAlert(_ message: String?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle("Activate Binge"), .withMessage(message ?? "The operation couldn’t be completed."), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                if message == kSessionExpire || message == "No device to unregister." {
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
                completion?()
            }
        }
    }
    
    func alertWithInactiveTitleAndMessage(_ message: String?, title: String?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(title ?? ""), .withMessage(message ?? "The operation couldn’t be completed."), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                if message == kSessionExpire || message == "No device to unregister." {
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
                completion?()
            }
        }
    }
    
    func tempSuspensionAlert(_ message: String?, title: String?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.skip, .withTopImage(.alert), .withTitle(title ?? ""), .withMessage(message ?? "The operation couldn’t be completed."), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                if message == kSessionExpire || message == "No device to unregister." {
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
                completion?()
            }
        }
    }
    
    func inactiveDTHAlert(_ message: String?, title: String?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(title ?? ""), .withMessage(message ?? "The operation couldn’t be completed."), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                if message == kSessionExpire || message == "No device to unregister." {
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
                completion?()
            }
        }
    }
    
    func noInternet(_ message: String = "", completion: (() -> Void)? = nil) {
        if !noInternetAppear {
            noInternetAppear = true
            DispatchQueue.main.async {
                kAppDelegate.window?.makeToast("No Internet Connection", duration: 1.0, point: .bottom, title: "", image: UIImage.init(named: "nointernet"), toastYPosition: tabBarHeight) { (boolVal) in
                    print("No Internet Appeared")
                    self.view.endEditing(true)
                    self.hideActivityIndicator()
                    noInternetAppear = false
                    completion?()
                }
            }
        }
    }
    
    
    func otpResent(_ message: String = "", completion: (() -> Void)? = nil) {
        if !noInternetAppear {
            noInternetAppear = true
            DispatchQueue.main.async {
                kAppDelegate.window?.makeToastOnlyForMessage(message) { (boolVal) in
                    print("OTP Resent")
                    self.view.endEditing(true)
                    self.hideActivityIndicator()
                    noInternetAppear = false
                    completion?()
                }
            }
        }
    }
    
    func noInternetForSplash(completion: (() -> Void)? = nil){
        let noInternetVC = NoInternetVC()
        noInternetVC.modalPresentationStyle = .fullScreen
        noInternetVC.modalTransitionStyle = .crossDissolve
        noInternetVC.passConfig {
            completion?()
        }
        self.present(noInternetVC, animated: true, completion: nil)
    }
    
//    func popupForFireTv(_ message: String = "", completion: ((Bool) -> Void)? = nil){
//        AlertController.initialization().showAlert(.doubleButton(.with(title: "Avail Now"), .cancel, .hidden, .withTitle(kFSPopupHeader), .withMessage("+ 3 months of Amazon Prime Video at no extra cost"), .amazonImage, .hidden)) { flag in
//            self.view.endEditing(true)
//            self.hideActivityIndicator()
//            completion?(flag)
//        }
//    }
    
    func popupForFireTv(_ message: String = "", completion: ((Bool) -> Void)? = nil){
        AlertController.initialization().showAlert(.doubleButtonWithBody(.with(title: "Avail Now"), .cancel, .hidden, .withTitle(kFSPopupHeader), .withBodyMessage(kFSPopupBody), .withMessage("+ 3 months of Amazon Prime Video at no extra cost"), .amazonImage, .hidden)) { (flag) in
            self.view.endEditing(true)
            self.hideActivityIndicator()
            completion?(flag)
        }
    }
    
    func deliveryFireStickPopup(_ message: String?, completion: (() -> Void)? = nil){
        AlertController.initialization().showAlert(.singleButton(.with(title: "Proceed"), .withTopImage(.success), .withTitle("Thank You"), .withMessage(message ?? "Your firestick will be delivered to your address by 28/3/2021"), .hidden, .hidden)) { _ in
            self.view.endEditing(true)
            self.hideActivityIndicator()
            self.navigationController?.popToRootViewController(animated: true)
            completion?()
        }
    }
    
    func deliveryPopupWithReschedule(_ message: String?, _ isDIY: Bool, completion:((Bool) -> Void)? = nil){
        var title = ""
        if isDIY {
            title = "Thank You"
        } else {
          title = "Installation Scheduled"
        }
        AlertController.initialization().showAlert(.singleButton(.with(title: "Start Watching Now"), .withTopImage(.success), .withTitle(title), .withMessage(message ?? "Details will be sent via SMS on your Registered Mobile Number."), .hidden, .hidden)) { flag in
            self.view.endEditing(true)
            self.hideActivityIndicator()
            completion?(flag)
        }
    }
    
    func noSubscription(_ message: String = "", completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton((completion == nil) ? .ok: .retry,
                                                                     .withTopImage(.upgradeSubscription),
                                                                     .withTitle("No Subscription"),
                                                                     (message != "") ? .withMessage(message) : .withMessage("You are not subscribed to Tata Sky Binge."),
                                                                     .hidden,
                                                                     .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                completion?()
            }
        }
    }
    
    func upgradeSubscription(_ message: String = "", completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton((completion == nil) ? .ok: .retry,
                                                                     .withTopImage(.upgradeSubscription),
                                                                     .withTitle(""),
                                                                     (message != "") ? .withMessage(message) : .withMessage("You are not subscribed to Tata Sky Binge Premium."),
                                                                     .hidden,
                                                                     .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                completion?()
            }
        }
    }
    
    func partnerPending(_ message: String = "", completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton((completion == nil) ? .ok: .retry,
                                                                     .withTopImage(.upgradeSubscription),
                                                                     .withTitle("Soon!"),
                                                                     (message != "") ? .withMessage(message) : .withMessage("Partner Integration in Process"),
                                                                     .hidden,
                                                                     .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                completion?()
            }
        }
    }
    
    func showAler123t() {
        let alert = UIAlertController(title: "Alert", message: "Prime App Content Cant be played in app", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        switch action.style {
                                        case .default:
                                            print("default")
                                        case .cancel:
                                            print("cancel")
                                        case .destructive:
                                            print("destructive")
                                            
                                        @unknown default:
                                            break
                                        }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showOkAlertWithCallback(_ message: String, completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .hidden, .hidden/*.bingeAnywhere*/, .withMessage(message), .hidden, .hidden)) { _ in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                completion()
            }
        }
    }
    
    func showHotstarProceedAlertWithCallback(_ header: String, _ message: String, completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            BAKeychainManager().hotStarAlertCount += 1
            let isHotStarInstalled = UtilityFunction.shared.isHotstarInstalled()
            AlertController.initialization().showAlert(.doubleButton( isHotStarInstalled ? .proceed : .install, .cancel, .withTopImage(.hungama), .withTitle(header), .withMessage(message), .hidden, .hidden), false, 20.0) { (response) in
                self.view.endEditing(true)
                self.hideActivityIndicator()
                if response {
                    completion()
                }
            }
        }
    }
    
    
    func showPrimeProceedAlertWithCallback(_ header: String?, _ numberOfButtons: Int, okButtonTitle: String?, _ cancelButtonTitle: String? ,_ message: String,_ isErrorHeaderLogo: Bool, completion: @escaping (Bool) -> Void) {
        DispatchQueue.main.async {
            if numberOfButtons == 1 {
                self.singleButtonHandlingForPartnerApp(header, okButtonTitle: okButtonTitle, cancelButtonTitle, message, isErrorHeaderLogo) { (userResponse) in
                    completion(userResponse)
                }
            } else if numberOfButtons == 2 {
                self.doubleButtonHandlingForPartnerApp(header, okButtonTitle: okButtonTitle ?? "Ok", cancelButtonTitle ?? "Cancel", message, isErrorHeaderLogo) { (userResponse) in
                    completion(userResponse)
                }
            }
        }
    }
    
    
    func singleButtonHandlingForPartnerApp(_ header: String?, okButtonTitle: String?, _ cancelButtonTitle: String? ,_ message: String,_ isErrorHeaderLogo: Bool, completion: @escaping (Bool) -> Void) {
        AlertController.initialization().showAlert(.singleButton(.with(title: okButtonTitle ?? "Ok"), (isErrorHeaderLogo ? .withTopImage(.alert) : .withTopImage(.prime)), .withTitle(header ?? ""), .withMessage(message), .hidden, .hidden), false) { (flagValue) in
            self.view.endEditing(true)
            self.hideActivityIndicator()
            completion(flagValue)
        }
    }
    
    
    func doubleButtonHandlingForPartnerApp(_ header: String?, okButtonTitle: String, _ cancelButtonTitle: String ,_ message: String,_ isErrorHeaderLogo: Bool, completion: @escaping (Bool) -> Void) {
        AlertController.initialization().showAlert(.doubleButton(.with(title: okButtonTitle), .with(title: cancelButtonTitle), .withTopImage(.prime), .withTitle(header ?? ""), .withMessage(message), .hidden, .hidden), false) { (flagValue) in
            self.view.endEditing(true)
            self.hideActivityIndicator()
            completion(flagValue)
        }
    }
    
    
    @objc func retryButtonAction() {
        
    }
}

//
//  TabBaseViewController.swift
//  BingeAnywhere
//
//  Created by Shivam on 26/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit
import Mixpanel
import ARSLineProgress
import HungamaPlayer

var tabBarHeight: CGFloat = 0.0
class TabBaseViewController: UITabBarController, UITabBarControllerDelegate, MoengageProtocol {
    
    private var bounceAnimation: CAKeyframeAnimation = {
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0, 1.4, 0.9, 1.02, 1.0]
        bounceAnimation.duration = TimeInterval(0.3)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        return bounceAnimation
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(showBadgeCountOnTabBarItem(at:)),name:Notification.Name(rawValue: "didReceiveNotification"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resetBadgeCount),name:Notification.Name(rawValue: "resetBadgeCount"),object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.backgroundColor = .BAdarkBlue1Color
        self.tabBar.unselectedItemTintColor = .BAlightBlueGrey
        self.tabBar.tintColor = .BAwhite
        tabBarHeight = self.tabBar.frame.height
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarHeight = self.tabBar.frame.height
        showBadgeCountOnTabBarItem(at: 3)
    }
    
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        // find index if the selected tab bar item, then find the corresponding view and get its image, the view position is offset by 1 because the first item is the background (at least in this case)
        guard let idx = tabBar.items?.firstIndex(of: item), tabBar.subviews.count > idx + 1, let imageView = tabBar.subviews[idx + 1].subviews.compactMap({ $0 as? UIImageView }).first else {
            return
        }
        imageView.layer.add(bounceAnimation, forKey: nil)
        print("Selected item")
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        // to remove hungama player object whenever tab switch
        HungamaPlayerManager.shared.stop()
        HungamaPlayerManager.shared.releasePlayer()
        BAKeychainManager().isSonyLivLoggedIn = false
        BAKeychainManager().isOnEditProfileScreen = false
        // nil crash removed by checking with gurad let
        guard let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController) else {
            return
        }
        switch selectedIndex {
        case 0:
            let topView = UIApplication.shared.topMostViewController()
            if let navController = topView?.navigationController {
                let HomeLandController = checkForHomeLandingViewController(navController)
                if HomeLandController != nil {
                    MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeMain.rawValue)
                    UtilityFunction.shared.performTaskInMainQueue {
                        (HomeLandController as! HomeLandingViewController).createHomeTitleArray()
                        (HomeLandController as! HomeLandingViewController).pageMenu?.moveToPage(0)
                        (HomeLandController as! HomeLandingViewController).scrollToSelectedPage(0)
                        //(((HomeLandController as! HomeLandingViewController).pageMenu?.controllerArray[0]) as? HomeVC)?.refreshHomeData()
                    }
                    if navController.viewControllers.count > 1 {
                        navController.popToRootViewController(animated: true)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
                    print("first tab bar was selected")
                }
            }
        case 1:
            let topView = UIApplication.shared.topMostViewController()
            if let navController = topView?.navigationController {
                if navController.viewControllers.count > 1 {
                    if let controller = ((navController.viewControllers[0]) as? SearchLandingVC) {
                        controller.isFromTabChange = true
                        print("second tab bar was selected11111111")
                        if controller.noDataLabelHeightConstraint.constant > 0 {
                            controller.setNoDataLabelText(false)
                        }
                    }
                    navController.popToRootViewController(animated: true)
                } else if navController.viewControllers.count == 1 {
                    if let controller = ((navController.viewControllers[0]) as? SearchLandingVC) {
                        controller.isFromTabChange = true
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfigureSearchTab"), object: nil)
                        print("second tab bar was selected2222222")
                        if controller.noDataLabelHeightConstraint != nil {
                            controller.setNoDataLabelText(false)
                        }
                    }
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
            print("second tab bar was selected")
        case 2:
            let topView = UIApplication.shared.topMostViewController()
            if let navController = topView?.navigationController {
                if navController.viewControllers.count > 1 {
                    navController.popToRootViewController(animated: true)
                }
            }
        case 3:
            let topView = UIApplication.shared.topMostViewController()
            if let navController = topView?.navigationController {
                if navController.viewControllers.count > 1 {
                    navController.popToRootViewController(animated: true)
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
            print("third tab bar was selected")
        case 4:
            let topView = UIApplication.shared.topMostViewController()
            if let navController = topView?.navigationController {
                if navController.viewControllers.count > 1 {
                    navController.popToRootViewController(animated: true)
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeObservers"), object: nil)
        default:
            return
        }
    }
    
    func checkForHomeLandingViewController(_ navControl: UINavigationController) -> UIViewController? {
        let arr = navControl.viewControllers
        for controller in arr where controller is HomeLandingViewController {
            return controller
        }
        return nil
    }
    
}

extension TabBaseViewController {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController) {
            switch selectedIndex {
            case 0:
                if let controller = tabBarController.viewControllers?[0] {
                    if let vc = controller as? UINavigationController {
                        let vcArray = vc.viewControllers
                        if vcArray.count > 0 {
                            if let vc = vcArray[0] as? HomeLandingViewController {
                                vc.firstTabSelected = true
                            }
                        }
                    }
                }
            default:
                break
            }
        }

        
        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
            return false // Make sure you want this as false
        }
        
        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.2, options: [.transitionCrossDissolve], completion: nil)
        }
        return true
    }
    
    
    // In order to hotfix the notification badge icon and list added all these hot fix to let it work properly nead to update once release is done
    @objc func showBadgeCountOnTabBarItem(at index: Int) {
        if let tabItems = self.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            if getUnreadMessageCount() > 0 {
                tabItem.badgeValue = "●"
                tabItem.badgeColor = .clear
                tabItem.setBadgeTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.BAerrorRed], for: .normal)
            }else{
                tabItem.badgeValue = nil
            }
        }
    }
    
    @objc func resetBadgeCount() {
        if let tabItems = self.tabBar.items {
            let tabItem = tabItems[3]
            tabItem.badgeValue = nil
        }
    }
    
}
/*
 
 if selectedIndex == 0 {
 let topView = UIApplication.shared.topMostViewController()
 if let navController = topView?.navigationController {
 let HomeLandController = checkForHomeLandingViewController(navController)
 if HomeLandController != nil {
 (HomeLandController as! HomeLandingViewController).pageMenu?.moveToPage(0)
 print("first tab bar was selected")
 }
 }
 } else {
 //do whatever
 }*/

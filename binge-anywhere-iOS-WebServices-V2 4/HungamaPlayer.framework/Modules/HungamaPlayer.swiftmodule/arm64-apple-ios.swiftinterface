// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target arm64-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name HungamaPlayer
import AVFoundation
import CommonCrypto
import Foundation
import HA
@_exported import HungamaPlayer
import Swift
import SystemConfiguration
import UIKit
public enum PlayerState : Swift.Int {
  case idle
  case loading
  case buffering
  case ready
  case ended
  case unknown
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class HungamaPlayerManager : ObjectiveC.NSObject {
  public static let shared: HungamaPlayer.HungamaPlayerManager
  public func setSDKMode(_ mode: HungamaPlayer.SDKMode)
  public func setPartnerUniqueId(_ partnerUniqueId: Swift.String)
  public func load(content: HungamaPlayer.IContentVO, withContentLoadListener listener: HungamaPlayer.OnPlayerContentLoadListener) throws
  public func initializePlayer() throws
  public func preparePlayer(forPlayerView playerView: HungamaPlayer.HungamaPlayerView?, withStateChangeListener listener: HungamaPlayer.OnPlayerStateChangeListener?) throws
  public func start()
  public func isPlaying() -> Swift.Bool
  public func getCurrentPosition() -> Swift.Int
  public func getTotalDuration() -> Swift.Int
  public func getContentBufferedPosition() -> Swift.Int
  public func getTotalBufferedDuration() -> Swift.Int
  public func togglePlayPause()
  public func seek(to duration: Swift.Int)
  public func stop()
  public func getPlaybackVarints() -> [Swift.String]
  public func getCurrentPlaybackVariant() -> Swift.String
  public func setPlaybackVariant(_ variant: Swift.String)
  public func isSubtitleAvailable() -> Swift.Bool
  public func getSubtitleLanguages() -> [Swift.String]
  public func setSubtitleLanguage(_ language: Swift.String)
  public func isSubtitleEnabled() -> Swift.Bool
  public func setSubtitleEnabled(_ enabled: Swift.Bool)
  public func isMuted() -> Swift.Bool
  public func setMuted(_ muted: Swift.Bool)
  public func releasePlayer()
  @objc deinit
}
public protocol IContentVO : AnyObject {
  func getId() -> Swift.String
  func getTitle() -> Swift.String
  func getPosterURL() -> Swift.String
  func getType() -> HungamaPlayer.ContentType
}
public protocol OnPlayerContentLoadListener : AnyObject {
  func onContentLoadSuccess()
  func onContentLoadFailed(withError errorMessage: Swift.String)
}
public protocol OnPlayerStateChangeListener : AnyObject {
  func onPlayer(stateChanged state: HungamaPlayer.PlayerState)
  func onPlayer(error: HungamaPlayer.HungamaPlayerError)
}
public enum HungamaPlayerError : Swift.Error {
  case userIdNotAvailable(Swift.String)
  case contentNotAvailable(Swift.String)
  case unsupportedDRM(Swift.String)
  case invalidInput(Swift.String)
  case playbackIssue(Swift.String)
  case networkNotAvailable(Swift.String)
  case contentURLNotAvailable(Swift.String)
  case playerViewNotAvailable(Swift.String)
}
public enum SDKMode : Swift.String {
  case production
  case uat
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@objc @_inheritsConvenienceInitializers public class HungamaPlayerView : UIKit.UIView {
  public var logoBottomMargin: CoreGraphics.CGFloat {
    get
    set
  }
  public var subtitleBottomMargin: CoreGraphics.CGFloat {
    get
    set
  }
  public var videoGravity: AVFoundation.AVLayerVideoGravity {
    get
    set
  }
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public class var layerClass: Swift.AnyClass {
    @objc get
  }
  @objc deinit
}
public enum ContentType : Swift.Int {
  case movie
  case tvShowEpisode
  case musicVideo
  case shortVideo
  case unknown
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
  public init?(rawValue: Swift.Int)
}
extension HungamaPlayer.PlayerState : Swift.Equatable {}
extension HungamaPlayer.PlayerState : Swift.Hashable {}
extension HungamaPlayer.PlayerState : Swift.RawRepresentable {}
extension HungamaPlayer.SDKMode : Swift.Equatable {}
extension HungamaPlayer.SDKMode : Swift.Hashable {}
extension HungamaPlayer.SDKMode : Swift.RawRepresentable {}
extension HungamaPlayer.ContentType : Swift.Equatable {}
extension HungamaPlayer.ContentType : Swift.Hashable {}
extension HungamaPlayer.ContentType : Swift.RawRepresentable {}

//
//  FacebookAnalytics.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 18/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import FacebookCore

class FacebookAnalytics {
    internal static let tracker: FacebookAnalytics = {
        return FacebookAnalytics()
    }()
    
    //Initializer access level change now
    private init () {
        print("FacebookAnalytics Intialised")
    }
   
    func trackEvent(with eventName: String, parameters: [String : Any]? = nil){
        guard eventName.contains("LOGIN") else {return} /*|| eventName.contains("LOGOUT")*/
        var allParameters = parameters
        // Source don't need to be track, in login failed
        if (eventName == MixpanelConstants.Event.loginFailed.rawValue){
            allParameters?.removeValue(forKey: MixpanelConstants.ParamName.source.rawValue)
        }
        
        print("Facebook Analytics Event: \(eventName)\n FirebaseProperties: \(String(describing: allParameters))")
        if let _parameters = allParameters {
            AppEvents.logEvent(AppEvents.Name(rawValue: eventName), parameters: _parameters)
        } else {
            AppEvents.logEvent(AppEvents.Name(rawValue: eventName))
        }
    }
}

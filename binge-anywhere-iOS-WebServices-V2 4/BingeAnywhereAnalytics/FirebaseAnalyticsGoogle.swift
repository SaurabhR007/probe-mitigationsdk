//
//  FirebaseAnalyticsGoogle.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 19/05/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class FirebaseAnalyticsManager {
    internal static let tracker: FirebaseAnalyticsManager = {
        return FirebaseAnalyticsManager()
    }()
    
    //Initializer access level change now
    private init () {
        print("FirebaseAnalytics Intialised")
    }
    
    func trackEvent(with eventName: String, parameters: [String : Any]? = nil){
        guard eventName.contains("LOGIN") else {return} /*|| eventName.contains("LOGOUT")*/
        var allParameters = parameters
        // Source don't need to be track, in login failed
        if (eventName == MixpanelConstants.Event.loginFailed.rawValue){
            allParameters?.removeValue(forKey: MixpanelConstants.ParamName.source.rawValue)
        }
        
        let updatedEventName = eventName.replacingOccurrences(of: "-", with: "_")
        print("Firebase Analytics Event: \(updatedEventName)\n FirebaseProperties: \(String(describing: allParameters))")
        if let _parameters = allParameters {
            Analytics.logEvent(updatedEventName, parameters: _parameters)
        } else {
            Analytics.logEvent(updatedEventName, parameters: allParameters)
        }
    }
}

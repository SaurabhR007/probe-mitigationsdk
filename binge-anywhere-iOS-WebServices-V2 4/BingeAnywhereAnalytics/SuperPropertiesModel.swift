//
//  SuperPropertiesModel.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 07/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

struct SuperPropertiesModel {
    var deviceID: String = kDeviceId
    var sID: String = String(BAKeychainManager().acccountDetail?.subscriberId ?? 0)
    var rmn: String = (BAKeychainManager().mobileNumberForSuperProperty.isEmpty ? BAKeychainManager().acccountDetail?.rmn : BAKeychainManager().mobileNumberForSuperProperty) ?? ""//BAKeychainManager().acccountDetail?.rmn ?? "" //BAKeychainManager().mobileNumber
	var email: String = BAKeychainManager().acccountDetail?.emailId ?? ""
    var profileName: String = BAKeychainManager().acccountDetail?.aliasName ?? ""
    var loggedInCount: String = BAKeychainManager().acccountDetail?.deviceLoginCount ?? "0"
    var dateOfSubscription: String = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packCreationDate ?? ""
    var premiumUser = "false"
    var pvr = "false"
	var noOfProfile = BAKeychainManager().acccountDetail?.baId ?? 1
    var bingeAccountCount: String = BAKeychainManager().acccountDetail?.bingeAccountCount ?? "0"
    var subscriber = BAKeychainManager().acccountDetail?.accountStatus == kActive ? "YES" : "NO"
    var isATVUser = BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.atv.rawValue ? "YES" :"NO"
    var isFireTV = BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.stick.rawValue ? "YES" :"NO"
    var firstTimeLogin = BAKeychainManager().acccountDetail?.firstTimeLoginDate ?? ""
    var firstName = BAKeychainManager().acccountDetail?.firstName ?? ""
    var lastName = BAKeychainManager().acccountDetail?.lastName ?? ""
    var freeTrial = (BAKeychainManager().acccountDetail?.freeTrialAvailed ?? false) ? "YES" : "NO"

    // New User Properties to solve the business use-cases
    var packName = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packName ?? ""
    var packPrice = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.packPrice ?? ""
    var subscriptionType = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.subscriptionType ?? ""

    /*
    func getSuperProperties() -> [String: String] {
        return ["SID": sID,
                "DEVICE-ID": deviceID,
                "ACTIVE-PROFILE": "",
                "RMN": rmn,
                "EMAIL": email,
                "NAME": profileName,
                "PREMIUM-USER": premiumUser,
                "DATE_OF_SUBSCRIPTION": dateOfSubscription,
                "BINGE ACCOUNTS COUNT": bingeAccountCount,
                "LOGGED_IN_DEVICE_COUNT": loggedInCount,
                "SUBSCRIBED": subscriber,
                "PVR": pvr]
    }*/
    
    func getMixpanelSuperProperties() -> [String: String] {
        return ["FIRST-NAME": firstName,
                "LAST-NAME": lastName,
                "$name": firstName + lastName,
                "SID": sID,
                "RMN": rmn,
                "EMAIL": email,
                "DATE-OF-SUBSCRIPTION": dateOfSubscription,
                "BINGE-ACCOUNTS-COUNT": bingeAccountCount,
                "LOGGED-IN-DEVICE-COUNT": loggedInCount,
                "SUBSCRIBED": subscriber,
                "FIRE-TV": isFireTV,
                "ATV": isATVUser,
                "FIRST-TIME-LOGIN": firstTimeLogin,
                "FREE-TRIAL": freeTrial,
                "PACK-NAME" : packName,
                "PACK-PRICE" : packPrice,
                "SUBSCRIPTION-TYPE" : subscriptionType
        ]
    }
    
    func getSuperProperties() -> [String: String] {
            return ["SID": sID
            ]
    }
}

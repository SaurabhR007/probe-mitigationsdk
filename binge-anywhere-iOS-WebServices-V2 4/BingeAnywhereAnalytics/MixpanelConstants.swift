//
//  MixpanelConstants.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 30/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

enum MixpanelConstants {
    enum Event: String {
        case appLaunch = "APP-LAUNCH"
        case homeMain = "HOME-MAIN"
        case loginInitiate = "LOGIN-INITIATE"
        case loginEnter = "LOGIN-ENTER"
        case loginOtpInvoke = "LOGIN-OTP-INVOKE"
        case loginOtpReceived = "LOGIN-OTP-RECEIVED"
        case initialBufferTime = "INITIAL-BUFFER-TIME"
        case pauseContent = "PAUSE-CONTENT"
        case resumeContent = "RESUME-CONTENT"
        case watchDurationOnDemand = "WATCH-DURATION-ONDEMAND"
        case restartClick = "RESTART-CLICK"
        case viewContentDetail = "VIEW-CONTENT-DETAIL"//"DETAIL-CLICK"
        case viewContentFavourite = "VIEW-CONTENT-FAVOURITE"
        case addContentFavourite = "ADD-CONTENT-FAVOURITE"
        case playTrailer = "PLAY-TRAILER"
        case loginOtpEnter = "LOGIN-OTP-ENTER"
        case loginOtpResend = "LOGIN-OTP-RESEND"
        case loginSuccess  = "LOGIN-SUCCESS"
        case loginPassword = "LOGIN-PASSWORD"
        case forgotPassword = "FORGOT-PASSWORD"
        case passwordChangeSuccess = "PASSWORD-RESET-SUCCESS"
        case passwordChangeFailed = "PASSWORD-RESET-FAILED"
        case logout = "LOGOUT"
        case logoutFailed = "LOGOUT-FAILED"
        case loginFailed = "LOGIN-FAILED"
        case playContent = "PLAY-CONTENT"
        case homeOnDemand  = "HOME-ON-DEMAND"
        case homeWatchlist  = "HOME-WATCHLIST"
        case failurePlayback = "PLAYBACK-FAILURE"
        
        //Watchlist
        case viewWatchList = "VIEW-WATCHLIST"//"VIEW-FAVORITE"
        case deleteFavorite = "DELETE-FAVORITE"
        
        //Transaction History
        case transactionHistory = "TRANSACTION-HISTORY"
        case editProfile = "EDIT-PROFILE"
        case homeAccount = "HOME-ACCOUNT"
        case notificationSetting = "NOTIFICATION-SETTINGS"
        
        //ViewHistory
        case viewHistory = "VIEW-HISTORY"
        case editViewHistory = "EDIT-VIEW-HISTORY"
        case deleteViewHistory = "DELETE-VIEW-HISTORY"

        //search related
        case search = "SEARCH"
        case noSearchResult = "NO-SEARCH-RESULT"
        case searchResult = "SEARCH-RESULT"
        case searchMic = "SEARCH-MIC"
        case searchVoicePermission = "SEARCH-VOICE-PERMISSION"
        case searchReactivateVoice = "SEARCH-REACTIVATE-MIC"

        //See All
        case seeAllFromHome = "SEE-ALL"//"HOME-SEE-ALL"
        case seeAllFromOnDemand = "ON-DEMAND-HOME-SEE-ALL"

        //Device Management
        case listDevices = "LIST-DEVICES"
        case removeDevice = "REMOVE-DEVICE"
        case maxDeviceLimit = "MAX-DEVICE-LIMIT"
        case autoPlayTrailer = "AUTOPLAY-TRAILER-SETTING"

        //Help & FAQ
        case emailUs = "EMAIL-US"
        case raiseRequest = "RAISE-REQUEST"
        case chatWithTS = "CHAT-WITH-TATASKY"
        case callTataSky = "CALL-TATASKY"
        case faqView = "FAQ-VIEW"

        //Main Home Page
        case homeClick = "HOME-CLICK"
        case liveHomeClick = "LIVE-HOME-CLICK"
        case onDemandHomeClick = "ON-DEMAND-HOME-CLICK"
        case homePageView = "HOME-PAGE-VIEW"
        case contactUs = "CONTACT-US"
        case privacyPolicy = "PRIVACY-POLICY"
        case terms = "TERMS-CONDITIONS"
        
        case shareWatsapp = "SHARE-WHATSAPP"
        //case shareGeneral = ""

        //Detail View
        case viewOnDemand = "VIEW-ONDEMAND"

        //Player related
        case changeVideoOrientation = "CHANGE-VIDEO-ORIENTATION"
        case resumeCount = "NUMBER-OF-RESUME"
        case pauseCount = "NUMBER-OF-PAUSE"
        case appUpdated = "$ae_updated"
        case allowTransactionNotification = "ALLOW-TRANSACTION-NOTIFICATION"
        case allowOfferNotificaion = "ALLOW-OFFER-NOTIFICATION"
        case allowWatchNotification = "ALLOW-WATCH-NOTIFICATION"
        case switchProfile = "SWITCH-PROFILE"
        case viewNotification = "VIEW-NOTIFICATION"
        case deleteNotifcation = "DELETE-NOTIFICATION"
        case searchStart = "SEARCH-START"
        case selectVideoQuality = "SELECT-VIDEO-QUALITY"
        case durationSeconds = "DURATION-SECONDS"
        case registerOnTataSky = "REGISTER-ON-TATASKY"
        
        // Password Related Events
        case updatePassword = "UPDATE-PASSWORD-INVOKED"
        case updatePasswordSucess = "UPDATE-PASSWORD-SUCCESS"
        case updatePasswordFailed = "UPDATE-PASSWORD-FAILED"
        case updateEmail = "UPDATE-PROFILE"
        
        
        //Prime Related Events
        case primeSuspended = "PRIME-SUSPEND-ALERT"
        case primeResumeSuccess = "PRIME-RESUME-SUCCESS"

    }

    enum ParamName: String {
        case source = "SOURCE"
        case value = "VALUE"
        case toBaid = "TO-BAID"
        case sid = "SID"
        case origin  = "ORIGIN"
        case enableNotification = "ENABLE-NOTIFICATION"
        case railPosition = "RAIL-POSITION"//"SECTION-POSITION"
        case contentType = "CONTENT-TYPE"
        case configType = "CONFIG-TYPE"
        case railTitle = "RAIL-TITLE"//"TITLE-SECTION"
        case vodRail = "RAIL"
        case pageName = "PAGE-NAME"
        case partner = "PARTNER"
        case section = "SECTION"
        case contentTitle = "CONTENT-TITLE"
        case contentPosition = "CONTENT-POSITION-SECTION"
        case heroBannerNumber = "HERO-BANNER-NUMBER"
        case genre = "GENRE"
        case subscribed = "SUBSCRIBED"
        case enabled = "ENABLED"
        case contentGenre = "CONTENT-GENRE"
        case contentLanguage = "CONTENT-LANGUAGE"
        case type = "TYPE"
        case auth = "AUTH"
        case reason = "REASON"
        case quality = "QUALITY"
        case partnerName = "PARTNER-NAME"
        case watchNotification = "WATCH-NOTIFICATIONS"
        case allowed = "ALLOWED"
        case durationSeconds = "DURATION-SECONDS"
        case duraionMinunte = "DURATION-MINUTES"
        case playingHome = "PLAYING-MODE"
        case keyWord = "KEYWORD"
        case searchAccess = "SEARCH-ACCESS"
        case screenName = "SCREEN-NAME"
        case searchType = "SEARCH-TYPE"
        case searchCount = "SEARCH-COUNT"
        case name = "NAME"
        case partnerHome = "PARTNER-HOME"
        case startTime = "START-TIME"
        case email = "EMAIL-ID"
        case stopTime = "STOP-TIME"
        case initialBufferTimeSeconds = "INITIAL-BUFFER-TIME-SECONDS"
        case initialBufferTimeMinutes = "INITIAL-BUFFER-DURATION-MINUTES"
        case numberOfPause = "NUMBER-OF-PAUSE"
        case numberOfResume = "NUMBER-OF-RESUME"
        case action = "ACTION"
        case packType = "PACK-TYPE"
        case packName = "PACK-NAME"
        case packPrice = "PACK-PRICE"
    }

    enum ParamValue: String {
        case welcome = "WELCOME"
        case appLaunch = "App Launch"
        case trueValue = "true"
        case falseValue = "false"
        case editorial = "Editorial"
        case rail = "Rail"
        case recomendation = "Recommendation"
        case recomended = "Recommended"
        case vod = "VOD"
        case hero = "Hero"
        case bingeMobile = "Binge_Mobile"
        case homeMain = "Home-Main"
        case otp = "OTP"
        case password = "PASSWORD"
        case lowQuality = "low"
        case mediumQuality = "medium"
        case highQuality = "high"
        case autoQuality = "auto"
        case onDemand = "ON-DEMAND"
        case offline = "OFFLINE"
        case online = "ONLINE"
        case drama = "DRAMA"
        case recentSearch = "Recent Search"
        case searchPage = "Search Page"
        case resultPage = "Result Page"
        case text = "Text"
        case deny = "Deny"
        case allow = "Allow"
        case voice = "Voice"
        case manual = "Manual"
        case browseByGenre = "Browse-By-Genre"
        case browseByLanguage = "Browse-By-Language"
        case search = "Search"
    }

    enum PropertyKeys: String {
        case name = "NAME"
        case sid = "SID"
        case bingeAccountCount = "BINGE ACCOUNTS COUNT"
        case subscribed = "SUBSCRIBED"
        case dateOfSubscription = "DATE_OF_SUBSCRIPTION"
        case loggendInDeviceCount = "LOGGED_IN_DEVICE_COUNT"
        case premiumUser
        case pvrBox
        case activeProfileType
        case noOfProfile
        case rmn = "RMN"
        case email = "EMAIL"
        case atv = "ATV"
        case fireTv = "FIRE TV"
        case firstTimeLogin = "FIRST TIME LOGIN"
        case firstName = "First Name"
        case lastName = "Last Name"
        
    }

    enum UserAttributes: String {
        case subscriberID = "Subscriber Id"
        case subscriberName = "Subscriber Name"
        case premiumSubcriber = "isPremiumSubscriber"
        case hasPVRBox = "hasPVRBox"
        case accountStatus = "AccountStatus"
        case emailAddress = "EmailAddress"
        case mobileNumber = "MobileNumber"
        case typeOfProfile = "TypeOfProfile"
        case profileName = "PROFILE_NAME"
        case age = "AGE"
        case gender = "GENDER"
    }
}

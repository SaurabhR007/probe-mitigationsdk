//
//  Mixpanel.swift
//  BingeAnywhere
//
//  Created by Shivam on 08/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import Mixpanel

class MixpanelManager {
    
    internal static let shared: MixpanelManager = {
        return MixpanelManager()
    }()
    
    //Initializer access level change now
    private init () {
        print("MixPanel Intialised")
    }
    
    func registerMixPanelInstance(_ token: String) {
        Mixpanel.initialize(token: token)
//        updateUserProperties()
    }
    
    func updateUserProperties() {
//        registerSuperProperties()
        registerPeopleProperties()
    }
    
    func trackEventWith(eventName: String) {
        var updatedEventName = eventName
        updatedEventName = updateAnalyticsEventName(&updatedEventName)
        Mixpanel.mainInstance().track(event: updatedEventName)
        MoengageManager.shared.trackEventWith(updatedEventName)
        print("Mixpanel Event: \(eventName)")
        FacebookAnalytics.tracker.trackEvent(with: updatedEventName)
        FirebaseAnalyticsManager.tracker.trackEvent(with: updatedEventName)
    }
    
    func trackEventWith(eventName: String, properties: [String: Any]) {
        let propertyDict =  properties
        var updatedEventName = eventName
        if BAKeychainManager().customerName == "" {
           let sid = String(BAKeychainManager().acccountDetail?.subscriberId ?? 0)
           if sid == "0" {
               //Mixpanel.mainInstance().registerSuperProperties(["": ""])
           } else {
               Mixpanel.mainInstance().registerSuperProperties(["SID": sid])
           }
        } else {
            let sid = String(BAKeychainManager().acccountDetail?.subscriberId ?? 0)
            if sid == "0" {
                Mixpanel.mainInstance().registerSuperProperties(["SID": ""])
            } else {
                Mixpanel.mainInstance().registerSuperProperties(["SID": sid])
            }
        }
        updatedEventName = updateAnalyticsEventName(&updatedEventName)
        Mixpanel.mainInstance().track(event: updatedEventName, properties: propertyDict as? Properties)
        MoengageManager.shared.trackEventWith(updatedEventName, propertyDict)
        print("Mixpanel Event: \(eventName)\n MixpanelProperties: \(propertyDict)")
        FacebookAnalytics.tracker.trackEvent(with: updatedEventName, parameters: propertyDict)
        FirebaseAnalyticsManager.tracker.trackEvent(with: updatedEventName, parameters: propertyDict)
    }
    
    // To update all event name with same key name for mixpanel and moengage`
    func updateAnalyticsEventName(_ event: inout String) -> String {
        let wordArr = event.split(separator: " ")
        var updatedEvent = ""
        if !wordArr.isEmpty {
            if wordArr.count > 1 {
                for i in 0..<wordArr.count {
                    if i == 0 {
                        updatedEvent = String(wordArr[i])
                    } else {
                        updatedEvent += "-" + String(wordArr[i])
                    }
                    updatedEvent = updatedEvent.uppercased()
                    updatedEvent = updatedEvent.replacingOccurrences(of: "_", with: "-")
                    updatedEvent = updatedEvent.trimmingCharacters(in: .whitespacesAndNewlines)
                    return updatedEvent
                }
            } else {
                updatedEvent = String(wordArr[0])
                updatedEvent = updatedEvent.uppercased()
                updatedEvent = updatedEvent.replacingOccurrences(of: "_", with: "-")
                updatedEvent = updatedEvent.trimmingCharacters(in: .whitespacesAndNewlines)
                return updatedEvent
            }
        }
        return event
    }
    
    func registerSuperProperties() {
        Mixpanel.mainInstance().registerSuperProperties(["SID": String(BAKeychainManager().acccountDetail?.subscriberId ?? 0)])
    }
    
    // To register all the people property when user logged In
    func registerPeopleProperties() {
        //registerSuperProperties()
//        if sid == "0" {
//            Mixpanel.mainInstance().registerSuperProperties(["SID": ""])
//        } else {
       // Mixpanel.mainInstance().registerSuperProperties(["SID": String(BAKeychainManager().acccountDetail?.subscriberId ?? 0)])
        //}
        //Mixpanel.mainInstance().registerSuperProperties(["SID": ""])
        Mixpanel.mainInstance().people.set(properties: SuperPropertiesModel().getMixpanelSuperProperties())
    }
    
    // To register single property if needed from anywhere in code
    func setPeoplePropertForKey(key: String, value: MixpanelType) {
        Mixpanel.mainInstance().people.set(property: key, to: value)
    }
    
    //To be called when user logged In
    func saveMixPanelDistinctID() {
        Mixpanel.mainInstance().distinctId = kDeviceId
        let distinctID = Mixpanel.mainInstance().distinctId
        //Mixpanel.mainInstance().createAlias(SuperPropertiesModel().sID, distinctId: Mixpanel.mainInstance().distinctId)
        //Mixpanel.mainInstance().createAlias(SuperPropertiesModel().sID, distinctId: distinctID)
        Mixpanel.mainInstance().identify(distinctId: SuperPropertiesModel().sID)
       // Mixpanel.mainInstance().identify(distinctId: SuperPropertiesModel().sID)
    }
    
    // MARK: Create Alias For Anonymous Events to SID
    func createAliasDistinctID() {
        Mixpanel.mainInstance().createAlias(SuperPropertiesModel().sID, distinctId: Mixpanel.mainInstance().distinctId )
    }
    
    // To update login time people property
    func setPeopleProperty() {
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.firstName.rawValue, to: SuperPropertiesModel().firstName)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.lastName.rawValue, to: SuperPropertiesModel().lastName)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.sid.rawValue, to: SuperPropertiesModel().sID)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.firstTimeLogin.rawValue, to: SuperPropertiesModel().firstTimeLogin)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.rmn.rawValue, to: SuperPropertiesModel().rmn)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.email.rawValue, to: SuperPropertiesModel().email)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.fireTv.rawValue, to: SuperPropertiesModel().isFireTV)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.atv.rawValue, to: SuperPropertiesModel().isATVUser)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.bingeAccountCount.rawValue, to: SuperPropertiesModel().bingeAccountCount)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.subscribed.rawValue, to: SuperPropertiesModel().subscriber)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.loggendInDeviceCount.rawValue, to: SuperPropertiesModel().loggedInCount)
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.dateOfSubscription.rawValue, to: SuperPropertiesModel().dateOfSubscription)
    }
    
    func updateEmail(_ email: MixpanelType) {
        Mixpanel.mainInstance().people.set(property: MixpanelConstants.PropertyKeys.email.rawValue, to: email)
    }
    
    func updatePackChanges(){
        Mixpanel.mainInstance().people.set(properties: SuperPropertiesModel().getMixpanelSuperProperties())
    }
    
    func logOutUser() {
        Mixpanel.mainInstance().reset()
        Mixpanel.mainInstance().flush()
    }
}

//
//  Moengage.swift
//  BingeAnywhere
//
//  Created by Shivam on 08/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import MoEngage


import Foundation
import MoEngage

protocol MoengageProtocol {
    func getUnreadMessageCount() -> Int
}

extension MoengageProtocol {
    func getUnreadMessageCount() -> Int {
        return MOInbox.getUnreadNotifictionCount()
    }
}

class MoengageManager {
    internal static let shared: MoengageManager = {
        return MoengageManager()
    }()
    
    //Initializer access level change now
    private init () {
        print("MoEngage Intialised")
    }
    
    //    func registerMoengageInstance(_ token: String, _ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
    //        MoEngage.setAppGroupID("group.com.tatasky.binge.moengage")
    //        MoEngage.sharedInstance().initializeProd(withAppID: token, withLaunchOptions: launchOptions)
    //        // Uncomment this if something is required regarding user attributes
    //        // Commented this line to remove issue of not getting Mo-Message
    //    }
    
    func trackEventWith(_ eventName: String) {
        MoEngage.sharedInstance().trackEvent(eventName, with: nil)
        print("Moengage Event: \(eventName)")
        MoEngage.sharedInstance().syncNow()
    }
    
    func trackEventWith(_ eventName: String, _ properties: [String: Any]) {
        let payloadProperty = NSMutableDictionary(dictionary: properties)
        let moengageDic = MOProperties.init(attributes: payloadProperty)
        MoEngage.sharedInstance().trackEvent(eventName, with: moengageDic)
        print("Moengage Event: \(eventName)\n MixpanelProperties: \(moengageDic)")
        MoEngage.sharedInstance().syncNow()
    }
    
    func setUserPersonalAttributes() {
        MoEngage.sharedInstance().setUserName(SuperPropertiesModel().profileName)
        MoEngage.sharedInstance().setUserEmailID(SuperPropertiesModel().email)
        MoEngage.sharedInstance().setUserMobileNo(SuperPropertiesModel().rmn)
        MoEngage.sharedInstance().syncNow()
    }
    
    func userUniqueId() {
        MoEngage.sharedInstance().setUserUniqueID(SuperPropertiesModel().sID)
    }
    
    func updateUnqiueId() {
        MoEngage.sharedInstance().setAlias(SuperPropertiesModel().sID)
    }
    
    func setMoengageNotificationReceiverFlag(_ isPushOptedOut: Bool) {
        MoEngage.sharedInstance().optOut(ofMoEngagePushNotification: isPushOptedOut)
    }
    
    func setUserAttributes() {
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().profileName, forKey: "NAME")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().sID, forKey: "SID")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().rmn, forKey: "RMN")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().email, forKey: "EMAIL")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().isFireTV, forKey: "FIRE TV")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().isATVUser, forKey: "ATV")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().bingeAccountCount, forKey: "BINGE ACCOUNTS COUNT")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().subscriber, forKey: "SUBSCRIBED")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().loggedInCount, forKey: "LOGGED_IN_DEVICE_COUNT")
        MoEngage.sharedInstance().setUserAttribute(SuperPropertiesModel().dateOfSubscription, forKey: "DATE_OF_SUBSCRIPTION")
        setUserPersonalAttributes()
    }
    
    func logOutUser() {
        MOInbox.removeMessages()
        MoEngage.sharedInstance().resetUser()
    }
    
    func setUniqueUser() {
        if let deviceID = KeychainWrapper.standard.string(forKey: UserDefault.UserInfo.vendorIdentifier.rawValue) {
            MoEngage.sharedInstance().setUserUniqueID(deviceID)
        } else {
            if let deviceID = UIDevice.current.identifierForVendor?.uuidString {
                MoEngage.sharedInstance().setUserUniqueID(deviceID)
                KeychainWrapper.standard.set(deviceID, forKey: UserDefault.UserInfo.vendorIdentifier.rawValue)
            }
        }
        print("UDID \(String(describing: KeychainWrapper.standard.string(forKey: UserDefault.UserInfo.vendorIdentifier.rawValue)))")
    }
}

//
//  NSDate+Extensions.swift
//  TataSky-Universal
//
//  Created by Ajay Sharma on 23/12/16.
//  Copyright © 2016 TTN. All rights reserved.
//

import Foundation

extension NSDate {
    convenience init?(jsonDate: String) {
        let prefix = "/Date("
        let suffix = ")/"
        let scanner = Scanner(string: jsonDate)

        // Check prefix:
        if scanner.scanString(prefix, into: nil) {

            // Read milliseconds part:
            var milliseconds: Int64 = 0
            if scanner.scanInt64(&milliseconds) {
                // Milliseconds to seconds:
                var timeStamp = TimeInterval(milliseconds)/1000.0

                // Read optional timezone part:
                var timeZoneOffset: Int = 0
                if scanner.scanInt(&timeZoneOffset) {
                    let hours = timeZoneOffset / 100
                    let minutes = timeZoneOffset % 100
                    // Adjust timestamp according to timezone:
                    timeStamp += TimeInterval(3600 * hours + 60 * minutes)
                }

                // Check suffix:
                if scanner.scanString(suffix, into: nil) {
                    // Success! Create NSDate and return.
                    self.init(timeIntervalSince1970: timeStamp)
                    return
                }
            }
        }

        // Wrong format, return nil. (The compiler requires us to
        // do an initialization first.)
        self.init(timeIntervalSince1970: 0)
        return nil
    }

//    if let theDate = NSDate(jsonDate: "/Date(1427909016000-0800)/")
//    {
//        println(theDate)
//    }
//    else
//    {
//    println("wrong format")
//    }

}

extension Date {
    func asString(dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
}

extension String {
    func asDate(dateFormat: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: self)!
    }

    func convertDateFormater(dateFormatterString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        guard let date = dateFormatter.date(from: self) else {return ""}
        dateFormatter.dateFormat = dateFormatterString
        return  dateFormatter.string(from: date)
    }
}

extension Date {
    
    func daysBetween(date: Date) -> Int {
        return Date.daysBetween(start: self, end: date)
    }
    
    static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        return a.value(for: .day)!
    }
    
    static func daysDiff(startTimeStamp: Double, endTimeStamp: Double) -> String {
        
        let startDate = Date(timeIntervalSince1970: startTimeStamp/1000)
        let endDate = Date(timeIntervalSince1970: endTimeStamp/1000)
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day]//[.day, .hour, .minute, .second]
        let difference = Calendar.current.dateComponents(dayHourMinuteSecond, from: startDate as Date, to: endDate as Date)
        var seconds = "\(difference.second ?? 0)s"
        var minutes = "\(difference.minute ?? 0)m" + " " + seconds
        var hours = "\(difference.hour ?? 0)h" + " " + minutes
        var days = "\(difference.day ?? 0)d" + " " + hours
        if let day = difference.day {
            days = String(day + 1)
            if day == 0 {
                days += " day"
            } else if day >= 1 {
                days += " days"
            }
        }
        
        //        days = "\(difference.day ?? 0) days"
        hours = "\(difference.hour ?? 0)h"
        minutes = "\(difference.minute ?? 0)m"
        seconds = "\(difference.second ?? 0)s"
        
        if let day = difference.day, day          != 0 { return days }
        if let hour = difference.hour, hour       != 0 { return hours }
        if let minute = difference.minute, minute != 0 { return minutes }
        if let second = difference.second, second != 0 { return seconds }
        return days
    }
    
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

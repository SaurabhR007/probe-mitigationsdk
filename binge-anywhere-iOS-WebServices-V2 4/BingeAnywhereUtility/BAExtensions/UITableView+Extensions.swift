//
//  UITableView+Extensions.swift
//  TataSky-Universal
//
//  Created by Shivam Srivastava on 07/01/19.
//  Copyright © 2019 TTN. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UITableView relaod data extension
extension UITableView {
    func reloadData(completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations: { self.reloadData() }) { _ in completion() }
        }
    }

    func isCellVisible(section: Int, row: Int) -> Bool {
        guard let indexes = self.indexPathsForVisibleRows else {
            return false
        }
        return indexes.contains {$0.section == section && $0.row == row }
    }

    func addWatermark(with text: String) {
        let label = UILabel(frame: CGRect(x: 5.0, y: 0.0, width: self.bounds.size.width - 5.0, height: self.bounds.size.height))
        label.text = text
        label.font = UIFont(name: "Helvetica Neue Bold", size: 20.0)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        self.backgroundView = label
    }

    func stopFloatingSectionHeader() {
        let dummyViewHeight: CGFloat = 40
        let dummyView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.bounds.size.width), height: dummyViewHeight))
        self.tableHeaderView = dummyView
        contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
    }

    func hasRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
    }

    func scrollToTop(animated: Bool) {
        let indexPath = IndexPath(row: 0, section: 0)
        if self.hasRowAtIndexPath(indexPath: indexPath) {
            self.scrollToRow(at: indexPath, at: .top, animated: animated)
        }
    }
}

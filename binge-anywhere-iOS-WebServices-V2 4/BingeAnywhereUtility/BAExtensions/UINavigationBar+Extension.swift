//
//  UINavigationBar+Extension.swift
//  TataSky-Universal
//
//  Created by Neha Jain on 06/07/17.
//  Copyright © 2017 TTN. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {

    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: isPhone ? 46 : 44)
    }

    func addShadowEffect() {
        //Add shadow effect
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowRadius = 4.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
    }

    func setImageAndTintColor() {
        tintColor = UIColor.white
        titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let backgroundImage = UIImage(named: "NavBarBg")?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), resizingMode: UIImage.ResizingMode.stretch)
        setBackgroundImage(backgroundImage, for: .default)
    }

    func removeImageAndTintColor() {
        tintColor = UIColor.black
        titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        setBackgroundImage(nil, for: .default)
    }

    func removeShadowEffect() {
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowRadius = 0.0
        layer.shadowOpacity = 0.0
        layer.masksToBounds = false
    }
}

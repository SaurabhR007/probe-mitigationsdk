//
//  ViewHelperExtensions.swift
//  TataSky-Universal
//
//  Created by Shivam Srivastava on 11/10/18.
//  Copyright © 2018 TTN. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func addBlackGradientLayer(frame: CGRect, colors: [UIColor]) {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors.map {$0.cgColor}
        self.layer.addSublayer(gradient)
    }
}

//extension UIView {
//    class func fromNib<T: UIView>() -> T {
//        return Bundle.main.loadNibNamed(String(describing: T.self), owner: T.self, options: nil)![0] as! T
//    }
//}

extension UIView {
    func layerGradient(topColor: CGColor, bottomColor: CGColor) {
        let layer: CAGradientLayer = CAGradientLayer()
        layer.frame = self.bounds
        let colorTop =  topColor
        let colorBottom = bottomColor
        layer.locations = [0, 0.9]
        layer.startPoint = CGPoint(x: 0.0, y: 0.0)
        layer.endPoint = CGPoint(x: 1.0, y: 0.0)
        layer.colors = [colorTop, colorBottom]
        self.layer.insertSublayer(layer, at: 0)
    }

    func layerVerticleGradient(/*topColor : CGColor , bottomColor : CGColor, */colors: [UIColor]) {
        let layer: CAGradientLayer = CAGradientLayer()
        layer.frame = self.bounds
//        let colorTop =  topColor
//        let colorBottom = bottomColor
        layer.locations = [0, 0.9]
        layer.startPoint = CGPoint(x: 0.0, y: 0.0)
        layer.endPoint = CGPoint(x: 0.0, y: 1.0)
//        layer.colors = [colorTop, colorBottom]
        layer.colors = colors.map {$0.cgColor}
        self.layer.insertSublayer(layer, at: 0)

    }

    func makeRoundedCorner(radius: Double) {
        self.layer.cornerRadius = CGFloat(radius)
        self.clipsToBounds = true
    }

    class func loadFromNibNamed(nibNamed: String, bundle: Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
        ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")

        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards

        self.layer.add(animation, forKey: nil)
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            self.clipsToBounds = true
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
            }
    }
}

//
//  String+JSONDictionary.swift
//  TataSky-Universal
//
//  Created by Ajay Sharma on 14/11/16.
//  Copyright © 2016 TTN. All rights reserved.
//

import Foundation

//enum ValidateFriendlyNameError: String, Error {
//    case needsMinOneCharacter
//    case allowedSpecialCharacters
//    case needsMaxTenCharacters
//}

extension String {

    // Explictily removing "\n" with "" since we cannot pass it in header
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString)
    }

    // Check - If String is Empty OR Not
    func isStringEmpty(stringValue: String) -> Bool {
        var stringValue = stringValue
        var returnValue = false

        if stringValue.isEmpty  == true {
            returnValue = true
            return returnValue
        }

        // Make sure user did not submit number of empty spaces
        stringValue = stringValue.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if stringValue.isEmpty {
            returnValue = true
            return returnValue
        }

        return returnValue
    }

    func isValidFriendlyName() -> Bool {
        let regX = "[a-zA-Z0-9_ .]{2,10}\\z"
        let validate = NSPredicate(format: "SELF MATCHES %@", regX)
        return validate.evaluate(with: self)
    }

    func isValidSubsriberID() -> Bool {
        let regX = "[0-9]{10,10}\\z"
        let validate = NSPredicate(format: "SELF MATCHES %@", regX)
        return validate.evaluate(with: self)
    }

    func isValidRMN() -> Bool {
        let regX = "[0-9]{10,10}\\z"
        let validate = NSPredicate(format: "SELF MATCHES %@", regX)
        return validate.evaluate(with: self)
    }

    func isValidPassword() -> Bool {
        if self.count > 7 && self.count < 33 {
            var satisfiedCondition = 0
            let firstCharIndex = self.index(self.startIndex, offsetBy: 1)
            let firstChar = String(self[..<firstCharIndex])
            //            let firstChar = self.substring(to: firstCharIndex)
            let requiredConditions = firstChar.isSpecialCharacter() ? 3 : 2

            //Check for special character
            if self.isSpecialCharacter() {
                satisfiedCondition += 1
            }

            //Check for characters
            let alpha = ".*[a-zA-Z]+.*"
            let validateAlpha = NSPredicate(format: "SELF MATCHES %@", alpha)
            if validateAlpha.evaluate(with: self) {
                satisfiedCondition += 1
            }

            //Check for numbers
            let numbers = ".*[0-9]+.*"
            let validateNumbers = NSPredicate(format: "SELF MATCHES %@", numbers)
            if validateNumbers.evaluate(with: self) {
                satisfiedCondition += 1
            }

            return satisfiedCondition >= requiredConditions
        }
        return false
    }

    func isSpecialCharacter() -> Bool {
        let charset = CharacterSet(charactersIn: "^[~!@#$%^&*\\(\\)+`={}\\[\\]:;\"'\\|<>?,.\\/-]")
        if self.rangeOfCharacter(from: charset) != nil {
            print("yes")
            return true
        }
        return false
    }

    func isValidOTP() -> Bool {
        let regX = "[0-9]{6,6}\\z"
        let validate = NSPredicate(format: "SELF MATCHES %@", regX)
        return validate.evaluate(with: self)
    }

    func isValidProfileName() -> Bool {
        let regX = "[a-zA-Z0-9_@\\- ]{1,20}\\z"
        let validate = NSPredicate(format: "SELF MATCHES %@", regX)
        return validate.evaluate(with: self)
    }

    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    func sliceByCharacter(from: Character, to: Character) -> String? {
        let fromIndex = self.index(self.index(of: from)!, offsetBy: 1)
        let toIndex = self.index(self.index(of: to)!, offsetBy: -1)
        return String(self[fromIndex...toIndex])
    }

    func sliceByString(from: String, to: String) -> String? {

        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }

}

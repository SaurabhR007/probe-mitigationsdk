//
//  BAReachAbility.swift
//  BingeAnywhere
//
//  Created by Shivam on 07/01/20.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import SystemConfiguration

class BAReachAbility: NSObject {

    static let reachability = Reachability(hostname: "https://www.google.com")!
    static var isNetworkRechable: Bool = false

    class func checkNetworkRechability() {
        self.isNetworkRechable = reachability.isReachable
        reachability.whenReachable = { reachability in
            self.isNetworkRechable = true
            if reachability.isReachableViaWiFi {
                self.isNetworkRechable = true
                print("Reachable via WiFi")
            } else {
                self.isNetworkRechable = true
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { reachability in
            print("Not reachable")
            self.isNetworkRechable = false
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
    }

    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         return isReachable && !needsConnection
         */
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        BAReachAbility.isNetworkRechable = (isReachable && !needsConnection)
        return BAReachAbility.isNetworkRechable

    }
}

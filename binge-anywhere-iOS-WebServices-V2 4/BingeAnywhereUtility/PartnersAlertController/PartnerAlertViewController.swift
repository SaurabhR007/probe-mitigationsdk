//
//  PartnerAlertViewController.swift
//  BingeAnywhere
//
//  Created by Shivam on 08/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit

class PartnerAlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.layer.opacity = 0.9
        self.addShadow()
        // Do any additional setup after loading the view.
    }
    

    func addShadow() {
        alertView.layer.cornerRadius = 4
        alertView.layer.shadowColor = UIColor.black.cgColor
        alertView.layer.shadowOpacity = 0.3
        alertView.layer.shadowOffset = CGSize.init(width: 0, height: 2)
        alertView.layer.shadowRadius = 20
    }
    
    @IBAction func proceedButtonTapped(_ sender: Any) {
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

//
//  CustomLoader.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 10/10/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class CustomLoader: UIViewController {
    
    /// Creates a single instance for using CustomLoader
    /// - returns: CustomLoader
    static let shared = CustomLoader()
    
    /// Bool: To check if loader is already loading
    public var isLoading: Bool {
        get {
            return !arcsNotAdded
        }
    }
    
    private var arcsNotAdded: Bool = true {
        didSet {
            if arcsNotAdded {
                [imageView1, imageView2, imageView3, imageView4].forEach({
                    $0.layer.removeAllAnimations()
//                    $0.layer.removeFromSuperlayer()
                    
                })
            }
        }
    }
    
    private var shouldResumeAnimation: Bool = false
    private let outerCircle: CAShapeLayer = CAShapeLayer()
    private let middleCircle: CAShapeLayer = CAShapeLayer()
    private let innerCircle: CAShapeLayer = CAShapeLayer()
    
    private let imageView1: UIImageView = UIImageView(image: UIImage(named: "loaderMiddleImage"))
    private let imageView2: UIImageView = UIImageView(image: UIImage(named: "loaderMiddleImage"))
    private let imageView3: UIImageView = UIImageView(image: UIImage(named: "loaderMiddleImage"))
    private let imageView4: UIImageView = UIImageView(image: UIImage(named: "loaderMiddleImage"))
    private let parentView = UIView()
        
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        addImages()
        addNotifications()
    }
    
    private func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func applicationWillResignActive() {
        [imageView1, imageView2, imageView3, imageView4].forEach ({
            $0.layer.removeAllAnimations()
        })
    }
    
    @objc func applicationDidBecomeActive() {
        if shouldResumeAnimation {
            animateFirstView()
        }
    }
    
    deinit {
        removeNotificaitons()
    }
    
    private func removeNotificaitons() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    // MARK: - Public Methods
    /// Show loader over current window
    /// - parameter interactionEnabled: is used to enable userInteraction, if nothing is passed interaction will be enabled
    func showLoader(_ interactionEnabled: Bool = true) {
        if let appDelegate = UIApplication.shared.delegate, let window = appDelegate.window, let rootViewController = window?.rootViewController {
            var topViewController = rootViewController
            let topVCArray = topViewController.children
            for obj in topVCArray where obj is CustomLoader { return }
            while topViewController.presentedViewController != nil {
                topViewController = topViewController.presentedViewController!
            }
            if topVCArray.count == 1 && topVCArray.first is InitialLandingController { //HOTFix for initial landing controller
                topViewController = topVCArray.first!
            }
            //topViewController.addChild(self) // Crash here iphone 6 plus
            topViewController.view.addSubview(view)
            didMove(toParent: topViewController)
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.frame = CGRect(topViewController.view.center.x, topViewController.view.center.y, 85, 60)
            view.center = topViewController.view.center
            kAppDelegate.window?.isUserInteractionEnabled = interactionEnabled
            shouldResumeAnimation = true
            arcsNotAdded ? self.animateFirstView() : debugPrint("Images are already added")
        }
    }
    
    private func addImages() {
        parentView.backgroundColor = .clear
        parentView.frame = CGRect(x: 0, y: 0, width: 85, height: 60)
        
        imageView1.frame = CGRect(x: 10, y: 15, width: 15, height: 23)
        imageView1.alpha = 0.0
        imageView2.frame = CGRect(x: 10, y: 13, width: 20, height: 28)
        imageView2.alpha = 0.0
        imageView3.frame = CGRect(x: 30, y: 10, width: 30, height: 33)
        imageView4.frame = CGRect(x: 60, y: 13, width: 20, height: 28)
        imageView4.alpha = 0.0
        
        self.view.addSubview(parentView)
        self.parentView.addSubview(imageView1)
        self.parentView.addSubview(imageView2)
        self.parentView.addSubview(imageView3)
        self.parentView.addSubview(imageView4)
    }
        
    private func animateFirstView() {
//        arcsNotAdded = false
        UIView.animate(withDuration: 0.35, delay: 0, options: [.curveEaseOut], animations: {
            self.imageView1.alpha = 0.5
            self.imageView1.frame = CGRect(x: 10, y: 13, width: 20, height: 28)
        }, completion: { (finished) in
            if finished {
                self.imageView2.alpha = 0.5
                self.imageView1.alpha = 0.0
                self.imageView1.frame = CGRect(x: 10, y: 15, width: 15, height: 23)
                self.animateSecondAndThirdView()
            }
        })
//        UIView.animate(withDuration: 0.35, delay: 0, options: [.curveEaseOut]) {
//            self.imageView1.alpha = 0.5
//            self.imageView1.frame = CGRect(x: 10, y: 13, width: 20, height: 28)
//        } completion: { (finished) in
//            if finished {
//                self.imageView2.alpha = 0.5
//                self.imageView1.alpha = 0.0
//                self.imageView1.frame = CGRect(x: 10, y: 15, width: 15, height: 23)
//                self.animateSecondAndThirdView()
//            }
//        }
    }
    
    private func animateSecondAndThirdView() {
        UIView.animate(withDuration: 0.35, delay: 0, options: [.curveEaseInOut], animations: {
            self.imageView2.alpha = 1.0
            self.imageView2.frame = CGRect(x: 30, y: 10, width: 30, height: 33)
            self.imageView3.frame = CGRect(x: 60, y: 13, width: 20, height: 28)
            self.imageView3.alpha = 0.5
            self.animateFourthView()
        }, completion: { (finished) in
            if finished {
                self.imageView4.alpha = 0.5
                self.imageView2.alpha = 0.0
                self.imageView2.frame = CGRect(x: 10, y: 13, width: 20, height: 28)
                self.imageView3.frame = CGRect(x: 30, y: 10, width: 30, height: 33)
                self.imageView3.alpha = 1.0
            }
        })
    }
    
    private func animateFourthView() {
        UIView.animate(withDuration: 0.35, delay: 0, options: [.curveEaseIn], animations: {
            self.imageView4.frame = CGRect(x: 70, y: 15, width: 15, height: 23)
            self.imageView4.alpha = 0.0
            self.animateFirstView()
        }, completion: { (finished) in
            if finished {
                self.imageView4.alpha = 0.0
                self.imageView4.frame = CGRect(x: 60, y: 13, width: 20, height: 28)
            }
        })

    }
    
    func showLoader(on view: UIView) {
        view.addSubview(self.view)
        view.bringSubviewToFront(self.view)
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.frame = CGRect(view.center.x, view.center.y, 85, 60)
        self.view.center = view.center
        shouldResumeAnimation = true
        animateFirstView()
    }
    
    func hideLoader(on view: UIView) {
        shouldResumeAnimation = false
        view.willRemoveSubview(self.view)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    // thread alert so moved into the main queue
    /// Show loader view from current window
    func hideLoader() {
        UtilityFunction.shared.performTaskInMainQueue {
            self.shouldResumeAnimation = false
            self.willMove(toParent: nil)
            //guard let subviews = superview?.subviews else {return}
            //for view in sta {
                self.view.removeFromSuperview()
                self.removeFromParent()
            //}
    //        self.view.removeFromSuperview()
    //        self.removeFromParent()
    //        arcsNotAdded = true
            kAppDelegate.window?.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - Private methods
    private func configureArcs() {
        arcsNotAdded = false
        
        let arcStartAngle: CGFloat = -CGFloat.pi / 2
        let arcEndAngle: CGFloat = 0
        let centrePoint: CGPoint = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        
        let circleColorOuter: CGColor = UIColor(red: 255/255, green: 144/255, blue: 0/255, alpha: 1.0).cgColor
        let circleColorMiddle: CGColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor
        let circleColorInner: CGColor = UIColor(red: 10/255, green: 0/255, blue: 178/255, alpha: 1.0).cgColor
                
        let outerPath = UIBezierPath(arcCenter: centrePoint, radius: CGFloat(77/2), startAngle: arcStartAngle, endAngle: arcEndAngle, clockwise: true)
        let midPath = UIBezierPath(arcCenter: centrePoint, radius: CGFloat(65/2), startAngle: arcStartAngle, endAngle: arcEndAngle, clockwise: true)
        let innerPath = UIBezierPath(arcCenter: centrePoint, radius: CGFloat(53/2), startAngle: arcStartAngle, endAngle: arcEndAngle, clockwise: true)

        configureArcLayer(outerCircle, forView: view, withPath: outerPath.cgPath, withBounds: view.bounds, withColor: circleColorOuter)
        configureArcLayer(middleCircle, forView: view, withPath: midPath.cgPath, withBounds: view.bounds, withColor: circleColorMiddle)
        configureArcLayer(innerCircle, forView: view, withPath: innerPath.cgPath, withBounds: view.bounds, withColor: circleColorInner)
        
        animateArcs(outerCircle, middleCircle, innerCircle)
    }
    
    // MARK: Layer setup
    private func configureArcLayer(_ layer: CAShapeLayer, forView view: UIView, withPath path: CGPath, withBounds bounds: CGRect, withColor color: CGColor) {
        layer.path = path
        layer.frame = bounds
        layer.lineWidth = 1.5
        layer.strokeColor = color
        layer.fillColor = UIColor.clear.cgColor
        layer.isOpaque = true
        view.layer.addSublayer(layer)
    }
    
    // MARK: Arc Animation
    private func animateArcs(_ outerCircle: CAShapeLayer, _ middleCircle: CAShapeLayer, _ innerCircle: CAShapeLayer) {
        DispatchQueue.main.async {
            let outerAnimation = CABasicAnimation(keyPath: "transform.rotation")
            outerAnimation.toValue = 2 * CGFloat.pi
            outerAnimation.duration = 1.5
            outerAnimation.repeatCount = Float(UINT64_MAX)
            outerAnimation.isRemovedOnCompletion = false
            outerCircle.add(outerAnimation, forKey: "outerCircleRotation")
            
            if let middleAnimation = outerAnimation.copy() as? CABasicAnimation {
                middleAnimation.duration = 1
                middleCircle.add(middleAnimation, forKey: "middleCircleRotation")
            }
            
            if let innerAnimation = outerAnimation.copy() as? CABasicAnimation {
                innerAnimation.duration = 0.75
                innerCircle.add(innerAnimation, forKey: "middleCircleRotation")
            }
        }
    }
}

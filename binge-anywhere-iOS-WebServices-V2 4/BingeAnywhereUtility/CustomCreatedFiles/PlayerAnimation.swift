//
//  PlayerAnimation.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 03/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

//public protocol Fadeable {
//    var alpha: CGFloat {get set}
//    mutating func fadeAnimation(fadeIn: Bool, duration: TimeInterval, delay: TimeInterval, completion: @escaping (Bool) -> Void)
//}
//
//extension UIView: Fadeable {
//
//    public  func fadeAnimation(fadeIn: Bool, duration: TimeInterval, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
//        UIView.animate(withDuration: duration, delay: delay, options: (fadeIn ? UIView.AnimationOptions.curveEaseIn : UIView.AnimationOptions.curveEaseOut), animations: {
//            self.alpha = (fadeIn ? 1.0 : 0.0)
//        }, completion: completion)
//     }
//
//}

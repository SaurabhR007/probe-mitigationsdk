//
//  CustomtextField.swift
//  BingeAnywhere
//
//  Created by Shivam on 14/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
open class CustomtextField: UITextField {
    
//    let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // setup()
    }
    
//    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
    
    func setup() {
        let border = CALayer()
        let width = CGFloat(2.0)
        //        border.borderColor = UIColor.darkGray.cgColor
        //        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        self.borderStyle = .line
        //        self.returnKeyType = .done
        //.keyboardType = .numberPad
    }
    
    func placeholderColorAndFont(color: UIColor, font: UIFont) {
        if #available(iOS 13, *) {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [.foregroundColor: color, .font: font])
        } else {
            self.setValue(color, forKeyPath: "_placeholderLabel.textColor")
        }
    }
}

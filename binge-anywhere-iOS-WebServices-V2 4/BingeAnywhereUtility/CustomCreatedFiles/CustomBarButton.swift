//
//  CustomBarButton.swift
//  BingeAnywhere
//
//  Created by Rohan Kanoo on 02/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class BarButton: UIButton {
    override var alignmentRectInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)
    }
}

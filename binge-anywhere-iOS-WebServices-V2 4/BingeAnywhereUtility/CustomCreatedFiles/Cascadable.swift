//
//  Cascadable.swift
//  HeroPOC
//
//  Created by Ankit Nigam on 07/07/17.
//  Copyright © 2017 ToTheNew. All rights reserved.
//

import Foundation
import UIKit

/// Existential - Temporary protocol
typealias CascadableScroll = Cascadable & UIScrollViewDelegate

/// Cascadable Protocol for cell cascade animation
protocol Cascadable: class {

    associatedtype GridViewType
    associatedtype ViewCellType

    var cascadeQueue: OperationQueue { get }
    var needAnimationFlag: Bool { get set}
    var stopCollectionViewAnimationOnScroll: Bool {get}

    func cascadeCollectionView(_ collectionView: GridViewType, willDisplay cell: ViewCellType, forItemAt indexPath: IndexPath)
    func cascadeScrollViewWillBeginDragging(_ scrollView: UIScrollView)
}

extension Cascadable where Self: UIViewController, GridViewType == UICollectionView, ViewCellType == UICollectionViewCell, Self: UIScrollViewDelegate {

    func cascadeScrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if stopCollectionViewAnimationOnScroll {
          needAnimationFlag = false

        }
    }

    func cascadeCollectionView(_ collectionView: GridViewType, willDisplay cell: ViewCellType, forItemAt indexPath: IndexPath) {
        //print("\(indexPath.row) Visible cell: \(collectionView.visibleCells.count)")
        if stopCollectionViewAnimationOnScroll {
            if indexPath.row > collectionView.visibleCells.count || !needAnimationFlag {
                needAnimationFlag = false
                cell.isHidden = false
                return
            }
        }

        let op1 = BlockOperation { () -> Void in
            OperationQueue.main.addOperation({ () -> Void in
                let cellContent = cell

                let offsetPositioning = CGPoint(x: 0, y: -20)
                var transform = CATransform3DIdentity
                transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50)

                cellContent.layer.transform = transform
                cellContent.layer.opacity = 0.0
                cell.contentView.alpha = 0.0
                cell.isHidden = false

                if !collectionView.visibleCells.isEmpty {
                    let delay = 0.10 * Double(indexPath.row % collectionView.visibleCells.count)
                    UIView.animate(withDuration: 0.8, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveLinear, animations: { () -> Void in
                        cellContent.layer.transform = CATransform3DIdentity
                        cellContent.layer.opacity = 1
                        cell.contentView.alpha = 1.0
                    }) { (_) -> Void in
                    }
                }
            })
        }
        if indexPath.row > 0 && cascadeQueue.operationCount>0 {
            op1.addDependency(cascadeQueue.operations.last!) // crash here
        }
        op1.name = "operation \(indexPath.row)"
        op1.completionBlock = {

            //print(self.cascadeQueue.operations.count)
            //print("\(String(describing: op1.name)) completed")
        }
        cascadeQueue.maxConcurrentOperationCount = 1
        cascadeQueue.addOperation(op1)
    }
}

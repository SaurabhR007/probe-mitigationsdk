//
//  ScreenRecordingDetector.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit


let kScreenRecordingDetectorTimerInterval: Float = 1.0
var kScreenRecordingDetectorRecordingStatusChangedNotification = "kScreenRecordingDetectorRecordingStatusChangedNotification"

class ScreenRecordingDetector {
    var lastRecordingState = false
    private var timer: Timer?
    
    static let sharedInstance = ScreenRecordingDetector()
    
    init() {
        // do some init stuff here..
        lastRecordingState = false // initially the recording state is 'NO'. This is the default state.
        timer = nil
    }
    
    
    
    func isRecording() -> Bool {
        for screen in UIScreen.screens {
            if #available(iOS 11.0, *) {
                if screen.responds(to: #selector(getter: UIScreen.isCaptured)) {
                    // iOS 11+ has isCaptured method.
                    if screen.perform(#selector(getter: UIScreen.isCaptured)) != nil {
                        return true // screen capture is active
                    } else if screen.mirrored != nil {
                        return true // mirroring is active
                    }
                }
            }else {
                // iOS version below 11.0
                if screen.mirrored != nil {
                    return true
                }
            }
        }
        return false
    }
    
     func triggerDetectorTimer() {
        let detector = ScreenRecordingDetector.sharedInstance
        if detector.timer != nil {
            self.stopTimer()
        }
        detector.timer = Timer.scheduledTimer(
            timeInterval: TimeInterval(kScreenRecordingDetectorTimerInterval),
            target: detector,
            selector: #selector(checkCurrentRecordingStatus(_:)),
            userInfo: nil,
            repeats: true)
    }
    
    @objc func checkCurrentRecordingStatus(_ timer: Timer?) {
        let isRecording = self.isRecording()
        if isRecording != lastRecordingState {
            let center = NotificationCenter.default
            center.post(name: NSNotification.Name(kScreenRecordingDetectorRecordingStatusChangedNotification), object: nil)
        }
        lastRecordingState = isRecording
    }
    
    func stopTimer() {
        let detector = ScreenRecordingDetector.sharedInstance
        if detector.timer != nil {
            detector.timer?.invalidate()
            detector.timer = nil
        }
    }
}

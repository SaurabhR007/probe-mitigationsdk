//
//  Extensions.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import TTTAttributedLabel

extension UIView {
    enum RoundBy {
        case width
        case height
    }
    
    func setViewShadow(opacity: Float, blur: CGFloat, shadowOffsetWidth: CGFloat = 0.0, shadowOffsetHeight: CGFloat = 0.0) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = blur
        self.layer.masksToBounds = false
    }
    
    func removeViewShadow() {
        self.layer.masksToBounds = true
    }
    
    func makeCircular(roundBy: RoundBy) {
        layer.masksToBounds = true
        switch roundBy {
        case .height:
            layer.cornerRadius = frame.size.height/2
        case .width:
            layer.cornerRadius = frame.size.width/2
        }
    }
    
    func isHidden(_ flag: Bool, height with: CGFloat = 0) {
        self.isHidden = flag
        self.heightAnchor.constraint(equalToConstant: flag ? 0 : with).isActive = true
    }
    
    
    func setRoundedCorners(cornerRadius: CGFloat, maskCorners: CACornerMask ) {
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = maskCorners
        }
    }
    
    //	func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
    //		let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    //		let mask = CAShapeLayer()
    //		mask.path = path.cgPath
    //		self.layer.mask = mask
    //	}
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowOffset = .zero
        layer.shadowRadius = 12
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func convertToImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func isHiddenAnimated(value: Bool, duration: Double = 0.33) {
        UIView.animate(withDuration: duration) { [weak self] in
            self?.isHidden = value
            self?.alpha = value ? 0 : 1
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = newValue
            //          layer.shadowRadius = shadowRadius
        }
    }
    
    /* The color of the shadow. Defaults to opaque black. Colors created
     * from patterns are currently NOT supported. Animatable. */
    @IBInspectable var shadowColor: UIColor? {
        set {
            layer.shadowColor = newValue!.cgColor
        }
        get {
            if let color = layer.shadowColor {
                return UIColor.init(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    /* The opacity of the shadow. Defaults to 0. Specifying a value outside the
     * [0,1] range will give undefined results. Animatable. */
    @IBInspectable var shadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
    /* The shadow offset. Defaults to (0, -3). Animatable. */
    @IBInspectable var shadowOffset: CGPoint {
        set {
            layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }
        get {
            return CGPoint(x: layer.shadowOffset.width, y: layer.shadowOffset.height)
        }
    }
    
    /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
    //    @IBInspectable var shadowRadius: CGFloat {
    //        set {
    //            layer.shadowRadius = newValue
    //        }
    //        get {
    //            return layer.shadowRadius
    //        }
    //    }
}
extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let xAxis = (bounds.width - size.width) / 2.0
        let yAxis = (bounds.height - size.height) / 2.0
        
        return CGRect(x: xAxis, y: yAxis, width: size.width, height: size.height)
    }
}

extension UIButton {
    func setContentModeAspectFill() {
        self.contentMode = .scaleAspectFill
        self.contentHorizontalAlignment = .fill
        self.contentVerticalAlignment = .fill
    }
    
    func underline() {
        UIView.performWithoutAnimation {
            guard let text = self.title(for: .normal) else { return }
            let attributedString = NSMutableAttributedString(string: text, attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            self.setAttributedTitle(attributedString, for: .normal)
            self.layoutIfNeeded()
        }
    }
}

extension String {
    static let success = "Success"
    static let failure = "failure"
    static let empty = "empty"
    
    var integer: Int {
        return Int(self) ?? 0
    }
    
    func toBool() -> Bool {
        switch self {
        case "True", "true", "yes", "YES", "1", "TRUE":
            return true
        case "False", "false", "no", "NO", "0", "FALSE":
            return false
        default:
            return false
        }
    }
    func toBoolString() -> String {
        switch self {
        case "True", "true", "yes", "YES", "1", "TRUE", "Yes":
            return "1"
        case "False", "false", "no", "NO", "0", "FALSE", "No":
            return "0"
        default:
            return "0"
        }
    }
    
    func returnBoolString() -> String {
        switch self {
        case "True", "true", "yes", "YES", "1", "TRUE", "Yes":
            return "Yes"
        case "False", "false", "no", "NO", "0", "FALSE", "No":
            return "No"
        default:
            return "0"
        }
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    var length: Int {
        return self.count
    }
    
    static func localizedString(_ string: String) -> String {
        return NSLocalizedString(string, comment: "")
    }
    
    func object() -> [String: AnyObject] {
        let data: Data? = self.data(using: String.Encoding.utf8)
        do {
            let resultJson = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
            return resultJson!
        } catch let error as NSError {
            print(error)
            return [String: AnyObject]()
        }
    }
    
    var lastPathComponent: String {
        return (self as NSString).lastPathComponent
    }
    var pathExtension: String {
        return (self as NSString).pathExtension
    }
    var stringByDeletingLastPathComponent: String {
        return (self as NSString).deletingLastPathComponent
    }
    var stringByDeletingPathExtension: String {
        return (self as NSString).deletingPathExtension
    }
    var pathComponents: [String] {
        return (self as NSString).pathComponents
    }
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        let nsSt = self as NSString
        return nsSt.appendingPathExtension(ext)
    }
    
    func trimString(byString string: String) -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.width
    }
    
    func toJSONObject() -> Any? {
        let data = self.data(using: String.Encoding.utf8)
        return data?.formattedJSON()
    }
    
    func toImage() -> UIImage? {
        let dataDecoded: Data = Data(base64Encoded: self, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage
    }
    
    func hexadecimal() -> Data? {
        var data = Data(capacity: count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(location: 0, length: utf16.count)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            var num = UInt8(byteString, radix: 16)!
            data.append(&num, count: 1)
        }
        
        guard data.isEmpty else { return nil }
        
        return data
    }
    
    private func matches(pattern: String) -> Bool {
        let regex = try! NSRegularExpression(
            pattern: pattern,
            options: [.caseInsensitive])
        return regex.firstMatch(
            in: self,
            options: [],
            range: NSRange(location: 0, length: utf16.count)) != nil
    }
    func isValidURL() -> Bool {
        guard let url = URL(string: self) else { return false }
        if !UIApplication.shared.canOpenURL(url) {
            return false
        }
        
        let urlPattern = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$"
        return self.matches(pattern: urlPattern)
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "^(?=.*[a-zA-Z])(?=.*[0-9]).{6,16}", options: .regularExpression) == nil
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: self)
    }
    
    func isValidPasswordRegex() -> Bool {
        let passWordRegex = "(?=.*?[0-9])(?=.*?[#?!@$%^&*-])(?=.*?[A-Za-z]).{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passWordRegex).evaluate(with: self)
    }
    
    func replaceFrom(startIndex: Int, endIndex: Int, With: String) -> String {
        var strReplaced = self
        let start = self.index(self.startIndex, offsetBy: startIndex)
        let end = self.index(self.startIndex, offsetBy: endIndex)
        strReplaced = self.replacingCharacters(in: start...end, with: With)
        return strReplaced
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
    var isValidContact: Bool {
        let phoneNumberRegex = "^[0-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
    
    var secondFromString : Int {
        let components: Array = self.components(separatedBy: ":")
        if components.count == 3 {
            let hours = components[0].integer
            let minutes = components[1].integer
            let seconds = components[2].integer
            return Int((hours * 60 * 60) + (minutes * 60) + seconds)
        } else {
            return 0
        }
    }

}

extension Bool {
    func boolToInt() -> Int {
        if self {
            return 1
        }
        return 0
    }
    
    func boolToString() -> String {
        if self {
            return "true"
        }
        return "false"
    }
}

extension Array where Element: Equatable {
    mutating func removeObject(object: Element) {
        if let index = self.firstIndex(of: object) {
            self.remove(at: index)
        }
    }
    
    mutating func removeObjectsInArray(array: [Element]) {
        for object in array {
            self.removeObject(object: object)
        }
    }
}

extension Array {
    func toJSONString() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch {
            return nil
        }
    }
}

extension Dictionary {
    func toJSONString() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch {
            return nil
        }
    }
}

extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension Data {
    func formattedJSON() -> Any? {
        do {
            let dict = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.allowFragments)
            return dict
        } catch {
            return nil
        }
    }
    
    func hexadecimal() -> String {
        return map { String(format: "%02x", $0) }
            .joined(separator: "")
    }
}

extension Date {
    func toUTC() -> Date {
        let timeZone = NSTimeZone.default
        let seconds = -timeZone.secondsFromGMT(for: self)
        return Date(timeInterval: TimeInterval(seconds), since: self)
    }
}

extension UITextField {
    func addToolbarWithButtonTitled(title: String, forTarget: UIViewController, selector: Selector) {
        let flexibleSpaceLeft = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let barBtnDone = UIBarButtonItem.init(title: title, style: .done, target: forTarget, action: selector)
        
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: kAppDelegate.window!.frame.size.width, height: 40.0))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        toolBar.setItems([flexibleSpaceLeft, barBtnDone], animated: false)
        self.inputAccessoryView = toolBar
    }
    
    func setHorizontalPadding(left: CGFloat, right: CGFloat, isRightValueInUse: Bool = false) {
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: left, height: self.frame.size.height))
        self.leftView = leftPaddingView
        self.leftViewMode = .always
        
        
        // To solve bugs https://jira.tothenew.com/browse/TBAA-4515
        if isRightValueInUse {
            let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: right, height: self.frame.size.height))
            self.rightView = rightPaddingView
            self.rightViewMode = .always
        }
    }
}

extension UICollectionView {
    func addWatermark(with text: String) {
        let label = UILabel(frame: CGRect(x: 5.0, y: 0.0, width: self.bounds.size.width - 5.0, height: self.bounds.size.height))
        label.text = text
        label.font = UIFont(name: "Helvetica Neue Bold", size: 20.0)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        self.backgroundView = label
    }
    
    func reloadData(completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations: { self.reloadData() }) { _ in completion() }
        }
    }
}

extension Collection {
    subscript(optional i: Index) -> Iterator.Element? {
        return self.indices.contains(i) ? self[i] : nil
    }
}

extension Data {
    var format: String {
        let array = [UInt8](self)
        let ext: String
        switch array[0] {
        case 0xFF:
            ext = "jpg"
        case 0x89:
            ext = "png"
        case 0x47:
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "unknown"
        }
        return ext
    }
}
protocol JSONAble {}
extension JSONAble {
    func toDictionary() -> [String: Any] {
        var dict = [String: Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dict[key] = child.value
            }
        }
        return dict
    }
}

extension UIStoryboard {
    class func loadViewController<T: UIViewController>(storyBoardName: String, identifierVC: String, type: T.Type, function: String = #function) -> T {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        guard let controller = storyboard.instantiateViewController(withIdentifier: identifierVC) as? T else {
            fatalError("ViewController with identifier \(identifierVC), not found in  Storyboard.\nFile : \(storyBoardName) \n\nFunction : \(function)")
        }
        return controller
    }
}

extension UIFont {
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                     attributes: [NSAttributedString.Key.font: self],
                                                     context: nil).size
    }
}

extension UINavigationController {
    
    //	public func pushViewController(viewController: UIViewController, animated: Bool, completion: (() -> Void)?) {
    //		CATransaction.begin()
    //		CATransaction.setCompletionBlock(completion)
    //		pushViewController(viewController, animated: animated)
    //		CATransaction.commit()
    //	}
    
}

extension UILabel {
    func scaleFont() {
        self.font = UIFont(name: (self.font?.fontName)!, size: (self.font!.pointSize) * screenScaleFactorForWidth)
    }
    
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}

extension Array {
    func uniqueArrayElement<S: Sequence, E: Hashable>(source: S) -> [E] where E==S.Iterator.Element {
        var seen: [E: Bool] = [ : ]
        return source.filter({ (val) -> Bool in
            return seen.updateValue(true, forKey: val) == nil
        })
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround(_ tableView: UITableView) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = true
        tableView.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
}

extension DispatchQueue {
    static func background(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}

extension Dictionary where Value: Equatable {
    func containsValue(value: Value) -> Bool {
        return self.contains { $0.1 == value }
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let navigation = self as? UINavigationController {
            if let nav = navigation.visibleViewController {
                return nav.topMostViewController()
            } else {
                //TODO: Hotfix for image picker crash need to update.
                return  kAppDelegate.window?.rootViewController?.presentedViewController ?? navigation.visibleViewController!.topMostViewController()
            }
        }
        if let tab = self as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            if let visibleController = navigation.visibleViewController {
                return visibleController.topMostViewController()
            }
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}

extension UISegmentedControl {
    func removeBorder() {
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.BAwhite.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.BAwhite.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)
        self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 67/255, green: 129/255, blue: 244/255, alpha: 1.0)], for: .selected)
    }
    
    func addUnderlineForSelectedSegment() {
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 67/255, green: 129/255, blue: 244/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }
    
    func changeUnderlinePosition() {
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}

extension UIImage {
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}

extension Int {
    var boolValue: Bool { return self != 0 }
}

// MARK: - Device Types & Size
struct ScreenSize {
    static let screenWidth         = UIScreen.main.bounds.size.width
    static let screenHeight        = UIScreen.main.bounds.size.height
    static let screenMaxLength    = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let screenMinLength    = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

extension UIDevice {
    var hasNotch: Bool {
        var bottom: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        } else {
            // Fallback on earlier versions
        }
        return bottom > 0
    }
}

// MARK: - Done Button On KeyBoard
extension UITextField {
    func addDoneButtonOnKeyBoardWithControl() {
        let kTextColor = UIColor(red: 55.0/255.0, green: 70.0/255.0, blue: 126.0/255.0, alpha: 1.0)
        let dateToolBar = UIToolbar()
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButtonDate = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(UIView.endEditing(_:)))
        dateToolBar.barStyle = .default
        dateToolBar.isTranslucent = true
        dateToolBar.tintColor = kTextColor
        dateToolBar.sizeToFit()
        dateToolBar.setItems([flex, doneButtonDate], animated: false)
        dateToolBar.isUserInteractionEnabled = true
        self.inputAccessoryView = dateToolBar
    }
}

// MARK: Get App Version
extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

// MARK: Read Less/More Label Extension
extension TTTAttributedLabel {
    func showTextOnTTTAttributeLable(str: String, readMoreText: String, readLessText: String, font: UIFont?, charatersBeforeReadMore: Int, activeLinkColor: UIColor, isReadMoreTapped: Bool, isReadLessTapped: Bool) {
        var summaryString = ""
        let text = str + readLessText
        let attributedFullText = NSMutableAttributedString.init(string: text)
        let rangeLess = NSString(string: text).range(of: readLessText, options: String.CompareOptions.caseInsensitive)
        let detailRange = NSString(string: text).range(of: str, options: String.CompareOptions.caseInsensitive)
        attributedFullText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], range: detailRange)
        attributedFullText.addAttributes([NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15)], range: detailRange)
        attributedFullText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor], range: rangeLess)
        attributedFullText.addAttributes([NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15)], range: rangeLess)
        var subStringWithReadMore = ""
        if text.count > charatersBeforeReadMore {
            let start = String.Index(encodedOffset: 0)
            let end = String.Index(encodedOffset: charatersBeforeReadMore)
            summaryString = String(text[start..<end])
            subStringWithReadMore = String(text[start..<end]) + readMoreText
        }
        
        let attributedLessText = NSMutableAttributedString.init(string: subStringWithReadMore)
        let nsRange = NSString(string: subStringWithReadMore).range(of: readMoreText, options: String.CompareOptions.caseInsensitive)
        let summaryRange = NSString(string: text).range(of: summaryString, options: String.CompareOptions.caseInsensitive)
        attributedLessText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], range: summaryRange)
        attributedLessText.addAttributes([NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15)], range: summaryRange)
        attributedLessText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor], range: nsRange)
        attributedLessText.addAttributes([NSAttributedString.Key.font: UIFont.skyTextFont(.regular, size: 15)], range: nsRange)
        self.attributedText = attributedLessText
        self.activeLinkAttributes = [NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor]
        self.linkAttributes = [NSAttributedString.Key.foregroundColor: UIColor.BABlueTextColor]
        self.addLink(toTransitInformation: ["ReadMore": "1"], with: nsRange)
        
        if isReadMoreTapped {
            self.numberOfLines = 0
            self.attributedText = attributedFullText
            self.addLink(toTransitInformation: ["ReadLess": "1"], with: rangeLess)
        }
        if isReadLessTapped {
            self.numberOfLines = 3
            self.attributedText = attributedLessText
        }
    }
}

extension UIButton {
    func selectedButton(title: String, iconName: String) {
        self.backgroundColor = .BABlueColor
        self.setTitle(title, for: .normal)
        self.setTitle(title, for: .highlighted)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitleColor(UIColor.white, for: .highlighted)
        self.setImage(UIImage(named: iconName), for: .normal)
        self.setImage(UIImage(named: iconName), for: .highlighted)
        //   let imageWidth = self.imageView!.frame.width
        //   let textWidth = (title as NSString).size(withAttributes:[NSAttributedString.Key.font:self.titleLabel!.font!]).width
        //   let width = textWidth + imageWidth + 24
        //   //24 - the sum of your insets from left and right
        //   widthConstraints.constant = width
        self.layoutIfNeeded()
    }
}

extension UISearchBar {
    var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            // Fallback on earlier versions
            for view: UIView in (self.subviews[0]).subviews {
                if let textField = view as? UITextField {
                    return textField
                }
            }
        }
        return nil
    }
}

extension CALayer {
    
    enum KeyPathString: String {
        case bounds
        case cornerRadius
        case borderWidth
    }
    
    func keyFrameAnimationWith(keyPath: String, initialValue: Double, finalValue: Double, divisionsForInterpolation: Int, autoReverse: Bool = false) -> CAKeyframeAnimation {
        
        let animation = CAKeyframeAnimation(keyPath: keyPath)
        
        var keyTimes = [NSNumber]()
        var keyValues = [Any]()
        
        keyTimes.append(NSNumber(value: 0.0))
        keyValues.append(NSNumber(value: initialValue))
        
        (1...divisionsForInterpolation).forEach { (value) in
            keyTimes.append(NSNumber(value: (Double(value) * 1.0)/Double(divisionsForInterpolation)))
            let difference = finalValue - initialValue
            let delta = (Double(difference) * 1.0)/Double(divisionsForInterpolation)
            keyValues.append(NSNumber(value: initialValue + (delta * Double(value))))
        }
        animation.keyTimes = keyTimes
        animation.values = keyValues
        animation.autoreverses = autoReverse
        return animation
    }
    
    func animateBoundsWith(keyPath: String, initialRadius: CGFloat, delta: CGFloat) -> CABasicAnimation {
        let diameterTuple = (initialRadius * 2, initialRadius * 2 + delta * 2.0)
        let animation = CABasicAnimation(keyPath: keyPath)
        animation.fromValue = NSValue(cgRect: CGRect(x: 0, y: 0, width: diameterTuple.0, height: diameterTuple.0))
        animation.toValue = NSValue(cgRect: CGRect(x: 0, y: 0, width: diameterTuple.1, height: diameterTuple.1))
        return animation
    }
    
    func pause() {
        let pausedTime: CFTimeInterval = self.convertTime(CACurrentMediaTime(), from: nil)
        self.speed = 0.0
        self.timeOffset = pausedTime
    }
    
    func resume() {
        let pausedTime: CFTimeInterval = self.timeOffset
        self.speed = 1.0
        self.timeOffset = 0.0
        self.beginTime = 0.0
        let timeSincePause: CFTimeInterval = self.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        self.beginTime = timeSincePause
    }
}

extension URLRequest {
    
    public func cURL(pretty: Bool = false) -> String {
        let newLine = pretty ? "\\\n" : ""
        let method = (pretty ? "--request " : "-X ") + "\(self.httpMethod ?? "GET") \(newLine)"
        let url: String = (pretty ? "--url " : "") + "\'\(self.url?.absoluteString ?? "")\' \(newLine)"
        
        var cURL = "curl "
        var header = ""
        var data: String = ""
        
        if let httpHeaders = self.allHTTPHeaderFields, httpHeaders.keys.count > 0 {
            for (key,value) in httpHeaders {
                header += (pretty ? "--header " : "-H ") + "\'\(key): \(value)\' \(newLine)"
            }
        }
        
        if let bodyData = self.httpBody, let bodyString = String(data: bodyData, encoding: .utf8) {
            data = "--data '\(bodyString)'"
        }
        
        cURL += method + url + header + data
        
        return cURL
    }
}

extension Notification.Name {
    static let notificationPackSubscription = Notification.Name("PackSubscriptionNotification")
}

extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
}

extension Float {
    func cleanValue() -> String {
        let intValue = Int(self)
        if self == 0 {return "0"}
        if self / Float (intValue) == 1 { return "\(intValue)" }
        return "\(self)"
    }
}

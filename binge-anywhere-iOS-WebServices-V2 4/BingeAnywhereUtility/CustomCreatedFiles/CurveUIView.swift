//
//  CurveUIView.swift
//  BingeAnywhere
//
//  Created by Shivam on 02/06/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import UIKit

class CurveView: UIView {

    var once = true

    var hasNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else {
            return false
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if once {
            let bb = UIBezierPath()
            bb.move(to: CGPoint(x: 0, y: self.frame.height))
            // the offset here is 40 you can play with it to increase / decrease the curve height (hasNotch ? 180 : 200 )
            bb.addQuadCurve(to: CGPoint(x: self.frame.width, y: self.frame.height), controlPoint: CGPoint(x: self.frame.width / 2 , y: self.frame.height + (hasNotch ? 180 : 200 )))
            bb.close()
            let l = CAShapeLayer()
            l.path = bb.cgPath
            l.fillColor =  self.backgroundColor!.cgColor
            self.layer.insertSublayer(l,at:0)
            once = false
        }

    }
}

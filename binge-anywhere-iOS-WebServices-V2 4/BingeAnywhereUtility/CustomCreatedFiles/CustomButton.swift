//
//  CustomButton.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

enum buttonStyle {
	case primary
	case secondary
	case tertiary
	case other
}

class CustomButton: UIButton {

    var imageName: String = ""

    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    func setup() {
        let screenRect: CGRect           = UIScreen.main.bounds
        let screenWidth: CGFloat         = screenRect.size.width
        let scalefactor: CGFloat         = screenWidth / 375.0
		print(scalefactor)
        self.titleLabel!.font           =  UIFont(name: (self.titleLabel!.font.fontName), size: (self.titleLabel!.font.pointSize))!
        self.isExclusiveTouch           = true
    }

    func text(_ title: String) {
        self.setTitle(title, for: [])
    }
	
	func style(_ type: buttonStyle, isActive: Bool = true) {
		self.makeCircular(roundBy: .height)
		self.isEnabled = isActive
		self.alpha = isActive ? 1 : 1
		switch type {
		case .primary:
			self.backgroundColor = .BABlueColor
			self.tintColor = .BAwhite
		case .secondary:
			self.backgroundColor = .BAwhite
			self.tintColor = .BABlueColor
		case .tertiary:
			self.backgroundColor = .BAmidBlue
			self.tintColor = .BAwhite
		case .other:
			self.backgroundColor = .clear
			self.tintColor = .BABlueColor
			self.underline()
		}
	}

    func fontAndColor(font: UIFont, color: UIColor) {
        self.titleLabel?.font = font
        self.setTitleColor(color, for: [])
    }
}

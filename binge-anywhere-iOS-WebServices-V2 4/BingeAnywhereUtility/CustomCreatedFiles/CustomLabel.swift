//
//  CustomLabel.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    func setup() {
//        let screenRect: CGRect = UIScreen.main.bounds
//        let screenWidth: CGFloat = screenRect.size.width
//        let scalefactor: CGFloat = screenWidth / 375.0
        self.font = UIFont(name: "SkyTextMedium-Regular", size: (self.font.pointSize))
    }

    func setupCustomFontAndColor(font: UIFont, color: UIColor) {
        self.font = font
        self.textColor = color
    }

}

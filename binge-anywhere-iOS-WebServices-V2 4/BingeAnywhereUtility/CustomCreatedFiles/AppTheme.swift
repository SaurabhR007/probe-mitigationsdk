//
//  AppTheme.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var BABlueColor: UIColor {
        return UIColor(red: 80.0 / 255.0, green: 135.0 / 255.0, blue: 199.0 / 255.0, alpha: 1.0)
    }

    static var BApinkColor: UIColor {
        return UIColor(red: 229.0 / 255.0, green: 0.0, blue: 125.0 / 255.0, alpha: 1.0)
    }

    static var BAdarkBlueBackground: UIColor {
        return UIColor(red: 29.0 / 255.0, green: 30.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }

    static var BAdarkBlue2Color: UIColor {
        return UIColor(red: 43.0 / 255.0, green: 45.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
    }

    static var BAdarkBlue1Color: UIColor {
        return UIColor(red: 51.0 / 255.0, green: 53.0 / 255.0, blue: 77.0 / 255.0, alpha: 1.0)
    }
    
    static var BAdarkBlue3Color: UIColor {
        return UIColor(red: 47.0 / 255.0, green: 53.0 / 255.0, blue: 77.0 / 255.0, alpha: 1.0)
    }

    static var BABlueTextColor: UIColor {
           return UIColor(red: 74.0 / 255.0, green: 144.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
       }

    static var BAmidBlue: UIColor {
        return UIColor(red: 82.0 / 255.0, green: 84.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    }

    static var BASelectedBlue: UIColor {
        return UIColor(red: 82.0 / 255.0, green: 85.0 / 255.0, blue: 119.0 / 255.0, alpha: 1.0)
    }

    static var BAlightBlueGrey: UIColor {
        return UIColor(red: 163.0 / 255.0, green: 166.0 / 255.0, blue: 194.0 / 255.0, alpha: 1.0)
    }

    static var BAwhite: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }

    static var BAsuccessGreen: UIColor {
        return UIColor(red: 126.0 / 255.0, green: 211.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
    }

    static var BAerrorRed: UIColor {
        return UIColor(red: 229.0 / 255.0, green: 108.0 / 255.0, blue: 108.0 / 255.0, alpha: 1.0)
    }

    static var BAPlaceholderBlue: UIColor {
        return UIColor(red: 138.0 / 255.0, green: 139.0 / 255.0, blue: 108.0 / 255.0, alpha: 1.0)
    }

    static var BASecondaryGray: UIColor {
        return UIColor(red: 173.0 / 255.0, green: 175.0 / 255.0, blue: 203.0 / 255.0, alpha: 1.0)
    }

    static var BAGreyButtonColor: UIColor {
        return UIColor(red: 68.0 / 255.0, green: 71.0 / 255.0, blue: 100.0 / 255.0, alpha: 1.0)
    }

    static var BARailTitleColor: UIColor {
        return UIColor(red: 238.0 / 255.0, green: 238.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }

    static var BAMorebackgroundColor: UIColor {
        return UIColor(red: 32.0 / 255.0, green: 33.0 / 255.0, blue: 55.0 / 255.0, alpha: 1.0)
    }

    static var BATrendingPurple: UIColor {
        return UIColor(red: 97.0 / 255.0, green: 37.0 / 255.0, blue: 115.0 / 255.0, alpha: 1.0)
    }

    static var BAExperingRed: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 0.0 / 255.0, blue: 15.0 / 255.0, alpha: 1.0)
    }

    static var BAExclusiveOrange: UIColor {
        return UIColor(red: 232.0 / 255.0, green: 76.0 / 255.0, blue: 9.0 / 255.0, alpha: 1.0)
    }

    static var BANewBlue: UIColor {
        return UIColor(red: 29.0 / 255.0, green: 110.0 / 255.0, blue: 203.0 / 255.0, alpha: 1.0)
    }
}

extension UIFont {
    public enum SkyText: String {
        case medium = "Medium-Regular"
        case regular = "-Regular"
        case italic = "-Italic"
        case bold = "-Bold"
    }

    static func skyTextFont(_ type: SkyText, size: CGFloat = UIFont.systemFontSize) -> UIFont {

//        for familyName in UIFont.familyNames {
//            print("\n-- \(familyName) \n")
//            for fontName in UIFont.fontNames(forFamilyName: familyName) {
//                print(fontName)
//            }
//        }
        return UIFont(name: "SkyText\(type.rawValue)", size: size)!
    }

    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }

    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }

}

//
//  UtilityFunction.swift
//  BingeAnywhere
//
//  Created by Shivam on 12/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import SystemConfiguration
import MobileCoreServices
import ErosNow_iOS_SDK
import SonyLIVSDK
import Logix_Player_IOS_SDK
import UIKit
import Alamofire

class UtilityFunction: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    internal static let shared: UtilityFunction = {
        return UtilityFunction()
    }()
    
    //Initializer access level change now
    private override init () {
        
    }
    
    //    var imagePickerHanler: ((_ info: [String: AnyObject]?, _ imagePickerHanler: UIImage?) -> Void)?
    var imagePickerHandler: ((_ imagePickerHandler: UIImage?) -> Void)?
    var currentViewController: UIViewController?
    var sonyLivLoginCount = 0
    
    //    func startLoader(_ text: String) {
    //        DispatchQueue.main.async {
    //            SVProgressHUD.show(withStatus: text)
    //            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
    //        }
    //    }
    
    func performTaskInMainQueue(task:@escaping () -> Void) {
        DispatchQueue.main.async {
            task()
        }
    }
    
    func performTaskAfterDelay(_ timeInteval: TimeInterval, _ task:@escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: (.now() + timeInteval)) {
            task()
        }
    }
    
    func getUniqueId() -> String {
        return UUID().uuidString
    }
    
    func registerCell(_ table: UITableView, _ nibName: String) {
        table.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
    }
    
    func showViewSmoothly(views: [UIView]) {
        for view in views {
            view.isHidden = false
            view.alpha = 0
            UIView.animate(withDuration: 0.6, animations: {
                view.alpha = 1
            })
        }
    }
    
    func hideViewSmoothly(views: [UIView]) {
        for view in views {
            view.alpha = 1
            UIView.animate(withDuration: 0.6, animations: {
                view.alpha = 0
            }, completion: { _ in
                view.isHidden = true
            })
        }
    }
    
    func trimWhiteSpaces(value: String?) -> String {
        let trimmedString = value?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        return trimmedString
    }
    
    // iOS Version Check
    func isIOS10OrHigher() -> Bool {
        let versionNumber = floor(NSFoundationVersionNumber)
        return versionNumber > NSFoundationVersionNumber_iOS_9_4
    }
    
    func showAlertOnWindowACASecurityCheck(message: String) {
        DispatchQueue.main.async( execute: {
            let alertController = UIAlertController(title: AlertConstant.alertTitle.rawValue, message: message, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: AlertConstant.ok.rawValue, style: .default) { (action) in
                exit(0)
            }
            alertController.addAction(defaultAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }
    
    func checkForIphoneX() -> CGFloat {
        var padding: CGFloat = ToastManager.shared.style.verticalPadding
        print(padding)
        let deviceIdiom = (UIDevice().userInterfaceIdiom == .phone)
        if deviceIdiom {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("IPHONE 5,5S,5C,6s,8 and all non notch")
                return padding
            case 1792, 2688, 2436:
                padding += 7.0
                print("IPHONE XS_MAX")
                return padding
            default:
                return padding
            }
        }
        return padding
    }
    
    func addCircularGradient(imageView: UIImageView) {
        UtilityFunction.shared.performTaskInMainQueue {
            let gradient = CAGradientLayer()
            gradient.frame =  CGRect(origin: CGPoint.zero, size: imageView.frame.size)
            gradient.colors = [
                UIColor(red: 240.0/255, green: 131.0/255, blue: 000.0/255, alpha: 1).cgColor,
                UIColor(red: 227.0/255, green: 000.0/255, blue: 015.0/255, alpha: 1).cgColor,
                UIColor(red: 167.0/255, green: 040.0/255, blue: 121.0/255, alpha: 1).cgColor,
                UIColor(red: 006.0/255, green: 068.0/255, blue: 151.0/255, alpha: 1).cgColor
            ]
            gradient.locations = [0.0, 0.25, 0.71, 1.0]
            imageView.makeCircular(roundBy: .height)
            imageView.clipsToBounds = true
            let shape = CAShapeLayer()
            shape.lineWidth = 5
            shape.path = UIBezierPath(arcCenter: CGPoint(x: imageView.frame.size.width/2, y: imageView.frame.size.height/2), radius: imageView.frame.size.width/2, startAngle: CGFloat(Double.pi * 0), endAngle: CGFloat(Double.pi * 2), clockwise: true).cgPath
            shape.strokeColor = UIColor.black.cgColor
            shape.fillColor = UIColor.clear.cgColor
            gradient.mask = shape
            imageView.layer.addSublayer(gradient)
        }
    }
    
    func addCircularGradientWithoutColor(imageView: UIImageView) {
        UtilityFunction.shared.performTaskInMainQueue {
            let gradient = CAGradientLayer()
            gradient.frame =  CGRect(origin: CGPoint.zero, size: imageView.frame.size)
            gradient.colors = [
                UIColor(red: 64.0/255, green: 67.0/255, blue: 84.0/255, alpha: 1).cgColor,
                UIColor(red: 64.0/255, green: 67.0/255, blue: 84.0/255, alpha: 1).cgColor,
                UIColor(red: 64.0/255, green: 67.0/255, blue: 84.0/255, alpha: 1).cgColor,
                UIColor(red: 64.0/255, green: 67.0/255, blue: 84.0/255, alpha: 1).cgColor
            ]
            gradient.locations = [0.0, 0.25, 0.71, 1.0]
            imageView.makeCircular(roundBy: .height)
            imageView.clipsToBounds = true
            let shape = CAShapeLayer()
            shape.lineWidth = 5
            shape.path = UIBezierPath(arcCenter: CGPoint(x: imageView.frame.size.width/2, y: imageView.frame.size.height/2), radius: imageView.frame.size.width/2, startAngle: CGFloat(Double.pi * 0), endAngle: CGFloat(Double.pi * 2), clockwise: true).cgPath
            shape.strokeColor = UIColor.black.cgColor
            shape.fillColor = UIColor.clear.cgColor
            gradient.mask = shape
            imageView.layer.addSublayer(gradient)
        }
    }
    
    func getImageFromGalleryFromViewController(viewController: UIViewController?, callBack:@escaping (_ selectedImage: UIImage?) -> Void) {
        self.currentViewController = viewController
        self.imagePickerHandler = callBack
        let imagePicker: UIImagePickerController = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.mediaTypes = [kUTTypeImage as String]
        
        if viewController != nil {
            self.currentViewController = viewController
            self.currentViewController!.present(imagePicker, animated: true, completion: {
                imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                imagePicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            })
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if (appDelegate.window?.rootViewController) != nil {
                self.currentViewController = appDelegate.window?.rootViewController!
                (self.currentViewController)!.present(imagePicker, animated: true, completion: {
                    imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                    imagePicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                })
            } else {
                self.imagePickerHandler!(nil)
            }
        }
    }
    
    func compressImage(_ image: UIImage) -> Data {
        // Reducing file size to a 10th
        var actualHeight: CGFloat = image.size.height
        var actualWidth: CGFloat = image.size.width
        let maxHeight: CGFloat = 1136.0
        let maxWidth: CGFloat = 640.0
        var imgRatio: CGFloat = actualWidth/actualHeight
        let maxRatio: CGFloat = maxWidth/maxHeight
        var compressionQuality: CGFloat = 0.5
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: compressionQuality)
        UIGraphicsEndImageContext()
        return imageData!
    }
    
    func getImageFromCameraFromViewController(viewController: UIViewController?, isSelfie: Bool, callBack: @escaping (_ selectedImage: UIImage?) -> Void) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.currentViewController = viewController
            self.imagePickerHandler = callBack
            let imagePicker: UIImagePickerController = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            if isSelfie {
                imagePicker.cameraDevice = .front
            }
            if viewController != nil {
                self.currentViewController = viewController
                self.currentViewController!.present(imagePicker, animated: true, completion: nil)
            } else {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if (appDelegate.window?.rootViewController) != nil {
                    self.currentViewController = UIApplication.shared.topMostViewController()
                    (self.currentViewController)!.present(imagePicker, animated: true, completion: nil)
                } else {
                    self.imagePickerHandler!(nil)
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let image = info[.editedImage] as! UIImage
        //        if mediaType == "public.image"
        //        {
        if info[.editedImage] as? UIImage != nil {
            if self.imagePickerHandler != nil {
                self.imagePickerHandler!(image)
                self.imagePickerHandler  = nil
            }
        } else {
            if self.imagePickerHandler != nil {
                self.imagePickerHandler!(nil)
                self.imagePickerHandler = nil
            }
        }
        //        }
        self.currentViewController?.dismiss(animated: true, completion: {
            if !BAReachAbility.isConnectedToNetwork() {
                if !noInternetAppear {
                    noInternetAppear = true
                    DispatchQueue.main.async {
                        kAppDelegate.window?.makeToast("No Internet Connection", duration: 1.0, point: .bottom, title: "", image: UIImage.init(named: "nointernet"), toastYPosition: tabBarHeight) { (boolVal) in
                            print("No Internet Appeared")
                            noInternetAppear = false
                        }
                    }
                }
                //                DispatchQueue.main.async {
                //                    kAppDelegate.window?.makeToast("No Internet Connection", duration: 1.0, point: .bottom, title: "", image: UIImage.init(named: "nointernet"), toastYPosition: tabBarHeight) { (boolVal) in
                //                        print("No Internet Appeared")
                //                        noInternetAppear = false
                //                    }
                ////                    AlertController.initialization().showAlert(.singleButton(.okay, .withTopImage(.noInternet), .bingeAnywhere, .withMessage("Please check your internet connection."), .hidden, .hidden)) { _ in }
                //                }
            }
        })
    }
    
    public func showActionSheetFromViewController(viewController: UIViewController, options: [String], title: String, message: String, onSelect:@escaping (_ selectedIndex: Int, _ selectedTitle: String) -> Void) {
        let optionMenu = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: ""), preferredStyle: .actionSheet)
        for option in options {
            let action = UIAlertAction(title: option, style: .default, handler: {(_: UIAlertAction!) -> Void in
                
                if let index = options.firstIndex(of: option), index > -1, index < options.count {
                    onSelect(index, option)
                } else {
                    onSelect(-1, "")
                }
            })
            
            optionMenu.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cancelAction)
        viewController.present(optionMenu, animated: true, completion: nil)
    }
    
    func logoutUser() {
        MixpanelManager.shared.logOutUser()
        MoengageManager.shared.logOutUser()
        PubNubManager.shared.unsubscribedFromChannel()
        ENSDK.shared.logout()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        //        clearLocalData()
        clearKeychainData()
    }
    
    func clearLocalData() {
        let domain = Bundle.main.bundleIdentifier!
        kUserDefaults.removePersistentDomain(forName: domain)
        print(Array(kUserDefaults.dictionaryRepresentation().keys).count)
        for item in kUserDefaults.dictionaryRepresentation().keys {
            print(" \(item) is deleted from the app userDefault")
        }
        kUserDefaults.synchronize()
    }
    
    func clearKeychainData() {
        let isKeychainRemoved: Bool = kKeychainStandard.removeAllKeys()
        if !isKeychainRemoved {
            clearKeychainData()
        }
        BAKeychainManager().apnsDeviceToken = BAUserDefaultManager().apnsDeviceToken
        kUserDefaults.removeObject(forKey: "contentType")
        kUserDefaults.removeObject(forKey: "contentId")
        // BAKeychainManager().deviceId = BAUserDefaultManager().deviceId
    }
    
    func taRecommendedRailTitle(taShowType: String) -> String {
        guard let param = defractTAShowType(taShowType) else {
            return "Related Rails"
        }
        let contentType = param[1]
        if contentType == ContentType.movies.rawValue {
            return "Related Movies"
        } else if contentType == ContentType.webShorts.rawValue {
            return "Related Shorts"
        } else if contentType == ContentType.tvShows.rawValue {
            return "Related Shows"
        } else if contentType == ContentType.brand.rawValue {
            return "Related Brands"
        } else if contentType == ContentType.series.rawValue {
            return "Related Series"
        }
        return "Related Shows"
    }
    
    func filterTAData(_ contentListModel: [BAContentListModel], _ filterPrimeData: Bool = true) -> [BAContentListModel] {
        var data = contentListModel
        data = contentListModel.filter({ (data) -> Bool in
            if (data.contractName?.uppercased().lowercased() == ProviderType.rental.rawValue.lowercased() && data.provider?.uppercased().lowercased() == ProviderType.tataSky.rawValue.lowercased() ) {
                return true
            }
            if filterPrimeData {
                return !(data.provider?.uppercased().lowercased() == ProviderType.sunnext.rawValue.lowercased() || data.provider?.uppercased().lowercased() == ProviderType.tataSky.rawValue.lowercased() || data.provider?.uppercased().lowercased() == ProviderType.prime.rawValue.lowercased())
            } else {
                return !(data.provider?.uppercased().lowercased() == ProviderType.sunnext.rawValue.lowercased() || data.provider?.uppercased().lowercased() == ProviderType.tataSky.rawValue.lowercased())
            }
            
        })
        return data
    }
    
    func removeRentalData(_ contentListModel: [BAContentListModel], _ filterPrimeData: Bool = true) -> [BAContentListModel] {
        var data = contentListModel
        data = contentListModel.filter({ (data) -> Bool in
            return !((data.contractName?.uppercased().lowercased() == ProviderType.rental.rawValue.lowercased() && data.provider?.uppercased().lowercased() == ProviderType.tataSky.rawValue.lowercased()) || data.provider?.uppercased().lowercased() == ProviderType.sunnext.rawValue.lowercased() || data.provider?.uppercased().lowercased() == ProviderType.tataSky.rawValue.lowercased() || data.provider?.uppercased().lowercased() == ProviderType.prime.rawValue.lowercased() )
            
        })
        return data
    }
    
    
    func removeDuplicatesArrayObject(_ arrObj: inout [BAContentListModel]) -> [BAContentListModel] {
        
        var keyedDict = [String:Bool]()
        var dupesArrObj = [Int]()
        for (index, object) in arrObj.enumerated() {
            let dictKey = String(object.id ?? 0) + (object.contentType ?? "")
            let keyExixt = (keyedDict[dictKey] != nil)
            if keyExixt {
                dupesArrObj.append(index)
            } else {
                keyedDict[dictKey] = true
            }
        }
        
        if !dupesArrObj.isEmpty {
            dupesArrObj.reverse()
        }
        
        if !dupesArrObj.isEmpty {
            for item in dupesArrObj {
                arrObj.remove(at: item)
            }
        }
        return arrObj
    }
    
    
    /**
     * Splits a string at the first occurrence of a delimiter string
     * ## Examples:
     * splitAtFirst(str: "element=value", delimiter: "=") // "element", "value"
     */
    public static func splitAtFirst(str: String, delimiter: String) -> (firstString: String, secondString: String)? {
        guard let upperIndex = (str.range(of: delimiter)?.upperBound), let lowerIndex = (str.range(of: delimiter)?.lowerBound) else { return nil }
        let firstPart: String = .init(str.prefix(upTo: lowerIndex))
        let lastPart: String = .init(str.suffix(from: upperIndex))
        return (firstPart, lastPart)
    }
    
    func defractTAShowType(_ taShowType: String) -> [String]? {
        if taShowType.contains("-") {
            let delimiter = "-"
            let splitArray = taShowType.components(separatedBy: delimiter)
            var param = [String]()
            param.append(splitArray[0])
            param.append(splitArray[1])
            return param
        }
        return nil
    }
    
    func defractErrorCode(_ errorCode: String) -> [String]? {
        if errorCode.contains(":") {
            let delimiter = ":"
            let splitArray = errorCode.components(separatedBy: delimiter)
            var param = [String]()
            param.append(splitArray[0])
            param.append(splitArray[1])
            return param
        }
        return nil
    }
    
    func postNotificationObserver(name: Notification.Name) {
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    
    func checkDateForExpiry(_ expiryDate: String) -> (Bool, String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = (dateFormatter.date(from: expiryDate) ?? Date())
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd/MM/yy"
        let returnDate = dateFormatter2.string(from: date)
        
        if Date() < dateFormatter.date(from: expiryDate) ?? Date() {
            print("Not Yet expiryDate")
            return(false, returnDate )
        } else {
            print("expiryDate has passed")
            return(true, returnDate )
        }
    }
    
    
    func isValidPrimeSubscription(_ primeData: PrimePackDetails) -> Bool {
        return (primeData.bundleState?.lowercased() == PrimeAppConstant.activated.rawValue.lowercased() || primeData.bundleState?.lowercased() == PrimeAppConstant.suspended.rawValue.lowercased())
    }
    
    
    func checkForPrimeSubscription() -> (Bool?) {
        if let subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails {
            if let primeData = subscriptionData.primePackDetails {
                if primeData.bundleState?.lowercased() == PrimeAppConstant.activated.rawValue.lowercased() {
                    return (true)
                } else if primeData.bundleState?.lowercased() == PrimeAppConstant.suspended.rawValue.lowercased() {
                    return (false)
                }
            } else {
                return (nil)
            }
        }
        return (nil)
    }
    
    func checkForPrimeSuspendedState() -> (PrimeAppConstant?) {
        if let subscriptionData = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails {
            if let primeData = subscriptionData.primePackDetails?.primeMessageDetails {
                if primeData.statusType?.lowercased() == PrimeAppConstant.active.rawValue.lowercased() {
                    return .active
                } else if primeData.statusType?.lowercased() == PrimeAppConstant.inActive.rawValue.lowercased() {
                    return .inActive
                } else if primeData.statusType?.lowercased() == PrimeAppConstant.trivial.rawValue.lowercased() {
                    return .trivial
                }
            } else {
                return nil
            }
        }
        return nil
    }
    
    
    func getPrimeMessageStringFromPrimePopUpDataNode(_ messageType: Int) -> String {
        var message = ""
        if let primeDataNode = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.primePackDetails?.primeMessageDetails {
            switch messageType {
            case 0:
                message = primeDataNode.primeTitle1 ?? "Log in to Amazon Prime"
                break
            case 1:
                message = primeDataNode.primeTitle2 ?? "Please install the Prime Video App to play this content."
                break
            case 2:
                message = primeDataNode.primeMessage1 ?? "You are an Amazon Prime member through Tata Sky. Please login to Amazon with valid credentials"
                break
            case 3:
                message = primeDataNode.primeMessage2 ?? "You are an Amazon Prime member through Tata Sky. Please login to Amazon with valid credentials"
                break
            default:
                break
            }
        }
        return message
    }
    
    func checkIsPrimeAppInstalledOrNot() -> Bool {
        if let primeAppLink = URL(string: "aiv://") {
            if UIApplication.shared.canOpenURL(primeAppLink) {
                return true
            }
            return false
        }
        return false
    }
    
    func isHotstarInstalled() -> Bool {
        if let hotstarappLink = URL(string: "hotstar://") {
            if UIApplication.shared.canOpenURL(hotstarappLink) {
                return true
            }
            return false
        }
        return false
    }
    
    func alertPopUpBox(_ subscriptionLogOut: Bool) {
        
        if !subscriptionLogOut {
            self.pubnubEventLogout()
            return
        }
        
        let message = kSubscriptionDropped
        let header = kSubscriptionCancelled
//        if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.binge.rawValue || BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.stick.rawValue {
//            message = kFSPackDroppedMessage
//            header = kFSInactiveHeader
//        } else if BAKeychainManager().acccountDetail?.subscriptionType == BASubscriptionsConstants.atv.rawValue {
//            message = kATVBingeInactive
//            header = kATVInactiveHeader
//        }
        DispatchQueue.main.async {
            BAKeychainManager().deviceLoggedOut = true
            CustomLoader.shared.hideLoader()
            AlertController.initialization().showAlert(.singleButton(.ok, .hidden, .withTitle(header), .withMessage(message), .hidden, .hidden)) { _ in
                self.logoutUserViaApi()
            }
        }
    }
    
    
    func pubnubEventLogout() {
        UtilityFunction.shared.performTaskInMainQueue {
            CustomLoader.shared.hideLoader()
            let controller = UIApplication.shared.topMostViewController()
            if (controller?.isKind(of: BAFullScreenPlayerViewController.self))! {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logoutFromPlayer"), object: nil)
            } else {
                AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.alert), .withTitle(kDeviceRemovedHeader), .withMessage(kSessionExpire), .hidden, .hidden)) { _ in
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                }
            }
        }
    }
    
    func logoutUserViaApi() {
        let vc = MoreVC()
        if let topMostController = UIApplication.shared.topMostViewController() {
            if let topVC = topMostController as? BaseViewController {
                topVC.showActivityIndicator(isUserInteractionEnabled: true)
                vc.signOut()
                return
            } else {
                vc.signOut()
            }
        }
    }
    
    
    // To login Eros Now User
    func checkErosNowPartnerLogin(completion: @escaping (Bool, String) -> Void) {
        self.loginErosNow { (flagValue, message) in
            completion(flagValue, message)
        }
    }
    
    func loginErosNow(completion: @escaping (Bool, String) -> Void) {
        debugPrint(" Eros_Now SSO Login action")
        print("this is partner id \(ErosNowConstants.partnerId)")
        print("this is user token \(ErosNowConstants.userToken)")
        configureErosNowSDK()
        ENSDK.shared.ssoLogin (
            partnerId: ErosNowConstants.partnerId, //"9007961696-VKK0L83493183035", //
            userToken: ErosNowConstants.userToken // "MVyTDN7j6UhPgW2Izoy53NkVJJ7iAv7o" //
        ) {(result: Result<String, ENError>) in
            switch result {
            case .success(let response):
                print(response)
                print("Eros Now Logged In")
                BAKeychainManager().isErosNowLoggedIn = true
                completion(true, "")
            case .failure(let error):
                BAKeychainManager().isErosNowLoggedIn = false
                print(error.description)
                print("Eros Now LogIn Fail")
                completion(false, error.description)
            }
        }
    }
    
    
    private func configureErosNowSDK() {
        let configuration = ENConfiguration(
            environment: ErosNowConstants.environment,
            partnerCode: ErosNowConstants.partnerCode,
            apiClientId: isUatBuild() ? ErosNowConstants.uatApiClientCode : ErosNowConstants.prodApiClientCode,
            country: ErosNowConstants.country,
            deviceId: kDeviceId //"8539b44d253647e6"// kDeviceId // ErosNowConstants.partnerId // This value is updated by Eros Now Team on call
        )
        print("this is device Id \(kDeviceId)")
        print("this is client key Id \(configuration.apiClientId)")
        ENSDK.setup(with: configuration)
        ENSDK.shared.enableLogs = true
    }
    
    
    func getErosNowContentProfile(for contentId: String, completion: @escaping (Bool, ENContentProfile?, String) -> Void) {
        configureErosNowSDK()
        ENSDK.shared.getContentProfile(contentId: contentId) { [weak self] (result: Result<ENContentProfile, ENError>) in
            guard self != nil else {
                completion(false, nil, .empty)
                return
            }
            switch result {
            case .success(let contentProfile):
                print(contentProfile)
                completion(true, contentProfile, .success)
            case .failure(let error):
                print(error.localizedDescription)
                completion(false, nil, error.description)
            }
        }
    }
    
    func isUatBuild() -> Bool {
        #if DEBUG
        return true//false
        #else
        return false
        #endif
    }
    
    
    
    
    func createCloudinaryImageUrlforSameRatio(url: String?, width: Int = 0, height: Int = 0, trim: Bool = false) -> String? {
        if let _url = url, !(url?.isEmpty ?? true) {
            let cloudAccountUrl = BAConfigManager.shared.configModel?.data?.config?.url?.image?.cloudAccountUrl ?? ""
            let imageUrl = cloudAccountUrl + (width != 0 ? "w_\(width)," : "") + (height != 0 ? "h_\(height)," : "") + (trim ? "e_trim," : "") + "f_webp,q_auto:good/" +  _url // if webp iamge failed update f_webp with f_jpg
            return imageUrl
        }
        return nil
    }
    
    // To login Sony Liv User
    func checkForSonyLivPartnerLogin(completion: @escaping (Bool, String) -> Void) {
        if BAKeychainManager().sonyLivShortToken.isEmpty {
            fetchSonyLivShortToken { (flagValue) in
                if flagValue {
                    self.sonyLivSDKInitialisation { (sonyLivLoginStatus, erroMessage) in
                        completion(sonyLivLoginStatus, erroMessage)
                    }
                } else {
                    completion(false, kSomethingWentWrong)
                }
            }
        } else {
            sonyLivSDKInitialisation { (sonyLivLoginStatus, erroMessage) in
                completion(sonyLivLoginStatus, erroMessage)
            }
        }
    }
    
    // Sony liv SDK Initiaizer
    func sonyLivSDKInitialisation(completion: @escaping (Bool, String) -> Void) {
        let sdkInitializer = SdkInitilizerModel.getSonyLivInitializers()
        print("Sony Liv SSO Login Action with Sdk initializer \(sdkInitializer)")
        SonyLIVSDKManager.initSDK(partnerdetail: sdkInitializer) { (status, error) in
            print("this is status \(status) and this is error \(String(describing: error))")
            switch status {
            case .inProgress:
                print("Sony Liv Login is in Progress")
                BAKeychainManager().isSonyLivLoggedIn = false
                break
            case .success:
                print("Sony Liv Logged In Successfully")
                BAKeychainManager().isSonyLivLoggedIn = true
                completion(true, "")
                break
            case .failure:
                print("Sony Liv Login Failed")
                BAKeychainManager().isSonyLivLoggedIn = false
                BAKeychainManager().sonyLivShortToken = ""
                let error = (error?.errorCode ?? "") + ":" + (error?.errorMessage ?? "")
                if self.sonyLivLoginCount == 1 {
                    self.sonyLivLoginCount = 0
                    completion(false, error)
                    return
                }
                self.fetchSonyLivShortToken {(flagValue) in
                    self.sonyLivLoginCount += 1
                    if flagValue {
                        self.checkForSonyLivPartnerLogin { (sonyLivLoginStatus, errorMessage) in
                            completion(sonyLivLoginStatus, errorMessage)
                        }
                    } else {
                        completion(false, kSomethingWentWrong)
                    }
                }
                break
            }
        }
    }

    
    func fetchSonyLivShortToken(completion: @escaping (Bool) -> Void) {
        let kProdApiPath = "https://tatasky-tsmore-kong.videoready.tv/zee5-playback-api/sony/fetch/token"
        let kUatApiPath = "https://tatasky-uat-tsmore-kong.videoready.tv/zee5-playback-api/sony/fetch/token"
        let apiPath = isUatBuild() ? kUatApiPath : kProdApiPath
        let apiHeader = HTTPHeader(name: "Authorization", value: "bearer \(BAKeychainManager().userAccessToken)")
        let apiHeaders = HTTPHeaders(arrayLiteral: apiHeader)
        BAKeychainManager().sonyLivShortToken = ""
        AF.request(apiPath, parameters: nil, headers: apiHeaders).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    completion(false)
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    print("Error: Cannot convert JSON object to Pretty JSON data")
                    completion(false)
                    return
                }
                guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                    print("Error: Could print JSON in String")
                    completion(false)
                    return
                }
                print(prettyPrintedJson)
                let decoder = JSONDecoder()
                if let data = AFdata.data {
                    let responseData = try decoder.decode(shortTokenApiResponse.self, from: data)
                    BAKeychainManager().sonyLivShortToken = responseData.data?.token ?? ""
                    completion(true)
                    return
                }
                completion(false)
            } catch {
                print("Error: Trying to convert JSON data to string")
                completion(false)
                return
            }
        }
    }
    
    // To reset the app badge count
    func resetAppbadgeCount() {
        self.performTaskInMainQueue {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    // To paginate forcefully depends on data availability
    func forcePaginate(_ colletionView: UICollectionView, _ nextPageAvailable: Bool?) -> Bool {
        let colletionHeight  = colletionView.contentSize.height
        if let nextPage = nextPageAvailable {
            if nextPage {
                return (colletionHeight < kDeviceHeight)
            }
        }
        return false
    }
    
    
}

// MARK: - NotificationCenter
// Get Version of app
func getAppVersion() -> String {
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    return version + "." + build
}


struct shortTokenApiResponse: Codable {
    let title : String?
    let code : Int?
    let data : sonyLivToken?
    let message : String?

    enum CodingKeys: String, CodingKey {
        case title = "title"
        case code = "code"
        case data = "data"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(sonyLivToken.self, forKey: .data)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}


struct sonyLivToken : Codable {
    let token : String?
    enum CodingKeys: String, CodingKey {
        case token = "token"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        token = try values.decodeIfPresent(String.self, forKey: .token)
    }
}

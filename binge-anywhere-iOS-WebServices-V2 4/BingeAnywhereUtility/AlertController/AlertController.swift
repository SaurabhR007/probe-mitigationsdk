//
//  AlertController.Swift
//  AlertController
//
//  Created by Shivam on 09/12/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Kingfisher

class AlertController: BaseViewController {
	
    // MARK: - Private Properties	
	private var alertProperties: AlertControllerType = .singleButton(.ok, .hidden, .bingeAnywhere, .hidden, .hidden, .hidden)
    private var showCrossButton: Bool = false
    
    // MARK: - Public Properties
	@IBOutlet weak var topInfoImageView: UIImageView!
	@IBOutlet weak var alertContainerView: UIView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var messageLabel: CustomLabel!
	@IBOutlet weak var errorCodeLabel: CustomLabel!
	@IBOutlet weak var cancelButton: CustomButton!
    @IBOutlet weak var okButton: CustomButton!
	@IBOutlet weak var webImageView: UIImageView!
    @IBOutlet weak var crossbutton: CustomButton!
    @IBOutlet weak var bodyLabel: CustomLabel!
    @IBOutlet weak var alertWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!

    /// AlertController Completion handler
	typealias AlertCompletionBlock = ((Bool) -> Void)?
    private var block: AlertCompletionBlock?
    private var viewTopConstraintValue: CGFloat = 44.0

    // MARK: - AlertController Initialization
	
    /**
     Creates a instance for using AlertController
     - returns: AlertController
     */
    static func initialization() -> AlertController {
        let alertController = AlertController(nibName: "AlertController", bundle: nil)
        return alertController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAlertController()
    }

    // MARK: - AlertController Private Functions
	
    /// Inital View Setup
    private func setupAlertController() {

		let blurEffect = UIBlurEffect(style: .dark)
		let blurVisualEffectView = UIVisualEffectView(effect: blurEffect)
        blurVisualEffectView.frame = self.view.bounds
        blurVisualEffectView.alpha = 0.8
        self.view.backgroundColor = .clear
        blurVisualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		self.view.insertSubview(blurVisualEffectView, at: 0)
        crossbutton.isHidden = !showCrossButton
		for view in self.view.subviews where view != alertContainerView {
			view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.backgroundViewTapped)))
		}
		
		alertWidthConstraint.constant = alertProperties.alertWidth
        stackViewTopConstraint.constant = viewTopConstraintValue*screenScaleFactorForWidth
        alertContainerView.makeRoundedCorner(radius: 4)
		
		titleLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 20), color: .BAwhite)
		messageLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAlightBlueGrey)
        bodyLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16), color: .BAwhite)
		
		switch alertProperties {
		case .noButton(let topImage, let title, let message, let webImage, let error):
			setupAlertData(topImage, title, message, webImage, error)
		case .singleButton(let okTitle, let topImage, let title, let message, let webImage, let error):
			setupAlertData(okTitle: okTitle.title, topImage, title, message, webImage, error)
        case .singleButtonAccount(let okTitle, let topImage, let title, let message, let webImage, let error):
            setupAlertData(okTitle: okTitle.title, topImage, title, message, webImage, error)
		case .doubleButton(let okTitle, let cancelTitle, let topImage, let title, let message, let webImage, let error):
			setupAlertData(okTitle: okTitle.title, cancelTitle: cancelTitle.title, topImage, title, message, webImage, error)
        case .doubleButtonWithBody(let okTitle, let cancelTitle, let topImage, let title, let body, let message, let webImage, let error):
            setupAlertData(okTitle: okTitle.title, cancelTitle: cancelTitle.title, topImage, title, message, webImage, error, body)
        }
		
		okButton.style(.primary)
		okButton.isHidden = alertProperties.okHidden
		cancelButton.isHidden = alertProperties.cancelHidden
		cancelButton.style(.other)
	}
	
    private func setupAlertData(okTitle: String = "Ok", cancelTitle: String = "Cancel",_ topImage: AlertControllerType.topImage, _ title: AlertControllerType.title, _ message: AlertControllerType.message, _ webImage: AlertControllerType.webImage, _ error: AlertControllerType.errorCode, _ body: AlertControllerType.body = .hidden) {
		okButton.text(okTitle)
		cancelButton.text(cancelTitle)
		
		topInfoImageView.isHidden = topImage.topHidden
		switch topImage {
		case .withTopImage(let image): topInfoImageView.image = image.alertImage
		case .hidden: break
		}
		
		titleLabel.isHidden = title.infoLabelHidden
		switch title {
		case .withTitle(let title): titleLabel.text = title
		case .bingeAnywhere: titleLabel.text = "Tata Sky Binge"//"Binge Anywhere"
		case .hidden: titleLabel.text = ""
		}
		
		messageLabel.isHidden = message.infoLabelHidden
		switch message {
		case .withMessage(let message): messageLabel.text = message
		case .hidden: messageLabel.text = ""
		}
        
        bodyLabel.isHidden = body.infoLabelHidden
        switch body {
        case .withBodyMessage(let message): bodyLabel.text = message
        case .hidden: bodyLabel.text = ""
        }

		webImageView.isHidden = webImage.webImageHidden
		switch webImage {
		case .withImage(let urlString):
			let url = URL.init(string: urlString)
			if let _url = url {
				webImageView.alpha = 0
				webImageView.kf.setImage(with: ImageResource(downloadURL: _url), completionHandler: { _ in
					UIView.animate(withDuration: 0.6, animations: {
						self.webImageView.alpha = 1
					})
				})
			} else {
				webImageView.isHidden = true
			}
		case .hidden:
			webImageView.image = nil
            
        case .amazonImage:
            webImageView.image = UIImage(named:"AmazonFireTv") ?? UIImage()
		}
		
		errorCodeLabel.isHidden = error.errorHidden
		switch error {
		case .withError(let code): errorCodeLabel.text = "Error code: " + code
		case .hidden: errorCodeLabel.text = ""
		}
	}
	
    /// Show Alert Controller
    private func show() {
        if let appDelegate = UIApplication.shared.delegate,
           let window = appDelegate.window,
           let rootViewController = window?.rootViewController {
            var topViewController = rootViewController
            let topVCArray = topViewController.children
            for obj in topVCArray {
                for obj2 in obj.children where obj2 is AlertController {
                    return
                }
			}
            while topViewController.presentedViewController != nil {
                topViewController = topViewController.presentedViewController!
            }
			if topVCArray.count == 1 && topVCArray.first is InitialLandingController { //HOTFix for initial landing controller
				topViewController = topVCArray.first!
			}
            CustomLoader.shared.hideLoader()
            topViewController.addChild(self)
            topViewController.view.addSubview(view)
            viewWillAppear(true)
            didMove(toParent: topViewController)
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.frame = topViewController.view.bounds
            alertContainerView.alpha = 0.0
            alertContainerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            alertContainerView.center = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height + alertContainerView.frame.height))
            UIView.animate(withDuration: 0.4, delay: 0.2, options: .curveEaseOut, animations: { () -> Void in
                self.alertContainerView.alpha = 1.0
                kAppDelegate.window?.endEditing(true)
                self.alertContainerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.alertContainerView.center = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0))
            }, completion: nil)
        }
		
		switch alertProperties {
		case .noButton(_, _, _, _, _):
			var seconds = 0
			let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
				seconds += 1
				if seconds >= 2 {
					timer.invalidate()
					self.hide() {
						self.block!!(true)
					}
				}
			}
			RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
		default: break
		}
    }

    /// Hide Alert Controller
	private func hide(completion: (() -> Void)?) {
        self.view.endEditing(true)
        self.alertContainerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.alertContainerView.alpha = 0.0
            self.alertContainerView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.alertContainerView.center = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
        }, completion: nil)

        UIView.animate(withDuration: 0.25, delay: 0.05, options: .curveEaseIn, animations: { () -> Void in
            self.view.alpha = 0.0

        }) { (_) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
			completion?()
        }
    }

    // MARK: - UIButton Clicks
    @IBAction func btnCancelTapped(sender: UIButton) {
		hide() {
			self.block!!(false)
		}
    }

    @IBAction func btnOkTapped(sender: UIButton) {
		hide(){
			self.block!!(true)
		}
    }
    
    @IBAction func crossButtonTapped(sender: UIButton) {
        hide(){
            self.block!!(false)
        }
    }
    
    /// Hide Alert Controller on background tap
    @objc func backgroundViewTapped() {
//		hide(){
//			self.block!!(true)
//		}
	}

    // MARK: - Alert Functions
    /**
     Display an Alert
     - parameter aStrMessage: Message to display in Alert
     - parameter aCancelBtnTitle: Cancel button title
     - parameter aOtherBtnTitle: Other button title
     - parameter otherButtonArr: Array of other button title
     - parameter completion: Completion block. true for Okay Button and false for Cancel Button
     */

    public func showAlert(_ alertData: AlertControllerType = .singleButton(.ok, .hidden, .bingeAnywhere, .withMessage("Error"), .hidden, .hidden), _ showCrossButton: Bool = false, _ topConstraintValue: CGFloat = 44.0, completion: AlertCompletionBlock) {
        kAppDelegate.window?.endEditing(true)
		self.alertProperties = alertData
        self.showCrossButton = showCrossButton
        self.viewTopConstraintValue = topConstraintValue
        show()
        block = completion
    }
}

//
//  AlertControllerData.swift
//  BingeAnywhere
//
//  Created by Abhishek Maurya on 21/07/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

enum AlertControllerType {
	case noButton(topImage, title, message, webImage, errorCode)
	case singleButton(primaryButton, topImage, title, message, webImage, errorCode)
    case singleButtonAccount(primaryButton, topImage, title, message, webImage, errorCode)
	case doubleButton(primaryButton, secondaryButton, topImage, title, message, webImage, errorCode)
    case doubleButtonWithBody(primaryButton, secondaryButton, topImage, title, body, message, webImage, errorCode)
	enum primaryButton {
		case ok
		//case okay
		case yes
        case yesLogout
		case retry
        case skip
        case cancel
        case proceed
        case install
        case backToAccounts
		case with(title: String)
		
		var title: String {
			switch self {
			case .ok: return "Ok"
			//case .okay: return "Okay"
			case .yes: return "Yes"
            case .yesLogout: return "Yes, Log out"
			case .retry: return "Retry"
            case .skip: return "Skip"
            case .proceed: return "Proceed"
            case .cancel: return "Cancel"
            case .backToAccounts: return "Back To Accounts"
            case .install: return "Install"
			case .with(let title): return title
			}
		}
	}
	
	enum secondaryButton {
		case cancel
		case no
        case skip
        case remindeMe
		case with(title: String)
		
		var title: String {
			switch self {
			case .cancel: return "Cancel"
			case .no: return "No"
            case .skip: return "Skip"
            case .remindeMe: return "Reminde me later"
			case .with(let title): return title
			}
		}
	}
	
    enum topImage {
        case withTopImage(topImage)
        case hidden
        
        var topHidden: Bool {
            switch self {
            case .hidden: return true
            default: return false
            }
        }
        
        enum topImage {
            case alert
            case tataLogo
            case downgradeSubscription
            case upgradeSubscription
            case success
            case noInternet
            case hungama
            case prime
            
            var alertImage:UIImage{
                switch self {
				case .alert: return UIImage(named:"inActiveSubscription") ?? UIImage()
                case .tataLogo: return UIImage(named:"NavigationHeaderLogo") ?? UIImage()
                case .downgradeSubscription: return UIImage(named:"removeSubscription") ?? UIImage()
                case .upgradeSubscription: return UIImage(named:"upgradeSubscription") ?? UIImage()
                case .success: return UIImage(named:"success") ?? UIImage()
                case .noInternet: return UIImage(named:"nointernet") ?? UIImage()
                case .hungama: return UIImage(named: "atomPartnerlogoPiPageDisneyHotstarPremium") ?? UIImage()
                case .prime: return UIImage(named: "AmazonPrimeLogo") ?? UIImage()
                }
            }
        }
    }
	
	enum title {
		case withTitle(String)
		case bingeAnywhere
		case hidden
		
		var infoLabelHidden:Bool {
			switch self {
			case .hidden : return true
			default : return false
			}
		}
	}
    
    enum message {
		case withMessage(String)
        case hidden
        
        var infoLabelHidden:Bool {
            switch self {
            case .hidden : return true
            default : return false
            }
        }
    }
    
    enum body {
        case withBodyMessage(String)
        case hidden
       
        var infoLabelHidden:Bool {
            switch self {
            case .hidden : return true
            default : return false
            }
        }
    }
    
    enum webImage {
		case withImage(url: String)
        case hidden
        case amazonImage
        
        var webImageHidden:Bool {
            switch self {
            case .hidden : return true
            default : return false
            }
        }
    }
    
    enum errorCode {
		case withError(code: String)
        case hidden
        
        var errorHidden:Bool {
            switch self {
            case .hidden : return true
            default : return false
            }
        }
    }
    
    var okHidden: Bool {
        switch self {
        case .noButton( _, _, _, _, _): return true
        default: return false
        }
    }
    
    var cancelHidden: Bool {
        switch self {
		case .doubleButton( _, _, _, _, _, _, _): return false
        case .doubleButtonWithBody(_, _, _, _, _, _, _, _): return false
        default: return true
        }
    }
	
	var alertWidth: CGFloat {
		switch UIDevice.current.userInterfaceIdiom {
		case .phone: return 280.0
		case .pad: return 340.0
		default: return 300.0
		}
	}
}


//
//  CellView.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 07/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//


import JTAppleCalendar

private let preDateSelectable: Bool = false
private let todayColor: UIColor = UIColor.black
private let selectableDateColor: UIColor = .black
private let selectedRoundColor: UIColor = .BAmidBlue

class CellView: JTACDayCell {
    
    @IBOutlet weak var stableBackView: AnimationView!
    @IBOutlet var selectedView: AnimationView!
    @IBOutlet var dayLabel: UILabel!
    var cellIsSelected :Bool = false
    
    
    override func awakeFromNib() {
        //self.stableBackView.layer.cornerRadius = self.frame.height * 0.13
        //self.stableBackView.layer.borderColor = UIColor.white.cgColor
        //self.stableBackView.layer.borderWidth = 0.3
        self.isUserInteractionEnabled = true
    }
    
    func handleCellSelection(cellState: CellState, date: Date, selectedDate: Date) {
        
        //InDate, OutDate
        let secondInterval = 2505600 //for 29 days
        let endSelectableDate = Date().addingTimeInterval(TimeInterval(secondInterval))
        if cellState.dateBelongsTo != .thisMonth {
            self.dayLabel.text = ""
            self.isUserInteractionEnabled = false
            self.stableBackView.isHidden = true
        } else if date.isSmaller(to: Date()) && !preDateSelectable {
            self.dayLabel.text = cellState.text
            self.dayLabel.textColor = UIColor.gray
            self.isUserInteractionEnabled = false
            self.stableBackView.isHidden = true
        } else if date.isBigger(to: endSelectableDate){
            self.dayLabel.text = cellState.text
            self.dayLabel.textColor = UIColor.gray
            self.isUserInteractionEnabled = false
            self.stableBackView.isHidden = true
        }else{
            self.stableBackView.isHidden = false
            self.isUserInteractionEnabled = true
            self.stableBackView.isHidden = false
            dayLabel.text = cellState.text
            dayLabel.textColor = selectableDateColor
        }
        
        configueViewIntoBubbleView(cellState, date: date)
        
        if date.isEqual(to: Date()) {
            if !cellState.isSelected {
                self.dayLabel.textColor = todayColor
            }
        }
    }
    
    func cellSelectionChanged(_ cellState: CellState, date: Date) {
        if cellState.isSelected == true {
            if self.cellIsSelected == true {
                configueViewIntoBubbleView(cellState, date: date)
                self.selectedView.animateWithBounceEffect(withCompletionHandler: {
                })
            }
        } else {
            configueViewIntoBubbleView(cellState, date: date, animateDeselection: true)
        }
    }
    
    fileprivate func configueViewIntoBubbleView(_ cellState: CellState, date: Date, animateDeselection: Bool = false) {
        if cellState.isSelected && self.isUserInteractionEnabled {
            self.cellIsSelected = false
            self.selectedView.makeCircular(roundBy: .height)
            self.selectedView.layer.backgroundColor = selectedRoundColor.cgColor
            self.dayLabel.textColor = .white
        } else {
            if animateDeselection {
                if date.isEqual(to: Date())
                {
                    self.dayLabel.textColor = todayColor
                }else{
                    self.dayLabel.textColor = selectableDateColor
                }
                if self.cellIsSelected == false {
                    self.selectedView.layer.backgroundColor = UIColor.clear.cgColor
                    self.cellIsSelected = true
                    //                    self.selectedView.animateWithFadeEffect(withCompletionHandler: { () -> Void in
                    //                        self.selectedView.layer.backgroundColor = UIColor.clear.cgColor
                    //                        self.cellIsSelected = true
                    //                        self.selectedView.alpha = 1
                    //                    })
                }
            } else {
                self.selectedView.layer.backgroundColor = UIColor.clear.cgColor
                self.cellIsSelected = true
            }
        }
    }
}

//
//  CalendarPopUp.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 07/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//


import UIKit
import JTAppleCalendar

protocol CalendarPopUpDelegate: class {
    func dateChaged(date: Date, dateMsg: String)
}

class CalendarPopUp: UIView {
    
    @IBOutlet weak var calendarHeaderLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var calendarView: JTACMonthView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var sunLabel: UILabel!
    @IBOutlet weak var monLabel: UILabel!
    @IBOutlet weak var tueLabel: UILabel!
    @IBOutlet weak var wedLabel: UILabel!
    @IBOutlet weak var friLabel: UILabel!
    @IBOutlet weak var satLabel: UILabel!
    @IBOutlet weak var thuLabel: UILabel!
    var dateStrMsg: String = ""
    
    
    weak var calendarDelegate: CalendarPopUpDelegate?
    
    let calLanguage: CalendarLanguage = .english
    var endDate: Date!
    var startDate: Date = Date().getStart()
    var testCalendar = Calendar(identifier: .gregorian)
    var selectedDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var selected:Date = Date() {
        didSet {
            
            calendarView.scrollToDate(selected)
            calendarView.selectDates([selected])
        }
    }
    
    @IBAction func closePopupButtonPressed(_ sender: AnyObject) {
        if let superView = self.superview as? PopupContainer {
            (superView).close()
        }
    }
    
    @IBAction func GetDateOk(_ sender: Any) {
        
        calendarDelegate?.dateChaged(date: selectedDate, dateMsg: dateStrMsg)
        if let superView = self.superview as? PopupContainer {
            (superView ).close()
        }
    }
    
    @IBAction func leftAction(_ sender: Any) {
        let collectionBounds = self.calendarView.bounds
        let contentOffset = CGFloat(floor(self.calendarView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset:contentOffset)
        
    }
    
    @IBAction func rightAction(_ sender: Any) {
        let collectionBounds = self.calendarView.bounds
        let contentOffset = CGFloat(floor(self.calendarView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    
    override func awakeFromNib() {
        //Calendar
        // You can also use dates created from this function
        
        configureUI()
        endDate = Calendar.current.date(byAdding: .month, value: 1, to: startDate)!
        setCalendar()
        setDate()
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        let cornerRadius = CGFloat(10.0)
        //       let shadowOpacity = Float(1)
        self.layer.cornerRadius = cornerRadius
        //        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        //        layer.masksToBounds = false
        //        layer.shadowColor = UIColor.gray.cgColor
        //        layer.shadowOffset = CGSize(width: 2, height: 5)
        //        layer.shadowOpacity = shadowOpacity
        //        layer.shadowPath = shadowPath.cgPath
    }
    
    func configureUI(){
        sunLabel.font = UIFont.skyTextFont(.regular, size: 12)
        sunLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        monLabel.font = UIFont.skyTextFont(.regular, size: 12)
        monLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        tueLabel.font = UIFont.skyTextFont(.regular, size: 12)
        tueLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        wedLabel.font = UIFont.skyTextFont(.regular, size: 12)
        wedLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        thuLabel.font = UIFont.skyTextFont(.regular, size: 12)
        thuLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        friLabel.font = UIFont.skyTextFont(.regular, size: 12)
        friLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        satLabel.font = UIFont.skyTextFont(.regular, size: 12)
        satLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        
        yearLabel.font = UIFont.skyTextFont(.medium, size: 14)
        dateLabel.font = UIFont.skyTextFont(.bold, size: 31)
        calendarHeaderLabel.font = UIFont.skyTextFont(.medium, size: 14)
        cancelButton.titleLabel?.font = UIFont.skyTextFont(.medium, size: 14)
        cancelButton.titleLabel?.textColor = .BABlueColor
        okButton.titleLabel?.font = UIFont.skyTextFont(.medium, size: 14)
        okButton.titleLabel?.textColor = .BABlueColor
    }
    
    func setDate() {
        let month = testCalendar.dateComponents([.month], from: selectedDate).month!
        let weekday = testCalendar.component(.weekday, from: selectedDate)
        
        let monthName = GetHumanDate(month: month, language: calLanguage) // DateFormatter().monthSymbols[(month-1) % 12] //
        let week = GetWeekByLang(weekDay: weekday, language: calLanguage) // DateFormatter().shortWeekdaySymbols[weekday-1]
        
        let day = testCalendar.component(.day, from: selectedDate)
        let year = testCalendar.component(.year, from: selectedDate)
        dateLabel.text = "\(week.prefix(3)), " + monthName.prefix(3) + " " + String(day)
        let dayLastDigit = String(day).suffix(1)
        var daySuffix = ""
        if (String(day).count > 1) && (String(day).prefix(1) == "1") {
            daySuffix = "th"
        } else {
            switch dayLastDigit {
            case "1" :
                daySuffix = "st"
            case "2" :
                daySuffix = "nd"
            case "3" :
                daySuffix = "rd"
            default :
                daySuffix = "th"
            }
        }
        dateStrMsg = "\(week) " + String(day) + daySuffix + " " + monthName.prefix(3) + " " + String(year).suffix(2)
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = GetHumanDate(month: month, language: calLanguage) // DateFormatter().monthSymbols[Int(month.month!)-1]
        
        let year = testCalendar.component(.year, from: startDate)
        calendarHeaderLabel.text = monthName + " " + String(year)
        yearLabel.text = String(year)
    }
    
    func setCalendar() {
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        let nibName = UINib(nibName: "CellView", bundle:nil)
        calendarView.register(nibName, forCellWithReuseIdentifier: "CellView")
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        calendarView.scrollDirection = .horizontal
        calendarView.scrollingMode = .stopAtEachCalendarFrame
        calendarView.showsHorizontalScrollIndicator = false
        calendarView.isUserInteractionEnabled = true
    }
    
    func moveCollectionToFrame(contentOffset:CGFloat){
        UIView.animate(withDuration: 0.3, animations: {
            let frame : CGRect = CGRect(x: contentOffset, y: self.calendarView.contentOffset.y, width: self.calendarView.frame.width, height: self.calendarView.frame.height)
            self.calendarView.scrollRectToVisible(frame, animated: false)
        }) { _ in
            self.setupViewsOfCalendar(from: self.calendarView.visibleDates())
        }
    }
    
}

// MARK : JTAppleCalendarDelegate
extension CalendarPopUp: JTACMonthViewDelegate, JTACMonthViewDataSource {
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: testCalendar,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: DaysOfWeek.sunday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        (cell as? CellView)?.handleCellSelection(cellState: cellState, date: date, selectedDate: selectedDate)
    }
    
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
        myCustomCell.handleCellSelection(cellState: cellState, date: date, selectedDate: selectedDate)
        return myCustomCell
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath){
        selectedDate = date
        (cell as? CellView)?.cellSelectionChanged(cellState, date: date)
    }
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        (cell as? CellView)?.cellSelectionChanged(cellState, date: date)
        
    }
    
    
    func calendar(_ calendar: JTACMonthView, willResetCell cell: JTACDayCell) {
        (cell as? CellView)?.selectedView.isHidden = true
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
}


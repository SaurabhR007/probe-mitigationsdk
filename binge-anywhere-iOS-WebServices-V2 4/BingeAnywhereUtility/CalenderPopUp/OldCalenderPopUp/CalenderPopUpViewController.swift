//
//  CalenderPopUpViewController.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 05/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalenderPopUpViewController: UIViewController {
    
    
    @IBOutlet weak var yearLabel: UIView!
    @IBOutlet weak var monthYearView: UILabel!
    @IBOutlet weak var calenderView: JTACMonthView!
    @IBOutlet weak var dateHeaderLabel: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    var curMonth:Int = 0
    var curYear:Int = 0
    var curDate:Int = 0
    
    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calenderView.scrollDirection = .horizontal
        calenderView.scrollingMode = .stopAtEachCalendarFrame
        calenderView.showsHorizontalScrollIndicator = false
        setCalendar()

    }
    
    func setCalendar() {
        calenderView.calendarDataSource = self
        calenderView.calendarDelegate = self
        calenderView.allowsSelection = true
        calenderView.isUserInteractionEnabled = true
        let nibName = UINib(nibName: "DateCollectionViewCell", bundle:nil)
        calenderView.register(nibName, forCellWithReuseIdentifier: "DateCollectionViewCell")
        //calenderView.minimumLineSpacing = 0
        //calenderView.minimumInteritemSpacing = 0
    }

//    func configureCell(view: JTACDayCell?, cellState: CellState) {
//        guard let cell = view as? DateCollectionViewCell  else { return }
//        cell.dateLabel.text = cellState.text
//        handleCellTextColor(cell: cell, cellState: cellState)
//        handleCellSelected(cell: cell, cellState: cellState)
//        if cellState.dateBelongsTo == .thisMonth{
//            let date:Date = cellState.date
//            curMonth = Calendar.current.dateComponents([.month], from: date).month!
//            curYear = Calendar.current.dateComponents([.year], from: date).year!
//            curDate = Calendar.current.dateComponents([.day], from: Date()).day!
//            print(curMonth,curYear,curDate)
//        }
//    }
//
//    func handleCellTextColor(cell: DateCollectionViewCell, cellState: CellState) {
//        if cellState.dateBelongsTo == .thisMonth {
//            cell.dateLabel.textColor = UIColor.black
//            cell.isHidden = false
//        } else {
//            cell.dateLabel.textColor = UIColor.gray
//            cell.isHidden = true
//        }
//    }
//
//    func handleCellSelected(cell: DateCollectionViewCell, cellState: CellState) {
//        if cellState.isSelected {
//            //cell.selectedView.layer.cornerRadius =  18
//            cell.dateLabel.textColor = .white
//            cell.selectedView.isHidden = false
//        } else {
//            cell.selectedView.isHidden = true
//        }
//    }
    
    func moveCollectionToFrame(contentOffset:CGFloat){
        let frame : CGRect = CGRect(x: contentOffset, y: self.calenderView.contentOffset.y, width: self.calenderView.frame.width, height: self.calenderView.frame.height)
        self.calenderView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func leftAction(_ sender: Any) {
        let collectionBounds = self.calenderView.bounds
        let contentOffset = CGFloat(floor(self.calenderView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset:contentOffset)
    }
    
   
    @IBAction func rightAction(_ sender: Any) {
        let collectionBounds = self.calenderView.bounds
        let contentOffset = CGFloat(floor(self.calenderView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismissDetail()
    }
    
    @IBAction func okTapped(_ sender: Any) {
        
    }
    
}

extension CalenderPopUpViewController: JTACMonthViewDataSource {
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"

        let startDate = formatter.date(from: "2018 01 01")!
        let endDate = Date()
        return ConfigurationParameters(startDate: startDate, endDate: endDate)
    }
}

extension CalenderPopUpViewController: JTACMonthViewDelegate {
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCell", for: indexPath) as! DateCollectionViewCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        (cell as? DateCollectionViewCell )?.configureCell(cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath){
        (cell as? DateCollectionViewCell )?.configureCell(cellState: cellState)
    }

    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        (cell as? DateCollectionViewCell )?.configureCell(cellState: cellState)
    }
}

//
//  DateCollectionViewCell.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 05/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//


import JTAppleCalendar

class DateCollectionViewCell: JTACDayCell {
    

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(cellState: CellState) {
            dateLabel.text = cellState.text
            handleCellTextColor(cellState: cellState)
            handleCellSelected(cellState: cellState)
        }

        func handleCellTextColor(cellState: CellState) {
            if cellState.dateBelongsTo == .thisMonth {
                dateLabel.textColor = UIColor.black
                self.isHidden = false
            } else {
                dateLabel.textColor = UIColor.gray
                self.isHidden = true
            }
        }
    
        func handleCellSelected(cellState: CellState) {
            if cellState.isSelected {
                //cell.selectedView.layer.cornerRadius =  18
                dateLabel.textColor = .white
                selectedView.isHidden = false
            } else {
                selectedView.isHidden = true
            }
        }

}

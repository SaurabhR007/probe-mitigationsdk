//
//  UIViewRotation.swift
//  TataSky-Universal
//
//  Created by Neha Jain on 08/05/17.
//  Copyright © 2017 Ajay Sharma. All rights reserved.
//

import Foundation
import UIKit
import ARSLineProgress

fileprivate var dispatchGroup = DispatchGroup()
var timer: Timer?

protocol NetworkDataProtocol {
    func showActivityIndicator(isUserInteractionEnabled: Bool)
    func hideActivityIndicator()
    func showNoWifiScreen(onView placeHolderSuperView: UIView)
    //func showLoaderAnimation()
    //    func showNoDataAvailableScreen(onView placeHolderSuperView: UIView)
    //    func showPlaceholderScreenForLogin(onView placeHolderSuperView: UIView, typeOfView: TypeOfView)
    //    func showPlaceholderScreenWithoutButton(onView placeHolderSuperView: UIView, typeOfView: TypeOfView)
    //    func retryButtonAction()
    //    func resumePlayingfromRestore()
    //    func resumePlayingfromBeginning()
    //    func cancelCWPopUp()
    func removePlaceholderView()
    //    func showNoInternetAlert()
    //    func showDownloadableListing()
    //    func showNoDataPlaceholderForSeeAll(onView placeHolderSuperView: UIView, stringArray: [String])
}

//@objc protocol NetworkDataShowLoaderProtocol {
//    @objc func showLoaderAnimation()
//}

extension NetworkDataProtocol where Self: UIViewController /*& NetworkDataShowLoaderProtocol*/ {
    
    // MARK: - Placeholder view
    func showNoWifiScreen(onView placeHolderSuperView: UIView) {
        hideActivityIndicator()
        removePlaceholderView()
        let placeholderView: NoDataFoundView = .fromNib()
        placeholderView.tag = 990
        placeholderView.setUI(forViewType: .noInternet, typeOfButton: .noButton)
        placeholderView.retryButton.removeTarget(nil, action: nil, for: .allEvents)
        placeholderView.retryButton.addTarget(self, action: #selector(BaseViewController.retryButtonAction as (BaseViewController) -> () -> Void), for: .touchUpInside)
        setFrameAndAddSubview(placeholderView: placeholderView, placeHolderSuperView: placeHolderSuperView)
    }
    
    func showTimedOutScreen(onView placeHolderSuperView: UIView) {
        hideActivityIndicator()
        removePlaceholderView()
        let placeholderView: NoDataFoundView = .fromNib()
        placeholderView.tag = 990
        placeholderView.setUI(forViewType: .retry, typeOfButton: .retry)
        placeholderView.retryButton.removeTarget(nil, action: nil, for: .allEvents)
        placeholderView.retryButton.addTarget(self, action: #selector(BaseViewController.retryButtonAction as (BaseViewController) -> () -> Void), for: .touchUpInside)
        setFrameAndAddSubview(placeholderView: placeholderView, placeHolderSuperView: placeHolderSuperView)
    }
    
    func showNoDataAvailableScreen(onView placeHolderSuperView: UIView) {
        removePlaceholderView()
        let placeholderView: NoDataFoundView = .fromNib()
        placeholderView.tag = 990
        placeholderView.setUI(forViewType: .noInternet, typeOfButton: .noButton)
        placeholderView.retryButton.removeTarget(nil, action: nil, for: .allEvents)
        setFrameAndAddSubview(placeholderView: placeholderView, placeHolderSuperView: placeHolderSuperView)
    }
    
    func showPlaceholderScreenWithoutButton(onView placeHolderSuperView: UIView, typeOfView: TypeOfView) {
        removePlaceholderView()
        let placeholderView: NoDataFoundView = .fromNib()
        placeholderView.tag = 990
        placeholderView.setUI(forViewType: typeOfView, typeOfButton: .noButton)
        placeholderView.retryButton.removeTarget(nil, action: nil, for: .allEvents)
        setFrameAndAddSubview(placeholderView: placeholderView, placeHolderSuperView: placeHolderSuperView)
    }
    
    func removePlaceholderView() {
        if let placeHolderView = view.viewWithTag(990) {
            placeHolderView.removeFromSuperview()
        }
    }
    
    func setFrameAndAddSubview(placeholderView: UIView, placeHolderSuperView: UIView) {
        placeholderView.frame = placeHolderSuperView.bounds
        placeholderView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        placeHolderSuperView.addSubview(placeholderView)
    }
    
    // MARK: - Button Action
    func retryButtonAction() {
        print("retry action of protocol")
    }
    
    func loginButtonAction() {
        print("login action of protocol")
    }
    
    func showDownloadableListing() {
        print("show Downloadable Listing")
    }
    // MARK: - Activity indicato@objc r
    func showActivityIndicator(isUserInteractionEnabled: Bool) {
        //timer = Timer.scheduledTimer(timeInterval: Double((BAConfigManager.shared.configModel?.data?.config?.loaderDelayTime ?? 100) / 1000), target: self, selector: #selector(showLoaderAnimation), userInfo: nil, repeats: false)
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: Double((BAConfigManager.shared.configModel?.data?.config?.loaderDelayTime ?? 100) / 1000), repeats: false) { [weak self] timer in
                self?.showLoaderAnimation()
            }
        }
//        self.showLoaderAnimation()
    }
    
    func showLoaderAnimation() {
        DispatchQueue.main.async {
            print("------------------ Show Loader --------------------")
            CustomLoader.shared.showLoader(false)
        }
    }
    
    func hideActivityIndicator() {
        print("------------------ Hide Loader --------------------")
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        CustomLoader.shared.hideLoader()
    }
    
    func showNoInternetAlert() {
        DispatchQueue.main.async {
            AlertController.initialization().showAlert(.singleButton(.ok, .withTopImage(.noInternet), .withTitle("No Internet Conncetion"), .withMessage("Make sure that Wi-Fi or mobile data is turned on, then try again."), .hidden, .hidden)) { _ in }
        }
    }
}

extension UIView {
    
    func rotate360Degrees(duration: CFTimeInterval = 0.3, repeatCount: Float = .infinity) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = repeatCount
        layer.add(rotateAnimation, forKey: nil)
    }
    
    func stopRotation () {
        layer.removeAllAnimations()
    }
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

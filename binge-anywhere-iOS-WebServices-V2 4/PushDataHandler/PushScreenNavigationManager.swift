//
//  PushScreenNavigationManager.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 23/03/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class PushScreenNavigationManager: BaseViewController {
    
    internal static let sharedInstance: PushScreenNavigationManager = {
        return PushScreenNavigationManager()
    }()
    
    let taRecommendationVM = TARecommendationVM()
    
    func moveToPIScreen(_ contentType: String, _ vodId: Int) {
        // MARK: Content Detail API Calling
        if let topMostController = UIApplication.shared.topMostViewController() {
            let contentDetailVM = BAVODDetailViewModal(repo: ContentDetailRepo(), endPoint: contentType + "/\(vodId)")
            if let vc = topMostController as? NetworkDataProtocol {
                vc.showActivityIndicator(isUserInteractionEnabled: true)
            }
            contentDetailVM.getContentScreenData { (flagValue, message, isApiError) in
                if let vc = topMostController as? NetworkDataProtocol {
                    vc.hideActivityIndicator()
                }
                if flagValue {
                    let contentDetailVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.BAVODDetailStoryBoard.rawValue, identifierVC: ViewControllers.contentDetailVC.rawValue, type: BAVODDetailViewController.self)
                    contentDetailVC.parentNavigation = topMostController.navigationController
                    contentDetailVC.contentDetail = contentDetailVM.contentDetail
                    contentDetailVC.id = vodId
                    contentDetailVC.hideActivityIndicator()
                    topMostController.navigationController?.pushViewController(contentDetailVC, animated: true)
                } else if isApiError {
                    self.apiError(message, title: kSomethingWentWrong)
                } else {
                    self.apiError(message, title: "")
                }
            }
        }
    }
    
    
    func navigateToTASeeAll(_ screenName: PushScreenName, _ data: [String: Any]) {
        if let topMostController = UIApplication.shared.topMostViewController() {
            if !checkCurrentVC(vC: topMostController, screenName: screenName) {
                switch screenName {
                case .continueWatch_Screen:
                    let cwSeeAllVC: BAContinueWatchingSeeAllViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.continueWatchVC.rawValue, type: BAContinueWatchingSeeAllViewController.self)
                    cwSeeAllVC.isCW = true
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, cwSeeAllVC)
                    } else {
                        topMostController.navigationController?.pushViewController(cwSeeAllVC, animated: true)
                    }
                    break
                case .recommendation_Screen:
                    let placeHolder = data["placeHolder"] as? String ?? ""
                    let title = data["railTitle"] as? String ?? ""
                    let id = data["id"] as? String ?? ""
                    let homeScreenData = HomeScreenDataItems()
                    taRecommendationVM.fetchTARecommendationSeeAllRailData(placeHolder, title, id: id) { (seeAllData) in
                        if data.count > 0 {
                            let seeAllVC: SeeAllVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllVC.rawValue, type: SeeAllVC.self)
                            seeAllVC.contentId = Int(id) ?? 0
                            seeAllVC.isTAData = true
                            seeAllVC.isTVODData = false
                            homeScreenData.contentList = seeAllData
                            homeScreenData.title = title
                            homeScreenData.layoutType = "LANDSCAPE"
                            homeScreenData.placeHolder = placeHolder
                            homeScreenData.id = Int(id) ?? 0
                            homeScreenData.sectionType = TAConstant.recommendationType.rawValue
                            seeAllVC.taContentlListData = homeScreenData
                            if self.checkPlayerVC(vC: topMostController) {
                                self.navigateVIAFullScreenMoviePlayer(topMostController, seeAllVC)
                            } else {
                                topMostController.navigationController?.pushViewController(seeAllVC, animated: true)
                            }
                        }
                    }
                    break
                default:
                    break
                }
            }
        }
    }
    
    func navigateToSpecificScreen(screenName: PushScreenName, railId: Int?, railTitle: String?) {
        if let topMostController = UIApplication.shared.topMostViewController() {
            if !checkCurrentVC(vC: topMostController, screenName: screenName) {
                switch screenName {
                case .home_Screen:
                    kAppDelegate.createHomeTabRootView()
                    break
                case .login_Screen:
                    UtilityFunction.shared.logoutUser()
                    kAppDelegate.createLoginRootView(isUserLoggedOut: true)
                    break
                case .myAccount_Screen:
                    let myAccountVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.homeTab.fileName, identifierVC: ViewControllers.accountVC.rawValue, type: AccountVC.self)
                    changeTabSelection(3)
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, myAccountVC)
                    } else {
                        topMostController.navigationController?.pushViewController(myAccountVC, animated: true)
                    }
                    break
                case .selfCareRecharge_Screen:
                    break
                case .watchList_Screen:
                    let watchListVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.homeTab.fileName, identifierVC: ViewControllers.watchListVC.rawValue, type: WatchlistVC.self)
                    changeTabSelection(2)
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, watchListVC)
                    } else {
                        topMostController.navigationController?.pushViewController(watchListVC, animated: true)
                    }
                    break
                case .helpFaq_Screen:
                    let moreVC: MoreVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.homeTab.fileName, identifierVC: ViewControllers.moreVC.rawValue, type: MoreVC.self)
                    moreVC.navigateToFAQ = true
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, moreVC)
                    } else {
                        topMostController.navigationController?.pushViewController(moreVC, animated: false)
                    }
                    break
                case .seeAll_Screen:
                    let seeAllVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllVC.rawValue, type: SeeAllVC.self)
                    seeAllVC.contentId = railId ?? 0
                    seeAllVC.title = railTitle
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, seeAllVC)
                    } else {
                        topMostController.navigationController?.pushViewController(seeAllVC, animated: true)
                    }
                    break
                case .appSeeAll_Screen:
                    let appsSeeAllVC: BAAppsSeeAllViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllForApps.rawValue, type: BAAppsSeeAllViewController.self)
                    appsSeeAllVC.contentId = railId ?? 0
                    appsSeeAllVC.title = railTitle
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, appsSeeAllVC)
                    } else {
                        topMostController.navigationController?.pushViewController(appsSeeAllVC, animated: true)
                    }
                    break
                case .taSeeAll_Screen:
                    let seeAllVC  = UIStoryboard.loadViewController(storyBoardName: StoryboardType.seeAllStoryBoard.fileName, identifierVC: ViewControllers.seeAllVC.rawValue, type: SeeAllVC.self)
                    seeAllVC.contentId = railId ?? 0
                    seeAllVC.title = railTitle
                    if checkPlayerVC(vC: topMostController) {
                        navigateVIAFullScreenMoviePlayer(topMostController, seeAllVC)
                    } else {
                        topMostController.navigationController?.pushViewController(seeAllVC, animated: true)
                    }
                    break
                default:
                    break
                }
            }
        }
    }
    
    func changeTabSelection(_ idx: Int) {
        if let rootWindow = kAppDelegate.window?.rootViewController {
            if let tabView = rootWindow as? UITabBarController {
                tabView.selectedIndex = idx
            }
        }
    }
    
    func checkCurrentVC(vC: UIViewController, screenName: PushScreenName) -> Bool {
        switch screenName {
        case .myAccount_Screen:
            return vC is AccountVC
        case .watchList_Screen:
            return vC is WatchlistVC
        case .helpFaq_Screen:
            return vC is BAWebViewViewController
        default:
            return false
        }
    }
    
    func checkPlayerVC(vC: UIViewController) -> Bool {
        return vC is BAFullScreenMoviePlayerViewController
    }
    
    func navigateVIAFullScreenMoviePlayer(_ currentVC: UIViewController,_ navigationVC: UIViewController) {
        if let vc = currentVC as? BAFullScreenMoviePlayerViewController {
            vc.delegate?.isDismissed()
            vc.dismiss(animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if let topMostController = UIApplication.shared.topMostViewController() {
                        topMostController.navigationController?.pushViewController(navigationVC, animated: true)
                    }
                }
            }
        }
    }
}

//
//  PubNubManager.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 07/04/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import PubNub

class PubNubManager {
    
    private var client: PubNub?
    var pubNubChannel: String?
    var subscriptionViewModel: PackSubscriptionVM?
    //    let channels = ["Channel-5a16kuuc1"]  My Own Id PubNubChannel
    var channels = ["sub_\(BAKeychainManager().sId)"]
    let listener = SubscriptionListener(queue: .main)
    var deviceInfoArray: [DeviceInfo]?
    var switchAccountModel: BAATVUpgradeViewModal?
    
    
    internal static let shared: PubNubManager = {
        return PubNubManager()
    }()
    
    //Initializer access level change now
    private init () {
        self.recheckVM()
    }
    
    private func recheckVM() {
        if self.subscriptionViewModel == nil {
            self.subscriptionViewModel = PackSubscriptionVM()
        }
        
        if self.switchAccountModel == nil {
            self.switchAccountModel = BAATVUpgradeViewModal(repo: BAATVUpgradeRepo())
        }
    }
    
    
    func setUpPubNub() {
        if BAReachAbility.isConnectedToNetwork() {
            PubNub.log.levels = [.all]
            channels = ["sub_\(BAKeychainManager().sId)"]
            PubNub.log.writers = [ConsoleLogWriter(), FileLogWriter()]
            if let clientObj = client {
                clientObj.subscribe(to: [channels[0]], withPresence: true)
                client?.add(listener)
                pubNubListener()
                return
            }
            var config = PubNubConfiguration(publishKey: UtilityFunction.shared.isUatBuild() ? kPubNubUatPublicKey : kPubNubProdPublicKey, subscribeKey: UtilityFunction.shared.isUatBuild() ? kPubNubUatSubscriberKey : kPubNubProdSubscriberKey)
            config.uuid = kDeviceId
            //            config.origin = ""
            client = PubNub(configuration: config)
            client?.subscribe(to: [channels[0]], withPresence: true)
            client?.add(listener)
            pubNubListener()
        }
    }
    
    func pubNubListener() {
        listener.didReceiveMessage = { message in
            print("[Message]: \(message.payload)")
            self.parsePubNubJSON(data: message.payload)
            self.checkForLogout()
            //                self.parsePubNubDict()  Uncomment to perform some action on receiving message
        }
        listener.didReceiveStatus = { status in
            switch status {
            case .success(let connection):
                if connection == .connected {
                    //                        self.client!.publish(channel: self.channels[0], message: "Hello PubNub Swift!") { result in
                    //                            print(result.map { "Publish Response at \($0.timetoken.timetokenDate)" })
                    //                        }
                }
            case .failure(let error):
                print("Status Error: \(error.localizedDescription)")
            }
        }
        client!.add(listener)
        client!.subscribe(to: channels, withPresence: true)
    }
    
    
    func parsePubNubJSON(data: JSONCodable) {
        if BAKeychainManager().userAccessToken.isEmpty {
            return 
        }
        let decoder = JSONDecoder()
        if let _data = data.jsonData {
            if let jsonPubNub = try? decoder.decode(PubNubDataModel.self, from: _data) {
                self.deviceInfoArray = jsonPubNub.deviceInfo
            }
        }
    }
    
    
    
    func checkForLogout() {
        if let _deviceInfoArray = deviceInfoArray {
            for deviceInfo in _deviceInfoArray {
                //deviceInfo.deviceType?.caseInsensitiveCompare("ANYWHERE") == .orderedSame &&
                if deviceInfo.baId == BAKeychainManager().baId {
                    BAKeychainManager().contentPlayBack = deviceInfo.contentPlayBackHybrid ?? false
                    BAKeychainManager().deviceLoggedOut = deviceInfo.logoutHybrid ?? false
                    BAKeychainManager().atvCancelled = deviceInfo.atvCancelled ?? false
                    if BAKeychainManager().deviceLoggedOut {
                        UtilityFunction.shared.alertPopUpBox(true)
                    } else if BAKeychainManager().atvCancelled {
                        checkForBingeUpdate()
                    } else {
                        checkForDeviceIdInList(deviceInfo)
                    }
                }
            }
        }
    }
    
    
    func checkForDeviceIdInList (_ deviceInfo: DeviceInfo) {
        if let deviceList = deviceInfo.deviceList {
            if deviceList.contains(kDeviceId) {
                callCurrentPackSubscriptionModel()
            } else {
                UtilityFunction.shared.alertPopUpBox(false)
            }
        }
    }
    
    func checkForBingeUpdate() {
        if BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.largeScreenEligibility ?? false {
            DispatchQueue.main.async {
                CustomLoader.shared.hideLoader()
                AlertController.initialization().showAlert(.singleButton(.ok, .hidden, .withTitle(""), .withMessage(kBingePlus299Pack), .hidden, .hidden)) { _ in
                    self.switchATVAccount()
                }
            }
        } else {
            DispatchQueue.main.async {
                CustomLoader.shared.hideLoader()
                AlertController.initialization().showAlert(.singleButton(.ok, .hidden, .withTitle(""), .withMessage(kBingePlus149Pack), .hidden, .hidden)) { _ in
                    self.switchATVAccount()
                }
            }
        }
    }
    
    func switchATVAccount() {
        if let switchAccountVM = switchAccountModel {
            switchAccountVM.switchUser(completion: { (flag, message, isApiError) in
                //self.hideActivityIndicator()
                if flag {
                    self.dismissController()
                } else if isApiError {
                   // self.apiError(message ?? "Failed to select profile", title: kSomethingWentWrong)
                } else {
                   // self.apiError(message ?? "Failed to select profile", title: "")
                }
            })
        }
    }
    
    func dismissController() {
        //var userProfile = [String: String]()
//        userProfile["name"] = userProfileData?[_index].aliasName ?? ""
//        userProfile["aliasName"] = userProfileData?[_index].aliasName ?? ""
//        userProfile["baId"] = String(userProfileData?[_index].baId ?? 0)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfile"), object: nil, userInfo: userProfile)
        navigateToHome()
        //self.navigationController!.popToViewController(controller, animated: true)
    }
    
    func navigateToHome() {
        kAppDelegate.createHomeTabRootView()
    }
    
    func checkForCurrentUser() -> Bool {
        if let _deviceInfoArray = deviceInfoArray {
            for deviceInfo in _deviceInfoArray {
                //                if deviceInfo.deviceType?.caseInsensitiveCompare("ANYWHERE") == .orderedSame && deviceInfo.baId == BAUserDefaultManager().baId {
                if deviceInfo.baId == BAKeychainManager().baId {
                    return true
                }
            }
        }
        return false
    }
    
    func callCurrentPackSubscriptionModel() {
        if checkForCurrentUser() {
            subscriptionViewModel?.packSubscriptionApiCall(completion: { (flagValue, message, isApiError) in
                if flagValue {
                    UtilityFunction.shared.performTaskInMainQueue {
                        UtilityFunction.shared.postNotificationObserver(name: .notificationPackSubscription)
                    }
                }
            })
        }
    }
    
    func checkForAlreadySubscribedChannel() -> Bool {
        if BAReachAbility.isConnectedToNetwork() {
            guard let clientObj = client?.subscribedChannels.contains(channels[0]) else {
                return false
            }
            return clientObj
        } else {
            return false
        }
    }
    
    func unsubscribedFromChannel() {
        if BAReachAbility.isConnectedToNetwork() {
            if let clientObj = client {
                clientObj.unsubscribeAll()
            }
        }
    }
    
    //TODO : Refactor with Shivam
    func getSubscribedChannelHistory() {
        if BAReachAbility.isConnectedToNetwork() {
            client?.fetchMessageHistory(for: [channels[0]], completion: { (result) in
                switch result {
                case .success(let message):
                    print(message)
                case .failure(let error):
                    print(error)
                }
            })
        }
    }
    
//    func parsePubNubDict() {
//        alertPopUpBox()
//    }
    
//
//    func alertPopUpBox() {
//        DispatchQueue.main.async {
//            AlertController.initialization().showAlert(.singleButton(.okay, .hidden, .bingeAnywhere, .withMessage("Your device has been removed from another location"), .hidden, .hidden)) { _ in
//                self.logOutUser()
////                let vc = MoreVC()
////                if let topMostController = UIApplication.shared.topMostViewController() {
////                    if let topVC = topMostController as? BaseViewController {
////                        topVC.showActivityIndicator(isUserInteractionEnabled: true)
////                        vc.signOut()
////                        return
////                    } else {
////                        vc.signOut()
////                    }
////                }
//            }
//        }
//    }
}

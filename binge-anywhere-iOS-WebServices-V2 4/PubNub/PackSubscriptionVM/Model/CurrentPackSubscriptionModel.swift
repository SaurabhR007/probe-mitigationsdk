//
//  CurrentPackSubscriptionModel.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation


struct CurrentPackSubscriptionModel : Codable {
    let code : Int?
    let message : String?
    let data : PartnerSubscriptionDetails?
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(PartnerSubscriptionDetails.self, forKey: .data)
    }
    
}

//struct SubscriptionData : Codable {
//    let packName : String?
//    let packId : String?
//    let packCreatedDate : String?
//    let packPrice : String?
//    let expirationDate : String?
//    let rechargeDue : String?
//    let amountDue : String?
//    let providers : [Providers]?
//
//    enum CodingKeys: String, CodingKey {
//
//        case packName = "packName"
//        case packId = "packId"
//        case packCreatedDate = "packCreatedDate"
//        case packPrice = "packPrice"
//        case expirationDate = "expirationDate"
//        case rechargeDue = "rechargeDue"
//        case amountDue = "amountDue"
//        case providers = "providers"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        packName = try values.decodeIfPresent(String.self, forKey: .packName)
//        packId = try values.decodeIfPresent(String.self, forKey: .packId)
//        packCreatedDate = try values.decodeIfPresent(String.self, forKey: .packCreatedDate)
//        packPrice = try values.decodeIfPresent(String.self, forKey: .packPrice)
//        expirationDate = try values.decodeIfPresent(String.self, forKey: .expirationDate)
//        rechargeDue = try values.decodeIfPresent(String.self, forKey: .rechargeDue)
//        amountDue = try values.decodeIfPresent(String.self, forKey: .amountDue)
//        providers = try values.decodeIfPresent([Providers].self, forKey: .providers)
//    }
//
//}
//
//
//struct Providers : Codable {
//    let name : String?
//    let providerId : String?
//    let iconUrl : String?
//
//    enum CodingKeys: String, CodingKey {
//        case name = "name"
//        case providerId = "providerId"
//        case iconUrl = "iconUrl"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        name = try values.decodeIfPresent(String.self, forKey: .name)
//        providerId = try values.decodeIfPresent(String.self, forKey: .providerId)
//        iconUrl = try values.decodeIfPresent(String.self, forKey: .iconUrl)
//    }
//
//}

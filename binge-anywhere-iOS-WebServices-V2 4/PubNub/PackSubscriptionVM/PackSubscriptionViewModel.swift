//
//  PackSubscriptionViewModel.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import HungamaPlayer
import Mixpanel

protocol PackSubscriptionViewModel {
    func packSubscriptionApiCall(completion: @escaping (Bool, String?, Bool) -> Void)
}


class PackSubscriptionVM: PackSubscriptionDataCall, PackSubscriptionViewModel {
    var apiParams = [AnyHashable : Any]()
    var requestToken: ServiceCancellable?
    
    
    func packSubscriptionApiCall(completion: @escaping (Bool, String?, Bool) -> Void) {
        createPostDict()
        requestToken = packSubscriptionDataCall(completion: { (apiData, error)  in
            self.requestToken = nil
            if let _apiData = apiData {
                if _apiData.serviceResponse.statusCode == 401 {
                    completion(false, kSessionExpire, false)
                }
                if let statusCode = _apiData.parsed.code {
                    if statusCode == 0 {
                        
                        BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails = _apiData.parsed.data
                        BAKeychainManager().contentPlayBack = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.contentPlayBackHybrid ?? false
                        BAKeychainManager().deviceLoggedOut = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.logoutHybrid ?? false
                        ErosNowConstants.partnerId = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.partnerUniqueId ?? ""
                        ErosNowConstants.userToken = BAKeychainManager().acccountDetail?.deviceAuthenticateToken ?? ""
                        self.setHungamaPartnerUniqueId()
                        MixpanelManager.shared.updatePackChanges() //Code commented as per subhro and QE team discussion
                        if BAKeychainManager().deviceLoggedOut {
                            UtilityFunction.shared.alertPopUpBox(true)
                        } else {
                            completion(true, nil, false)
                        }
                    } else {
                        if _apiData.serviceResponse.statusCode != 200 {
                            if let errorMessage = _apiData.parsed.message {
                                completion(false, errorMessage, true)
                            } else {
                                completion(false, "Some Error Occurred", true)
                            }
                        } else {
                            if let errorMessage = _apiData.parsed.message {
                                completion(false, errorMessage, false)
                            } else {
                                completion(false, "Some Error Occurred", false)
                            }
                        }
                    }
                } else {
                    completion(false, "Some Error Occurred", true)
                }
            } else if let error = error {
                completion(false, error.error.localizedDescription, true)
            } else {
                completion(false, "Some Error Occurred", true)
            }
        })
    }
    
    func setHungamaPartnerUniqueId() {
        if BAKeychainManager().isHungamaIdSet {return}
        let hungamaPartnerDSN = BAKeychainManager().acccountDetail?.partnerSubscriptionsDetails?.hungamaDSN ?? ""
        let sdkMode = UtilityFunction.shared.isUatBuild() ? SDKMode.uat : SDKMode.production
        BAKeychainManager().isHungamaIdSet = true
        HungamaPlayerManager.shared.setPartnerUniqueId(hungamaPartnerDSN)
        HungamaPlayerManager.shared.setSDKMode(sdkMode)
    }

    
    func createPostDict() {
        apiParams["baId"] = BAKeychainManager().baId
        apiParams["sid"] = BAKeychainManager().sId
    }
    
}

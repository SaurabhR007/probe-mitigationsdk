//
//  PackSubscritpionDataCall.swift
//  BingeAnywhere
//
//  Created by Shivam Srivastava on 25/06/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

protocol PackSubscriptionDataCall {
    var apiParams: [AnyHashable: Any] {get set}
    func packSubscriptionDataCall(completion: @escaping(APIServicResult<CurrentPackSubscriptionModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable?
}

extension PackSubscriptionDataCall {
    func packSubscriptionDataCall(completion: @escaping(APIServicResult<CurrentPackSubscriptionModel>?, ServiceProviderError?) -> Void) -> ServiceCancellable? {
        var target = ServiceRequestDetail.init(endpoint: .currentPackSubscriptionDetail(param: apiParams))
        
        target.detail.headers["authorization"] = BAKeychainManager().userAccessToken
        target.detail.headers["subscriberId"] = BAKeychainManager().sId
        
        return APIService().request(target) { (response: APIResult<APIServicResult<CurrentPackSubscriptionModel>, ServiceProviderError>) in
            completion(response.value, response.error)
        }
    }
}

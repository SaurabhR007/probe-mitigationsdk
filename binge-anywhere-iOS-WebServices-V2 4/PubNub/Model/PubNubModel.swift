//
//  PubNubDataModel.swift
//  BingeAnywhere
//
//  Created by Shivam on 23/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation
import Foundation

struct PubNubDataModel : Codable {
    
    let acStatus : String?
    let deviceInfo : [DeviceInfo]?
    let devices : [Device]?
    let entitlements : [Entitlement]?
    let isDeviceManagement : Bool?
    let isPremium : Bool?
    let isPVR : Bool?
    let profiles : [Profiles]?
    let rmn : String?
    let sid : String?
    let sName : String?
    let prime: PrimeData?
    let timestamp : String?
    
    enum CodingKeys: String, CodingKey {
        case acStatus = "acStatus"
        case deviceInfo = "deviceInfo"
        case devices = "devices"
        case entitlements = "entitlements"
        case isDeviceManagement = "isDeviceManagement"
        case isPremium = "isPremium"
        case isPVR = "isPVR"
        case profiles = "profiles"
        case rmn = "rmn"
        case sid = "sid"
        case sName = "sName"
        case timestamp = "timestamp"
        case prime = "prime"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        acStatus = try values.decodeIfPresent(String.self, forKey: .acStatus)
        deviceInfo = try values.decodeIfPresent([DeviceInfo].self, forKey: .deviceInfo)
        devices = try values.decodeIfPresent([Device].self, forKey: .devices)
        entitlements = try values.decodeIfPresent([Entitlement].self, forKey: .entitlements)
        isDeviceManagement = try values.decodeIfPresent(Bool.self, forKey: .isDeviceManagement)
        isPremium = try values.decodeIfPresent(Bool.self, forKey: .isPremium)
        isPVR = try values.decodeIfPresent(Bool.self, forKey: .isPVR)
        profiles = try values.decodeIfPresent([Profiles].self, forKey: .profiles)
        rmn = try values.decodeIfPresent(String.self, forKey: .rmn)
        sid = try values.decodeIfPresent(String.self, forKey: .sid)
        sName = try values.decodeIfPresent(String.self, forKey: .sName)
        timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
        prime = try values.decodeIfPresent(PrimeData.self, forKey: .prime)
    }
    
}


struct PrimeData : Codable {
    
    let status : String?
    let type : String?
    let planId : String?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case type = "type"
        case planId = "planId"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        planId = try values.decodeIfPresent(String.self, forKey: .planId)
    }
    
}


struct Profiles : Codable {
    
    let appProfileLanguage : String?
    let id : String?
    let profileName : String?
    
    enum CodingKeys: String, CodingKey {
        case appProfileLanguage = "appProfileLanguage"
        case id = "id"
        case profileName = "profileName"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        appProfileLanguage = try values.decodeIfPresent(String.self, forKey: .appProfileLanguage)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        profileName = try values.decodeIfPresent(String.self, forKey: .profileName)
    }
    
}


struct Entitlement : Codable {
    
    let pkgId : String?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        case pkgId = "pkgId"
        case type = "type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pkgId = try values.decodeIfPresent(String.self, forKey: .pkgId)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
    
}


struct DeviceInfo : Codable {
    
    let baId : String?
    let bingeAccountStatus : String?
    let contentPlayBack : Bool?
    let contentPlayBackHybrid: Bool?
    let logoutHybrid: Bool?
    let deviceList : [String]?
    let deviceType : String?
    let logout : Bool?
    let atvCancelled: Bool?
    let serialNo : String?
    let status : String?
    let subscriptionStatus : String?
    
    enum CodingKeys: String, CodingKey {
        case baId = "baId"
        case bingeAccountStatus = "bingeAccountStatus"
        case contentPlayBack = "contentPlayBack"
        case contentPlayBackHybrid = "contentPlayBackHybrid"
        case logoutHybrid = "logoutHybrid"
        case deviceList = "deviceList"
        case deviceType = "deviceType"
        case logout = "logout"
        case serialNo = "serialNo"
        case status = "status"
        case atvCancelled = "atvCancelled"
        case subscriptionStatus = "subscriptionStatus"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        baId = try values.decodeIfPresent(String.self, forKey: .baId)
        bingeAccountStatus = try values.decodeIfPresent(String.self, forKey: .bingeAccountStatus)
        contentPlayBack = try values.decodeIfPresent(Bool.self, forKey: .contentPlayBack)
        contentPlayBackHybrid = try values.decodeIfPresent(Bool.self, forKey: .contentPlayBackHybrid)
        logoutHybrid = try values.decodeIfPresent(Bool.self, forKey: .logoutHybrid)
        deviceList = try values.decodeIfPresent([String].self, forKey: .deviceList)
        deviceType = try values.decodeIfPresent(String.self, forKey: .deviceType)
        logout = try values.decodeIfPresent(Bool.self, forKey: .logout)
        atvCancelled = try values.decodeIfPresent(Bool.self, forKey: .atvCancelled)
        serialNo = try values.decodeIfPresent(String.self, forKey: .serialNo)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        subscriptionStatus = try values.decodeIfPresent(String.self, forKey: .subscriptionStatus)
    }
}


struct Device : Codable {
    
    let deviceId : String?
    let devicePlatform : String?
    let deviceType : String?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        case deviceId = "deviceId"
        case devicePlatform = "devicePlatform"
        case deviceType = "deviceType"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        deviceId = try values.decodeIfPresent(String.self, forKey: .deviceId)
        devicePlatform = try values.decodeIfPresent(String.self, forKey: .devicePlatform)
        deviceType = try values.decodeIfPresent(String.self, forKey: .deviceType)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}

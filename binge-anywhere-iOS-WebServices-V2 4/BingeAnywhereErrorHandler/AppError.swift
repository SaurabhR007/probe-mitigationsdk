//
//  AppError.swift
//  BingeAnywhere
//
//  Created by Shivam on 18/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit

protocol ErrorHandler {
    func throwing(_ error: AppError) throws
}

extension ErrorHandler {
    func throwing(_ error: AppError) throws {
        throw error
    }
}

enum AppError {

    case network(type: Enums.NetworkError)
    case file(type: Enums.FileError)
    case validation(type: Enums.ValidationError)
    case custom(errorDescription: String?)

    class Enums { }
}

extension AppError: LocalizedError {
    var errorDescription: String? {

        switch self {
        case .network(let type):
            return type.localizedDescription
        case .file(let type):
            return type.localizedDescription
        case .validation(let type):
            return type.localizedDescription
        case .custom(let errorDescription):
            return errorDescription
        }
    }
}

// MARK: - Network Errors

extension AppError.Enums {
    enum NetworkError {
        case parsing
        case notFound
        case custom(errorCode: Int?, errorDescription: String?)
    }
}

extension AppError.Enums.NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .parsing:
            return "Parsing error"
        case .notFound:
            return "URL Not Found"
        case .custom(_, let errorDescription):
            return errorDescription
        }
    }

    var errorCode: Int? {
        switch self {
        case .parsing:
            return nil
        case .notFound:
            return 404
        case .custom(let errorCode, _):
            return errorCode
        }
    }
}

// MARK: - FIle Errors

extension AppError.Enums {
    enum FileError {
        case read(path: String)
        case write(path: String, value: Any)
        case custom(errorDescription: String?)
    }
}

extension AppError.Enums.FileError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .read(let path):
            return "Could not read file from \"\(path)\""
        case .write(let path, let value):
            return "Could not write value \"\(value)\" file from \"\(path)\""
        case .custom(let errorDescription):
            return errorDescription
        }
    }
}

// MARK: - Validation Errors

extension AppError.Enums {
    enum ValidationError {
        case mobileNumberInvalid(message: String, number: String)
        case emailInvalid(message: String, email: String)
        case otpInvalid(message: String, otp: String)
    }
}

extension AppError.Enums.ValidationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .mobileNumberInvalid(let messsage, let number):
            return NSLocalizedString("\(number) is invalid, \(messsage)", comment: messsage)
        case .emailInvalid(let messsage, let email):
            return NSLocalizedString("\(email) is invalid, please enter valid number", comment: messsage)
        case .otpInvalid(let messsage, let otp):
            return NSLocalizedString("\(otp) is invalid, please enter valid number", comment: messsage)
        }
    }
}

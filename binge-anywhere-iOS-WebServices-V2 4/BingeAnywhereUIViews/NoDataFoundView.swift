//
//  NoDataFoundView.swift
//  BingeAnywhere
//
//  Created by Shivam on 07/01/20.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
enum TypeOfView {
    case noInternet
    case retry
    case noData
}

enum TypeOfButton: Int {
    case retry
    case login
    case noButton
    case downloadables
}

class NoDataFoundView: UIView {

    @IBOutlet weak var retryButton: CustomButton!
    @IBOutlet weak var infoLabel: CustomLabel!
    @IBOutlet weak var placeHolderImage: UIImageView!
    @IBOutlet weak var retryButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!

    func setUI () {
        retryButton.backgroundColor = .BAdarkBlueBackground
		retryButton.text("Retry")
        retryButton.backgroundColor = .BABlueColor
        retryButton.tintColor = .BAwhite
        retryButton.makeCircular(roundBy: .height)
        retryButton.fontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        retryButton.isExclusiveTouch = true
        infoLabel.setupCustomFontAndColor(font: .skyTextFont(.medium, size: 16 * screenScaleFactorForWidth), color: .BAwhite)
        retryButtonHeightConstraint.constant = 40.0
        viewHeightConstraint.constant = 250.0
    }

    func setUI(forViewType typeOfView: TypeOfView, typeOfButton: TypeOfButton) {
        setUI()
        switch typeOfView {
        case .noInternet:
            placeHolderImage.image = UIImage.init(named: "noConnection")
            infoLabel.text = "You need to connect to the internet to access this content.Please check your internet connection."
        case .retry:
            placeHolderImage.image = UIImage.init(named: "noConnection")
            infoLabel.text = "Request timed out. Please retry."
        default:
            retryButton.isHidden = true
            retryButtonHeightConstraint.constant = 0.0
            viewHeightConstraint.constant = 200.0
        }
    }
}

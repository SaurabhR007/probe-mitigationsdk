//
//  HAController.h
//  HA
//
//  Created by Rishab Bokaria on 12/6/18.
//  Copyright © 2018 Hungama Digital Media Entertainment Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HAEvent.h"

NS_ASSUME_NONNULL_BEGIN

@interface HAController : NSObject

- (instancetype)init __attribute__((unavailable("Must use +getInstance instead.")));
+ (void)initializeWithAppKey:(NSString *)appkey forServiceId:(NSString *)serviceId affiliateId:(NSString *)affiliateId andAppDomainPrefix:(NSString *)appDomainPrefix withOptions:(NSDictionary *)launchOptions;
+ (void)initializeWithAppKey:(NSString *)appkey forServiceId:(NSString *)serviceId affiliateId:(NSString *)affiliateId andAppDomainPrefix:(NSString *)appDomainPrefix defaultEventsEnabled:(BOOL)enabled withOptions:(NSDictionary *)launchOptions;

+ (instancetype)getInstance;

- (void)onReceiveFCMToken:(NSString *)token;
- (void)onUpdateAffiliateId:(NSString *)affiliateId;
- (void)onUserLoggedInWithUserId:(NSString *)userId forUserName:(NSString *)userName andUserType:(NSString *)userType;
- (void)onUserLoggedOut;

- (void)applicationDidBecomeActive;
- (void)log:(nonnull HAEvent *)event;

@end

NS_ASSUME_NONNULL_END

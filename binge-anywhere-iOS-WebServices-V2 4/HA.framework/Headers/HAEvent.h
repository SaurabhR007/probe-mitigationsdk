//
//  HAEvent.h
//  HA
//
//  Created by Rishab Bokaria on 12/10/18.
//  Copyright © 2018 Hungama Digital Media Entertainment Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HAEvent : NSObject

- (instancetype)init __attribute__((unavailable("Must use -initWithName: instead.")));
- (instancetype)initWithName:(NSString *)name NS_DESIGNATED_INITIALIZER;

- (void)addBooleanProperty:(BOOL)value forKey:(NSString *)key;
- (void)addLongProperty:(long)value forKey:(NSString *)key;
- (void)addIntProperty:(int)value forKey:(NSString *)key;
- (void)addFloatProperty:(float)value forKey:(NSString *)key;
- (void)addDoubleProperty:(double)value forKey:(NSString *)key;
- (void)addStringProperty:(NSString *)value forKey:(NSString *)key;

- (NSString *)name;
- (NSString *)time;
- (NSString *)type;
- (NSString *)property;

@end

NS_ASSUME_NONNULL_END

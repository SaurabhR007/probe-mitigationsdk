// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name VideoPlayer
import AVFoundation
import AVKit
import Foundation
import Swift
import UIKit
import UserNotifications
@_exported import VideoPlayer
public enum ValidationResult {
  case success
  case failure(Swift.Error)
}
public typealias Parameters = [Swift.String : Any]
public typealias HTTPHeaders = [Swift.String : Swift.String]
public enum VideoDidEnd {
  case success
  case failure
  public static func == (a: VideoPlayer.VideoDidEnd, b: VideoPlayer.VideoDidEnd) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
extension TTNFairPlayer {
  @objc override dynamic public func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
}
@objc @_inheritsConvenienceInitializers open class TTNPlayer : UIKit.UIView {
  weak public var delegate: VideoPlayer.TTNPlayerDelegate?
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc required override dynamic public init(frame: CoreGraphics.CGRect)
  open func prepareToPlay(withAsset asset: AVFoundation.AVURLAsset)
  public func showBasicControls(_ bool: Swift.Bool)
  @objc deinit
}
public enum TTNFairPlayerNotification : Swift.String {
  case playerLoadStateDidChange
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
public enum TTNFairPlayerLoadState {
  case playable
  case playthroughOk
  case unknown
  public static func == (a: VideoPlayer.TTNFairPlayerLoadState, b: VideoPlayer.TTNFairPlayerLoadState) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public enum TTNFairPlayerItemStatus {
  case unknown
  case readyToPlay
  case failed
  public static func == (a: VideoPlayer.TTNFairPlayerItemStatus, b: VideoPlayer.TTNFairPlayerItemStatus) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public enum TTNFairPlayerPlaybackState {
  case stopped
  case playing
  case paused
  case seeking
  case pending
  public static func == (a: VideoPlayer.TTNFairPlayerPlaybackState, b: VideoPlayer.TTNFairPlayerPlaybackState) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public enum TTNFairPlayerDRMType {
  case kaltura
  case kidskaltura
  case fairplay
  public static func == (a: VideoPlayer.TTNFairPlayerDRMType, b: VideoPlayer.TTNFairPlayerDRMType) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
@objc public class TTNFairPlayer : ObjectiveC.NSObject {
  weak public var delegate: VideoPlayer.TTNPlayerDelegate?
  public var playbackState: VideoPlayer.TTNFairPlayerPlaybackState
  public var loadState: VideoPlayer.TTNFairPlayerLoadState
  public var layer: AVFoundation.AVPlayerLayer?
  public var player: AVFoundation.AVPlayer {
    get
    set
  }
  public init(withFrame frame: CoreGraphics.CGRect)
  public func prepareToPlay(withAsset asset: AVFoundation.AVURLAsset)
  public func removePlayerObservers()
  @objc deinit
  @objc override dynamic public init()
}
public struct HeartbeatConfigutaion {
  public var heartbeatURL: Swift.String
  public var timeInterval: Foundation.TimeInterval
  public var heartbeatParams: [Swift.String : Any]
  public var header: [Swift.String : Swift.String]
  public var enableHeartbeat: Swift.Bool {
    get
  }
  public init(heartbeatURL: Swift.String, timeInterval: Foundation.TimeInterval, heartbeatParams: [Swift.String : Any], header: [Swift.String : Swift.String])
}
public protocol PlayerHeartbeatDelegate {
  func heartbeatDidReceiveResponse(response: VideoPlayer.DataResponse<Any>)
}
extension PlayerHeartbeatDelegate {
  public func heartbeatDidReceiveResponse(response: VideoPlayer.DataResponse<Any>)
}
public protocol TTNPlayerDelegate : ObjectiveC.NSObject, VideoPlayer.PlayerHeartbeatDelegate {
}
extension TTNFairPlayer {
  public var duration: Foundation.TimeInterval {
    get
  }
  public var playableDuration: Foundation.TimeInterval {
    get
  }
  public var currentPlaybackTime: Foundation.TimeInterval {
    get
    set(newCurrentPlaybackTime)
  }
  public var isPlayerPlaying: Swift.Bool {
    get
  }
  public var allAudioTracks: [VideoPlayer.AudioDisplayTrack] {
    get
  }
  public var allSubTitles: Swift.Array<Any> {
    get
  }
}
public protocol PlayerHeartbeat {
  func heartbeatConfigDetails(heartbeatConfig: VideoPlayer.HeartbeatConfigutaion)
  func processHeartBeat(isRecursive: Swift.Bool)
  func processHeartBeat(parameters: [Swift.String : Any], isRecursive: Swift.Bool)
  func stopHeartbeat()
}
extension TTNFairPlayer : VideoPlayer.PlayerHeartbeat {
  public func heartbeatConfigDetails(heartbeatConfig: VideoPlayer.HeartbeatConfigutaion)
  public func processHeartBeat(isRecursive: Swift.Bool = true)
  public func processHeartBeat(parameters: [Swift.String : Any], isRecursive: Swift.Bool = true)
  public func stopHeartbeat()
}
public enum Result<Value> {
  case success(Value)
  case failure(Swift.Error)
  public var isSuccess: Swift.Bool {
    get
  }
  public var isFailure: Swift.Bool {
    get
  }
  public var value: Value? {
    get
  }
  public var error: Swift.Error? {
    get
  }
}
public struct DataResponse<Value> {
  public let result: VideoPlayer.Result<Value>
  public var value: Value? {
    get
  }
  public var error: Swift.Error? {
    get
  }
  public init(result: VideoPlayer.Result<Value>)
}
public protocol URLConvertible {
  func asURL() throws -> Foundation.URL
}
extension String : VideoPlayer.URLConvertible {
  public func asURL() throws -> Foundation.URL
}
public let NOTIF_LicenseAcqusitionStatus: Swift.String
public let NOTIF_PlayerDidFinish: Swift.String
public let NOTIF_NewAccessLogEntry: Swift.String
public let NOTIF_PlayerPlaybackStateDidChange: Swift.String
public let NOTIF_PlayerLoadStateDidChange: Swift.String
public let NOTIF_PlaybackStalled: Swift.String
public let NOTIF_BufferDisplayLayerFailedToDecode: Swift.String
public let NOTIF_PlayerItemStatusDidChange: Swift.String
public let NOTIF_PlayerDurationAvailable: Swift.String
public let NOTIF_ScreenRecordingEnabled: Swift.String
public let NOTIF_PlaybackRestricted: Swift.String
extension TTNFairPlayer {
  public func play()
  public func pause()
  public func stop()
  public func hideSubtitle()
  public func seek(to time: Foundation.TimeInterval)
  public func changeAudioTrackWith(index: Swift.Int)
  public func setVideoQuality(quality: VideoPlayer.VideoQuality)
  public var isMuted: Swift.Bool {
    get
    set
  }
  public var isExternalPlaybackEnabled: Swift.Bool {
    get
  }
  public func changeSubtitleTrackWith(index: Swift.Int)
}
public enum VideoQuality {
  case auto(value: Swift.Double = 4000000)
  case high(value: Swift.Double = 1500*1024)
  case medium(value: Swift.Double = 700*1024)
  case low(value: Swift.Double = 400*1024)
}
@objc @_inheritsConvenienceInitializers public class TTNPlayerConfiguration : ObjectiveC.NSObject {
  public static let sharedInstance: VideoPlayer.TTNPlayerConfiguration
  public func vendorInfo(data: [Swift.String : Any], vendorDataRequired: Swift.Bool)
  public func drmInfo(data: Swift.String, drmType: VideoPlayer.TTNFairPlayerDRMType)
  public func fpsCertificateUrl(_ fpsCertificateUrl: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
public enum TTNNetworkError : Swift.Error {
  public enum ParameterEncodingFailureReason {
    case missingURL
    case jsonEncodingFailed(error: Swift.Error)
    case propertyListEncodingFailed(error: Swift.Error)
  }
  public enum MultipartEncodingFailureReason {
    case bodyPartURLInvalid(url: Foundation.URL)
    case bodyPartFilenameInvalid(in: Foundation.URL)
    case bodyPartFileNotReachable(at: Foundation.URL)
    case bodyPartFileNotReachableWithError(atURL: Foundation.URL, error: Swift.Error)
    case bodyPartFileIsDirectory(at: Foundation.URL)
    case bodyPartFileSizeNotAvailable(at: Foundation.URL)
    case bodyPartFileSizeQueryFailedWithError(forURL: Foundation.URL, error: Swift.Error)
    case bodyPartInputStreamCreationFailed(for: Foundation.URL)
    case outputStreamCreationFailed(for: Foundation.URL)
    case outputStreamFileAlreadyExists(at: Foundation.URL)
    case outputStreamURLInvalid(url: Foundation.URL)
    case outputStreamWriteFailed(error: Swift.Error)
    case inputStreamReadFailed(error: Swift.Error)
  }
  public enum ResponseValidationFailureReason {
    case dataFileNil
    case dataFileReadFailed(at: Foundation.URL)
    case missingContentType(acceptableContentTypes: [Swift.String])
    case unacceptableContentType(acceptableContentTypes: [Swift.String], responseContentType: Swift.String)
    case unacceptableStatusCode(code: Swift.Int)
  }
  public enum ResponseSerializationFailureReason {
    case inputDataNil
    case inputDataNilOrZeroLength
    case inputFileNil
    case inputFileReadFailed(at: Foundation.URL)
    case stringSerializationFailed(encoding: Swift.String.Encoding)
    case jsonSerializationFailed(error: Swift.Error)
    case propertyListSerializationFailed(error: Swift.Error)
  }
  case invalidURL(url: VideoPlayer.URLConvertible)
  case parameterEncodingFailed(reason: VideoPlayer.TTNNetworkError.ParameterEncodingFailureReason)
  case multipartEncodingFailed(reason: VideoPlayer.TTNNetworkError.MultipartEncodingFailureReason)
  case responseValidationFailed(reason: VideoPlayer.TTNNetworkError.ResponseValidationFailureReason)
  case responseSerializationFailed(reason: VideoPlayer.TTNNetworkError.ResponseSerializationFailureReason)
}
public struct AudioDisplayTrack {
  public var languageCode: Swift.String
  public var title: Swift.String
  public var id: Swift.Int
}
@objc @_inheritsConvenienceInitializers public class TTNPlayerInfo : ObjectiveC.NSObject {
  public var audioDisplayTrack: [VideoPlayer.AudioDisplayTrack]
  @objc override dynamic public init()
  @objc deinit
}
extension VideoPlayer.VideoDidEnd : Swift.Equatable {}
extension VideoPlayer.VideoDidEnd : Swift.Hashable {}
extension VideoPlayer.TTNFairPlayerNotification : Swift.Equatable {}
extension VideoPlayer.TTNFairPlayerNotification : Swift.Hashable {}
extension VideoPlayer.TTNFairPlayerNotification : Swift.RawRepresentable {}
extension VideoPlayer.TTNFairPlayerLoadState : Swift.Equatable {}
extension VideoPlayer.TTNFairPlayerLoadState : Swift.Hashable {}
extension VideoPlayer.TTNFairPlayerItemStatus : Swift.Equatable {}
extension VideoPlayer.TTNFairPlayerItemStatus : Swift.Hashable {}
extension VideoPlayer.TTNFairPlayerPlaybackState : Swift.Equatable {}
extension VideoPlayer.TTNFairPlayerPlaybackState : Swift.Hashable {}
extension VideoPlayer.TTNFairPlayerDRMType : Swift.Equatable {}
extension VideoPlayer.TTNFairPlayerDRMType : Swift.Hashable {}

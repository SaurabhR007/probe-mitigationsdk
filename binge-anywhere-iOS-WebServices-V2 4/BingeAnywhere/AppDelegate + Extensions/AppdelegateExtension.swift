//
//  AppdelegateExtension.swift
//  BingeAnywhere
//
//  Created by Shivam on 20/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import Foundation
import UIKit
import Mixpanel
import HungamaPlayer

extension AppDelegate {
    
    func createHomeTabRootView() {
        BAKeychainManager().customerName = "Guest User"
        let homeTabViewController: TabBaseViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.homeTab.fileName, identifierVC: "TabBaseViewController", type: TabBaseViewController.self)
        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.homeMain.rawValue)
        self.window?.rootViewController = homeTabViewController
        kAppDelegate.isAppLaunchedThroughPush = false
        kAppDelegate.isAppLaunchedThroughDeepLink = false
        PubNubManager.shared.setUpPubNub()
        //callAnalyticsData()
//        MoengageManager.shared.updateUnqiueId()
//        MoengageManager.shared.setUserAttributes()
//
//        MixpanelManager.shared.saveMixPanelDistinctID()
//        MixpanelManager.shared.setPeopleProperty()
        
        HungamaPlayerManager.shared.releasePlayer()
        if BAKeychainManager().deepLinkScreenOpened {
            checkForDeeplinkData()
        } else {
            self.window?.makeKeyAndVisible()
        }
    }
    
    
    func callAnalyticsData() {
        MoengageManager.shared.userUniqueId()
        MixpanelManager.shared.saveMixPanelDistinctID()
        
        MixpanelManager.shared.registerPeopleProperties()
        MoengageManager.shared.setUserAttributes()
    }
    
    
    func checkForDeeplinkData() {
        if BAKeychainManager().deepLinkScreenOpened {
            BAKeychainManager().deepLinkScreenOpened = !BAKeychainManager().deepLinkScreenOpened
            UtilityFunction.shared.performTaskInMainQueue {
                let contentType = kUserDefaults.object(forKey: "contentType") as? String
                let contentId = kUserDefaults.object(forKey: "contentId") as? String
                kAppDelegate.parseDeepLinkData(contentType: contentType ?? "", contentId: contentId ?? "")
                kUserDefaults.removeObject(forKey: "contentType")
                kUserDefaults.removeObject(forKey: "contentId")
            }
        }
    }

    func createLoginRootView(isUserLoggedOut: Bool = false) {
        HungamaPlayerManager.shared.releasePlayer()
        if isUserLoggedOut {
            let loginViewController: LoginVC = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "LoginVC", type: LoginVC.self)
            let navController = UINavigationController.init(rootViewController: loginViewController)
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
        } else {
            let loginViewController: InitialLandingController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "ViewController", type: InitialLandingController.self)
            let navController = UINavigationController.init(rootViewController: loginViewController)
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
            kAppDelegate.isAppLaunchedThroughPush = false
            kAppDelegate.isAppLaunchedThroughDeepLink = false
            MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.loginEnter.rawValue, properties: [MixpanelConstants.ParamName.source.rawValue: MixpanelConstants.ParamValue.appLaunch.rawValue])
        }
    }
    
    func createTransactionDec() {
        BAKeychainManager().customerName = "Guest User"
        let homeTabViewController: BATransactionHistoryViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Account.fileName, identifierVC: "BATransactionHistoryViewController", type: BATransactionHistoryViewController.self)
        self.window?.rootViewController = homeTabViewController
        self.window?.makeKeyAndVisible()
    }
    
    func navigateToSpecificScreen(_ contentType: String, _ contentId: String) {
        let content_ID = Int(contentId) ?? 0
        let content_Type = updateContentType(contentType)
        PushScreenNavigationManager.sharedInstance.moveToPIScreen(content_Type, content_ID)
    }
    
    func updateContentType(_ contentType: String) -> String {
        var railContentType = contentType.uppercased()
        switch railContentType {
        case ContentType.series.rawValue:
            railContentType = railContentType.lowercased()
        case ContentType.brand.rawValue:
            railContentType = railContentType.lowercased()
        default:
            railContentType = "vod"
        }
        return railContentType
    }
    
    func parsePushData(_ userInfo: [AnyHashable: Any]) {
        if let app_extra_dict = userInfo["app_extra"] as? [String: Any], let pushDict = app_extra_dict["screenData"] as? [String: Any] {
            let jsonStr = pushDict["screenData"] as? String ?? ""
            //let screenName = pushDict["screenName"]
            let data = Data(jsonStr.utf8)
            do {
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    // try to read out a string array
                    if let screenName = json["screenName"] as? String {
                        if screenName == PushScreenName.detail_Screen.rawValue {
                            if let screenData = json["data"] as? [String: Any] {
                                navigateToSpecificScreen(screenData["contentType"] as? String  ?? "", screenData["id"] as? String ?? "0")
                            }
                        } else {
                            if let screenName = PushScreenName(rawValue: screenName) {
                                if let screenData = json["data"] as? [String: Any] {
                                    let sectionSource = (screenData["sectionSource"] as? String ?? "")//HOTFIX for opening CW and TA see all screen from push
                                    if sectionSource.length > 0 {
                                        if sectionSource == "PROVIDER" {
                                            PushScreenNavigationManager.sharedInstance.navigateToSpecificScreen(screenName: .appSeeAll_Screen, railId: Int(screenData["id"] as? String ?? "0"), railTitle: screenData["railTitle"] as? String ?? "")
                                        } else if sectionSource == "CONTINUE_WATCHING" {
                                            PushScreenNavigationManager.sharedInstance.navigateToTASeeAll(.continueWatch_Screen ,screenData)
                                        } else if sectionSource == "RECOMMENDATION" {
                                            PushScreenNavigationManager.sharedInstance.navigateToTASeeAll(.recommendation_Screen ,screenData)
                                        }
                                    } else {
                                        PushScreenNavigationManager.sharedInstance.navigateToSpecificScreen(screenName: screenName, railId: Int(screenData["id"] as? String ?? "0"), railTitle: screenData["railTitle"] as? String ?? "")
                                    }
                                } else {
                                    PushScreenNavigationManager.sharedInstance.navigateToSpecificScreen(screenName: screenName, railId: nil, railTitle: nil)
                                }
                            }
                        }
                    }
                    
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
            
        }
    }
}

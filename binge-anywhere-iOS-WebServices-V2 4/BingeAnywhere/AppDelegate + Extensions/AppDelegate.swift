//
//  AppDelegate.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import CoreData
import Mixpanel
import MoEngage
import Firebase
import FirebaseCore
import PubNub
import Kingfisher
import KingfisherWebP
import AppTrackingTransparency
import FacebookCore
import VideoPlayer


var noInternetAppear = false
var deviceLoggedOutPopUpAppear = false
var appEnteredInBackground = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MOMessagingDelegate, MoengageProtocol /*ReachabilityObserverDelegate*/ {
    
    var window: UIWindow?
    var pubnub: PubNub!
    var pubnubViewModal: BAPubnubHistoryViewModal?
    var isAppLaunchedThroughPush = false
    var isAppLaunchedThroughDeepLink = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //print("VideoPlayerVersionNumber",VideoPlayerVersionNumber)

        //print("Keychain device Id ------>", BAKeychainManager().deviceId)
        BAKeychainManager().isSonyLivLoggedIn = false
        BAKeychainManager().deepLinkScreenOpened = false
        BAKeychainManager().isScreenPushCodeCalled = false
        var retrievedId: String? = BAKeychainManager().deviceId//KeychainWrapper.standard.string(forKey: "UUID")
        if retrievedId == nil || (retrievedId?.isEmpty ?? false) {
            let saveSuccessful: Bool = KeychainWrapper.standard.set(UIDevice.current.identifierForVendor?.uuidString ?? "", forKey: "UUID")
            if saveSuccessful {
                retrievedId = KeychainWrapper.standard.string(forKey: "UUID")
                BAKeychainManager().deviceId = retrievedId ?? ""
            } else {
                retrievedId = nil
            }
        } else {
            BAUserDefaultManager().deviceId = BAKeychainManager().deviceId
        }
        
        if !BAUserDefaultManager().hasRunBefore {
            let isKeychainRemoved: Bool = kKeychainStandard.removeAllKeys()
            if isKeychainRemoved {
                BAUserDefaultManager().hasRunBefore = true
            }
        }
        
        if retrievedId == nil || (retrievedId?.isEmpty ?? false) {
            //BAUserDefaultManager().deviceId = BAKeychainManager().deviceId
        } else {
            BAKeychainManager().deviceId = BAUserDefaultManager().deviceId
            // DO Nothing
        }
        print("Key Chain Device Id ------>", BAKeychainManager().deviceId)
        /// Uncomment below line to slow animation on real device. Use only for debugging purpose.
        
        
        /*******************************************************/
        //              Moengage Setup for SDK 7.              //
        /*******************************************************/
        
        UNUserNotificationCenter.current().delegate = self
        MOMessaging.sharedInstance().messagingDelegate = self
        MoEngage.sharedInstance().disableBadgeReset = true
        
        //This is to enable logs of MoEngage SDK
        MoEngage.enableSDKLogs(false)
        //TODO: Add your MoEngage App ID
        let yourMoEAppID = "H96OKLTDJTGID5RVY1M8FIIK"
        //TODO: Add your App Group ID
        let appGroupID = "group.com.tatasky.binge.moengage"
        var sdkConfig = MOSDKConfig.init(appID: yourMoEAppID)
        sdkConfig.appGroupID = appGroupID
        
        DispatchQueue.main.async {
            #if DEBUG
            debugPrint("Moengage in Debug Mode")
            MoEngage.sharedInstance().initializeTest(with: sdkConfig, andLaunchOptions: launchOptions)
            #else
            debugPrint("Moengage in Release Mode")
            MoEngage.sharedInstance().initializeLive(with: sdkConfig, andLaunchOptions: launchOptions)
            #endif
        }
        
        //For registering for remote notification
        let categoriesForiOS10 = self.getCategoriesForiOS10()
        MoEngage.sharedInstance().registerForRemoteNotification(withCategories: categoriesForiOS10, withUserNotificationCenterDelegate: self)
        //For tracking if it's a new install or on update by user
        self.sendAppStatusToMoEngage()
        self.saveAppVersionToDefaults()
//        //print("6120456793",BAKeychainManager().mobileNumber)
//        ProbeInterface.registerForMitigationSession(rmn : "9916315079", enableTestLogs: true)
         
        
        
        /*******************************************************/
        //              Mixpanel Setup.                        //
        /*******************************************************/
        
        let yourMixpanelKey = UtilityFunction.shared.isUatBuild() ? kMixpanelUATKey : kMixpanelUATKey
        MixpanelManager.shared.registerMixPanelInstance(yourMixpanelKey)
        
        // Case - Fallback when user has killed the app & subscription needs to be updated. code was commented as discussion with Subhro and QE team on 10 June
        if BAKeychainManager().customerName == "Guest User" {
            callAnalyticsData()
        }

        MixpanelManager.shared.trackEventWith(eventName: MixpanelConstants.Event.appLaunch.rawValue)
        
        requestPermission()
        
        FirebaseApp.configure()
     //   Analytics.setAnalyticsCollectionEnabled(true)
        
        // Facebook Events Initialization
        AppEvents.activateApp()
        FacebookCore.Settings.enableLoggingBehavior(.appEvents)
        
        kAppDelegate.createLoginRootView()
        
        if let notification = launchOptions?[.remoteNotification] as? [AnyHashable: Any] {
            //            let data = NSKeyedArchiver.archivedData(withRootObject: notification)
            let randomFilename = UUID().uuidString
            let fullPath = getDocumentsDirectory().appendingPathComponent(randomFilename)
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: notification, requiringSecureCoding: false)
                kUserDefaults.set(data, forKey: "dict1")
                BAKeychainManager().isScreenPushCodeCalled = true
                isAppLaunchedThroughPush = true
                try data.write(to: fullPath)
            } catch {
                print("Couldn't write file")
            }
        }
        
        if let _ = launchOptions?[.url] as? URL {
        } else {
            kUserDefaults.set(userActivity, forKey: "userActivity")
            //BAKeychainManager().deepLinkScreenOpened = true
            isAppLaunchedThroughDeepLink = true
        }
        
        // To Support Web P Image Support in our app
        KingfisherManager.shared.defaultOptions += [
            .processor(WebPProcessor.default),
            .cacheSerializer(WebPSerializer.default)
        ]
        
     //   print(getEntry(gate:6, people:[1,2,3,4,5,6]))
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        getPublicIPAddress(requestURL:URL(string:PublicIPAPIURLs.IPv6.ipify.rawValue)! ) { (ip,error) in
            if let error = error {
                   print(error.localizedDescription)
               } else if let ip = ip {
                   print("Get IP \(ip)")
               }
        }
        return true
    }
    
    func getEntry(gate:Int,people:[Int]) -> Int{
         var standingGate = 1
         var counter = 0
         var peopleArray = people
      
       while true {
           print("Inner")
               if peopleArray[standingGate]  >= 0{
               standingGate = standingGate + 1
               if standingGate > gate{
                   standingGate = 1
               }
           }else{
               standingGate = standingGate + 1
               return standingGate
           }
             while true{
                  print("Outer")
                let total =  peopleArray[counter] - 1
                if total > 0{
                   peopleArray[counter] = total
                }else{
                    peopleArray[counter] = 0
                }
                 if counter  < peopleArray.count - 1{
                    counter = counter + 1
                 }else{
                     counter = 0
                     break
                 }
           }
         }
    }

 
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any?] = [:]) -> Bool {
        
        ApplicationDelegate.shared.application(app,open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        return true
    }
    
    //Example to define categories
    //This method gives categories for iOS version 10.0 and above
    @available(iOS 10.0, *)
    func getCategoriesForiOS10() -> Set<UNNotificationCategory>{
        
        let acceptAction = UNNotificationAction.init(identifier: "ACCEPT_IDENTIFIER", title: "Accept", options: .authenticationRequired)
        let declineAction = UNNotificationAction.init(identifier: "DECLINE_IDENTIFIER", title: "Decline", options: .destructive)
        let maybeAction = UNNotificationAction.init(identifier: "MAYBE_IDENTIFIER", title: "May Be", options: .foreground)
        
        let inviteCategory = UNNotificationCategory.init(identifier: "INVITE_CATEGORY", actions: [acceptAction,declineAction,maybeAction], intentIdentifiers: [], options: .customDismissAction)
        let categoriesSet = Set.init([inviteCategory])
        
        return categoriesSet;
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        debugPrint("--------------1--------------")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        appEnteredInBackground = true
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        debugPrint("--------------2--------------")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PausePlayer"), object: nil)
        appEnteredInBackground = true
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        debugPrint("--------------3--------------")
        if UIScreen.screens.count > 1 {
            print("Screen mirroring statrted")
        } else {
            print("Screen mirroring stopped")
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
        NotificationCenter.default.post(Notification(name: Notification.Name("didReceiveNotification")))
        NotificationCenter.default.post(Notification(name: Notification.Name("updateNotificationCount")))
        appEnteredInBackground = false
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PlayerPlayerOnEnteringForeground"), object: nil)
        debugPrint("--------------4--------------")
        pubnubViewModal = BAPubnubHistoryViewModal(repo: BAPubnubHistoryRepo())
        getPubnubHistory()
        if UIScreen.screens.count > 1 {
            print("Screen mirroring statrted")
            DispatchQueue.main.async {
                kAppDelegate.window?.makeToastOnlyForMessage("Sorry, casting is not allowed.") { (boolVal) in
                    self.window?.rootViewController?.view.endEditing(true)
                    //self.hideActivityIndicator()
                    noInternetAppear = false
                    //completion?()
                }
            }
        } else {
            print("Screen mirroring stopped")
        }
        
        appEnteredInBackground = false
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        debugPrint("--------------5--------------")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard let url = userActivity.webpageURL else {
            // createLoginRootView()
            return false
        }
        let parsedValue = url.pathComponents.indices.contains(2)
        if !parsedValue {
            return false
        }
        
        if BAKeychainManager().customerName == "Guest User" && !isAppLaunchedThroughDeepLink {
            parseDeepLinkData(contentType: url.pathComponents[2], contentId: url.lastPathComponent)
        } else {
            kUserDefaults.set(url.pathComponents[2], forKey: "contentType")
            kUserDefaults.set(url.lastPathComponent, forKey: "contentId")
            BAKeychainManager().deepLinkScreenOpened = true
            createLoginRootView()
        }
        return true
    }
    
    func getPubnubHistory() {
        if BAReachAbility.isConnectedToNetwork() {
            if pubnubViewModal != nil {
                pubnubViewModal?.getPubnubHistory(completion: {(flagValue, message, isApiError) in
                    if flagValue {
                        // Do Nothing
                    } else if isApiError {
                        //self.apiError(message, title: kSomethingWentWrong)
                    } else {
                        //self.apiError(message, title: "")
                    }
                })
            }
        } else {
            //
        }
    }
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        let parsedValue = userActivity.webpageURL?.pathComponents.indices.contains(2) ?? false
        if parsedValue {
            BAKeychainManager().deepLinkScreenOpened = true
            kUserDefaults.set(userActivity.webpageURL?.pathComponents[2], forKey: "contentType")
            kUserDefaults.set(userActivity.webpageURL?.lastPathComponent, forKey: "contentId")
            if BAKeychainManager().customerName == "Guest User" && !isAppLaunchedThroughDeepLink {
                parseDeepLinkData(contentType: userActivity.webpageURL?.pathComponents[2] ?? "", contentId: userActivity.webpageURL?.lastPathComponent ?? "")
            } else {
                createLoginRootView()
            }
        }
    }
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let parsedValue = userActivity?.webpageURL?.pathComponents.indices.contains(2) ?? false
        if parsedValue {
            BAKeychainManager().deepLinkScreenOpened = true
            kUserDefaults.set(userActivity?.webpageURL?.pathComponents[2], forKey: "contentType")
            kUserDefaults.set(userActivity?.webpageURL?.lastPathComponent, forKey: "contentId")
            if BAKeychainManager().customerName == "Guest User" && !isAppLaunchedThroughDeepLink {
                parseDeepLinkData(contentType: userActivity?.webpageURL?.pathComponents[2] ?? "", contentId: userActivity?.webpageURL?.lastPathComponent ?? "")
            } else {
                createLoginRootView()
            }
        }
    }
    
    //MARK:- Push Notification delegate Method
    //Remote notification Registration callback methods
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //Call only if MoEngageAppDelegateProxyEnabled is NO
        MoEngage.sharedInstance().setPushToken(deviceToken)
        
        
        BAKeychainManager().apnsDeviceToken = deviceToken.hexString
        print("APNs Device Token -> \(deviceToken.hexString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //Call only if MoEngageAppDelegateProxyEnabled is NO
        MoEngage.sharedInstance().didFailToRegisterForPush()
    }
    
    
    // MARK: - UserNotifications Framework callback method
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //Call only if MoEngageAppDelegateProxyEnabled is NO
        MoEngage.sharedInstance().userNotificationCenter(center, didReceive: response)
        
        //Custom Handling of notification if Any
        let pushDictionary = response.notification.request.content.userInfo
        if BAKeychainManager().customerName == "Guest User" && !isAppLaunchedThroughPush {
            parsePushData(pushDictionary)
        } else {
            createLoginRootView()
        }
        completionHandler()
    }
    
    func parseDeepLinkData(contentType: String, contentId: String) {
        navigateToSpecificScreen(contentType, contentId)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //This is to only to display Alert and enable notification sound
        completionHandler([.sound, .alert])
    }
    
    
    // MARK: For Below iOS 10.0
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if (Double(UIDevice.current.systemVersion)! < 10.0 ){
            //            NotificationCenter.default.post(Notification(name: Notification.Name("didReceiveNotification")))
            MoEngage.sharedInstance().didReceieveNotificationinApplication(application, withInfo: userInfo)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //        NotificationCenter.default.post(Notification(name: Notification.Name("didReceiveNotification")))
        MoEngage.sharedInstance().didReceieveNotificationinApplication(application, withInfo: userInfo)
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        if let identifier = identifier {
            MoEngage.sharedInstance().handleAction(withIdentifier: identifier, forRemoteNotification: userInfo)
        }
    }
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BingeAnywhere")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - Send app status to MoEngage
    func sendAppStatusToMoEngage () {
        if UserDefaults.standard.object(forKey: "app version") == nil {
            MoEngage.sharedInstance().appStatus(INSTALL)
            return
        }
        if (self.getAppVersion() as AnyObject).floatValue > (UserDefaults.standard.object(forKey: "app version") as AnyObject).floatValue {
            MoEngage.sharedInstance().appStatus(UPDATE)
        }
    }
    
    func saveAppVersionToDefaults () {
        UserDefaults.standard.set(self.getAppVersion(), forKey: "app version")
    }
    
    func getAppVersion () -> AnyObject {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")! as AnyObject
    }
    
    func notificationClicked(withScreenName screenName: String?, andKVPairs kvPairs: [AnyHashable : Any]?) {
        if let screenName = screenName {
            print("Navigate to Screen:\(screenName)")
        }
        if let actionKVPairs = kvPairs {
            print("Selected Action KVPair:\(actionKVPairs)")
        }
    }
    
    func notificationClicked(withScreenName screenName: String?, kvPairs: [AnyHashable : Any]?, andPushPayload userInfo: [AnyHashable : Any]) {
        print("Push Payload: \(userInfo)")
        if let screenName = screenName {
            print("Navigate to Screen:\(screenName)")
        }
        if let actionKVPairs = kvPairs {
            print("Selected Action KVPair:\(actionKVPairs)")
        }
    }
    
    static func requestIDFAPermission() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    print("ATTrackingManager - Authorized")
                case .denied:
                    print("Denied")
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    print("Not Determined")
                case .restricted:
                    print("Restricted")
                @unknown default:
                    print("Unknown")
                }
            }
        }
    }
    
}

//
//  AppDelgate+Rotation.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 15/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

extension AppDelegate {

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {

        // Make sure the root controller has been set
        // (won't initially be set when the app is launched)
        
        if let controller = UIApplication.shared.topMostViewController() {
            if BAKeychainManager().isOnEditProfileScreen {
                return UIInterfaceOrientationMask.portrait
            } else if controller is LandscapeSupportable {
                return UIInterfaceOrientationMask.landscape
            } else if controller is BAVODDetailViewController || controller is BAFullScreenPlayerViewController {
                guard let controller = controller as? OrientationHandlable else {
                    return UIInterfaceOrientationMask.all
                }
                return controller.blockRotation ? .portrait : .all
            } else if controller.isKind(of: SFSafariViewController.self) {
                return UIInterfaceOrientationMask.all
            } else if !BAKeychainManager().isSonyLivLoggedIn && (controller is UIAlertController) {
                return UIInterfaceOrientationMask.portrait
            }
            else if !(controller is BaseViewController) && !(controller is BAFullScreenPlayerViewController) {
                print("this is not inherrited from base view controller \(controller)")
                return UIInterfaceOrientationMask.landscapeRight
            }
            // Else only allow the window to support portrait orientation
            // Else only allow the window to support portrait orientation
            else {
                return UIInterfaceOrientationMask.portrait
            }
        }

        // If the root view controller hasn't been set yet, just
        // return anything
        return UIInterfaceOrientationMask.portrait
    }
}

protocol LandscapeSupportable where Self: UIViewController {} //Empty Protocol for Type Checking to avoid modifying appdelegate again and again

protocol OrientationHandlable {
    var blockRotation: Bool { get }
}

protocol PortaitSupportable where Self: UIViewController {}

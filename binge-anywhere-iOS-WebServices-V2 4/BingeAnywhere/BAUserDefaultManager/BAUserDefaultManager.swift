//
//  BAUserDefaultManager.swift
//  BingeAnywhere
//
//  Created by Harsh Singh on 07/01/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation

class BAUserDefaultManager {
    
    var hasRunBefore: Bool {
        get {
            return kUserDefaults.bool(forKey: UserDefault.UserInfo.hasRunBefore.rawValue)
        }
        set {
            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.hasRunBefore.rawValue)
        }
    }
    
//    var hotStarpopupCount: String {
//        get {
//            return kUserDefaults.String(forKey: UserDefault.UserInfo.hotstarpopupCount.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.String(newValue, forKey: UserDefault.UserInfo.hotstarpopupCount.rawValue) as? String ?? ""
//        }
//    }

//    var customerName: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.customerName.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.customerName.rawValue)
//        }
//    }

//    var deviceId: String {
//        get {
//            return UserDefaults.standard.value(forKey: UserDefault.UserInfo.deviceId.rawValue) as? String ?? ""
//        }
//        set {
//            UserDefaults.standard.set(newValue, forKey: UserDefault.UserInfo.deviceId.rawValue)
//        }
//    }
    
//    var loginType: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.loginType.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.loginType.rawValue)
//        }
//    }

    //    var isAutoPlayOn: Bool {
    //        get {
    //            return kUserDefaults.bool(forKey: UserDefault.UserInfo.autoPlay.rawValue)
    //        }
    //        set {
    //            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.autoPlay.rawValue)
    //        }
    //    }

//    var isScreenPushCodeCalled: Bool {
//        get {
//            return kUserDefaults.bool(forKey: UserDefault.UserInfo.pushScreenOpened.rawValue)
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.pushScreenOpened.rawValue)
//        }
//    }

//    var isMicEnabled: Bool {
//        get {
//            return kUserDefaults.bool(forKey: UserDefault.UserInfo.isMicEnabled.rawValue)
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.isMicEnabled.rawValue)
//        }
//    }
    
//    var isFSPopUpShown: Bool {
//        get {
//            return kUserDefaults.bool(forKey: UserDefault.UserInfo.isFSPopUpShown.rawValue)
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.isFSPopUpShown.rawValue)
//        }
//    }

//    var localSearchArray: [String] {
//        get {
//            return kUserDefaults.array(forKey: UserDefault.SearchArrayResult.searchResultArray.rawValue) as? [String] ?? [String]()
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.SearchArrayResult.searchResultArray.rawValue)
//        }
//    }

//    var baId: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.baId.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.baId.rawValue)
//        }
//    }
//
//    var sId: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.subscriberId.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.subscriberId.rawValue)
//        }
//    }
//
//    var dsn: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.dsn.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.dsn.rawValue)
//        }
//    }
//
//	var targetBaId: String {
//		get {
//			return kUserDefaults.value(forKey: UserDefault.UserInfo.targetBaId.rawValue) as? String ?? ""
//		}
//		set {
//			kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.targetBaId.rawValue)
//		}
//	}
//
//	var userAccessToken: String {
//		get {
//			return kUserDefaults.value(forKey: UserDefault.UserInfo.userAccessToken.rawValue) as? String ?? ""
//		} set {
//			kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.userAccessToken.rawValue)
//		}
//	}
//
//	var deviceAccessToken: String {
//		get {
//			return kUserDefaults.value(forKey: UserDefault.UserInfo.deviceAccessToken.rawValue) as? String ?? ""
//		} set {
//			kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.deviceAccessToken.rawValue)
//		}
//	}
//
//
//    var configTAAccessToken: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.configTAAccessToken.rawValue) as? String ?? ""
//        } set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.configTAAccessToken.rawValue)
//        }
//    }
//
//    var customerIndex: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.selectedIndex.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.selectedIndex.rawValue)
//        }
//    }
//
//    var customerID: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.selectedID.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.selectedID.rawValue)
//        }
//    }
//
//    var isVolume: Bool {
//        get {
//            return kUserDefaults.bool(forKey: UserDefault.UserInfo.isVolume.rawValue) as? Bool ?? false
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.isVolume.rawValue)
//        }
//    }
//
//    var videoQuality: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.videoQuality.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.videoQuality.rawValue)
//        }
//    }
//
//    var profileId: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.profileId.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.profileId.rawValue)
//        }
//    }
//
//    var aliasName: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.aliasName.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.aliasName.rawValue)
//        }
//    }
//
//    var fullName: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.fullname.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.fullname.rawValue)
//        }
//    }
//
//	var baIdCount: Int {
//		get {
//			return kUserDefaults.value(forKey: UserDefault.UserInfo.baIdCount.rawValue) as? Int ?? 1
//		}
//		set {
//			kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.baIdCount.rawValue)
//		}
//	}
//
//    var mobileNumber: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.mobileNumber.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.mobileNumber.rawValue)
//        }
//    }
	
//	var acccountDetail: AccountDetailListResponseData? {
//		get {
//			if let loginResponse = kUserDefaults.retrieve(object: AccountDetailListResponseData.self, fromKey: UserDefault.UserInfo.accountDetailServerResponse.rawValue) {
//				return loginResponse
//			}
//			return nil
//		}
//		set(loginResponse) {
//			kUserDefaults.save(customObject: loginResponse, inKey: UserDefault.UserInfo.accountDetailServerResponse.rawValue)
//		}
//	}

//    var profileImageRelativePath: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.profileImageRelativePath.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.profileImageRelativePath.rawValue)
//        }
//    }
//
//    var pageNameOnHome: String {
//        get {
//            return kUserDefaults.value(forKey: UserDefault.UserInfo.pageNameOnHome.rawValue) as? String ?? ""
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.pageNameOnHome.rawValue)
//        }
//    }
//
//    var contentPlayBack: Bool {
//        get {
//            return kUserDefaults.bool(forKey: UserDefault.UserInfo.contentPlayBack.rawValue)
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.contentPlayBack.rawValue)
//        }
//    }
//
//    var deviceLoggedOut: Bool {
//        get {
//            return kUserDefaults.bool(forKey: UserDefault.UserInfo.deviceLoggedOut.rawValue)
//        }
//        set {
//            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.deviceLoggedOut.rawValue)
//        }
//    }
//
    var apnsDeviceToken: String {
        get {
            return kUserDefaults.value(forKey: UserDefault.UserInfo.apnsDeviceToken.rawValue) as? String ?? ""
        }
        set {
            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.apnsDeviceToken.rawValue)
        }
    }
    
    var deviceId: String {
       get {
            return kUserDefaults.value(forKey: UserDefault.UserInfo.deviceId.rawValue) as? String ?? ""
        }
        set {
            kUserDefaults.set(newValue, forKey: UserDefault.UserInfo.deviceId.rawValue)
        }
    }

    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return kUserDefaults.object(forKey: key) != nil
    }
}

extension UserDefaults {

    @discardableResult
    func getSetReferralLink(with value: String? = nil, shouldRemove: Bool = false) -> String? {

        let key = "ReferralLink"

        if shouldRemove {
            kUserDefaults.removeObject(forKey: key)
        } else if let value = value {
            kUserDefaults.set(value, forKey: key)
        }

        return kUserDefaults.object(forKey: key) as? String

    }

    func save<T: Encodable>(customObject object: T, inKey key: String) {

        let encoder = JSONEncoder()

        if let encoded = try? encoder.encode(object) {
            self.set(encoded, forKey: key)
        }

    }

    func retrieve<T: Decodable>(object type: T.Type, fromKey key: String) -> T? {

        if let data = self.data(forKey: key) {

            let decoder = JSONDecoder()

            if let object = try? decoder.decode(type, from: data) {
                return object
            } else {
                print("Couldnt decode object")
                return nil
            }

        } else {
            print("Couldnt find key")
            return nil
        }

    }

    func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = self.value(forKey: key) as? Data else { return nil }
        return try? decoder.decode(type.self, from: data)
    }

    func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
        let data = try? encoder.encode(object)
        self.set(data, forKey: key)
    }

}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

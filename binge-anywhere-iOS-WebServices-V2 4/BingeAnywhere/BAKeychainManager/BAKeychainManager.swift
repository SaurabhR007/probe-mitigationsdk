//
//  BAKeychainManager.swift
//  BingeAnywhere
//
//  Created by Rohan Kanoo on 26/01/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import Foundation

class BAKeychainManager {
    
    var customerName: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.customerName.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.customerName.rawValue)
        }
    }
    
    var mobileNumberForSuperProperty: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.mobileNumberForSuperProperty.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.mobileNumberForSuperProperty.rawValue)
        }
    }
    
    var deviceId: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.deviceId.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.deviceId.rawValue)
            BAUserDefaultManager().deviceId = newValue
        }
    }
    
    var loginType: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.loginType.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.loginType.rawValue)
        }
    }
    
    var isAutoPlayOn: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.autoPlay.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.autoPlay.rawValue)
        }
    }
    
    var isHungamaIdSet: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isHungamaIdSet.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isHungamaIdSet.rawValue)
        }
    }
    
    var isErosNowLoggedIn: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isErosNowLoggedIn.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isErosNowLoggedIn.rawValue)
        }
    }
    
    var isSonyLivLoggedIn: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isSonyLivLoggedIn.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isSonyLivLoggedIn.rawValue)
        }
    }
    
    var isOnEditProfileScreen: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isOnEditProfileScreen.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isOnEditProfileScreen.rawValue)
        }
    }
    
    var sonyLivShortToken: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.sonyLivShortToken.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.sonyLivShortToken.rawValue)
        }
    }
    
    var isScreenPushCodeCalled: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.pushScreenOpened.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.pushScreenOpened.rawValue)
        }
    }
    
    var deepLinkScreenOpened: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.deepLinkScreenOpened.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.deepLinkScreenOpened.rawValue)
        }
    }
    
    var isMicEnabled: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isMicEnabled.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isMicEnabled.rawValue)
        }
    }
    
    var isFSPopUpShown: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isFSPopUpShown.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isFSPopUpShown.rawValue)
        }
    }
    
    var localSearchArray: [String] {
        get {
            return kKeychainStandard.object(forKey: UserDefault.SearchArrayResult.searchResultArray.rawValue) as? [String] ?? [String]()
        }
        set {
            let data = NSKeyedArchiver.archivedData(withRootObject: newValue)
            kKeychainStandard.set(data, forKey: UserDefault.SearchArrayResult.searchResultArray.rawValue)
        }
    }
    
    var baId: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.baId.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.baId.rawValue)
        }
    }

    var email: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.email.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.email.rawValue)
        }
    }
    
    var sId: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.subscriberId.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.subscriberId.rawValue)
        }
    }
    
    var dsn: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.dsn.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.dsn.rawValue)
        }
    }
    
    var targetBaId: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.targetBaId.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.targetBaId.rawValue)
        }
    }
    
    var userAccessToken: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.userAccessToken.rawValue) ?? ""
        } set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.userAccessToken.rawValue)
        }
    }
    
    var deviceAccessToken: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.deviceAccessToken.rawValue) ?? ""
        } set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.deviceAccessToken.rawValue)
        }
    }


    var configTAAccessToken: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.configTAAccessToken.rawValue) ?? ""
        } set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.configTAAccessToken.rawValue)
        }
    }

    var customerIndex: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.selectedIndex.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.selectedIndex.rawValue)
        }
    }

    var customerID: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.selectedID.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.selectedID.rawValue)
        }
    }

    var isVolume: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.isVolume.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.isVolume.rawValue)
        }
    }

    var videoQuality: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.videoQuality.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.videoQuality.rawValue)
        }
    }

    var profileId: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.profileId.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.profileId.rawValue)
        }
    }

    var aliasName: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.aliasName.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.aliasName.rawValue)
        }
    }

    var fullName: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.fullname.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.fullname.rawValue)
        }
    }
    
    var baIdCount: Int {
        get {
            return kKeychainStandard.integer(forKey: UserDefault.UserInfo.baIdCount.rawValue) ?? 1
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.baIdCount.rawValue)
        }
    }
    
    
    var hotStarAlertCount: Int {
        get {
            return kKeychainStandard.integer(forKey: UserDefault.UserInfo.hotStarAlertCount.rawValue) ?? 0
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.hotStarAlertCount.rawValue)
        }
    }
    
    var primeAppAlertCount: Int {
        get {
            return kKeychainStandard.integer(forKey: UserDefault.UserInfo.primeAppAlertCount.rawValue) ?? 0
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.primeAppAlertCount.rawValue)
        }
    }

    var mobileNumber: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.mobileNumber.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.mobileNumber.rawValue)
        }
    }
    
    var acccountDetail: AccountDetailListResponseData? {
        get {
            if let loginResponse = kKeychainStandard.retrieve(object: AccountDetailListResponseData.self, fromKey: UserDefault.UserInfo.accountDetailServerResponse.rawValue) {
                return loginResponse
            }
            return nil
        }
        set(loginResponse) {
            kKeychainStandard.save(customObject: loginResponse, inKey: UserDefault.UserInfo.accountDetailServerResponse.rawValue)
        }
    }

    var profileImageRelativePath: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.profileImageRelativePath.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.profileImageRelativePath.rawValue)
        }
    }
    
    var pageNameOnHome: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.pageNameOnHome.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.pageNameOnHome.rawValue)
        }
    }

    var contentPlayBack: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.contentPlayBack.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.contentPlayBack.rawValue)
        }
    }
    
    var atvCancelled: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.atvCancelled.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.atvCancelled.rawValue)
        }
    }
    
    var deviceLoggedOut: Bool {
        get {
            return kKeychainStandard.bool(forKey: UserDefault.UserInfo.deviceLoggedOut.rawValue) ?? false
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.deviceLoggedOut.rawValue)
        }
    }
    
    var apnsDeviceToken: String {
        get {
            return kKeychainStandard.string(forKey: UserDefault.UserInfo.apnsDeviceToken.rawValue) ?? ""
        }
        set {
            kKeychainStandard.set(newValue, forKey: UserDefault.UserInfo.apnsDeviceToken.rawValue)
            BAUserDefaultManager().apnsDeviceToken = newValue
        }
    }
}

extension KeychainWrapper {
    func save<T: Encodable>(customObject object: T, inKey key: String) {

        let encoder = JSONEncoder()

        if let encoded = try? encoder.encode(object) {
            self.set(encoded, forKey: key)
        }

    }

    func retrieve<T: Decodable>(object type: T.Type, fromKey key: String) -> T? {

        if let data = self.data(forKey: key) {

            let decoder = JSONDecoder()

            if let object = try? decoder.decode(type, from: data) {
                return object
            } else {
                print("Couldnt decode object")
                return nil
            }

        } else {
            print("Couldnt find key")
            return nil
        }
    }
}

//
//  ViewController.swift
//  BingeAnywhere
//
//  Created by Shivam on 05/11/19.
//  Copyright © 2019 ttn. All rights reserved.
//

import UIKit
import Foundation
import ARSLineProgress
import AVKit

func performACASecurityValidation(userContext: UnsafeMutableRawPointer?) {
    guard userContext != nil else { return }
    debugPrint("Inside the block of ACA Security Validation")
    let initialLandingController = Unmanaged<InitialLandingController>.fromOpaque(userContext!).takeRetainedValue()
    initialLandingController.videoLaunchCheck()
}

class InitialLandingController: BaseViewController {

    // MARK: - Variables
    var requestToken: ServiceCancellable?
    var player: AVPlayer?
    let repo = ConfigData()
    var isVideoFinished: Bool = false
    var isConfigFetched: Bool = false
    var isLoggedOut: Bool = false

    // MARK: - Outlets
    @IBOutlet weak var welcomeTitleLabel: CustomLabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var signUpButton: CustomButton!
    
    let aSession: AVAudioSession = AVAudioSession()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ///Necessary to inititalize AVAudioSession to get notified about any interruptions.
        try? aSession.setCategory(.playback, options: [.allowBluetooth, .allowBluetoothA2DP])
        try? aSession.setMode(.default)
        try? aSession.setActive(true, options: [])
        
        NotificationCenter.default.addObserver(self, selector: #selector(configureSplashPlayback), name: NSNotification.Name(rawValue: "PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(configureConfig), name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleInterruption(notification:)), name: AVAudioSession.interruptionNotification, object: nil)
        ActiveCloakAppManager.sharedInstance.setup()
        //videoLaunchCheck()
        configureUI()
        fetchConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        super.setNeedsStatusBarAppearanceUpdate()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        super.setNeedsStatusBarAppearanceUpdate()
        checkForACA()
    }
    
    func checkForACA() {
//        var status: ITAC_Status = ITAC_Status(isJailbroken: 0, isIVCheckFailed: 0, isHookDetected: 0)
//        let userPtr = Unmanaged<InitialLandingController>.passRetained(self).toOpaque()
//     //   ITAC_Agent_SetCallback(ActiveCloakAppManager.iacHandle, performACASecurityValidation, userPtr);
//      //  let result: uint = ITAC_Agent_Check(ActiveCloakAppManager.iacHandle, &status);
//        if result != 0 {
//            hideActivityIndicator()
//            UtilityFunction.shared.showAlertOnWindowACASecurityCheck(message: AppStringConstant.acaCheckFailed.rawValue)
//        } else {
//            let (success, message) = ActiveCloakAppManager.sharedInstance.statusValidation(status: status)
//            if success {
////                showHomeScreen.securityCheckPassed = true
//            }else {
//                hideActivityIndicator()
//                UtilityFunction.shared.showAlertOnWindowACASecurityCheck(message: message)
//            }
//        }
    }
    
    func videoLaunchCheck() {
        if isLoggedOut {
            moveToLogin()
        } else {
            if BAReachAbility.isConnectedToNetwork() {
                initialiseVideo(withAudio: true)
            } else {
                //noInternet()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("View did disappear called")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "PlayerPlayerOnEnteringForeground"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ConfigureView"), object: nil)
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.interruptionNotification, object: nil)
    }

    // MARK: - Fuctions
    func configureUI() {
        super.configureNavigationBar(with: .clearBackground, nil, nil, nil)
        self.view.backgroundColor = .white
    }
    
    func initialiseVideo(withAudio: Bool = false) {
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        guard let path = Bundle.main.path(forResource: "LaunchAnimation", ofType:"mp4") else { return }
        player = AVPlayer(url: NSURL(fileURLWithPath: path) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspect
        playerLayer.frame = logoImageView.frame
        view.layer.addSublayer(playerLayer)
        player?.isMuted = !withAudio
        player?.play()
        
    }
    
    @objc func playerDidFinishPlaying() {
        isVideoFinished = true
        if BAReachAbility.isConnectedToNetwork() {
            initialiseRootView()
        } else {
            noInternetForSplash() {
                self.fetchConfig()
            }
        }
    }
    
    @objc func configureConfig() {
        fetchConfig()
    }
    
    @objc func configureSplashPlayback() {
        print("Coming from backgroung")
        player?.play()
        //fetchConfig()
    }
    
    @objc func handleInterruption(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let typeInt = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
        let type = AVAudioSession.InterruptionType(rawValue: typeInt) else {
                return
        }

        switch type {
        case .began:
            // Continue playing your player
            player?.play()

        case .ended:
            if let optionInt = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt {
                let options = AVAudioSession.InterruptionOptions(rawValue: optionInt)
                if options.contains(.shouldResume) {
                    // Resume your player
                    player?.play()
                }
            }
        @unknown default:
            break
        }
    }
    
    private func checkVideoPlayback() {
        isConfigFetched = true
        initialiseRootView()
    }

    func showControls() {

        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "LoginVC", type: LoginVC.self)
            navigationController?.pushViewController(pushView, animated: true)
    }

    override func retryButtonAction() {
        fetchConfig()
    }

    func fetchConfig() {
        if BAReachAbility.isConnectedToNetwork() {
            removePlaceholderView()
            requestToken = repo.getConfigData(apiParams: APIParams(), completion: { (configData, error)  in
                self.requestToken = nil
                if let configData = configData {
                    BAConfigManager.shared.configModel = configData.parsed
                    self.handleNavigation()
                } else if let error = error {
                    if error.error.urlError?.code.rawValue == -1009 {
                        self.noInternetForSplash() {
                            if self.isVideoFinished {
                                self.fetchConfig()
                            } else {
                                self.initialiseVideo(withAudio: true)
                                self.fetchConfig()
                            }
                        }
                    } else if error.error.urlError?.code.rawValue == -1018 {
                        ///Ignore this case to avoid Internation roaming not allowed popup.
                    } else {
                        self.showOkAlertWithCallback(error.error.localizedDescription) {
                            self.initialiseVideo(withAudio: true)
                            self.fetchConfig()
                        }
                    }
                }
            })
        } else {
            noInternetForSplash() {
                if self.isVideoFinished {
                    self.fetchConfig()
                } else {
                    self.initialiseVideo(withAudio: true)
                    self.fetchConfig()
                }
            }
        }
    }

    func handleNavigation() {
        let currentVersion = getAppVersion()
        if let config = BAConfigManager.shared.configModel {
            let forceUpgradeVersion = config.data?.app?.appUpgrade?.ios?.forceUpgradeVersion ?? ""
            let recommendedUpgradeVersion = config.data?.app?.appUpgrade?.ios?.recommendedVersion ?? ""
//            if currentVersion.compare(forceUpgradeVersion, options: .numeric) == .orderedAscending {
//                showAlert(type: .forceUpgrade)
//            } else if currentVersion.compare(recommendedUpgradeVersion, options: .numeric) == .orderedAscending {
                showAlert(type: .recommended)
//            } else {
//                checkVideoPlayback()
//            }
        }
    }

    private func showAlert(type: AlertType) {

        switch type {
        case .forceUpgrade:
            let message = BAConfigManager.shared.configModel?.data?.app?.appUpgrade?.ios?.forceUpgradeMessage
            let alert = UIAlertController.init(title: "Upgrade", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "App Store", style: .default, handler: { (_) in
                let urlStr = "itms://itunes.apple.com/app/apple-store/id1555688122?mt=8"
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(URL(string: urlStr)!)
                }
            }))
            present(alert, animated: true, completion: nil)

        case .recommended:
            let message = BAConfigManager.shared.configModel?.data?.app?.appUpgrade?.ios?.recommendedMessage
            let alert = UIAlertController.init(title: "Recommended Upgrade", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
                self.initialiseRootView(isFromUpgrade: true)
            }))
            alert.addAction(UIAlertAction.init(title: "App Store", style: .default, handler: { (_) in
                let urlStr = "itms://itunes.apple.com/app/apple-store/id1555688122?mt=8"
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(URL(string: urlStr)!)
                }
            }))
            present(alert, animated: true, completion: nil)
        }
    }

    // MARK: - Actions
    @IBAction func moveToLoginScreen() {
        let pushView = UIStoryboard.loadViewController(storyBoardName: StoryboardType.main.fileName, identifierVC: "LoginVC", type: LoginVC.self)
        navigationController?.pushViewController(pushView, animated: true)
    }

    @IBAction func navigateToSignUpScreen() {
        super.moveToSignUpScreen()
    }
    
    func moveToLogin() {
        showControls()
    }

    func initialiseRootView(isFromUpgrade: Bool = false) {
        if isVideoFinished && isConfigFetched {
            isVideoFinished = false
            isConfigFetched = false
            if BAKeychainManager().customerName == "Guest User" {
                kAppDelegate.createHomeTabRootView()
            } else {
                if let flag = BAConfigManager.shared.configModel?.data?.config?.showMarketingScreen, flag {
                    self.pushToMarketingScreen()
                }else {
                    self.showControls()
                }
            }
        } else if isFromUpgrade {
            if BAKeychainManager().customerName == "Guest User" {
                kAppDelegate.createHomeTabRootView()
            } else {
                if let flag = BAConfigManager.shared.configModel?.data?.config?.showMarketingScreen, flag {
                    self.pushToMarketingScreen()
                }else {
                    self.showControls()
                }
            }
        }
    }
    
    func pushToMarketingScreen(){
        let marketingVC: MarketingViewController = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Maketing.rawValue, identifierVC: ViewControllers.marketingVC.rawValue, type: MarketingViewController.self)
        self.navigationController?.pushViewController(marketingVC, animated: true)
    }
}

//
//  noInternetVC.swift
//  BingeAnywhere
//
//  Created by Saket Kumar on 16/12/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import UIKit

class NoInternetVC: BaseViewController {
    
    
    let wifiImageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "nointernet") ?? UIImage()
        iv.widthAnchor.constraint(equalToConstant: 96).isActive = true
        iv.heightAnchor.constraint(equalToConstant: 74).isActive = true
        return iv
    }()
    
    let noInternetLabel : UILabel = {
        let title = UILabel()
        title.text = "No Internet Connection"
        title.textColor = .white
        title.font = UIFont.skyTextFont(.regular, size: 30.0)
        title.setLblLineHeight(lineHeight: 4.0)
        title.numberOfLines = 0
        title.lineBreakMode = .byTruncatingTail
        title.textAlignment = .center
        title.widthAnchor.constraint(equalToConstant: 280).isActive = true
        title.heightAnchor.constraint(equalToConstant: 72).isActive = true
        return title
    }()
    
    let msgLabel : UILabel = {
        let msg = UILabel()
        msg.text = kNoInternetBody//"Make sure that Wi-Fi or mobile data is turned on, then try again."
        msg.textColor = .white
        msg.font = UIFont.skyTextFont(.regular, size: 16.0)
        msg.setLblLineHeight(lineHeight: 4.0)
        msg.numberOfLines = 0
        msg.lineBreakMode = .byTruncatingTail
        msg.textAlignment = .center
        msg.widthAnchor.constraint(equalToConstant: 280).isActive = true
        msg.heightAnchor.constraint(equalToConstant: 44).isActive = true
        return msg
    }()
    
    let retryButton : UIButton = {
        let retryBtn = UIButton()
        retryBtn.backgroundColor = .BABlueColor
        retryBtn.setTitle("Retry", for: .normal)
        retryBtn.titleLabel?.textAlignment = .center
        retryBtn.titleLabel?.font = UIFont.skyTextFont(.regular, size: 16)
        retryBtn.setTitleColor(.white, for: .normal)
        retryBtn.widthAnchor.constraint(equalToConstant: 209).isActive = true
        retryBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        retryBtn.layer.cornerRadius = 22
        retryBtn.addTarget(self, action: #selector(retryConnection), for: .touchUpInside)
        return retryBtn
        
    }()
    
    var block:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .BAdarkBlueBackground
        
        configureViewComponents()
    }
    
    @objc func retryConnection(){
        if BAReachAbility.isConnectedToNetwork(){
            self.dismiss(animated: true) {
                self.block!()
            }
        }
    }
    
    func passConfig( completion :(() -> Void)? ){
        block = completion
    }
    
    func configureViewComponents(){
        let stackView = UIStackView(arrangedSubviews: [wifiImageView,noInternetLabel,msgLabel,retryButton])
        stackView.axis = .vertical
        if #available(iOS 11.0, *) {
            stackView.setCustomSpacing(31, after: wifiImageView)
            stackView.setCustomSpacing(10, after: noInternetLabel)
            stackView.setCustomSpacing(25, after: msgLabel)
        } else {
            stackView.spacing = 20
        }
        //stackView.spacing = 20
        stackView.distribution = .fill
        stackView.alignment = .center
        view.addSubview(stackView)
        stackView.anchor(top: nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 40, paddingBottom: 0, paddingRight:40, width: 0, height: 300)
        //stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
        
    }
    
}

extension UIView {
    func anchor(top:NSLayoutYAxisAnchor?,left:NSLayoutXAxisAnchor?,bottom:NSLayoutYAxisAnchor?,right:NSLayoutXAxisAnchor?,paddingTop:CGFloat,paddingLeft:CGFloat,paddingBottom:CGFloat,paddingRight:CGFloat,width: CGFloat, height: CGFloat){
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0{
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0{
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}


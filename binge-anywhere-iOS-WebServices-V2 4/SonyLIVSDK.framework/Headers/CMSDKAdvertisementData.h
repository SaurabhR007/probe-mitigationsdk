//
//  CMSDKAdvertisementData.h
//  CMSDKManager
//
//  Created by Crypto on 8/23/16.
//  Copyright © 2016 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMSDKAdvertisementData : NSObject

@property (setter=reportEvent:) BOOL reportEvent;

@property (nonatomic, strong) NSString *audioMP3;
@property (nonatomic, strong) NSString *videoMP4;
@property (nonatomic, strong) NSString *video3GP;

@property BOOL skipAllowed;

@property (nonatomic, strong) NSString *cachedImageFilePath;

-(void)placementDictionarySetup:(NSDictionary *)plDict;
-(void)playEventReport;

-(BOOL)hasAudioOrVideoMedia;

@end

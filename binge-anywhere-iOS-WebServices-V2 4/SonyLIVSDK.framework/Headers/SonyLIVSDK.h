//
//  SonyLIVSDK.h
//  SonyLIVSDK
//
//  Created by Saurabh Kapoor on 19/12/20.
//  Copyright © 2020 Sony Pictures Networks India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMSDKManager.h"
//#import "GAI.h"
//! Project version number for SonyLIVSDK.
FOUNDATION_EXPORT double SonyLIVSDKVersionNumber;

//! Project version string for SonyLIVSDK.
FOUNDATION_EXPORT const unsigned char SonyLIVSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SonyLIVSDK/PublicHeader.h>



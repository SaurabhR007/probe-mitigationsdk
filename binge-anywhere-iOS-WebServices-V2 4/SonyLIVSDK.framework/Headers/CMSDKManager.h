//
//  CMSDKManager.h
//  CMSDKManager
//
//  Copyright © 2015 Catch Media. All rights reserved.
//

/**
 *  More documentation can be found at http://docs.catchmedia.com/sdk/iOS/
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "CMSDKAppEvent.h"
#import "CMSDKMediaEvent.h"

#import "CMSDKConsumptionEvent.h"
#import "CMSDKConsumerTagReadHolder.h"
#import "CMSDKConsumptionStateHolder.h"

#import "CMSDKMediaTagReadHolder.h"
#import "CMSDKMediaTagMultiHolder.h"

#if TARGET_OS_IOS
#import "CMSDKAPNObject.h"
#endif

#import "CMSDKMailConfig.h"

#import "CMSDKPlaylist.h"
#import "CMSDKPlaylistItem.h"

#import "CMSDKAdvertisementData.h"

#import "CMSDKAvPlayerIntegration.h"

// -----------------------------------------------------------------------------
// SDK Constants

/**
 *  Used when defining the error domain with NSError
 */
extern NSString *const CMSDKErrorDomain;

// -----------------------------------------------------------------------------
// SDK Notifications

/**
 *  Called when a deeplink url is received and userInfo dictionary will contain @"url":<link>
 */
extern NSString *const CMSDKDeepLinkURLAvailableNotification;

/**
 *  Called when the SDK hides a push message created by the SDK.
 */
extern NSString *const CMSDKHideAPNSWindow;

/**
 *  Called when a session has been created (no userInfo available)
 */
extern NSString *const CMSDKSessionAvailabeNotification;

/**
 *  Called when one of dashboard values were updated
 */
extern NSString *const CMSDKUpdateDashboardValues;

/**
 *  Called when a user has been set (no userInfo available)
 */
extern NSString *const CMSDKUserSetNotification;

/**
 *  Called when a tag has been sent to Web Services and the userInfo will contain the tag sent.
 */
extern NSString *const CMSDKTagSentNotification;

/**
 *  Called when a tag has been deleted in Web Services and the userInfo will contain the tag sent.
 */
extern NSString *const CMSDKTagDeletedNotification;

/**
 *  CMSDKplayingNotification notification is called when the SDK is playing an audio or video file.
 *  The userInfo will contain the key:value -> playingType:<then_the_type>
 */
extern NSString *const CMSDKplayingNotification;

/**
 *  CMSDKAdsAvailableNotification notification is called when the SDK has ads ready to be used.
 *  This is called everytime the SDK refreshes the available ads.
 */
extern NSString *const CMSDKAdsAvailableNotification;

/**
 *  CMSDKReceivedInboxNotification notification is called when the SDK has received email.
 */
extern NSString *const CMSDKReceivedInboxNotification;

/**
 *  CMSDKNewCountChangedInboxNotification notification is called when the SDK has a change in the number of new email.
 *  This notification is called when there could have been a change in the number of new messages.  You should call the 
 *  SDK method -(int)getMailNewCount to get the correct count.
 */
extern NSString *const CMSDKNewCountChangedInboxNotification;

/**
 *  CMSDKDeletedInboxNotification notification is called when the SDK has deleted an email.
 */
extern NSString *const CMSDKDeletedInboxNotification;


// -----------------------------------------------------------------------------


@protocol CMSDKManagerProtocol;

/**
 *  The CMSDKManager
 */
@interface CMSDKManager : NSObject


@property (nonatomic, weak) id<CMSDKManagerProtocol> delegate;

/**
 *  isApnsTokenSet can be used to check if the SDK has set the Apple Push Notification Token.
 *
 *  @warning If this token is not set the SDK has not reported the token to Catch Media and push notification services will not reach this device.
 */
@property (nonatomic, getter = isApnsTokenSet) BOOL apnsTokenSet;

/**
 *  The shared CMSDKManager instance.
 *
 *  This is the gateway to all that is the CMSDKManager and you call it as [CMSDKManager instance];
 *
 *  Calling this will start the SDK setup, but will stop short of a complete initialization of the SDK.
 *  You NEED to call - (void)intializeSDK:(BOOL)waitForSilentUser;
 *  to continue the SDK setup.
 *
 *  @return a singleton of the CMSDKManager
 */
+ (instancetype)instance;

/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method. 
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  Example: [[CMSDKManager instance] intializeSDK:NO]
 *
 *  @param waitForSilentUser BOOL to flag the SDK your intention to provide a unique device ID or to use one created by the SDK.
 */
- (void)intializeSDK:(BOOL)waitForSilentUser;

/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method.
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  Example: [[CMSDKManager instance] intializeSDKWithCampaigns:NO withCampaigns:YES]
 *
 *  @param waitForSilentUser BOOL to flag the SDK your intention to provide a unique device ID or to use one created by the SDK.
 *  @param withCampaigns BOOL to flag the SDK your intention to fetch ads campaigns, by default it is NO
 */
- (void)intializeSDKWithCampaigns:(BOOL)waitForSilentUser withCampaigns:(BOOL)withCampaignsBool;

/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method.
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  @param waitForSilentUser BOOL to flag the SDK your intention to provide a unique device ID or to use one created by the SDK.
 *  @param extraData  NSDictionary of additional data you want to pass in with registration
 */
- (void)intializeSDKWithExtra:(BOOL)waitForSilentUser extraData:(NSDictionary*)extraData;

/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method.
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  @param waitForSilentUser BOOL to flag the SDK your intention to provide a unique device ID or to use one created by the SDK.
 *  @param withCampaigns BOOL to flag the SDK your intention to fetch ads campaigns, by default it is NO
 *  @param extraData  NSDictionary of additional data you want to pass in with registration
 */
- (void)intializeSDKWithCampaignsAndExtra:(BOOL)waitForSilentUser withCampaigns:(BOOL)withCampaignsBool extraData:(NSDictionary*)extraData;

    
/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method.
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  @param waitForSilentUser BOOL to flag the SDK your intention to provide a unique device ID or to use one created by the SDK.
 *  @param withCampaigns BOOL to flag the SDK your intention to fetch ads campaigns, by default it is NO
 *  @param extraData  NSDictionary of additional data you want to pass in with registration
 *  @param appCode NSString that is your new appCode
 */
- (void)intializeSDKWithCampaignsAndExtraAndNewAppCode:(BOOL)waitForSilentUser withCampaigns:(BOOL)withCampaignsBool extraData:(NSDictionary*)extraData appCode:(NSString*)appCode;

/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method.
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  @param config NSDictionary, contains optional BOOLs: waitForSilentUser, withCampaigns, withInbox, withRecoProfile
 *  @param extraData  NSDictionary of additional data you want to pass in with registration
 */
- (void)initializeSDKWithConfigAndExtra:(NSDictionary*)config extraData:(NSDictionary*)extraData;


/**
 *  The first time you Call the instance class method a single instance of the required objects will be created.
 *  If you have a previous Unique Identification for the device, you can pass it in to the SDK.
 *
 *  Place [CMSDKManager instance]; in your AppDelegate's application:didFinishLaunchingWithOptions: to start the SDK instance.
 *  Then call this method to initialized the SDK.
 *
 *  If you plan to use your own unique device id then pass YES to this method.
 *  If you want to use a unique device id created by the SDK then pass NO to this Method.
 *
 *  setSilentUser:silentUserString needs to be called to pass in your own unique device id.
 *
 *  Also, use [CMSDKManager instance] to call the methods within the CMSDKManager.
 *
 *  @param config NSDictionary, contains optional BOOLs: waitForSilentUser, withCampaigns, withInbox, withRecoProfile
 *  @param extraData  NSDictionary of additional data you want to pass in with registration
 *  @param appCode NSString that is your new appCode
 */
- (void)initializeSDKWithConfigAndExtraAndNewAppCode:(NSDictionary*)config extraData:(NSDictionary*)extraData appCode:(NSString*)appCode;

/**
 *  If you used a BOOL value of YES for intializeSDK:(BOOL)waitForSilentUser then you must use this method to set the unique device id.
 *
 *  @warning The SDK will not continue its setup until this parameter is set.
 *
 *  @param silentUserString NSString that is your uniqu device id
 */
- (void)setSilentUser:(NSString *)silentUserString;
    
    
/**
 * Changes appCode to a different value
 *
 * @param appCode NSString that is your new appCode
 */
- (void)changeAppCode:(NSString*)appCode;

/**
 * Updates extraData of this device
 *
 * @param extraData NSDictionary of additional data you want to associate with this device
 */
- (void)updateDeviceExtraData:(NSDictionary*)extraData;

/**
 * Updates extraData of the current user
 *
 * @param extraData NSDictionary of additional data you want to associate with this device
 */
- (void)updateUserExtraData:(NSDictionary*)extraData;
    
// -----------------------------------------------------------------------------

#pragma mark - Register/ Unregister a user

/**
 *  If your app has a register/known user option, you should associate the account with a unique identifier in the CMSDK. This enables unified tracking  *across multiple devices. A CMSDK user only needs a unique String ID like a device ID, phone number or email address. You can use any String you like, provided it is unique for each user. When you sign up a new user, you must also include a String signup type. This String should describe the method of sign up, but can be left null. You may want to use signup type to indicate if the user signed up through Google of Facebook. You can also associate extra data with the user as String key-value pairs.
 *
 *  @param userName   NSString that you are using for a username
 *  @param signupType NSString generally will be "default"
 *  @param extraData  NSDictionary of additional data you want to pass in with registration
 *  @param mergeBool  BOOL to flag if you want to merge this devices silent user data with this registered user
 *  @param completion The completion handler block allows you to react to the BOOL success for the attempt to register the user
 *  @param error      NSError CMSDK will signal about invalid state when setUser is called
 */
-(void)setUser:(NSString*)userName signUpType:(NSString*)signupType extraData:(NSDictionary*)extraData merge:(BOOL)mergeBool withCompletion:(void (^)(BOOL success))completion error:(NSError **)error;

/**
*  If your app has a register/known user option, you should associate the account with a unique identifier in the CMSDK. This enables unified tracking  *across multiple devices. A CMSDK user only needs a unique String ID like a device ID, phone number or email address. You can use any String you like, provided it is unique for each user. When you sign up a new user, you must also include a String signup type. This String should describe the method of sign up, but can be left null. You may want to use signup type to indicate if the user signed up through Google of Facebook. You can also associate extra data with the user as String key-value pairs.
*
*  @param userName   NSString that you are using for a username
*  @param signupType NSString generally will be "default"
*  @param extraData  NSDictionary of additional data you want to pass in with registration
*  @param mergeBool  BOOL to flag if you want to merge this devices silent user data with this registered user
*  @param profile NSString profile name
*  @param isChildProfile BOOL if profile belongs to a child
*  @param completion The completion handler block allows you to react to the BOOL success for the attempt to register the user
*  @param error      NSError CMSDK will signal about invalid state when setUser is called
*/
-(void)setUser:(NSString*)userName signUpType:(NSString*)signupType extraData:(NSDictionary*)extraData merge:(BOOL)mergeBool withProfile:(NSString*)profile withIsChildProfile:(BOOL)isChildProfile withCompletion:(void (^)(BOOL success))completion error:(NSError **)error;

/**
 * Sets profile for the current user
 * @param profile NSString string describing a profile to use for the current user
 * @param isChildProfile BOOL if that profile belongs to a child, child constraints should be applied
 */
-(void)setProfile:(NSString*)profile withIsChildProfile:(BOOL)isChildProfile;
/**
 *  Calling this method will unset the current user and fall back to the silent user.
 */
-(void)unsetUser;

/**
 * Retrieving the consumer tag named "reco_profile"
 * @return consumer tag, saved locally in CMSDK, this is synchronous function
 */
-(NSString*)getRecommendationProfile;

// -----------------------------------------------------------------------------

#pragma mark - Push Notifications

/**
 *  You will need to add a forward in your app delegate's
 application:didRegisterForRemoteNotificationsWithDeviceToken: method
 and pass this method the NSData to allow the SDK to register the token with Catch Media
 *
 *  @param deviceToken Pass the NSData you received from your app delegate method with the same name.
 *
 *  @return BOOL of successful attempt within the SDK
 */
-(BOOL)didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

/**
 * Application needs to forward remote notifications error to CMSDK
 */
- (void)didFailToRegisterForRemoteNotificationsWithError:(NSError*)error;
/**
 *  When your app has received an apns your app delegate receives it in the
 - (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
 method and needs to forward it on to the SDK here.
 *
 *  @param userInfo pass the userInfo from your app delegate method to this method
 */
-(void)didReceiveRemoteNotification:(NSDictionary *)userInfo;

//#pragma mark - deeplinking

//-(void)applicationOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

// -----------------------------------------------------------------------------

#pragma mark - Consumer Tagging

/**
 *   Create Consumer Tag with NSString for tagName and tagValue
 *
 *  @param tagName  Name of the tag you are creating
 *  @param tagValue Value for the tag name
 */
-(void)createTag:(NSString*)tagName withValue:(NSString *)tagValue;

/**
 *  Delete Consumer Tag with NSString for tagName
 *
 *  @param tagName Name of the tag you want to delete
 */
-(void)deleteTag:(NSString*)tagName;

/**
 *  Read ALL Consumer Tags for current consumer.
 *
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKConsumerTagReadHolder *tagsHolder.
 *  The tagsHolder will contain the tag results in NSMutableDictionary *tagsDict.
 */
-(void)readConsumerTagsWithCompletion:(void (^)(BOOL success, CMSDKConsumerTagReadHolder *tagsHolder))completionHandler;

/**
 *  Read ALL Consumer Tags for current consumer with the filtered list of tagsArray.
 *
 *  @param tagsArray         Array of tags as NSStrings
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKConsumerTagReadHolder *tagsHolder.
 *  The tagsHolder will contain the tag results in NSMutableDictionary *tagsDict.
 */
-(void)readConsumerTags:(NSArray*)tagsArray withCompletion:(void (^)(BOOL success, CMSDKConsumerTagReadHolder *tagsHolder))completionHandler;

/**
 * Read consumption states by mediaID (optional) and mediaKind filter
 *
 *  @param mediaID   NSString
 *  @param mediaKind NSString
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKConsumptionStateHolder *resultsHolder.
 *  The resultsHolder will contain the results in NSMutableArray *results.
 */
-(void)readConsumptionState:(NSString*)mediaID mediaKind:(NSString*)mediaKind withCompletion:(void (^)(BOOL success, CMSDKConsumptionStateHolder *resultsHolder))completionHandler;


// -----------------------------------------------------------------------------

#pragma mark - Media Tagging

/**
 *   Create Media Tag requires a NSDictionary with four key/value pairs.
 
 *   NSDictionary *tagsDict = @{
 *
 *   @"media_id":@intValue,          <- media_id is an integer value
 *
 *   @"media_kind":@"kindString",    <- media_kind is a registered string
 *
 *   @"tag":@"nameString",           <- tag is a string
 *
 *   @"value":@(1)                   <- value conforms to tag configuration
 *
 *   };
 *
 *  @param tagDict NSDictionary of tag key/values defined above
 */
-(void)createMediaTag:(NSDictionary*)tagDict;

/**
 *  Delete a media tag
 *
 *  @param tagName   NSString
 *  @param mediaID   NSString
 *  @param mediaKind NSString
 */
-(void)deleteMediaTag:(NSString*)tagName mediaID:(NSString*)mediaID mediaKind:(NSString*)mediaKind;

/**
 *  Read all media tags
 *
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKMediaTagReadHolder *tagsHolder.
 *  The tagsHolder will contain the tag results in NSMutableArray *tagsArray.
 */
-(void)readMediaTagsWithCompletion:(void (^)(BOOL success, CMSDKMediaTagReadHolder *tagsHolder))completionHandler;

/**
 *   Read Media Tag Multi requires a NSArray with objects of NSDictionary with two key/value pairs.
 
 *   NSArray *mediaListArray = @[
 *
 *   @{ @"media_id":@intValue,  <- media_id is an integer value
 *
 *   @"media_kind":@"mediaKindString"}     <- media_kind is a registered string
 *
 *   ];
 *
 *  @param mediaListArray    array of media dictionaries that contain keys for media_id and media_kind
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKMediaTagMultiHolder *tagsHolder.
 *  The tagsHolder will contain the tag results in NSMutableArray *tagsArray.
 */
-(void)readMediaTagMulti:(NSArray *)mediaListArray withCompletion:(void (^)(BOOL success, CMSDKMediaTagMultiHolder *tagsHolder))completionHandler;


/**
 *  Read Media Tag requires the name NSString.
 The completion handler result with success (YES) will also return the tagsArray with the result from webservices.
 *
 *  @param tagName           NSString
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKMediaTagReadHolder *tagsHolder.
 *  The tagsHolder will contain the tag results in NSMutableArray *tagsArray.
 */
-(void)readMediaTag:(NSString*)tagName withCompletion:(void (^)(BOOL success, CMSDKMediaTagReadHolder *tagsHolder))completionHandler;

/**
 *  Read Media Tag requires the name NSString and optional filter.
 The completion handler result with success (YES) will also return the tagsArray with the result from webservices.
 *
 *  @param tagName           NSString
 *  @param filter   filter dictionary containing either index,offset,limit values or date_start (with optional date_end)
 *  @param completionHandler The completion block will contain the BOOL success and the CMSDKMediaTagReadHolder *tagsHolder.
 *  The tagsHolder will contain the tag results in NSMutableArray *tagsArray.
 */

-(void)readMediaTag:(NSString*)tagName withFilter:(NSDictionary *)filter withCompletion:(void (^)(BOOL success, CMSDKMediaTagReadHolder *tagsHolder))completionHandler;

// -----------------------------------------------------------------------------

#pragma mark - Application Events


/**
 * To send an App report, you need to create a CMSDKAppEvent and pass it to the SDK.
 *
 * Example:
 *
 * Create a dictionary with extra data
 *
 * NSMutableDictionary *extraData = @{@"Dog Type":@"Labrador", @"Play Level":@"Novice"};
 *
 * Create a new app event object with a type string and your extra data
 *
 * CMSDKAppEvent *newAppEvent = [[CMSDKAppEvent alloc] initWithType:@"playDog"
 extraData:extraData];
 *
 * Then report the event with this method.
 *
 * [[CMSDKManager instance] reportAppEventWithEvent:newAppEvent]; 
 *
 *  @param CMSDKAppEvent is the event object you created for the report
 */
-(void)reportAppEventWithEvent:(CMSDKAppEvent*)CMSDKAppEvent;



// -----------------------------------------------------------------------------

#pragma mark - Media Events

/**
 *  Use this method to report a CMSDKMediaEvent object you have created.
 *  The CMSDKMediaEvent objects need to be sent using this method.
 *
 * Example:
 *
 * Create a dictionary with extra data
 *
 * NSMutableDictionary *extraData = @{@"app_location":@"favourites", @"my_key":@"myData"};
 *
 *
 * Create a new media event object with enums and extra data dictionary.
 *
 * Enum:
 *
 * mediaEventKind chosenMediaEventKind = mediaTypeVideo;
 *
 * CMSDKMediaEvent *newMediaEvent =
 *
 * [[CMSDKMediaEvent alloc] initWithMediaID:@"US-Z9A-05-00001"
 *
 * mediaKind:chosenMediaEventKind
 *
 * mediaEventType:@"bookmark"
 *
 * ExtraData:extraData];
 *
 * Use this method to report your event
 *
 * [[CMSDKManager instance] reportMediaEvent:newMediaEvent];
 *
 *  @param mediaEvent a CMSDKMediaEvent you created
 */
-(void)reportMediaEvent:(CMSDKMediaEvent*)mediaEvent;

#pragma mark - Players Integrations

-(CMSDKAvPlayerIntegration*)createAvPlayerIntegration:(AVPlayer*)player playerItem:(AVPlayerItem*)playerItem mediaID:(NSString*)mediaID mediaKind:(NSString*)mediaKind extraData:(NSDictionary*)extraDataDictionary configuration:(NSDictionary*)configurationDictionary;

#pragma mark - Campaigns

/**
 *  Use this method to check if there are ad placements available for a certain type.
 *
 *  @return of YES means there is at least one placement for that type.  No if there is none.
 */
-(BOOL)adPlacementsReadyForType:(NSString*)placementType;

/**
 *  These methods will create required viewController/data for the supplied placement type
 *
 *  @warning You should call adPlacementsReadyForType: before calling this method to check if a placement is available.
 *  @warning Calling this method when no placement for a type is available and the returned value will be nil.
 */
- (UITableViewCell*)createTableViewCell:(NSString*)placementType;
- (UIViewController*)createBannerViewController:(NSString*)placementType;
- (UIViewController*)createDrawerViewController:(NSString*)placementType;
- (UIViewController*)createPlayerAdViewController:(NSString*)placementType;
- (CMSDKAdvertisementData*)getMediaAdData:(NSString*)placementType;

#pragma mark - Inbox

-(int)getMailNewCount;

-(UIViewController *)getInboxViewController;

-(void)getInboxMailItemsArrayWithCompletion:(void (^)(BOOL success, NSArray *mailItemsArray))completion;

-(void)createRequestForInitialInbox;

-(void)createRequestForPagedInbox;

-(BOOL)isMailListLastPage;

-(BOOL)isMailFetchingMail;

-(void)getMailItemThumbImage:(NSMutableDictionary *)mailAttachDict;

-(UIImage *)getMailNewImage;

-(void)setDeletedMailItems:(NSArray *)deletedItemsArray;

-(NSMutableDictionary *)getInboxMailItem:(NSDictionary *)mailChoice;

#pragma mark - Playlist Methods

/**
 *  Use this method to get back an NSArray of the playlists objects in the completion block.  Each object is a CMSDKPlaylist.
 *
 *  @param filter can be a certain playlist type that you want to get back.  Leave it nil if you don't want to use it.
 */
- (void)readPlaylistsWithOptionalFilter:(NSString*)filter withCompletion:(void (^)(BOOL success, NSArray *playLists))completionHandler;

/**
 *  Use this method to get back an NSArray of a specific playlist's items objects in the completion block.  Each item in the array is a CMSDKPlaylistItem.
 *
 *  @param playlist is a CMSDKPlaylist object
 */
- (void)readPlaylist:(CMSDKPlaylist *)playlist withCompletion:(void (^)(BOOL success, CMSDKPlaylist *playlist))completionHandler;

/**
 *  Use this method to create a new playlist.
 *
 *  @param playlist is a CMSDKPlaylist object
 */
- (void)createPlaylist:(CMSDKPlaylist *)createPlaylist withCompletion:(void (^)(BOOL success, CMSDKPlaylist *playlist))completionHandler;

/**
 *  Use this method to update a playlist.
 *
 *  @param playlist is a CMSDKPlaylist object
 *  @param mediaList is the NSArray of CMSDKPlaylistItem objects. Leave mediaList nil if you don't want to update the media list.
 */
- (void)updatePlaylist:(CMSDKPlaylist *)playlist mediaList:(NSArray*)mediaList withCompletion:(void (^)(BOOL success))completionHandler;

/**
 *  Use this method to append media to a playlist.
 *
 *  @param playlistID is the integer ID of the playlist that you would like to append.
 *  @param mediaList is the NSArray of CMSDKPlaylistItem objects to apppend to the playlist.
 */
- (void)appendPlaylist:(CMSDKPlaylist *)playlist mediaList:(NSArray*)mediaList withCompletion:(void (^)(BOOL success))completionHandler;


/**
 *  Use this method to delete a playlist.
 *
 *  @param playlist is a CMSDKPlaylist object
 *  @warning This can not be undone!
 */
- (void)deletePlaylist:(CMSDKPlaylist *)playlist withCompletion:(void (^)(BOOL success))completionHandler;

#pragma mark - demo app
/**
 *  This method is only provided to support the Demo Application.
 *  @warning No gaurantees are given for this method.  It can change at anytime without warning.
 *  @warning Do not use it!
 */
-(NSDictionary*)getDashboardValues;

// -(void)activate404Test;

/**
 * For purposes of inter-platform-CMSDK interaction
 * @return CM device ID
 */
-(NSString*)getCMDeviceId;

@end

// -----------------------------------------------------------------------------


#pragma mark - Protocols

/**
 *  CMSDKManagerProtocol used for Apple Push Notification Services
 */
@protocol CMSDKManagerProtocol <NSObject>

@required
// for Push Notification objects to be handed over to you

/**
 * By implementing this protocol you are letting the SDK know you want to handle displaying
 push notifications yourself.  You will receive the components of the push notification in
 the userInfo and it is up to you to use them correctly.  The SDK will not present a push notification
 when they are received, but will hand it back to you with this method.
 *
 * @warning Implementing this protocal will automatically stop the SDK from displaying push notifications.  You are expected to create your own push view with the returned components contained in userInfo.
 *
 * @warning If the delegate is unassigned at anytime this will trigger the SDK to handle displaying push notifications again.
 *
 *  @param userInfo Contains objects of the push notification.
 */
-(void)CMSDKDidReceiveRemoteNotification:(NSDictionary *)userInfo;

@end


//
//  CMSDKAppEvent.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  CMSDKAppEvent is used as a template for reporting app events.
 */
@interface CMSDKAppEvent : NSObject <NSCopying, NSCoding>


@property (nonatomic, strong) NSMutableDictionary *eventDict;

-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;

/**
 *  for initializing an event
 *
 *  @param type      is a NSString of the event type you are creating
 *  @param extraData and NSDicationary of extra key/values for your event
 *
 *  @return a CMSDKAppEvent object is returned for reporting
 */
-(instancetype)initWithType:(NSString*)type extraData:(NSDictionary*)extraData;

@end

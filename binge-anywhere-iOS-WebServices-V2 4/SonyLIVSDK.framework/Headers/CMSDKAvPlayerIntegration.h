//
//  CMSDKAvPlayerIntegration.h
//
//  Copyright © 2018 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface CMSDKAvPlayerIntegration : NSObject

/**
 * optional configuration may contain two fields:
 *      recordForReadIntervalSeconds - how often to update server-side about current consumption positon
 *      lifeTimeDays - for how many days consumption position should be kept
 */
-(instancetype)initWithPlayer:(AVPlayer*)player playerItem:(AVPlayerItem*)playerItem mediaID:(NSString*)mediaID mediaKind:(NSString*)mediaKind extraData:(NSDictionary*)extraDataDictionary configuration:(NSDictionary*)configurationDictionary;

/**
 * Use this method when media is changed (replaced) in AVPlayer
 */
-(void)changeMedia:(AVPlayerItem*)playerItem mediaID:(NSString*)mediaID mediaKind:(NSString*)mediaKind extraData:(NSDictionary*)extraDataDictionary configuration:(NSDictionary*)configurationDictionary;

/**
 * Use this method when current media should be seeked
 */
-(void)seekToTime:(float)newTime;

/**
 * Stop the integration, send interruption events
 */
-(void)stop;

@end

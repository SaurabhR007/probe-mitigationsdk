//
//  CMSDKConsumptionStateHolder.h
//  CMSDKManager
//
//  Copyright © 2017 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  CMSDKConsumptionStateHolder is used to manage read consumption states from webservices
 */
@interface CMSDKConsumptionStateHolder : NSObject <NSCopying, NSCoding>

/**
 *  This CMSDKConsumptionStateHolder object will contain this resultsArray of returned results
 */
@property (nonatomic, strong) NSMutableArray *resultsArray;


-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;
-(instancetype)initWithResults:(NSMutableArray*)results;

@end

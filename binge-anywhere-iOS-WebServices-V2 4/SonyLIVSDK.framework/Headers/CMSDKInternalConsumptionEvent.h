//
//  CMSDKInternalConsumptionEvent.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CMSDKInternalConsumptionEvent : NSObject <NSCopying, NSCoding>

typedef NS_ENUM(NSInteger, internalConsumptionEventContentType) {
    internalContentTypeTrack = 1,
    internalContentTypeVideo,
    internalContentTypeFilm,
    internalContentTypeAlbum,
    internalContentTypeArtist,
    internalContentTypeGenre,
    internalContentTypeEbook,
    internalContentTypeNotifications
};

typedef NS_ENUM(NSInteger, internalConsumptionEventType) {
    internalConsumptionTypeStream = 1,
    internalConsumptionTypeDownload,
    internalConsumptionTypeCached,
    internalConsumptionTypeSaved
};


@property (nonatomic, strong, getter=eventDict) NSMutableDictionary *eventDict;

-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;

-(instancetype)initWithMediaID:(NSString*)mediaId
                   contentType:(enum internalConsumptionEventContentType)contentType
               consumptionType:(enum internalConsumptionEventType)consumptionType
                    deliveryID:(NSString*)deliveryId
                     extraData:(NSDictionary*)extraData;

-(void)start:(double)startPosition;

-(void)update:(double)deltaUpdate;

-(void)stopWithDuration:(double)duration stopTime:(double)stopTime didFinishContent:(BOOL)didFinish;

@end

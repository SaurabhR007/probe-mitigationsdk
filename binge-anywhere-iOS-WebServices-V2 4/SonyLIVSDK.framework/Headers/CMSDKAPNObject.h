//
//  CMSDKAPNObject.h
//  PlayAnywhere
//
//  Created on 2/7/15.
//  Copyright (c) 2015 Catchmedia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVKit;

#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@class CMSDKActionButton;

@interface CMSDKAPNObject : NSObject <AVPlayerViewControllerDelegate >

// movie player for ios 7
@property (strong, nonatomic) MPMoviePlayerController *myPlayer;

// movie player for ios 8 and up
@property (strong, nonatomic) AVPlayerViewController *playerViewController;

// Dictionary object that contains all the necessary objects for the current mail item.
@property (strong, nonatomic) NSMutableDictionary *CMSDKAPNObjectDict;


-(instancetype)init;

-(void)setPushDict:(NSDictionary*)pushDict;

-(void)stopPlayer;

@end

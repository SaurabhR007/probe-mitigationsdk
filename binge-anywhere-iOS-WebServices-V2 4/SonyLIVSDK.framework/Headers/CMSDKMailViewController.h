//
//  CMSDKMailViewController.h
//  PlayAnywhere
//
//  Created on 9/18/14.
//  Copyright (c) 2014 Catchmedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSDKSharedHeader.h"

@interface CMSDKMailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

-(BOOL)isCMSDKMailViewController;

-(void)shouldShowBackButton:(BOOL)showButton;

-(void)setActionDictionaryForStoryboard:(NSMutableDictionary*)actionDict;

@end

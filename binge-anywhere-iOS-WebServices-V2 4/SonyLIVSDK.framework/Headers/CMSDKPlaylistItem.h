//
//  CMSDKPlaylistItem.h
//  CMSDKManager
//
//  Created by Christopher Scott on 5/17/16.
//  Copyright © 2016 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMSDKPlaylistItem : NSObject

typedef NS_ENUM(NSInteger, playlistItemContentType) {
    playlistItemContentTypeTrack = 1,
    playlistItemContentTypeVideo,
    playlistItemContentTypeFilm,
    playlistItemContentTypeArtist,
    playlistItemContentTypeEbook
};

@property (readonly, nonatomic, getter=getId) NSNumber *itemId;

@property (readonly, nonatomic, strong, getter=getName) NSString *itemName;

@property (readonly, nonatomic, strong, getter=getItemContentType) NSString *itemContentType;

@property (readonly, nonatomic, strong) NSNumber *thirdPartyItemId;

@property (readonly, nonatomic, strong) NSString *thirdPartyItemContentType;

/**
 *  Use this method to create a new playlist item instance.
 *
 *  @param idNum NSNumber is necessary to create an instance
 *  @param name NSString is NOT necessary to create an instance.  Send nil if you don't have one.
 *  @param contentType enum is necessary to create an instance
 *
 *  @return is an instance of CMSDKPlaylistItem
 */
- (instancetype)initWithPlaylistItemID: (NSNumber *)idNum name:(NSString *)name contentType:(enum playlistItemContentType)contentType;


/**
 *  Use this method to create a new playlist instance.
 *  @warning This method is used internally to the SDK and
 */
- (instancetype)initWithPlaylistItemDict: (NSDictionary *)newPlaylistItemDict;


- (id)copyWithZone:(NSZone *)zone;

@end


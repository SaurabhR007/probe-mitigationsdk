//
//  CMSDKMailConfig.h
//  CMSDKManager
//
//  Created by Christopher Scott on 4/28/16.
//  Copyright © 2016 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CMSDKMailConfig : NSObject

@property __block BOOL mainBackgroundBool;
@property __block BOOL editButtonColorBool;
@property __block BOOL buttonColorBool;

+ (instancetype)instance;

-(void)setMailListMainBackgroundColorWithRed:(float)redMBG green:(float)greenMBG blue:(float)blueMBG alpha:(float)alphaMBG;

-(UIColor*)mailMainBackgroundColor;

-(void)setMailEditButtonLabelColorWithRed:(float)redMBG green:(float)greenMBG blue:(float)blueMBG alpha:(float)alphaMBG;

-(UIColor*)mailEditButtonLabelColor;

-(void)setMailActionButtonColorWithRed:(float)redMBG green:(float)greenMBG blue:(float)blueMBG alpha:(float)alphaMBG;

-(UIColor*)mailActionButtonColor;

-(void)setFont:(NSString*)fontName;

-(NSString*)fontName;

@end

//
//  CMSDKMediaTagReadHolder.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  CMSDKMediaTagReadHolder is used to manage read tags from webservices
 */
@interface CMSDKMediaTagReadHolder : NSObject <NSCopying, NSCoding>

/**
 *  This CMSDKMediaTagReadHolder object will contain this tagsArray of returned results
 */
@property (nonatomic, strong) NSMutableArray *tagsArray;


-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;
-(instancetype)initWithTags:(NSMutableArray*)tags;

@end

//
//  CMSDKConsumerTagReadHolder.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  CMSDKConsumerTagReadHolder is used to manage read tags from webservices
 */
@interface CMSDKConsumerTagReadHolder : NSObject <NSCopying, NSCoding>

/**
 *  This CMSDKConsumerTagReadHolder object will contain this tagsDict of returned results
 */
@property (nonatomic, strong) NSMutableDictionary *tagsDict;


-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;
-(instancetype)initWithTags:(NSMutableDictionary*)tags;

@end

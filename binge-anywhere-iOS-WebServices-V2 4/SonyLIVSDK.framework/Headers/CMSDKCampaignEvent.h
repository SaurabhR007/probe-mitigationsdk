//
//  CMSDKCampaignEvent.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  CMSDKCampaignEvent is used as a template for reporting app events.
 */
@interface CMSDKCampaignEvent : NSObject <NSCopying, NSCoding>


@property (nonatomic, strong) NSMutableDictionary *eventDict;

-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;

/**
 *  for initializing an event
 *
 *  @param type      is a NSString of the Campaign Play Event type you are creating
 *  @param extraData and NSDicationary of extra key/values for your event.  This should contain all the 
 *      required and optional components listed in 12.31.2 descriptor.
 *
 *  @return a CMSDKCampaignEvent object is returned for reporting
 */
-(instancetype)initWithType:(NSString*)type extraData:(NSDictionary*)extraData;

-(void)updateEventDictionary:(NSDictionary*)extraData;

@end

//
//  CMSDKMediaEvent.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  CMSDKMediaEvent is used as a template for reporting media events.
 */
@interface CMSDKMediaEvent : NSObject <NSCopying, NSCoding>

/**
 *  used to create the mediaEvent by restricting the options for media event kind
 */
typedef NS_ENUM(NSInteger, mediaEventKind) {
    mediaTypeTrack = 1,
    mediaTypeVideo,
    mediaTypeFilm,
    mediaTypeArtist,
    mediaTypeEbook
};


@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSString *eventKind;
@property (nonatomic, strong) NSString *eventType;
@property (nonatomic, strong) NSMutableDictionary *extraDataDict;
@property (nonatomic, strong) NSMutableDictionary *eventDict;

-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;

/**
 *  for initializing a media event
 *
 *  @param mediaIDString        NSString of the media's id
 *  @param mediaKindEnum        an Enum value of the type mediaEventKind
 *  @param mediaEventTypeString NSString for the event type
 *  @param extraDataDictionary  NSDictionary for extra key/value data for the event
 *
 *  @return CMSDKMediaEvent object is returned for reporting
 */
-(instancetype)initWithMediaID:(NSString*)mediaIDString mediaKind:(enum mediaEventKind)mediaKindEnum mediaEventType:(NSString*)mediaEventTypeString extraData:(NSDictionary*)extraDataDictionary;

@end

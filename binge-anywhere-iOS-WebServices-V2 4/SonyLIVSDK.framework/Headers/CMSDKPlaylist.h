//
//  CMSDKPlaylist.h
//  CMSDKManager
//
//  Created by Christopher Scott on 5/17/16.
//  Copyright © 2016 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMSDKPlaylist : NSObject

typedef NS_ENUM(NSInteger, playlistContentTypeEnum) {
    playlistContentTypeTrack = 1,
    playlistContentTypeVideo,
    playlistContentTypeFilm,
    playlistContentTypeAlbum,
    playlistContentTypeArtist,
    playlistContentTypeGenre,
    playlistContentTypeEbook
};


// This var allows access to its setter and getter

@property (nonatomic, strong, getter=getName, setter=setName:) NSString *name;


// These vars allow access to getters only

@property (readonly, nonatomic, getter=getId) NSNumber *playlistId;

@property (readonly, nonatomic, getter=getItemCount) NSNumber *playlistItemCount;

@property (readonly, nonatomic, getter=getDateCreated) NSDate *playlistDateCreated;

@property (readonly, nonatomic, strong, getter=getPlaylistContentType) NSString *playlistContentType;

@property (readonly,strong, nonatomic) NSMutableArray *playlistItems;


/**
 *  Use this method to create a new playlist instance
 *
 *  @param playlistName is a name for the playlist
 *  @param contentType is an enum type for the playlist
 *
 *  @return instance of a CMSDKPlaylist
 */
- (instancetype)initWithPlaylistName:(NSString *)playlistName contentType:(enum playlistContentTypeEnum)contentType;

/**
 *  Use this method to update a playlist items
 *  @warning This method is used internally to the SDK and
 *
 *  @param idNum is an NSNumber for the playlist id.
 *  @param playlistName is a name for the playlist
 *  @param contentType is an enum type for the playlist
 *
 *  @return instance of a CMSDKPlaylist
 */
- (void)updateItems:(NSArray *)newPlaylistItems;




/**
 *  Use this method to create a new playlist instance.
 *  @warning This method is used internally to the SDK and
 */
- (instancetype)initWithPlaylistDict: (NSDictionary *)newPlaylistDict;

/**
 *  Use this method to update a playlist instance.
 *  @warning This method is used internally to the SDK and
 */
- (BOOL)updateWithPlaylistDict: (NSDictionary *)newPlaylistDict;


- (id)copyWithZone:(NSZone *)zone;

@end

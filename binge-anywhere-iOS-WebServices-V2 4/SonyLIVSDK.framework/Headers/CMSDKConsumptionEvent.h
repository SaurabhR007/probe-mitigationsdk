//
//  CMSDKConsumptionEvent.h
//  CMSDKManager
//
//  Created on 12/21/15.
//  Copyright © 2015 Catch Media. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  CMSDKConsumptionEvent is used as a template for reporting media usage events.
 */
@interface CMSDKConsumptionEvent : NSObject <NSCopying, NSCoding>

/**
 *  Enum for restricting content types options
 */
typedef NS_ENUM(NSInteger, consumptionEventContentType) {
    contentTypeTrack = 1,
    contentTypeVideo,
    contentTypeFilm,
    contentTypeArtist,
    contentTypeEbook,
};

/**
 *  Enum for restricting event types options
 */
typedef NS_ENUM(NSInteger, consumptionEventType) {
    consumptionTypeStream = 1,
    consumptionTypeDownload,
    consumptionTypeCached,
    consumptionTypeSaved
};


@property (nonatomic, strong, getter=eventDict) NSMutableDictionary *eventDict;

-(id)copyWithZone:(NSZone *)zone;
-(void)encodeWithCoder:(NSCoder*)coder;

/**
 *  This method is used to initalize a consumption event, but does not start its reporting.
 *  The start method needs to be called to start tracking the event for reporting.
 *
 * @warning Unlike other events that need to be reported.  Consumption events are reported on their own.
 * There can only be one consumption event started at anytime.  If a new consumption event is started it 
 * will push any other consumption event to the server.
 *
 *  @param mediaId         NSString
 *  @param contentType     enum consumptionEventContentType
 *  @param consumptionType enum consumptionEventType
 *  @param deliveryId      NSString (optional)
 *  @param extraData       NSDictionary of additional key/value data (optional)
 *
 *  @return The returned instance is used to update the event
 */
-(instancetype)initWithMediaID:(NSString*)mediaId
                   contentType:(enum consumptionEventContentType)contentType
               consumptionType:(enum consumptionEventType)consumptionType
                    deliveryID:(NSString*)deliveryId
                     extraData:(NSDictionary*)extraData;
/**
 *  This method is used to initalize a consumption event, but does not start its reporting.
 *  The start method needs to be called to start tracking the event for reporting.
 *
 * @warning Unlike other events that need to be reported.  Consumption events are reported on their own.
 * There can only be one consumption event started at anytime.  If a new consumption event is started it
 * will push any other consumption event to the server.
 *
 *  @param mediaId         NSString
 *  @param contentType     enum consumptionEventContentType
 *  @param consumptionType enum consumptionEventType
 *  @param deliveryId      NSString (optional)
 *  @param extraData       NSDictionary of additional key/value data (optional)
 *  @param recordForReadIntervalSeconds how often to update server-side about current consumption positon
 *  @param lifeTimeDays for how many days consumption position should be kept
 *
 *  @return The returned instance is used to update the event
 */
-(instancetype)initWithMediaID:(NSString*)mediaId
                   contentType:(enum consumptionEventContentType)contentType
               consumptionType:(enum consumptionEventType)consumptionType
                    deliveryID:(NSString*)deliveryId
                     extraData:(NSDictionary*)extraData
  recordForReadIntervalSeconds:(double)recordForReadIntervalSeconds
                  lifeTimeDays:(double)lifeTimeDays
                    ;

/**
*  This method is used to initalize a consumption event, but does not start its reporting.
*  The start method needs to be called to start tracking the event for reporting.
*
* @warning Unlike other events that need to be reported.  Consumption events are reported on their own.
* There can only be one consumption event started at anytime.  If a new consumption event is started it
* will push any other consumption event to the server.
*
*  @param mediaId         NSString
*  @param contentType     enum consumptionEventContentType
*  @param consumptionType enum consumptionEventType
*  @param deliveryId      NSString (optional)
*  @param extraData       NSDictionary of additional key/value data (optional)
*  @param mediaDuration     this is media total duration
*
*  @return The returned instance is used to update the event
*/
-(instancetype)initWithMediaID:(NSString*)mediaId
                   contentType:(enum consumptionEventContentType)contentType
               consumptionType:(enum consumptionEventType)consumptionType
                    deliveryID:(NSString*)deliveryId
                     extraData:(NSDictionary*)extraData
                 mediaDuration:(double)mediaDuration;

/**
*  This method is used to initalize a consumption event, but does not start its reporting.
*  The start method needs to be called to start tracking the event for reporting.
*
* @warning Unlike other events that need to be reported.  Consumption events are reported on their own.
* There can only be one consumption event started at anytime.  If a new consumption event is started it
* will push any other consumption event to the server.
*
*  @param mediaId         NSString
*  @param contentType     enum consumptionEventContentType
*  @param consumptionType enum consumptionEventType
*  @param deliveryId      NSString (optional)
*  @param extraData       NSDictionary of additional key/value data (optional)
*  @param mediaDuration     this is media total duration
*  @param recordForReadIntervalSeconds how often to update server-side about current consumption positon
*  @param lifeTimeDays for how many days consumption position should be kept
*
*  @return The returned instance is used to update the event
*/
-(instancetype)initWithMediaID:(NSString*)mediaId
                   contentType:(enum consumptionEventContentType)contentType
               consumptionType:(enum consumptionEventType)consumptionType
                    deliveryID:(NSString*)deliveryId
                     extraData:(NSDictionary*)extraData
                 mediaDuration:(double)mediaDuration
  recordForReadIntervalSeconds:(double)seconds
                  lifeTimeDays:(double)days;

/**
 *  This method needs to be called to set the start position of the event and to start its tracking for reporting.
 *
 *  @param startPosition doouble of the start position in seconds
 */
-(void)start:(double)startPosition;

/**
 *  This method is used to update the media events listened to duration.
 *
 *  If you are updating a slider every second you could update this event at the same time.
 *  In this case you would send in the delta of listened time (1 second).
 *
 *  Using this delta allows you to track the duration of a play event even if the user scrubs within the media.
 *  Scrubbing in a media event could allow a duration to be much larger than the duration of the media itself.
 *
 *  @warning This generally needs to be the delta of a range.
 *
 *  @param deltaUpdate double of seconds
 */
-(void)update:(double)deltaUpdate;

/**
 *  This method is used to update the media events listened to duration.
 *
 *  If you are updating a slider every second you could update this event at the same time.
 *  In this case you would send in the delta of listened time (1 second).
 *
 *  Using this delta allows you to track the duration of a play event even if the user scrubs within the media.
 *  Scrubbing in a media event could allow a duration to be much larger than the duration of the media itself.
 *
 *  @warning This generally needs to be the delta of a range.
 *
 *  @param deltaUpdate double of seconds
 *  @param position current player position in seconds
 */
-(void)update:(double)deltaUpdate withPosition:(double)position;

/**
 *  This method is called to stop the event and to report it to webservices.
 *
 *  @param duration  double in seconds (if you are using the update: method -- this is the same delta value)
 *  @param stopTime  double of the stop time in seconds
 *  @param didFinish BOOL to report if the media did finish
 */
-(void)stopWithDuration:(double)duration stopTime:(double)stopTime didFinishContent:(BOOL)didFinish;

/**
 *  This method is called to stop the event and to report it to webservices. Allows to add extra data
 *
 *  @param duration  double in seconds (if you are using the update: method -- this is the same delta value)
 *  @param stopTime  double of the stop time in seconds
 *  @param didFinish BOOL to report if the media did finish
 *  @param extraData optional extra data to send with event
 */
-(void)stopWithDurationAndExtra:(double)duration stopTime:(double)stopTime didFinishContent:(BOOL)didFinish withExtraData:(NSDictionary*)extraData;

/**
 * Utility method to check if event has been started
 */
-(BOOL)isStarted;

@end

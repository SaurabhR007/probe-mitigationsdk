// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name ErosNow_iOS_SDK
import AVFoundation
@_exported import ErosNow_iOS_SDK
import Foundation
import Swift
import UIKit
public struct ErrorInfo : Swift.Equatable {
  public let statusCode: Swift.Int
  public let apiStatusCode: ErosNow_iOS_SDK.APIResponseStatusCode?
  public let errorMessage: Swift.String?
  public let response: ErosNow_iOS_SDK.Response?
  public init(statusCode: Swift.Int, apiStatusCode: ErosNow_iOS_SDK.APIResponseStatusCode? = nil, errorMessage: Swift.String? = Strings.Error.defaultErrorMessage, response: ErosNow_iOS_SDK.Response? = nil)
  public static func == (lhs: ErosNow_iOS_SDK.ErrorInfo, rhs: ErosNow_iOS_SDK.ErrorInfo) -> Swift.Bool
}
extension ErrorInfo : Swift.CustomStringConvertible, Swift.CustomDebugStringConvertible {
  public var description: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
}
public enum ENPlayerEvent : Swift.RawRepresentable, Swift.Equatable {
  case play
  case pause
  case resume
  case buffering
  case finish
  case failed
  case close
  case custom(event: Swift.String)
  public var rawValue: Swift.String {
    get
  }
  public init(rawValue: Swift.String)
  public typealias RawValue = Swift.String
}
public struct APIResponse<T> : Swift.Decodable where T : Swift.Decodable {
  public let status: Swift.String?
  public let statusCode: ErosNow_iOS_SDK.APIResponseStatusCode?
  public let message: Swift.String?
  public let data: T
  public init(from decoder: Swift.Decoder) throws
}
public enum APIResponseStatusCode : Swift.String, Swift.Decodable, Swift.Equatable, Swift.CaseIterable {
  case incorrectUsernamePassword
  case invalidOTP
  case success
  case emailOrMobileNotFound
  case accountUserNotFound
  case accountLinkExist
  case countryCodeHeaderMissing
  case invalidToken
  case tokenNotFound
  case tokenExpired
  case couldNotRefreshAuthToken
  case alreadySubscribed
  case userDoesNotExist
  case sessionExpired
  case concurrentStreaming
  case noSubscription
  case notLoggedIn
  case unknown
  public init(from decoder: Swift.Decoder) throws
  public init?(rawValue: Swift.String)
  public typealias AllCases = [ErosNow_iOS_SDK.APIResponseStatusCode]
  public typealias RawValue = Swift.String
  public static var allCases: [ErosNow_iOS_SDK.APIResponseStatusCode] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public enum ServerError : Swift.Error {
  case sessionExpired(ErosNow_iOS_SDK.ErrorInfo)
  case apiError(ErosNow_iOS_SDK.ErrorInfo)
  case unprocessableEntity(ErosNow_iOS_SDK.ErrorInfo)
  case unauthorised(ErosNow_iOS_SDK.ErrorInfo)
  case badRequest(ErosNow_iOS_SDK.ErrorInfo)
  case noServer(ErosNow_iOS_SDK.ErrorInfo)
  case serverUnavailable(ErosNow_iOS_SDK.ErrorInfo)
  case underlying(ErosNow_iOS_SDK.ErrorInfo)
  case unknownFailure(ErosNow_iOS_SDK.ErrorInfo)
}
extension ServerError : Foundation.LocalizedError {
  public var failureReason: Swift.String? {
    get
  }
  public var errorDescription: Swift.String? {
    get
  }
  public var localizedDescription: Swift.String {
    get
  }
}
extension ServerError : Swift.CustomStringConvertible, Swift.CustomDebugStringConvertible {
  public var description: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
}
public enum HTTPError : Swift.Error {
  case unknown(ErosNow_iOS_SDK.ErrorInfo)
  case cancelled(ErosNow_iOS_SDK.ErrorInfo)
  case timedOut(ErosNow_iOS_SDK.ErrorInfo)
  case cannotFindHost(ErosNow_iOS_SDK.ErrorInfo)
  case cannotConnectToHost(ErosNow_iOS_SDK.ErrorInfo)
  case connectionLost(ErosNow_iOS_SDK.ErrorInfo)
  case notConnectedToInternet(ErosNow_iOS_SDK.ErrorInfo)
  case unknownFailure(ErosNow_iOS_SDK.ErrorInfo)
}
extension HTTPError : Foundation.LocalizedError {
  public var failureReason: Swift.String? {
    get
  }
  public var errorDescription: Swift.String? {
    get
  }
  public var localizedDescription: Swift.String {
    get
  }
  public var errorInfo: ErosNow_iOS_SDK.ErrorInfo {
    get
  }
}
extension HTTPError : Swift.CustomStringConvertible, Swift.CustomDebugStringConvertible {
  public var description: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
}
public enum ClientError : Swift.Error {
  case sdkSetupNotDone(ErosNow_iOS_SDK.ErrorInfo)
  case contentIdIsEmpty(ErosNow_iOS_SDK.ErrorInfo)
}
extension ClientError : Foundation.LocalizedError {
  public var failureReason: Swift.String? {
    get
  }
  public var errorDescription: Swift.String? {
    get
  }
  public var localizedDescription: Swift.String {
    get
  }
  public var errorInfo: ErosNow_iOS_SDK.ErrorInfo {
    get
  }
}
extension ClientError : Swift.CustomStringConvertible, Swift.CustomDebugStringConvertible {
  public var description: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
}
public struct ENConfiguration {
  public var environment: ErosNow_iOS_SDK.EnvironmentType
  public var partnerCode: Swift.String
  public var apiClientId: Swift.String
  public var country: Swift.String
  public var deviceId: Swift.String
  public init(environment: ErosNow_iOS_SDK.EnvironmentType, partnerCode: Swift.String, apiClientId: Swift.String, country: Swift.String, deviceId: Swift.String)
}
public enum Strings {
  public enum Error {
    public static let defaultTitle: Swift.String
    public static let defaultErrorMessage: Swift.String
    public static let noInternetConnection: Swift.String
    public static let sessionExpired: Swift.String
    public static let sdkSetupNotDone: Swift.String
    public static let emptyContentId: Swift.String
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers final public class ENSDK : ObjectiveC.NSObject {
  public static let shared: ErosNow_iOS_SDK.ENSDK
  final public var configuration: ErosNow_iOS_SDK.ENConfiguration! {
    get
  }
  final public var enableLogs: Swift.Bool {
    get
    set
  }
  public static func setup(with configuration: ErosNow_iOS_SDK.ENConfiguration)
  final public func ssoLogin(partnerId: Swift.String, userToken: Swift.String, completion: @escaping (Swift.Result<Swift.String, ErosNow_iOS_SDK.ENError>) -> Swift.Void)
  final public func getContentProfile(contentId: Swift.String, completion: @escaping (Swift.Result<ErosNow_iOS_SDK.ENContentProfile, ErosNow_iOS_SDK.ENError>) -> Swift.Void)
  final public func logPlayerEvent(event: ErosNow_iOS_SDK.ENPlayerEvent)
  final public func playerClosed()
  final public func logout()
  @objc deinit
}
extension ENSDK {
  final public func initializePlayer(withAssetInfo info: ErosNow_iOS_SDK.ENPlaybackAssetInfo, avPlayer: AVFoundation.AVPlayer)
  final public func changeAssetForPlayer(assetInfo info: ErosNow_iOS_SDK.ENPlaybackAssetInfo, player avPlayer: AVFoundation.AVPlayer?)
}
extension ENSDK {
  @objc override final public func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
}
public struct ENPlaybackAssetInfo : Swift.Codable {
  public let playbackURL: Swift.String
  public let assetId: Swift.String
  public let assetTitle: Swift.String
  public let contentId: Swift.String
  public let contentType: ErosNow_iOS_SDK.ENContentType
  public init(playbackURL: Swift.String, assetId: Swift.String, assetTitle: Swift.String, contentId: Swift.String, contentType: ErosNow_iOS_SDK.ENContentType)
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
extension URLSessionConfiguration {
  public static var erosnowConfiguration: Foundation.URLSessionConfiguration {
    get
  }
}
public struct ENContentProfile : Swift.Decodable {
  public var assetId: Swift.String
  public var assetTitle: Swift.String
  public var contentId: Swift.String
  public var contentTitle: Swift.String?
  public var contentTypeId: ErosNow_iOS_SDK.ENContentType
  public var streamImageUrl: Swift.String?
  public var releaseDate: Swift.String?
  public var shortDescription: Swift.String?
  public var thumbnailUrl: Foundation.URL? {
    get
  }
  public var contentProgress: Swift.Int {
    get
  }
  public var cueTime: Swift.Int? {
    get
  }
  public var totalDuration: Swift.Int {
    get
  }
  public var isCompleted: Swift.Bool {
    get
  }
  public var streamUrl: Foundation.URL? {
    get
  }
  public var subtitle: [ErosNow_iOS_SDK.ENSubtitle]? {
    get
  }
  public init(from decoder: Swift.Decoder) throws
}
public struct ENSubtitle {
}
public enum ENContentType : Swift.String, Swift.Codable {
  case none
  case fullLength
  case trailer
  case musicVideo
  case musicAudio
  case dialoguePromo
  case theatricalTrailer
  case songPromo
  case events
  case makingOf
  case clipSong
  case clipScene
  case tvSeason
  case tvEpisode
  case originals
  case photoAlbum
  case unknown
  public init(from decoder: Swift.Decoder) throws
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum LoginMode : Swift.Equatable {
  case email(Swift.String)
  case mobile(number: Swift.String, callingCode: Swift.String)
  public static func == (a: ErosNow_iOS_SDK.LoginMode, b: ErosNow_iOS_SDK.LoginMode) -> Swift.Bool
}
public enum ENError : Swift.Error {
  case serverError(ErosNow_iOS_SDK.ServerError)
  case httpError(ErosNow_iOS_SDK.HTTPError)
  case clientError(ErosNow_iOS_SDK.ClientError)
}
extension ENError : Foundation.LocalizedError {
  public var failureReason: Swift.String? {
    get
  }
  public var errorDescription: Swift.String? {
    get
  }
  public var localizedDescription: Swift.String {
    get
  }
  public var errorInfo: ErosNow_iOS_SDK.ErrorInfo {
    get
  }
}
extension ENError : Swift.CustomStringConvertible, Swift.CustomDebugStringConvertible {
  public var description: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
}
public enum RetryResult {
  case retry
  case retryWithDelay(Foundation.TimeInterval)
  case doNotRetry
  case doNotRetryWithError(Swift.Error)
}
public protocol RequestRetrier {
  func retry(_ request: Foundation.URLRequest, for session: Foundation.URLSession, dueTo error: Swift.Error, with statusCode: Swift.Int?, completion: @escaping (ErosNow_iOS_SDK.RetryResult) -> Swift.Void)
}
extension RequestRetrier {
  public func retry(_ request: Foundation.URLRequest, for session: Foundation.URLSession, dueTo error: Swift.Error, with statusCode: Swift.Int?, completion: @escaping (ErosNow_iOS_SDK.RetryResult) -> Swift.Void)
}
@_hasMissingDesignatedInitializers open class Retrier : ErosNow_iOS_SDK.RequestRetrier {
  public func retry(_ request: Foundation.URLRequest, for session: Foundation.URLSession, dueTo error: Swift.Error, with statusCode: Swift.Int?, completion: @escaping (ErosNow_iOS_SDK.RetryResult) -> Swift.Void)
  @objc deinit
}
public struct Response : Swift.Equatable {
  public let statusCode: Swift.Int
  public let data: Foundation.Data?
  public let request: Foundation.URLRequest?
  public let response: Foundation.HTTPURLResponse?
  public init(statusCode: Swift.Int, data: Foundation.Data?, request: Foundation.URLRequest?, response: Foundation.HTTPURLResponse?)
  public static func == (a: ErosNow_iOS_SDK.Response, b: ErosNow_iOS_SDK.Response) -> Swift.Bool
}
public enum EnvironmentType : Swift.String, Swift.Equatable {
  case staging
  case production
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
extension ErosNow_iOS_SDK.APIResponseStatusCode : Swift.Hashable {}
extension ErosNow_iOS_SDK.APIResponseStatusCode : Swift.RawRepresentable {}
extension ErosNow_iOS_SDK.ENContentType : Swift.Equatable {}
extension ErosNow_iOS_SDK.ENContentType : Swift.Hashable {}
extension ErosNow_iOS_SDK.ENContentType : Swift.RawRepresentable {}
extension ErosNow_iOS_SDK.EnvironmentType : Swift.Hashable {}
extension ErosNow_iOS_SDK.EnvironmentType : Swift.RawRepresentable {}

//
//  NotificationService.swift
//  NotificationServices
//
//  Created by Shivam on 03/03/21.
//  Copyright © 2021 ttn. All rights reserved.
//

import UserNotifications
import MORichNotification

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        
        //TODO: Add your App Group ID
    
        MORichNotification.setAppGroupID("group.com.tatasky.binge.moengage")
        
        //Handle Rich Notification
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        MORichNotification.handle(request, withContentHandler: contentHandler)
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
}

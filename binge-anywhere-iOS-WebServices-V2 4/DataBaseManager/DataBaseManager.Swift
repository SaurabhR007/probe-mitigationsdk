//
//  DatsBaseManager.swift
//  BingeAnywhere
//
//  Created by Shivam on 04/08/20.
//  Copyright © 2020 ttn. All rights reserved.
//

import Foundation
import CoreData

class DataBaseManager {
    
    class func getContextObject() -> NSManagedObjectContext {
        let context :NSManagedObjectContext!
        context = kAppDelegate.persistentContainer.viewContext
        return context
    }
    
    // MARK: - Get Single ManageObject for given Entity With Query(Predicate) in Prefered ManagedObjectContext
    class  func object(entityName: String, predicate: NSPredicate, fromContext contextNew: NSManagedObjectContext) -> AnyObject? {
        var result: [AnyObject?]? = nil
        let request:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        request.entity = NSEntityDescription.entity(forEntityName: entityName, in: contextNew)
        request.predicate = predicate
        do {
            try result = contextNew.fetch(request)
            if (result?.count) ?? 0 > 0{
                return (result?[0])
            }
        } catch _ {
            print("failed operation")
        }
        return nil
    }
    
    class  func createObjectOfEntity(entityName: String, fromContext contextNew: NSManagedObjectContext) -> AnyObject {
        let object: NSManagedObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: contextNew)
        return object
    }
    
    // MARK: - Save LearnAction Event in DataBase
    class func saveLearnActionAnalyticsData(_ data: [String: Any],_ lastCallToLearnAction: Date) {
        let context = getContextObject()
        let predicate : NSPredicate = NSPredicate(format: "contentId = %d", data["contentId"] as? Int64 ?? 0)
        let newEntity = (object(entityName: "LADataEntry", predicate: predicate, fromContext: context) as? LADataEntry) ?? createObjectOfEntity(entityName: "LADataEntry", fromContext: context) as! LADataEntry
        newEntity.contentType = data["contentType"] as? String ?? ""
        newEntity.contentId = data["contentId"] as? Int64 ?? 0
        newEntity.timeStamp = lastCallToLearnAction
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    class func getLearnActionAnalyticsData(_ contentID: Int64) -> [LADataEntry]? {
        let context = getContextObject()
        let entity = NSEntityDescription.entity(forEntityName: "LADataEntry", in: context)
        let predicate : NSPredicate = NSPredicate(format: "contentId = %d", contentID)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.predicate = predicate
        request.entity = entity
        do {
            let result = try context.fetch(request) as! [LADataEntry]
            if result.count > 0 {
                print(result)
                return result
            }
        } catch {
        }
        return nil
    }
    
    
    
    // MARK: - Save JWT Token in DataBase
    class func saveJwtTokenData(_ data: [String: Any]) {
        let context = getContextObject()
        let predicate : NSPredicate = NSPredicate(format: "contentId = %d", data["contentId"] as? Int64 ?? 0)
        let newEntity = (object(entityName: "PlayerJWTToken", predicate: predicate, fromContext: context) as? PlayerJWTToken) ?? createObjectOfEntity(entityName: "PlayerJWTToken", fromContext: context) as! PlayerJWTToken
        newEntity.token = data["token"] as? String ?? ""
        newEntity.contentId = data["contentId"] as? Int64 ?? 0
        newEntity.usageCount = data["usageCount"] as? Int64 ?? 0
        newEntity.expiresIn = data["expiresIn"] as? Int64 ?? 0
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    class func getJwtTokenData(_ contentID: Int64) -> [PlayerJWTToken]? {
        let context = getContextObject()
        let entity = NSEntityDescription.entity(forEntityName: "PlayerJWTToken", in: context)
        let predicate : NSPredicate = NSPredicate(format: "contentId = %d", contentID)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.predicate = predicate
        request.entity = entity
        do {
            let result = try context.fetch(request) as! [PlayerJWTToken]
            if result.count > 0 {
                print(result)
                return result
            }
        } catch {
        }
        return nil
    }
    
    class func getTotalJwtTokenDataCount() -> Int {
        let context = getContextObject()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PlayerJWTToken")
        do {
            let count = try context.count(for: fetchRequest)
            print(count)
            return count
        } catch {
            print(error.localizedDescription)
        }
        return 0
    }
    
    class func getTotalJwtData() -> [PlayerJWTToken]? {
        let context = getContextObject()
        let entity = NSEntityDescription.entity(forEntityName: "PlayerJWTToken", in: context)
        let request = NSFetchRequest<NSFetchRequestResult>()
        let sortDescriptor = NSSortDescriptor(key: "usageCount", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        request.predicate = nil
        request.entity = entity
        do {
            let result = try context.fetch(request) as! [PlayerJWTToken]
            if result.count > 0 {
                print(result)
                return result
            }
        } catch {
        }
        return nil
    }
    
    class func deleteJwtToken(_ contentID: Int64) {
        let context = getContextObject()
        let entity = NSEntityDescription.entity(forEntityName: "PlayerJWTToken", in: context)
        let predicate : NSPredicate = NSPredicate(format: "contentId = %d", contentID)
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PlayerJWTToken")
        deleteFetch.entity = entity
        deleteFetch.predicate = predicate
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
}
